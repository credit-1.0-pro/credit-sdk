<?php

return array(
    CSRF_VERIFY_FAILURE => array(
        'id'=>CSRF_VERIFY_FAILURE,
        'link'=>'',
        'status'=>403,
        'code'=>'CSRF_VERIFY_FAILURE',
        'title'=>'当前页面信息失效，请刷新重试',
        'detail'=>'当前页面信息失效，请刷新重试',
        'source'=>array(
        ),
        'meta'=>array()
    ),
    AFS_VERIFY_FAILURE => array(
        'id'=>AFS_VERIFY_FAILURE,
        'link'=>'',
        'status'=>403,
        'code'=>'AFS_VERIFY_FAILURE',
        'title'=>'滑动验证失效,请刷新页面',
        'detail'=>'滑动验证失效,请刷新页面',
        'source'=>array(
        ),
        'meta'=>array()
    ),
    NEED_SIGNIN => array(
        'id'=>NEED_SIGNIN,
        'link'=>'',
        'status'=>403,
        'code'=>'NEED_SIGNIN',
        'title'=>'登录状态已失效，请您重新登录',
        'detail'=>'登录状态已失效，请您重新登录',
        'source'=>array(
        ),
        'meta'=>array()
    ),
    SMS_SEND_TOO_QUICK => array(
        'id'=>SMS_SEND_TOO_QUICK,
        'link'=>'',
        'status'=>403,
        'code'=>'SMS_SEND_TOO_QUICK',
        'title'=>'短信发送太频繁,请稍后再试',
        'detail'=>'短信发送太频繁,请稍后再试',
        'source'=>array(
        ),
        'meta'=>array()
    ),
    HASH_INVALID => array(
        'id'=>HASH_INVALID,
        'link'=>'',
        'status'=>403,
        'code'=>'HASH_INVALID',
        'title'=>'哈希无效,请刷新页面',
        'detail'=>'哈希无效,请刷新页面',
        'source'=>array(
        ),
        'meta'=>array()
    ),
    LOGIN_EXPIRED => array(
        'id'=>LOGIN_EXPIRED,
        'link'=>'',
        'status'=>403,
        'code'=>'LOGIN_EXPIRED',
        'title'=>'登录状态已失效，请您重新登录',
        'detail'=>'登录状态已失效，请您重新登录',
        'source'=>array(
        ),
        'meta'=>array()
    ),
    PURVIEW_UNDEFINED => array(
        'id'=>PURVIEW_UNDEFINED,
        'link'=>'',
        'status'=>403,
        'code'=>'PURVIEW_UNDEFINED',
        'title'=>'该账号未分配权限，请联系管理员！',
        'detail'=>'该账号未分配权限，请联系管理员！',
        'source'=>array(
            'pointer'=>'purview'
        ),
        'meta'=>array()
    ),
    CAPTCHA_ERROR => array(
        'id'=>CAPTCHA_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CAPTCHA_ERROR',
        'title'=>'验证码不正确,请重新输入',
        'detail'=>'验证码不正确,请重新输入',
        'source'=>array(
            'pointer'=>'captcha'
        ),
        'meta'=>array()
    ),
    PARAMETER_IS_UNIQUE => array(
        'id' => PARAMETER_IS_UNIQUE,
        'link' => '',
        'status' => 403,
        'code' => 'PARAMETER_IS_UNIQUE',
        'title' => '数据已经存在,请重新输入',
        'detail' => '数据已经存在,请重新输入',
        'source' => array(
            'pointer' => ''
        ),
        'meta' => array()
    ),
    PARAMETER_FORMAT_ERROR => array(
        'id' => PARAMETER_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'PARAMETER_FORMAT_ERROR',
        'title' => '数据格式不正确,请重新输入',
        'detail' => '数据格式不正确,请重新输入',
        'source' => array(
            'pointer' => ''
        ),
        'meta' => array()
    ),
    PARAMETER_IS_EMPTY => array(
        'id'=>PARAMETER_IS_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'PARAMETER_IS_EMPTY',
        'title'=>'数据不能为空,请继续完善信息',
        'detail'=>'数据不能为空,请继续完善信息',
        'source'=>array(
            'pointer'=>''
        ),
        'meta'=>array()
    ),
    STATUS_CAN_NOT_MODIFY => array(
        'id' => STATUS_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'STATUS_CAN_NOT_MODIFY',
        'title'=>'该状态不能进行此操作,请刷新页面后重新尝试',
        'detail'=>'该状态不能进行此操作,请刷新页面后重新尝试',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    APPLY_STATUE_CAN_NOT_MODIFY => array(
        'id'=>APPLY_STATUE_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'APPLY_STATUE_CAN_NOT_MODIFY',
        'title'=>'审核状态不能操作',
        'detail'=>'审核状态不能操作',
        'source'=>array(
            'pointer'=>'applyStatus'
        ),
        'meta'=>array()
    ),
    IMAGE_FORMAT_ERROR => array(
        'id'=>IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'IMAGE_FORMAT_ERROR',
        'title'=>'图片格式不正确,应为jpg,jpeg,png格式,请重新上传',
        'detail'=>'图片格式不正确,应为jpg,jpeg,png格式,请重新上传',
        'source'=>array(
            'pointer'=>'image'
        ),
        'meta'=>array()
    ),
    ATTACHMENT_FORMAT_ERROR => array(
        'id'=>ATTACHMENT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ATTACHMENT_FORMAT_ERROR',
        'title'=>'附件格式不正确,应为zip, doc, docx, xls, xlsx, pdf, rar, ppt 格式,请重新上传',
        'detail'=>'附件格式不正确,应为zip, doc, docx, xls, xlsx, pdf, rar, ppt 格式,请重新上传',
        'source'=>array(
            'pointer'=>'attachments'
        ),
        'meta'=>array()
    ),
    REAL_NAME_FORMAT_ERROR => array(
        'id'=>REAL_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'REAL_NAME_FORMAT_ERROR',
        'title'=>'姓名格式不正确,应为2-15位字符,请重新输入',
        'detail'=>'姓名格式不正确,应为2-15位字符,请重新输入',
        'source'=>array(
            'pointer'=>'realName'
        ),
        'meta'=>array()
    ),
    CELLPHONE_FORMAT_ERROR => array(
        'id'=>CELLPHONE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CELLPHONE_FORMAT_ERROR',
        'title'=>'手机号格式不正确,应为11位数字,请重新输入',
        'detail'=>'手机号格式不正确,应为11位数字,请重新输入',
        'source'=>array(
            'pointer'=>'cellphone'
        ),
        'meta'=>array()
    ),
    CARD_ID_FORMAT_ERROR => array(
        'id'=>CARD_ID_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CARD_ID_FORMAT_ERROR',
        'title'=>'身份号格式不正确,应为15或18位数字或数字+大写字母X,请重新输入',
        'detail'=>'身份号格式不正确,应为15或18位数字或数字+大写字母X,请重新输入',
        'source'=>array(
            'pointer'=>'cardId'
        ),
        'meta'=>array()
    ),
    NAME_FORMAT_ERROR => array(
        'id'=>NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NAME_FORMAT_ERROR',
        'title'=>'名称格式不正确,应为2-10位字符,请重新输入',
        'detail'=>'名称格式不正确,应为2-10位字符,请重新输入',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    TITLE_FORMAT_ERROR => array(
        'id'=>TITLE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TITLE_FORMAT_ERROR',
        'title'=>'标题格式不正确,应为5-80位字符,请重新输入',
        'detail'=>'标题格式不正确,应为5-80位字符,请重新输入',
        'source'=>array(
            'pointer'=>'title'
        ),
        'meta'=>array()
    ),
    DESCRIPTION_FORMAT_ERROR => array(
        'id'=>DESCRIPTION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'DESCRIPTION_FORMAT_ERROR',
        'title'=>'描述格式不正确,应为1-200位字符,请重新输入',
        'detail'=>'描述格式不正确,应为1-200位字符,请重新输入',
        'source'=>array(
            'pointer'=>''
        ),
        'meta'=>array()
    ),
    REASON_FORMAT_ERROR => array(
        'id'=>REASON_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'REASON_FORMAT_ERROR',
        'title'=>'原因格式不正确,应为1-200位字符,请重新输入',
        'detail'=>'原因格式不正确,应为1-200位字符,请重新输入',
        'source'=>array(
            'pointer'=>'reason'
        ),
        'meta'=>array()
    ),
    SOURCE_FORMAT_ERROR => array(
        'id' => SOURCE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SOURCE_FORMAT_ERROR',
        'title'=>'来源格式不正确,应为2-15位字符,请重新输入',
        'detail'=>'来源格式不正确,应为2-15位字符,请重新输入',
        'source'=>array(
            'pointer'=>'source'
        ),
        'meta'=>array()
    ),
    STATUS_FORMAT_ERROR => array(
        'id' => STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'STATUS_FORMAT_ERROR',
        'title'=>'状态格式不正确,请重新输入',
        'detail'=>'状态格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    STATUS_NOT_ENABLE => array(
        'id' => STATUS_NOT_ENABLE,
        'link'=>'',
        'status'=>403,
        'code'=>'STATUS_NOT_ENABLE',
        'title'=>'状态非启用',
        'detail'=>'状态非启用',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    STATUS_NOT_DISABLE => array(
        'id' => STATUS_NOT_DISABLE,
        'link'=>'',
        'status'=>403,
        'code'=>'STATUS_NOT_DISABLE',
        'title'=>'状态非禁用',
        'detail'=>'状态非禁用',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    STATUS_NOT_TOP => array(
        'id' => STATUS_NOT_TOP,
        'link'=>'',
        'status'=>403,
        'code'=>'STATUS_NOT_TOP',
        'title'=>'状态非置顶',
        'detail'=>'状态非置顶',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    STATUS_NOT_CANCEL_TOP => array(
        'id' => STATUS_NOT_CANCEL_TOP,
        'link'=>'',
        'status'=>403,
        'code'=>'STATUS_NOT_CANCEL_TOP',
        'title'=>'状态已置顶',
        'detail'=>'状态已置顶',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    UNIFIED_SOCIAL_CREDIT_CODE_FORMAT_ERROR => array(
        'id' => UNIFIED_SOCIAL_CREDIT_CODE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'UNIFIED_SOCIAL_CREDIT_CODE_FORMAT_ERROR',
        'title'=>'统一社会信用代码格式不正确',
        'detail'=>'统一社会信用代码格式不正确',
        'source'=>array(
            'pointer'=>'unifiedSocialCreditCode'
        ),
        'meta'=>array()
    ),
    STICK_FORMAT_ERROR => array(
        'id' => STICK_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'STICK_FORMAT_ERROR',
        'title'=>'置顶状态格式不正确,请重新输入',
        'detail'=>'置顶状态格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'stick'
        ),
        'meta'=>array()
    ),
    JWT_TOKEN_EMPTY => array(
        'id'=>JWT_TOKEN_EMPTY,
        'link'=>'',
        'status'=>409,
        'code'=>'JWT_TOKEN_EMPTY',
        'title'=>'jwt-token 为空，您没有访问权限',
        'detail'=>'jwt-token 为空，您没有访问权限',
        'source'=>array(
            'pointer'=>'jwt'
        ),
        'meta'=>array()
    ),
    JWT_TOKEN_OVERDUE => array(
        'id'=>JWT_TOKEN_OVERDUE,
        'link'=>'',
        'status'=>409,
        'code'=>'JWT_TOKEN_OVERDUE',
        'title'=>'jwt-token 已过期，请重新登录',
        'detail'=>'jwt-token 已过期，请重新登录',
        'source'=>array(
            'pointer'=>'jwt'
        ),
        'meta'=>array()
    ),
    JWT_TOKEN_ERROR => array(
        'id'=>JWT_TOKEN_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'JWT_TOKEN_ERROR',
        'title'=>'jwt-token 验证失败,请重新登录',
        'detail'=>'jwt-token 验证失败,请重新登录',
        'source'=>array(
            'pointer'=>'jwt'
        ),
        'meta'=>array()
    ),
    FILE_IS_EMPTY => array(
        'id'=>FILE_IS_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_IS_EMPTY',
        'title'=>'文件为空,请继续完善信息',
        'detail'=>'文件为空,请继续完善信息',
        'source'=>array(
            'pointer'=>'file'
        ),
        'meta'=>array()
    ),
    FILE_FORMAT_ERROR => array(
        'id'=>FILE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_FORMAT_ERROR',
        'title'=>'文件格式不正确,请重新输入',
        'detail'=>'文件格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'file'
        ),
        'meta'=>array()
    ),
    FILE_SIZE_FORMAT_ERROR => array(
        'id'=>FILE_SIZE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_SIZE_FORMAT_ERROR',
        'title'=>'文件大小超出限制,请重新输入',
        'detail'=>'文件大小超出限制,请重新输入',
        'source'=>array(
            'pointer'=>'fileSize'
        ),
        'meta'=>array()
    ),
    FILE_SIZE_FORMAT_ERROR => array(
        'id'=>FILE_SIZE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_SIZE_FORMAT_ERROR',
        'title'=>'文件大小超出限制,请重新输入',
        'detail'=>'文件大小超出限制,请重新输入',
        'source'=>array(
            'pointer'=>'fileSize'
        ),
        'meta'=>array()
    ),
    FILE_EXTENSION_FORMAT_ERROR => array(
        'id'=>FILE_EXTENSION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_EXTENSION_FORMAT_ERROR',
        'title'=>'文件后缀不支持,请重新输入',
        'detail'=>'文件后缀不支持,请重新输入',
        'source'=>array(
            'pointer'=>'fileExtension'
        ),
        'meta'=>array()
    ),
    FILE_IS_UNIQUE => array(
        'id'=>FILE_IS_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_IS_UNIQUE',
        'title'=>'文件重复上传,请重新输入',
        'detail'=>'文件重复上传,请重新输入',
        'source'=>array(
            'pointer'=>'file'
        ),
        'meta'=>array()
    ),
    PASSWORD_FORMAT_ERROR => array(
        'id'=>PASSWORD_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'PASSWORD_FORMAT_ERROR',
        'title'=>'密码格式不正确,应为8-20位大写字母+小写字母+数字+特殊字符的组合,请重新输入',
        'detail'=>'密码格式不正确,应为8-20位大写字母+小写字母+数字+特殊字符的组合,请重新输入',
        'source'=>array(
            'pointer'=>'password'
        ),
        'meta'=>array()
    ),
    OLD_PASSWORD_INCORRECT => array(
        'id'=>OLD_PASSWORD_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'OLD_PASSWORD_INCORRECT',
        'title'=>'当前密码不正确,请重新输入',
        'detail'=>'当前密码不正确,请重新输入',
        'source'=>array(
            'pointer'=>'oldPassword'
        ),
        'meta'=>array()
    ),
    PASSWORD_INCORRECT => array(
        'id'=>PASSWORD_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PASSWORD_INCORRECT',
        'title'=>'密码不正确,请重新输入',
        'detail'=>'密码不正确,请重新输入',
        'source'=>array(
            'pointer'=>'password'
        ),
        'meta'=>array()
    ),
    PASSWORD_INCONSISTENCY => array(
        'id'=>PASSWORD_INCONSISTENCY,
        'link'=>'',
        'status'=>403,
        'code'=>'PASSWORD_INCONSISTENCY',
        'title'=>'密码不一致,请重新输入',
        'detail'=>'密码不一致,请重新输入',
        'source'=>array(
            'pointer'=>'password'
        ),
        'meta'=>array()
    ),
    CELLPHONE_EXIST => array(
        'id'=>CELLPHONE_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'CELLPHONE_EXIST',
        'title'=>'手机号已经存在,请重新输入',
        'detail'=>'手机号已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'cellphone'
        ),
        'meta'=>array()
    ),
    CELLPHONE_NOT_EXIST => array(
        'id'=>CELLPHONE_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'CELLPHONE_NOT_EXIST',
        'title'=>'该账号不存在，请重新输入',
        'detail'=>'该账号不存在，请重新输入',
        'source'=>array(
            'pointer'=>'cellphone'
        ),
        'meta'=>array()
    ),
    USER_STATUS_DISABLE => array(
        'id'=>USER_STATUS_DISABLE,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_STATUS_DISABLE',
        'title'=>'当前账号已被禁用，请联系管理员查询具体情况',
        'detail'=>'当前账号已被禁用，请联系管理员查询具体情况',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    APPLY_CREW_FORMAT_ERROR => array(
        'id' => APPLY_CREW_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'APPLY_CREW_FORMAT_ERROR',
        'title'=>'审核人格式不正确,请重新输入',
        'detail'=>'审核人格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'applyCrewId'
        ),
        'meta'=>array()
    ),
    CREW_ID_FORMAT_ERROR => array(
        'id' => CREW_ID_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_ID_FORMAT_ERROR',
        'title'=>'发布人格式不正确,请重新输入',
        'detail'=>'发布人格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'crewId'
        ),
        'meta'=>array()
    ),
    USER_GROUP_NAME_FORMAT_ERROR => array(
        'id'=>USER_GROUP_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_GROUP_NAME_FORMAT_ERROR',
        'title'=>'委办局名称格式不正确, 应为2-20位字符, 请重新输入',
        'detail'=>'委办局名称格式不正确, 应为2-20位字符, 请重新输入',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    USER_GROUP_NAME_IS_UNIQUE => array(
        'id'=>USER_GROUP_NAME_IS_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_GROUP_NAME_IS_UNIQUE',
        'title'=>'委办局名称已经存在，请重新输入',
        'detail'=>'委办局名称已经存在，请重新输入',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    USER_GROUP_SHORT_NAME_FORMAT_ERROR => array(
        'id'=>USER_GROUP_SHORT_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_GROUP_SHORT_NAME_FORMAT_ERROR',
        'title'=>'委办局简称格式不正确, 应为1-8位字符, 请重新输入',
        'detail'=>'委办局简称格式不正确, 应为1-8位字符, 请重新输入',
        'source'=>array(
            'pointer'=>'shortName'
        ),
        'meta'=>array()
    ),
    USER_GROUP_ADMINISTRATIVE_AREA_FORMAT_ERROR => array(
        'id'=>USER_GROUP_ADMINISTRATIVE_AREA_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_GROUP_ADMINISTRATIVE_AREA_FORMAT_ERROR',
        'title'=>'所属行政区域格式不正确,请重新输入',
        'detail'=>'所属行政区域格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'administrativeArea'
        ),
        'meta'=>array()
    ),
    DEPARTMENT_NAME_FORMAT_ERROR => array(
        'id'=>DEPARTMENT_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'DEPARTMENT_NAME_FORMAT_ERROR',
        'title'=>'科室名称格式不正确, 应为2-20位字符, 请重新输入',
        'detail'=>'科室名称格式不正确, 应为2-20位字符, 请重新输入',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    DEPARTMENT_NAME_IS_UNIQUE => array(
        'id'=>DEPARTMENT_NAME_IS_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'DEPARTMENT_NAME_IS_UNIQUE',
        'title'=>'科室名称已经存在,请重新输入',
        'detail'=>'科室名称已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    DEPARTMENT_USER_GROUP_IS_EMPTY => array(
        'id'=>DEPARTMENT_USER_GROUP_IS_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'DEPARTMENT_USER_GROUP_IS_EMPTY',
        'title'=>'所属委办局不能为空,请继续完善信息',
        'detail'=>'所属委办局不能为空,请继续完善信息',
        'source'=>array(
            'pointer'=>'userGroup'
        ),
        'meta'=>array()
    ),
    DEPARTMENT_USER_GROUP_FORMAT_ERROR => array(
        'id'=>DEPARTMENT_USER_GROUP_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'DEPARTMENT_USER_GROUP_FORMAT_ERROR',
        'title'=>'所属委办局格式不正确,请重新输入',
        'detail'=>'所属委办局格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'userGroup'
        ),
        'meta'=>array()
    ),
    CREW_CATEGORY_NOT_EXIST => array(
        'id'=>CREW_CATEGORY_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_CATEGORY_NOT_EXIST',
        'title'=>'员工类型不存在,请核对后重新输入',
        'detail'=>'员工类型不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'category'
        ),
        'meta'=>array()
    ),
    CREW_PURVIEW_FORMAT_ERROR => array(
        'id'=>CREW_PURVIEW_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_PURVIEW_FORMAT_ERROR',
        'title'=>'员工权限范围格式不正确,请重新输入',
        'detail'=>'员工权限范围格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'purview'
        ),
        'meta'=>array()
    ),
    CREW_PURVIEW_HIERARCHY_FORMAT_ERROR => array(
        'id'=>CREW_PURVIEW_HIERARCHY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_PURVIEW_HIERARCHY_FORMAT_ERROR',
        'title'=>'员工类型权限不足',
        'detail'=>'员工类型权限不足',
        'source'=>array(
            'pointer'=>'purview'
        ),
        'meta'=>array()
    ),
    DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP => array(
        'id'=>DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP,
        'link'=>'',
        'status'=>403,
        'code'=>'DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP',
        'title'=>'该所属科室不属于该所属委办局,请重新输入',
        'detail'=>'该所属科室不属于该所属委办局,请重新输入',
        'source'=>array(
            'pointer'=>'department'
        ),
        'meta'=>array()
    ),
    CREW_USER_GROUP_FORMAT_ERROR => array(
        'id'=>CREW_USER_GROUP_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_USER_GROUP_FORMAT_ERROR',
        'title'=>'所属委办局格式不正确,请重新输入',
        'detail'=>'所属委办局格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'department'
        ),
        'meta'=>array()
    ),
    CREW_DEPARTMENT_FORMAT_ERROR => array(
        'id'=>CREW_DEPARTMENT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_DEPARTMENT_FORMAT_ERROR',
        'title'=>'所属科室格式不正确,请重新输入',
        'detail'=>'所属科室格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'department'
        ),
        'meta'=>array()
    ),
    EMAIL_FORMAT_ERROR => array(
        'id' => EMAIL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'EMAIL_FORMAT_ERROR',
        'title'=>'邮箱格式不正确,应为2-30位字符,且必须包含"@"和".",请重新输入',
        'detail'=>'邮箱格式不正确,应为2-30位字符,且必须包含"@"和".",请重新输入',
        'source'=>array(
            'pointer'=>'email'
        ),
        'meta'=>array()
    ),
    USER_NAME_NOT_EXIT => array(
        'id' => USER_NAME_NOT_EXIT,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_NAME_NOT_EXIT',
        'title'=>'用户名不存在,请核对后重新输入',
        'detail'=>'用户名不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'userName'
        ),
        'meta'=>array()
    ),
    CONTACT_ADDRESS_FORMAT_ERROR => array(
        'id' => CONTACT_ADDRESS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CONTACT_ADDRESS_FORMAT_ERROR',
        'title'=>'联系地址格式不正确,应为1-255位字符,请重新输入',
        'detail'=>'联系地址格式不正确,应为1-255位字符,请重新输入',
        'source'=>array(
            'pointer'=>'contactAddress'
        ),
        'meta'=>array()
    ),
    SECURITY_QUESTION_FORMAT_ERROR => array(
        'id' => SECURITY_QUESTION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SECURITY_QUESTION_FORMAT_ERROR',
        'title'=>'密保问题格式不正确,请重新输入',
        'detail'=>'密保问题格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'securityQuestion'
        ),
        'meta'=>array()
    ),
    SECURITY_ANSWER_FORMAT_ERROR => array(
        'id' => SECURITY_ANSWER_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SECURITY_ANSWER_FORMAT_ERROR',
        'title'=>'密保答案格式不正确,应为1-30位字符,请重新输入',
        'detail'=>'密保答案格式不正确,应为1-30位字符,请重新输入',
        'source'=>array(
            'pointer'=>'securityAnswer'
        ),
        'meta'=>array()
    ),
    GENDER_FORMAT_ERROR => array(
        'id' => GENDER_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'GENDER_FORMAT_ERROR',
        'title'=>'性别格式不正确,请重新输入',
        'detail'=>'性别格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'email'
        ),
        'meta'=>array()
    ),
    USER_NAME_EXIST => array(
        'id' => USER_NAME_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_NAME_EXIST',
        'title'=>'用户名已经存在,请重新输入',
        'detail'=>'用户名已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'userName'
        ),
        'meta'=>array()
    ),
    EMAIL_EXIST => array(
        'id' => EMAIL_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'EMAIL_EXIST',
        'title'=>'邮箱已经存在,请重新输入',
        'detail'=>'邮箱已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'email'
        ),
        'meta'=>array()
    ),
    USER_NAME_FORMAT_ERROR => array(
        'id' => USER_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_NAME_FORMAT_ERROR',
        'title'=>'用户名格式不正确,应为2-20位字符,请重新输入',
        'detail'=>'用户名格式不正确,应为2-20位字符,请重新输入',
        'source'=>array(
            'pointer'=>'userName'
        ),
        'meta'=>array()
    ),
    SECURITY_ANSWER_INCORRECT => array(
        'id' => SECURITY_ANSWER_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'SECURITY_ANSWER_INCORRECT',
        'title'=>'密保答案不正确,请重新输入',
        'detail'=>'密保答案不正确,请重新输入',
        'source'=>array(
            'pointer'=>'userName'
        ),
        'meta'=>array()
    ),
    NEWS_CATEGORY_NAME_FORMAT_ERROR => array(
        'id' => NEWS_CATEGORY_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NEWS_CATEGORY_NAME_FORMAT_ERROR',
        'title'=>'分类名称格式不正确, 应为2-20位字符, 请重新输入',
        'detail'=>'分类名称格式不正确, 应为2-20位字符, 请重新输入',
        'source'=>array(
            'pointer'=>'newsCategoryName'
        ),
        'meta'=>array()
    ),
    NEWS_CATEGORY_NAME_IS_UNIQUE => array(
        'id' => NEWS_CATEGORY_NAME_IS_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'NEWS_CATEGORY_NAME_IS_UNIQUE',
        'title'=>'分类名称已经存在，请重新输入',
        'detail'=>'分类名称已经存在，请重新输入',
        'source'=>array(
            'pointer'=>'newsCategoryName'
        ),
        'meta'=>array()
    ),
    NEWS_CATEGORY_GRADE_FORMAT_ERROR => array(
        'id' => NEWS_CATEGORY_GRADE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NEWS_CATEGORY_GRADE_FORMAT_ERROR',
        'title'=>'分类等级格式不正确,请重新输入',
        'detail'=>'分类等级格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'newsCategoryGrade'
        ),
        'meta'=>array()
    ),
    NEWS_CATEGORY_PARENT_CATEGORY_FORMAT_ERROR => array(
        'id' => NEWS_CATEGORY_PARENT_CATEGORY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NEWS_CATEGORY_PARENT_CATEGORY_FORMAT_ERROR',
        'title'=>'一级分类格式不正确,请重新输入',
        'detail'=>'一级分类格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'parentCategory'
        ),
        'meta'=>array()
    ),
    NEWS_CATEGORY_CATEGORY_FORMAT_ERROR => array(
        'id' => NEWS_CATEGORY_CATEGORY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NEWS_CATEGORY_CATEGORY_FORMAT_ERROR',
        'title'=>'二级分类格式不正确,请重新输入',
        'detail'=>'二级分类格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'category'
        ),
        'meta'=>array()
    ),
    NEWS_CATEGORY_PARENT_CATEGORY_NOT_EXIST => array(
        'id' => NEWS_CATEGORY_PARENT_CATEGORY_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'NEWS_CATEGORY_PARENT_CATEGORY_NOT_EXIST',
        'title'=>'一级分类不存在，请核对后重新输入',
        'detail'=>'一级分类不存在，请核对后重新输入',
        'source'=>array(
            'pointer'=>'parentCategory'
        ),
        'meta'=>array()
    ),
    NEWS_CATEGORY_CATEGORY_NOT_EXIST => array(
        'id' => NEWS_CATEGORY_CATEGORY_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'NEWS_CATEGORY_CATEGORY_NOT_EXIST',
        'title'=>'二级分类不存在，请核对后重新输入',
        'detail'=>'二级分类不存在，请核对后重新输入',
        'source'=>array(
            'pointer'=>'category'
        ),
        'meta'=>array()
    ),
    CATEGORY_NOT_BELONG_TO_THE_PARENT_CATEGORY => array(
        'id' => CATEGORY_NOT_BELONG_TO_THE_PARENT_CATEGORY,
        'link'=>'',
        'status'=>403,
        'code'=>'CATEGORY_NOT_BELONG_TO_THE_PARENT_CATEGORY',
        'title'=>'二级分类不属于一级分类，请重新输入',
        'detail'=>'二级分类不属于一级分类，请重新输入',
        'source'=>array(
            'pointer'=>'categoryNotBelongToParentCategory'
        ),
        'meta'=>array()
    ),
    NEWS_CONTENT_FORMAT_ERROR => array(
        'id' => NEWS_CONTENT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NEWS_CONTENT_FORMAT_ERROR',
        'title'=>'内容格式不正确,请重新输入',
        'detail'=>'内容格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'content'
        ),
        'meta'=>array()
    ),
    NEWS_TYPE_FORMAT_ERROR => array(
        'id' => NEWS_TYPE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NEWS_TYPE_FORMAT_ERROR',
        'title'=>'新闻分类格式不正确,请重新输入',
        'detail'=>'新闻分类格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'newsType'
        ),
        'meta'=>array()
    ),
    NEWS_TYPE_NOT_EXIT => array(
        'id' => NEWS_TYPE_NOT_EXIT,
        'link'=>'',
        'status'=>403,
        'code'=>'NEWS_TYPE_NOT_EXIT',
        'title'=>'新闻分类不存在，请核对后重新输入',
        'detail'=>'新闻分类不存在，请核对后重新输入',
        'source'=>array(
            'pointer'=>'newsType'
        ),
        'meta'=>array()
    ),
    DIMENSION_FORMAT_ERROR => array(
        'id' => DIMENSION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'DIMENSION_FORMAT_ERROR',
        'title'=>'公开类型格式不正确,请重新输入',
        'detail'=>'公开类型格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'dimension'
        ),
        'meta'=>array()
    ),
    HOME_PAGE_SHOW_STATUS_FORMAT_ERROR => array(
        'id' => HOME_PAGE_SHOW_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HOME_PAGE_SHOW_STATUS_FORMAT_ERROR',
        'title'=>'首页展示状态格式不正确,请重新输入',
        'detail'=>'首页展示状态格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'homePageShowStatus'
        ),
        'meta'=>array()
    ),
    BANNER_STATUS_FORMAT_ERROR => array(
        'id' => BANNER_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'BANNER_STATUS_FORMAT_ERROR',
        'title'=>'轮播状态格式不正确,请重新输入',
        'detail'=>'轮播状态格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'bannerStatus'
        ),
        'meta'=>array()
    ),
    BANNER_IMAGE_FORMAT_ERROR => array(
        'id' => BANNER_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'BANNER_IMAGE_FORMAT_ERROR',
        'title'=>'图片格式不正确,应为jpg,jpeg,png格式,请重新上传',
        'detail'=>'图片格式不正确,应为jpg,jpeg,png格式,请重新上传',
        'source'=>array(
            'pointer'=>'bannerImage'
        ),
        'meta'=>array()
    ),
    RELEASE_TIME_FORMAT_ERROR => array(
        'id' => RELEASE_TIME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RELEASE_TIME_FORMAT_ERROR',
        'title'=>'发布时间格式不正确,请重新输入',
        'detail'=>'发布时间格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'releaseTime'
        ),
        'meta'=>array()
    ),
    TEMPLATE_NAME_FORMAT_ERROR => array(
        'id' => TEMPLATE_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_NAME_FORMAT_ERROR',
        'title'=>'目录名称格式不正确,应为1-100位字符,请重新输入',
        'detail'=>'目录名称格式不正确,应为1-100位字符,请重新输入',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    TEMPLATE_IDENTIFY_FORMAT_ERROR => array(
        'id' => TEMPLATE_IDENTIFY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_IDENTIFY_FORMAT_ERROR',
        'title'=>'目录标识格式不正确,应为1-100位大写字母,请重新输入',
        'detail'=>'目录标识格式不正确,应为1-100位大写字母,请重新输入',
        'source'=>array(
            'pointer'=>'identify'
        ),
        'meta'=>array()
    ),
    TEMPLATE_SUBJECT_CATEGORY_FORMAT_ERROR => array(
        'id' => TEMPLATE_SUBJECT_CATEGORY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_SUBJECT_CATEGORY_FORMAT_ERROR',
        'title'=>'主体类别格式不正确,请重新输入',
        'detail'=>'主体类别格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'subjectCategory'
        ),
        'meta'=>array()
    ),
    TEMPLATE_DIMENSION_FORMAT_ERROR => array(
        'id' => TEMPLATE_DIMENSION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_DIMENSION_FORMAT_ERROR',
        'title'=>'公开范围格式不正确,请重新输入',
        'detail'=>'公开范围格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'dimension'
        ),
        'meta'=>array()
    ),
    TEMPLATE_EXCHANGE_FREQUENCY_FORMAT_ERROR => array(
        'id' => TEMPLATE_EXCHANGE_FREQUENCY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_EXCHANGE_FREQUENCY_FORMAT_ERROR',
        'title'=>'更新频率格式不正确,请重新输入',
        'detail'=>'更新频率格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'exchangeFrequency'
        ),
        'meta'=>array()
    ),
    TEMPLATE_INFO_CLASSIFY_FORMAT_ERROR => array(
        'id' => TEMPLATE_INFO_CLASSIFY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_INFO_CLASSIFY_FORMAT_ERROR',
        'title'=>'信息分类格式不正确,请重新输入',
        'detail'=>'信息分类格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'infoClassify'
        ),
        'meta'=>array()
    ),
    TEMPLATE_INFO_CATEGORY_FORMAT_ERROR => array(
        'id' => TEMPLATE_INFO_CATEGORY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_INFO_CATEGORY_FORMAT_ERROR',
        'title'=>'信息类别格式不正确,请重新输入',
        'detail'=>'信息类别格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'infoCategory'
        ),
        'meta'=>array()
    ),
    TEMPLATE_DESCRIPTION_FORMAT_ERROR => array(
        'id' => TEMPLATE_DESCRIPTION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_DESCRIPTION_FORMAT_ERROR',
        'title'=>'目录描述格式不正确,不能超过2000位字符,请重新输入',
        'detail'=>'目录描述格式不正确,不能超过2000位字符,请重新输入',
        'source'=>array(
            'pointer'=>'description'
        ),
        'meta'=>array()
    ),
    TEMPLATE_ITEMS_FORMAT_ERROR => array(
        'id' => TEMPLATE_ITEMS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_ITEMS_FORMAT_ERROR',
        'title'=>'模板信息格式不正确,请重新输入',
        'detail'=>'模板信息格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'items'
        ),
        'meta'=>array()
    ),
    TEMPLATE_SOURCE_UNITS_FORMAT_ERROR => array(
        'id' => TEMPLATE_SOURCE_UNITS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_SOURCE_UNITS_FORMAT_ERROR',
        'title'=>'来源委办局格式不正确,请重新输入',
        'detail'=>'来源委办局格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'sourceUnitIds'
        ),
        'meta'=>array()
    ),
    TEMPLATE_VERSION_DESCRIPTION_FORMAT_ERROR => array(
        'id' => TEMPLATE_VERSION_DESCRIPTION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_VERSION_DESCRIPTION_FORMAT_ERROR',
        'title'=>'版本描述格式不正确,应为1-2000位字符,请重新输入',
        'detail'=>'版本描述格式不正确,应为1-2000位字符,请重新输入',
        'source'=>array(
            'pointer'=>'versionDescription'
        ),
        'meta'=>array()
    ),
    TEMPLATE_VERSION_FORMAT_ERROR => array(
        'id' => TEMPLATE_VERSION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_VERSION_FORMAT_ERROR',
        'title'=>'版本记录格式不正确,请重新输入',
        'detail'=>'版本记录格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'templateVersion'
        ),
        'meta'=>array()
    ),
    TEMPLATE_ITEMS_NAME_FORMAT_ERROR => array(
        'id' => TEMPLATE_ITEMS_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_ITEMS_NAME_FORMAT_ERROR',
        'title'=>'信息项名称格式不正确,应为1-100位字符,请重新输入',
        'detail'=>'信息项名称格式不正确,应为1-100位字符,请重新输入',
        'source'=>array(
            'pointer'=>'itemsName'
        ),
        'meta'=>array()
    ),
    TEMPLATE_ITEMS_IDENTIFY_FORMAT_ERROR => array(
        'id' => TEMPLATE_ITEMS_IDENTIFY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_ITEMS_IDENTIFY_FORMAT_ERROR',
        'title'=>'数据标识格式不正确,应为1-100位大写字母,请重新输入',
        'detail'=>'数据标识格式不正确,应为1-100位大写字母,请重新输入',
        'source'=>array(
            'pointer'=>'itemsIdentify'
        ),
        'meta'=>array()
    ),
    TEMPLATE_SUBJECT_NAME_NOT_EXIT => array(
        'id' => TEMPLATE_SUBJECT_NAME_NOT_EXIT,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_SUBJECT_NAME_NOT_EXIT',
        'title'=>'主体名称不存在,请核对后重新输入',
        'detail'=>'主体名称不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'itemsSubjectName'
        ),
        'meta'=>array()
    ),
    TEMPLATE_CARD_ID_NOT_EXIT => array(
        'id' => TEMPLATE_CARD_ID_NOT_EXIT,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_CARD_ID_NOT_EXIT',
        'title'=>'证件号码不存在,请核对后重新输入',
        'detail'=>'证件号码不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'itemsCardId'
        ),
        'meta'=>array()
    ),
    TEMPLATE_UNIFIED_SOCIAL_CREDIT_CODE_NOT_EXIT => array(
        'id' => TEMPLATE_UNIFIED_SOCIAL_CREDIT_CODE_NOT_EXIT,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_UNIFIED_SOCIAL_CREDIT_CODE_NOT_EXIT',
        'title'=>'统一社会信用代码不存在,请核对后重新输入',
        'detail'=>'统一社会信用代码不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'itemsUnifiedSocialCreditCode'
        ),
        'meta'=>array()
    ),
    TEMPLATE_ITEMS_TYPE_FORMAT_ERROR => array(
        'id' => TEMPLATE_ITEMS_TYPE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_ITEMS_TYPE_FORMAT_ERROR',
        'title'=>'信息项数据类型格式不正确,请重新输入',
        'detail'=>'信息项数据类型格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'itemsType'
        ),
        'meta'=>array()
    ),
    TEMPLATE_ITEMS_LENGTH_FORMAT_ERROR => array(
        'id' => TEMPLATE_ITEMS_LENGTH_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_ITEMS_LENGTH_FORMAT_ERROR',
        'title'=>'信息项数据长度格式不正确,请重新输入',
        'detail'=>'信息项数据长度格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'itemsLength'
        ),
        'meta'=>array()
    ),
    TEMPLATE_ITEMS_OPTIONS_FORMAT_ERROR => array(
        'id' => TEMPLATE_ITEMS_OPTIONS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_ITEMS_OPTIONS_FORMAT_ERROR',
        'title'=>'信息项可选范围格式不正确,请重新输入',
        'detail'=>'信息项可选范围格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'itemsOptions'
        ),
        'meta'=>array()
    ),
    TEMPLATE_IS_NECESSARY_FORMAT_ERROR => array(
        'id' => TEMPLATE_IS_NECESSARY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_IS_NECESSARY_FORMAT_ERROR',
        'title'=>'信息项是否必填格式不正确,请重新输入',
        'detail'=>'信息项是否必填格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'itemsIsNecessary'
        ),
        'meta'=>array()
    ),
    TEMPLATE_IS_MASKED_FORMAT_ERROR => array(
        'id' => TEMPLATE_IS_MASKED_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_IS_MASKED_FORMAT_ERROR',
        'title'=>'信息项是否脱敏格式不正确,请重新输入',
        'detail'=>'信息项是否脱敏格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'itemsIsMasked'
        ),
        'meta'=>array()
    ),
    TEMPLATE_MASKED_RULE_FORMAT_ERROR => array(
        'id' => TEMPLATE_MASKED_RULE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_MASKED_RULE_FORMAT_ERROR',
        'title'=>'信息项脱敏规则格式不正确,请重新输入',
        'detail'=>'信息项脱敏规则格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'itemsMaskedRule'
        ),
        'meta'=>array()
    ),
    TEMPLATE_REMARKS_FORMAT_ERROR => array(
        'id' => TEMPLATE_REMARKS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_REMARKS_FORMAT_ERROR',
        'title'=>'信息项备注格式不正确,不能超过2000位字符,请重新输入',
        'detail'=>'信息项备注格式不正确,不能超过2000位字符,请重新输入',
        'source'=>array(
            'pointer'=>'itemsRemarks'
        ),
        'meta'=>array()
    ),
    TEMPLATE_VERSION_NOT_EXIT => array(
        'id' => TEMPLATE_VERSION_NOT_EXIT,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_VERSION_NOT_EXIT',
        'title'=>'版本记录不存在,请核对后重新输入',
        'detail'=>'版本记录不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'templateVersion'
        ),
        'meta'=>array()
    ),
    TEMPLATE_IDENTIFY_IS_UNIQUE => array(
        'id' => TEMPLATE_IDENTIFY_IS_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_IDENTIFY_IS_UNIQUE',
        'title'=>'目录标识已经存在,请重新输入',
        'detail'=>'目录标识已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'templateIdentify'
        ),
        'meta'=>array()
    ),
    TEMPLATE_ITEMS_DIMENSION_FORMAT_ERROR => array(
        'id' => TEMPLATE_ITEMS_DIMENSION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_ITEMS_DIMENSION_FORMAT_ERROR',
        'title'=>'信息项公开范围格式不正确,请重新输入',
        'detail'=>'信息项公开范围格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'itemsDimension'
        ),
        'meta'=>array()
    ),
    TEMPLATE_ITEMS_NAME_IS_UNIQUE => array(
        'id' => TEMPLATE_ITEMS_NAME_IS_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_ITEMS_NAME_IS_UNIQUE',
        'title'=>'信息项名称在模板信息中必须唯一,请重新输入',
        'detail'=>'信息项名称在模板信息中必须唯一,请重新输入',
        'source'=>array(
            'pointer'=>'itemsName'
        ),
        'meta'=>array()
    ),
    TEMPLATE_ITEMS_IDENTIFY_IS_UNIQUE => array(
        'id' => TEMPLATE_ITEMS_IDENTIFY_IS_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_ITEMS_IDENTIFY_IS_UNIQUE',
        'title'=>'数据标识在模板信息中必须唯一,请重新输入',
        'detail'=>'数据标识在模板信息中必须唯一,请重新输入',
        'source'=>array(
            'pointer'=>'itemsIdentify'
        ),
        'meta'=>array()
    ),
    RULE_VERSION_DESCRIPTION_FORMAT_ERROR => array(
        'id' => RULE_VERSION_DESCRIPTION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_VERSION_DESCRIPTION_FORMAT_ERROR',
        'title'=>'版本描述格式不正确,应为1-2000位字符,请重新输入',
        'detail'=>'版本描述格式不正确,应为1-2000位字符,请重新输入',
        'source'=>array(
            'pointer'=>'ruleVersionDescription'
        ),
        'meta'=>array()
    ),
    RULE_RULES_FORMAT_ERROR => array(
        'id' => RULE_RULES_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_RULES_FORMAT_ERROR',
        'title'=>'规则格式不正确,请重新输入',
        'detail'=>'规则格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'rules'
        ),
        'meta'=>array()
    ),
    RULE_TEMPLATE_FORMAT_ERROR => array(
        'id' => RULE_TEMPLATE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_TEMPLATE_FORMAT_ERROR',
        'title'=>'目录格式不正确,请重新输入',
        'detail'=>'目录格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'ruleTemplate'
        ),
        'meta'=>array()
    ),
    RULE_VERSION_FORMAT_ERROR => array(
        'id' => RULE_VERSION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_VERSION_FORMAT_ERROR',
        'title'=>'规则版本记录格式不正确,请重新输入',
        'detail'=>'规则版本记录格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'ruleVersion'
        ),
        'meta'=>array()
    ),
    RULE_RULES_NAME_NOT_EXIT => array(
        'id' => RULE_RULES_NAME_NOT_EXIT,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_RULES_NAME_NOT_EXIT',
        'title'=>'规则名称不存在,请核对后重新输入',
        'detail'=>'规则名称不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'rulesName'
        ),
        'meta'=>array()
    ),
    RULE_COMPLETION_RULES_FORMAT_ERROR => array(
        'id' => RULE_COMPLETION_RULES_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPLETION_RULES_FORMAT_ERROR',
        'title'=>'补全规则格式不正确,请重新输入',
        'detail'=>'补全规则格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'completionRules'
        ),
        'meta'=>array()
    ),
    RULE_COMPLETION_RULES_COUNT_ERROR => array(
        'id' => RULE_COMPLETION_RULES_COUNT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPLETION_RULES_COUNT_ERROR',
        'title'=>'补全数量不正确,请重新输入',
        'detail'=>'补全数量不正确,请重新输入',
        'source'=>array(
            'pointer'=>'completionRulesCount'
        ),
        'meta'=>array()
    ),
    RULE_COMPLETION_RULES_TEMPLATE_FORMAT_ERROR => array(
        'id' => RULE_COMPLETION_RULES_TEMPLATE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPLETION_RULES_TEMPLATE_FORMAT_ERROR',
        'title'=>'补全信息项来源格式不正确,请重新输入',
        'detail'=>'补全信息项来源格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'completionRulesTemplate'
        ),
        'meta'=>array()
    ),
    RULE_COMPLETION_RULES_BASE_FORMAT_ERROR => array(
        'id' => RULE_COMPLETION_RULES_BASE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPLETION_RULES_BASE_FORMAT_ERROR',
        'title'=>'补全依据格式不正确,请重新输入',
        'detail'=>'补全依据格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'completionRulesBase'
        ),
        'meta'=>array()
    ),
    RULE_COMPLETION_RULES_TEMPLATE_ITEM_FORMAT_ERROR => array(
        'id' => RULE_COMPLETION_RULES_TEMPLATE_ITEM_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPLETION_RULES_TEMPLATE_ITEM_FORMAT_ERROR',
        'title'=>'补全信息项格式不正确,请重新输入',
        'detail'=>'补全信息项格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'completionRulesTemplateItem'
        ),
        'meta'=>array()
    ),
    RULE_COMPARISON_RULES_FORMAT_ERROR => array(
        'id' => RULE_COMPARISON_RULES_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPARISON_RULES_FORMAT_ERROR',
        'title'=>'比对规则格式不正确,请重新输入',
        'detail'=>'比对规则格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'comparisonRules'
        ),
        'meta'=>array()
    ),
    RULE_COMPARISON_RULES_COUNT_ERROR => array(
        'id' => RULE_COMPARISON_RULES_COUNT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPARISON_RULES_COUNT_ERROR',
        'title'=>'比对数量不正确,请重新输入',
        'detail'=>'比对数量不正确,请重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesCount'
        ),
        'meta'=>array()
    ),
    RULE_COMPARISON_RULES_TEMPLATE_FORMAT_ERROR => array(
        'id' => RULE_COMPARISON_RULES_TEMPLATE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPARISON_RULES_TEMPLATE_FORMAT_ERROR',
        'title'=>'比对信息项来源格式不正确,请重新输入',
        'detail'=>'比对信息项来源格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesTemplate'
        ),
        'meta'=>array()
    ),
    RULE_COMPARISON_RULES_BASE_FORMAT_ERROR => array(
        'id' => RULE_COMPARISON_RULES_BASE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPARISON_RULES_BASE_FORMAT_ERROR',
        'title'=>'比对依据格式不正确,请重新输入',
        'detail'=>'比对依据格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesBase'
        ),
        'meta'=>array()
    ),
    RULE_COMPARISON_RULES_TEMPLATE_ITEM_FORMAT_ERROR => array(
        'id' => RULE_COMPARISON_RULES_TEMPLATE_ITEM_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPARISON_RULES_TEMPLATE_ITEM_FORMAT_ERROR',
        'title'=>'比对信息项格式不正确,请重新输入',
        'detail'=>'比对信息项格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesTemplateItem'
        ),
        'meta'=>array()
    ),
    RULE_DE_DUPLICATION_RULES_FORMAT_ERROR => array(
        'id' => RULE_DE_DUPLICATION_RULES_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_DE_DUPLICATION_RULES_FORMAT_ERROR',
        'title'=>'去重规则格式不正确,请重新输入',
        'detail'=>'去重规则格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'deDuplicationRules'
        ),
        'meta'=>array()
    ),
    RULE_DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR => array(
        'id' => RULE_DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR',
        'title'=>'去重信息项格式不正确,请重新输入',
        'detail'=>'去重信息项格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'deDuplicationRulesItems'
        ),
        'meta'=>array()
    ),
    RULE_DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR => array(
        'id' => RULE_DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR',
        'title'=>'去重结果处理格式不正确,请重新输入',
        'detail'=>'去重结果处理格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'deDuplicationRulesResult'
        ),
        'meta'=>array()
    ),
    RULE_COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST => array(
        'id' => RULE_COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST',
        'title'=>'目录待补全信息项不存在,请核对后重新输入',
        'detail'=>'目录待补全信息项不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'completionRulesTransformationItem'
        ),
        'meta'=>array()
    ),
    RULE_COMPLETION_RULES_TEMPLATE_NOT_EXIST => array(
        'id' => RULE_COMPLETION_RULES_TEMPLATE_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPLETION_RULES_TEMPLATE_NOT_EXIST',
        'title'=>'补全信息项来源不存在,请核对后重新输入',
        'detail'=>'补全信息项来源不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'completionRulesTemplate'
        ),
        'meta'=>array()
    ),
    RULE_COMPLETION_RULES_TEMPLATE_ITEM_NOT_EXIST => array(
        'id' => RULE_COMPLETION_RULES_TEMPLATE_ITEM_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPLETION_RULES_TEMPLATE_ITEM_NOT_EXIST',
        'title'=>'补全信息项不存在,请核对后重新输入',
        'detail'=>'补全信息项不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'completionRulesTemplateItem'
        ),
        'meta'=>array()
    ),
    RULE_COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST => array(
        'id' => RULE_COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST',
        'title'=>'目录待比对信息项不存在,请核对后重新输入',
        'detail'=>'目录待比对信息项不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesTransformationItem'
        ),
        'meta'=>array()
    ),
    RULE_COMPARISON_RULES_TEMPLATE_NOT_EXIST => array(
        'id' => RULE_COMPARISON_RULES_TEMPLATE_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPARISON_RULES_TEMPLATE_NOT_EXIST',
        'title'=>'比对信息项来源不存在,请核对后重新输入',
        'detail'=>'比对信息项来源不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesTemplate'
        ),
        'meta'=>array()
    ),
    RULE_COMPARISON_RULES_TEMPLATE_ITEM_NOT_EXIST => array(
        'id' => RULE_COMPARISON_RULES_TEMPLATE_ITEM_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPARISON_RULES_TEMPLATE_ITEM_NOT_EXIST',
        'title'=>'比对信息项不存在,请核对后重新输入',
        'detail'=>'比对信息项不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesTemplateItem'
        ),
        'meta'=>array()
    ),
    RULE_COMPARISON_RULES_BASE_CANNOT_NAME => array(
        'id' => RULE_COMPARISON_RULES_BASE_CANNOT_NAME,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPARISON_RULES_BASE_CANNOT_NAME',
        'title'=>'比对项为企业名称,比对依据不能选择企业名称信息项',
        'detail'=>'比对项为企业名称,比对依据不能选择企业名称信息项',
        'source'=>array(
            'pointer'=>'comparisonRulesBaseName'
        ),
        'meta'=>array()
    ),
    RULE_COMPARISON_RULES_BASE_CANNOT_IDENTIFY => array(
        'id' => RULE_COMPARISON_RULES_BASE_CANNOT_IDENTIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_COMPARISON_RULES_BASE_CANNOT_IDENTIFY',
        'title'=>'比对项为主体代码(统一社会信用代码/身份证号),那比对依据不能选择主体代码(统一社会信用代码/身份证号)信息项',
        'detail'=>'比对项为主体代码(统一社会信用代码/身份证号),那比对依据不能选择主体代码(统一社会信用代码/身份证号)信息项',
        'source'=>array(
            'pointer'=>'comparisonRulesBaseIdentify'
        ),
        'meta'=>array()
    ),
    RULE_DE_DUPLICATION_RULES_ITEMS_NOT_EXIST => array(
        'id' => RULE_DE_DUPLICATION_RULES_ITEMS_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_DE_DUPLICATION_RULES_ITEMS_NOT_EXIST',
        'title'=>'去重信息项不存在,请核对后重新输入',
        'detail'=>'去重信息项不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'deDuplicationRulesItems'
        ),
        'meta'=>array()
    ),
    RULE_TRANSFORMATION_TEMPLATE_ITEM_TYPE_NOT_EXIST => array(
        'id' => RULE_TRANSFORMATION_TEMPLATE_ITEM_TYPE_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_TRANSFORMATION_TEMPLATE_ITEM_TYPE_NOT_EXIST',
        'title'=>'目标信息项类型不存在,请核对后重新输入',
        'detail'=>'目标信息项类型不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'transformationTemplateItem'
        ),
        'meta'=>array()
    ),
    RULE_TYPE_CANNOT_TRANSFORMATION => array(
        'id' => RULE_TYPE_CANNOT_TRANSFORMATION,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_TYPE_CANNOT_TRANSFORMATION',
        'title'=>'类型不能转换,请核对后重新输入',
        'detail'=>'类型不能转换,请核对后重新输入',
        'source'=>array(
            'pointer'=>'ruleType'
        ),
        'meta'=>array()
    ),
    RULE_LENGTH_CANNOT_TRANSFORMATION => array(
        'id' => RULE_LENGTH_CANNOT_TRANSFORMATION,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_LENGTH_CANNOT_TRANSFORMATION',
        'title'=>'长度不能转换,请核对后重新输入',
        'detail'=>'长度不能转换,请核对后重新输入',
        'source'=>array(
            'pointer'=>'ruleLength'
        ),
        'meta'=>array()
    ),
    RULE_DIMENSION_CANNOT_TRANSFORMATION => array(
        'id' => RULE_DIMENSION_CANNOT_TRANSFORMATION,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_DIMENSION_CANNOT_TRANSFORMATION',
        'title'=>'公开范围不能转换,请核对后重新输入',
        'detail'=>'公开范围不能转换,请核对后重新输入',
        'source'=>array(
            'pointer'=>'ruleDimension'
        ),
        'meta'=>array()
    ),
    RULE_IS_MASKED_CANNOT_TRANSFORMATION => array(
        'id' => RULE_IS_MASKED_CANNOT_TRANSFORMATION,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_IS_MASKED_CANNOT_TRANSFORMATION',
        'title'=>'脱敏状态不能转换,请核对后重新输入',
        'detail'=>'脱敏状态不能转换,请核对后重新输入',
        'source'=>array(
            'pointer'=>'ruleIsMasked'
        ),
        'meta'=>array()
    ),
    RULE_MASK_RULE_CANNOT_TRANSFORMATION => array(
        'id' => RULE_MASK_RULE_CANNOT_TRANSFORMATION,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_MASK_RULE_CANNOT_TRANSFORMATION',
        'title'=>'脱敏范围不能转换,请核对后重新输入',
        'detail'=>'脱敏范围不能转换,请核对后重新输入',
        'source'=>array(
            'pointer'=>'ruleMaskRule'
        ),
        'meta'=>array()
    ),
    RULE_RULES_TEMPLATE_ZRR_NOT_INCLUDE_IDENTIFY => array(
        'id' => RULE_RULES_TEMPLATE_ZRR_NOT_INCLUDE_IDENTIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_RULES_TEMPLATE_ZRR_NOT_INCLUDE_IDENTIFY',
        'title'=>'当比对/补全信息项来源为自然人,依据必须包含身份证号',
        'detail'=>'当比对/补全信息项来源为自然人,依据必须包含身份证号',
        'source'=>array(
            'pointer'=>'ruleZrrBaseIdentify'
        ),
        'meta'=>array()
    ),
    RULE_TEMPLATE_NOT_EXIST => array(
        'id' => RULE_TEMPLATE_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_TEMPLATE_NOT_EXIST',
        'title'=>'目录不存在,请核对后重新输入',
        'detail'=>'目录不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'ruleTemplate'
        ),
        'meta'=>array()
    ),
    RULE_TEMPLATE_VERSION_NOT_EXIST => array(
        'id' => RULE_TEMPLATE_VERSION_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_TEMPLATE_VERSION_NOT_EXIST',
        'title'=>'目录版本记录不存在,请核对后重新输入',
        'detail'=>'目录版本记录不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'ruleTemplateVersion'
        ),
        'meta'=>array()
    ),
    RULE_VERSION_NOT_EXIST => array(
        'id' => RULE_VERSION_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_VERSION_NOT_EXIST',
        'title'=>'规则版本记录不存在,请核对后重新输入',
        'detail'=>'规则版本记录不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'ruleVersion'
        ),
        'meta'=>array()
    ),
    RULE_IS_UNIQUE => array(
        'id' => RULE_IS_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'RULE_IS_UNIQUE',
        'title'=>'规则已经存在,请重新输入',
        'detail'=>'规则已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'rule'
        ),
        'meta'=>array()
    ),
    RESOURCE_CATALOG_DATA_EXPIRATION_DATE_FORMAT_ERROR => array(
        'id' => RESOURCE_CATALOG_DATA_EXPIRATION_DATE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RESOURCE_CATALOG_DATA_EXPIRATION_DATE_FORMAT_ERROR',
        'title'=>'有效期限格式不正确,请重新输入',
        'detail'=>'有效期限格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'expirationDate'
        ),
        'meta'=>array()
    ),
    RESOURCE_CATALOG_DATA_TEMPLATE_NOT_EXIT => array(
        'id' => RESOURCE_CATALOG_DATA_TEMPLATE_NOT_EXIT,
        'link'=>'',
        'status'=>403,
        'code'=>'RESOURCE_CATALOG_DATA_TEMPLATE_NOT_EXIT',
        'title'=>'目录不存在,请核对后重新输入',
        'detail'=>'目录不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'template'
        ),
        'meta'=>array()
    ),
    RESOURCE_CATALOG_DATA_TEMPLATE_FORMAT_ERROR => array(
        'id' => RESOURCE_CATALOG_DATA_TEMPLATE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RESOURCE_CATALOG_DATA_TEMPLATE_FORMAT_ERROR',
        'title'=>'目录格式不正确,请重新输入',
        'detail'=>'目录格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'template'
        ),
        'meta'=>array()
    ),
    RESOURCE_CATALOG_DATA_ITEMS_DATA_FORMAT_ERROR => array(
        'id' => RESOURCE_CATALOG_DATA_ITEMS_DATA_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RESOURCE_CATALOG_DATA_ITEMS_DATA_FORMAT_ERROR',
        'title'=>'资源目录数据格式不正确,请重新输入',
        'detail'=>'资源目录数据格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'itemsData'
        ),
        'meta'=>array()
    ),
    RESOURCE_CATALOG_DATA_IS_UNIQUE => array(
        'id' => RESOURCE_CATALOG_DATA_IS_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'RESOURCE_CATALOG_DATA_IS_UNIQUE',
        'title'=>'资源目录数据已经存在,请重新输入',
        'detail'=>'资源目录数据已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'resourceCatalogData'
        ),
        'meta'=>array()
    ),
);
