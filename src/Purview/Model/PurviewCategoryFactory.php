<?php
namespace Sdk\Purview\Model;

class PurviewCategoryFactory
{
    const MAPS = array(
        'userGroups/departments'=>IPurviewAble::CATEGORY['USER_GROUP'],
        'userGroups'=>IPurviewAble::CATEGORY['USER_GROUP'],
        'users/crews'=>IPurviewAble::CATEGORY['CREW'],
        'users/members'=>IPurviewAble::CATEGORY['MEMBER'],
        'news/categories'=>IPurviewAble::CATEGORY['NEWS_CATEGORY'],
        'news'=>IPurviewAble::CATEGORY['NEWS'],
        'news/unAuditedNews'=>IPurviewAble::CATEGORY['UN_AUDITED_NEWS'],
        'resourceCatalogs/templates'=>IPurviewAble::CATEGORY['TEMPLATE'],
        'resourceCatalogs/templateVersions'=>IPurviewAble::CATEGORY['TEMPLATE'],
        'resourceCatalogs/unAuditedTemplates'=>IPurviewAble::CATEGORY['UN_AUDITED_TEMPLATE'],
        'resourceCatalogs/ruless'=>IPurviewAble::CATEGORY['RULE'],
        'resourceCatalogs/rulesVersions'=>IPurviewAble::CATEGORY['RULE'],
        'resourceCatalogs/unAuditedRules'=>IPurviewAble::CATEGORY['UN_AUDITED_RULE'],
    );

    public function getCategory(string $resource) : int
    {
        return isset(self::MAPS[$resource]) ? self::MAPS[$resource] : 0;
    }
}
