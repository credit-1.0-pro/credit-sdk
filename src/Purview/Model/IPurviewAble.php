<?php
namespace Sdk\Purview\Model;

interface IPurviewAble
{
    const CATEGORY = array(
        'USER_GROUP' => 1,// 委办局
        'CREW' => 2,// 员工
        'MEMBER' => 3,// 前台用户管理
        'NEWS_CATEGORY' => 4,// 新闻分类管理
        'NEWS' => 5,// 新闻管理
        'UN_AUDITED_NEWS' => 6,// 新闻审核
        'TEMPLATE' => 7,// 目录管理
        'UN_AUDITED_TEMPLATE' => 8,// 目录审核
        'RULE' => 9,// 规则管理
        'UN_AUDITED_RULE' => 10,// 规则审核
    );
}
