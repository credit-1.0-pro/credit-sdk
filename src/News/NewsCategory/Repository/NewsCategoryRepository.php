<?php
namespace Sdk\News\NewsCategory\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperateAbleRepositoryTrait;

use Sdk\News\NewsCategory\Adapter\NewsCategory\INewsCategoryAdapter;
use Sdk\News\NewsCategory\Adapter\NewsCategory\NewsCategoryMockAdapter;
use Sdk\News\NewsCategory\Adapter\NewsCategory\NewsCategoryRestfulAdapter;

class NewsCategoryRepository extends Repository implements INewsCategoryAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperateAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'NEWS_CATEGORY_LIST';
    const FETCH_ONE_MODEL_UN = 'NEWS_CATEGORY_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new NewsCategoryRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new NewsCategoryMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
