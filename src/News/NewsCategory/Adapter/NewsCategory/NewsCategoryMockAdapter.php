<?php
namespace Sdk\News\NewsCategory\Adapter\NewsCategory;

use Sdk\Common\Adapter\OperateAbleMockAdapterTrait;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory;

class NewsCategoryMockAdapter implements INewsCategoryAdapter
{
    use OperateAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockNewsCategoryFactory::generateNewsCategory($id);
    }

    public function fetchList(array $ids) : array
    {
        $newsCategoryList = array();

        foreach ($ids as $id) {
            $newsCategoryList[] = MockNewsCategoryFactory::generateNewsCategory($id);
        }

        return $newsCategoryList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockNewsCategoryFactory::generateNewsCategory($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $newsCategoryList = array();

        foreach ($ids as $id) {
            $newsCategoryList[] = MockNewsCategoryFactory::generateNewsCategory($id);
        }

        return $newsCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
