<?php
namespace Sdk\News\NewsCategory\Adapter\NewsCategory;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

interface INewsCategoryAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperateAbleAdapter
{

}
