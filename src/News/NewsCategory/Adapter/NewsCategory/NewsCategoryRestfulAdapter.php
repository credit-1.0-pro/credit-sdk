<?php
namespace Sdk\News\NewsCategory\Adapter\NewsCategory;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Model\NullNewsCategory;
use Sdk\News\NewsCategory\Translator\NewsCategoryRestfulTranslator;

class NewsCategoryRestfulAdapter extends GuzzleAdapter implements INewsCategoryAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'NEWS_CATEGORY_LIST'=>[
            'fields' => []
        ],
        'NEWS_CATEGORY_FETCH_ONE'=>[
            'fields' => []
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new NewsCategoryRestfulTranslator();
        $this->resource = 'news/categories';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullNewsCategory::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            100 => [
                'parentCategory' => NEWS_CATEGORY_PARENT_CATEGORY_NOT_EXIST,
                'category' => NEWS_CATEGORY_CATEGORY_NOT_EXIST
            ],
            101 => [
                'name' => NEWS_CATEGORY_NAME_FORMAT_ERROR,
                'grade' => NEWS_CATEGORY_GRADE_FORMAT_ERROR,
                'parentCategory' => NEWS_CATEGORY_PARENT_CATEGORY_FORMAT_ERROR,
                'category' => NEWS_CATEGORY_CATEGORY_FORMAT_ERROR
            ],
            103 => [
                'newsCategoryName' => NEWS_CATEGORY_NAME_IS_UNIQUE
            ],
            104 => [
                'categoryNotBelongToParentCategory' => CATEGORY_NOT_BELONG_TO_THE_PARENT_CATEGORY
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function addAction(NewsCategory $newsCategory) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $newsCategory,
            array('name', 'grade', 'parentCategory', 'category')
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($newsCategory);
            return true;
        }

        return false;
    }

    protected function editAction(NewsCategory $newsCategory) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $newsCategory,
            array('name')
        );

        $this->patch(
            $this->getResource().'/'.$newsCategory->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($newsCategory);
            return true;
        }

        return false;
    }
}
