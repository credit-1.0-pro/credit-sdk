<?php
namespace Sdk\News\NewsCategory\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Model\NullNewsCategory;

class NewsCategoryRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $newsCategory = null)
    {
        return $this->translateToObject($expression, $newsCategory);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $newsCategory = null)
    {
        if (empty($expression)) {
            return NullNewsCategory::getInstance();
        }

        if ($newsCategory == null) {
            $newsCategory = new NewsCategory();
        }
        
        $data = $expression['data'];

        $id = $data['id'];
        $newsCategory->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $newsCategory->setName($attributes['name']);
        }
        if (isset($attributes['grade'])) {
            $newsCategory->setGrade($attributes['grade']);
        }
        if (isset($attributes['parentCategory'])) {
            $newsCategory->setParentCategory($attributes['parentCategory']);
        }
        if (isset($attributes['category'])) {
            $newsCategory->setCategory($attributes['category']);
        }
        if (isset($attributes['updateTime'])) {
            $newsCategory->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['createTime'])) {
            $newsCategory->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['status'])) {
            $newsCategory->setStatus($attributes['status']);
        }

        return $newsCategory;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($newsCategory, array $keys = array())
    {
        if (!$newsCategory instanceof NewsCategory) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'grade',
                'parentCategory',
                'category',
                'updateTime',
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'newsCategories'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $newsCategory->getId();
        }
        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $newsCategory->getName();
        }
        if (in_array('grade', $keys)) {
            $attributes['grade'] = $newsCategory->getGrade();
        }
        if (in_array('parentCategory', $keys)) {
            $attributes['parentCategory'] = $newsCategory->getParentCategory();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $newsCategory->getCategory();
        }
        if (in_array('updateTime', $keys)) {
            $attributes['updateTime'] = $newsCategory->getUpdateTime();
        }

        $expression['data']['attributes'] = $attributes;
        
        return $expression;
    }
}
