<?php
namespace Sdk\News\NewsCategory\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Model\NullNewsCategory;

class NewsCategoryTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $newsCategory = null)
    {
        unset($newsCategory);
        unset($expression);
        return NullNewsCategory::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($newsCategory, array $keys = array())
    {
        if (!$newsCategory instanceof NewsCategory) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'category',
                'parentCategory',
                'grade',
                'updateTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($newsCategory->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $newsCategory->getName();
        }
        if (in_array('category', $keys)) {
            $expression['category'] = marmot_encode($newsCategory->getCategory());
        }
        if (in_array('parentCategory', $keys)) {
            $expression['parentCategory'] = marmot_encode($newsCategory->getParentCategory());
        }
        if (in_array('grade', $keys)) {
            $expression['grade'] = [
                'id' => marmot_encode($newsCategory->getGrade()),
                'name' => NewsCategory::GRADE_CN[$newsCategory->getGrade()]
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $newsCategory->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H点i分', $newsCategory->getUpdateTime());
        }
        
        return $expression;
    }
}
