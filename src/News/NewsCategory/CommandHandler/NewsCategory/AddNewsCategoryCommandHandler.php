<?php
namespace Sdk\News\NewsCategory\CommandHandler\NewsCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Command\NewsCategory\AddNewsCategoryCommand;

class AddNewsCategoryCommandHandler implements ICommandHandler
{
    private $newsCategory;

    public function __construct()
    {
        $this->newsCategory = new NewsCategory();
    }

    public function __destruct()
    {
        unset($this->newsCategory);
    }

    protected function getNewsCategory() : NewsCategory
    {
        return $this->newsCategory;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddNewsCategoryCommand)) {
            throw new \InvalidArgumentException;
        }

        $newsCategory = $this->getNewsCategory();
        $newsCategory->setName($command->name);
        $newsCategory->setGrade($command->grade);
        $newsCategory->setParentCategory($command->parentCategory);
        $newsCategory->setCategory($command->category);

        return $newsCategory->add();
    }
}
