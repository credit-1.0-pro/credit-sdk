<?php
namespace Sdk\News\NewsCategory\CommandHandler\NewsCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class NewsCategoryCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\News\NewsCategory\Command\NewsCategory\AddNewsCategoryCommand' =>
        'Sdk\News\NewsCategory\CommandHandler\NewsCategory\AddNewsCategoryCommandHandler',
        'Sdk\News\NewsCategory\Command\NewsCategory\EditNewsCategoryCommand' =>
        'Sdk\News\NewsCategory\CommandHandler\NewsCategory\EditNewsCategoryCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
