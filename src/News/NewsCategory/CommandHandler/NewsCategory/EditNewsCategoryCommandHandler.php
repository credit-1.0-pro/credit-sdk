<?php
namespace Sdk\News\NewsCategory\CommandHandler\NewsCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Repository\NewsCategoryRepository;
use Sdk\News\NewsCategory\Command\NewsCategory\EditNewsCategoryCommand;

class EditNewsCategoryCommandHandler implements ICommandHandler
{
    private $repository;

    public function __construct()
    {
        $this->repository = new NewsCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : NewsCategoryRepository
    {
        return $this->repository;
    }

    protected function fetchNewsCategory(int $id) : NewsCategory
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditNewsCategoryCommand)) {
            throw new \InvalidArgumentException;
        }

        $newsCategory = $this->fetchNewsCategory($command->id);
        $newsCategory->setName($command->name);

        return $newsCategory->edit();
    }
}
