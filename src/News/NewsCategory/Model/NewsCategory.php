<?php
namespace Sdk\News\NewsCategory\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\IOperateAble;
use Sdk\Common\Model\OperateAbleTrait;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\News\NewsCategory\Repository\NewsCategoryRepository;

class NewsCategory implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;
    
    const STATUS_NORMAL = 0; //正常
    
    const GRADE = array(
        'GRADE_ONE' => 1,
        'GRADE_TWO' => 2,
        'GRADE_THREE' => 3
    );

    const GRADE_ENCODE = array(
        'GRADE_ONE' => 'MA',
        'GRADE_TWO' => 'MQ',
        'GRADE_THREE' => 'Mg'
    );

    const GRADE_CN = array(
        self::GRADE['GRADE_ONE'] => '一级',
        self::GRADE['GRADE_TWO'] => '二级',
        self::GRADE['GRADE_THREE'] => '三级'
    );
    /**
     * @var int $id
     */
    protected $id;
    /**
     * @var string $name 分类名称
     */
    protected $name;
    /**
     * @var int $grade 分类等级
     */
    protected $grade;
    /**
     * @var int $parentCategory 一级分类
     */
    protected $parentCategory;
    /**
     * @var int $category 二级分类
     */
    protected $category;

    protected $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->grade = self::GRADE['GRADE_ONE'];
        $this->parentCategory = 0;
        $this->category = 0;
        $this->status = self::STATUS_NORMAL;
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->repository = new NewsCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->grade);
        unset($this->parentCategory);
        unset($this->category);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setParentCategory(int $parentCategory) : void
    {
        $this->parentCategory = $parentCategory;
    }

    public function getParentCategory() : int
    {
        return $this->parentCategory;
    }

    public function setCategory(int $category) : void
    {
        $this->category = $category;
    }

    public function getCategory() : int
    {
        return $this->category;
    }
    
    public function setGrade(int $grade) : void
    {
        $this->grade = in_array($grade, self::GRADE) ? $grade : self::GRADE['GRADE_ONE'];
    }

    public function getGrade() : int
    {
        return $this->grade;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->repository;
    }

    protected function fetchOne($id) : NewsCategory
    {
        return $this->getIOperateAbleAdapter()->fetchOne($id);
    }

    protected function fetchParentCategory() : NewsCategory
    {
        $parentCategoryId = $this->getParentCategory();

        return !empty($parentCategoryId) ? $this->fetchOne($parentCategoryId) : NullNewsCategory::getInstance();
    }

    protected function fetchCategory() : NewsCategory
    {
        $categoryId = $this->getCategory();

        return !empty($categoryId) ? $this->fetchOne($categoryId) : NullNewsCategory::getInstance();
    }

    protected function validate() : bool
    {
        $parentCategory = $this->fetchParentCategory();
        $category = $this->fetchCategory();

        return $this->isParentCategoryExist($parentCategory) &&
               $this->isCategoryExist($category) &&
               $this->isBelongToParentCategory($parentCategory, $category) &&
               $this->nameIsExist();
    }

    //验证一级分类不为空
    protected function isParentCategoryExist(NewsCategory $parentCategory) : bool
    {
        if (!empty($this->getParentCategory()) && $parentCategory instanceof INull) {
            Core::setLastError(NEWS_CATEGORY_PARENT_CATEGORY_NOT_EXIST);
            return false;
        }

        return true;
    }

    //验证二级分类不为空
    protected function isCategoryExist(NewsCategory $category) : bool
    {
        if (!empty($this->getCategory()) && $category instanceof INull) {
            Core::setLastError(NEWS_CATEGORY_CATEGORY_NOT_EXIST);
            return false;
        }

        return true;
    }

    //验证二级分类是否属于一级分类
    protected function isBelongToParentCategory(NewsCategory $parentCategory, NewsCategory $category) : bool
    {
        if (!empty($this->getParentCategory()) &&
            !empty($this->getCategory()) &&
            $parentCategory->getId() != $category->getParentCategory()
        ) {
            Core::setLastError(CATEGORY_NOT_BELONG_TO_THE_PARENT_CATEGORY);
            return false;
        }

        return true;
    }

    protected function nameIsExist(): bool
    {
        $sort = ['-updateTime'];

        $filter['unique'] = $this->getName();
        $filter['parentCategory'] = $this->getParentCategory();
        $filter['category'] = $this->getCategory();
        
        if (!empty($this->getId())) {
            $filter['id'] = $this->getId();
        }

        list($count, $newsCategoryList) = $this->getIOperateAbleAdapter()->scenario(
            NewsCategoryRepository::LIST_MODEL_UN
        )->search($filter, $sort);

        unset($newsCategoryList);

        if (!empty($count)) {
            Core::setLastError(NEWS_CATEGORY_NAME_IS_UNIQUE);
            return false;
        }

        return true;
    }

    protected function addAction(): bool
    {
        return $this->validate() && $this->getIOperateAbleAdapter()->add($this);
    }

    protected function editAction(): bool
    {
        return $this->nameIsExist() && $this->getIOperateAbleAdapter()->edit($this);
    }
}
