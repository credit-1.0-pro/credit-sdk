<?php
namespace Sdk\News\NewsCategory\Command\NewsCategory;

use Marmot\Interfaces\ICommand;

class AddNewsCategoryCommand implements ICommand
{
    /**
     * @var string $name 分类名称
     */
    public $name;
    /**
     * @var string $grade 分类等级
     */
    public $grade;
    /**
     * @var string $parentCategory 一级分类
     */
    public $parentCategory;
    /**
     * @var int $category 二级分类
     */
    public $category;

    public $id;

    public function __construct(
        string $name,
        int $grade,
        int $parentCategory,
        int $category,
        int $id = 0
    ) {
        $this->name = $name;
        $this->grade = $grade;
        $this->parentCategory = $parentCategory;
        $this->category = $category;
        $this->id = $id;
    }
}
