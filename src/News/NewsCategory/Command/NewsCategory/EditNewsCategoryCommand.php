<?php
namespace Sdk\News\NewsCategory\Command\NewsCategory;

use Marmot\Interfaces\ICommand;

class EditNewsCategoryCommand implements ICommand
{
    /**
     * @var string $name 分类名称
     */
    public $name;

    public $id;

    public function __construct(
        string $name,
        int $id
    ) {
        $this->name = $name;
        $this->id = $id;
    }
}
