<?php
namespace Sdk\News\NewsCategory\WidgetRules;

use Marmot\Core;
use Respect\Validation\Validator as V;

use Sdk\News\NewsCategory\Model\NewsCategory;

class NewsCategoryWidgetRules
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    const NAME_MIN_LENGTH = 2;
    const NAME_MAX_LENGTH = 20;

    public function newsCategoryName($newsCategoryName) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::NAME_MIN_LENGTH,
            self::NAME_MAX_LENGTH
        )->validate($newsCategoryName)) {
            Core::setLastError(NEWS_CATEGORY_NAME_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    public function grade($grade) : bool
    {
        if (!V::numeric()->validate($grade) || !in_array($grade, NewsCategory::GRADE)) {
            Core::setLastError(NEWS_CATEGORY_GRADE_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    public function parentCategory($grade, $parentCategory) : bool
    {
        if (!V::numeric()->validate($parentCategory)) {
            Core::setLastError(NEWS_CATEGORY_PARENT_CATEGORY_FORMAT_ERROR);
            return false;
        }
        
        if ($grade == NewsCategory::GRADE['GRADE_ONE'] && !empty($parentCategory)) {
            Core::setLastError(NEWS_CATEGORY_PARENT_CATEGORY_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    public function category($grade, $category) : bool
    {
        if (!V::numeric()->validate($category)) {
            Core::setLastError(NEWS_CATEGORY_CATEGORY_FORMAT_ERROR);
            return false;
        }
        
        if ($grade != NewsCategory::GRADE['GRADE_THREE'] && !empty($category)) {
            Core::setLastError(NEWS_CATEGORY_CATEGORY_FORMAT_ERROR);
            return false;
        }

        return true;
    }
}
