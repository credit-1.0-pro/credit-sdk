<?php
namespace Sdk\News\News\Adapter\News;

use Sdk\Common\Adapter\TopAbleMockAdapterTrait;
use Sdk\Common\Adapter\EnableAbleMockAdapterTrait;
use Sdk\Common\Adapter\OperateAbleMockAdapterTrait;

use Sdk\News\News\Model\News;
use Sdk\News\News\Utils\MockFactory;

class NewsMockAdapter implements INewsAdapter
{
    use OperateAbleMockAdapterTrait, EnableAbleMockAdapterTrait, TopAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateNews($id);
    }

    public function fetchList(array $ids) : array
    {
        $newsList = array();

        foreach ($ids as $id) {
            $newsList[] = MockFactory::generateNews($id);
        }

        return $newsList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateNews($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $newsList = array();

        foreach ($ids as $id) {
            $newsList[] = MockFactory::generateNews($id);
        }

        return $newsList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function move(News $news) : bool
    {
        unset($news);

        return true;
    }
}
