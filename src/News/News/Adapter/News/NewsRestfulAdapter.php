<?php
namespace Sdk\News\News\Adapter\News;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\TopAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\ITopAble;

use Sdk\News\News\Model\News;
use Sdk\News\News\Model\NullNews;
use Sdk\News\News\Translator\NewsRestfulTranslator;

class NewsRestfulAdapter extends GuzzleAdapter implements INewsAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        TopAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;
    
    const SCENARIOS = [
        'NEWS_LIST'=>[
            'fields' => [],
            'include' => 'crews,publishUserGroup,parentCategory,category,newsType'
        ],
        'NEWS_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crews,publishUserGroup,parentCategory,category,newsType'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new NewsRestfulTranslator();
        $this->resource = 'news';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullNews::getInstance();
    }

    protected function getMapErrors() : array
    {
        $mapError = array(
            101 => array(
                'title' => TITLE_FORMAT_ERROR,
                'source' => SOURCE_FORMAT_ERROR,
                'cover' => IMAGE_FORMAT_ERROR,
                'attachments' => ATTACHMENT_FORMAT_ERROR,
                'content' => NEWS_CONTENT_FORMAT_ERROR,
                'status' => STATUS_FORMAT_ERROR,
                'stick' => STICK_FORMAT_ERROR,
                'newsType' => NEWS_TYPE_FORMAT_ERROR,
                'dimension' => DIMENSION_FORMAT_ERROR,
                'homePageShowStatus' => HOME_PAGE_SHOW_STATUS_FORMAT_ERROR,
                'bannerStatus' => BANNER_STATUS_FORMAT_ERROR,
                'bannerImage' => BANNER_IMAGE_FORMAT_ERROR,
                'crewId' => CREW_ID_FORMAT_ERROR,
                'applyCrewId' => APPLY_CREW_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
                'releaseTime' => RELEASE_TIME_FORMAT_ERROR
            ),
            100 => array(
                'crew' => PARAMETER_IS_EMPTY,
                'publishCrew' => PARAMETER_IS_EMPTY,
                'newsType' => NEWS_TYPE_NOT_EXIT,
            ),
            102 => array(
                'status' => STATUS_CAN_NOT_MODIFY,
                'stick' => STATUS_CAN_NOT_MODIFY,
                'applyStatus' => APPLY_STATUE_CAN_NOT_MODIFY,
            )
        );

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function addAction(News $news) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $news,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'releaseTime',
                'crew'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($news);
            return true;
        }

        return false;
    }

    protected function editAction(News $news) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $news,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'releaseTime',
                'crew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$news->getId(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($news);
            return true;
        }

        return false;
    }

    protected function enableAction(IEnableAble $enableAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray($enableAbleObject, array('crew'));

        $this->patch(
            $this->getResource().'/'.$enableAbleObject->getId().'/enable',
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($enableAbleObject);
            return true;
        }
        return false;
    }

    protected function disableAction(IEnableAble $enableAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray($enableAbleObject, array('crew'));

        $this->patch(
            $this->getResource().'/'.$enableAbleObject->getId().'/disable',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($enableAbleObject);
            return true;
        }
        return false;
    }

    protected function topAction(ITopAble $topAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray($topAbleObject, array('crew'));
        
        $this->patch(
            $this->getResource().'/'.$topAbleObject->getId().'/top',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($topAbleObject);
            return true;
        }
        return false;
    }

    protected function cancelTopAction(ITopAble $topAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray($topAbleObject, array('crew'));

        $this->patch(
            $this->getResource().'/'.$topAbleObject->getId().'/cancelTop',
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($topAbleObject);
            return true;
        }
        return false;
    }

    public function move(News $news) : bool
    {
        $data = $this->getTranslator()->objectToArray($news, array('crew'));
    
        $this->patch(
            $this->getResource().'/'.$news->getId().'/'.'move/'.$news->getNewsType()->getId(),
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($news);
            return true;
        }

        return false;
    }
}
