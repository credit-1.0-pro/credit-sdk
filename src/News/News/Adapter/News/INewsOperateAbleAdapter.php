<?php
namespace Sdk\News\News\Adapter\News;

use Sdk\Common\Adapter\IOperateAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;

interface INewsOperateAbleAdapter extends IOperateAbleAdapter, IEnableAbleAdapter, ITopAbleAdapter
{
    
}
