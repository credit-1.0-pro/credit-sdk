<?php
namespace Sdk\News\News\Adapter\News;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;

interface INewsFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
