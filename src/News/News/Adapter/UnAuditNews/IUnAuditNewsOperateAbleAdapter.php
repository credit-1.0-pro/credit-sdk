<?php
namespace Sdk\News\News\Adapter\UnAuditNews;

use Sdk\Common\Adapter\IOperateAbleAdapter;
use Sdk\Common\Adapter\IApplyAbleAdapter;

interface IUnAuditNewsOperateAbleAdapter extends IOperateAbleAdapter, IApplyAbleAdapter
{
    
}
