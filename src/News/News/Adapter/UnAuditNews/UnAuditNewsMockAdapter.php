<?php
namespace Sdk\News\News\Adapter\UnAuditNews;

use Sdk\Common\Adapter\ApplyAbleMockAdapterTrait;
use Sdk\Common\Adapter\OperateAbleMockAdapterTrait;

use Sdk\News\News\Model\UnAuditNews;
use Sdk\News\News\Utils\MockFactory;

class UnAuditNewsMockAdapter implements IUnAuditNewsAdapter
{
    use ApplyAbleMockAdapterTrait, OperateAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateUnAuditNews($id);
    }

    public function fetchList(array $ids) : array
    {
        $newsList = array();

        foreach ($ids as $id) {
            $newsList[] = MockFactory::generateUnAuditNews($id);
        }

        return $newsList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateUnAuditNews($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $newsList = array();

        foreach ($ids as $id) {
            $newsList[] = MockFactory::generateUnAuditNews($id);
        }

        return $newsList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
