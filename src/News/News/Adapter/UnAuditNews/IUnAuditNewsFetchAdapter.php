<?php
namespace Sdk\News\News\Adapter\UnAuditNews;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;

interface IUnAuditNewsFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
