<?php
namespace Sdk\News\News\Adapter\UnAuditNews;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Sdk\Common\Model\IApplyAble;

use Sdk\News\News\Model\UnAuditNews;
use Sdk\News\News\Model\NullUnAuditNews;

use Sdk\News\News\Translator\UnAuditNewsRestfulTranslator;

class UnAuditNewsRestfulAdapter extends GuzzleAdapter implements IUnAuditNewsAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'UN_AUDIT_NEWS_LIST'=>[
            'fields' => [],
            'include' => 'crew,publishUserGroup,applyUserGroup,applyCrew,publishCrew,parentCategory,category,newsType'
        ],
        'UN_AUDIT_NEWS_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,publishUserGroup,applyUserGroup,applyCrew,publishCrew,parentCategory,category,newsType'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UnAuditNewsRestfulTranslator();
        $this->resource = 'news/unAuditedNews';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullUnAuditNews::getInstance();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            101 => [
                'title' => TITLE_FORMAT_ERROR,
                'source' => SOURCE_FORMAT_ERROR,
                'cover' => IMAGE_FORMAT_ERROR,
                'attachments' => ATTACHMENT_FORMAT_ERROR,
                'content' => NEWS_CONTENT_FORMAT_ERROR,
                'status' => STATUS_FORMAT_ERROR,
                'stick' => STICK_FORMAT_ERROR,
                'newsType' => NEWS_TYPE_FORMAT_ERROR,
                'dimension' => DIMENSION_FORMAT_ERROR,
                'homePageShowStatus' => HOME_PAGE_SHOW_STATUS_FORMAT_ERROR,
                'bannerStatus' => BANNER_STATUS_FORMAT_ERROR,
                'bannerImage' => BANNER_IMAGE_FORMAT_ERROR,
                'crewId' => CREW_ID_FORMAT_ERROR,
                'applyCrewId' => APPLY_CREW_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
                'releaseTime' => RELEASE_TIME_FORMAT_ERROR
            ],
            100 => [
                'crew' => PARAMETER_IS_EMPTY,
                'newsType' => NEWS_TYPE_NOT_EXIT,
                'publishCrew' => PARAMETER_IS_EMPTY,
            ],
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY,
                'stick' => STATUS_CAN_NOT_MODIFY,
                'applyStatus' => APPLY_STATUE_CAN_NOT_MODIFY,
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function addAction(UnAuditNews $unAuditNews) : bool
    {
        unset($unAuditNews);
        return false;
    }

    protected function editAction(UnAuditNews $unAuditNews) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditNews,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'releaseTime',
                'crew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$unAuditNews->getId().'/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditNews);
            return true;
        }

        return false;
    }

    protected function approveAction(IApplyAble $applyAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray($applyAbleObject, array('applyCrew'));
       
        $this->patch(
            $this->getResource().'/'.$applyAbleObject->getId().'/approve',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($applyAbleObject);
            return true;
        }
        return false;
    }

    protected function rejectAction(IApplyAble $applyAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $applyAbleObject,
            array(
                'rejectReason',
                'applyCrew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$applyAbleObject->getId().'/reject',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($applyAbleObject);
            return true;
        }
        return false;
    }
}
