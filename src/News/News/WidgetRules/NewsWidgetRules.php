<?php
namespace Sdk\News\News\WidgetRules;

use Marmot\Core;
use Respect\Validation\Validator as V;

use Sdk\News\News\Model\News;
use Sdk\Common\WidgetRules\WidgetRules;

class NewsWidgetRules
{
    private static $instance;

    protected function getCommonWidgetRule() : WidgetRules
    {
        return new WidgetRules();
    }

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function dimension($dimension) : bool
    {
        if (!V::numeric()->positive()->validate($dimension) || !in_array($dimension, News::DIMENSION)) {
            Core::setLastError(DIMENSION_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    public function homePageShowStatus($homePageShowStatus) : bool
    {
        if (!V::numeric()->validate($homePageShowStatus)
        || !in_array($homePageShowStatus, News::HOME_PAGE_SHOW_STATUS)) {
            Core::setLastError(HOME_PAGE_SHOW_STATUS_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    public function bannerStatus($bannerStatus) : bool
    {
        if (!V::numeric()->validate($bannerStatus) || !in_array($bannerStatus, News::BANNER_STATUS)) {
            Core::setLastError(BANNER_STATUS_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    public function bannerImage($bannerStatus, $bannerImage) : bool
    {
        if ($bannerStatus != News::BANNER_STATUS['ENABLED'] && !empty($bannerImage)) {
            Core::setLastError(BANNER_IMAGE_FORMAT_ERROR);
            return false;
        }

        if ($bannerStatus == News::BANNER_STATUS['ENABLED'] && !$this->getCommonWidgetRule()->image($bannerImage)) {
            Core::setLastError(BANNER_IMAGE_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    public function releaseTime($releaseTime) : bool
    {
        if (!V::numeric()->validate($releaseTime)) {
            Core::setLastError(RELEASE_TIME_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    public function content($content) : bool
    {
        if (empty($content) || !$this->getCommonWidgetRule()->formatString($content, 'content')) {
            Core::setLastError(NEWS_CONTENT_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }
}
