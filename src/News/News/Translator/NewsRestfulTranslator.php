<?php
namespace Sdk\News\News\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\News\News\Model\News;
use Sdk\News\News\Model\NullNews;
use Sdk\Common\Translator\RestfulTranslatorTrait;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\News\NewsCategory\Translator\NewsCategoryRestfulTranslator;

class NewsRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $news = null)
    {
        return $this->translateToObject($expression, $news);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    protected function getNewsCategoryRestfulTranslator(): NewsCategoryRestfulTranslator
    {
        return new NewsCategoryRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $news = null)
    {
        if (empty($expression)) {
            return new NullNews();
        }

        if ($news == null) {
            $news = new News();
        }
       
        $data = $expression['data'];
        
        $id = $data['id'];
        $news->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $news->setTitle($attributes['title']);
        }

        if (isset($attributes['description'])) {
            $news->setDescription($attributes['description']);
        }

        if (isset($attributes['attachments'])) {
            $news->setAttachments($attributes['attachments']);
        }

        if (isset($attributes['content'])) {
            $news->setContent($attributes['content']);
        }

        if (isset($attributes['source'])) {
            $news->setSource($attributes['source']);
        }

        if (isset($attributes['cover'])) {
            $news->setCover($attributes['cover']);
        }

        if (isset($attributes['releaseTime'])) {
            $news->setReleaseTime($attributes['releaseTime']);
        }

        if (isset($attributes['dimension'])) {
            $news->setDimension($attributes['dimension']);
        }

        if (isset($attributes['bannerImage'])) {
            $news->setBannerImage($attributes['bannerImage']);
        }

        if (isset($attributes['bannerStatus'])) {
            $news->setBannerStatus($attributes['bannerStatus']);
        }

        if (isset($attributes['homePageShowStatus'])) {
            $news->setHomePageShowStatus($attributes['homePageShowStatus']);
        }

        if (isset($attributes['stick'])) {
            $news->setStick($attributes['stick']);
        }

        if (isset($attributes['createTime'])) {
            $news->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['statusTime'])) {
            $news->setStatusTime($attributes['statusTime']);
        }

        if (isset($attributes['updateTime'])) {
            $news->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['status'])) {
            $news->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
    
        if (isset($relationships['crew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['crew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;

            $news->setCrew($crew);
        }

        if (isset($relationships['publishUserGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['publishUserGroup']['data']);
            $news->setUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($userGroup));
        }
        
        if (isset($relationships['newsType']['data'])) {
            $newsType = $this->changeArrayFormat($relationships['newsType']['data']);
            $news->setNewsType($this->getNewsCategoryRestfulTranslator()->arrayToObject($newsType));
        }

        return $news;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($news, array $keys = array())
    {
        if (!$news instanceof News) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'releaseTime',
                'crew'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'news'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $news->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $news->getTitle();
        }
        if (in_array('source', $keys)) {
            $attributes['source'] = $news->getSource();
        }
        if (in_array('attachments', $keys)) {
            $attributes['attachments'] = $news->getAttachments();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $news->getStatus();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $news->getDimension();
        }
        if (in_array('releaseTime', $keys)) {
            $attributes['releaseTime'] = $news->getReleaseTime();
        }
        if (in_array('bannerImage', $keys)) {
            $attributes['bannerImage'] = $news->getBannerImage();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $news->getContent();
        }
        if (in_array('stick', $keys)) {
            $attributes['stick'] = $news->getStick();
        }
        if (in_array('cover', $keys)) {
            $attributes['cover'] = $news->getCover();
        }
        if (in_array('bannerStatus', $keys)) {
            $attributes['bannerStatus'] = $news->getBannerStatus();
        }
        if (in_array('homePageShowStatus', $keys)) {
            $attributes['homePageShowStatus'] = $news->getHomePageShowStatus();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $news->getCrew()->getId()
                )
            );
        }

        if (in_array('newsType', $keys)) {
            $expression['data']['relationships']['newsType']['data'] = array(
                array(
                    'type' => 'newsCategories',
                    'id' => $news->getNewsType()->getId()
                )
            );
        }
        
        return $expression;
    }
}
