<?php
namespace Sdk\News\News\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\Model\IEnableAble;

use Sdk\News\News\Model\News;
use Sdk\News\News\Model\NullNews;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;
use Sdk\News\NewsCategory\Translator\NewsCategoryTranslator;

class NewsTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $news = null)
    {
        unset($news);
        unset($expression);
        return new NullNews();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getNewsCategoryTranslator() : NewsCategoryTranslator
    {
        return new NewsCategoryTranslator();
    }

   /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($news, array $keys = array())
    {
        if (!$news instanceof News) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'description',
                'newsType' => [],
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'releaseTime',
                'crew' => [],
                'userGroup'=> [],
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($news->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = Filter::dhtmlspecialchars($news->getTitle());
        }
        if (in_array('source', $keys)) {
            $expression['source'] = Filter::dhtmlspecialchars($news->getSource());
        }
        if (in_array('attachments', $keys)) {
            $expression['attachments'] = $news->getAttachments();
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension'] = [
                'id' => marmot_encode($news->getDimension()),
                'name' => News::DIMENSION_CN[$news->getDimension()]
            ];
        }
        if (in_array('bannerImage', $keys)) {
            $expression['bannerImage'] = $news->getBannerImage();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = Filter::dhtmlspecialchars(str_replace("ℑ", "", $news->getContent()));//phpcs:ignore
        }
        if (in_array('description', $keys)) {
            $expression['description'] = Filter::dhtmlspecialchars($news->getDescription());
        }
        if (in_array('cover', $keys)) {
            $expression['cover'] = $news->getCover();
        }
        if (in_array('stick', $keys)) {
            $expression['stick'] = [
                'id' => marmot_encode($news->getStick()),
                'name' => ITopAble::STICK_CN[$news->getStick()],
                'type' => ITopAble::STICK_TAG_TYPE[$news->getStick()]
            ];
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($news->getStatus()),
                'name' => IEnableAble::STATUS_CN[$news->getStatus()],
                'type' => IEnableAble::STATUS_TAG_TYPE[$news->getStatus()]
            ];
        }
        if (in_array('bannerStatus', $keys)) {
            $expression['bannerStatus'] = [
                'id' => marmot_encode($news->getBannerStatus()),
                'name' => News::BANNER_STATUS_CN[$news->getBannerStatus()],
                'type' => News::BANNER_STATUS_TAG_TYPE[$news->getBannerStatus()]
            ];
        }
        if (in_array('homePageShowStatus', $keys)) {
            $expression['homePageShowStatus'] =[
                'id' => marmot_encode($news->getHomePageShowStatus()),
                'name' => News::HOME_PAGE_SHOW_STATUS_CN[$news->getHomePageShowStatus()],
                'type' => News::HOME_PAGE_SHOW_STATUS_TAG_TYPE[$news->getHomePageShowStatus()]
            ];
        }

        if (in_array('releaseTime', $keys)) {
            $expression['releaseTime'] = $news->getReleaseTime();
            $expression['releaseTimeFormat'] = date('Y年m月d日', $news->getReleaseTime());
        }

        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $news->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $news->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $news->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $news->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $news->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $news->getCreateTime());
        }
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $news->getCrew(),
                $keys['crew']
            );
        }
        if (isset($keys['userGroup'])) {
            $expression['publishUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $news->getUserGroup(),
                $keys['userGroup']
            );
        }
        if (isset($keys['newsType'])) {
            $expression['newsType'] = $this->getNewsCategoryTranslator()->objectToArray(
                $news->getNewsType(),
                $keys['newsType']
            );
        }

        return $expression;
    }
}
