<?php
namespace Sdk\News\News\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\News\News\Model\UnAuditNews;
use Sdk\News\News\Model\NullUnAuditNews;

use Sdk\Common\Translator\ApplyTranslatorTrait;

class UnAuditNewsTranslator extends NewsTranslator implements ITranslator
{
    use ApplyTranslatorTrait;

    public function arrayToObject(array $expression, $unAuditedNews = null)
    {
        unset($unAuditedNews);
        unset($expression);
        return new NullUnAuditNews();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function objectToArray($unAuditedNews, array $keys = array())
    {
        if (!$unAuditedNews instanceof UnAuditNews) {
            return array();
        }

        $news = parent::objectToArray($unAuditedNews, $keys);

        $apply = $this->applyObjectToArray($unAuditedNews, $keys);

        $unAuditedNewsArray = array_merge($news, $apply);

        return $unAuditedNewsArray;
    }
}
