<?php
namespace Sdk\News\News\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\ApplyRestfulTranslatorTrait;

use Sdk\News\News\Model\UnAuditNews;
use Sdk\News\News\Model\NullUnAuditNews;

class UnAuditNewsRestfulTranslator extends NewsRestfulTranslator implements IRestfulTranslator
{
    use ApplyRestfulTranslatorTrait;

    public function arrayToObject(array $expression, $unAuditedNews = null)
    {
        return $this->translateToObject($expression, $unAuditedNews);
    }

    protected function translateToObject(array $expression, $unAuditedNews = null)
    {
        if (empty($expression)) {
            return new NullUnAuditNews();
        }

        if ($unAuditedNews == null) {
            $unAuditedNews = new UnAuditNews();
        }
        
        $unAuditedNews = parent::translateToObject($expression, $unAuditedNews);

        $unAuditedNews = $this->applyTranslateToObject($expression, $unAuditedNews);

        return $unAuditedNews;
    }

    public function objectToArray($unAuditedNews, array $keys = array())
    {
        if (!$unAuditedNews instanceof UnAuditNews) {
            return array();
        }

        $expression = array(
            'data' => array(
                'type' => 'unAuditedNews'
            )
        );

        $news = parent::objectToArray($unAuditedNews, $keys);

        $apply = $this->applyObjectToArray($unAuditedNews, $keys);

        $newsAttributes = isset($news['data']['attributes']) ? $news['data']['attributes'] : array();
        $applyAttributes = isset($apply['data']['attributes']) ? $apply['data']['attributes'] : array();
        $expression['data']['attributes'] = array_merge($newsAttributes, $applyAttributes);

        $newsRelationships = isset($news['data']['relationships']) ? $news['data']['relationships'] : array();
        $applyRelationships = isset($apply['data']['relationships']) ? $apply['data']['relationships'] : array();
        $expression['data']['relationships'] = array_merge($newsRelationships, $applyRelationships);

        return $expression;
    }
}
