<?php
namespace Sdk\News\News\CommandHandler\UnAuditNews;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\RejectCommandHandler;

class RejectUnAuditNewsCommandHandler extends RejectCommandHandler
{
    use UnAuditNewsCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditNews($id);
    }
}
