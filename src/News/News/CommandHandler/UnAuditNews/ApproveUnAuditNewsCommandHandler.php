<?php
namespace Sdk\News\News\CommandHandler\UnAuditNews;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\ApproveCommandHandler;

class ApproveUnAuditNewsCommandHandler extends ApproveCommandHandler
{
    use UnAuditNewsCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditNews($id);
    }
}
