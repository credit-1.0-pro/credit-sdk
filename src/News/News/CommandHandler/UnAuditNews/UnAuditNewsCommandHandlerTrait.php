<?php
namespace Sdk\News\News\CommandHandler\UnAuditNews;

use Sdk\News\News\Model\UnAuditNews;
use Sdk\News\News\Repository\UnAuditNewsRepository;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Repository\NewsCategoryRepository;

trait UnAuditNewsCommandHandlerTrait
{
    protected function getRepository() : UnAuditNewsRepository
    {
        return new UnAuditNewsRepository();
    }
    
    protected function fetchUnAuditNews(int $id) : UnAuditNews
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getNewsCategoryRepository() : NewsCategoryRepository
    {
        return new NewsCategoryRepository();
    }
    
    protected function fetchNewsCategory(int $id) : NewsCategory
    {
        return $this->getNewsCategoryRepository()->fetchOne($id);
    }
}
