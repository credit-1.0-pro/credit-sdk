<?php
namespace Sdk\News\News\CommandHandler\UnAuditNews;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\News\News\Command\UnAuditNews\EditUnAuditNewsCommand;

class EditUnAuditNewsCommandHandler implements ICommandHandler
{
    use UnAuditNewsCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditUnAuditNewsCommand)) {
            throw new \InvalidArgumentException;
        }

        $newsCategory = $this->fetchNewsCategory($command->newsType);
        $unAuditNews = $this->fetchUnAuditNews($command->id);

        $unAuditNews->setTitle($command->title);
        $unAuditNews->setSource($command->source);
        $unAuditNews->setCover($command->cover);
        $unAuditNews->setAttachments($command->attachments);
        $unAuditNews->setBannerImage($command->bannerImage);
        $unAuditNews->setContent($command->content);
        $unAuditNews->setNewsType($newsCategory);
        $unAuditNews->setDimension($command->dimension);
        $unAuditNews->setStatus($command->status);
        $unAuditNews->setStick($command->stick);
        $unAuditNews->setBannerStatus($command->bannerStatus);
        $unAuditNews->setHomePageShowStatus($command->homePageShowStatus);
        $unAuditNews->setReleaseTime($command->releaseTime);
       
        return $unAuditNews->edit();
    }
}
