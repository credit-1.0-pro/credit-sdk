<?php
namespace Sdk\News\News\CommandHandler\UnAuditNews;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class UnAuditNewsCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\News\News\Command\UnAuditNews\EditUnAuditNewsCommand'=>
            'Sdk\News\News\CommandHandler\UnAuditNews\EditUnAuditNewsCommandHandler',
        'Sdk\News\News\Command\UnAuditNews\ApproveUnAuditNewsCommand'=>
            'Sdk\News\News\CommandHandler\UnAuditNews\ApproveUnAuditNewsCommandHandler',
        'Sdk\News\News\Command\UnAuditNews\RejectUnAuditNewsCommand'=>
            'Sdk\News\News\CommandHandler\UnAuditNews\RejectUnAuditNewsCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
