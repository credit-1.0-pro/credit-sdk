<?php
namespace Sdk\News\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\News\News\Command\News\AddNewsCommand;

class AddNewsCommandHandler implements ICommandHandler
{
    use NewsCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddNewsCommand)) {
            throw new \InvalidArgumentException;
        }

        $news = $this->getNews();
        $news = $this->newsExecuteAction($command, $news);

        return $news->add();
    }
}
