<?php
namespace Sdk\News\News\CommandHandler\News;

use Sdk\Common\Model\IEnableAble;
use Sdk\Common\CommandHandler\DisableCommandHandler;

class DisableNewsCommandHandler extends DisableCommandHandler
{
    use NewsCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchNews($id);
    }
}
