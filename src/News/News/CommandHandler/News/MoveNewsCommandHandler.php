<?php
namespace Sdk\News\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\News\News\Command\News\MoveNewsCommand;

class MoveNewsCommandHandler implements ICommandHandler
{
    use NewsCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof MoveNewsCommand)) {
            throw new \InvalidArgumentException;
        }
    
        $newsCategory = $this->fetchNewsCategory($command->newType);
        $news = $this->fetchNews($command->id);
        $news->setNewsType($newsCategory);
        
        return $news->move();
    }
}
