<?php
namespace Sdk\News\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\News\News\Command\News\EditNewsCommand;

class EditNewsCommandHandler implements ICommandHandler
{
    use NewsCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditNewsCommand)) {
            throw new \InvalidArgumentException;
        }

        $news = $this->fetchNews($command->id);
        $news = $this->newsExecuteAction($command, $news);

        return $news->edit();
    }
}
