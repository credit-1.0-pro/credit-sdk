<?php
namespace Sdk\News\News\CommandHandler\News;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\CommandHandler\CancelTopCommandHandler;

class CancelTopNewsCommandHandler extends CancelTopCommandHandler
{
    use NewsCommandHandlerTrait;
    
    protected function fetchITopObject($id) : ITopAble
    {
        return $this->fetchNews($id);
    }
}
