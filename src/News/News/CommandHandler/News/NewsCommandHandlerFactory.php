<?php
namespace Sdk\News\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class NewsCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\News\News\Command\News\EditNewsCommand'=>
            'Sdk\News\News\CommandHandler\News\EditNewsCommandHandler',
        'Sdk\News\News\Command\News\AddNewsCommand'=>
            'Sdk\News\News\CommandHandler\News\AddNewsCommandHandler',
        'Sdk\News\News\Command\News\EnableNewsCommand'=>
            'Sdk\News\News\CommandHandler\News\EnableNewsCommandHandler',
        'Sdk\News\News\Command\News\DisableNewsCommand'=>
            'Sdk\News\News\CommandHandler\News\DisableNewsCommandHandler',
        'Sdk\News\News\Command\News\TopNewsCommand'=>
            'Sdk\News\News\CommandHandler\News\TopNewsCommandHandler',
        'Sdk\News\News\Command\News\CancelTopNewsCommand'=>
            'Sdk\News\News\CommandHandler\News\CancelTopNewsCommandHandler',
        'Sdk\News\News\Command\News\MoveNewsCommand'=>
            'Sdk\News\News\CommandHandler\News\MoveNewsCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
