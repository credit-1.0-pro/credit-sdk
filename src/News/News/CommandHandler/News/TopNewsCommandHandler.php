<?php
namespace Sdk\News\News\CommandHandler\News;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\CommandHandler\TopCommandHandler;

class TopNewsCommandHandler extends TopCommandHandler
{
    use NewsCommandHandlerTrait;
    
    protected function fetchITopObject($id) : ITopAble
    {
        return $this->fetchNews($id);
    }
}
