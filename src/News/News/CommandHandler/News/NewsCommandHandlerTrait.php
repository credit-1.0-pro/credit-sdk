<?php
namespace Sdk\News\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;

use Sdk\News\News\Model\News;
use Sdk\News\News\Repository\NewsRepository;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Repository\NewsCategoryRepository;

trait NewsCommandHandlerTrait
{
    protected function getNews() : News
    {
        return new News();
    }

    protected function getRepository() : NewsRepository
    {
        return new NewsRepository();
    }
    
    protected function fetchNews(int $id) : News
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getNewsCategoryRepository() : NewsCategoryRepository
    {
        return new NewsCategoryRepository();
    }
    
    protected function fetchNewsCategory(int $id) : NewsCategory
    {
        return $this->getNewsCategoryRepository()->fetchOne($id);
    }

    protected function newsExecuteAction(ICommand $command, News $news) : News
    {
        $newsCategory = $this->fetchNewsCategory($command->newsType);

        $news->setCover($command->cover);
        $news->setStick($command->stick);
        $news->setTitle($command->title);
        $news->setSource($command->source);
        $news->setStatus($command->status);
        $news->setContent($command->content);
        $news->setNewsType($newsCategory);
        $news->setDimension($command->dimension);
        $news->setAttachments($command->attachments);
        $news->setBannerImage($command->bannerImage);
        $news->setBannerStatus($command->bannerStatus);
        $news->setHomePageShowStatus($command->homePageShowStatus);
        $news->setReleaseTime($command->releaseTime);

        return $news;
    }
}
