<?php
namespace Sdk\News\News\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperateAbleRepositoryTrait;
use Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\TopAbleRepositoryTrait;

use Sdk\News\News\Adapter\News\NewsRestfulAdapter;
use Sdk\News\News\Adapter\News\NewsMockAdapter;
use Sdk\News\News\Adapter\News\INewsAdapter;
use Sdk\News\News\Model\News;

class NewsRepository extends Repository implements INewsAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        TopAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'NEWS_LIST';
    const FETCH_ONE_MODEL_UN = 'NEWS_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new NewsRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new NewsMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function move(News $news) : bool
    {
        return $this->getAdapter()->move($news);
    }
}
