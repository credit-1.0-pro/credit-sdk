<?php
namespace Sdk\News\News\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperateAbleRepositoryTrait;
use Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ApplyAbleRepositoryTrait;

use Sdk\News\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter;
use Sdk\News\News\Adapter\UnAuditNews\UnAuditNewsMockAdapter;
use Sdk\News\News\Adapter\UnAuditNews\IUnAuditNewsAdapter;

class UnAuditNewsRepository extends Repository implements IUnAuditNewsAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        ApplyAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'UN_AUDIT_NEWS_LIST';
    const FETCH_ONE_MODEL_UN = 'UN_AUDIT_NEWS_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new UnAuditNewsRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new UnAuditNewsMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
