<?php
namespace Sdk\News\News\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Common\Model\NullOperateAbleTrait;
use Sdk\Common\Model\NullEnableAbleTrait;
use Sdk\Common\Model\NullTopAbleTrait;

class NullNews extends News implements INull
{
    use NullEnableAbleTrait, NullOperateAbleTrait, NullTopAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function move() : bool
    {
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
