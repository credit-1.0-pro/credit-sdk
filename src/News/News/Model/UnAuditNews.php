<?php
namespace Sdk\News\News\Model;

use Marmot\Core;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\ApplyAbleTrait;
use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\News\News\Repository\UnAuditNewsRepository;

class UnAuditNews extends News implements IApplyAble
{
    use ApplyAbleTrait;
    
    private $unAuditNewsRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->rejectReason = '';
        $this->applyInfoType = 0;
        $this->relationId = 0;
        $this->publishCrew = new Crew();
        $this->applyCrew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->applyUserGroup = new UserGroup();
        $this->publishUserGroup = new UserGroup();
        $this->operationType = self::OPERATION_TYPE['NULL'];
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->unAuditNewsRepository = new UnAuditNewsRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->rejectReason);
        unset($this->operationType);
        unset($this->applyInfoType);
        unset($this->applyStatus);
        unset($this->relationId);
        unset($this->publishCrew);
        unset($this->applyCrew);
        unset($this->applyUserGroup);
        unset($this->publishUserGroup);
        unset($this->unAuditNewsRepository);
    }
    
    protected function getUnAuditNewsRepository()
    {
        return $this->unAuditNewsRepository;
    }

    /**
     * 审核通过/驳回
     * @return [bool]
     */
    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getUnAuditNewsRepository();
    }

    /**
     * 新增编辑
     * @return [bool]
     */
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getUnAuditNewsRepository();
    }
}
