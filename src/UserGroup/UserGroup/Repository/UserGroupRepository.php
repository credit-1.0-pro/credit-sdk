<?php
namespace Sdk\UserGroup\UserGroup\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperateAbleRepositoryTrait;

use Sdk\UserGroup\UserGroup\Adapter\IUserGroupAdapter;
use Sdk\UserGroup\UserGroup\Adapter\UserGroupMockAdapter;
use Sdk\UserGroup\UserGroup\Adapter\UserGroupRestfulAdapter;

class UserGroupRepository extends Repository implements IUserGroupAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperateAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'USER_GROUP_LIST';
    const FETCH_ONE_MODEL_UN = 'USER_GROUP_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new UserGroupRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new UserGroupMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
