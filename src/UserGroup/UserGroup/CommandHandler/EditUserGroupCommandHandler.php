<?php
namespace Sdk\UserGroup\UserGroup\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Command\EditUserGroupCommand;
use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;

class EditUserGroupCommandHandler implements ICommandHandler
{
    private $repository;

    public function __construct()
    {
        $this->repository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository(): UserGroupRepository
    {
        return $this->repository;
    }

    protected function fetchUserGroup(int $id): UserGroup
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof EditUserGroupCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $userGroup = $this->fetchUserGroup($command->id);
        $userGroup->setName($command->name);
        $userGroup->setShortName($command->shortName);
        $userGroup->setUnifiedSocialCreditCode($command->unifiedSocialCreditCode);

        return $userGroup->edit();
    }
}
