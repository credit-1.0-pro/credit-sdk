<?php
namespace Sdk\UserGroup\UserGroup\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Command\AddUserGroupCommand;

class AddUserGroupCommandHandler implements ICommandHandler
{
    private $userGroup;

    public function __construct()
    {
        $this->userGroup = new UserGroup();
    }

    public function __destruct()
    {
        unset($this->userGroup);
    }

    protected function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }
    
    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(ICommand $command)
    {
        if (!($command instanceof AddUserGroupCommand)) {
            throw new \InvalidArgumentException;
        }

        $userGroup = $this->getUserGroup();
        $userGroup->setName($command->name);
        $userGroup->setShortName($command->shortName);
        $userGroup->setUnifiedSocialCreditCode($command->unifiedSocialCreditCode);
        $userGroup->setAdministrativeArea($command->administrativeArea);

        return $userGroup->add();
    }
}
