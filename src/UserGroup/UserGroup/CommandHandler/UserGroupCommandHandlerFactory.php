<?php
namespace Sdk\UserGroup\UserGroup\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class UserGroupCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\UserGroup\UserGroup\Command\AddUserGroupCommand' =>
        'Sdk\UserGroup\UserGroup\CommandHandler\AddUserGroupCommandHandler',
        'Sdk\UserGroup\UserGroup\Command\EditUserGroupCommand' =>
        'Sdk\UserGroup\UserGroup\CommandHandler\EditUserGroupCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
