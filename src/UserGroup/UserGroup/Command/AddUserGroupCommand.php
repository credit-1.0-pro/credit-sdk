<?php
namespace Sdk\UserGroup\UserGroup\Command;

use Marmot\Interfaces\ICommand;

class AddUserGroupCommand implements ICommand
{
    public $name;
    
    public $shortName;

    public $unifiedSocialCreditCode;

    public $administrativeArea;

    public function __construct(
        string $name,
        string $shortName,
        string $unifiedSocialCreditCode,
        int $administrativeArea
    ) {
        $this->name = $name;
        $this->shortName = $shortName;
        $this->unifiedSocialCreditCode = $unifiedSocialCreditCode;
        $this->administrativeArea = $administrativeArea;
    }
}
