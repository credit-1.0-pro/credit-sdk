<?php
namespace Sdk\UserGroup\UserGroup\Command;

use Marmot\Interfaces\ICommand;

class EditUserGroupCommand implements ICommand
{
    public $name;
    
    public $shortName;

    public $unifiedSocialCreditCode;

    public $id;

    public function __construct(
        string $name,
        string $shortName,
        string $unifiedSocialCreditCode,
        int $id
    ) {
        $this->name = $name;
        $this->shortName = $shortName;
        $this->unifiedSocialCreditCode = $unifiedSocialCreditCode;
        $this->id = $id;
    }
}
