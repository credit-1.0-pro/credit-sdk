<?php
namespace Sdk\UserGroup\UserGroup\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Model\NullUserGroup;

class UserGroupTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $userGroup = null)
    {
        unset($userGroup);
        unset($expression);
        return NullUserGroup::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($userGroup, array $keys = array())
    {
        if (!$userGroup instanceof UserGroup) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'shortName',
                'unifiedSocialCreditCode',
                'administrativeArea',
                'updateTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($userGroup->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $userGroup->getName();
        }
        if (in_array('shortName', $keys)) {
            $expression['shortName'] = $userGroup->getShortName();
        }
        if (in_array('unifiedSocialCreditCode', $keys)) {
            $expression['unifiedSocialCreditCode'] = $userGroup->getUnifiedSocialCreditCode();
        }
        if (in_array('administrativeArea', $keys)) {
            $expression['administrativeArea'] = [
                'id' => marmot_encode($userGroup->getAdministrativeArea()),
                'name' => UserGroup::ADMINISTRATIVE_AREA_CN[$userGroup->getAdministrativeArea()]
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $userGroup->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H点i分', $userGroup->getUpdateTime());
        }
        
        return $expression;
    }
}
