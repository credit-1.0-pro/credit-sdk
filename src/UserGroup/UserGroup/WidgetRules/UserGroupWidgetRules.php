<?php
namespace Sdk\UserGroup\UserGroup\WidgetRules;

use Marmot\Core;
use Respect\Validation\Validator as V;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

class UserGroupWidgetRules
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    const NAME_MIN_LENGTH = 2;
    const NAME_MAX_LENGTH = 20;

    //@tod 入参约束
    public function userGroupName($userGroupName) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::NAME_MIN_LENGTH,
            self::NAME_MAX_LENGTH
        )->validate($userGroupName)) {
            Core::setLastError(USER_GROUP_NAME_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    const SHORT_NAME_MIN_LENGTH = 1;
    const SHORT_NAME_MAX_LENGTH = 8;

    public function shortName($shortName) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::SHORT_NAME_MIN_LENGTH,
            self::SHORT_NAME_MAX_LENGTH
        )->validate($shortName)) {
            Core::setLastError(USER_GROUP_SHORT_NAME_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    public function administrativeArea($administrativeArea) : bool
    {
        if (!V::numeric()->validate($administrativeArea)
        || !in_array($administrativeArea, UserGroup::ADMINISTRATIVE_AREA)) {
            Core::setLastError(USER_GROUP_ADMINISTRATIVE_AREA_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }
}
