<?php
namespace Sdk\UserGroup\UserGroup\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\IOperateAble;
use Sdk\Common\Model\OperateAbleTrait;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;

class UserGroup implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;
    
    const STATUS_NORMAL = 0; //正常
    
    const ADMINISTRATIVE_AREA = array(
        'WLCB' => 0,
        'JNQ' => 100,
        'FZS' => 101,
        'CYQQ' => 102,
        'CYZQ' => 103,
        'CYHQ' => 104,
        'SZWQ' => 105,
        'ZZX' => 106,
        'LCX' => 107,
        'XHX' => 108,
        'SDX' => 109,
        'HDX' => 110
    );

    const ADMINISTRATIVE_AREA_CN = array(
        '0' => '乌兰察布市',
        '100' => '集宁区',
        '101' => '丰镇市',
        '102' => '察右前旗',
        '103' => '察右中旗',
        '104' => '察右后旗',
        '105' => '四子王旗',
        '106' => '卓资县',
        '107' => '凉城县',
        '108' => '兴和县',
        '109' => '商都县',
        '110' => '化德县'
    );

    private $id;
    
    private $name;
    
    private $shortName;

    private $unifiedSocialCreditCode;

    private $administrativeArea;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->shortName = '';
        $this->unifiedSocialCreditCode = '';
        $this->administrativeArea = self::ADMINISTRATIVE_AREA['WLCB'];
        $this->updateTime = 0;
        $this->createTime = 0;
        $this->status = self::STATUS_NORMAL;
        $this->statusTime = 0;
        $this->repository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->shortName);
        unset($this->unifiedSocialCreditCode);
        unset($this->administrativeArea);
        unset($this->updateTime);
        unset($this->createTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setShortName(string $shortName): void
    {
        $this->shortName = $shortName;
    }

    public function getShortName(): string
    {
        return $this->shortName;
    }
    
    public function setUnifiedSocialCreditCode(string $unifiedSocialCreditCode) : void
    {
        $this->unifiedSocialCreditCode = $unifiedSocialCreditCode;
    }

    public function getUnifiedSocialCreditCode() : string
    {
        return $this->unifiedSocialCreditCode;
    }
    
    public function setAdministrativeArea(int $administrativeArea) : void
    {
        $this->administrativeArea = in_array(
            $administrativeArea,
            self::ADMINISTRATIVE_AREA
        ) ? $administrativeArea : self::ADMINISTRATIVE_AREA['WLCB'];
    }

    public function getAdministrativeArea() : int
    {
        return $this->administrativeArea;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->repository;
    }

    protected function nameIsExist(): bool
    {
        $sort = ['-updateTime'];

        $filter['unique'] = $this->getName();
        $filter['administrativeArea'] = $this->getAdministrativeArea();
        
        if (!empty($this->getId())) {
            $filter['id'] = $this->getId();
        }

        list($count, $userGroupList) = $this->getIOperateAbleAdapter()->scenario(
            UserGroupRepository::LIST_MODEL_UN
        )->search($filter, $sort);

        unset($userGroupList);

        if (!empty($count)) {
            Core::setLastError(USER_GROUP_NAME_IS_UNIQUE);
            return false;
        }

        return true;
    }

    protected function addAction(): bool
    {
        return $this->nameIsExist() && $this->getIOperateAbleAdapter()->add($this);
    }

    protected function editAction(): bool
    {
        return $this->nameIsExist() && $this->getIOperateAbleAdapter()->edit($this);
    }
}
