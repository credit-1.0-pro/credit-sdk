<?php
namespace Sdk\UserGroup\UserGroup\Adapter;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

interface IUserGroupAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperateAbleAdapter
{

}
