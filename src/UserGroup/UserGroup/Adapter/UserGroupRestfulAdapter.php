<?php
namespace Sdk\UserGroup\UserGroup\Adapter;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Model\NullUserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

class UserGroupRestfulAdapter extends GuzzleAdapter implements IUserGroupAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'USER_GROUP_LIST'=>[
            'fields' => []
        ],
        'USER_GROUP_FETCH_ONE'=>[
            'fields' => []
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UserGroupRestfulTranslator();
        $this->resource = 'userGroups';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullUserGroup::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            101 => [
                'name'=>USER_GROUP_NAME_FORMAT_ERROR,
                'shortName'=>USER_GROUP_SHORT_NAME_FORMAT_ERROR,
                'administrativeArea' => USER_GROUP_ADMINISTRATIVE_AREA_FORMAT_ERROR,
                'unifiedSocialCreditCode' => UNIFIED_SOCIAL_CREDIT_CODE_FORMAT_ERROR
            ],
            103 => [
                'userGroupName'=>USER_GROUP_NAME_IS_UNIQUE
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function addAction(UserGroup $userGroup) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $userGroup,
            array('name', 'shortName', 'administrativeArea', 'unifiedSocialCreditCode')
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($userGroup);
            return true;
        }

        return false;
    }

    protected function editAction(UserGroup $userGroup) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $userGroup,
            array('name', 'shortName', 'unifiedSocialCreditCode')
        );

        $this->patch(
            $this->getResource().'/'.$userGroup->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($userGroup);
            return true;
        }

        return false;
    }
}
