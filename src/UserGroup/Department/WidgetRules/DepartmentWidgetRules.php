<?php
namespace Sdk\UserGroup\Department\WidgetRules;

use Marmot\Core;
use Respect\Validation\Validator as V;

class DepartmentWidgetRules
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    const NAME_MIN_LENGTH = 2;
    const NAME_MAX_LENGTH = 20;

    //@tod 入参约束
    public function departmentName($departmentName) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::NAME_MIN_LENGTH,
            self::NAME_MAX_LENGTH
        )->validate($departmentName)) {
            Core::setLastError(DEPARTMENT_NAME_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }
}
