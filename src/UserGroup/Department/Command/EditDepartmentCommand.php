<?php
namespace Sdk\UserGroup\Department\Command;

use Marmot\Interfaces\ICommand;

class EditDepartmentCommand implements ICommand
{
    public $id;
    
    public $name;
    
    public function __construct(
        int $id,
        string $name
    ) {
        $this->id = $id;
        $this->name = $name;
    }
}
