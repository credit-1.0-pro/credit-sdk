<?php
namespace Sdk\UserGroup\Department\Command;

use Marmot\Interfaces\ICommand;

class AddDepartmentCommand implements ICommand
{
    public $name;
    
    public $userGroupId;

    public function __construct(
        string $name,
        int $userGroupId
    ) {
        $this->name = $name;
        $this->userGroupId = $userGroupId;
    }
}
