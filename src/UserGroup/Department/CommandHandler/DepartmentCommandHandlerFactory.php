<?php
namespace Sdk\UserGroup\Department\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class DepartmentCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\UserGroup\Department\Command\AddDepartmentCommand' =>
        'Sdk\UserGroup\Department\CommandHandler\AddDepartmentCommandHandler',
        'Sdk\UserGroup\Department\Command\EditDepartmentCommand' =>
        'Sdk\UserGroup\Department\CommandHandler\EditDepartmentCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
