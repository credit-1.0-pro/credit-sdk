<?php
namespace Sdk\UserGroup\Department\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\UserGroup\Department\Model\Department;
use Sdk\UserGroup\Department\Command\EditDepartmentCommand;
use Sdk\UserGroup\Department\Repository\DepartmentRepository;

class EditDepartmentCommandHandler implements ICommandHandler
{
    private $repository;

    public function __construct()
    {
        $this->repository = new DepartmentRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository(): DepartmentRepository
    {
        return $this->repository;
    }

    protected function fetchDepartment(int $id): Department
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof EditDepartmentCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $department = $this->fetchDepartment($command->id);
        $department->setName($command->name);

        return $department->edit();
    }
}
