<?php
namespace Sdk\UserGroup\Department\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\UserGroup\Department\Model\Department;
use Sdk\UserGroup\Department\Command\AddDepartmentCommand;
use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;

class AddDepartmentCommandHandler implements ICommandHandler
{
    private $department;

    private $userGroupRepository;

    public function __construct()
    {
        $this->department = new Department();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->department);
    }

    protected function getDepartment() : Department
    {
        return $this->department;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }
    
    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(ICommand $command)
    {
        if (!($command instanceof AddDepartmentCommand)) {
            throw new \InvalidArgumentException;
        }

        $department = $this->getDepartment();
        $department->setUserGroup(
            $this->getUserGroupRepository()->fetchOne($command->userGroupId)
        );
        $department->setName($command->name);

        return $department->add();
    }
}
