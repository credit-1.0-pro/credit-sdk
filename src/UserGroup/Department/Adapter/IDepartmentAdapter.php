<?php
namespace Sdk\UserGroup\Department\Adapter;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

interface IDepartmentAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperateAbleAdapter
{

}
