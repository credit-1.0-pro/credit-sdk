<?php
namespace Sdk\UserGroup\Department\Adapter;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\UserGroup\Department\Model\Department;
use Sdk\UserGroup\Department\Model\NullDepartment;
use Sdk\UserGroup\Department\Translator\DepartmentRestfulTranslator;

class DepartmentRestfulAdapter extends GuzzleAdapter implements IDepartmentAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
        'DEPARTMENT_LIST'=>[
            'fields' => [
                'departments'=>'name,updateTime,userGroup',
                'userGroups'=>'name'
            ],
            'include' => 'userGroup'
        ],
        'DEPARTMENT_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'userGroup'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new DepartmentRestfulTranslator();
        $this->resource = 'userGroups/departments';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullDepartment::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            100 => [
                'userGroupId'=>DEPARTMENT_USER_GROUP_IS_EMPTY
            ],
            101 => [
                'name'=>DEPARTMENT_NAME_FORMAT_ERROR,
                'userGroupId' => DEPARTMENT_USER_GROUP_FORMAT_ERROR
            ],
            103 => [
                'departmentName'=>DEPARTMENT_NAME_IS_UNIQUE
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }
    
    protected function addAction(Department $department) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $department,
            array('name', 'userGroup')
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($department);
            return true;
        }

        return false;
    }

    protected function editAction(Department $department) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $department,
            array('name')
        );

        $this->patch(
            $this->getResource().'/'.$department->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($department);
            return true;
        }

        return false;
    }
}
