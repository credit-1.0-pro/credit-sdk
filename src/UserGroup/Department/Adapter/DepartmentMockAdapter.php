<?php
namespace Sdk\UserGroup\Department\Adapter;

use Sdk\Common\Adapter\OperateAbleMockAdapterTrait;

use Sdk\UserGroup\Department\Utils\MockDepartmentFactory;

class DepartmentMockAdapter implements IDepartmentAdapter
{
    use OperateAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockDepartmentFactory::generateDepartment($id);
    }

    public function fetchList(array $ids) : array
    {
        $departmentList = array();

        foreach ($ids as $id) {
            $departmentList[] = MockDepartmentFactory::generateDepartment($id);
        }

        return $departmentList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockDepartmentFactory::generateDepartment($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $departmentList = array();

        foreach ($ids as $id) {
            $departmentList[] = MockDepartmentFactory::generateDepartment($id);
        }

        return $departmentList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
