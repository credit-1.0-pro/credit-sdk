<?php
namespace Sdk\UserGroup\Department\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperateAbleRepositoryTrait;

use Sdk\UserGroup\Department\Adapter\IDepartmentAdapter;
use Sdk\UserGroup\Department\Adapter\DepartmentMockAdapter;
use Sdk\UserGroup\Department\Adapter\DepartmentRestfulAdapter;

class DepartmentRepository extends Repository implements IDepartmentAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperateAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'DEPARTMENT_LIST';
    const FETCH_ONE_MODEL_UN = 'DEPARTMENT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new DepartmentRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new DepartmentMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
