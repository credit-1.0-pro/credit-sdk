<?php
namespace Sdk\User\Member\Command\Member;

use Marmot\Interfaces\ICommand;

class AddMemberCommand implements ICommand
{
    public $userName;
    
    public $realName;
    
    public $cardId;
    
    public $cellphone;
    
    public $email;
    
    public $contactAddress;
    
    public $securityAnswer;
    
    public $password;
    
    public $securityQuestion;
    
    public $id;

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function __construct(
        string $userName,
        string $realName,
        string $cardId,
        string $cellphone,
        string $email,
        string $contactAddress,
        string $securityAnswer,
        string $password,
        int $securityQuestion = 0,
        int $id = 0
    ) {
        $this->userName = $userName;
        $this->realName = $realName;
        $this->cardId = $cardId;
        $this->cellphone = $cellphone;
        $this->email = $email;
        $this->contactAddress = $contactAddress;
        $this->securityAnswer = $securityAnswer;
        $this->password = $password;
        $this->securityQuestion = $securityQuestion;
        $this->id = $id;
    }
}
