<?php
namespace Sdk\User\Member\Command\Member;

use Marmot\Interfaces\ICommand;

class EditMemberCommand implements ICommand
{
    public $gender;

    public $id;

    public function __construct(
        int $gender,
        int $id
    ) {
        $this->gender = $gender;
        $this->id = $id;
    }
}
