<?php
namespace Sdk\User\Member\Command\Member;

use Marmot\Interfaces\ICommand;

class ValidateSecurityMemberCommand implements ICommand
{
    public $securityAnswer;
    
    public $id;
    
    public function __construct(
        string $securityAnswer,
        int $id = 0
    ) {
        $this->securityAnswer = $securityAnswer;
        $this->id = $id;
    }
}
