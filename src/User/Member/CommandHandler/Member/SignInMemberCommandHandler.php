<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\User\Member\Command\Member\SignInMemberCommand;

class SignInMemberCommandHandler implements ICommandHandler
{
    use MemberCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof SignInMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->getMember();
        $member->setUserName($command->userName);
        $member->setPassword($command->password);

        return $member->signIn();
    }
}
