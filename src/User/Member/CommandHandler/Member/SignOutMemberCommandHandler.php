<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Command\Member\SignOutMemberCommand;

class SignOutMemberCommandHandler implements ICommandHandler
{
    private $member;

    public function __construct()
    {
        $this->member = Core::$container->get('member');
    }

    public function __destruct()
    {
        unset($this->member);
    }

    protected function getMember()
    {
        return $this->member;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof SignOutMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->getMember();

        return $member->signOut();
    }
}
