<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class MemberCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\User\Member\Command\Member\AddMemberCommand' =>
        'Sdk\User\Member\CommandHandler\Member\AddMemberCommandHandler',
        'Sdk\User\Member\Command\Member\EditMemberCommand' =>
        'Sdk\User\Member\CommandHandler\Member\EditMemberCommandHandler',
        'Sdk\User\Member\Command\Member\EnableMemberCommand' =>
        'Sdk\User\Member\CommandHandler\Member\EnableMemberCommandHandler',
        'Sdk\User\Member\Command\Member\DisableMemberCommand' =>
        'Sdk\User\Member\CommandHandler\Member\DisableMemberCommandHandler',
        'Sdk\User\Member\Command\Member\SignInMemberCommand' =>
        'Sdk\User\Member\CommandHandler\Member\SignInMemberCommandHandler',
        'Sdk\User\Member\Command\Member\SignOutMemberCommand' =>
        'Sdk\User\Member\CommandHandler\Member\SignOutMemberCommandHandler',
        'Sdk\User\Member\Command\Member\ResetPasswordMemberCommand' =>
        'Sdk\User\Member\CommandHandler\Member\ResetPasswordMemberCommandHandler',
        'Sdk\User\Member\Command\Member\UpdatePasswordMemberCommand' =>
        'Sdk\User\Member\CommandHandler\Member\UpdatePasswordMemberCommandHandler',
        'Sdk\User\Member\Command\Member\ValidateSecurityMemberCommand' =>
        'Sdk\User\Member\CommandHandler\Member\ValidateSecurityMemberCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
