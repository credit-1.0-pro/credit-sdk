<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\User\Member\Command\Member\AddMemberCommand;

class AddMemberCommandHandler implements ICommandHandler
{
    use MemberCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof AddMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->getMember();
        $member->setUserName($command->userName);
        $member->setRealName($command->realName);
        $member->setCellphone($command->cellphone);
        $member->setEmail($command->email);
        $member->setCardId($command->cardId);
        $member->setContactAddress($command->contactAddress);
        $member->setSecurityQuestion($command->securityQuestion);
        $member->setSecurityAnswer($command->securityAnswer);
        $member->setPassword($command->password);
        
        return $member->add();
    }
}
