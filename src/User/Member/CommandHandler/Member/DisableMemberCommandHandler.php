<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Sdk\Common\Model\IEnableAble;
use Sdk\Common\CommandHandler\DisableCommandHandler;

class DisableMemberCommandHandler extends DisableCommandHandler
{
    use MemberCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchMember($id);
    }
}
