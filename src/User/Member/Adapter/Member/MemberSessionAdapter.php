<?php
namespace Sdk\User\Member\Adapter\Member;

use Marmot\Core;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Model\NullMember;
use Sdk\User\Member\Translator\MemberSessionTranslator;
use Sdk\User\Member\Adapter\Member\Query\MemberSessionDataCacheQuery;

class MemberSessionAdapter
{
    const TTL = 300;

    private $session;

    private $translator;

    public function __construct()
    {
        $this->session = new MemberSessionDataCacheQuery();
        $this->translator = new MemberSessionTranslator();
    }

    protected function getSession() : MemberSessionDataCacheQuery
    {
        return $this->session;
    }

    protected function getTranslator() : MemberSessionTranslator
    {
        return $this->translator;
    }

    protected function getTTL() : int
    {
        return Core::$container->has('cache.session.ttl') ? Core::$container->get('cache.session.ttl') : self::TTL;
    }

    public function get(int $id)
    {
        $info = $this->getSession()->get($id);

        return empty($info) ? NullMember::getInstance(): $this->getTranslator()->arrayToObject($info);
    }

    public function save(Member $member) : bool
    {
        $info = $this->getTranslator()->objectToArray($member);

        return $this->getSession()->save($member->getId(), $info, $this->getTTL());
    }

    public function del(int $id) : bool
    {
        return $this->getSession()->del($id);
    }
}
