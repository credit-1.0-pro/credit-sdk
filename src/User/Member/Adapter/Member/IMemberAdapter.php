<?php
namespace Sdk\User\Member\Adapter\Member;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\User\Member\Model\Member;

interface IMemberAdapter extends IAsyncAdapter, IFetchAbleAdapter, IEnableAbleAdapter, IOperateAbleAdapter
{
    public function signIn(Member $member) : bool;

    public function resetPassword(Member $member) : bool;

    public function updatePassword(Member $member) : bool;

    public function validateSecurity(Member $member) : bool;
}
