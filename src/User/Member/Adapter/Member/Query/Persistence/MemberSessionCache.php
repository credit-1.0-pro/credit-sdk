<?php
namespace Sdk\User\Member\Adapter\Member\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class MemberSessionCache extends Cache
{
    public function __construct()
    {
        parent::__construct('member.session');
    }
}
