<?php
namespace Sdk\User\Member\Adapter\Member\Query;

use Marmot\Framework\Query\DataCacheQuery;

use Sdk\User\Member\Adapter\Member\Query\Persistence\MemberSessionCache;

class MemberSessionDataCacheQuery extends DataCacheQuery
{
    public function __construct()
    {
        parent::__construct(new MemberSessionCache());
    }
}
