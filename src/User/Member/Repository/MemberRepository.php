<?php
namespace Sdk\User\Member\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Sdk\Common\Repository\OperateAbleRepositoryTrait;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Adapter\Member\IMemberAdapter;
use Sdk\User\Member\Adapter\Member\MemberMockAdapter;
use Sdk\User\Member\Adapter\Member\MemberRestfulAdapter;

class MemberRepository extends Repository implements IMemberAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        EnableAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'MEMBER_LIST';
    const FETCH_ONE_MODEL_UN = 'MEMBER_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new MemberRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    protected function getActualAdapter()
    {
        return $this->adapter;
    }

    protected function getMockAdapter()
    {
        return new MemberMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function signIn(Member $member) : bool
    {
        return $this->getAdapter()->signIn($member);
    }
    
    public function resetPassword(Member $member) : bool
    {
        return $this->getAdapter()->resetPassword($member);
    }
    
    public function updatePassword(Member $member) : bool
    {
        return $this->getAdapter()->updatePassword($member);
    }
    
    public function validateSecurity(Member $member) : bool
    {
        return $this->getAdapter()->validateSecurity($member);
    }
}
