<?php
namespace Sdk\User\Member\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Common\Model\NullEnableAbleTrait;
use Sdk\Common\Model\NullOperateAbleTrait;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NullMember extends Member implements INull
{
    use NullEnableAbleTrait, NullOperateAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function isCellphoneExist() : bool
    {
        return $this->resourceNotExist();
    }

    public function isEmailExist() : bool
    {
        return $this->resourceNotExist();
    }

    public function isUserNameExist() : bool
    {
        return $this->resourceNotExist();
    }

    public function resetPassword() : bool
    {
        return $this->resourceNotExist();
    }

    public function updatePassword() : bool
    {
        return $this->resourceNotExist();
    }

    public function validateSecurity() : bool
    {
        return $this->resourceNotExist();
    }

    public function signIn() : bool
    {
        return $this->resourceNotExist();
    }

    public function saveCookie() : bool
    {
        return $this->resourceNotExist();
    }

    public function saveSession() : bool
    {
        return $this->resourceNotExist();
    }

    public function signOut() : bool
    {
        return $this->resourceNotExist();
    }

    public function clearCookie() : bool
    {
        return $this->resourceNotExist();
    }

    public function clearSession() : bool
    {
        return $this->resourceNotExist();
    }

    public function validateIdentify(string $identify) : bool
    {
        unset($identify);
        return $this->resourceNotExist();
    }
}
