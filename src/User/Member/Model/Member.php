<?php
namespace Sdk\User\Member\Model;

use Marmot\Core;
use Marmot\Framework\Classes\Cookie;

use Sdk\Common\Model\IEnableAble;
use Sdk\User\Member\Repository\MemberRepository;
use Sdk\User\Member\Repository\MemberSessionRepository;

use Sdk\User\Model\User;

class Member extends User
{
    //@todo 这样命名无意义, ONE->1, TWO->2
    //如果要命名需要使用更语义化命名方式
    /**
     * @var SECURITY_QUESTION['NULL']  未知 0
     * @var SECURITY_QUESTION['QUESTION_ONE']  你父亲或母亲的姓名 1
     * @var SECURITY_QUESTION['QUESTION_TWO']  你喜欢看的电影 2
     * @var SECURITY_QUESTION['QUESTION_THREE']  你最好朋友的名字 3
     * @var SECURITY_QUESTION['QUESTION_FOUR']  你毕业于哪个高中 4
     * @var SECURITY_QUESTION['QUESTION_FIVE']  你最喜欢的颜色 5
     */
    const SECURITY_QUESTION = array(
        'NULL' => 0,
        'QUESTION_ONE' => 1,
        'QUESTION_TWO' => 2,
        'QUESTION_THREE' => 3,
        'QUESTION_FOUR' => 4,
        'QUESTION_FIVE' => 5,
    );

    const SECURITY_QUESTION_CN = array(
        Member::SECURITY_QUESTION['QUESTION_ONE'] => '你父亲或母亲的姓名',
        Member::SECURITY_QUESTION['QUESTION_TWO'] => '你喜欢看的电影',
        Member::SECURITY_QUESTION['QUESTION_THREE'] => '你最好朋友的名字',
        Member::SECURITY_QUESTION['QUESTION_FOUR'] => '你毕业于哪个高中',
        Member::SECURITY_QUESTION['QUESTION_FIVE'] => '你最喜欢的颜色'
    );

    private $email;

    private $contactAddress;

    private $securityQuestion;

    private $securityAnswer;
    
    private $repository;

    private $cookie;

    private $memberSessionRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->email = '';
        $this->contactAddress = '';
        $this->securityQuestion = self::SECURITY_QUESTION['QUESTION_ONE'];
        $this->securityAnswer = '';
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new MemberRepository();
        $this->cookie = new Cookie();
        $this->memberSessionRepository = new MemberSessionRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->email);
        unset($this->contactAddress);
        unset($this->securityQuestion);
        unset($this->securityAnswer);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
        unset($this->cookie);
        unset($this->memberSessionRepository);
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setContactAddress(string $contactAddress): void
    {
        $this->contactAddress = $contactAddress;
    }

    public function getContactAddress(): string
    {
        return $this->contactAddress;
    }

    public function setSecurityQuestion(int $securityQuestion) : void
    {
        $this->securityQuestion = in_array(
            $securityQuestion,
            self::SECURITY_QUESTION
        ) ? $securityQuestion : self::SECURITY_QUESTION['NULL'];
    }

    public function getSecurityQuestion(): int
    {
        return $this->securityQuestion;
    }

    public function setSecurityAnswer(string $securityAnswer): void
    {
        $this->securityAnswer = $securityAnswer;
    }

    public function getSecurityAnswer(): string
    {
        return $this->securityAnswer;
    }

    protected function getRepository(): MemberRepository
    {
        return $this->repository;
    }

    protected function getCookie() : Cookie
    {
        return $this->cookie;
    }

    protected function getMemberSessionRepository() : MemberSessionRepository
    {
        return $this->memberSessionRepository;
    }
    
    protected function isCellphoneExist() : bool
    {
        $filter = array();

        $filter['cellphone'] = $this->getCellphone();
        $sort = ['-updateTime'];

        list($memberList, $count) = $this->getRepository()->search($filter, $sort);
        unset($memberList);
        
        if (!empty($count)) {
            Core::setLastError(CELLPHONE_EXIST);
            return false;
        }

        return true;
    }

    protected function isEmailExist() : bool
    {
        $filter = array();

        $filter['email'] = $this->getEmail();
        $sort = ['-updateTime'];

        list($memberList, $count) = $this->getRepository()->search($filter, $sort);
        unset($memberList);
        
        if (!empty($count)) {
            Core::setLastError(EMAIL_EXIST);
            return false;
        }

        return true;
    }

    protected function isUserNameExist() : bool
    {
        $filter = array();

        $filter['userName'] = $this->getUserName();
        $sort = ['-updateTime'];

        list($memberList, $count) = $this->getRepository()->search($filter, $sort);
        unset($memberList);
        
        if (!empty($count)) {
            Core::setLastError(USER_NAME_EXIST);
            return false;
        }

        return true;
    }

    protected function addAction() : bool
    {
        return $this->isUserNameExist()
            && $this->isCellphoneExist()
            && $this->isEmailExist()
            && $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return $this->getRepository()->edit($this);
    }

    public function resetPassword() : bool
    {
        return $this->getRepository()->resetPassword($this);
    }

    public function updatePassword() : bool
    {
        return $this->getRepository()->updatePassword($this);
    }

    public function validateSecurity() : bool
    {
        return $this->getRepository()->validateSecurity($this);
    }
    
    //用户登录
    public function signIn() : bool
    {
        if ($this->getRepository()->signIn($this)) {
            $this->saveCookie() && $this->saveSession();
            return true;
        }

        return false;
    }

    //储存cookie
    protected function saveCookie() : bool
    {
        $cookie = $this->getCookie();
        $cookie->name = Core::$container->get('cookie.name');
        $cookie->value = $this->getId().':'.$this->generateIdentify();
        $cookie->expire = Core::$container->get('cookie.duration');

        return $cookie->add();
    }

    //储存session
    protected function saveSession() : bool
    {
        if ($this->getMemberSessionRepository()->save($this)) {
            Core::$container->set('member', $this->getMemberSessionRepository()->get($this->getId()));
            return true;
        }

        return false;
    }

    //用户退出
    public function signOut() : bool
    {
        return $this->clearCookie() && $this->clearSession();
    }

    //清除cookie
    protected function clearCookie() : bool
    {
        $cookie = $this->getCookie();
        $cookie->name = Core::$container->get('cookie.name');
        $cookie->value = 0;
        $cookie->expire = -1;

        return $cookie->add();
    }

    //清除session
    protected function clearSession() : bool
    {
        return $this->getMemberSessionRepository()->clear($this->getId());
    }

    //验证登录表示
    public function validateIdentify(string $identify) : bool
    {
        if ($this->getIdentify() != $identify) {
            $this->clearCookie();
            return false;
        }

         return true;
    }
}
