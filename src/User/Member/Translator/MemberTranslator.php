<?php
namespace Sdk\User\Member\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\User\Model\User;
use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Model\NullMember;

use Sdk\Common\Utils\Mask;
use Sdk\Common\Model\IEnableAble;

class MemberTranslator implements ITranslator
{
    const CARD_ID_MAX_LENGTH = 18;
    
    public function arrayToObject(array $expression, $member = null)
    {
        unset($member);
        unset($expression);
        return NullMember::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

   /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($member, array $keys = array())
    {
        if (!$member instanceof Member) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'userName',
                'realName',
                'cardId',
                'cellphone',
                'email',
                'contactAddress',
                'securityQuestion',
                'gender',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($member->getId());
        }
        if (in_array('userName', $keys)) {
            $expression['userName'] = $member->getUserName();
        }
        if (in_array('realName', $keys)) {
            $expression['realName'] = $member->getRealName();
        }
        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $member->getCellphone();
            $expression['cellphoneMask'] = Mask::mask($expression['cellphone'], 4, 3);
        }
        if (in_array('email', $keys)) {
            $expression['email'] = $member->getEmail();
        }
        if (in_array('cardId', $keys)) {
            $expression['cardId'] = $member->getCardId();
            $expression['cardIdMask'] = $this->cardIdMask($member->getCardId());
        }
        if (in_array('contactAddress', $keys)) {
            $expression['contactAddress'] = $member->getContactAddress();
        }
        if (in_array('gender', $keys)) {
            $expression['gender'] = [
                'id' => marmot_encode($member->getGender()),
                'name' => User::GENDER_CN[$member->getGender()]
            ];
        }
        if (in_array('securityQuestion', $keys)) {
            $expression['securityQuestion'] = [
                'id' => marmot_encode($member->getSecurityQuestion()),
                'name' => array_key_exists(
                    $member->getSecurityQuestion(),
                    Member::SECURITY_QUESTION_CN
                ) ? Member::SECURITY_QUESTION_CN[$member->getSecurityQuestion()] : ''
            ];
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($member->getStatus()),
                'name' => IEnableAble::STATUS_CN[$member->getStatus()],
                'type' => IEnableAble::STATUS_TAG_TYPE[$member->getStatus()]
            ];
        }
        
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $member->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H点i分', $member->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $member->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H点i分', $member->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $member->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H点i分', $member->getCreateTime());
        }

        return $expression;
    }

    private function cardIdMask(string $cartId) : string
    {
        return strlen($cartId) == self::CARD_ID_MAX_LENGTH ? Mask::mask($cartId, 4, 10) : Mask::mask($cartId, 4, 7);
    }
}
