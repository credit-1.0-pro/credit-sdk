<?php
namespace Sdk\User\Member\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\User\Member\Model\Member;

class MemberSessionTranslator implements IRestfulTranslator
{
    public function arrayToObject(array $expression, $member = null)
    {
        if ($member == null) {
            $member = new Member($expression['id']);
        }

        $member->setId($expression['id']);
        $member->setIdentify($expression['identify']);
        $member->setCellphone($expression['cellphone']);
        $member->setUserName($expression['userName']);
        $member->setRealName($expression['realName']);
        $member->setAvatar($expression['avatar']);
        
        return $member;
    }

    public function arrayToObjects(array $expression) : array
    {
        return [
            $expression['id'] => $this->arrayToObject($expression)
        ];
    }

    public function objectToArray($member, array $keys = array())
    {
        unset($keys);
        $expression = array();

        $expression['id'] = $member->getId();
        $expression['identify'] = $member->getIdentify();
        $expression['cellphone'] = $member->getCellphone();
        $expression['userName'] = $member->getUserName();
        $expression['realName'] = $member->getRealName();
        $expression['avatar'] = $member->getAvatar();
        
        return $expression;
    }
}
