<?php
namespace Sdk\User\Member\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Model\NullMember;
use Sdk\Common\Translator\RestfulTranslatorTrait;

class MemberRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $member = null)
    {
        return $this->translateToObject($expression, $member);
    }

    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $member = null)
    {
        if (empty($expression)) {
            return NullMember::getInstance();
        }

        if ($member == null) {
            $member = new Member();
        }

        $data = $expression['data'];
        
        $id = $data['id'];
        $member->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['userName'])) {
            $member->setUserName($attributes['userName']);
        }

        if (isset($attributes['realName'])) {
            $member->setRealName($attributes['realName']);
        }

        if (isset($attributes['cardId'])) {
            $member->setCardId($attributes['cardId']);
        }

        if (isset($attributes['contactAddress'])) {
            $member->setContactAddress($attributes['contactAddress']);
        }
        
        if (isset($attributes['securityQuestion'])) {
            $member->setSecurityQuestion($attributes['securityQuestion']);
        }
        
        if (isset($attributes['cellphone'])) {
            $member->setCellphone($attributes['cellphone']);
        }

        if (isset($attributes['email'])) {
            $member->setEmail($attributes['email']);
        }

        if (isset($attributes['gender'])) {
            $member->setGender($attributes['gender']);
        }

        if (isset($attributes['createTime'])) {
            $member->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['statusTime'])) {
            $member->setStatusTime($attributes['statusTime']);
        }

        if (isset($attributes['updateTime'])) {
            $member->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['status'])) {
            $member->setStatus($attributes['status']);
        }
        
        return $member;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($member, array $keys = array())
    {
        if (!$member instanceof Member) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'userName',
                'realName',
                'cardId',
                'cellphone',
                'email',
                'contactAddress',
                'securityQuestion',
                'securityAnswer',
                'password',
                'gender',
                'oldPassword',
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'members'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $member->getId();
        }

        $attributes = array();

        if (in_array('gender', $keys)) {
            $attributes['gender'] = $member->getGender();
        }
        if (in_array('userName', $keys)) {
            $attributes['userName'] = $member->getUserName();
        }
        if (in_array('realName', $keys)) {
            $attributes['realName'] = $member->getRealName();
        }
        if (in_array('cellphone', $keys)) {
            $attributes['cellphone'] = $member->getCellphone();
        }
        if (in_array('email', $keys)) {
            $attributes['email'] = $member->getEmail();
        }
        if (in_array('cardId', $keys)) {
            $attributes['cardId'] = $member->getCardId();
        }
        if (in_array('contactAddress', $keys)) {
            $attributes['contactAddress'] = $member->getContactAddress();
        }
        if (in_array('securityQuestion', $keys)) {
            $attributes['securityQuestion'] = $member->getSecurityQuestion();
        }
        if (in_array('securityAnswer', $keys)) {
            $attributes['securityAnswer'] = $member->getSecurityAnswer();
        }
        if (in_array('password', $keys)) {
            $attributes['password'] = $member->getPassword();
        }
        if (in_array('oldPassword', $keys)) {
            $attributes['oldPassword'] = $member->getOldPassword();
        }

        $expression['data']['attributes'] = $attributes;
        
        return $expression;
    }
}
