<?php
namespace Sdk\User\Crew\Model;

use Marmot\Core;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\Department\Model\Department;

use Sdk\User\Model\User;
use Sdk\User\Crew\Repository\CrewRepository;
use Sdk\User\Crew\Repository\CrewSessionRepository;

/**
 * @todo 修改待定
 * 1. 去掉注释
 * 2. 改为多态实现
 */
class Crew extends User
{
    /**
     * @var CATEGORY['NULL']  游客 0
     * @var CATEGORY['SUPERTUBE']  超级管理员 1
     * @var CATEGORY['PLATFORM_ADMINISTRATORS']  平台管理员 2
     * @var CATEGORY['USERGROUP_ADMINISTRATORS']  委办局管理员 3
     * @var CATEGORY['USERGROUP_OPERATION']  操作人员 4
     */
    const CATEGORY = array(
        'NULL' => 0,
        'SUPERTUBE' => 1,
        'PLATFORM_ADMINISTRATORS' => 2,
        'USERGROUP_ADMINISTRATORS' => 3,
        'USERGROUP_OPERATION' => 4
    );

    // const CATEGORY_MAPS = array( //预留多态，不要删
    //     self::CATEGORY['SUPERTUBE'] => 'Sdk\User\Crew\Model\SuperAdministrator',
    //     self::CATEGORY['PLATFORM_ADMINISTRATORS'] => 'Sdk\User\Crew\Model\PlatformAdministrator',
    //     self::CATEGORY['USERGROUP_ADMINISTRATORS'] => 'Sdk\User\Crew\Model\UserGroupAdministrator',
    //     self::CATEGORY['USERGROUP_OPERATION'] => 'Sdk\User\Crew\Model\Operator',
    //     self::CATEGORY['NULL'] => 'Sdk\User\Crew\Model\Guest',
    // );

    /**
     * [$userGroup 委办局]
     * @var [class]
     */
    private $userGroup;
    /**
     * [$purview 权限范围]
     * @var [array]
     */
    private $purview;
    /**
     * [$category 用户类型]
     * @var [int]
     */
    private $category;
    /**
     * [$department 科室]
     * @var [object]
     */
    private $department;
    
    private $repository;

    private $crewSessionRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->userGroup = new UserGroup();
        $this->purview = array();
        $this->category = self::CATEGORY['NULL'];
        $this->department = new Department();
        $this->repository = new CrewRepository();
        $this->crewSessionRepository = new CrewSessionRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->userGroup);
        unset($this->purview);
        unset($this->category);
        unset($this->department);
        unset($this->repository);
        unset($this->crewSessionRepository);
    }

    public function setUserGroup(UserGroup $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setPurview(array $purview) : void
    {
        $this->purview = $purview;
    }

    public function getPurview() : array
    {
        return $this->purview;
    }

    public function setCategory(int $category) : void
    {
        $this->category = in_array(
            $category,
            self::CATEGORY
        ) ? $category : self::CATEGORY['NULL'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setDepartment(Department $department) : void
    {
        $this->department = $department;
    }

    public function getDepartment() : Department
    {
        return $this->department;
    }

    protected function getRepository() : CrewRepository
    {
        return $this->repository;
    }

    protected function getCrewSessionRepository() : CrewSessionRepository
    {
        return $this->crewSessionRepository;
    }

    public function signOut() : bool
    {
        return $this->clearSession();
    }

    protected function clearSession() : bool
    {
        return $this->getCrewSessionRepository()->clear($this->getId());
    }

    // public static function create(int $category = 0) : Crew
    // {
    //     $crew = isset(self::CATEGORY_MAPS[$category]) ? self::CATEGORY_MAPS[$category] : '';

    //     return class_exists($crew) ? new $crew : Guest::getInstance();
    // }
    
    public function saveSession() : bool
    {
        if ($this->getCrewSessionRepository()->save($this)) {
            Core::$container->set('crewSession', $this->getCrewSessionRepository()->get($this->getId()));

            return true;
        }

        return false;
    }

    /**
     * 验证登录表示
     */
    public function validateIdentify(string $identify) : bool
    {
        if ($this->getIdentify() != $identify) {
            Core::$container->set('jwt', '');
            return false;
        }

        return true;
    }

    protected function addAction(): bool
    {
        return $this->isCellphoneExist() && $this->getRepository()->add($this);
    }

    protected function isCellphoneExist(): bool
    {
        $sort = ['-updateTime'];

        $filter['cellphone'] = $this->getCellphone();

        list($count, $crewList) = $this->getRepository()->scenario(
            CrewRepository::LIST_MODEL_UN
        )->search($filter, $sort);

        unset($crewList);

        if (!empty($count)) {
            Core::setLastError(CELLPHONE_EXIST);
            return false;
        }

        return true;
    }
}
