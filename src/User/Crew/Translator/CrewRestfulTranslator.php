<?php
namespace Sdk\User\Crew\Translator;

use Marmot\Core;

use Sdk\User\Translator\UserRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;
use Sdk\UserGroup\Department\Translator\DepartmentRestfulTranslator;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Model\Guest;

class CrewRestfulTranslator extends UserRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getDepartmentRestfulTranslator(): DepartmentRestfulTranslator
    {
        return new DepartmentRestfulTranslator();
    }

    public function arrayToObject(array $expression, $crew = null)
    {
        return $this->translateToObject($expression, $crew);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $crew = null)
    {
        if (empty($expression)) {
            return new Guest();
        }

        if ($crew == null) {
            $crew = new Crew();
        }

        $crew = parent::translateToObject($expression, $crew);

        $data =  $expression['data'];

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['purview'])) {
            $crew->setPurview($attributes['purview']);
        }

        if (isset($attributes['category'])) {
            $crew->setCategory($attributes['category']);
        }

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        
        if (isset($relationships['department']['data'])) {
            $department = $this->changeArrayFormat($relationships['department']['data']);
            $crew->setDepartment($this->getDepartmentRestfulTranslator()->arrayToObject($department));
        }

        if (isset($relationships['userGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['userGroup']['data']);
            
            $crew->setUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($userGroup));
        }
      
        return $crew;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($crew, array $keys = array())
    {
        $user = parent::objectToArray($crew, $keys);

        if (!$crew instanceof Crew) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'userGroup',
                'category',
                'department',
                'purview'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'crews'
            )
        );

        $expression['data']['attributes'] = $user['data']['attributes'];

        $attributes = array();
        
        if (in_array('category', $keys)) {
            $attributes['category'] = $crew->getCategory();
        }

        if (in_array('purview', $keys)) {
            $attributes['purview'] = $crew->getPurview();
        }

        $expression['data']['attributes'] = array_merge($attributes, $expression['data']['attributes']);

        if (in_array('userGroup', $keys)) {
            $expression['data']['relationships']['userGroup']['data'] = array(
                array(
                    'type' => 'userGroups',
                    'id' => $crew->getUserGroup()->getId()
                )
            );
        }

        if (in_array('department', $keys)) {
            $expression['data']['relationships']['department']['data'] = array(
                array(
                    'type' => 'departments',
                    'id' => $crew->getDepartment()->getId()
                )
            );
        }

        return $expression;
    }
}
