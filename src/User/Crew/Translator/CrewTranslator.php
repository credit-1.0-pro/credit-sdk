<?php
namespace Sdk\User\Crew\Translator;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Model\Guest;
use Sdk\User\Translator\UserTranslator;

class CrewTranslator extends UserTranslator
{
    const CATEGORY = array(
        0 => '游客',
        1 => '超级管理员',
        2 => '平台管理员',
        3 => '委办局管理员',
        4 => '操作用户'
    );

    public function arrayToObject(array $expression, $crew = null)
    {
        unset($crew);
        unset($expression);
        return new Guest();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($crew, array $keys = array())
    {
        $user = parent::objectToArray($crew, $keys);

        if (!$crew instanceof Crew) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'userGroup',
                'category',
                'department',
                'purview'
            );
        }

        $expression = array();

        if (in_array('userGroup', $keys)) {
            $expression['userGroup']['id'] = marmot_encode($crew->getUserGroup()->getId());
            $expression['userGroup']['name'] = $crew->getUserGroup()->getName();
        }
        if (in_array('category', $keys)) {
            $expression['category']['id'] = marmot_encode($crew->getCategory());
            $expression['category']['name'] = self::CATEGORY[$crew->getCategory()];
        }
        if (in_array('department', $keys)) {
            $expression['department'] = array();
            
            if ($crew->getDepartment()->getId() > 0) {
                $expression['department']['id'] = marmot_encode($crew->getDepartment()->getId());
                $expression['department']['name'] = $crew->getDepartment()->getName();
            }
        }
        if (in_array('purview', $keys)) {
            $expression['purview'] = $crew->getPurview();
        }
        
        $expression = array_merge($user, $expression);

        return $expression;
    }
}
