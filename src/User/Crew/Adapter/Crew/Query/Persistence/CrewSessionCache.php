<?php
namespace Sdk\User\Crew\Adapter\Crew\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class CrewSessionCache extends Cache
{
    public function __construct()
    {
        parent::__construct('crew.session');
    }
}
