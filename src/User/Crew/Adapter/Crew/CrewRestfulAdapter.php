<?php
namespace Sdk\User\Crew\Adapter\Crew;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Model\Guest;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use function GuzzleHttp\json_encode;

class CrewRestfulAdapter extends GuzzleAdapter implements ICrewAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'CREW_LIST'=>[
            'fields'=>[
                'crews'=>'realName,updateTime,status,userGroup,cellphone,category,department',
                'userGroups'=>'name',
                'departments' => 'name'
            ],
            'include'=>'userGroup,department'
        ],
        'CREW_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'userGroup,department'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CrewRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'users/crews';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getNullObject() : INull
    {
        return Guest::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            10 => CELLPHONE_NOT_EXIST,
            101 => [
                'category'=>CREW_CATEGORY_NOT_EXIST,
                'purview'=>CREW_PURVIEW_FORMAT_ERROR,
                'userGroupId'=>CREW_USER_GROUP_FORMAT_ERROR,
                'departmentId'=>CREW_DEPARTMENT_FORMAT_ERROR
            ],
            102 => [
                'status'=>STATUS_CAN_NOT_MODIFY,
                'crewStatus'=>USER_STATUS_DISABLE,
            ],
            103 => [
                'cellphone'=>CELLPHONE_EXIST
            ],
            501 => REAL_NAME_FORMAT_ERROR,
            502 => CELLPHONE_FORMAT_ERROR,
            503 => PASSWORD_FORMAT_ERROR,
            504 => CARD_ID_FORMAT_ERROR,
            505 => PASSWORD_INCORRECT,
            1001 => DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function addAction(Crew $crew) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $crew,
            array('realName', 'cellphone', 'password', 'cardId', 'userGroup', 'category', 'department', 'purview')
        );

        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($crew);
            return true;
        }

        return false;
    }

    protected function editAction(Crew $crew) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $crew,
            array('realName', 'cardId', 'userGroup', 'category', 'department', 'purview')
        );

        $this->patch(
            $this->getResource().'/'.$crew->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($crew);
            return true;
        }

        return false;
    }

    public function signIn(Crew $crew) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $crew,
            array(
                'userName','password'
            )
        );

        $this->post(
            $this->getResource().'/signIn',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($crew);
            return true;
        }

        return false;
    }
}
