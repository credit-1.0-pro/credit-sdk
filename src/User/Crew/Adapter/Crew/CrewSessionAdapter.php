<?php
namespace Sdk\User\Crew\Adapter\Crew;

use Marmot\Core;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Model\Guest;
use Sdk\User\Crew\Translator\CrewSessionTranslator;
use Sdk\User\Crew\Adapter\Crew\Query\CrewSessionDataCacheQuery;

class CrewSessionAdapter
{
    private $session;

    private $translator;

    public function __construct()
    {
        $this->session = new CrewSessionDataCacheQuery();
        $this->translator = new CrewSessionTranslator();
    }

    protected function getSession() : CrewSessionDataCacheQuery
    {
        return $this->session;
    }

    protected function getTranslator() : CrewSessionTranslator
    {
        return $this->translator;
    }

    protected function getTTL() : int
    {
        return Core::$container->has('cache.session.ttl') ? Core::$container->get('cache.session.ttl') : 300;
    }

    public function get(int $id)
    {
        $info = $this->getSession()->get($id);

        return empty($info) ? Guest::getInstance(): $this->getTranslator()->arrayToObject($info);
    }

    public function save(Crew $crew) : bool
    {
        $info = $this->getTranslator()->objectToArray($crew);

        return $this->getSession()->save($crew->getId(), $info, $this->getTTL());
    }

    public function del(int $id) : bool
    {
        return $this->getSession()->del($id);
    }
}
