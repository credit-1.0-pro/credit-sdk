<?php
namespace Sdk\User\Crew\Adapter\Crew;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\User\Crew\Model\Crew;

interface ICrewAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperateAbleAdapter, IEnableAbleAdapter
{
    public function signIn(Crew $crew) : bool;
}
