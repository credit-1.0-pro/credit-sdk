<?php
namespace Sdk\User\Crew\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Sdk\Common\Repository\OperateAbleRepositoryTrait;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Adapter\Crew\ICrewAdapter;
use Sdk\User\Crew\Adapter\Crew\CrewMockAdapter;
use Sdk\User\Crew\Adapter\Crew\CrewRestfulAdapter;

class CrewRepository extends Repository implements ICrewAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        EnableAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'CREW_LIST';
    const FETCH_ONE_MODEL_UN = 'CREW_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CrewRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new CrewMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function signIn(Crew $crew) : bool
    {
        return $this->getAdapter()->signIn($crew);
    }
}
