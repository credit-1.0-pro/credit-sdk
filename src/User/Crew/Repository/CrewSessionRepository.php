<?php
namespace Sdk\User\Crew\Repository;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Adapter\Crew\CrewSessionAdapter;

class CrewSessionRepository
{
    private $adapter;
    
    public function __construct()
    {
        $this->adapter = new CrewSessionAdapter();
    }

    protected function getAdapter() : CrewSessionAdapter
    {
        return $this->adapter;
    }

    public function save(Crew $crew) : bool
    {
        return $this->getAdapter()->save($crew);
    }
    
    public function get(int $id)
    {
        return $this->getAdapter()->get($id);
    }

    public function clear(int $id) : bool
    {
        return $this->getAdapter()->del($id);
    }
}
