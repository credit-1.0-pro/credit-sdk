<?php
namespace Sdk\User\Crew\Command\Crew;

use Marmot\Interfaces\ICommand;

class SignInCrewCommand implements ICommand
{
    public $userName;

    public $password;

    public $id;

    public function __construct(
        string $userName,
        string $password,
        int $id = 0
    ) {
        $this->userName = $userName;
        $this->password = $password;
        $this->id = $id;
    }
}
