<?php
namespace Sdk\User\Crew\Command\Crew;

use Marmot\Interfaces\ICommand;

class AddCrewCommand implements ICommand
{
    public $realName;

    public $cellphone;

    public $password;

    public $cardId;

    public $userGroupId;

    public $category;

    public $departmentId;
    
    public $purview;

    public $id;

    public function __construct(
        string $realName,
        string $cellphone,
        string $password,
        string $cardId,
        int $userGroupId = 0,
        int $category = 0,
        int $departmentId = 0,
        array $purview = array(),
        int $id = 0
    ) {
        $this->realName = $realName;
        $this->cellphone = $cellphone;
        $this->password = $password;
        $this->cardId = $cardId;
        $this->userGroupId = $userGroupId;
        $this->category = $category;
        $this->departmentId = $departmentId;
        $this->purview = $purview;
        $this->id = $id;
    }
}
