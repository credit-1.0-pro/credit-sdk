<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Command\Crew\SignOutCrewCommand;

class SignOutCrewCommandHandler implements ICommandHandler
{
    private $crew;

    public function __construct()
    {
        $this->crew = Core::$container->get('crew');
    }

    public function __destruct()
    {
        unset($this->crew);
    }

    protected function getCrew()
    {
        return $this->crew;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof SignOutCrewCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->getCrew();

        return $crew->signOut();
    }
}
