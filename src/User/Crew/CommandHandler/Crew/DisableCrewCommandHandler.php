<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Sdk\Common\Model\IEnableAble;
use Sdk\Common\CommandHandler\DisableCommandHandler;

class DisableCrewCommandHandler extends DisableCommandHandler
{
    use CrewCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchCrew($id);
    }
}
