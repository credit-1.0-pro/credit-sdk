<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Repository\CrewRepository;
use Sdk\User\Crew\Command\Crew\EditCrewCommand;

use Sdk\UserGroup\Department\Repository\DepartmentRepository;
use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;

class EditCrewCommandHandler implements ICommandHandler
{
    private $repository;

    private $userGroupRepository;

    public function __construct()
    {
        $this->repository = new CrewRepository();
        $this->userGroupRepository = new UserGroupRepository();
        $this->departmentRepository = new DepartmentRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->userGroupRepository);
        unset($this->departmentRepository);
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getDepartmentRepository() : DepartmentRepository
    {
        return $this->departmentRepository;
    }

    protected function getRepository() : CrewRepository
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof EditCrewCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->getRepository()->fetchOne($command->id);

        $crew->setRealName($command->realName);
        $crew->setCardId($command->cardId);
        $crew->setCategory($command->category);
        $crew->setPurview($command->purview);
        $crew->setUserGroup(
            $this->getUserGroupRepository()->fetchOne($command->userGroupId)
        );
        $crew->setDepartment(
            $this->getDepartmentRepository()->fetchOne($command->departmentId)
        );
        
        return $crew->edit();
    }
}
