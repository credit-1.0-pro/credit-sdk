<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Repository\CrewRepository;

trait CrewCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new CrewRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : CrewRepository
    {
        return $this->repository;
    }
    
    protected function fetchCrew(int $id) : Crew
    {
        return $this->getRepository()->fetchOne($id);
    }
}
