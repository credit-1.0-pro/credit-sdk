<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Sdk\Common\Model\IEnableAble;
use Sdk\Common\CommandHandler\EnableCommandHandler;

class EnableCrewCommandHandler extends EnableCommandHandler
{
    use CrewCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchCrew($id);
    }
}
