<?php
/**
 * 默认使用常量0/默认状态
 */
define('STATUS_NORMAL', 0);
/**
 * 默认使用常量0
 */
define('CONSTANT', 0);
/**
 * 公用常量
 */
define('PAGE', 1);
/**
 * 公用常量
 */
define('SIZE', 10);
/**
 * 公用最大常量
 */
define('COMMON_SIZES', 1000);

/**
 * 删除状态
 */
define('STATUS_DELETE', -2);
