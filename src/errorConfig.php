<?php
/**
    * 1-99 系统错误规范
    * 100-200 通用错误规范
    * 201-300 OA通用错误规范
    * 301-400 门户通用错误规范
    * 401-500 共享通用错误规范
    * 501-600 用户通用错误规范
    * 601-10000 预留错误规范
    * 10001-10050 委办局错误规范
    * 10051-10100 科室错误规范
    * 10101-10200 员工错误规范
    * 10201-10300 门户网用户错误规范
    * 10301-10400 新闻分类错误规范
    * 10401-10500 新闻管理错误规范
    * 10501-10600 目录管理错误规范
    * 10601-10700 规则管理错误规范
    * 10701-10800 资源目录数据管理错误规范
 */

// 1-99 系统错误规范----------------------------------------------------------------

/**
 * csrf 验证失效
 */
define('CSRF_VERIFY_FAILURE', 15);
/**
 * 滑动验证失败
 */
define('AFS_VERIFY_FAILURE', 16);
/**
 * 用户未登录
 */
define('NEED_SIGNIN', 17);
/**
 * 短信发送太频繁
 */
define('SMS_SEND_TOO_QUICK', 18);
/**
 * 哈希无效
 */
define('HASH_INVALID', 19);
/**
 * 用户未登录
 */
define('LOGIN_EXPIRED', 20);
/**
 * 权限未定义
 */
define('PURVIEW_UNDEFINED', 21);
/**
 * 验证码不正确
 */
define('CAPTCHA_ERROR', 22);

// 100-200 通用错误规范----------------------------------------------------------------
/**
 * 数据不能重复
 */
define('PARAMETER_IS_UNIQUE', 100);
/**
 * 数据格式不正确
 */
define('PARAMETER_FORMAT_ERROR', 101);
/**
 * 参数为空
 */
define('PARAMETER_IS_EMPTY', 102);
/**
 * 状态不能操作
 */
define('STATUS_CAN_NOT_MODIFY', 103);
/**
 * 审核状态不能操作
 */
define('APPLY_STATUE_CAN_NOT_MODIFY', 104);

/**
 * 图片格式不正确
 */
define('IMAGE_FORMAT_ERROR', 121);
/**
 * 附件格式不正确
 */
define('ATTACHMENT_FORMAT_ERROR', 122);
/**
 * 姓名格式不正确
 */
define('REAL_NAME_FORMAT_ERROR', 123);
/**
 * 手机号格式不正确
 */
define('CELLPHONE_FORMAT_ERROR', 124);
/**
 * 身份证号格式不正确
 */
define('CARD_ID_FORMAT_ERROR', 125);
/**
 * 名称格式不正确
 */
define('NAME_FORMAT_ERROR', 126);
/**
 * 标题格式不正确
 */
define('TITLE_FORMAT_ERROR', 127);
/**
 * 描述格式不正确
 */
define('DESCRIPTION_FORMAT_ERROR', 128);
/**
 * 原因格式不正确
 */
define('REASON_FORMAT_ERROR', 129);
/**
 * 来源格式不正确
 */
define('SOURCE_FORMAT_ERROR', 130);
/**
 * 状态格式不正确
 */
define('STATUS_FORMAT_ERROR', 131);
/**
 * 状态非启用
 */
define('STATUS_NOT_ENABLE', 132);
/**
 * 状态非禁用
 */
define('STATUS_NOT_DISABLE', 133);
/**
 * 状态非置顶
 */
define('STATUS_NOT_TOP', 134);
/**
 * 状态已置顶
 */
define('STATUS_NOT_CANCEL_TOP', 135);
/**
 * 统一社会信用代码格式不正确
 */
define('UNIFIED_SOCIAL_CREDIT_CODE_FORMAT_ERROR', 136);
/**
 * 置顶状态格式不正确
 */
define('STICK_FORMAT_ERROR', 137);

// 201-300 OA通用错误规范----------------------------------------------------------------
/**
 * jwt-token 为空
 */
define('JWT_TOKEN_EMPTY', 201);
/**
 * jwt-token 已过期
 */
define('JWT_TOKEN_OVERDUE', 202);
/**
 * jwt-token 验证失败
 */
define('JWT_TOKEN_ERROR', 203);
/**
 * 文件为空
 */
define('FILE_IS_EMPTY', 204);
/**
 * 文件格式不正确
 */
define('FILE_FORMAT_ERROR', 205);
/**
 * 文件大小超出限制
 */
define('FILE_SIZE_FORMAT_ERROR', 206);
/**
 * 文件后缀不支持
 */
define('FILE_EXTENSION_FORMAT_ERROR', 207);
/**
 * 文件重复上传
 */
define('FILE_IS_UNIQUE', 208);

// 501-600 用户通用错误规范----------------------------------------------------------------

/**
 * 密码格式不正确
 */
define('PASSWORD_FORMAT_ERROR', 501);
/**
 * 旧密码不正确
 */
define('OLD_PASSWORD_INCORRECT', 502);
/**
 * 密码不正确
 */
define('PASSWORD_INCORRECT', 503);
/**
 * 确认密码与密码不一致
 */
define('PASSWORD_INCONSISTENCY', 504);
/**
 * 手机号已存在
 */
define('CELLPHONE_EXIST', 505);
/**
 * 手机号不存在
 */
define('CELLPHONE_NOT_EXIST', 506);
/**
 * 账号已被禁用
 */
define('USER_STATUS_DISABLE', 507);
/**
 * 审核人格式不正确
 */
define('APPLY_CREW_FORMAT_ERROR', 508);
/**
 * 发布人格式不正确
 */
define('CREW_ID_FORMAT_ERROR', 509);

// 10001-10050 委办局管理错误规范----------------------------------------------------------------
/**
 * 委办局名称格式不正确
 */
define('USER_GROUP_NAME_FORMAT_ERROR', 10001);
/**
 * 委办局名称已经存在
 */
define('USER_GROUP_NAME_IS_UNIQUE', 10002);
/**
 * 委办局简称格式不正确
 */
define('USER_GROUP_SHORT_NAME_FORMAT_ERROR', 10003);
/**
 * 所属行政区域格式不正确
 */
define('USER_GROUP_ADMINISTRATIVE_AREA_FORMAT_ERROR', 10004);

// 10051-10100 科室管理错误规范----------------------------------------------------------------
/**
 * 科室名称格式不正确
 */
define('DEPARTMENT_NAME_FORMAT_ERROR', 10051);
/**
 * 科室名称已经存在
 */
define('DEPARTMENT_NAME_IS_UNIQUE', 10052);
/**
 * 所属委办局为空
 */
define('DEPARTMENT_USER_GROUP_IS_EMPTY', 10053);
/**
 * 所属委办局格式不正确
 */
define('DEPARTMENT_USER_GROUP_FORMAT_ERROR', 10054);

// 10101-10200 员工管理错误规范----------------------------------------------------------------
/**
 * 员工类型不存在
 */
define('CREW_CATEGORY_NOT_EXIST', 10101);
/**
 * 员工权限范围格式不正确
 */
define('CREW_PURVIEW_FORMAT_ERROR', 10102);
/**
 * 员工类型权限不足
 */
define('CREW_PURVIEW_HIERARCHY_FORMAT_ERROR', 10103);
/**
 * 该所属科室不属于该所属委办局
 */
define('DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP', 10104);
/**
 * 所属委办局格式不正确
 */
define('CREW_USER_GROUP_FORMAT_ERROR', 10105);
/**
 * 所属科室格式不正确
 */
define('CREW_DEPARTMENT_FORMAT_ERROR', 10106);

// 10201-10300 前台用户管理模块错误规范----------------------------------------------------------------
/**
 * 邮箱格式不正确
 */
define('EMAIL_FORMAT_ERROR', 10201);
/**
 * 用户名不存在
 */
define('USER_NAME_NOT_EXIT', 10202);
/**
 * 联系地址格式不正确
 */
define('CONTACT_ADDRESS_FORMAT_ERROR', 10203);
/**
 * 密保问题格式不正确
 */
define('SECURITY_QUESTION_FORMAT_ERROR', 10204);
/**
 * 密保答案格式不正确
 */
define('SECURITY_ANSWER_FORMAT_ERROR', 10205);
/**
 * 性别格式不正确
 */
define('GENDER_FORMAT_ERROR', 10206);
/**
 * 用户名已经存在
 */
define('USER_NAME_EXIST', 10207);
/**
 * 邮箱已经存在
 */
define('EMAIL_EXIST', 10208);
/**
 * 用户名格式不正确
 */
define('USER_NAME_FORMAT_ERROR', 10209);
/**
 * 密保答案不正确
 */
define('SECURITY_ANSWER_INCORRECT', 10210);

// 10301-10400 新闻分类管理模块错误规范----------------------------------------------------------------
/**
 * 分类名称格式不正确
 */
define('NEWS_CATEGORY_NAME_FORMAT_ERROR', 10301);
/**
 * 分类名称已经存在
 */
define('NEWS_CATEGORY_NAME_IS_UNIQUE', 10302);
/**
 * 分类等级格式不正确
 */
define('NEWS_CATEGORY_GRADE_FORMAT_ERROR', 10303);
/**
 * 一级分类格式不正确
 */
define('NEWS_CATEGORY_PARENT_CATEGORY_FORMAT_ERROR', 10304);
/**
 * 二级分类格式不正确
 */
define('NEWS_CATEGORY_CATEGORY_FORMAT_ERROR', 10305);
/**
 * 一级分类不存在
 */
define('NEWS_CATEGORY_PARENT_CATEGORY_NOT_EXIST', 10306);
/**
 * 二级分类不存在
 */
define('NEWS_CATEGORY_CATEGORY_NOT_EXIST', 10307);
/**
 * 二级分类不属于一级分类
 */
define('CATEGORY_NOT_BELONG_TO_THE_PARENT_CATEGORY', 10308);

// 10401-10500 新闻管理模块错误规范----------------------------------------------------------------
/**
 * 新闻内容格式不正确
 */
define('NEWS_CONTENT_FORMAT_ERROR', 10401);
/**
 * 新闻分类格式不正确
 */
define('NEWS_TYPE_FORMAT_ERROR', 10402);
/**
 * 新闻分类不存在
 */
define('NEWS_TYPE_NOT_EXIT', 10403);
/**
 * 公开类型格式不正确
 */
define('DIMENSION_FORMAT_ERROR', 10404);
/**
 * 首页展示状态格式不正确
 */
define('HOME_PAGE_SHOW_STATUS_FORMAT_ERROR', 10405);
/**
 * 轮播状态格式不正确
 */
define('BANNER_STATUS_FORMAT_ERROR', 10406);
/**
 * 图片格式不正确
 */
define('BANNER_IMAGE_FORMAT_ERROR', 10407);
/**
 * 发布时间格式不正确
 */
define('RELEASE_TIME_FORMAT_ERROR', 10408);

// 10501-10600 目录管理模块错误规范----------------------------------------------------------------
/**
 * 目录名称格式不正确
 */
define('TEMPLATE_NAME_FORMAT_ERROR', 10501);
/**
 * 目录标识格式不正确
 */
define('TEMPLATE_IDENTIFY_FORMAT_ERROR', 10502);
/**
 * 主体类别格式不正确
 */
define('TEMPLATE_SUBJECT_CATEGORY_FORMAT_ERROR', 10503);
/**
 * 公开范围格式不正确
 */
define('TEMPLATE_DIMENSION_FORMAT_ERROR', 10504);
/**
 * 更新频率格式不正确
 */
define('TEMPLATE_EXCHANGE_FREQUENCY_FORMAT_ERROR', 10505);
/**
 * 信息分类格式不正确
 */
define('TEMPLATE_INFO_CLASSIFY_FORMAT_ERROR', 10506);
/**
 * 信息类别格式不正确
 */
define('TEMPLATE_INFO_CATEGORY_FORMAT_ERROR', 10507);
/**
 * 目录描述格式不正确
 */
define('TEMPLATE_DESCRIPTION_FORMAT_ERROR', 10508);
/**
 * 模板信息格式不正确
 */
define('TEMPLATE_ITEMS_FORMAT_ERROR', 10509);
/**
 * 来源委办局格式不正确
 */
define('TEMPLATE_SOURCE_UNITS_FORMAT_ERROR', 10510);
/**
 * 版本描述格式不正确
 */
define('TEMPLATE_VERSION_DESCRIPTION_FORMAT_ERROR', 10511);
/**
 * 版本记录格式不正确
 */
define('TEMPLATE_VERSION_FORMAT_ERROR', 10512);
/**
 * 信息项名称格式不正确
 */
define('TEMPLATE_ITEMS_NAME_FORMAT_ERROR', 10513);
/**
 * 数据标识格式不正确
 */
define('TEMPLATE_ITEMS_IDENTIFY_FORMAT_ERROR', 10514);
/**
 * 主体名称不存在
 */
define('TEMPLATE_SUBJECT_NAME_NOT_EXIT', 10515);
/**
 * 证件号码不存在
 */
define('TEMPLATE_CARD_ID_NOT_EXIT', 10516);
/**
 * 统一社会信用代码不存在
 */
define('TEMPLATE_UNIFIED_SOCIAL_CREDIT_CODE_NOT_EXIT', 10517);
/**
 * 信息项数据类型格式不正确
 */
define('TEMPLATE_ITEMS_TYPE_FORMAT_ERROR', 10518);
/**
 * 信息项数据长度格式不正确
 */
define('TEMPLATE_ITEMS_LENGTH_FORMAT_ERROR', 10519);
/**
 * 信息项可选范围格式不正确
 */
define('TEMPLATE_ITEMS_OPTIONS_FORMAT_ERROR', 10520);
/**
 * 信息项是否必填格式不正确
 */
define('TEMPLATE_IS_NECESSARY_FORMAT_ERROR', 10521);
/**
 * 信息项是否脱敏格式不正确
 */
define('TEMPLATE_IS_MASKED_FORMAT_ERROR', 10522);
/**
 * 信息项脱敏规则格式不正确
 */
define('TEMPLATE_MASKED_RULE_FORMAT_ERROR', 10523);
/**
 * 信息项备注格式不正确
 */
define('TEMPLATE_REMARKS_FORMAT_ERROR', 10524);
/**
 * 版本记录不存在
 */
define('TEMPLATE_VERSION_NOT_EXIT', 10525);
/**
 * 目录标识已经存在
 */
define('TEMPLATE_IDENTIFY_IS_UNIQUE', 10526);
/**
 * 信息项公开范围格式不正确
 */
define('TEMPLATE_ITEMS_DIMENSION_FORMAT_ERROR', 10527);
/**
 * 信息项名称在模板信息中必须唯一
 */
define('TEMPLATE_ITEMS_NAME_IS_UNIQUE', 10528);
/**
 * 数据标识在模板信息中必须唯一
 */
define('TEMPLATE_ITEMS_IDENTIFY_IS_UNIQUE', 10529);

// 10601-10700 规则管理模块错误规范----------------------------------------------------------------
/**
 * 版本描述格式不正确
 */
define('RULE_VERSION_DESCRIPTION_FORMAT_ERROR', 10601);
/**
 * 规则格式不正确
 */
define('RULE_RULES_FORMAT_ERROR', 10602);
/**
 * 目录格式不正确
 */
define('RULE_TEMPLATE_FORMAT_ERROR', 10603);
/**
 * 规则版本记录格式不正确
 */
define('RULE_VERSION_FORMAT_ERROR', 10604);
/**
 * 规则名称不存在
 */
define('RULE_RULES_NAME_NOT_EXIT', 10605);
/**
 * 补全规则格式不正确
 */
define('RULE_COMPLETION_RULES_FORMAT_ERROR', 10606);
/**
 * 补全数量不正确
 */
define('RULE_COMPLETION_RULES_COUNT_ERROR', 10607);
/**
 * 补全信息项来源格式不正确
 */
define('RULE_COMPLETION_RULES_TEMPLATE_FORMAT_ERROR', 10608);
/**
 * 补全依据格式不正确
 */
define('RULE_COMPLETION_RULES_BASE_FORMAT_ERROR', 10609);
/**
 * 补全信息项格式不正确
 */
define('RULE_COMPLETION_RULES_TEMPLATE_ITEM_FORMAT_ERROR', 10610);
/**
 * 比对规则格式不正确
 */
define('RULE_COMPARISON_RULES_FORMAT_ERROR', 10611);
/**
 * 比对数量不正确
 */
define('RULE_COMPARISON_RULES_COUNT_ERROR', 10612);
/**
 * 比对信息项来源格式不正确
 */
define('RULE_COMPARISON_RULES_TEMPLATE_FORMAT_ERROR', 10613);
/**
 * 比对依据格式不正确
 */
define('RULE_COMPARISON_RULES_BASE_FORMAT_ERROR', 10614);
/**
 * 比对信息项格式不正确
 */
define('RULE_COMPARISON_RULES_TEMPLATE_ITEM_FORMAT_ERROR', 10615);
/**
 * 去重规则格式不正确
 */
define('RULE_DE_DUPLICATION_RULES_FORMAT_ERROR', 10616);
/**
 * 去重信息项格式不正确
 */
define('RULE_DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR', 10617);
/**
 * 去重结果处理格式不正确
 */
define('RULE_DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR', 10618);
/**
 * 目录待补全信息项不存在
 */
define('RULE_COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST', 10619);
/**
 * 补全信息项来源不存在
 */
define('RULE_COMPLETION_RULES_TEMPLATE_NOT_EXIST', 10620);
/**
 * 补全信息项不存在
 */
define('RULE_COMPLETION_RULES_TEMPLATE_ITEM_NOT_EXIST', 10621);
/**
 * 目录待比对信息项不存在
 */
define('RULE_COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST', 10622);
/**
 * 比对信息项来源不存在
 */
define('RULE_COMPARISON_RULES_TEMPLATE_NOT_EXIST', 10623);
/**
 * 比对信息项不存在
 */
define('RULE_COMPARISON_RULES_TEMPLATE_ITEM_NOT_EXIST', 10624);
/**
 * 比对项为企业名称,比对依据不能选择企业名称信息项.
 */
define('RULE_COMPARISON_RULES_BASE_CANNOT_NAME', 10625);
/**
 * 比对项为主体代码(统一社会信用代码/身份证号),那比对依据不能选择主体代码(统一社会信用代码/身份证号)信息项.
 */
define('RULE_COMPARISON_RULES_BASE_CANNOT_IDENTIFY', 10626);
/**
 * 去重信息项不存在.
 */
define('RULE_DE_DUPLICATION_RULES_ITEMS_NOT_EXIST', 10627);
/**
 * 目标信息项类型不存在.
 */
define('RULE_TRANSFORMATION_TEMPLATE_ITEM_TYPE_NOT_EXIST', 10628);
/**
 * 类型不能转换.
 */
define('RULE_TYPE_CANNOT_TRANSFORMATION', 10629);
/**
 * 长度不能转换.
 */
define('RULE_LENGTH_CANNOT_TRANSFORMATION', 10630);
/**
 * 公开范围不能转换.
 */
define('RULE_DIMENSION_CANNOT_TRANSFORMATION', 10631);
/**
 * 脱敏状态不能转换.
 */
define('RULE_IS_MASKED_CANNOT_TRANSFORMATION', 10632);
/**
 * 脱敏范围不能转换.
 */
define('RULE_MASK_RULE_CANNOT_TRANSFORMATION', 10633);
/**
 * 当比对/补全信息项来源为自然人,依据必须包含身份证号.
 */
define('RULE_RULES_TEMPLATE_ZRR_NOT_INCLUDE_IDENTIFY', 10634);
/**
 * 目录不存在.
 */
define('RULE_TEMPLATE_NOT_EXIST', 10635);
/**
 * 目录版本记录不存在.
 */
define('RULE_TEMPLATE_VERSION_NOT_EXIST', 10636);
/**
 * 规则版本记录不存在.
 */
define('RULE_VERSION_NOT_EXIST', 10637);
/**
 * 规则已经存在.
 */
define('RULE_IS_UNIQUE', 10638);

// 10701-10800 资源目录数据管理模块错误规范----------------------------------------------------------------
/**
 * 有效期限格式不正确
 */
define('RESOURCE_CATALOG_DATA_EXPIRATION_DATE_FORMAT_ERROR', 10701);
/**
 * 目录不存在
 */
define('RESOURCE_CATALOG_DATA_TEMPLATE_NOT_EXIT', 10702);
/**
 * 目录格式不正确
 */
define('RESOURCE_CATALOG_DATA_TEMPLATE_FORMAT_ERROR', 10703);
/**
 * 资源目录数据格式不正确
 */
define('RESOURCE_CATALOG_DATA_ITEMS_DATA_FORMAT_ERROR', 10704);
/**
 * 主体类别格式不正确
 */
define('RESOURCE_CATALOG_DATA_SUBJECT_CATEGORY_FORMAT_ERROR', 10705);
/**
 * 资源目录数据已经存在
 */
define('RESOURCE_CATALOG_DATA_IS_UNIQUE', 10706);
