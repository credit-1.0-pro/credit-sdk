<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IOperateAble;

interface IOperateAbleAdapter
{
    public function add(IOperateAble $operatAbleObject) : bool;

    public function edit(IOperateAble $operatAbleObject) : bool;
}
