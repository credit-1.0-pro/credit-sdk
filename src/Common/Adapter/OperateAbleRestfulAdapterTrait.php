<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IOperateAble;

trait OperateAbleRestfulAdapterTrait
{
    abstract protected function addAction(IOperateAble $operateAbleObject) : bool;
    abstract protected function editAction(IOperateAble $operateAbleObject) : bool;

    public function add(IOperateAble $operateAbleObject) : bool
    {
        return $this->addAction($operateAbleObject);
    }

    public function edit(IOperateAble $operateAbleObject) : bool
    {
        return $this->editAction($operateAbleObject);
    }
}
