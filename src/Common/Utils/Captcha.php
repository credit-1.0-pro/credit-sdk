<?php
namespace Sdk\Common\Utils;

use Sdk\Common\Persistence\UtilsSession;

use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;

use Marmot\Core;

class Captcha
{
    /**
     * 图片验证码
     * @link https://github.com/Gregwar/Captcha
     *
     * phraseBuilder 里面的数字代表出现几个字符
     * setMaxBehindLines 图片数字后面的遮盖线
     * setMaxFrontLines 图片数字前面的遮盖线
     * output 中的数字代表图片质量
     */
    public function render()
    {
        $phraseBuilder = $this->getPhraseBuilder();
       
        $captchaBuilder = $this->getCaptchaBuilder($phraseBuilder);
        $builder = $this->getBuildPhrase($captchaBuilder);
       
        $this->phraseOutPut($builder);
    }
    /**
     * 初始化builder
     */
    protected function getPhraseBuilder()
    {
        $phraseBuilder = new PhraseBuilder(4);

        return $phraseBuilder;
    }

    protected function getCaptchaBuilder($phraseBuilder)
    {
        $captchaBuilder = new CaptchaBuilder(null, $phraseBuilder);

        return $captchaBuilder;
    }
    /**
     * 生成一个验证码
     */
    protected function getBuildPhrase($builder)
    {
        $builder->setMaxBehindLines(0);
        $builder->setMaxFrontLines(0);
        $builder->build();
        
        self::storePhrase($builder->getPhrase());

        return $builder;
    }
    /**
     * 将验证码以图片形式输出
     * @codeCoverageIgnore
     * @todo header方法未封装，暂时不写单元测试
     */
    protected function phraseOutPut($builder)
    {
        header('Content-type: image/jpeg');
        $builder->output(100);
    }

    private function storePhrase(string $phrase) : bool
    {
        $session = new UtilsSession();
        return $session->save('captcha', $phrase);
    }

    public function validate(string $phrase)
    {
        $session = new UtilsSession();
        
        if ($phrase != $session->get('captcha')) {
            Core::setLastError(CAPTCHA_ERROR);
            return false;
        }
        return true;
    }
}
