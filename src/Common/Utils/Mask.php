<?php
namespace Sdk\Common\Utils;

class Mask
{
    public static function mask(
        string $string,
        int $start,
        $length = 0
    ) {
        return self::substrReplaceCn($string, '*', $start, $length);
    }
    
    /**
     * @SuppressWarnings(PHPMD.ElseExpression)
     */
    public static function substrReplaceCn($string, $repalce = '*', $start = 0, $len = 0)
    {
        $count = mb_strlen($string, 'UTF-8');

        if (!$count) {
            return $string;
        }

        if ($len == 0) {
            $end = $count;
        } else {
            $end = $start + $len;
        }

        $byte = 0;
        $returnString = '';
        
        while ($byte < $count) {
            $tmpString = mb_substr($string, $byte, 1, 'UTF-8');
            if ($start <= $byte && $byte < $end) {
                $returnString .= $repalce;
            } else {
                $returnString .= $tmpString;
            }
            $byte ++;
        }
        
        return $returnString;
    }
}
