<?php
namespace Sdk\Common\Utils;

use Sdk\Common\Persistence\UtilsSession;

use Marmot\Framework\Classes\Filter;

/**
 * @codeCoverageIgnore
 */
class Cryptojs
{
    use RandomTokenTrait;

    public function generateHash() : string
    {
        $hash = self::random(50);
        self::storeHash($hash);
        return $hash;
    }

    private function storeHash(string $hash) : bool
    {
        $session = new UtilsSession();
        return $session->save('cryptojs_hash', $hash);
    }

    /**
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function decrypt(string $jsonString)
    {
        $session = new UtilsSession();
        $hash = $session->get('cryptojs_hash');
        
        $jsonString = stripslashes(Filter::dhtmlspecialchars($jsonString));
        $jsondata = json_decode($jsonString, true);
        try {
            $salt = hex2bin($jsondata["s"]);
            $iv  = hex2bin($jsondata["iv"]);
        } catch (Throwable $e) {
            return null;
        }
       
        $ct = base64_decode($jsondata["ct"]);
        $concatedPassphrase = $hash.$salt;

        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
            $result .= $md5[$i];
        }
        
        $key = substr($result, 0, 32);
        
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        
        return json_decode($data, true);
    }
}
