<?php
namespace Sdk\Common\Persistence;

use Marmot\Framework\Classes\Session;

class UtilsSession extends Session
{
    public function __construct()
    {
        parent::__construct('utils');
    }
}
