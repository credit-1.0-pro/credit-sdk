<?php
namespace Sdk\Common\WidgetRules;

use Marmot\Core;

use Respect\Validation\Validator as V;

use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\ITopAble;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class WidgetRules
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function formatNumeric($data, $pointer) : bool
    {
        if (!V::intVal()->validate($data)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }
        return  true;
    }

    public function formatString($data, string $pointer = '') : bool
    {
        if (is_string($data)) {
            return true;
        }
        Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
        return false;
    }

    public function formatArray($data, string $pointer = '') : bool
    {
        if (is_array($data)) {
            return true;
        }
        Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
        return false;
    }

    const TITLE_MIN_LENGTH = 5;
    const TITLE_MAX_LENGTH = 80;

    public function title($data, string $pointer = 'title') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::TITLE_MIN_LENGTH,
            self::TITLE_MAX_LENGTH
        )->validate($data)) {
            Core::setLastError(TITLE_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }
    
    const SOURCE_MIN_LENGTH = 2;
    const SOURCE_MAX_LENGTH = 15;
    public function source($data, string $pointer = 'source') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::SOURCE_MIN_LENGTH,
            self::SOURCE_MAX_LENGTH
        )->validate($data)) {
            Core::setLastError(SOURCE_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    public function image($image, $pointer = 'image') : bool
    {
        if (!V::arrayType()->validate($image)) {
            Core::setLastError(IMAGE_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        if (!isset($image['identify']) || !$this->validateImageExtension($image['identify'])) {
            Core::setLastError(IMAGE_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    private function validateImageExtension($image) : bool
    {
        if (!V::extension('png')->validate($image)
            && !V::extension('jpg')->validate($image)
            && !V::extension('jpeg')->validate($image)) {
            return false;
        }

        return true;
    }

    const ATTACHMENT_MAX_TOTAL = 5;

    public function attachments($attachments, $pointer = 'attachments') : bool
    {
        if (!V::arrayType()->validate($attachments)) {
            Core::setLastError(ATTACHMENT_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        if (count($attachments) > self::ATTACHMENT_MAX_TOTAL) {
            Core::setLastError(ATTACHMENT_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        foreach ($attachments as $attachment) {
            if (!$this->validateAttachmentExtension($attachment['identify'])) {
                return false;
            }
        }

        return true;
    }

    const ATTACHMENT_TYPE = [
        'zip','doc',
        'docx','xls',
        'xlsx','pdf',
        'rar','ppt'
    ];

    private function validateAttachmentExtension(string $attachment, $pointer = '') : bool
    {
        foreach (self::ATTACHMENT_TYPE as $val) {
            if (V::extension($val)->validate($attachment)) {
                return true;
            }
        }
        Core::setLastError(ATTACHMENT_FORMAT_ERROR, array('pointer'=>$pointer));
        return false;
    }

    public function status($status, $pointer = 'status') : bool
    {
        if (!V::numeric()->validate($status) || !in_array($status, IEnableAble::STATUS)) {
            Core::setLastError(STATUS_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    public function stick($stick, $pointer = 'stick') : bool
    {
        if (!V::numeric()->validate($stick) ||!in_array($stick, ITopAble::STICK)) {
            Core::setLastError(STICK_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }
        return true;
    }

    const REASON_MIN_LENGTH = 1;
    const REASON_MAX_LENGTH = 200;

    public function reason($data, string $pointer = 'reason') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::REASON_MIN_LENGTH,
            self::REASON_MAX_LENGTH
        )->validate($data)) {
            Core::setLastError(REASON_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    const DESCRIPTION_MIN_LENGTH = 1;
    const DESCRIPTION_MAX_LENGTH = 200;

    public function description($data, string $pointer = 'description') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::DESCRIPTION_MIN_LENGTH,
            self::DESCRIPTION_MAX_LENGTH
        )->validate($data)) {
            Core::setLastError(DESCRIPTION_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    public function images($images, $pointer = 'image') : bool
    {
        if (!V::arrayType()->validate($images)) {
            Core::setLastError(IMAGE_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        foreach ($images as $image) {
            if (!$this->validateImageExtension($image['identify'])) {
                Core::setLastError(IMAGE_FORMAT_ERROR, array('pointer'=>$pointer));
                return false;
            }
        }
        return true;
    }

    public function unifiedSocialCreditCode($unifiedSocialCreditCode) : bool
    {
        $reg = '/[a-zA-Z0-9]/';
        if (!V::alnum()->noWhitespace()->length(13, 18)->validate($unifiedSocialCreditCode) ||
        !preg_match($reg, $unifiedSocialCreditCode)) {
            Core::setLastError(UNIFIED_SOCIAL_CREDIT_CODE_FORMAT_ERROR);
            return false;
        }

        return true;
    }
}
