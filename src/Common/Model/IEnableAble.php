<?php
namespace Sdk\Common\Model;

interface IEnableAble
{
    const STATUS = array(
        'ENABLED' => 0 ,
        'DISABLED' => -2
    );

    const STATUS_CN = array(
        self::STATUS['ENABLED'] => '启用',
        self::STATUS['DISABLED'] => '禁用'
    );

    const STATUS_TAG_TYPE = array(
        self::STATUS['ENABLED'] => 'success',
        self::STATUS['DISABLED'] => 'danger'
    );

    public function enable() : bool;
    
    public function disable() : bool;
}
