<?php
namespace Sdk\Common\Model;

use Marmot\Core;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;

use Sdk\Common\Adapter\IApplyAbleAdapter;

trait ApplyAbleTrait
{
    private $applyStatus;
    
    private $rejectReason;

    private $operationType;

    private $applyInfoType;

    private $publishCrew;

    private $applyCrew;

    private $relationId;

    private $applyUserGroup;

    private $publishUserGroup;

    public function setApplyStatus(int $applyStatus) : void
    {
        $this->applyStatus = in_array($applyStatus, IApplyAble::APPLY_STATUS)
        ? $applyStatus
        : IApplyAble::APPLY_STATUS['PENDING'];
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }

    public function setRejectReason(string $rejectReason) : void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }

    public function setOperationType(int $operationType): void
    {
        $this->operationType = $operationType;
    }

    public function getOperationType(): int
    {
        return $this->operationType;
    }

    public function setApplyInfoType(int $applyInfoType): void
    {
        $this->applyInfoType = $applyInfoType;
    }

    public function getApplyInfoType(): int
    {
        return $this->applyInfoType;
    }

    public function setRelationId(int $relationId) : void
    {
        $this->relationId = $relationId;
    }

    public function getRelationId() : int
    {
        return $this->relationId;
    }

    public function setPublishCrew(Crew $publishCrew) : void
    {
        $this->publishCrew = $publishCrew;
    }

    public function getPublishCrew() : Crew
    {
        return $this->publishCrew;
    }

    public function setApplyCrew(Crew $applyCrew) : void
    {
        $this->applyCrew = $applyCrew;
    }

    public function getApplyCrew() : Crew
    {
        return $this->applyCrew;
    }

    public function setPublishUserGroup(UserGroup $publishUserGroup) : void
    {
        $this->publishUserGroup = $publishUserGroup;
    }

    public function getPublishUserGroup() : UserGroup
    {
        return $this->publishUserGroup;
    }

    public function setApplyUserGroup(UserGroup $applyUserGroup) : void
    {
        $this->applyUserGroup = $applyUserGroup;
    }

    public function getApplyUserGroup() : UserGroup
    {
        return $this->applyUserGroup;
    }

    public function approve() : bool
    {
        return $this->approveAction();
    }

    protected function approveAction(): bool
    {
        $applyAdapter = $this->getIApplyAbleAdapter();

        return $applyAdapter->approve($this);
    }

    public function reject() : bool
    {
        return $this->rejectAction();
    }

    protected function rejectAction(): bool
    {
        $applyAdapter = $this->getIApplyAbleAdapter();
        return $applyAdapter->reject($this);
    }

    abstract protected function getIApplyAbleAdapter() : IApplyAbleAdapter;
}
