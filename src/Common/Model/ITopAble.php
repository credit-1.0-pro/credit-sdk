<?php
namespace Sdk\Common\Model;

interface ITopAble
{
    const STICK = array(
        'DISABLED' => 0 ,
        'ENABLED' => 2
    );

    const STICK_CN = array(
        self::STICK['DISABLED'] => '未置顶',
        self::STICK['ENABLED'] => '置顶',
    );

    const STICK_TAG_TYPE = array(
        self::STICK['DISABLED'] => 'info',
        self::STICK['ENABLED'] => 'success',
    );
    
    public function top() : bool;
    
    public function cancelTop() : bool;
}
