<?php
namespace Sdk\Common\Model;

use Sdk\Common\Adapter\IOperateAbleAdapter;

trait OperateAbleTrait
{
    public function add() : bool
    {
        return $this->addAction();
    }

    protected function addAction(): bool
    {
        $repository = $this->getIOperateAbleAdapter();

        return $repository->add($this);
    }

    public function edit() : bool
    {
        return $this->editAction();
    }

    protected function editAction(): bool
    {
        $repository = $this->getIOperateAbleAdapter();

        return $repository->edit($this);
    }

    abstract protected function getIOperateAbleAdapter() : IOperateAbleAdapter;
}
