<?php
namespace Sdk\Common\Model;

interface IApplyAble
{
    const APPLY_STATUS = array(
        'NOT_SUBMITTED' => -1,
        'PENDING' => 0,
        'APPROVE' => 2,
        'REJECT' => -2,
        'REVOKED' => -4,
    );

    const APPLY_STATUS_CN = array(
        self::APPLY_STATUS['NOT_SUBMITTED'] => '未提交',
        self::APPLY_STATUS['PENDING'] => '待审核',
        self::APPLY_STATUS['APPROVE'] => '已通过',
        self::APPLY_STATUS['REJECT'] => '已驳回',
        self::APPLY_STATUS['REVOKED'] => '已撤销',
    );

    const APPLY_STATUS_TAG_TYPE = array(
        self::APPLY_STATUS['NOT_SUBMITTED'] => 'warning',
        self::APPLY_STATUS['PENDING'] => 'warning',
        self::APPLY_STATUS['APPROVE'] => 'success',
        self::APPLY_STATUS['REJECT'] => 'danger',
        self::APPLY_STATUS['REVOKED'] => 'danger',
    );

    const OPERATION_TYPE = array(
        'NULL' => 0,
        'ADD' => 1,
        'EDIT' => 2,
        'ENABLED' => 3,
        'DISABLED' => 4,
        'TOP' => 5,
        'CANCEL_TOP' => 6,
        'MOVE' => 7,
        'ACCEPT' => 8,
        'VERSION_RESTORE' => 9
    );

    const OPERATION_TYPE_CN = array(
        self::OPERATION_TYPE['NULL'] => '无',
        self::OPERATION_TYPE['ADD'] => '新增',
        self::OPERATION_TYPE['EDIT'] => '编辑',
        self::OPERATION_TYPE['ENABLED'] => '启用',
        self::OPERATION_TYPE['DISABLED'] => '禁用',
        self::OPERATION_TYPE['TOP'] => '置顶',
        self::OPERATION_TYPE['CANCEL_TOP'] => '取消置顶',
        self::OPERATION_TYPE['MOVE'] => '移动',
        self::OPERATION_TYPE['ACCEPT'] => '受理',
        self::OPERATION_TYPE['VERSION_RESTORE'] => '版本还原',
        
    );

    public function approve() : bool;

    public function reject() : bool;
}
