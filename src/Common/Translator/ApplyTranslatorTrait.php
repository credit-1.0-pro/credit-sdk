<?php
namespace Sdk\Common\Translator;

use Sdk\Common\Model\IApplyAble;
use Marmot\Framework\Classes\Filter;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

trait ApplyTranslatorTrait
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function applyObjectToArray($apply, array $keys = array())
    {
        if (!$apply instanceof IApplyAble) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'rejectReason',
                'applyStatus',
                'applyInfoType',
                'operationType',
                'relationId',
                'publishCrew' => [],
                'applyCrew' => [],
                'applyUserGroup' => []
            );
        }

        $expression = array();

        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = Filter::dhtmlspecialchars($apply->getRejectReason());
        }
        if (in_array('relationId', $keys)) {
            $expression['relationId'] = marmot_encode($apply->getRelationId());
        }
        if (in_array('applyInfoType', $keys)) {
            $expression['applyInfoType'] = marmot_encode($apply->getApplyInfoType());
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = [
                'id' => marmot_encode($apply->getApplyStatus()),
                'name' => isset(IApplyAble::APPLY_STATUS_CN[$apply->getApplyStatus()]) ?
                            IApplyAble::APPLY_STATUS_CN[$apply->getApplyStatus()] :
                            '',
                'type' => isset(IApplyAble::APPLY_STATUS_TAG_TYPE[$apply->getApplyStatus()]) ?
                            IApplyAble::APPLY_STATUS_TAG_TYPE[$apply->getApplyStatus()] :
                            ''
            ];
        }
        if (in_array('operationType', $keys)) {
            $expression['operationType']= [
                'id' => marmot_encode($apply->getOperationType()),
                'name' => isset(IApplyAble::OPERATION_TYPE_CN[$apply->getOperationType()]) ?
                            IApplyAble::OPERATION_TYPE_CN[$apply->getOperationType()] :
                            ''
            ];
        }
        if (isset($keys['applyCrew'])) {
            $expression['applyCrew'] = $this->getCrewTranslator()->objectToArray(
                $apply->getApplyCrew(),
                $keys['applyCrew']
            );
        }
        if (isset($keys['applyUserGroup'])) {
            $expression['applyUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $apply->getApplyUserGroup(),
                $keys['applyUserGroup']
            );
        }
        if (isset($keys['publishCrew'])) {
            $expression['publishCrew'] = $this->getCrewTranslator()->objectToArray(
                $apply->getPublishCrew(),
                $keys['publishCrew']
            );
        }

        return $expression;
    }
}
