<?php
namespace Sdk\Common\Translator;

use Marmot\Core;

use Sdk\Common\Model\IApplyAble;

use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

trait ApplyRestfulTranslatorTrait
{
    use RestfulTranslatorTrait;

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function applyTranslateToObject(array $expression, $apply = null)
    {
        $data = $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['applyStatus'])) {
            $apply->setApplyStatus($attributes['applyStatus']);
        }

        if (isset($attributes['rejectReason'])) {
            $apply->setRejectReason($attributes['rejectReason']);
        }

        if (isset($attributes['operationType'])) {
            $apply->setOperationType($attributes['operationType']);
        }

        if (isset($attributes['applyInfoType'])) {
            $apply->setApplyInfoType($attributes['applyInfoType']);
        }

        if (isset($attributes['relationId'])) {
            $apply->setRelationId($attributes['relationId']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['applyCrew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['applyCrew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
           
            $applyCrew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;
            $apply->setApplyCrew($applyCrew);
        }

        if (isset($relationships['applyUserGroup']['data'])) {
            $applyUserGroup = $this->changeArrayFormat($relationships['applyUserGroup']['data']);
            $apply->setApplyUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($applyUserGroup));
        }

        if (isset($relationships['publishCrew']['data'])) {
            $publishCrew = $this->changeArrayFormat($relationships['publishCrew']['data']);
            $apply->setPublishCrew($this->getCrewRestfulTranslator()->arrayToObject($publishCrew));
        }

        return $apply;
    }

    public function applyObjectToArray($apply, array $keys = array())
    {
        if (!$apply instanceof IApplyAble) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'rejectReason',
                'applyCrew',
            );
        }

        $attributes = array();

        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $apply->getRejectReason();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('applyCrew', $keys)) {
            $expression['data']['relationships']['applyCrew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $apply->getApplyCrew()->getId()
                )
            );
        }

        return $expression;
    }
}
