<?php
namespace Sdk\ResourceCatalog\ErrorData\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullErrorData extends ErrorData implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function onlineEdit() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
