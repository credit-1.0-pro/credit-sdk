<?php
namespace Sdk\ResourceCatalog\ErrorData\Model;

use Sdk\ResourceCatalog\ErrorData\Repository\ErrorDataRepository;
use Sdk\ResourceCatalog\ResourceCatalogData\Model\ResourceCatalogData;

class ErrorData extends ResourceCatalogData
{
    /**
     * 错误类型
     * @var ['MISSING_DATA'] 数据缺失
     * @var ['COMPARISON_FAILED'] 比对失败
     * @var ['DUPLICATION_DATA'] 数据重复
     * @var ['FORMAT_VALIDATION_FAILED'] 数据格式错误
     */
    const ERROR_TYPE = array(
        'MISSING_DATA' => 1,//数据缺失
        'COMPARISON_FAILED' => 2,//比对失败
        'DUPLICATION_DATA' => 4,//数据重复
        'FORMAT_VALIDATION_FAILED' => 8//数据格式错误
    );

    /**
     * 错误原因
     * @var ['MISSING_DATA'] 数据缺失
     * @var ['COMPARISON_FAILED'] 比对失败
     * @var ['DUPLICATION_DATA'] 数据重复
     * @var ['FORMAT_VALIDATION_FAILED'] 数据格式错误
     */
    const ERROR_REASON = array(
        self::ERROR_TYPE['MISSING_DATA'] => '数据缺失',
        self::ERROR_TYPE['COMPARISON_FAILED'] => '比对失败',
        self::ERROR_TYPE['DUPLICATION_DATA'] => '数据重复',
        self::ERROR_TYPE['FORMAT_VALIDATION_FAILED'] => '数据格式错误'
    );

    /**
     * 失败状态
     * @var ['NORMAL'] 数据缺失
     * @var ['PROGRAM_EXCEPTION'] 程序异常
     * @var ['STORAGE_EXCEPTION'] 入库异常
     */
    const ERROR_STATUS = array(
        'NORMAL' => 0, //正常
        'PROGRAM_EXCEPTION' => 1, //程序异常
        'STORAGE_EXCEPTION' => 2, //入库异常
    );

    /**
     * 状态
     * @var ['UN_PROCESSED'] 待处理
     * @var ['PROCESSED'] 已处理
     */
    const STATUS = array(
        'UN_PROCESSED' => 0, //待处理
        'PROCESSED' => 2, //已处理
    );

    const STATUS_CN = array(
        self::STATUS['UN_PROCESSED'] => '待处理',
        self::STATUS['PROCESSED'] => '已处理'
    );

    const STATUS_TYPE = array(
        self::STATUS['UN_PROCESSED'] => 'waring',
        self::STATUS['PROCESSED'] => 'success'
    );

     /**
     * @var int $errorType 失败类型
     */
    protected $errorType;
     /**
     * @var array $errorReason 失败原因
     */
    protected $errorReason;
    /**
    * @var int $errorStatus 失败状态
    */
    protected $errorStatus;

    protected $repository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->errorStatus = 0;
        $this->errorType = 0;
        $this->errorReason = array();
        $this->status = self::STATUS['UN_PROCESSED'];
        $this->repository = new ErrorDataRepository();
    }

    public function __destruct()
    {
        unset($this->errorStatus);
        unset($this->errorType);
        unset($this->errorReason);
        unset($this->status);
        unset($this->repository);
    }

    public function setErrorType(int $errorType) : void
    {
        $this->errorType = $errorType;
    }

    public function getErrorType() : int
    {
        return $this->errorType;
    }

    public function setErrorReason(array $errorReason) : void
    {
        $this->errorReason = $errorReason;
    }

    public function getErrorReason() : array
    {
        return $this->errorReason;
    }

    public function setErrorStatus(int $errorStatus) : void
    {
        $this->errorStatus = $errorStatus;
    }

    public function getErrorStatus() : int
    {
        return $this->errorStatus;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['UN_PROCESSED'];
    }
    
    public function getRepository()
    {
        return $this->repository;
    }

    public function onlineEdit() : bool
    {
        return $this->getRepository()->onlineEdit($this);
    }
}
