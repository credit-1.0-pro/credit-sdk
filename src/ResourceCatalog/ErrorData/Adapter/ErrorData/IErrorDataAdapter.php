<?php
namespace Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData;

use Marmot\Interfaces\IAsyncAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;

interface IErrorDataAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    public function onlineEdit(ErrorData $errorData) : bool;
}
