<?php
namespace Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\ErrorData\Utils\MockFactory;

class ErrorDataMockAdapter implements IErrorDataAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateErrorData($id);
    }

    public function fetchList(array $ids) : array
    {
        $errorDataList = array();

        foreach ($ids as $id) {
            $errorDataList[] = MockFactory::generateErrorData($id);
        }

        return $errorDataList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateErrorData($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $errorDataList = array();

        foreach ($ids as $id) {
            $errorDataList[] = MockFactory::generateErrorData($id);
        }

        return $errorDataList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function onlineEdit(ErrorData $errorData) : bool
    {
        unset($errorData);

        return true;
    }
}
