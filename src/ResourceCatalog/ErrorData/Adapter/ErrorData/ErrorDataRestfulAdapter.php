<?php
namespace Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\ErrorData\Model\NullErrorData;
use Sdk\ResourceCatalog\ErrorData\Translator\ErrorDataRestfulTranslator;

class ErrorDataRestfulAdapter extends GuzzleAdapter implements IErrorDataAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    const MAP_ERROR = [
        101 => array(
            'itemsData' => RESOURCE_CATALOG_DATA_ITEMS_DATA_FORMAT_ERROR,
            'expirationDate'=>RESOURCE_CATALOG_DATA_EXPIRATION_DATE_FORMAT_ERROR,
            'crewId' => CREW_ID_FORMAT_ERROR,
            'templateId'=>RESOURCE_CATALOG_DATA_TEMPLATE_FORMAT_ERROR,
            'rejectReason' => REASON_FORMAT_ERROR,
            'subjectCategory'=>RESOURCE_CATALOG_DATA_SUBJECT_CATEGORY_FORMAT_ERROR
        ),
        100 => array(
            'crew' => PARAMETER_IS_EMPTY,
            'template' => RESOURCE_CATALOG_DATA_TEMPLATE_NOT_EXIT
        ),
        102 => array(
            'status' => STATUS_CAN_NOT_MODIFY,
            'applyStatus' => APPLY_STATUE_CAN_NOT_MODIFY,
        ),
        103 => RESOURCE_CATALOG_DATA_IS_UNIQUE,
        1101 => RESOURCE_CATALOG_DATA_ITEMS_DATA_FORMAT_ERROR
    ];
        
    private $translator;
    
    private $resource;
    
    const SCENARIOS = [
        'ERROR_DATA_LIST'=>[
            'fields' => [],
            'include' => 'templateVersion,template,task,errorItemsData'
        ],
        'ERROR_DATA_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'templateVersion,template,task,errorItemsData'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new ErrorDataRestfulTranslator();
        $this->resource = 'resourceCatalogs/errorData';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullErrorData::getInstance();
    }

    protected function getMapErrors() : array
    {
        $mapError = self::MAP_ERROR;

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function mapErrors() : void
    {
        $id = $this->lastErrorId();
        $pointer = $this->lastErrorPointer();
        $mapErrors = $this->getMapErrors();
    
        if (isset($mapErrors[$id])) {
            is_array($mapErrors[$id])
            ? Core::setLastError($mapErrors[$id][$pointer], array('pointer'=>$pointer))
            : Core::setLastError($mapErrors[$id], array('pointer'=>$pointer));
        }
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function onlineEdit(ErrorData $errorData) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $errorData,
            array('expirationDate', 'crew', 'itemsData')
        );
    
        $this->patch(
            $this->getResource().'/'.$errorData->getId().'/'.'onlineEdit',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($errorData);
            return true;
        }

        return false;
    }
}
