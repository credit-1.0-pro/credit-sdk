<?php
namespace Sdk\ResourceCatalog\ErrorData\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\ErrorData\Model\NullErrorData;

use Sdk\ResourceCatalog\Task\Translator\TaskRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;
use Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ErrorDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $errorData = null)
    {
        return $this->translateToObject($expression, $errorData);
    }

    protected function getTaskRestfulTranslator(): TaskRestfulTranslator
    {
        return new TaskRestfulTranslator();
    }

    protected function getTemplateRestfulTranslator(): TemplateRestfulTranslator
    {
        return new TemplateRestfulTranslator();
    }

    protected function getTemplateVersionRestfulTranslator(): TemplateVersionRestfulTranslator
    {
        return new TemplateVersionRestfulTranslator();
    }

    protected function getItemsDataRestfulTranslator(): ItemsDataRestfulTranslator
    {
        return new ItemsDataRestfulTranslator();
    }

    protected function translateToObject(array $expression, $errorData = null)
    {
        if (empty($expression)) {
            return new NullErrorData();
        }

        if ($errorData == null) {
            $errorData = new ErrorData();
        }
       
        $data = $expression['data'];
        
        $id = $data['id'];
        $errorData->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $errorData->setName($attributes['name']);
        }
        if (isset($attributes['identify'])) {
            $errorData->setIdentify($attributes['identify']);
        }
        if (isset($attributes['errorType'])) {
            $errorData->setErrorType($attributes['errorType']);
        }
        if (isset($attributes['errorReason'])) {
            $errorData->setErrorReason($attributes['errorReason']);
        }
        if (isset($attributes['errorStatus'])) {
            $errorData->setErrorStatus($attributes['errorStatus']);
        }
        if (isset($attributes['createTime'])) {
            $errorData->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['statusTime'])) {
            $errorData->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['updateTime'])) {
            $errorData->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $errorData->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['task']['data'])) {
            $task = $this->changeArrayFormat($relationships['task']['data']);
            $errorData->setTask(
                $this->getTaskRestfulTranslator()->arrayToObject($task)
            );
        }
        if (isset($relationships['templateVersion']['data'])) {
            $templateVersion = $this->changeArrayFormat($relationships['templateVersion']['data']);
            $errorData->setTemplateVersion(
                $this->getTemplateVersionRestfulTranslator()->arrayToObject($templateVersion)
            );
        }
        if (isset($relationships['template']['data'])) {
            $template = $this->changeArrayFormat($relationships['template']['data']);
            $errorData->setTemplate(
                $this->getTemplateRestfulTranslator()->arrayToObject($template)
            );
        }
        if (isset($relationships['errorItemsData']['data'])) {
            $errorItemsData = $this->changeArrayFormat($relationships['errorItemsData']['data']);
            $errorData->setItemsData(
                $this->getItemsDataRestfulTranslator()->arrayToObject($errorItemsData)
            );
        }

        return $errorData;
    }

    public function objectToArray($errorData, array $keys = array())
    {
        if (!$errorData instanceof ErrorData) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'expirationDate',
                'crew',
                'itemsData'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'errorData'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $errorData->getId();
        }

        $attributes = array();

        if (in_array('expirationDate', $keys)) {
            $attributes['expirationDate'] = $errorData->getExpirationDate();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $errorData->getCrew()->getId()
                )
            );
        }

        if (in_array('itemsData', $keys)) {
            $expression['data']['relationships']['itemsData']['data'] = array(
                array(
                    'type' => 'itemsData',
                    'attributes' => array(
                        'data' => $errorData->getItemsData()->getData()
                    )
                )
            );
        }
    
        return $expression;
    }
}
