<?php
namespace Sdk\ResourceCatalog\ErrorData\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\ErrorData\Model\NullErrorData;

use Sdk\ResourceCatalog\Task\Translator\TaskTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionTranslator;
use Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataTranslator;

class ErrorDataTranslator implements ITranslator
{

    public function arrayToObject(array $expression, $errorData = null)
    {
        unset($errorData);
        unset($expression);
        return new NullErrorData();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getTaskTranslator() : TaskTranslator
    {
        return new TaskTranslator();
    }

    protected function getTemplateTranslator() : TemplateTranslator
    {
        return new TemplateTranslator();
    }

    protected function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return new TemplateVersionTranslator();
    }

    protected function getItemsDataTranslator() : ItemsDataTranslator
    {
        return new ItemsDataTranslator();
    }

   /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($errorData, array $keys = array())
    {
        if (!$errorData instanceof ErrorData) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'errorType',
                'errorReason',
                'errorStatus',
                'task'=> [],
                'itemsData' => [],
                'template' => [],
                'templateVersion' => [],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($errorData->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $errorData->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $errorData->getIdentify();
        }
        if (in_array('errorType', $keys)) {
            $expression['errorType'] = $errorData->getErrorType();
        }
        if (in_array('errorReason', $keys)) {
            $expression['errorReason'] = $errorData->getErrorReason();
        }
        if (in_array('errorStatus', $keys)) {
            $expression['errorStatus'] = $errorData->getErrorStatus();
        }
        if (in_array('status', $keys)) {
            $status = $errorData->getStatus();
            $expression['status'] = array(
                'id' => marmot_encode($status),
                'name' => isset(ErrorData::STATUS_CN[$status]) ? ErrorData::STATUS_CN[$status] : '',
                'type' => isset(ErrorData::STATUS_TYPE[$status]) ? ErrorData::STATUS_TYPE[$status] : '',
            );
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $errorData->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $errorData->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $errorData->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $errorData->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $errorData->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $errorData->getCreateTime());
        }
        if (isset($keys['task'])) {
            $expression['task'] = $this->getTaskTranslator()->objectToArray(
                $errorData->getTask(),
                $keys['task']
            );
        }
        if (isset($keys['itemsData'])) {
            $expression['itemsData'] = $this->getItemsDataTranslator()->objectToArray(
                $errorData->getItemsData(),
                $keys['itemsData']
            );
        }
        if (isset($keys['template'])) {
            $expression['template'] = $this->getTemplateTranslator()->objectToArray(
                $errorData->getTemplate(),
                $keys['template']
            );
        }
        if (isset($keys['templateVersion'])) {
            $expression['templateVersion'] = $this->getTemplateVersionTranslator()->objectToArray(
                $errorData->getTemplateVersion(),
                $keys['templateVersion']
            );
        }

        return $expression;
    }
}
