<?php
namespace Sdk\ResourceCatalog\ErrorData\Command\ErrorData;

use Marmot\Interfaces\ICommand;

class OnlineEditErrorDataCommand implements ICommand
{
    public $id;

    public $itemsData;
    
    public $expirationDate;

    public function __construct(
        array $itemsData,
        int $expirationDate,
        int $id
    ) {
        $this->itemsData = $itemsData;
        $this->expirationDate = $expirationDate;
        $this->id = $id;
    }
}
