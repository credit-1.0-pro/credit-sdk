<?php
namespace Sdk\ResourceCatalog\ErrorData\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData\IErrorDataAdapter;
use Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData\ErrorDataMockAdapter;
use Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData\ErrorDataRestfulAdapter;

class ErrorDataRepository extends Repository implements IErrorDataAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ERROR_DATA_LIST';
    const FETCH_ONE_MODEL_UN = 'ERROR_DATA_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new ErrorDataRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new ErrorDataMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function onlineEdit(ErrorData $errorData) : bool
    {
        return $this->getAdapter()->onlineEdit($errorData);
    }
}
