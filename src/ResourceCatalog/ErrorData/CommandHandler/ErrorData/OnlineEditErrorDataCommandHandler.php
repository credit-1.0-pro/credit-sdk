<?php
namespace Sdk\ResourceCatalog\ErrorData\CommandHandler\ErrorData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\ErrorData\Repository\ErrorDataRepository;
use Sdk\ResourceCatalog\ErrorData\Command\ErrorData\OnlineEditErrorDataCommand;

class OnlineEditErrorDataCommandHandler implements ICommandHandler
{
    private $repository;

    public function __construct()
    {
        $this->repository = new ErrorDataRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : ErrorDataRepository
    {
        return $this->repository;
    }

    protected function fetchErrorData(int $id) : ErrorData
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof OnlineEditErrorDataCommand)) {
            throw new \InvalidArgumentException;
        }
    
        $errorData = $this->fetchErrorData($command->id);
        $errorData->setExpirationDate($command->expirationDate);
        $errorData->getItemsData()->setData($command->itemsData);
        
        return $errorData->onlineEdit();
    }
}
