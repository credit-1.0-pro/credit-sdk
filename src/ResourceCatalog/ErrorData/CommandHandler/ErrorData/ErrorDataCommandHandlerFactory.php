<?php
namespace Sdk\ResourceCatalog\ErrorData\CommandHandler\ErrorData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ErrorDataCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\ResourceCatalog\ErrorData\Command\ErrorData\OnlineEditErrorDataCommand'=>
            'Sdk\ResourceCatalog\ErrorData\CommandHandler\ErrorData\OnlineEditErrorDataCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
