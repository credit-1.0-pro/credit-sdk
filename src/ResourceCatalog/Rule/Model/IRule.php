<?php
namespace Sdk\ResourceCatalog\Rule\Model;

class IRule
{

    const RULE_NAME = [
        'COMPLETION_RULE' =>'completionRule',
        'COMPARISON_RULE'=>'comparisonRule',
        'DE_DUPLICATION_RULE'=>'deDuplicationRule'
    ];

    const COMPLETION_BASE = [
        'NAME'=>1,
        'IDENTIFY'=>2
    ];

    const COMPLETION_BASE_CN = [
        self::COMPLETION_BASE['NAME'] => '企业名称',
        self::COMPLETION_BASE['IDENTIFY'] => '主体代码(统一社会信用代码/身份证号)'
    ];

    const COMPARISON_BASE = [
        'NAME'=>1,
        'IDENTIFY'=>2
    ];

    const COMPARISON_BASE_CN = [
        self::COMPARISON_BASE['NAME'] => '企业名称',
        self::COMPARISON_BASE['IDENTIFY'] => '主体代码(统一社会信用代码/身份证号)'
    ];

    const DE_DUPLICATION_RESULT = [
        'DISCARD'=>1,
        'COVER'=>2
    ];

    const DE_DUPLICATION_RESULT_CN = [
        self::DE_DUPLICATION_RESULT['DISCARD'] => '丢弃',
        self::DE_DUPLICATION_RESULT['COVER'] => '覆盖'
    ];
}
