<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Common\Model\NullApplyAbleTrait;
use Sdk\Common\Model\NullOperateAbleTrait;

class NullUnAuditRule extends UnAuditRule implements INull
{
    use NullApplyAbleTrait, NullOperateAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function revoke() : bool
    {
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
