<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Marmot\Core;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\ApplyAbleTrait;
use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\ResourceCatalog\Rule\Repository\UnAuditRuleRepository;

class UnAuditRule extends Rule implements IApplyAble
{
    use ApplyAbleTrait;
    
    private $unAuditRuleRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->rejectReason = '';
        $this->applyInfoType = 0;
        $this->relationId = 0;
        $this->publishUserGroup = new UserGroup();
        $this->applyUserGroup = new UserGroup();
        $this->operationType = self::OPERATION_TYPE['NULL'];
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->publishCrew = new Crew();
        $this->applyCrew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->unAuditRuleRepository = new UnAuditRuleRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->rejectReason);
        unset($this->operationType);
        unset($this->applyInfoType);
        unset($this->applyStatus);
        unset($this->relationId);
        unset($this->publishCrew);
        unset($this->applyCrew);
        unset($this->applyUserGroup);
        unset($this->publishUserGroup);
        unset($this->unAuditRuleRepository);
    }
    
    protected function getUnAuditRuleRepository()
    {
        return $this->unAuditRuleRepository;
    }

    /**
     * 审核通过/驳回
     * @return [bool]
     */
    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getUnAuditRuleRepository();
    }

    /**
     * 新增编辑
     * @return [bool]
     */
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getUnAuditRuleRepository();
    }

    public function isPending() : bool
    {
        return $this->getApplyStatus() == IApplyAble::APPLY_STATUS['PENDING'];
    }

    public function revoke() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(APPLY_STATUE_CAN_NOT_MODIFY);
            return false;
        }
        
        return $this->getUnAuditRuleRepository()->revoke($this);
    }
}
