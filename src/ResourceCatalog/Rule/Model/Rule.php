<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\IOperateAble;
use Sdk\Common\Model\OperateAbleTrait;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;

use Sdk\ResourceCatalog\Rule\Repository\RuleRepository;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class Rule implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    /**
     * 主体类别
     * @var STATUS['NORMAL']  正常 0
     * @var STATUS['DELETED']  删除 -2
     */
    const STATUS = array(
        'NORMAL' => 0,
        'DELETED' => -2
    );
    const STATUS_CN = array(
        self::STATUS['NORMAL'] => '正常',
        self::STATUS['DELETED'] => '删除'
    );


    private $id;
    /**
     * @var string $versionDescription 规则版本描述
     */
    protected $versionDescription;
    /**
     * @var array $rules 规则信息
     */
    protected $rules;
    /**
     * @var Template $template 目录
     */
    protected $template;
    /**
     * @var TemplateVersion $templateVersion 目录版本
     */
    protected $templateVersion;
    /**
     * @var RuleVersion $ruleVersion 当前使用的版本记录
     */
    protected $ruleVersion;
    /**
     * @var Crew $crew 发布人
     */
    protected $crew;
    /**
     * @var UserGroup $userGroup 发布委办局
     */
    protected $userGroup;

    protected $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->versionDescription = '';
        $this->rules = array();
        $this->template = new Template();
        $this->templateVersion = new TemplateVersion();
        $this->ruleVersion = new RuleVersion();
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->userGroup = new UserGroup();
        $this->status = self::STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->repository = new RuleRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->versionDescription);
        unset($this->rules);
        unset($this->template);
        unset($this->ruleVersion);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setVersionDescription(string $versionDescription)
    {
        $this->versionDescription = $versionDescription;
    }

    public function getVersionDescription() : string
    {
        return $this->versionDescription;
    }

    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }

    public function getRules() : array
    {
        return $this->rules;
    }

    public function setTemplate(Template $template)
    {
        $this->template = $template;
    }

    public function getTemplate() : Template
    {
        return $this->template;
    }

    public function setTemplateVersion(TemplateVersion $templateVersion) : void
    {
        $this->templateVersion = $templateVersion;
    }

    public function getTemplateVersion() : TemplateVersion
    {
        return $this->templateVersion;
    }

    public function setRuleVersion(RuleVersion $ruleVersion) : void
    {
        $this->ruleVersion = $ruleVersion;
    }

    public function getRuleVersion() : RuleVersion
    {
        return $this->ruleVersion;
    }

    public function setCrew(Crew $crew)
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserGroup(UserGroup $userGroup)
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['NORMAL'];
    }
    
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * 新增编辑
     */
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 版本还原
     * @return [bool]
     */
    public function versionRestore() : bool
    {
        return $this->getRepository()->versionRestore($this);
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }

    public function delete() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }
        
        return $this->getRepository()->deleted($this);
    }
}
