<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;

class RuleVersion implements IObject
{
    use Object;
    /**
     * 状态
     * @var STATUS['ENABLED']  启用
     * @var STATUS['DISABLED']  禁用, 默认
     */
    const STATUS = array(
        'ENABLED' => 0,
        'DISABLED' => -2,
    );
    const STATUS_CN = array(
        self::STATUS['ENABLED'] => '启用',
        self::STATUS['DISABLED'] => '禁用',
    );

    private $id;
    /**
     * @var string $number 版本号
     */
    protected $number;
    /**
     * @var string $description 描述
     */
    protected $description;
    /**
     * @var int $ruleId 规则id
     */
    protected $ruleId;
    /**
     * @var array $info 版本信息
     */
    protected $info;
    /**
     * @var Crew $crew 发布人
     */
    protected $crew;
    /**
     * @var UserGroup $userGroup 发布委办局
     */
    protected $userGroup;
    /**
     * @var Template $template 目录
     */
    protected $template;
    /**
     * @var TemplateVersion $templateVersion 目录版本
     */
    protected $templateVersion;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->number = '';
        $this->description = '';
        $this->ruleId = 0;
        $this->info = array();
        $this->crew = new Crew();
        $this->userGroup = new UserGroup();
        $this->template = new Template();
        $this->templateVersion = new TemplateVersion();
        $this->status = self::STATUS['DISABLED'];
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->number);
        unset($this->description);
        unset($this->ruleId);
        unset($this->info);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->template);
        unset($this->templateVersion);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
    public function setNumber(string $number)
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setRuleId(int $ruleId)
    {
        $this->ruleId = $ruleId;
    }

    public function getRuleId() : int
    {
        return $this->ruleId;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setInfo(array $info)
    {
        $this->info = $info;
    }

    public function getInfo() : array
    {
        return $this->info;
    }

    public function setCrew(Crew $crew)
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setTemplateVersion(TemplateVersion $templateVersion) : void
    {
        $this->templateVersion = $templateVersion;
    }

    public function getTemplateVersion() : TemplateVersion
    {
        return $this->templateVersion;
    }

    public function setUserGroup(UserGroup $userGroup)
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setTemplate(Template $template)
    {
        $this->template = $template;
    }

    public function getTemplate() : Template
    {
        return $this->template;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['DISABLED'];
    }
}
