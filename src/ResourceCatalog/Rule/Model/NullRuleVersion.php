<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Marmot\Interfaces\INull;

class NullRuleVersion extends RuleVersion implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
