<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Common\Model\NullOperateAbleTrait;

class NullRule extends Rule implements INull
{
    use NullOperateAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function versionRestore() : bool
    {
        return $this->resourceNotExist();
    }

    public function delete() : bool
    {
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
