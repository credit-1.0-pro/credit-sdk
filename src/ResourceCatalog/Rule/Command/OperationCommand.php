<?php
namespace Sdk\ResourceCatalog\Rule\Command;

use Marmot\Interfaces\ICommand;

class OperationCommand implements ICommand
{
    public $id;

    public $versionDescription;

    public $rules;

    public function __construct(
        string $versionDescription,
        array $rules = array(),
        int $id = 0
    ) {
        $this->id = $id;
        $this->versionDescription = $versionDescription;
        $this->rules = $rules;
    }
}
