<?php
namespace Sdk\ResourceCatalog\Rule\Command\UnAuditRule;

use Marmot\Interfaces\ICommand;

class RevokeUnAuditRuleCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}
