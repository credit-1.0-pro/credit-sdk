<?php
namespace Sdk\ResourceCatalog\Rule\Command\Rule;

use Marmot\Interfaces\ICommand;

class VersionRestoreRuleCommand implements ICommand
{
    public $id;
    
    public $ruleVersionId;

    public function __construct(
        int $ruleVersionId,
        int $id
    ) {
        $this->id = $id;
        $this->ruleVersionId = $ruleVersionId;
    }
}
