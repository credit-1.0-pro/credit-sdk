<?php
namespace Sdk\ResourceCatalog\Rule\Command\Rule;

use Marmot\Interfaces\ICommand;

class DeleteRuleCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}
