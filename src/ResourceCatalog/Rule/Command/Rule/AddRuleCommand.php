<?php
namespace Sdk\ResourceCatalog\Rule\Command\Rule;

use Sdk\ResourceCatalog\Rule\Command\OperationCommand;

class AddRuleCommand extends OperationCommand
{
    public $template;

    public function __construct(
        string $versionDescription,
        array $rules,
        int $template,
        int $id = 0
    ) {
        parent::__construct(
            $versionDescription,
            $rules,
            $id
        );
        
        $this->template = $template;
    }
}
