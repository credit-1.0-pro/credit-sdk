<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\Rule;

use Sdk\Common\Adapter\OperateAbleMockAdapterTrait;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Utils\MockFactory;

class RuleMockAdapter implements IRuleAdapter
{
    use OperateAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateRule($id);
    }

    public function fetchList(array $ids) : array
    {
        $ruleList = array();

        foreach ($ids as $id) {
            $ruleList[] = MockFactory::generateRule($id);
        }

        return $ruleList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateRule($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $ruleList = array();

        foreach ($ids as $id) {
            $ruleList[] = MockFactory::generateRule($id);
        }

        return $ruleList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function versionRestore(Rule $rule) : bool
    {
        unset($rule);

        return true;
    }

    public function deleted(Rule $rule) : bool
    {
        unset($rule);

        return true;
    }
}
