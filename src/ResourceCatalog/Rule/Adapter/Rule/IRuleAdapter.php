<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\Rule;

use Marmot\Interfaces\IAsyncAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\ResourceCatalog\Rule\Model\Rule;

interface IRuleAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperateAbleAdapter
{
    public function versionRestore(Rule $rule) : bool;

    public function deleted(Rule $rule) : bool;
}
