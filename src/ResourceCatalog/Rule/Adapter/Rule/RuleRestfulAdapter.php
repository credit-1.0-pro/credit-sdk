<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\Rule;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Model\NullRule;
use Sdk\ResourceCatalog\Rule\Translator\RuleRestfulTranslator;

class RuleRestfulAdapter extends GuzzleAdapter implements IRuleAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    const MAP_ERROR = [
        101 => array(
            'versionDescription'=>RULE_VERSION_DESCRIPTION_FORMAT_ERROR,
            'crewId'=>CREW_ID_FORMAT_ERROR,
            'applyCrewId'=>APPLY_CREW_FORMAT_ERROR,
            'templateId'=>RULE_TEMPLATE_FORMAT_ERROR,
            'rejectReason'=>REASON_FORMAT_ERROR,
            'ruleVersionId'=> RULE_VERSION_FORMAT_ERROR,
            'rules'=>RULE_RULES_FORMAT_ERROR,
            'rulesName'=>RULE_RULES_NAME_NOT_EXIT,
            'completionRules'=>RULE_COMPLETION_RULES_FORMAT_ERROR,
            'completionRulesCount'=>RULE_COMPLETION_RULES_COUNT_ERROR,
            'completionRulesId' => RULE_COMPLETION_RULES_TEMPLATE_FORMAT_ERROR,
            'completionRulesBase'=>RULE_COMPLETION_RULES_BASE_FORMAT_ERROR,
            'completionRulesItem'=>RULE_COMPLETION_RULES_TEMPLATE_ITEM_FORMAT_ERROR,
            'comparisonRules'=>RULE_COMPARISON_RULES_FORMAT_ERROR,
            'comparisonRulesCount'=>RULE_COMPARISON_RULES_COUNT_ERROR,
            'comparisonRulesId' => RULE_COMPARISON_RULES_TEMPLATE_FORMAT_ERROR,
            'comparisonRulesBase'=>RULE_COMPARISON_RULES_BASE_FORMAT_ERROR,
            'comparisonRulesItem'=>RULE_COMPARISON_RULES_TEMPLATE_ITEM_FORMAT_ERROR,
            'deDuplicationRules'=>RULE_DE_DUPLICATION_RULES_FORMAT_ERROR,
            'deDuplicationRulesItems'=>RULE_DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR,
            'deDuplicationRulesResult' => RULE_DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR,
            'completionRulesTransformationItemNotExist'=>RULE_COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST,
            'completionRulesCompletionTemplateNotExist'=>RULE_COMPLETION_RULES_TEMPLATE_NOT_EXIST,
            'completionRulesCompletionTemplateItemNotExist'=>RULE_COMPLETION_RULES_TEMPLATE_ITEM_NOT_EXIST,
            'comparisonRulesTransformationItemNotExist'=>RULE_COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST,
            'comparisonRulesComparisonTemplateNotExist'=>RULE_COMPARISON_RULES_TEMPLATE_NOT_EXIST,
            'comparisonRulesComparisonTemplateItemNotExist'=>RULE_COMPARISON_RULES_TEMPLATE_ITEM_NOT_EXIST,
            'comparisonRulesBaseCannotName'=>RULE_COMPARISON_RULES_BASE_CANNOT_NAME,
            'comparisonRulesBaseCannotIdentify'=>RULE_COMPARISON_RULES_BASE_CANNOT_IDENTIFY,
            'deDuplicationRulesItemsNotExit'=>RULE_DE_DUPLICATION_RULES_ITEMS_NOT_EXIST,
            'transformationItemTypeNotExist'=>RULE_TRANSFORMATION_TEMPLATE_ITEM_TYPE_NOT_EXIST,
            'typeCannotTransformation'=>RULE_TYPE_CANNOT_TRANSFORMATION,
            'lengthCannotTransformation'=>RULE_LENGTH_CANNOT_TRANSFORMATION,
            'dimensionCannotTransformation'=>RULE_DIMENSION_CANNOT_TRANSFORMATION,
            'isMaskedCannotTransformation'=>RULE_IS_MASKED_CANNOT_TRANSFORMATION,
            'maskRuleCannotTransformation'=>RULE_MASK_RULE_CANNOT_TRANSFORMATION,
            'zrrNotIncludeIdentify'=>RULE_RULES_TEMPLATE_ZRR_NOT_INCLUDE_IDENTIFY,
        ),
        100 => array(
            'crew' => PARAMETER_IS_EMPTY,
            'publishCrew' => PARAMETER_IS_EMPTY,
            'template' => RULE_TEMPLATE_NOT_EXIST,
            'templateVersion' => RULE_TEMPLATE_VERSION_NOT_EXIST,
            'ruleVersion' => RULE_VERSION_NOT_EXIST
        ),
        102 => array(
            'status'=>STATUS_CAN_NOT_MODIFY,
            'applyStatus'=>APPLY_STATUE_CAN_NOT_MODIFY
        ),
        103 => array(
            'rule' => RULE_IS_UNIQUE,
        )
    ];
        
    private $translator;
    
    private $resource;
    
    const SCENARIOS = [
        'RULE_LIST'=>[
            'fields' => [],
            'include' => 'crew,publishUserGroup,templateVersion,template,ruleVersion'
        ],
        'RULE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,publishUserGroup,templateVersion,template,ruleVersion'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new RuleRestfulTranslator();
        $this->resource = 'resourceCatalogs/rules';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullRule::getInstance();
    }

    protected function getMapErrors() : array
    {
        $mapError = self::MAP_ERROR;

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function addAction(Rule $rule) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $rule,
            array(
                'rules',
                'versionDescription',
                'template',
                'crew'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($rule);
            return true;
        }

        return false;
    }

    protected function editAction(Rule $rule) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $rule,
            array(
                'rules',
                'versionDescription',
                'crew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$rule->getId(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($rule);
            return true;
        }

        return false;
    }

    public function versionRestore(Rule $rule) : bool
    {
        $data = $this->getTranslator()->objectToArray($rule, array('crew'));
    
        $this->patch(
            $this->getResource().'/'.$rule->getId().'/'.'versionRestore/'.$rule->getRuleVersion()->getId(),
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($rule);
            return true;
        }

        return false;
    }

    public function deleted(Rule $rule) : bool
    {
        $data = $this->getTranslator()->objectToArray($rule, array('crew'));
    
        $this->delete(
            $this->getResource().'/'.$rule->getId().'/'.'delete',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($rule);
            return true;
        }

        return false;
    }
}
