<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule;

use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;

interface IUnAuditRuleAdapter extends IUnAuditRuleFetchAdapter, IUnAuditRuleOperateAbleAdapter
{
    public function revoke(UnAuditRule $unAuditRule) : bool;
}
