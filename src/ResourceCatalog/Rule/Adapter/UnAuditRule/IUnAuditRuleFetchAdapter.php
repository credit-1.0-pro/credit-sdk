<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule;

use Marmot\Interfaces\IAsyncAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;

interface IUnAuditRuleFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
}
