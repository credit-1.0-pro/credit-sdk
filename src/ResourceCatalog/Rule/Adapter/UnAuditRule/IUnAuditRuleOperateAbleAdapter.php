<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule;

use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

interface IUnAuditRuleOperateAbleAdapter extends IOperateAbleAdapter, IApplyAbleAdapter
{
}
