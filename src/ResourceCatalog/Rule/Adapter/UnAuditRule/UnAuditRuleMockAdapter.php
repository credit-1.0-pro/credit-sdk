<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule;

use Sdk\Common\Adapter\ApplyAbleMockAdapterTrait;
use Sdk\Common\Adapter\OperateAbleMockAdapterTrait;

use Sdk\ResourceCatalog\Rule\Utils\MockFactory;
use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;

class UnAuditRuleMockAdapter implements IUnAuditRuleAdapter
{
    use ApplyAbleMockAdapterTrait, OperateAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateUnAuditRule($id);
    }

    public function fetchList(array $ids) : array
    {
        $ruleList = array();

        foreach ($ids as $id) {
            $ruleList[] = MockFactory::generateUnAuditRule($id);
        }

        return $ruleList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateUnAuditRule($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $ruleList = array();

        foreach ($ids as $id) {
            $ruleList[] = MockFactory::generateUnAuditRule($id);
        }

        return $ruleList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function revoke(UnAuditRule $unAuditRule) : bool
    {
        unset($unAuditRule);

        return true;
    }
}
