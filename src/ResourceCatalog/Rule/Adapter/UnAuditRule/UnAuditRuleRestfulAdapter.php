<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Model\NullUnAuditRule;
use Sdk\ResourceCatalog\Rule\Adapter\Rule\RuleRestfulAdapter;
use Sdk\ResourceCatalog\Rule\Translator\UnAuditRuleRestfulTranslator;

class UnAuditRuleRestfulAdapter extends GuzzleAdapter implements IUnAuditRuleAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'UN_AUDIT_RULE_LIST'=>[
            'fields' => [],
            'include' =>
                'crew,publishUserGroup,applyUserGroup,applyCrew,publishCrew,templateVersion,template,ruleVersion'
        ],
        'UN_AUDIT_RULE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>
                'crew,publishUserGroup,applyUserGroup,applyCrew,publishCrew,templateVersion,template,ruleVersion'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UnAuditRuleRestfulTranslator();
        $this->resource = 'resourceCatalogs/unAuditedRules';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullUnAuditRule::getInstance();
    }

    protected function getMapErrors() : array
    {
        $mapError = RuleRestfulAdapter::MAP_ERROR;

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function addAction(UnAuditRule $unAuditRule) : bool
    {
        unset($unAuditRule);
        return false;
    }

    protected function editAction(UnAuditRule $unAuditRule) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditRule,
            array(
                'versionDescription',
                'rules',
                'crew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$unAuditRule->getId().'/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditRule);
            return true;
        }

        return false;
    }

    protected function rejectAction(IApplyAble $unAuditRule) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditRule,
            array(
                'rejectReason',
                'applyCrew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$unAuditRule->getId().'/reject',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditRule);
            return true;
        }
        return false;
    }
    
    protected function approveAction(IApplyAble $unAuditRule) : bool
    {
        $data = $this->getTranslator()->objectToArray($unAuditRule, array('applyCrew'));
       
        $this->patch(
            $this->getResource().'/'.$unAuditRule->getId().'/approve',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($unAuditRule);
            return true;
        }
        return false;
    }

    public function revoke(UnAuditRule $unAuditRule) : bool
    {
        $data = $this->getTranslator()->objectToArray($unAuditRule, array('crew'));
    
        $this->patch(
            $this->getResource().'/'.$unAuditRule->getId().'/'.'revoke',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($unAuditRule);
            return true;
        }

        return false;
    }
}
