<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\RuleVersion;

use Marmot\Interfaces\IAsyncAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;

interface IRuleVersionAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
}
