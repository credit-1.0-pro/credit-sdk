<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\RuleVersion;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\ResourceCatalog\Rule\Model\NullRuleVersion;
use Sdk\ResourceCatalog\Rule\Translator\RuleVersionRestfulTranslator;

class RuleVersionRestfulAdapter extends GuzzleAdapter implements IRuleVersionAdapter
{
    use FetchAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait;

    private $translator;
    
    private $resource;
    
    const SCENARIOS = [
        'RULE_VERSION_LIST'=>[
            'fields' => [],
            'include' => 'crew,publishUserGroup,templateVersion,template'
        ],
        'RULE_VERSION_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,publishUserGroup,templateVersion,template'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new RuleVersionRestfulTranslator();
        $this->resource = 'resourceCatalogs/ruleVersions';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullRuleVersion::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
