<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\RuleVersion;

use Sdk\ResourceCatalog\Rule\Utils\MockFactory;

class RuleVersionMockAdapter implements IRuleVersionAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateRuleVersion($id);
    }

    public function fetchList(array $ids) : array
    {
        $ruleList = array();

        foreach ($ids as $id) {
            $ruleList[] = MockFactory::generateRuleVersion($id);
        }

        return $ruleList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateRuleVersion($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $ruleList = array();

        foreach ($ids as $id) {
            $ruleList[] = MockFactory::generateRuleVersion($id);
        }

        return $ruleList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
