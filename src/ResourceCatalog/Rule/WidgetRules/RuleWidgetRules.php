<?php
namespace Sdk\ResourceCatalog\Rule\WidgetRules;

use Marmot\Core;
use Respect\Validation\Validator as V;

use Sdk\ResourceCatalog\Rule\Model\IRule;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleWidgetRules
{
    const VERSION_DESCRIPTION_MIN_LENGTH = 1;
    const VERSION_DESCRIPTION_MAX_LENGTH = 2000;
    public function versionDescription($versionDescription) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::VERSION_DESCRIPTION_MIN_LENGTH,
            self::VERSION_DESCRIPTION_MAX_LENGTH
        )->validate($versionDescription)) {
            Core::setLastError(RULE_VERSION_DESCRIPTION_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    const COMPLETION_RULE_MAX_COUNT = 2;
    const COMPARISON_RULE_MAX_COUNT = 2;

    const VALIDATE_RULES = [
        IRule::RULE_NAME['COMPLETION_RULE'] => 'validateCompletionRules',
        IRule::RULE_NAME['COMPARISON_RULE'] => 'validateComparisonRules',
        IRule::RULE_NAME['DE_DUPLICATION_RULE'] => 'validateDeDuplicationRules',
    ];

    public function rules($rules) : bool
    {
        if (!V::arrayType()->validate($rules)) {
            Core::setLastError(RULE_RULES_FORMAT_ERROR);
            return false;
        }

        foreach ($rules as $key => $rule) {
            if (!$this->ruleName($key) || !$this->ruleFormat($rule) || !$this->validateRule($key, $rule)) {
                return false;
            }
        }

        return true;
    }

    //验证规则的键是否在范围内
    protected function ruleName(string $name) : bool
    {
        if (!in_array($name, IRule::RULE_NAME)) {
            Core::setLastError(RULE_RULES_NAME_NOT_EXIT);
            return false;
        }

        return true;
    }

    protected function ruleFormat($rule) : bool
    {
        if (!V::arrayType()->validate($rule)) {
            Core::setLastError(RULE_RULES_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    protected function validateRule(string $name, array $rule) : bool
    {
        $validateRule = isset(self::VALIDATE_RULES[$name]) ? self::VALIDATE_RULES[$name] : '';

        return method_exists($this, $validateRule) ? $this->$validateRule($rule) : false;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function validateCompletionRules(array $completionRules) : bool
    {
        foreach ($completionRules as $completionRule) {
            if (!V::arrayType()->validate($completionRule)) {
                Core::setLastError(RULE_COMPLETION_RULES_FORMAT_ERROR);
                return false;
            }

            if (count($completionRule) > self::COMPLETION_RULE_MAX_COUNT) {
                Core::setLastError(RULE_COMPLETION_RULES_COUNT_ERROR);
                return false;
            }

            foreach ($completionRule as $each) {
                if (!isset($each['id'])
                || !isset($each['base'])
                || !isset($each['item'])) {
                    Core::setLastError(RULE_COMPLETION_RULES_FORMAT_ERROR);
                    return false;
                }

                if (!is_numeric($each['id'])) {
                    Core::setLastError(RULE_COMPLETION_RULES_TEMPLATE_FORMAT_ERROR);
                    return false;
                }

                if (!is_array($each['base'])) {
                    Core::setLastError(RULE_COMPLETION_RULES_BASE_FORMAT_ERROR);
                    return false;
                }

                if (!is_string($each['item'])) {
                    Core::setLastError(RULE_COMPLETION_RULES_TEMPLATE_ITEM_FORMAT_ERROR);
                    return false;
                }

                foreach ($each['base'] as $base) {
                    if (!V::numeric()->validate($base) || !in_array($base, IRule::COMPLETION_BASE)) {
                        Core::setLastError(RULE_COMPLETION_RULES_BASE_FORMAT_ERROR);
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function validateComparisonRules(array $comparisonRules) : bool
    {
        foreach ($comparisonRules as $comparisonRule) {
            if (!V::arrayType()->validate($comparisonRule)) {
                Core::setLastError(RULE_COMPARISON_RULES_FORMAT_ERROR);
                return false;
            }

            if (count($comparisonRule) > self::COMPARISON_RULE_MAX_COUNT) {
                Core::setLastError(RULE_COMPARISON_RULES_COUNT_ERROR);
                return false;
            }

            foreach ($comparisonRule as $each) {
                if (!isset($each['id'])
                || !isset($each['base'])
                || !isset($each['item'])) {
                    Core::setLastError(RULE_COMPARISON_RULES_FORMAT_ERROR);
                    return false;
                }

                if (!is_numeric($each['id'])) {
                    Core::setLastError(RULE_COMPARISON_RULES_TEMPLATE_FORMAT_ERROR);
                    return false;
                }

                if (!is_array($each['base'])) {
                    Core::setLastError(RULE_COMPARISON_RULES_BASE_FORMAT_ERROR);
                    return false;
                }

                if (!is_string($each['item'])) {
                    Core::setLastError(RULE_COMPARISON_RULES_TEMPLATE_ITEM_FORMAT_ERROR);
                    return false;
                }

                foreach ($each['base'] as $base) {
                    if (!V::numeric()->validate($base) || !in_array($base, IRule::COMPARISON_BASE)) {
                        Core::setLastError(RULE_COMPARISON_RULES_BASE_FORMAT_ERROR);
                        return false;
                    }
                }
            }
        }

        return true;
    }

    protected function validateDeDuplicationRules(array $deDuplicationRule) : bool
    {
        if (!isset($deDuplicationRule['result'])
        || !isset($deDuplicationRule['items'])) {
            Core::setLastError(RULE_DE_DUPLICATION_RULES_FORMAT_ERROR);
            return false;
        }

        if (!is_array($deDuplicationRule['items'])) {
            Core::setLastError(RULE_DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR);
            return false;
        }

        $result = $deDuplicationRule['result'];
        if (!V::numeric()->validate($result) || !in_array($result, IRule::DE_DUPLICATION_RESULT)) {
            Core::setLastError(RULE_DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR);
            return false;
        }

        return true;
    }
}
