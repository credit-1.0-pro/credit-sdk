<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\RejectCommandHandler;

use Sdk\ResourceCatalog\Rule\CommandHandler\RuleCommandHandlerTrait;

class RejectUnAuditRuleCommandHandler extends RejectCommandHandler
{
    use RuleCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditRule($id);
    }
}
