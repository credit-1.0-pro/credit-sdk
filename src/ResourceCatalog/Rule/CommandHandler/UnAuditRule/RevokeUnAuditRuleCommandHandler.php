<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\Rule\CommandHandler\RuleCommandHandlerTrait;
use Sdk\ResourceCatalog\Rule\Command\UnAuditRule\RevokeUnAuditRuleCommand;

class RevokeUnAuditRuleCommandHandler implements ICommandHandler
{
    use RuleCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof RevokeUnAuditRuleCommand)) {
            throw new \InvalidArgumentException;
        }
    
        $unAuditRule = $this->fetchUnAuditRule($command->id);
        
        return $unAuditRule->revoke();
    }
}
