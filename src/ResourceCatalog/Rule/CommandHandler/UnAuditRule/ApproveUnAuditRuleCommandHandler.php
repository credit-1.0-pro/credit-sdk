<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\ApproveCommandHandler;

use Sdk\ResourceCatalog\Rule\CommandHandler\RuleCommandHandlerTrait;

class ApproveUnAuditRuleCommandHandler extends ApproveCommandHandler
{
    use RuleCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditRule($id);
    }
}
