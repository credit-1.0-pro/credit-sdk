<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class UnAuditRuleCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\ResourceCatalog\Rule\Command\UnAuditRule\EditUnAuditRuleCommand'=>
            'Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule\EditUnAuditRuleCommandHandler',
        'Sdk\ResourceCatalog\Rule\Command\UnAuditRule\ApproveUnAuditRuleCommand'=>
            'Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule\ApproveUnAuditRuleCommandHandler',
        'Sdk\ResourceCatalog\Rule\Command\UnAuditRule\RejectUnAuditRuleCommand'=>
            'Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule\RejectUnAuditRuleCommandHandler',
        'Sdk\ResourceCatalog\Rule\Command\UnAuditRule\RevokeUnAuditRuleCommand'=>
            'Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule\RevokeUnAuditRuleCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
