<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\Rule\CommandHandler\RuleCommandHandlerTrait;
use Sdk\ResourceCatalog\Rule\Command\UnAuditRule\EditUnAuditRuleCommand;

class EditUnAuditRuleCommandHandler implements ICommandHandler
{
    use RuleCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditUnAuditRuleCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditRule = $this->fetchUnAuditRule($command->id);
        $unAuditRule = $this->ruleExecuteAction($command, $unAuditRule);
       
        return $unAuditRule->edit();
    }
}
