<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Repository\RuleRepository;
use Sdk\ResourceCatalog\Rule\Repository\UnAuditRuleRepository;

use Sdk\ResourceCatalog\Rule\Model\RuleVersion;
use Sdk\ResourceCatalog\Rule\Repository\RuleVersionRepository;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;

trait RuleCommandHandlerTrait
{
    protected function getRule() : Rule
    {
        return new Rule();
    }

    protected function getUnAuditRule() : UnAuditRule
    {
        return new UnAuditRule();
    }

    protected function getRepository() : RuleRepository
    {
        return new RuleRepository();
    }

    protected function getUnAuditRuleRepository() : UnAuditRuleRepository
    {
        return new UnAuditRuleRepository();
    }
    
    protected function fetchRule(int $id) : Rule
    {
        return $this->getRepository()->fetchOne($id);
    }
    
    protected function fetchUnAuditRule(int $id) : UnAuditRule
    {
        return $this->getUnAuditRuleRepository()->fetchOne($id);
    }

    protected function getRuleVersionRepository() : RuleVersionRepository
    {
        return new RuleVersionRepository();
    }
    
    protected function fetchRuleVersion(int $id) : RuleVersion
    {
        return $this->getRuleVersionRepository()->fetchOne($id);
    }

    protected function getTemplateRepository() : TemplateRepository
    {
        return new TemplateRepository();
    }

    protected function fetchTemplate(int $id) : Template
    {
        return $this->getTemplateRepository()->fetchOne($id);
    }

    protected function ruleExecuteAction(ICommand $command, Rule $rule) : Rule
    {
        $rule->setVersionDescription($command->versionDescription);
        $rule->setRules($command->rules);
        
        return $rule;
    }
}
