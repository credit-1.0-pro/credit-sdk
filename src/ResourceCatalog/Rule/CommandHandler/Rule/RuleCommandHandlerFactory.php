<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\Rule;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class RuleCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\ResourceCatalog\Rule\Command\Rule\AddRuleCommand'=>
            'Sdk\ResourceCatalog\Rule\CommandHandler\Rule\AddRuleCommandHandler',
        'Sdk\ResourceCatalog\Rule\Command\Rule\EditRuleCommand'=>
            'Sdk\ResourceCatalog\Rule\CommandHandler\Rule\EditRuleCommandHandler',
        'Sdk\ResourceCatalog\Rule\Command\Rule\VersionRestoreRuleCommand'=>
            'Sdk\ResourceCatalog\Rule\CommandHandler\Rule\VersionRestoreRuleCommandHandler',
        'Sdk\ResourceCatalog\Rule\Command\Rule\DeleteRuleCommand'=>
            'Sdk\ResourceCatalog\Rule\CommandHandler\Rule\DeleteRuleCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
