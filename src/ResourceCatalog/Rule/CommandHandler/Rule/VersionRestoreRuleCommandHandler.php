<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\Rule;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\Rule\CommandHandler\RuleCommandHandlerTrait;
use Sdk\ResourceCatalog\Rule\Command\Rule\VersionRestoreRuleCommand;

class VersionRestoreRuleCommandHandler implements ICommandHandler
{
    use RuleCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof VersionRestoreRuleCommand)) {
            throw new \InvalidArgumentException;
        }
    
        $ruleVersion = $this->fetchRuleVersion($command->ruleVersionId);
        $rule = $this->fetchRule($command->id);
        $rule->setRuleVersion($ruleVersion);
        
        return $rule->versionRestore();
    }
}
