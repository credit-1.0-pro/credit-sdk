<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\Rule;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\Rule\Command\Rule\EditRuleCommand;
use Sdk\ResourceCatalog\Rule\CommandHandler\RuleCommandHandlerTrait;

class EditRuleCommandHandler implements ICommandHandler
{
    use RuleCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditRuleCommand)) {
            throw new \InvalidArgumentException;
        }

        $rule = $this->fetchRule($command->id);
        $rule = $this->ruleExecuteAction($command, $rule);

        return $rule->edit();
    }
}
