<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\Rule;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\Rule\Command\Rule\AddRuleCommand;
use Sdk\ResourceCatalog\Rule\CommandHandler\RuleCommandHandlerTrait;

class AddRuleCommandHandler implements ICommandHandler
{
    use RuleCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddRuleCommand)) {
            throw new \InvalidArgumentException;
        }

        $template = $this->fetchTemplate($command->template);

        $rule = $this->getRule();
        $rule->setTemplate($template);
        $rule = $this->ruleExecuteAction($command, $rule);

        return $rule->add();
    }
}
