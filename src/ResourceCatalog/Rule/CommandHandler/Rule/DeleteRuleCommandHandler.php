<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\Rule;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\Rule\Command\Rule\DeleteRuleCommand;
use Sdk\ResourceCatalog\Rule\CommandHandler\RuleCommandHandlerTrait;

class DeleteRuleCommandHandler implements ICommandHandler
{
    use RuleCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof DeleteRuleCommand)) {
            throw new \InvalidArgumentException;
        }
    
        $rule = $this->fetchRule($command->id);
        
        return $rule->delete();
    }
}
