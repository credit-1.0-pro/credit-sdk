<?php
namespace Sdk\ResourceCatalog\Rule\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ApplyAbleRepositoryTrait;
use Sdk\Common\Repository\OperateAbleRepositoryTrait;

use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule\IUnAuditRuleAdapter;
use Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule\UnAuditRuleMockAdapter;
use Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule\UnAuditRuleRestfulAdapter;

class UnAuditRuleRepository extends Repository implements IUnAuditRuleAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        ApplyAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'UN_AUDIT_RULE_LIST';
    const FETCH_ONE_MODEL_UN = 'UN_AUDIT_RULE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new UnAuditRuleRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new UnAuditRuleMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function revoke(UnAuditRule $unAuditRule) : bool
    {
        return $this->getAdapter()->revoke($unAuditRule);
    }
}
