<?php
namespace Sdk\ResourceCatalog\Rule\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\OperateAbleRepositoryTrait;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Adapter\Rule\IRuleAdapter;
use Sdk\ResourceCatalog\Rule\Adapter\Rule\RuleMockAdapter;
use Sdk\ResourceCatalog\Rule\Adapter\Rule\RuleRestfulAdapter;

class RuleRepository extends Repository implements IRuleAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'RULE_LIST';
    const FETCH_ONE_MODEL_UN = 'RULE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new RuleRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new RuleMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function versionRestore(Rule $rule) : bool
    {
        return $this->getAdapter()->versionRestore($rule);
    }

    public function deleted(Rule $rule) : bool
    {
        return $this->getAdapter()->deleted($rule);
    }
}
