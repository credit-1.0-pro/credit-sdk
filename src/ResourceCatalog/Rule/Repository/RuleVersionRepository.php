<?php
namespace Sdk\ResourceCatalog\Rule\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;

use Sdk\ResourceCatalog\Rule\Adapter\RuleVersion\IRuleVersionAdapter;
use Sdk\ResourceCatalog\Rule\Adapter\RuleVersion\RuleVersionMockAdapter;
use Sdk\ResourceCatalog\Rule\Adapter\RuleVersion\RuleVersionRestfulAdapter;

class RuleVersionRepository extends Repository implements IRuleVersionAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'RULE_VERSION_LIST';
    const FETCH_ONE_MODEL_UN = 'RULE_VERSION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new RuleVersionRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new RuleVersionMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
