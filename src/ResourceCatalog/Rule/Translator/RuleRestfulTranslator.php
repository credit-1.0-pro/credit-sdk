<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Model\NullRule;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class RuleRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $rule = null)
    {
        return $this->translateToObject($expression, $rule);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    protected function getTemplateRestfulTranslator(): TemplateRestfulTranslator
    {
        return new TemplateRestfulTranslator();
    }

    protected function getTemplateVersionRestfulTranslator(): TemplateVersionRestfulTranslator
    {
        return new TemplateVersionRestfulTranslator();
    }

    protected function getRuleVersionRestfulTranslator(): RuleVersionRestfulTranslator
    {
        return new RuleVersionRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $rule = null)
    {
        if (empty($expression)) {
            return new NullRule();
        }

        if ($rule == null) {
            $rule = new Rule();
        }
       
        $data = $expression['data'];
        
        $id = $data['id'];
        $rule->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['rules'])) {
            $rule->setRules($attributes['rules']);
        }
        if (isset($attributes['createTime'])) {
            $rule->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['statusTime'])) {
            $rule->setStatusTime($attributes['statusTime']);
        }

        if (isset($attributes['updateTime'])) {
            $rule->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['status'])) {
            $rule->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
    
        if (isset($relationships['crew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['crew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;

            $rule->setCrew($crew);
        }

        if (isset($relationships['publishUserGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['publishUserGroup']['data']);
            $rule->setUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($userGroup));
        }
        
        if (isset($relationships['template']['data'])) {
            $template = $this->changeArrayFormat($relationships['template']['data']);
            $rule->setTemplate(
                $this->getTemplateRestfulTranslator()->arrayToObject($template)
            );
        }
        if (isset($relationships['templateVersion']['data'])) {
            $templateVersion = $this->changeArrayFormat($relationships['templateVersion']['data']);
            $rule->setTemplateVersion(
                $this->getTemplateVersionRestfulTranslator()->arrayToObject($templateVersion)
            );
        }
        if (isset($relationships['ruleVersion']['data'])) {
            $ruleVersion = $this->changeArrayFormat($relationships['ruleVersion']['data']);
            $rule->setRuleVersion(
                $this->getRuleVersionRestfulTranslator()->arrayToObject($ruleVersion)
            );
        }

        return $rule;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($rule, array $keys = array())
    {
        if (!$rule instanceof Rule) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'versionDescription',
                'rules',
                'ruleVersion',
                'templateVersion',
                'template',
                'crew'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'rules'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $rule->getId();
        }

        $attributes = array();

        if (in_array('versionDescription', $keys)) {
            $attributes['versionDescription'] = $rule->getVersionDescription();
        }
        if (in_array('rules', $keys)) {
            $attributes['rules'] = $rule->getRules();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $rule->getCrew()->getId()
                )
            );
        }

        if (in_array('template', $keys)) {
            $expression['data']['relationships']['template']['data'] = array(
                array(
                    'type' => 'templates',
                    'id' => $rule->getTemplate()->getId()
                )
            );
        }

        if (in_array('templateVersion', $keys)) {
            $expression['data']['relationships']['templateVersion']['data'] = array(
                array(
                    'type' => 'templateVersions',
                    'id' => $rule->getTemplateVersion()->getId()
                )
            );
        }

        if (in_array('ruleVersion', $keys)) {
            $expression['data']['relationships']['ruleVersion']['data'] = array(
                array(
                    'type' => 'ruleVersions',
                    'id' => $rule->getRuleVersion()->getId()
                )
            );
        }
        
        return $expression;
    }
}
