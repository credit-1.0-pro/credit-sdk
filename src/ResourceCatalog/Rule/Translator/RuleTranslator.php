<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Model\NullRule;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

use Sdk\ResourceCatalog\Template\Translator\TemplateTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionTranslator;

class RuleTranslator implements ITranslator
{
    use RuleTranslatorTrait;
    
    public function arrayToObject(array $expression, $rule = null)
    {
        unset($rule);
        unset($expression);
        return new NullRule();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getTemplateTranslator() : TemplateTranslator
    {
        return new TemplateTranslator();
    }

    protected function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return new TemplateVersionTranslator();
    }

    protected function getRuleVersionTranslator() : RuleVersionTranslator
    {
        return new RuleVersionTranslator();
    }

   /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($rule, array $keys = array())
    {
        if (!$rule instanceof Rule) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'rules',
                'crew' => [],
                'userGroup'=> [],
                'ruleVersion'=> [],
                'template'=> [],
                'templateVersion'=> [],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($rule->getId());
        }
        if (in_array('rules', $keys)) {
            $expression['rules'] = $this->getRulesFormatConversion($rule->getRules());
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($rule->getStatus()),
                'name' => Rule::STATUS_CN[$rule->getStatus()]
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $rule->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $rule->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $rule->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $rule->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $rule->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $rule->getCreateTime());
        }
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $rule->getCrew(),
                $keys['crew']
            );
        }
        if (isset($keys['userGroup'])) {
            $expression['publishUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $rule->getUserGroup(),
                $keys['userGroup']
            );
        }
        if (isset($keys['ruleVersion'])) {
            $expression['ruleVersion'] = $this->getRuleVersionTranslator()->objectToArray(
                $rule->getRuleVersion(),
                $keys['ruleVersion']
            );
        }
        if (isset($keys['templateVersion'])) {
            $expression['templateVersion'] = $this->getTemplateVersionTranslator()->objectToArray(
                $rule->getTemplateVersion(),
                $keys['templateVersion']
            );
        }
        if (isset($keys['template'])) {
            $expression['template'] = $this->getTemplateTranslator()->objectToArray(
                $rule->getTemplate(),
                $keys['template']
            );
        }

        return $expression;
    }
}
