<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\ResourceCatalog\Rule\Model\RuleVersion;
use Sdk\ResourceCatalog\Rule\Model\NullRuleVersion;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;

class RuleVersionRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $ruleVersion = null)
    {
        return $this->translateToObject($expression, $ruleVersion);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getTemplateVersionRestfulTranslator(): TemplateVersionRestfulTranslator
    {
        return new TemplateVersionRestfulTranslator();
    }

    protected function getTemplateRestfulTranslator(): TemplateRestfulTranslator
    {
        return new TemplateRestfulTranslator();
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $ruleVersion = null)
    {
        if (empty($expression)) {
            return new NullRuleVersion();
        }

        if ($ruleVersion == null) {
            $ruleVersion = new RuleVersion();
        }
       
        $data = $expression['data'];
        
        $id = $data['id'];
        $ruleVersion->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['number'])) {
            $ruleVersion->setNumber($attributes['number']);
        }
        if (isset($attributes['description'])) {
            $ruleVersion->setDescription($attributes['description']);
        }
        if (isset($attributes['ruleId'])) {
            $ruleVersion->setRuleId($attributes['ruleId']);
        }
        if (isset($attributes['info'])) {
            $ruleVersion->setInfo($attributes['info']);
        }
        if (isset($attributes['createTime'])) {
            $ruleVersion->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['statusTime'])) {
            $ruleVersion->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['updateTime'])) {
            $ruleVersion->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $ruleVersion->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
    
        if (isset($relationships['crew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['crew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;

            $ruleVersion->setCrew($crew);
        }

        if (isset($relationships['template']['data'])) {
            $template = $this->changeArrayFormat($relationships['template']['data']);
            $ruleVersion->setTemplate($this->getTemplateRestfulTranslator()->arrayToObject($template));
        }

        if (isset($relationships['templateVersion']['data'])) {
            $templateVersion = $this->changeArrayFormat($relationships['templateVersion']['data']);
            $ruleVersion->setTemplateVersion(
                $this->getTemplateVersionRestfulTranslator()->arrayToObject($templateVersion)
            );
        }

        if (isset($relationships['publishUserGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['publishUserGroup']['data']);
            $ruleVersion->setUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($userGroup));
        }

        return $ruleVersion;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($ruleVersion, array $keys = array())
    {
        if (!$ruleVersion instanceof RuleVersion) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'description',
                'ruleId',
                'info',
                'crew',
                'template',
                'templateVersion'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'ruleVersions'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $ruleVersion->getId();
        }

        $attributes = array();

        if (in_array('number', $keys)) {
            $attributes['number'] = $ruleVersion->getNumber();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $ruleVersion->getDescription();
        }
        if (in_array('ruleId', $keys)) {
            $attributes['ruleId'] = $ruleVersion->getRuleId();
        }
        if (in_array('info', $keys)) {
            $attributes['info'] = $ruleVersion->getInfo();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $ruleVersion->getCrew()->getId()
                )
            );
        }

        if (in_array('template', $keys)) {
            $expression['data']['relationships']['template']['data'] = array(
                array(
                    'type' => 'templates',
                    'id' => $ruleVersion->getTemplate()->getId()
                )
            );
        }

        if (in_array('templateVersion', $keys)) {
            $expression['data']['relationships']['templateVersion']['data'] = array(
                array(
                    'type' => 'templateVersions',
                    'id' => $ruleVersion->getTemplateVersion()->getId()
                )
            );
        }

        return $expression;
    }
}
