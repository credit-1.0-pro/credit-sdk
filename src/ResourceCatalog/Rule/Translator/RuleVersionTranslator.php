<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Sdk\ResourceCatalog\Rule\Model\RuleVersion;
use Sdk\ResourceCatalog\Rule\Model\NullRuleVersion;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionTranslator;

class RuleVersionTranslator implements ITranslator
{
    use RuleTranslatorTrait;
    
    public function arrayToObject(array $expression, $ruleVersion = null)
    {
        unset($ruleVersion);
        unset($expression);
        return new NullRuleVersion();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return new TemplateVersionTranslator();
    }

    protected function getTemplateTranslator() : TemplateTranslator
    {
        return new TemplateTranslator();
    }
   /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($ruleVersion, array $keys = array())
    {
        if (!$ruleVersion instanceof RuleVersion) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'description',
                'ruleId',
                'info',
                'crew' => [],
                'userGroup'=> [],
                'template'=> [],
                'templateVersion'=> [],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($ruleVersion->getId());
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $ruleVersion->getNumber();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $ruleVersion->getDescription();
        }
        if (in_array('ruleId', $keys)) {
            $expression['ruleId'] = marmot_encode($ruleVersion->getRuleId());
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($ruleVersion->getStatus()),
                'name' => RuleVersion::STATUS_CN[$ruleVersion->getStatus()]
            ];
        }
        if (in_array('info', $keys)) {
            $expression['info'] = $this->getInfoCn($ruleVersion->getInfo());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $ruleVersion->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $ruleVersion->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $ruleVersion->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $ruleVersion->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $ruleVersion->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $ruleVersion->getCreateTime());
        }
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $ruleVersion->getCrew(),
                $keys['crew']
            );
        }
        if (isset($keys['userGroup'])) {
            $expression['publishUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $ruleVersion->getUserGroup(),
                $keys['userGroup']
            );
        }
        if (isset($keys['template'])) {
            $expression['template'] = $this->getTemplateTranslator()->objectToArray(
                $ruleVersion->getTemplate(),
                $keys['template']
            );
        }
        if (isset($keys['templateVersion'])) {
            $expression['templateVersion'] = $this->getTemplateVersionTranslator()->objectToArray(
                $ruleVersion->getTemplateVersion(),
                $keys['templateVersion']
            );
        }

        return $expression;
    }
}
