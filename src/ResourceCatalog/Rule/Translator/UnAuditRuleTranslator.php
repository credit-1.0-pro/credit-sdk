<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Model\NullUnAuditRule;

use Sdk\Common\Translator\ApplyTranslatorTrait;

class UnAuditRuleTranslator extends RuleTranslator implements ITranslator
{
    use ApplyTranslatorTrait;

    public function arrayToObject(array $expression, $unAuditedRule = null)
    {
        unset($unAuditedRule);
        unset($expression);
        return new NullUnAuditRule();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function objectToArray($unAuditedRule, array $keys = array())
    {
        if (!$unAuditedRule instanceof UnAuditRule) {
            return array();
        }

        $rule = parent::objectToArray($unAuditedRule, $keys);

        $apply = $this->applyObjectToArray($unAuditedRule, $keys);

        $unAuditedRuleArray = array_merge($rule, $apply);

        return $unAuditedRuleArray;
    }
}
