<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\ApplyRestfulTranslatorTrait;

use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Model\NullUnAuditRule;

class UnAuditRuleRestfulTranslator extends RuleRestfulTranslator implements IRestfulTranslator
{
    use ApplyRestfulTranslatorTrait;

    public function arrayToObject(array $expression, $unAuditedRule = null)
    {
        return $this->translateToObject($expression, $unAuditedRule);
    }

    protected function translateToObject(array $expression, $unAuditedRule = null)
    {
        if (empty($expression)) {
            return new NullUnAuditRule();
        }

        if ($unAuditedRule == null) {
            $unAuditedRule = new UnAuditRule();
        }
        
        $unAuditedRule = parent::translateToObject($expression, $unAuditedRule);

        $unAuditedRule = $this->applyTranslateToObject($expression, $unAuditedRule);

        return $unAuditedRule;
    }

    public function objectToArray($unAuditedRule, array $keys = array())
    {
        if (!$unAuditedRule instanceof UnAuditRule) {
            return array();
        }

        $expression = array(
            'data' => array(
                'type' => 'unAuditedRules'
            )
        );

        $rule = parent::objectToArray($unAuditedRule, $keys);

        $apply = $this->applyObjectToArray($unAuditedRule, $keys);

        $ruleAttributes = isset($rule['data']['attributes']) ? $rule['data']['attributes'] : array();
        $applyAttributes = isset($apply['data']['attributes']) ? $apply['data']['attributes'] : array();
        $expression['data']['attributes'] = array_merge($ruleAttributes, $applyAttributes);

        $ruleRelationships = isset($rule['data']['relationships']) ?
                                    $rule['data']['relationships'] :
                                    array();
        $applyRelationships = isset($apply['data']['relationships']) ? $apply['data']['relationships'] : array();
        $expression['data']['relationships'] = array_merge($ruleRelationships, $applyRelationships);

        return $expression;
    }
}
