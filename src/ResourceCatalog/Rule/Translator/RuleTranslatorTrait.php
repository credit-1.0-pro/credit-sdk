<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Sdk\ResourceCatalog\Rule\Model\IRule;

trait RuleTranslatorTrait
{
    protected function getRulesFormatConversion(array $rules) : array
    {
        $formatConversionMethod = array(
            IRule::RULE_NAME['COMPLETION_RULE'] => 'completionRulesFormatConversion',
            IRule::RULE_NAME['COMPARISON_RULE'] => 'comparisonRulesFormatConversion',
            IRule::RULE_NAME['DE_DUPLICATION_RULE'] => 'deDuplicationRulesFormatConversion',
        );

        $rulesFormatConversion = array();
        foreach ($rules as $name => $rule) {
            $method = isset($formatConversionMethod[$name]) ? $formatConversionMethod[$name] : '';

            $rulesFormatConversion[$name] = method_exists($this, $method) ? $this->$method($rule) : $rule;
        }

        return $rulesFormatConversion;
    }

    protected function completionRulesFormatConversion(array $rule) : array
    {
        foreach ($rule as $identify => $condition) {
            foreach ($condition as $key => $parent) {
                if (isset($parent['id'])) {
                    $rule[$identify][$key]['id'] = marmot_encode($parent['id']);
                }
                if (isset($parent['versionId'])) {
                    $rule[$identify][$key]['versionId'] = marmot_encode($parent['versionId']);
                }
                if (isset($parent['base'])) {
                    $rule[$identify][$key]['base'] = $this->getCompletionBaseCn($parent['base']);
                }
            }
        }

        return $rule;
    }

    protected function getCompletionBaseCn(array $base) : array
    {
        $baseCn = array();
        foreach ($base as $key => $value) {
            unset($key);
            $baseCn[] = array(
                'id' => marmot_encode($value),
                'name' => isset(IRule::COMPLETION_BASE_CN[$value]) ? IRule::COMPLETION_BASE_CN[$value] : ''
            );
        }

        return $baseCn;
    }

    protected function comparisonRulesFormatConversion(array $rule) : array
    {
        foreach ($rule as $identify => $condition) {
            foreach ($condition as $key => $parent) {
                if (isset($parent['id'])) {
                    $rule[$identify][$key]['id'] = marmot_encode($parent['id']);
                }
                if (isset($parent['versionId'])) {
                    $rule[$identify][$key]['versionId'] = marmot_encode($parent['versionId']);
                }
                if (isset($parent['base'])) {
                    $rule[$identify][$key]['base'] = $this->getComparisonBaseCn($parent['base']);
                }
            }
        }

        return $rule;
    }

    protected function getComparisonBaseCn(array $base) : array
    {
        $baseCn = array();
        foreach ($base as $key => $value) {
            unset($key);
            $baseCn[] = array(
                'id' => marmot_encode($value),
                'name' => isset(IRule::COMPARISON_BASE_CN[$value]) ? IRule::COMPARISON_BASE_CN[$value] : ''
            );
        }

        return $baseCn;
    }
    
    protected function deDuplicationRulesFormatConversion(array $rule) : array
    {
        if (isset($rule['result'])) {
            $rule['result'] = $this->getDeDuplicationResultCn($rule['result']);
        }

        return $rule;
    }

    protected function getDeDuplicationResultCn(int $result) : array
    {
        $data = array(
            'id' => marmot_encode($result),
            'name' => isset(IRule::DE_DUPLICATION_RESULT_CN[$result]) ?
                        IRule::DE_DUPLICATION_RESULT_CN[$result] :
                        ''
        );

        return $data;
    }

    protected function getInfoCn(array $info) : array
    {
        $info['rules'] = isset($info['rules']) ? $this->getRulesFormatConversion($info['rules']) : array();

        return $info;
    }
}
