<?php
namespace Sdk\ResourceCatalog\SearchData\Model;

use Sdk\Common\Model\IApplyAble;

use Sdk\ResourceCatalog\SearchData\Repository\SearchDataRepository;
use Sdk\ResourceCatalog\ResourceCatalogData\Model\ResourceCatalogData;

class SearchData extends ResourceCatalogData implements IApplyAble
{

    /**
     * 审核状态
     * @var ['DEFAULT'] 默认
     * @var ['PENDING'] 待审核
     * @var ['APPROVE'] 审核通过
     * @var ['REJECT'] 审核驳回
     */
    const DATA_APPLY_STATUS = array(
        'DEFAULT' => 0,  //默认
        'PENDING' => 2, //待审核
        'APPROVE' => 4, //审核通过
        'REJECT' => -4, //审核驳回
    );
    
    const DATA_APPLY_STATUS_CN = array(
        self::DATA_APPLY_STATUS['DEFAULT'] => '默认',
        self::DATA_APPLY_STATUS['PENDING'] => '待审核',
        self::DATA_APPLY_STATUS['APPROVE'] => '已通过',
        self::DATA_APPLY_STATUS['REJECT'] => '已驳回'
    );
    
    const DATA_APPLY_STATUS_TYPE = array(
        self::DATA_APPLY_STATUS['DEFAULT'] => 'success',
        self::DATA_APPLY_STATUS['PENDING'] => 'waring',
        self::DATA_APPLY_STATUS['APPROVE'] => 'success',
        self::DATA_APPLY_STATUS['REJECT'] => 'danger'
    );

    /**
     * 状态
     * @var ['NORMAL'] 正常
     * @var ['DELETED'] 屏蔽
     */
    const STATUS = array(
        'NORMAL' => 0,  //正常
        'DELETED' => -2 //屏蔽
    );

    const STATUS_CN = array(
        self::STATUS['NORMAL'] => '正常',
        self::STATUS['DELETED'] => '已屏蔽'
    );

    const STATUS_TYPE = array(
        self::STATUS['NORMAL'] => 'success',
        self::STATUS['DELETED'] => 'danger'
    );

    protected $repository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->status = self::STATUS['NORMAL'];
        $this->applyStatus = self::DATA_APPLY_STATUS['DEFAULT'];
        $this->rejectReason = '';
        $this->repository = new SearchDataRepository();
    }

    public function __destruct()
    {
        unset($this->status);
        unset($this->applyStatus);
        unset($this->rejectReason);
        unset($this->repository);
    }

    public function setApplyStatus(int $applyStatus) : void
    {
        $this->applyStatus = in_array(
            $applyStatus,
            self::DATA_APPLY_STATUS
        ) ? $applyStatus : self::DATA_APPLY_STATUS['DEFAULT'];
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }

    public function setRejectReason(string $rejectReason) : void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['NORMAL'];
    }
    
    public function getRepository()
    {
        return $this->repository;
    }

    public function approve() : bool
    {
        return $this->getRepository()->approve($this);
    }

    public function reject() : bool
    {
        return $this->getRepository()->reject($this);
    }

    public function onlineAdd() : bool
    {
        return $this->getRepository()->onlineAdd($this);
    }

    public function deleted() : bool
    {
        return $this->getRepository()->deleted($this);
    }
}
