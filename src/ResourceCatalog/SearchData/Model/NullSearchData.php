<?php
namespace Sdk\ResourceCatalog\SearchData\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullSearchData extends SearchData implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function approve() : bool
    {
        return $this->resourceNotExist();
    }

    public function reject() : bool
    {
        return $this->resourceNotExist();
    }

    public function onlineAdd() : bool
    {
        return $this->resourceNotExist();
    }

    public function deleted() : bool
    {
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
