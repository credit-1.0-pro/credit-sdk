<?php
namespace Sdk\ResourceCatalog\SearchData\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\Model\NullSearchData;

use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;
use Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class SearchDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $searchData = null)
    {
        return $this->translateToObject($expression, $searchData);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getTemplateVersionRestfulTranslator(): TemplateVersionRestfulTranslator
    {
        return new TemplateVersionRestfulTranslator();
    }

    protected function getItemsDataRestfulTranslator(): ItemsDataRestfulTranslator
    {
        return new ItemsDataRestfulTranslator();
    }

    protected function translateToObject(array $expression, $searchData = null)
    {
        if (empty($expression)) {
            return new NullSearchData();
        }

        if ($searchData == null) {
            $searchData = new SearchData();
        }
       
        $data = $expression['data'];
        
        $id = $data['id'];
        $searchData->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $searchData->setName($attributes['name']);
        }
        if (isset($attributes['identify'])) {
            $searchData->setIdentify($attributes['identify']);
        }
        if (isset($attributes['expirationDate'])) {
            $searchData->setExpirationDate($attributes['expirationDate']);
        }
        if (isset($attributes['subjectCategory'])) {
            $searchData->setSubjectCategory($attributes['subjectCategory']);
        }
        if (isset($attributes['dimension'])) {
            $searchData->setDimension($attributes['dimension']);
        }
        if (isset($attributes['exchangeFrequency'])) {
            $searchData->setExchangeFrequency($attributes['exchangeFrequency']);
        }
        if (isset($attributes['infoClassify'])) {
            $searchData->setInfoClassify($attributes['infoClassify']);
        }
        if (isset($attributes['infoCategory'])) {
            $searchData->setInfoCategory($attributes['infoCategory']);
        }
        if (isset($attributes['applyStatus'])) {
            $searchData->setApplyStatus($attributes['applyStatus']);
        }
        if (isset($attributes['rejectReason'])) {
            $searchData->setRejectReason($attributes['rejectReason']);
        }
        if (isset($attributes['createTime'])) {
            $searchData->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['statusTime'])) {
            $searchData->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['updateTime'])) {
            $searchData->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $searchData->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['publishUserGroup']['data'])) {
            $publishUserGroup = $this->changeArrayFormat($relationships['publishUserGroup']['data']);
            $searchData->setPublishUserGroup(
                $this->getUserGroupRestfulTranslator()->arrayToObject($publishUserGroup)
            );
        }
        if (isset($relationships['templateVersion']['data'])) {
            $templateVersion = $this->changeArrayFormat($relationships['templateVersion']['data']);
            $searchData->setTemplateVersion(
                $this->getTemplateVersionRestfulTranslator()->arrayToObject($templateVersion)
            );
        }
        if (isset($relationships['itemsData']['data'])) {
            $itemsData = $this->changeArrayFormat($relationships['itemsData']['data']);
            $searchData->setItemsData(
                $this->getItemsDataRestfulTranslator()->arrayToObject($itemsData)
            );
        }

        return $searchData;
    }

    public function objectToArray($searchData, array $keys = array())
    {
        if (!$searchData instanceof SearchData) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'rejectReason',
                'expirationDate',
                'crew',
                'template',
                'itemsData'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'searchData'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $searchData->getId();
        }

        $attributes = array();

        if (in_array('expirationDate', $keys)) {
            $attributes['expirationDate'] = $searchData->getExpirationDate();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $searchData->getRejectReason();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $searchData->getCrew()->getId()
                )
            );
        }
        if (in_array('template', $keys)) {
            $expression['data']['relationships']['template']['data'] = array(
                array(
                    'type' => 'templates',
                    'id' => $searchData->getTemplate()->getId()
                )
            );
        }

        if (in_array('itemsData', $keys)) {
            $expression['data']['relationships']['itemsData']['data'] = array(
                array(
                    'type' => 'itemsData',
                    'attributes' => array(
                        'data' => $searchData->getItemsData()->getData()
                    )
                )
            );
        }
    
        return $expression;
    }
}
