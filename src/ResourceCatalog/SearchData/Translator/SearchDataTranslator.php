<?php
namespace Sdk\ResourceCatalog\SearchData\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\Model\NullSearchData;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionTranslator;
use Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataTranslator;

class SearchDataTranslator implements ITranslator
{

    public function arrayToObject(array $expression, $searchData = null)
    {
        unset($searchData);
        unset($expression);
        return new NullSearchData();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return new TemplateVersionTranslator();
    }

    protected function getItemsDataTranslator() : ItemsDataTranslator
    {
        return new ItemsDataTranslator();
    }

   /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($searchData, array $keys = array())
    {
        if (!$searchData instanceof SearchData) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'expirationDate',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'applyStatus',
                'rejectReason',
                'status',
                'createTime',
                'updateTime',
                'statusTime',
                'itemsData' => [],
                'templateVersion' => [],
                'publishUserGroup' => [],
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($searchData->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $searchData->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $searchData->getIdentify();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $searchData->getRejectReason();
        }
        if (in_array('expirationDate', $keys)) {
            $expression['expirationDate'] = $searchData->getExpirationDate();
            $expression['expirationDateFormat'] = date('Y年m月d日', $searchData->getExpirationDate());
        }
        if (in_array('subjectCategory', $keys)) {
            $expression['subjectCategory'] = $this->typeFormatConversion(
                $searchData->getSubjectCategory(),
                Template::SUBJECT_CATEGORY_CN
            );
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension'] = $this->typeFormatConversion(
                $searchData->getDimension(),
                Template::DIMENSION_CN
            );
        }
        if (in_array('exchangeFrequency', $keys)) {
            $expression['exchangeFrequency'] = $this->typeFormatConversion(
                $searchData->getExchangeFrequency(),
                Template::EXCHANGE_FREQUENCY_CN
            );
        }
        if (in_array('infoClassify', $keys)) {
            $expression['infoClassify'] = $this->typeFormatConversion(
                $searchData->getInfoClassify(),
                Template::INFO_CLASSIFY_CN
            );
        }
        if (in_array('infoCategory', $keys)) {
            $expression['infoCategory'] = $this->typeFormatConversion(
                $searchData->getInfoCategory(),
                Template::INFO_CATEGORY_CN
            );
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = $this->statusFormatConversion(
                $searchData->getApplyStatus(),
                SearchData::DATA_APPLY_STATUS_CN,
                SearchData::DATA_APPLY_STATUS_TYPE
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion(
                $searchData->getStatus(),
                SearchData::STATUS_CN,
                SearchData::STATUS_TYPE
            );
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $searchData->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $searchData->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $searchData->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $searchData->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $searchData->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $searchData->getCreateTime());
        }
        if (isset($keys['itemsData'])) {
            $expression['itemsData'] = $this->getItemsDataTranslator()->objectToArray(
                $searchData->getItemsData(),
                $keys['itemsData']
            );
        }
        if (isset($keys['publishUserGroup'])) {
            $expression['publishUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $searchData->getPublishUserGroup(),
                $keys['publishUserGroup']
            );
        }
        if (isset($keys['templateVersion'])) {
            $expression['templateVersion'] = $this->getTemplateVersionTranslator()->objectToArray(
                $searchData->getTemplateVersion(),
                $keys['templateVersion']
            );
        }

        return $expression;
    }

    protected function typeFormatConversion(int $id, array $typeCn) : array
    {
        $data = array(
            'id' => marmot_encode($id),
            'name' => isset($typeCn[$id]) ? $typeCn[$id] : ''
        );

        return $data;
    }

    protected function statusFormatConversion(int $id, array $statusCn, array $statusType) : array
    {
        $data = array(
            'id' => marmot_encode($id),
            'name' => isset($statusCn[$id]) ? $statusCn[$id] : '',
            'type' => isset($statusType[$id]) ? $statusType[$id] : '',
        );

        return $data;
    }
}
