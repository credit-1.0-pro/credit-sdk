<?php
namespace Sdk\ResourceCatalog\SearchData\Command\SearchData;

use Marmot\Interfaces\ICommand;

class DeletedSearchDataCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}
