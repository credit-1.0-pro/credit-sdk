<?php
namespace Sdk\ResourceCatalog\SearchData\Command\SearchData;

use Marmot\Interfaces\ICommand;

class OnlineAddSearchDataCommand implements ICommand
{
    public $id;

    public $itemsData;
    
    public $expirationDate;

    public $template;

    public function __construct(
        array $itemsData,
        int $expirationDate,
        int $template,
        int $id = 0
    ) {
        $this->itemsData = $itemsData;
        $this->expirationDate = $expirationDate;
        $this->template = $template;
        $this->id = $id;
    }
}
