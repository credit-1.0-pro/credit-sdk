<?php
namespace Sdk\ResourceCatalog\SearchData\Adapter\SearchData;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\Model\NullSearchData;
use Sdk\ResourceCatalog\SearchData\Translator\SearchDataRestfulTranslator;

class SearchDataRestfulAdapter extends GuzzleAdapter implements ISearchDataAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait,
        ApplyAbleRestfulAdapterTrait;

    const MAP_ERROR = [
        101 => array(
            'itemsData' => RESOURCE_CATALOG_DATA_ITEMS_DATA_FORMAT_ERROR,
            'expirationDate'=>RESOURCE_CATALOG_DATA_EXPIRATION_DATE_FORMAT_ERROR,
            'crewId' => CREW_ID_FORMAT_ERROR,
            'templateId'=>RESOURCE_CATALOG_DATA_TEMPLATE_FORMAT_ERROR,
            'rejectReason' => REASON_FORMAT_ERROR,
            'subjectCategory'=>RESOURCE_CATALOG_DATA_SUBJECT_CATEGORY_FORMAT_ERROR
        ),
        100 => array(
            'crew' => PARAMETER_IS_EMPTY,
            'template' => RESOURCE_CATALOG_DATA_TEMPLATE_NOT_EXIT
        ),
        102 => array(
            'status' => STATUS_CAN_NOT_MODIFY,
            'applyStatus' => APPLY_STATUE_CAN_NOT_MODIFY,
        ),
        103 => RESOURCE_CATALOG_DATA_IS_UNIQUE,
        1101 => RESOURCE_CATALOG_DATA_ITEMS_DATA_FORMAT_ERROR
    ];
        
    private $translator;
    
    private $resource;
    
    const SCENARIOS = [
        'SEARCH_DATA_LIST'=>[
            'fields' => [],
            'include' => 'crew,publishUserGroup,templateVersion,template,ruleVersion,rule,task,itemsData'
        ],
        'SEARCH_DATA_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,publishUserGroup,templateVersion,template,ruleVersion,rule,task,itemsData'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new SearchDataRestfulTranslator();
        $this->resource = 'resourceCatalogs/searchData';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullSearchData::getInstance();
    }

    protected function mapErrors() : void
    {
        $id = $this->lastErrorId();
        $pointer = $this->lastErrorPointer();
        $mapErrors = $this->getMapErrors();
    
        if (isset($mapErrors[$id])) {
            is_array($mapErrors[$id])
            ? Core::setLastError($mapErrors[$id][$pointer], array('pointer'=>$pointer))
            : Core::setLastError($mapErrors[$id], array('pointer'=>$pointer));
        }
    }

    protected function getMapErrors() : array
    {
        $mapError = self::MAP_ERROR;

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function onlineAdd(SearchData $searchData) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $searchData,
            array('expirationDate', 'crew', 'itemsData', 'template')
        );
    
        $this->post(
            $this->getResource(),
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($searchData);
            return true;
        }

        return false;
    }

    public function deleted(SearchData $searchData) : bool
    {
        $this->patch(
            $this->getResource().'/'.$searchData->getId().'/delete'
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($searchData);
            return true;
        }
        return false;
    }
}
