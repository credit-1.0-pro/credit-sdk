<?php
namespace Sdk\ResourceCatalog\SearchData\Adapter\SearchData;

use Marmot\Interfaces\IAsyncAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IApplyAbleAdapter;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;

interface ISearchDataAdapter extends IAsyncAdapter, IFetchAbleAdapter, IApplyAbleAdapter
{
    public function deleted(SearchData $searchData) : bool;

    public function onlineAdd(SearchData $searchData) : bool;
}
