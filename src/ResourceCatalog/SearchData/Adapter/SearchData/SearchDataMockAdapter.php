<?php
namespace Sdk\ResourceCatalog\SearchData\Adapter\SearchData;

use Sdk\Common\Adapter\ApplyAbleMockAdapterTrait;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\Utils\MockFactory;

class SearchDataMockAdapter implements ISearchDataAdapter
{
    use ApplyAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateSearchData($id);
    }

    public function fetchList(array $ids) : array
    {
        $searchDataList = array();

        foreach ($ids as $id) {
            $searchDataList[] = MockFactory::generateSearchData($id);
        }

        return $searchDataList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateSearchData($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $searchDataList = array();

        foreach ($ids as $id) {
            $searchDataList[] = MockFactory::generateSearchData($id);
        }

        return $searchDataList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function deleted(SearchData $searchData) : bool
    {
        unset($searchData);
        return true;
    }

    public function onlineAdd(SearchData $searchData) : bool
    {
        unset($searchData);
        return true;
    }
}
