<?php
namespace Sdk\ResourceCatalog\SearchData\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ApplyAbleRepositoryTrait;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\Adapter\SearchData\ISearchDataAdapter;
use Sdk\ResourceCatalog\SearchData\Adapter\SearchData\SearchDataMockAdapter;
use Sdk\ResourceCatalog\SearchData\Adapter\SearchData\SearchDataRestfulAdapter;

class SearchDataRepository extends Repository implements ISearchDataAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, ApplyAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'SEARCH_DATA_LIST';
    const FETCH_ONE_MODEL_UN = 'SEARCH_DATA_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new SearchDataRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new SearchDataMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function onlineAdd(SearchData $searchData) : bool
    {
        return $this->getAdapter()->onlineAdd($searchData);
    }

    public function deleted(SearchData $searchData) : bool
    {
        return $this->getAdapter()->deleted($searchData);
    }
}
