<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\Repository\SearchDataRepository;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;

trait SearchDataCommandHandlerTrait
{
    protected function getSearchData() : SearchData
    {
        return new SearchData();
    }

    protected function getRepository() : SearchDataRepository
    {
        return new SearchDataRepository();
    }
    
    protected function fetchSearchData(int $id) : SearchData
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getTemplateRepository() : TemplateRepository
    {
        return new TemplateRepository();
    }

    protected function fetchTemplate(int $id) : Template
    {
        return $this->getTemplateRepository()->fetchOne($id);
    }
}
