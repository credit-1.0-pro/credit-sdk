<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class SearchDataCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\ResourceCatalog\SearchData\Command\SearchData\OnlineAddSearchDataCommand'=>
            'Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData\OnlineAddSearchDataCommandHandler',
        'Sdk\ResourceCatalog\SearchData\Command\SearchData\DeletedSearchDataCommand'=>
            'Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData\DeletedSearchDataCommandHandler',
        'Sdk\ResourceCatalog\SearchData\Command\SearchData\ApproveSearchDataCommand'=>
            'Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData\ApproveSearchDataCommandHandler',
        'Sdk\ResourceCatalog\SearchData\Command\SearchData\RejectSearchDataCommand'=>
            'Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData\RejectSearchDataCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
