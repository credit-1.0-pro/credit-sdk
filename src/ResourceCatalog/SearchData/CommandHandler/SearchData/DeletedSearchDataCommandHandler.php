<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\SearchData\Command\SearchData\DeletedSearchDataCommand;

class DeletedSearchDataCommandHandler implements ICommandHandler
{
    use SearchDataCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof DeletedSearchDataCommand)) {
            throw new \InvalidArgumentException;
        }
    
        $searchData = $this->fetchSearchData($command->id);
  
        return $searchData->deleted();
    }
}
