<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\ApproveCommandHandler;

class ApproveSearchDataCommandHandler extends ApproveCommandHandler
{
    use SearchDataCommandHandlerTrait;

    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchSearchData($id);
    }
}
