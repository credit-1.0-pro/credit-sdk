<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\SearchData\Command\SearchData\OnlineAddSearchDataCommand;

class OnlineAddSearchDataCommandHandler implements ICommandHandler
{
    use SearchDataCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof OnlineAddSearchDataCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $template = $this->fetchTemplate($command->template);

        $searchData = $this->getSearchData();
        $searchData->setTemplate($template);
        $searchData->setExpirationDate($command->expirationDate);
        $searchData->getItemsData()->setData($command->itemsData);
        
        return $searchData->onlineAdd();
    }
}
