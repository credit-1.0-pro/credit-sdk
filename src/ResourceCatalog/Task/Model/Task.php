<?php
namespace Sdk\ResourceCatalog\Task\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class Task implements IObject
{
    use Object;

    /**
     * @var STATUS['DEFAULT']  进行中,默认 0
     * @var STATUS['SUCCESS']  成功 2
     * @var STATUS['FAILURE']  失败 -2
     * @var STATUS['FAILURE_FILE_DOWNLOAD']  失败文件下载成功 -3
     */
    const STATUS = array(
        'DEFAULT' => 0,
        'SUCCESS' => 2,
        'FAILURE' => -2,
        'FAILURE_FILE_DOWNLOAD' => -3
    );

    const STATUS_CN = array(
        self::STATUS['DEFAULT'] => '进行中',
        self::STATUS['SUCCESS'] => '成功',
        self::STATUS['FAILURE'] => '失败',
        self::STATUS['FAILURE_FILE_DOWNLOAD'] => '失败'
    );

    const STATUS_TYPE = array(
        self::STATUS['DEFAULT'] => 'waring',
        self::STATUS['SUCCESS'] => 'success',
        self::STATUS['FAILURE'] => 'danger',
        self::STATUS['FAILURE_FILE_DOWNLOAD'] => 'danger'
    );

    private $id;
    /**
     * @var Crew $crew 发布人
     */
    protected $crew;
    /**
     * @var UserGroup $userGroup 发布委办局
     */
    protected $userGroup;
    /**
     * @var int $administrativeArea 所属行政区域
     */
    protected $administrativeArea;
    /**
     * @var int $total 总数
     */
    protected $total;
    /**
     * @var int $successNumber 成功数
     */
    protected $successNumber;
    /**
     * @var int $failureNumber 失败数
     */
    protected $failureNumber;
    /**
     * @var int $template 目录
     */
    protected $template;
    /**
     * @var int $templateVersion 目录版本记录
     */
    protected $templateVersion;
    /**
     * @var int $rule 规则
     */
    protected $rule;
    /**
     * @var int $ruleVersion 规则版本记录
     */
    protected $ruleVersion;
    /**
     * @var int $errorNumber 错误编号
     */
    protected $errorNumber;
    /**
     * @var string $fileName 文件名
     */
    protected $fileName;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->userGroup = new UserGroup();
        $this->administrativeArea = 0;
        $this->total = 0;
        $this->successNumber = 0;
        $this->failureNumber = 0;
        $this->template = 0;
        $this->templateVersion = 0;
        $this->rule = 0;
        $this->ruleVersion = 0;
        $this->errorNumber = 0;
        $this->fileName = '';
        $this->status = self::STATUS['DEFAULT'];
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->administrativeArea);
        unset($this->total);
        unset($this->successNumber);
        unset($this->failureNumber);
        unset($this->template);
        unset($this->templateVersion);
        unset($this->rule);
        unset($this->ruleVersion);
        unset($this->errorNumber);
        unset($this->fileName);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserGroup(UserGroup $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setAdministrativeArea(int $administrativeArea) : void
    {
        $this->administrativeArea = $administrativeArea;
    }

    public function getAdministrativeArea() : int
    {
        return $this->administrativeArea;
    }

    public function setTotal(int $total) : void
    {
        $this->total = $total;
    }

    public function getTotal() : int
    {
        return $this->total;
    }

    public function setSuccessNumber(int $successNumber) : void
    {
        $this->successNumber = $successNumber;
    }

    public function getSuccessNumber() : int
    {
        return $this->successNumber;
    }

    public function setFailureNumber(int $failureNumber) : void
    {
        $this->failureNumber = $failureNumber;
    }

    public function getFailureNumber() : int
    {
        return $this->failureNumber;
    }

    public function setTemplate(int $template) : void
    {
        $this->template = $template;
    }

    public function getTemplate() : int
    {
        return $this->template;
    }

    public function setTemplateVersion(int $templateVersion) : void
    {
        $this->templateVersion = $templateVersion;
    }

    public function getTemplateVersion() : int
    {
        return $this->templateVersion;
    }

    public function setRule(int $rule) : void
    {
        $this->rule = $rule;
    }

    public function getRule() : int
    {
        return $this->rule;
    }

    public function setRuleVersion(int $ruleVersion) : void
    {
        $this->ruleVersion = $ruleVersion;
    }

    public function getRuleVersion() : int
    {
        return $this->ruleVersion;
    }

    public function setErrorNumber(int $errorNumber) : void
    {
        $this->errorNumber = $errorNumber;
    }

    public function getErrorNumber() : int
    {
        return $this->errorNumber;
    }

    public function setFileName(string $fileName) : void
    {
        $this->fileName = $fileName;
    }

    public function getFileName() : string
    {
        return $this->fileName;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['DEFAULT'];
    }

    public function getFailureFileName() : string
    {
        $failureFileName = '';

        if ($this->getStatus() == self::STATUS['FAILURE_FILE_DOWNLOAD']) {
            $filePath = Core::$container->get('attachment.resourceCatalog.download.path').$this->getFileName();
            if ($this->fileIsExits($filePath)) {
                $failureFileName = $filePath;
            }
        }

        return $failureFileName;
    }

    protected function fileIsExits(string $filePath) : bool
    {
        return file_exists($filePath);
    }
}
