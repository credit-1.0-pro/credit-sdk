<?php
namespace Sdk\ResourceCatalog\Task\Model;

use Marmot\Interfaces\INull;

class NullTask extends Task implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
