<?php
namespace Sdk\ResourceCatalog\Task\Adapter\Task;

use Sdk\ResourceCatalog\Task\Model\Task;
use Sdk\ResourceCatalog\Task\Utils\MockFactory;

class TaskMockAdapter implements ITaskAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateTask($id);
    }

    public function fetchList(array $ids) : array
    {
        $taskList = array();

        foreach ($ids as $id) {
            $taskList[] = MockFactory::generateTask($id);
        }

        return $taskList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateTask($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $taskList = array();

        foreach ($ids as $id) {
            $taskList[] = MockFactory::generateTask($id);
        }

        return $taskList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
