<?php
namespace Sdk\ResourceCatalog\Task\Adapter\Task;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\ResourceCatalog\Task\Model\NullTask;
use Sdk\ResourceCatalog\Task\Translator\TaskRestfulTranslator;

class TaskRestfulAdapter extends GuzzleAdapter implements ITaskAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;
    
    private $resource;
    
    const SCENARIOS = [
        'TASK_LIST'=>[
            'fields' => [],
            'include' => 'crew'
        ],
        'TASK_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new TaskRestfulTranslator();
        $this->resource = 'resourceCatalogs/tasks';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullTask::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
