<?php
namespace Sdk\ResourceCatalog\Task\Adapter\Task;

use Marmot\Interfaces\IAsyncAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;

interface ITaskAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
}
