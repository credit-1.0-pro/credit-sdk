<?php
namespace Sdk\ResourceCatalog\Task\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ResourceCatalog\Task\Model\Task;
use Sdk\ResourceCatalog\Task\Model\NullTask;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

class TaskTranslator implements ITranslator
{

    public function arrayToObject(array $expression, $task = null)
    {
        unset($task);
        unset($expression);
        return NullTask::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

   /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($task, array $keys = array())
    {
        if (!$task instanceof Task) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'crew' => ['id', 'realName'],
                'userGroup'=> ['id', 'name'],
                'administrativeArea',
                'total',
                'successNumber',
                'failureNumber',
                'template',
                'templateVersion',
                'rule',
                'ruleVersion',
                'errorNumber',
                'fileName',
                'failureFileName',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($task->getId());
        }
        if (in_array('administrativeArea', $keys)) {
            $expression['administrativeArea'] = $task->getAdministrativeArea();
        }
        if (in_array('total', $keys)) {
            $expression['total'] = $task->getTotal();
        }
        if (in_array('successNumber', $keys)) {
            $expression['successNumber'] = $task->getSuccessNumber();
        }
        if (in_array('failureNumber', $keys)) {
            $expression['failureNumber'] = $task->getFailureNumber();
        }
        if (in_array('template', $keys)) {
            $expression['template'] = $task->getTemplate();
        }
        if (in_array('templateVersion', $keys)) {
            $expression['templateVersion'] = $task->getTemplateVersion();
        }
        if (in_array('rule', $keys)) {
            $expression['rule'] = $task->getRule();
        }
        if (in_array('ruleVersion', $keys)) {
            $expression['ruleVersion'] = $task->getRuleVersion();
        }
        if (in_array('errorNumber', $keys)) {
            $expression['errorNumber'] = $task->getErrorNumber();
        }
        if (in_array('fileName', $keys)) {
            $expression['fileName'] = $task->getFileName();
        }
        if (in_array('failureFileName', $keys)) {
            $expression['failureFileName'] = $task->getFailureFileName();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($task->getStatus()),
                'name' => TASK::STATUS_CN[$task->getStatus()],
                'type' => TASK::STATUS_TYPE[$task->getStatus()]
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $task->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $task->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $task->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $task->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $task->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $task->getCreateTime());
        }
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $task->getCrew(),
                $keys['crew']
            );
        }
        if (isset($keys['userGroup'])) {
            $expression['userGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $task->getUserGroup(),
                $keys['userGroup']
            );
        }

        return $expression;
    }
}
