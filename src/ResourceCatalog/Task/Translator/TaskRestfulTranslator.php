<?php
namespace Sdk\ResourceCatalog\Task\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\ResourceCatalog\Task\Model\Task;
use Sdk\ResourceCatalog\Task\Model\NullTask;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

class TaskRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $task = null)
    {
        return $this->translateToObject($expression, $task);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $task = null)
    {
        if (empty($expression)) {
            return NullTask::getInstance();
        }

        if ($task == null) {
            $task = new Task();
        }
       
        $data = $expression['data'];
        
        $id = $data['id'];
        $task->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['administrativeArea'])) {
            $task->setAdministrativeArea($attributes['administrativeArea']);
        }
        if (isset($attributes['total'])) {
            $task->setTotal($attributes['total']);
        }
        if (isset($attributes['successNumber'])) {
            $task->setSuccessNumber($attributes['successNumber']);
        }
        if (isset($attributes['failureNumber'])) {
            $task->setFailureNumber($attributes['failureNumber']);
        }
        if (isset($attributes['template'])) {
            $task->setTemplate($attributes['template']);
        }
        if (isset($attributes['templateVersion'])) {
            $task->setTemplateVersion($attributes['templateVersion']);
        }
        if (isset($attributes['rule'])) {
            $task->setRule($attributes['rule']);
        }
        if (isset($attributes['ruleVersion'])) {
            $task->setRuleVersion($attributes['ruleVersion']);
        }
        if (isset($attributes['errorNumber'])) {
            $task->setErrorNumber($attributes['errorNumber']);
        }
        if (isset($attributes['fileName'])) {
            $task->setFileName($attributes['fileName']);
        }
        if (isset($attributes['createTime'])) {
            $task->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['statusTime'])) {
            $task->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['updateTime'])) {
            $task->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $task->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
    
        if (isset($relationships['crew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['crew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;

            $task->setCrew($crew);
        }

        if (isset($relationships['publishUserGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['publishUserGroup']['data']);
            $task->setUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($userGroup));
        }

        return $task;
    }

    public function objectToArray($task, array $keys = array())
    {
        unset($task);
        unset($keys);
        return array();
    }
}
