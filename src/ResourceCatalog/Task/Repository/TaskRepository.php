<?php
namespace Sdk\ResourceCatalog\Task\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;

use Sdk\ResourceCatalog\Task\Adapter\Task\ITaskAdapter;
use Sdk\ResourceCatalog\Task\Adapter\Task\TaskMockAdapter;
use Sdk\ResourceCatalog\Task\Adapter\Task\TaskRestfulAdapter;

class TaskRepository extends Repository implements ITaskAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'TASK_LIST';
    const FETCH_ONE_MODEL_UN = 'TASK_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new TaskRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new TaskMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
