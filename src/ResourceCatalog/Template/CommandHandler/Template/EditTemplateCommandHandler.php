<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\Template;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\Template\Command\Template\EditTemplateCommand;
use Sdk\ResourceCatalog\Template\CommandHandler\TemplateCommandHandlerTrait;

class EditTemplateCommandHandler implements ICommandHandler
{
    use TemplateCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $template = $this->fetchTemplate($command->id);
        $template = $this->templateExecuteAction($command, $template);

        return $template->edit();
    }
}
