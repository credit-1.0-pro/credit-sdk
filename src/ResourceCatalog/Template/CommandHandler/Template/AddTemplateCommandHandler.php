<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\Template;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\Template\Command\Template\AddTemplateCommand;
use Sdk\ResourceCatalog\Template\CommandHandler\TemplateCommandHandlerTrait;

class AddTemplateCommandHandler implements ICommandHandler
{
    use TemplateCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $template = $this->getTemplate();
        $template = $this->templateExecuteAction($command, $template);

        return $template->add();
    }
}
