<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\Template;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class TemplateCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\ResourceCatalog\Template\Command\Template\AddTemplateCommand'=>
            'Sdk\ResourceCatalog\Template\CommandHandler\Template\AddTemplateCommandHandler',
        'Sdk\ResourceCatalog\Template\Command\Template\EditTemplateCommand'=>
            'Sdk\ResourceCatalog\Template\CommandHandler\Template\EditTemplateCommandHandler',
            'Sdk\ResourceCatalog\Template\Command\Template\VersionRestoreTemplateCommand'=>
                'Sdk\ResourceCatalog\Template\CommandHandler\Template\VersionRestoreTemplateCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
