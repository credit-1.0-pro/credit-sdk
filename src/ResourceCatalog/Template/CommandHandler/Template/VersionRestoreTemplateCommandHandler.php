<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\Template;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\Template\CommandHandler\TemplateCommandHandlerTrait;
use Sdk\ResourceCatalog\Template\Command\Template\VersionRestoreTemplateCommand;

class VersionRestoreTemplateCommandHandler implements ICommandHandler
{
    use TemplateCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof VersionRestoreTemplateCommand)) {
            throw new \InvalidArgumentException;
        }
    
        $templateVersion = $this->fetchTemplateVersion($command->templateVersionId);
        $template = $this->fetchTemplate($command->id);
        $template->setTemplateVersion($templateVersion);
        
        return $template->versionRestore();
    }
}
