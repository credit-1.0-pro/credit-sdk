<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class UnAuditTemplateCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Sdk\ResourceCatalog\Template\Command\UnAuditTemplate\EditUnAuditTemplateCommand'=>
            'Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate\EditUnAuditTemplateCommandHandler',
        'Sdk\ResourceCatalog\Template\Command\UnAuditTemplate\ApproveUnAuditTemplateCommand'=>
            'Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate\ApproveUnAuditTemplateCommandHandler',
        'Sdk\ResourceCatalog\Template\Command\UnAuditTemplate\RejectUnAuditTemplateCommand'=>
            'Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate\RejectUnAuditTemplateCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
