<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\ApproveCommandHandler;

use Sdk\ResourceCatalog\Template\CommandHandler\TemplateCommandHandlerTrait;

class ApproveUnAuditTemplateCommandHandler extends ApproveCommandHandler
{
    use TemplateCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditTemplate($id);
    }
}
