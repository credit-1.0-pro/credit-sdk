<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\RejectCommandHandler;

use Sdk\ResourceCatalog\Template\CommandHandler\TemplateCommandHandlerTrait;

class RejectUnAuditTemplateCommandHandler extends RejectCommandHandler
{
    use TemplateCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditTemplate($id);
    }
}
