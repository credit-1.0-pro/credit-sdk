<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ResourceCatalog\Template\CommandHandler\TemplateCommandHandlerTrait;
use Sdk\ResourceCatalog\Template\Command\UnAuditTemplate\EditUnAuditTemplateCommand;

class EditUnAuditTemplateCommandHandler implements ICommandHandler
{
    use TemplateCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditUnAuditTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditTemplate = $this->fetchUnAuditTemplate($command->id);
        $unAuditTemplate = $this->templateExecuteAction($command, $unAuditTemplate);
       
        return $unAuditTemplate->edit();
    }
}
