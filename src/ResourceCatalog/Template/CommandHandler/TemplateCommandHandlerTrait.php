<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;
use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;
use Sdk\ResourceCatalog\Template\Repository\UnAuditTemplateRepository;

use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Repository\TemplateVersionRepository;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;

trait TemplateCommandHandlerTrait
{
    protected function getTemplate() : Template
    {
        return new Template();
    }

    protected function getUnAuditTemplate() : UnAuditTemplate
    {
        return new UnAuditTemplate();
    }

    protected function getRepository() : TemplateRepository
    {
        return new TemplateRepository();
    }

    protected function getUnAuditTemplateRepository() : UnAuditTemplateRepository
    {
        return new UnAuditTemplateRepository();
    }
    
    protected function fetchTemplate(int $id) : Template
    {
        return $this->getRepository()->fetchOne($id);
    }
    
    protected function fetchUnAuditTemplate(int $id) : UnAuditTemplate
    {
        return $this->getUnAuditTemplateRepository()->fetchOne($id);
    }

    protected function getTemplateVersionRepository() : TemplateVersionRepository
    {
        return new TemplateVersionRepository();
    }
    
    protected function fetchTemplateVersion(int $id) : TemplateVersion
    {
        return $this->getTemplateVersionRepository()->fetchOne($id);
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return new UserGroupRepository();
    }

    protected function fetchSourceUnits(array $ids) : array
    {
        return $this->getUserGroupRepository()->fetchList($ids);
    }

    protected function templateExecuteAction(ICommand $command, Template $template) : Template
    {
        list($count, $sourceUnits) = $this->fetchSourceUnits(array_unique($command->sourceUnitIds));

        unset($count);

        $template->setName($command->name);
        $template->setIdentify($command->identify);
        $template->setDescription($command->description);
        $template->setVersionDescription($command->versionDescription);
        $template->setSubjectCategory($command->subjectCategory);
        $template->setDimension($command->dimension);
        $template->setExchangeFrequency($command->exchangeFrequency);
        $template->setInfoClassify($command->infoClassify);
        $template->setInfoCategory($command->infoCategory);
        $template->setItems($command->items);
        $template->clearSourceUnits();
        foreach ($sourceUnits as $sourceUnit) {
            $template->addSourceUnit($sourceUnit);
        }

        return $template;
    }
}
