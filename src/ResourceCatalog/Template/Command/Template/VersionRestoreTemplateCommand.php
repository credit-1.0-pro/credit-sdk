<?php
namespace Sdk\ResourceCatalog\Template\Command\Template;

use Marmot\Interfaces\ICommand;

class VersionRestoreTemplateCommand implements ICommand
{
    public $id;
    
    public $templateVersionId;

    public function __construct(
        int $templateVersionId,
        int $id
    ) {
        $this->id = $id;
        $this->templateVersionId = $templateVersionId;
    }
}
