<?php
namespace Sdk\ResourceCatalog\Template\Command;

use Marmot\Interfaces\ICommand;

 /**
   * @SuppressWarnings(PHPMD)
   */
class OperationCommand implements ICommand
{
    public $id;
    
    public $name;

    public $identify;

    public $description;

    public $versionDescription;

    public $subjectCategory;

    public $items;

    public $sourceUnitIds;

    public $dimension;

    public $exchangeFrequency;

    public $infoClassify;

    public $infoCategory;

    public function __construct(
        string $name,
        string $identify,
        string $description,
        string $versionDescription,
        array $subjectCategory = array(),
        array $items = array(),
        array $sourceUnitIds = array(),
        int $dimension = 0,
        int $exchangeFrequency = 0,
        int $infoClassify = 0,
        int $infoCategory = 0,
        int $id = 0
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->identify = $identify;
        $this->description = $description;
        $this->versionDescription = $versionDescription;
        $this->subjectCategory = $subjectCategory;
        $this->items = $items;
        $this->sourceUnitIds = $sourceUnitIds;
        $this->dimension = $dimension;
        $this->exchangeFrequency = $exchangeFrequency;
        $this->infoClassify = $infoClassify;
        $this->infoCategory = $infoCategory;
    }
}
