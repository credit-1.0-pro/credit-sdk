<?php
namespace Sdk\ResourceCatalog\Template\WidgetRules;

use Marmot\Core;
use Respect\Validation\Validator as V;

use Sdk\ResourceCatalog\Template\Model\Template;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class TemplateWidgetRules
{
    const NAME_MIN_LENGTH = 1;
    const NAME_MAX_LENGTH = 100;
    public function name($name) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::NAME_MIN_LENGTH,
            self::NAME_MAX_LENGTH
        )->validate($name)) {
            Core::setLastError(TEMPLATE_NAME_FORMAT_ERROR);
            return false;
        }
        return true;
    }

    public function identify($identify) : bool
    {
        $reg = '/^[A-Z][A-Z_]{0,98}[A-Z]$/';
        if (!preg_match($reg, $identify)) {
            Core::setLastError(TEMPLATE_IDENTIFY_FORMAT_ERROR);
            return false;
        }
        return true;
    }

    const DESCRIPTION_MIN_LENGTH = 1;
    const DESCRIPTION_MAX_LENGTH = 2000;
    public function description($description, $pointer = 'description') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::DESCRIPTION_MIN_LENGTH,
            self::DESCRIPTION_MAX_LENGTH
        )->validate($description)) {
            Core::setLastError(TEMPLATE_DESCRIPTION_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    public function subjectCategory($subjectCategory) : bool
    {
        if (empty($subjectCategory) || !is_array($subjectCategory)) {
            Core::setLastError(TEMPLATE_SUBJECT_CATEGORY_FORMAT_ERROR);
            return false;
        }

        foreach ($subjectCategory as $value) {
            if (!V::numeric()->positive()->validate($value) || !in_array($value, Template::SUBJECT_CATEGORY)) {
                Core::setLastError(TEMPLATE_SUBJECT_CATEGORY_FORMAT_ERROR);
                return false;
            }
        }

        return true;
    }

    public function sourceUnitIds($sourceUnitIds) : bool
    {
        if (empty($sourceUnitIds) || !is_array($sourceUnitIds)) {
            Core::setLastError(TEMPLATE_SOURCE_UNITS_FORMAT_ERROR);
            return false;
        }

        foreach ($sourceUnitIds as $id) {
            if (!is_numeric($id)) {
                Core::setLastError(TEMPLATE_SOURCE_UNITS_FORMAT_ERROR);
                return false;
            }
        }
        
        return true;
    }

    public function dimension($dimension) : bool
    {
        if (!V::numeric()->positive()->validate($dimension) || !in_array($dimension, Template::DIMENSION)) {
            Core::setLastError(TEMPLATE_DIMENSION_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    public function exchangeFrequency($exchangeFrequency) : bool
    {
        if (!V::numeric()->positive()->validate($exchangeFrequency)
            || !in_array($exchangeFrequency, Template::EXCHANGE_FREQUENCY)
        ) {
            Core::setLastError(TEMPLATE_EXCHANGE_FREQUENCY_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    public function infoClassify($infoClassify) : bool
    {
        if (!V::numeric()->positive()->validate($infoClassify) || !in_array($infoClassify, Template::INFO_CLASSIFY)) {
            Core::setLastError(TEMPLATE_INFO_CLASSIFY_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }
    
    public function infoCategory($infoCategory) : bool
    {
        if (!V::numeric()->positive()->validate($infoCategory) || !in_array($infoCategory, Template::INFO_CATEGORY)) {
            Core::setLastError(TEMPLATE_INFO_CATEGORY_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    const ITEMS_KEYS = array(
        'name',
        'identify',
        'type',
        'length',
        'options',
        'dimension',
        'isNecessary',
        'isMasked',
        'maskRule',
        'remarks'
    );

    public function items($subjectCategory, $items) : bool
    {
        if (empty($items) || !is_array($items)) {
            Core::setLastError(TEMPLATE_ITEMS_FORMAT_ERROR);
            return false;
        }

        foreach ($items as $each) {
            if (!empty(array_diff(self::ITEMS_KEYS, array_keys($each)))) {
                Core::setLastError(TEMPLATE_ITEMS_FORMAT_ERROR);
                return false;
            }
            if (!$this->name($each['name'])) {
                Core::setLastError(TEMPLATE_ITEMS_NAME_FORMAT_ERROR);
                return false;
            }
            if (!$this->identify($each['identify'])) {
                Core::setLastError(TEMPLATE_ITEMS_IDENTIFY_FORMAT_ERROR);
                return false;
            }
            if (!$this->type($each['type'])) {
                return false;
            }
            if (!$this->length($each['type'], $each['length'])) {
                return false;
            }
            if (!$this->options($each['type'], $each['options'])) {
                return false;
            }
            if (!$this->dimension($each['dimension'])) {
                Core::setLastError(TEMPLATE_ITEMS_DIMENSION_FORMAT_ERROR);
                return false;
            }
            if (!$this->isNecessary($each['isNecessary'])) {
                return false;
            }
            if (!$this->isMasked($each['isMasked'])) {
                return false;
            }
            if (!$this->maskRule($each['isMasked'], $each['maskRule'])) {
                return false;
            }
            if (!$this->remarks($each['remarks'])) {
                return false;
            }
            $names[] = $each['name'];
            $identifies[] = $each['identify'];
        }
        
        if (!$this->nameUnique($names)) {
            return false;
        }
        if (!$this->identifyUnique($identifies)) {
            return false;
        }
        if (!$this->identifyDefine($subjectCategory, $identifies)) {
            return false;
        }
        return true;
    }

    protected function nameUnique($names) : bool
    {
        if (count(array_unique($names)) != count($names)) {
            Core::setLastError(TEMPLATE_ITEMS_NAME_IS_UNIQUE);
            return false;
        }
        return true;
    }

    protected function identifyUnique($identifies) : bool
    {
        if (count(array_unique($identifies)) != count($identifies)) {
            Core::setLastError(TEMPLATE_ITEMS_IDENTIFY_IS_UNIQUE);
            return false;
        }
        return true;
    }

    const IDENTIFY_DEFINE = array(
        'ZTMC' => 'ZTMC',
        'ZJHM' => 'ZJHM',
        'TYSHXYDM' => 'TYSHXYDM',
    );
    const FR = array(
        Template::SUBJECT_CATEGORY['FRJFFRZZ'],
        Template::SUBJECT_CATEGORY['GTGSH']
    );
    protected function identifyDefine($subjectCategory, $identifies) : bool
    {
        if (!in_array(self::IDENTIFY_DEFINE['ZTMC'], $identifies)) {
            Core::setLastError(TEMPLATE_SUBJECT_NAME_NOT_EXIT);
            return false;
        }
        if (!empty($subjectCategory)) {
            foreach ($subjectCategory as $value) {
                if ($value == Template::SUBJECT_CATEGORY['ZRR']) {
                    if (!in_array(self::IDENTIFY_DEFINE['ZJHM'], $identifies)) {
                        Core::setLastError(TEMPLATE_CARD_ID_NOT_EXIT);
                        return false;
                    }
                }
                if (in_array($value, self::FR)) {
                    if (!in_array(self::IDENTIFY_DEFINE['TYSHXYDM'], $identifies)) {
                        Core::setLastError(TEMPLATE_UNIFIED_SOCIAL_CREDIT_CODE_NOT_EXIT);
                        return false;
                    }
                }
            }
        }
        if (empty($subjectCategory)) {
            if (!in_array(self::IDENTIFY_DEFINE['ZJHM'], $identifies)
                && !in_array(self::IDENTIFY_DEFINE['TYSHXYDM'], $identifies)) {
                Core::setLastError(TEMPLATE_ITEMS_IDENTIFY_FORMAT_ERROR);
                return false;
            }
        }
        return true;
    }

    protected function type($type) : bool
    {
        if (!V::numeric()->positive()->validate($type) || !in_array($type, Template::TYPE)) {
            Core::setLastError(TEMPLATE_ITEMS_TYPE_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    const TYPE_LENGTH = array(
        Template::TYPE['ZFX'],
        Template::TYPE['ZSX'],
        Template::TYPE['MJX'],
        Template::TYPE['JHX']
    );
    protected function length($type, $length) : bool
    {
        if (in_array($type, self::TYPE_LENGTH)) {
            if (!V::intVal()->validate($length)) {
                Core::setLastError(TEMPLATE_ITEMS_LENGTH_FORMAT_ERROR);
                return false;
            }
        }
        return true;
    }

    const TYPE_IS_ENUM = array(
        Template::TYPE['MJX'],
        Template::TYPE['JHX']
    );
    protected function options($type, $options) : bool
    {
        if (in_array($type, self::TYPE_IS_ENUM)) {
            if (empty($options) || !is_array($options)) {
                Core::setLastError(TEMPLATE_ITEMS_OPTIONS_FORMAT_ERROR);
                return false;
            }
        }
        if (!in_array($type, self::TYPE_IS_ENUM)) {
            if (!empty($options)) {
                Core::setLastError(TEMPLATE_ITEMS_OPTIONS_FORMAT_ERROR);
                return false;
            }
        }
        return true;
    }

    protected function isNecessary($isNecessary) : bool
    {
        if (!V::numeric()->validate($isNecessary) || !in_array($isNecessary, Template::IS_NECESSARY)) {
            Core::setLastError(TEMPLATE_IS_NECESSARY_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    protected function isMasked($isMasked) : bool
    {
        if (!V::numeric()->validate($isMasked) || !in_array($isMasked, Template::IS_MASKED)) {
            Core::setLastError(TEMPLATE_IS_MASKED_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }

    protected function maskRule($isMasked, $maskRule) : bool
    {
        if ($isMasked == Template::IS_MASKED['SHI']) {
            if (empty($maskRule) || !is_array($maskRule)) {
                Core::setLastError(TEMPLATE_MASKED_RULE_FORMAT_ERROR);
                return false;
            }
        }
        if ($isMasked == Template::IS_MASKED['FOU']) {
            if (!empty($maskRule)) {
                Core::setLastError(TEMPLATE_MASKED_RULE_FORMAT_ERROR);
                return false;
            }
        }
        return true;
    }

    const REMARKS_MIN_LENGTH = 1;
    const REMARKS_MAX_LENGTH = 2000;
    public function remarks($remarks) : bool
    {
        if (!empty($remarks)) {
            if (!V::charset('UTF-8')->stringType()->length(
                self::REMARKS_MIN_LENGTH,
                self::REMARKS_MAX_LENGTH
            )->validate($remarks)) {
                Core::setLastError(TEMPLATE_REMARKS_FORMAT_ERROR);
                return false;
            }
        }
        return true;
    }
}
