<?php
namespace Sdk\ResourceCatalog\Template\Model;

use Marmot\Interfaces\INull;

class NullTemplateVersion extends TemplateVersion implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
