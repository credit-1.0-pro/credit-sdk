<?php
namespace Sdk\ResourceCatalog\Template\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Common\Model\NullApplyAbleTrait;
use Sdk\Common\Model\NullOperateAbleTrait;

class NullUnAuditTemplate extends UnAuditTemplate implements INull
{
    use NullApplyAbleTrait, NullOperateAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
