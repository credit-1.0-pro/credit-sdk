<?php
namespace Sdk\ResourceCatalog\Template\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\IOperateAble;
use Sdk\Common\Model\OperateAbleTrait;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class Template implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    const STATUS_NORMAL = 0;
    /**
     * 主体类别
     * @var SUBJECT_CATEGORY['FRJFFRZZ']  法人及非法人组织 1
     * @var SUBJECT_CATEGORY['ZRR']  自然人 2
     * @var SUBJECT_CATEGORY['GTGSH']  个体工商户 3
     */
    const SUBJECT_CATEGORY = array(
        'FRJFFRZZ' => 1,
        'ZRR' => 2,
        'GTGSH' => 3
    );
    const SUBJECT_CATEGORY_CN = array(
        self::SUBJECT_CATEGORY['FRJFFRZZ'] => '法人及非法人组织',
        self::SUBJECT_CATEGORY['ZRR'] => '自然人',
        self::SUBJECT_CATEGORY['GTGSH'] => '个体工商户'
    );

    /**
     * 公开范围
     * @var DIMENSION['SHGK']  社会公开 1
     * @var DIMENSION['ZWGX']  政务共享 2
     * @var DIMENSION['SQCX']  授权查询 3
     */
    const DIMENSION = array(
        'SHGK' => 1,
        'ZWGX' => 2,
        'SQCX' => 3
    );
    const DIMENSION_CN = array(
        self::DIMENSION['SHGK'] => '社会公开',
        self::DIMENSION['ZWGX'] => '政务共享',
        self::DIMENSION['SQCX'] => '授权查询'
    );

    /**
     * 更新频率
     * @var EXCHANGE_FREQUENCY['SS']  实时 1
     * @var EXCHANGE_FREQUENCY['MR']  每日 2
     * @var EXCHANGE_FREQUENCY['MZ']  每周 3
     * @var EXCHANGE_FREQUENCY['MY']  每月 4
     * @var EXCHANGE_FREQUENCY['MJD']  每季度 5
     * @var EXCHANGE_FREQUENCY['MBN']  每半年 6
     * @var EXCHANGE_FREQUENCY['MYN']  每一年 7
     * @var EXCHANGE_FREQUENCY['MLN']  每两年 8
     * @var EXCHANGE_FREQUENCY['MSN']  每三年 9
     */
    const EXCHANGE_FREQUENCY = array(
        'SS' => 1,
        'MR' => 2,
        'MZ' => 3,
        'MY' => 4,
        'MJD' => 5,
        'MBN' => 6,
        'MYN' => 7,
        'MLN' => 8,
        'MSN' => 9
    );
    const EXCHANGE_FREQUENCY_CN = array(
        self::EXCHANGE_FREQUENCY['SS'] => '实时',
        self::EXCHANGE_FREQUENCY['MR'] => '每日',
        self::EXCHANGE_FREQUENCY['MZ'] => '每周',
        self::EXCHANGE_FREQUENCY['MY'] => '每月',
        self::EXCHANGE_FREQUENCY['MJD'] => '每季度',
        self::EXCHANGE_FREQUENCY['MBN'] => '每半年',
        self::EXCHANGE_FREQUENCY['MYN'] => '每一年',
        self::EXCHANGE_FREQUENCY['MLN'] => '每两年',
        self::EXCHANGE_FREQUENCY['MSN'] => '每三年'
    );
    
    /**
     * 信息分类
     * @var INFO_CLASSIFY['XZXK']  行政许可 1
     * @var INFO_CLASSIFY['XZCF']  行政处罚 2
     * @var INFO_CLASSIFY['HONGMD']  红名单 3
     * @var INFO_CLASSIFY['HEIMD']  黑名单 4
     * @var INFO_CLASSIFY['QT']  其他 5
     */
    const INFO_CLASSIFY = array(
        'XZXK' => 1,
        'XZCF' => 2,
        'HONGMD' => 3,
        'HEIMD' => 4,
        'QT' => 5
    );
    const INFO_CLASSIFY_CN = array(
        self::INFO_CLASSIFY['XZXK'] => '行政许可',
        self::INFO_CLASSIFY['XZCF'] => '行政处罚',
        self::INFO_CLASSIFY['HONGMD'] => '红名单',
        self::INFO_CLASSIFY['HEIMD'] => '黑名单',
        self::INFO_CLASSIFY['QT'] => '其他'
    );

    /**
     * 信息类别
     * @var INFO_CATEGORY['JCXX']  基础信息 1
     * @var INFO_CATEGORY['SHOUXXX']  守信信息 2
     * @var INFO_CATEGORY['SHIXXX']  失信信息 3
     * @var INFO_CATEGORY['QTXX']  其他信息 4
     */
    const INFO_CATEGORY = array(
        'JCXX' => 1,
        'SHOUXXX' => 2,
        'SHIXXX' => 3,
        'QTXX' => 4
    );
    const INFO_CATEGORY_CN = array(
        self::INFO_CATEGORY['JCXX'] => '基础信息',
        self::INFO_CATEGORY['SHOUXXX'] => '守信信息',
        self::INFO_CATEGORY['SHIXXX'] => '失信信息',
        self::INFO_CATEGORY['QTXX'] => '其他信息'
    );

    /**
     * 数据类型
     * @var TYPE['ZFX']  字符型 1
     * @var TYPE['RQX']  日期型 2
     * @var TYPE['ZSX']  整数型 3
     * @var TYPE['FDX']  浮点型 4
     * @var TYPE['MJX']  枚举型 5
     * @var TYPE['JHX']  集合型 6
     */
    const TYPE = array(
        'ZFX' => 1,
        'RQX' => 2,
        'ZSX' => 3,
        'FDX' => 4,
        'MJX' => 5,
        'JHX' => 6
    );

    const TYPE_CN = array(
        self::TYPE['ZFX'] => '字符型',
        self::TYPE['RQX'] => '日期型',
        self::TYPE['ZSX'] => '整数型',
        self::TYPE['FDX'] => '浮点型',
        self::TYPE['MJX'] => '枚举型',
        self::TYPE['JHX'] => '集合型'
    );
    /**
     * 数据类型-默认值
     */
    const TEMPLATE_TYPE_DEFAULT = array(
        self::TYPE['ZFX'] => '',
        self::TYPE['RQX'] => 0,
        self::TYPE['ZSX'] => 0,
        self::TYPE['FDX'] => 0,
        self::TYPE['MJX'] => '',
        self::TYPE['JHX'] => ''
    );
     /**
     * 数据类型-默认长度
     */
    const TEMPLATE_TYPE_DEFAULT_LENGTH = array(
        self::TYPE['RQX'] => 8,
        self::TYPE['FDX'] => 24,
    );
    /**
     * 是否必填
     * @var IS_NECESSARY['FOU']  否 0
     * @var IS_NECESSARY['SHI']  是 1，默认
     */
    const IS_NECESSARY = array(
        'FOU' => 0,
        'SHI' => 1
    );

    const IS_NECESSARY_CN = array(
        self::IS_NECESSARY['FOU'] => '否',
        self::IS_NECESSARY['SHI'] => '是'
    );
    /**
     * 是否脱敏
     * @var IS_MASKED['FOU']  否 0，默认
     * @var IS_MASKED['SHI']  是 1
     */
    const IS_MASKED = array(
        'FOU' => 0,
        'SHI' => 1
    );

    const IS_MASKED_CN = array(
        self::IS_MASKED['FOU'] => '否',
        self::IS_MASKED['SHI'] => '是'
    );

    private $id;

    /**
     * @var string $name 目录名称
     */
    protected $name;
    /**
     * @var string $identify 目录标识
     */
    protected $identify;
    /**
     * @var array $subjectCategory 主体类别
     */
    protected $subjectCategory;
    /**
     * @var int $dimension 公开范围
     */
    protected $dimension;
    /**
     * @var int $exchangeFrequency 更新频率
     */
    protected $exchangeFrequency;
    /**
     * @var int $infoClassify 信息分类
     */
    protected $infoClassify;
    /**
     * @var int $infoCategory 信息类别
     */
    protected $infoCategory;
    /**
     * @var string $description 目录描述
     */
    protected $description;
    /**
     * @var string $versionDescription 目录版本描述
     */
    protected $versionDescription;
    /**
     * @var array $sourceUnits 来源委办局
     */
    protected $sourceUnits;
    /**
     * @var array $items 模板信息
     */
    protected $items;
    /**
     * @var TemplateVersion $templateVersion 当前使用的版本记录
     */
    protected $templateVersion;
    /**
     * @var Crew $crew 发布人
     */
    protected $crew;
    /**
     * @var UserGroup $userGroup 发布委办局
     */
    protected $userGroup;

    protected $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->identify = '';
        $this->subjectCategory = array();
        $this->dimension = self::DIMENSION['SHGK'];
        $this->exchangeFrequency = 0;
        $this->infoClassify = 0;
        $this->infoCategory = 0;
        $this->description = '';
        $this->versionDescription = '';
        $this->sourceUnits = array();
        $this->items = array();
        $this->templateVersion = new TemplateVersion();
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->userGroup = new UserGroup();
        $this->status = self::STATUS_NORMAL;
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->repository = new TemplateRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->identify);
        unset($this->subjectCategory);
        unset($this->dimension);
        unset($this->exchangeFrequency);
        unset($this->infoClassify);
        unset($this->infoCategory);
        unset($this->description);
        unset($this->sourceUnits);
        unset($this->items);
        unset($this->templateVersion);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setIdentify(string $identify)
    {
        $this->identify = $identify;
    }

    public function getIdentify() : string
    {
        return $this->identify;
    }

    public function setSubjectCategory(array $subjectCategory)
    {
        $this->subjectCategory = $subjectCategory;
    }

    public function getSubjectCategory() : array
    {
        return $this->subjectCategory;
    }
    
    public function setDimension(int $dimension) : void
    {
        $this->dimension = in_array(
            $dimension,
            self::DIMENSION
        ) ? $dimension : self::DIMENSION['SHGK'];
    }

    public function getDimension() : int
    {
        return $this->dimension;
    }

    public function setExchangeFrequency(int $exchangeFrequency)
    {
        $this->exchangeFrequency = $exchangeFrequency;
    }

    public function getExchangeFrequency() : int
    {
        return $this->exchangeFrequency;
    }

    public function setInfoClassify(int $infoClassify)
    {
        $this->infoClassify = $infoClassify;
    }

    public function getInfoClassify() : int
    {
        return $this->infoClassify;
    }

    public function setInfoCategory(int $infoCategory)
    {
        $this->infoCategory = $infoCategory;
    }

    public function getInfoCategory() : int
    {
        return $this->infoCategory;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setVersionDescription(string $versionDescription)
    {
        $this->versionDescription = $versionDescription;
    }

    public function getVersionDescription() : string
    {
        return $this->versionDescription;
    }

    public function addSourceUnit(UserGroup $sourceUnit)
    {
        $this->sourceUnits[] = $sourceUnit;
    }

    public function clearSourceUnits() : void
    {
        $this->sourceUnits = [];
    }

    public function getSourceUnits() : array
    {
        return $this->sourceUnits;
    }

    public function setItems(array $items)
    {
        $this->items = $items;
    }

    public function getItems() : array
    {
        return $this->items;
    }

    public function setTemplateVersion(TemplateVersion $templateVersion) : void
    {
        $this->templateVersion = $templateVersion;
    }

    public function getTemplateVersion() : TemplateVersion
    {
        return $this->templateVersion;
    }

    public function setCrew(Crew $crew)
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserGroup(UserGroup $userGroup)
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
    
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * 新增编辑
     */
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 版本还原
     * @return [bool]
     */
    public function versionRestore() : bool
    {
        return $this->getRepository()->versionRestore($this);
    }
}
