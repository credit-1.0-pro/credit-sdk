<?php
namespace Sdk\ResourceCatalog\Template\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;

class TemplateVersion implements IObject
{
    use Object;
    /**
     * 状态
     * @var STATUS['ENABLED']  启用
     * @var STATUS['DISABLED']  禁用, 默认
     */
    const STATUS = array(
        'ENABLED' => 0,
        'DISABLED' => -2,
    );
    const STATUS_CN = array(
        self::STATUS['ENABLED'] => '启用',
        self::STATUS['DISABLED'] => '禁用',
    );

    private $id;
    /**
     * @var string $number 版本号
     */
    protected $number;
    /**
     * @var string $description 描述
     */
    protected $description;
    /**
     * @var int $templateId 目录id
     */
    protected $templateId;
    /**
     * @var array $info 版本信息
     */
    protected $info;
    /**
     * @var Crew $crew 发布人
     */
    protected $crew;
    /**
     * @var UserGroup $userGroup 发布委办局
     */
    protected $userGroup;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->number = '';
        $this->description = '';
        $this->templateId = 0;
        $this->info = array();
        $this->crew = new Crew();
        $this->userGroup = new UserGroup();
        $this->status = self::STATUS['DISABLED'];
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->number);
        unset($this->description);
        unset($this->templateId);
        unset($this->info);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
    public function setNumber(string $number)
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setTemplateId(int $templateId)
    {
        $this->templateId = $templateId;
    }

    public function getTemplateId() : int
    {
        return $this->templateId;
    }

    public function setInfo(array $info)
    {
        $this->info = $info;
    }

    public function getInfo() : array
    {
        return $this->info;
    }

    public function setCrew(Crew $crew)
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserGroup(UserGroup $userGroup)
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['DISABLED'];
    }
}
