<?php
namespace Sdk\ResourceCatalog\Template\Adapter\TemplateVersion;

use Marmot\Interfaces\IAsyncAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;

interface ITemplateVersionAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
}
