<?php
namespace Sdk\ResourceCatalog\Template\Adapter\TemplateVersion;

use Sdk\ResourceCatalog\Template\Utils\MockFactory;

class TemplateVersionMockAdapter implements ITemplateVersionAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateTemplateVersion($id);
    }

    public function fetchList(array $ids) : array
    {
        $templateList = array();

        foreach ($ids as $id) {
            $templateList[] = MockFactory::generateTemplateVersion($id);
        }

        return $templateList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateTemplateVersion($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $templateList = array();

        foreach ($ids as $id) {
            $templateList[] = MockFactory::generateTemplateVersion($id);
        }

        return $templateList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
