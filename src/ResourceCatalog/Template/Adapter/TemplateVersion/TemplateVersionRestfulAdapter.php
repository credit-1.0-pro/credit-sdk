<?php
namespace Sdk\ResourceCatalog\Template\Adapter\TemplateVersion;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\ResourceCatalog\Template\Model\NullTemplateVersion;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;

class TemplateVersionRestfulAdapter extends GuzzleAdapter implements ITemplateVersionAdapter
{
    use FetchAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait;

    private $translator;
    
    private $resource;
    
    const SCENARIOS = [
        'TEMPLATE_VERSION_LIST'=>[
            'fields' => [],
            'include' => 'crew,publishUserGroup'
        ],
        'TEMPLATE_VERSION_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,publishUserGroup'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new TemplateVersionRestfulTranslator();
        $this->resource = 'resourceCatalogs/templateVersions';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullTemplateVersion::getInstance();
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
