<?php
namespace Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate;

interface IUnAuditTemplateAdapter extends IUnAuditTemplateFetchAdapter, IUnAuditTemplateOperateAbleAdapter
{
    
}
