<?php
namespace Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate;

use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

interface IUnAuditTemplateOperateAbleAdapter extends IOperateAbleAdapter, IApplyAbleAdapter
{
    
}
