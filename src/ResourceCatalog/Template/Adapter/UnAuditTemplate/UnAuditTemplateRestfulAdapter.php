<?php
namespace Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;
use Sdk\ResourceCatalog\Template\Model\NullUnAuditTemplate;
use Sdk\ResourceCatalog\Template\Adapter\Template\TemplateRestfulAdapter;
use Sdk\ResourceCatalog\Template\Translator\UnAuditTemplateRestfulTranslator;

class UnAuditTemplateRestfulAdapter extends GuzzleAdapter implements IUnAuditTemplateAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'UN_AUDIT_TEMPLATE_LIST'=>[
            'fields' => [],
            'include' => 'crew,publishUserGroup,applyUserGroup,applyCrew,publishCrew,templateVersion,sourceUnits'
        ],
        'UN_AUDIT_TEMPLATE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,publishUserGroup,applyUserGroup,applyCrew,publishCrew,templateVersion,sourceUnits'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UnAuditTemplateRestfulTranslator();
        $this->resource = 'resourceCatalogs/unAuditedTemplates';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullUnAuditTemplate::getInstance();
    }

    protected function getMapErrors() : array
    {
        $mapError = TemplateRestfulAdapter::MAP_ERROR;

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function addAction(UnAuditTemplate $unAuditTemplate) : bool
    {
        unset($unAuditTemplate);
        return false;
    }

    protected function editAction(UnAuditTemplate $unAuditTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditTemplate,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'versionDescription',
                'items',
                'sourceUnits',
                'crew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$unAuditTemplate->getId().'/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditTemplate);
            return true;
        }

        return false;
    }

    protected function rejectAction(IApplyAble $applyAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $applyAbleObject,
            array(
                'rejectReason',
                'applyCrew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$applyAbleObject->getId().'/reject',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($applyAbleObject);
            return true;
        }
        return false;
    }
    
    protected function approveAction(IApplyAble $applyAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray($applyAbleObject, array('applyCrew'));
       
        $this->patch(
            $this->getResource().'/'.$applyAbleObject->getId().'/approve',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($applyAbleObject);
            return true;
        }
        return false;
    }
}
