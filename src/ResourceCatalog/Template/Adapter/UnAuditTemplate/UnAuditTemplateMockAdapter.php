<?php
namespace Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate;

use Sdk\Common\Adapter\ApplyAbleMockAdapterTrait;
use Sdk\Common\Adapter\OperateAbleMockAdapterTrait;

use Sdk\ResourceCatalog\Template\Utils\MockFactory;
use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;

class UnAuditTemplateMockAdapter implements IUnAuditTemplateAdapter
{
    use ApplyAbleMockAdapterTrait, OperateAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateUnAuditTemplate($id);
    }

    public function fetchList(array $ids) : array
    {
        $templateList = array();

        foreach ($ids as $id) {
            $templateList[] = MockFactory::generateUnAuditTemplate($id);
        }

        return $templateList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateUnAuditTemplate($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $templateList = array();

        foreach ($ids as $id) {
            $templateList[] = MockFactory::generateUnAuditTemplate($id);
        }

        return $templateList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
