<?php
namespace Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate;

use Marmot\Interfaces\IAsyncAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;

interface IUnAuditTemplateFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
