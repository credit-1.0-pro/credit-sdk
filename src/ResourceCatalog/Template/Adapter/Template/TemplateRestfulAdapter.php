<?php
namespace Sdk\ResourceCatalog\Template\Adapter\Template;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\NullTemplate;
use Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator;

class TemplateRestfulAdapter extends GuzzleAdapter implements ITemplateAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    const MAP_ERROR = [
        101 => array(
            'name' => TEMPLATE_NAME_FORMAT_ERROR,
            'identify'=>TEMPLATE_IDENTIFY_FORMAT_ERROR,
            'description'=>TEMPLATE_DESCRIPTION_FORMAT_ERROR,
            'versionDescription'=>TEMPLATE_VERSION_DESCRIPTION_FORMAT_ERROR,
            'subjectCategory'=>TEMPLATE_SUBJECT_CATEGORY_FORMAT_ERROR,
            'dimension' => TEMPLATE_DIMENSION_FORMAT_ERROR,
            'exchangeFrequency'=>TEMPLATE_EXCHANGE_FREQUENCY_FORMAT_ERROR,
            'infoClassify'=>TEMPLATE_INFO_CLASSIFY_FORMAT_ERROR,
            'infoCategory'=>TEMPLATE_INFO_CATEGORY_FORMAT_ERROR,
            'sourceUnitIds'=>TEMPLATE_SOURCE_UNITS_FORMAT_ERROR,
            'items'=>TEMPLATE_ITEMS_FORMAT_ERROR,
            'itemsName'=>TEMPLATE_ITEMS_NAME_FORMAT_ERROR,
            'itemsIdentify'=>TEMPLATE_ITEMS_IDENTIFY_FORMAT_ERROR,
            'itemsNameUnique'=>TEMPLATE_ITEMS_NAME_IS_UNIQUE,
            'itemsIdentifyUnique'=>TEMPLATE_ITEMS_IDENTIFY_IS_UNIQUE,
            'itemsDimension'=>TEMPLATE_ITEMS_DIMENSION_FORMAT_ERROR,
            'type' => TEMPLATE_ITEMS_TYPE_FORMAT_ERROR,
            'length' => TEMPLATE_ITEMS_LENGTH_FORMAT_ERROR,
            'options' => TEMPLATE_ITEMS_OPTIONS_FORMAT_ERROR,
            'isNecessary' => TEMPLATE_IS_NECESSARY_FORMAT_ERROR,
            'isMasked' => TEMPLATE_IS_MASKED_FORMAT_ERROR,
            'maskRule' => TEMPLATE_MASKED_RULE_FORMAT_ERROR,
            'remarks' => TEMPLATE_REMARKS_FORMAT_ERROR,
            'ZTMC' => TEMPLATE_SUBJECT_NAME_NOT_EXIT,
            'ZJHM' => TEMPLATE_CARD_ID_NOT_EXIT,
            'TYSHXYDM' => TEMPLATE_UNIFIED_SOCIAL_CREDIT_CODE_NOT_EXIT,
            'crewId' => CREW_ID_FORMAT_ERROR,
            'applyCrewId' => APPLY_CREW_FORMAT_ERROR,
            'templateVersionId' => TEMPLATE_VERSION_FORMAT_ERROR,
            'rejectReason' => REASON_FORMAT_ERROR
        ),
        100 => array(
            'crew' => PARAMETER_IS_EMPTY,
            'publishCrew' => PARAMETER_IS_EMPTY,
            'templateVersion' => TEMPLATE_VERSION_NOT_EXIT,
        ),
        102 => array(
            'applyStatus' => APPLY_STATUE_CAN_NOT_MODIFY,
        ),
        103 => array(
            'templateIdentify' => TEMPLATE_IDENTIFY_IS_UNIQUE,
        )
    ];
        
    private $translator;
    
    private $resource;
    
    const SCENARIOS = [
        'TEMPLATE_LIST'=>[
            'fields' => [],
            'include' => 'crew,publishUserGroup,templateVersion,sourceUnits'
        ],
        'TEMPLATE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,publishUserGroup,templateVersion,sourceUnits'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new TemplateRestfulTranslator();
        $this->resource = 'resourceCatalogs/templates';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getNullObject() : INull
    {
        return NullTemplate::getInstance();
    }

    protected function getMapErrors() : array
    {
        $mapError = self::MAP_ERROR;

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function addAction(Template $template) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $template,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'versionDescription',
                'items',
                'sourceUnits',
                'crew'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($template);
            return true;
        }

        return false;
    }

    protected function editAction(Template $template) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $template,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'versionDescription',
                'items',
                'sourceUnits',
                'crew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$template->getId(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($template);
            return true;
        }

        return false;
    }

    public function versionRestore(Template $template) : bool
    {
        $data = $this->getTranslator()->objectToArray($template, array('crew'));
    
        $this->patch(
            $this->getResource().'/'.$template->getId().'/'.'versionRestore/'.$template->getTemplateVersion()->getId(),
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($template);
            return true;
        }

        return false;
    }
}
