<?php
namespace Sdk\ResourceCatalog\Template\Adapter\Template;

use Marmot\Interfaces\IAsyncAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\ResourceCatalog\Template\Model\Template;

interface ITemplateAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperateAbleAdapter
{
    public function versionRestore(Template $template) : bool;
}
