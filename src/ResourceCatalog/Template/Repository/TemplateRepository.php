<?php
namespace Sdk\ResourceCatalog\Template\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\OperateAbleRepositoryTrait;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Adapter\Template\ITemplateAdapter;
use Sdk\ResourceCatalog\Template\Adapter\Template\TemplateMockAdapter;
use Sdk\ResourceCatalog\Template\Adapter\Template\TemplateRestfulAdapter;

class TemplateRepository extends Repository implements ITemplateAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'TEMPLATE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new TemplateRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new TemplateMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function versionRestore(Template $template) : bool
    {
        return $this->getAdapter()->versionRestore($template);
    }
}
