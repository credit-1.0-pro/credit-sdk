<?php
namespace Sdk\ResourceCatalog\Template\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;

use Sdk\ResourceCatalog\Template\Adapter\TemplateVersion\ITemplateVersionAdapter;
use Sdk\ResourceCatalog\Template\Adapter\TemplateVersion\TemplateVersionMockAdapter;
use Sdk\ResourceCatalog\Template\Adapter\TemplateVersion\TemplateVersionRestfulAdapter;

class TemplateVersionRepository extends Repository implements ITemplateVersionAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'TEMPLATE_VERSION_LIST';
    const FETCH_ONE_MODEL_UN = 'TEMPLATE_VERSION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new TemplateVersionRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new TemplateVersionMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
