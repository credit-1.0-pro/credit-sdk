<?php
namespace Sdk\ResourceCatalog\Template\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ApplyAbleRepositoryTrait;
use Sdk\Common\Repository\OperateAbleRepositoryTrait;

use Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate\IUnAuditTemplateAdapter;
use Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate\UnAuditTemplateMockAdapter;
use Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate\UnAuditTemplateRestfulAdapter;

class UnAuditTemplateRepository extends Repository implements IUnAuditTemplateAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        ApplyAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'UN_AUDIT_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'UN_AUDIT_TEMPLATE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new UnAuditTemplateRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new UnAuditTemplateMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
