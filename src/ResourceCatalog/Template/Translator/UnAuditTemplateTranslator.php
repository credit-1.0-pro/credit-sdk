<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;
use Sdk\ResourceCatalog\Template\Model\NullUnAuditTemplate;

use Sdk\Common\Translator\ApplyTranslatorTrait;

class UnAuditTemplateTranslator extends TemplateTranslator implements ITranslator
{
    use ApplyTranslatorTrait;

    public function arrayToObject(array $expression, $unAuditedTemplate = null)
    {
        unset($unAuditedTemplate);
        unset($expression);
        return new NullUnAuditTemplate();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function objectToArray($unAuditedTemplate, array $keys = array())
    {
        if (!$unAuditedTemplate instanceof UnAuditTemplate) {
            return array();
        }

        $template = parent::objectToArray($unAuditedTemplate, $keys);

        $apply = $this->applyObjectToArray($unAuditedTemplate, $keys);

        $unAuditedTemplateArray = array_merge($template, $apply);

        return $unAuditedTemplateArray;
    }
}
