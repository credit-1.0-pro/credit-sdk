<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\ApplyRestfulTranslatorTrait;

use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;
use Sdk\ResourceCatalog\Template\Model\NullUnAuditTemplate;

class UnAuditTemplateRestfulTranslator extends TemplateRestfulTranslator implements IRestfulTranslator
{
    use ApplyRestfulTranslatorTrait;

    public function arrayToObject(array $expression, $unAuditedTemplate = null)
    {
        return $this->translateToObject($expression, $unAuditedTemplate);
    }

    protected function translateToObject(array $expression, $unAuditedTemplate = null)
    {
        if (empty($expression)) {
            return new NullUnAuditTemplate();
        }

        if ($unAuditedTemplate == null) {
            $unAuditedTemplate = new UnAuditTemplate();
        }
        
        $unAuditedTemplate = parent::translateToObject($expression, $unAuditedTemplate);

        $unAuditedTemplate = $this->applyTranslateToObject($expression, $unAuditedTemplate);

        return $unAuditedTemplate;
    }

    public function objectToArray($unAuditedTemplate, array $keys = array())
    {
        if (!$unAuditedTemplate instanceof UnAuditTemplate) {
            return array();
        }

        $expression = array(
            'data' => array(
                'type' => 'unAuditedTemplates'
            )
        );

        $template = parent::objectToArray($unAuditedTemplate, $keys);

        $apply = $this->applyObjectToArray($unAuditedTemplate, $keys);

        $templateAttributes = isset($template['data']['attributes']) ? $template['data']['attributes'] : array();
        $applyAttributes = isset($apply['data']['attributes']) ? $apply['data']['attributes'] : array();
        $expression['data']['attributes'] = array_merge($templateAttributes, $applyAttributes);

        $templateRelationships = isset($template['data']['relationships']) ?
                                    $template['data']['relationships'] :
                                    array();
        $applyRelationships = isset($apply['data']['relationships']) ? $apply['data']['relationships'] : array();
        $expression['data']['relationships'] = array_merge($templateRelationships, $applyRelationships);

        return $expression;
    }
}
