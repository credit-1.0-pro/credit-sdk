<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Model\NullTemplateVersion;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

class TemplateVersionRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $templateVersion = null)
    {
        return $this->translateToObject($expression, $templateVersion);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $templateVersion = null)
    {
        if (empty($expression)) {
            return new NullTemplateVersion();
        }

        if ($templateVersion == null) {
            $templateVersion = new TemplateVersion();
        }
       
        $data = $expression['data'];
        
        $id = $data['id'];
        $templateVersion->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['number'])) {
            $templateVersion->setNumber($attributes['number']);
        }
        if (isset($attributes['description'])) {
            $templateVersion->setDescription($attributes['description']);
        }
        if (isset($attributes['templateId'])) {
            $templateVersion->setTemplateId($attributes['templateId']);
        }
        if (isset($attributes['info'])) {
            $templateVersion->setInfo($attributes['info']);
        }
        if (isset($attributes['createTime'])) {
            $templateVersion->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['statusTime'])) {
            $templateVersion->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['updateTime'])) {
            $templateVersion->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $templateVersion->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
    
        if (isset($relationships['crew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['crew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;

            $templateVersion->setCrew($crew);
        }

        if (isset($relationships['publishUserGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['publishUserGroup']['data']);
            $templateVersion->setUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($userGroup));
        }

        return $templateVersion;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($templateVersion, array $keys = array())
    {
        if (!$templateVersion instanceof TemplateVersion) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'description',
                'templateId',
                'info',
                'crew'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'templateVersions'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $templateVersion->getId();
        }

        $attributes = array();

        if (in_array('number', $keys)) {
            $attributes['number'] = $templateVersion->getNumber();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $templateVersion->getDescription();
        }
        if (in_array('templateId', $keys)) {
            $attributes['templateId'] = $templateVersion->getTemplateId();
        }
        if (in_array('info', $keys)) {
            $attributes['info'] = $templateVersion->getInfo();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $templateVersion->getCrew()->getId()
                )
            );
        }

        return $expression;
    }
}
