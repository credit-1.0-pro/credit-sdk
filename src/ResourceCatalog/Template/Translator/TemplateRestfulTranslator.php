<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\NullTemplate;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class TemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $template = null)
    {
        return $this->translateToObject($expression, $template);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    protected function getTemplateVersionRestfulTranslator(): TemplateVersionRestfulTranslator
    {
        return new TemplateVersionRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $template = null)
    {
        if (empty($expression)) {
            return new NullTemplate();
        }

        if ($template == null) {
            $template = new Template();
        }
       
        $data = $expression['data'];
        
        $id = $data['id'];
        $template->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $template->setName($attributes['name']);
        }
        if (isset($attributes['identify'])) {
            $template->setIdentify($attributes['identify']);
        }
        if (isset($attributes['description'])) {
            $template->setDescription($attributes['description']);
        }
        if (isset($attributes['subjectCategory'])) {
            $template->setSubjectCategory($attributes['subjectCategory']);
        }
        if (isset($attributes['dimension'])) {
            $template->setDimension($attributes['dimension']);
        }
        if (isset($attributes['exchangeFrequency'])) {
            $template->setExchangeFrequency($attributes['exchangeFrequency']);
        }
        if (isset($attributes['infoClassify'])) {
            $template->setInfoClassify($attributes['infoClassify']);
        }
        if (isset($attributes['infoCategory'])) {
            $template->setInfoCategory($attributes['infoCategory']);
        }
        if (isset($attributes['items'])) {
            $template->setItems($attributes['items']);
        }
        if (isset($attributes['createTime'])) {
            $template->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['statusTime'])) {
            $template->setStatusTime($attributes['statusTime']);
        }

        if (isset($attributes['updateTime'])) {
            $template->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['status'])) {
            $template->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
    
        if (isset($relationships['crew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['crew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;

            $template->setCrew($crew);
        }

        if (isset($relationships['publishUserGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['publishUserGroup']['data']);
            $template->setUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($userGroup));
        }
        
        if (isset($relationships['templateVersion']['data'])) {
            $templateVersion = $this->changeArrayFormat($relationships['templateVersion']['data']);
            $template->setTemplateVersion(
                $this->getTemplateVersionRestfulTranslator()->arrayToObject($templateVersion)
            );
        }
        if (isset($relationships['sourceUnits']['data'])) {
            $this->setUpSourceUnits($relationships['sourceUnits']['data'], $template);
        }

        return $template;
    }

    private function setUpSourceUnits(array $sourceUnits, Template $template)
    {
        foreach ($sourceUnits as $sourceUnitArray) {
            $sourceUnit = $this->changeArrayFormat($sourceUnitArray);
            $sourceUnitObject = $this->getUserGroupRestfulTranslator()->arrayToObject($sourceUnit);
            $template->addSourceUnit($sourceUnitObject);
        }
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($template, array $keys = array())
    {
        if (!$template instanceof Template) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'description',
                'versionDescription',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'sourceUnits',
                'templateVersion',
                'items',
                'crew'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'templates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $template->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $template->getName();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $template->getIdentify();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $template->getDescription();
        }
        if (in_array('versionDescription', $keys)) {
            $attributes['versionDescription'] = $template->getVersionDescription();
        }
        if (in_array('subjectCategory', $keys)) {
            $attributes['subjectCategory'] = $template->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $template->getDimension();
        }
        if (in_array('exchangeFrequency', $keys)) {
            $attributes['exchangeFrequency'] = $template->getExchangeFrequency();
        }
        if (in_array('infoClassify', $keys)) {
            $attributes['infoClassify'] = $template->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $attributes['infoCategory'] = $template->getInfoCategory();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $template->getItems();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $template->getCrew()->getId()
                )
            );
        }

        if (in_array('templateVersion', $keys)) {
            $expression['data']['relationships']['templateVersion']['data'] = array(
                array(
                    'type' => 'templateVersions',
                    'id' => $template->getTemplateVersion()->getId()
                )
            );
        }
        
        if (in_array('sourceUnits', $keys)) {
            $sourceUnitsArray = $this->setUpSourceUnitsArray($template);
            $expression['data']['relationships']['sourceUnits']['data'] = $sourceUnitsArray;
        }

        return $expression;
    }

    private function setUpSourceUnitsArray(Template $template)
    {
        $sourceUnitsArray = [];

        $sourceUnits = $template->getSourceUnits();
        foreach ($sourceUnits as $sourceUnit) {
            $sourceUnitsArray[] = array(
                    'type' => 'userGroups',
                    'id' => $sourceUnit->getId()
                );
        }

        return $sourceUnitsArray;
    }
}
