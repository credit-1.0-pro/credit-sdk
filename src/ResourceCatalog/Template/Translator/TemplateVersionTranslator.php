<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Model\NullTemplateVersion;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

class TemplateVersionTranslator implements ITranslator
{
    use TemplateTranslatorTrait;
    
    public function arrayToObject(array $expression, $templateVersion = null)
    {
        unset($templateVersion);
        unset($expression);
        return new NullTemplateVersion();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

   /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($templateVersion, array $keys = array())
    {
        if (!$templateVersion instanceof TemplateVersion) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'description',
                'templateId',
                'info',
                'crew' => [],
                'userGroup'=> [],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($templateVersion->getId());
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $templateVersion->getNumber();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $templateVersion->getDescription();
        }
        if (in_array('templateId', $keys)) {
            $expression['templateId'] = marmot_encode($templateVersion->getTemplateId());
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($templateVersion->getStatus()),
                'name' => TemplateVersion::STATUS_CN[$templateVersion->getStatus()]
            ];
        }
        if (in_array('info', $keys)) {
            $expression['info'] = $this->getInfoCn($templateVersion->getInfo());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $templateVersion->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $templateVersion->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $templateVersion->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $templateVersion->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $templateVersion->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $templateVersion->getCreateTime());
        }
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $templateVersion->getCrew(),
                $keys['crew']
            );
        }
        if (isset($keys['userGroup'])) {
            $expression['publishUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $templateVersion->getUserGroup(),
                $keys['userGroup']
            );
        }

        return $expression;
    }
}
