<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Sdk\ResourceCatalog\Template\Model\Template;

trait TemplateTranslatorTrait
{
    protected function getSubjectCategoryCn(array $subjectCategory) : array
    {
        $data = array();
        foreach ($subjectCategory as $key => $item) {
            $data[$key]['id'] = marmot_encode($item);
            $data[$key]['name'] = isset(Template::SUBJECT_CATEGORY_CN[$item]) ?
                                    Template::SUBJECT_CATEGORY_CN[$item] :
                                    '';
        }

        return $data;
    }

    protected function getDimensionCn(int $dimension) : array
    {
        $data = array(
            'id' => marmot_encode($dimension),
            'name' => isset(Template::DIMENSION_CN[$dimension]) ? Template::DIMENSION_CN[$dimension] : ''
        );

        return $data;
    }

    protected function getExchangeFrequencyCn(int $exchangeFrequency) : array
    {
        $data = array(
            'id' => marmot_encode($exchangeFrequency),
            'name' => isset(Template::EXCHANGE_FREQUENCY_CN[$exchangeFrequency]) ?
                        Template::EXCHANGE_FREQUENCY_CN[$exchangeFrequency] :
                        ''
        );

        return $data;
    }

    protected function getInfoClassifyCn(int $infoClassify) : array
    {
        $data = array(
            'id' => marmot_encode($infoClassify),
            'name' => isset(Template::INFO_CLASSIFY_CN[$infoClassify]) ? Template::INFO_CLASSIFY_CN[$infoClassify] : ''
        );

        return $data;
    }

    protected function getInfoCategoryCn(int $infoCategory) : array
    {
        $data = array(
            'id' => marmot_encode($infoCategory),
            'name' => isset(Template::INFO_CATEGORY_CN[$infoCategory]) ? Template::INFO_CATEGORY_CN[$infoCategory] : ''
        );

        return $data;
    }
   
    protected function getItemCn(array $items) : array
    {
        foreach ($items as $key => $item) {
            $items[$key]['isNecessary']= isset($item['isNecessary']) ?
                                        $this->getIsNecessaryCn($item['isNecessary']) :
                                        array();
            $items[$key]['isMasked']= isset($item['isMasked']) ?
                                        $this->getIsMaskedCn($item['isMasked']) :
                                        array();
            $items[$key]['dimension']= isset($item['dimension']) ?
                                        $this->getDimensionCn($item['dimension']) :
                                        array();
            $items[$key]['type']= isset($item['type']) ?
                                        $this->getTypeCn($item['type']) :
                                        array();
        }

        return $items;
    }
   
    private function getIsNecessaryCn(int $isNecessary) : array
    {
        $data = array(
            'id' => marmot_encode($isNecessary),
            'name' => isset(Template::IS_NECESSARY_CN[$isNecessary]) ? Template::IS_NECESSARY_CN[$isNecessary] : ''
        );

        return $data;
    }
   
    private function getIsMaskedCn(int $isMasked) : array
    {
        $data = array(
            'id' => marmot_encode($isMasked),
            'name' => isset(Template::IS_MASKED_CN[$isMasked]) ? Template::IS_MASKED_CN[$isMasked] : ''
        );

        return $data;
    }
    
    private function getTypeCn(int $type) : array
    {
        $data = array(
            'id' => marmot_encode($type),
            'name' => isset(Template::TYPE_CN[$type]) ? Template::TYPE_CN[$type] : ''
        );

        return $data;
    }

    protected function getInfoCn(array $info) : array
    {
        $info['items']= isset($info['items']) ? $this->getItemCn($info['items']) : array();
        $info['dimension']= isset($info['dimension']) ? $this->getDimensionCn($info['dimension']) : array();

        $info['infoCategory'] = $info['infoClassify'] =
        $info['subjectCategory'] = $info['exchangeFrequency'] = $info['sourceUnits'] = array();
        
        if (isset($info['info_category'])) {
            $info['infoCategory'] = $this->getInfoCategoryCn($info['info_category']);
            unset($info['info_category']);
        }
        if (isset($info['info_classify'])) {
            $info['infoClassify'] = $this->getInfoClassifyCn($info['info_classify']);
            unset($info['info_classify']);
        }
        if (isset($info['subject_category'])) {
            $info['subjectCategory'] = $this->getSubjectCategoryCn($info['subject_category']);
            unset($info['subject_category']);
        }
        if (isset($info['exchange_frequency'])) {
            $info['exchangeFrequency'] = $this->getExchangeFrequencyCn($info['exchange_frequency']);
            unset($info['exchange_frequency']);
        }
        if (isset($info['source_units'])) {
            $info['sourceUnits'] = $info['source_units'];
            unset($info['source_units']);
        }
            
        return $info;
    }
}
