<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\NullTemplate;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

class TemplateTranslator implements ITranslator
{
    use TemplateTranslatorTrait;
    
    public function arrayToObject(array $expression, $template = null)
    {
        unset($template);
        unset($expression);
        return new NullTemplate();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return new TemplateVersionTranslator();
    }

   /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($template, array $keys = array())
    {
        if (!$template instanceof Template) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'description',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'sourceUnits' => [],
                'items',
                'crew' => [],
                'userGroup'=> [],
                'templateVersion'=> [],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($template->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $template->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $template->getIdentify();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $template->getDescription();
        }
        if (in_array('subjectCategory', $keys)) {
            $subjectCategory = $template->getSubjectCategory();
            $expression['subjectCategory'] = $this->getSubjectCategoryCn($subjectCategory);
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension'] = $this->getDimensionCn($template->getDimension());
        }
        if (in_array('exchangeFrequency', $keys)) {
            $expression['exchangeFrequency'] = $this->getExchangeFrequencyCn($template->getExchangeFrequency());
        }
        if (in_array('infoClassify', $keys)) {
            $expression['infoClassify'] = $this->getInfoClassifyCn($template->getInfoClassify());
        }
        if (in_array('infoCategory', $keys)) {
            $expression['infoCategory'] = $this->getInfoCategoryCn($template->getInfoCategory());
        }
        if (in_array('items', $keys)) {
            $expression['items'] = $this->getItemCn($template->getItems());
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $template->getStatus();
        }
        if (isset($keys['sourceUnits'])) {
            $expression['sourceUnits'] = $this->sourceUnits($keys, $template);
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $template->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $template->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $template->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $template->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $template->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $template->getCreateTime());
        }
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $template->getCrew(),
                $keys['crew']
            );
        }
        if (isset($keys['userGroup'])) {
            $expression['publishUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $template->getUserGroup(),
                $keys['userGroup']
            );
        }
        if (isset($keys['templateVersion'])) {
            $expression['templateVersion'] = $this->getTemplateVersionTranslator()->objectToArray(
                $template->getTemplateVersion(),
                $keys['templateVersion']
            );
        }

        return $expression;
    }

    private function sourceUnits($keys, $template)
    {
        $sourceUnitsArray = array();

        $sourceUnits = $template->getSourceUnits();
        foreach ($sourceUnits as $sourceUnit) {
            $sourceUnitsArray[] = $this->getUserGroupTranslator()->objectToArray(
                $sourceUnit,
                $keys['sourceUnits']
            );
        }

        return $sourceUnitsArray;
    }
}
