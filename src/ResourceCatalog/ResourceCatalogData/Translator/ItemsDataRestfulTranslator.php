<?php
namespace Sdk\ResourceCatalog\ResourceCatalogData\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\ResourceCatalog\ResourceCatalogData\Model\ItemsData;
use Sdk\ResourceCatalog\ResourceCatalogData\Model\NullItemsData;

class ItemsDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $itemsData = null)
    {
        return $this->translateToObject($expression, $itemsData);
    }

    protected function translateToObject(array $expression, $itemsData = null)
    {
        if (empty($expression)) {
            return NullItemsData::getInstance();
        }

        if ($itemsData == null) {
            $itemsData = new ItemsData();
        }
       
        $data = $expression['data'];
        
        $id = $data['id'];
        $itemsData->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['data'])) {
            $itemsData->setData($attributes['data']);
        }

        return $itemsData;
    }

    public function objectToArray($itemsData, array $keys = array())
    {
        unset($itemsData);
        unset($keys);
        return array();
    }
}
