<?php
namespace Sdk\ResourceCatalog\ResourceCatalogData\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ResourceCatalog\ResourceCatalogData\Model\ItemsData;
use Sdk\ResourceCatalog\ResourceCatalogData\Model\NullItemsData;

class ItemsDataTranslator implements ITranslator
{

    public function arrayToObject(array $expression, $itemsData = null)
    {
        unset($itemsData);
        unset($expression);
        return NullItemsData::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function objectToArray($itemsData, array $keys = array())
    {
        if (!$itemsData instanceof ItemsData) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array('id', 'data');
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($itemsData->getId());
        }
        if (in_array('data', $keys)) {
            $expression['data'] = $itemsData->getData();
        }

        return $expression;
    }
}
