<?php
namespace Sdk\ResourceCatalog\ResourceCatalogData\Model;

class ItemsData
{
    private $id;

    private $data;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->data = array();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->data);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setData(array $data) : void
    {
        $this->data = $data;
    }

    public function getData() : array
    {
        return $this->data;
    }
}
