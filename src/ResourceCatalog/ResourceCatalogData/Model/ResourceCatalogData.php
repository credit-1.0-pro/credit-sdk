<?php
namespace Sdk\ResourceCatalog\ResourceCatalogData\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Model\RuleVersion;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;

use Sdk\ResourceCatalog\Task\Model\Task;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
abstract class ResourceCatalogData implements IObject
{
    use Object;

    const EXPIRATION_DATE_DEFAULT = '4102329600'; //20991231

    protected $id;
    /**
     * @var string $name 主体名称:企业名称/姓名
     */
    protected $name;
    /**
     * @var string $identify 主体代码:统一社会信用代码/身份证号
     */
    protected $identify;
     /**
     * @var int $expirationDate 有效期限
     */
    protected $expirationDate;
    /**
     * @var Crew $crew 发布人
     */
    protected $crew;
    /**
     * @var UserGroup $publishUserGroup 发布委办局
     */
    protected $publishUserGroup;
    /**
     * @var int $subjectCategory 主体类别
     */
    protected $subjectCategory;
    /**
     * @var int $dimension 公开范围
     */
    protected $dimension;
    /**
     * @var int $exchangeFrequency 更新频率
     */
    protected $exchangeFrequency;
    /**
     * @var int $infoClassify 信息分类
     */
    protected $infoClassify;
    /**
     * @var int $infoCategory 信息类别
     */
    protected $infoCategory;
    /**
     * @var Rule $rule 规则
     */
    protected $rule;
    /**
     * @var RuleVersion $ruleVersion 规则版本记录
     */
    protected $ruleVersion;
    /**
     * @var Template $template 目录
     */
    protected $template;
    /**
     * @var TemplateVersion $templateVersion 目录版本记录
     */
    protected $templateVersion;
    /**
     * @var Task $task 任务
     */
    protected $task;
    /**
     * @var ItemsData $itemsData 资源目录数据
     */
    protected $itemsData;
    
    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->identify = '';
        $this->expirationDate = self::EXPIRATION_DATE_DEFAULT;
        $this->subjectCategory = 0;
        $this->dimension = 0;
        $this->exchangeFrequency = 0;
        $this->infoClassify = 0;
        $this->infoCategory = 0;
        $this->rule = new Rule();
        $this->ruleVersion = new RuleVersion();
        $this->template = new Template();
        $this->templateVersion = new TemplateVersion();
        $this->task = new Task();
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->publishUserGroup = new UserGroup();
        $this->itemsData = new ItemsData();
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->identify);
        unset($this->expirationDate);
        unset($this->subjectCategory);
        unset($this->dimension);
        unset($this->exchangeFrequency);
        unset($this->infoClassify);
        unset($this->infoCategory);
        unset($this->rule);
        unset($this->ruleVersion);
        unset($this->template);
        unset($this->templateVersion);
        unset($this->task);
        unset($this->crew);
        unset($this->publishUserGroup);
        unset($this->itemsData);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setIdentify(string $identify) : void
    {
        $this->identify = $identify;
    }

    public function getIdentify() : string
    {
        return $this->identify;
    }
    
    public function setExpirationDate(int $expirationDate) : void
    {
        $this->expirationDate = $expirationDate;
    }

    public function getExpirationDate() : int
    {
        return $this->expirationDate;
    }

    public function setSubjectCategory(int $subjectCategory) : void
    {
        $this->subjectCategory = $subjectCategory;
    }

    public function getSubjectCategory() : int
    {
        return $this->subjectCategory;
    }
    
    public function setDimension(int $dimension) : void
    {
        $this->dimension = $dimension;
    }

    public function getDimension() : int
    {
        return $this->dimension;
    }

    public function setExchangeFrequency(int $exchangeFrequency) : void
    {
        $this->exchangeFrequency = $exchangeFrequency;
    }

    public function getExchangeFrequency() : int
    {
        return $this->exchangeFrequency;
    }

    public function setInfoClassify(int $infoClassify) : void
    {
        $this->infoClassify = $infoClassify;
    }

    public function getInfoClassify() : int
    {
        return $this->infoClassify;
    }

    public function setInfoCategory(int $infoCategory) : void
    {
        $this->infoCategory = $infoCategory;
    }

    public function getInfoCategory() : int
    {
        return $this->infoCategory;
    }

    public function setRule(Rule $rule) : void
    {
        $this->rule = $rule;
    }

    public function getRule() : Rule
    {
        return $this->rule;
    }

    public function setRuleVersion(RuleVersion $ruleVersion) : void
    {
        $this->ruleVersion = $ruleVersion;
    }

    public function getRuleVersion() : RuleVersion
    {
        return $this->ruleVersion;
    }

    public function setCrew(Crew $crew)
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setPublishUserGroup(UserGroup $publishUserGroup)
    {
        $this->publishUserGroup = $publishUserGroup;
    }

    public function getPublishUserGroup() : UserGroup
    {
        return $this->publishUserGroup;
    }

    public function setTemplate(Template $template) : void
    {
        $this->template = $template;
    }

    public function getTemplate() : Template
    {
        return $this->template;
    }

    public function setTemplateVersion(TemplateVersion $templateVersion) : void
    {
        $this->templateVersion = $templateVersion;
    }

    public function getTemplateVersion() : TemplateVersion
    {
        return $this->templateVersion;
    }

    public function setTask(Task $task) : void
    {
        $this->task = $task;
    }

    public function getTask() : Task
    {
        return $this->task;
    }

    public function setItemsData(ItemsData $itemsData) : void
    {
        $this->itemsData = $itemsData;
    }

    public function getItemsData() : ItemsData
    {
        return $this->itemsData;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
