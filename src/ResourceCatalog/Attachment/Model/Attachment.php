<?php
namespace Sdk\ResourceCatalog\Attachment\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Respect\Validation\Validator as V;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Model\Guest;
use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;

use Sdk\ResourceCatalog\Task\Repository\TaskRepository;

/**
 *
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class Attachment
{
    const MAX_FILE_SIZE = 10 * 1024 * 1024; //10MB
    const DEFAULT_ERROR_CODE = 0; //默认错误码
    const ALLOW_ATTACHMENT_EXTENSION = array(
        'xls', 'xlsx'
    );
    
    /**
     * @var string $name 上传文件名
     */
    private $name;
    /**
     * @var string $tmpName 文件临时存储的位置
     */
    private $tmpName;
    /**
     * @var int $size 文件大小
     */
    private $size;
    /**
     * @var int $error 由文件上传导致的错误代码
     */
    private $error;
    /**
     * @var string $path 文件上传路径
     */
    private $path;
    /**
     * @var string 文件hash
     */
    protected $hash;
    /**
     * @var Crew $crew
     */
    protected $crew;
    /**
     * @var Template $template
     */
    protected $template;

    protected $taskRepository;

    protected $templateRepository;

    public function __construct(array $file = array())
    {
        $this->name = isset($file['name']) ? $file['name'] : '';
        $this->tmpName = isset($file['tmp_name']) ? $file['tmp_name'] : '';
        $this->size = isset($file['size']) ? $file['size'] : 0;
        $this->error = isset($file['error']) ? $file['error'] : 0;
        $this->path = Core::$container->get('attachment.resourceCatalog.upload.path');
        $this->hash = '';
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Guest();
        $this->template = new Template();
        $this->taskRepository = new TaskRepository();
        $this->templateRepository = new TemplateRepository();
    }

    public function __destruct()
    {
        unset($this->name);
        unset($this->tmpName);
        unset($this->size);
        unset($this->error);
        unset($this->path);
        unset($this->hash);
        unset($this->crew);
        unset($this->template);
        unset($this->taskRepository);
        unset($this->templateRepository);
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setTmpName(string $tmpName): void
    {
        $this->tmpName = $tmpName;
    }

    public function getTmpName(): string
    {
        return $this->tmpName;
    }

    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setError(int $error): void
    {
        $this->error = $error;
    }

    public function getError(): int
    {
        return $this->error;
    }

    public function getPath() : string
    {
        return $this->path;
    }

    public function setHash(string $hash)
    {
        $this->hash = $hash;
    }

    public function getHash() : string
    {
        return $this->hash;
    }

    public function generateHash() : void
    {
        $hash = md5(md5_file($this->getTmpName()).$this->getSize());
        $this->setHash($hash);
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setTemplate(Template $template): void
    {
        $this->template = $template;
    }

    public function getTemplate(): Template
    {
        return $this->template;
    }

    protected function getTaskRepository() : TaskRepository
    {
        return $this->taskRepository;
    }

    protected function getTemplateRepository() : TemplateRepository
    {
        return $this->templateRepository;
    }

    public function upload()
    {
        if ($this->validate()) {
            $destinationFile = $this->generateFilePath();
            return $this->move($destinationFile);
        }

        return false;
    }

    /**
     * 验证文件
     * 1. 检测员工是否是登录状态
     * 2. 文件是否为空
     * 3. 文件大小是否超出限制
     * 4. 文件后缀是否在范围内
     * 5. 由文件上传导致的错误代码
     * 6. 验证文件名是否符合格式: 资源目录标识_资源目录id.xlsx,并验证目录是否存在标识是否匹配
     * 7. 验证文件是否重复
     */
    protected function validate() : bool
    {
        return $this->isCrewExist()
            && $this->validateFileIsEmpty()
            && $this->validateFileSize()
            && $this->validateFileExtension()
            && $this->validateFileError()
            && $this->validateFileName()
            && $this->validateFileIsExist();
    }

    //检测员工是否是登录状态检测员工是否是登录状态
    protected function isCrewExist() : bool
    {
        if ($this->getCrew() instanceof INull) {
            Core::setLastError(NEED_SIGNIN);
            return false;
        }

        $crewId = Core::$container->get('crew')->getId();

        if (empty($crewId)) {
            Core::setLastError(NEED_SIGNIN);
            return false;
        }
        return true;
    }
    //检测文件是否为空
    protected function validateFileIsEmpty() : bool
    {
        if (empty($this->getName())) {
            Core::setLastError(FILE_IS_EMPTY);
            return false;
        }

        return true;
    }
    //检测文件大小是否超出限制
    protected function validateFileSize() : bool
    {
        if ($this->getSize() > self::MAX_FILE_SIZE) {
            Core::setLastError(FILE_SIZE_FORMAT_ERROR);
            return false;
        }

        return true;
    }
    //检测文件后缀是否在范围内
    protected function validateFileExtension() : bool
    {
        $file = explode('.', $this->getName());

        if (!in_array(end($file), self::ALLOW_ATTACHMENT_EXTENSION)) {
            Core::setLastError(FILE_EXTENSION_FORMAT_ERROR);
            return false;
        }

        return true;
    }
    //由文件上传导致的错误代码
    protected function validateFileError() : bool
    {
        if ($this->getError() > self::DEFAULT_ERROR_CODE) {
            Core::setLastError(FILE_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    //验证文件名是否符合格式: 资源目录标识_资源目录id.xlsx,并验证目录是否存在标识是否匹配
    protected function validateFileName() : bool
    {
        $name = $this->getName();
        $nameInfo = explode('_', $name);

        if (isset($nameInfo[0]) && isset($nameInfo[1])) {
            if (is_string($nameInfo[0]) && is_string($nameInfo[1])) {
                $templateIdentify = $nameInfo[0];
                $nameInfo = explode('.', $nameInfo[1]);
                $templateId = '';
                if (isset($nameInfo[0])) {
                    $templateId = $nameInfo[0];
                }
                $templateId = marmot_decode($templateId);

                if ($this->getTemplate()->getId() != $templateId) {
                    Core::setLastError(FILE_FORMAT_ERROR);
                    return false;
                }

                $template = $this->getTemplateRepository()->fetchOne($templateId);

                if ($template instanceof INull) {
                    Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'template'));
                    return false;
                }

                if ($templateIdentify != $template->getIdentify()) {
                    Core::setLastError(FILE_FORMAT_ERROR);
                    return false;
                }

                $this->setTemplate($template);
                return true;
            }
        }

        Core::setLastError(FILE_FORMAT_ERROR);
        return false;
    }

    //验证文件是否重复
    protected function validateFileIsExist() : bool
    {
        $this->generateHash();
        $filter['hash'] = $this->getHash();
        list($count, $list) = $this->getTaskRepository()->search($filter, ['updateTime'], PAGE, PAGE);
        
        unset($list);
        
        if (!empty($count)) {
            Core::setLastError(FILE_IS_UNIQUE);
            return false;
        }

        return true;
    }

    protected function generateFilePath() : string
    {
        $file = explode('.', $this->getName());
        $templateId = $this->getTemplate()->getId();
        $templateIdentify = $this->getTemplate()->getIdentify();
        $crewId = $this->getCrew()->getId();
        $hash = $this->getHash();
        $extension = end($file);
        $path = $this->getPath();

        return $path.$templateIdentify.'_'.$templateId.'_'.$crewId.'_'.$hash.'.'.$extension;
    }
    
    protected function move(string $destinationFile) : bool
    {
        return move_uploaded_file($this->getTmpName(), $destinationFile);
    }
}
