# 新闻管理错误提示规范（10401-10500）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* ER-10401: 新闻内容格式不正确
* ER-10402: 新闻分类格式不正确
* ER-10403: 新闻分类不存在
* ER-10404: 公开类型格式不正确
* ER-10405: 首页展示状态格式不正确
* ER-10406: 轮播状态格式不正确
* ER-10407: 轮播图图片格式不正确
* ER-10408: 发布时间格式不正确

---

### <a name="ER-10401">ER-10401</a>

**id**

`10401`

**code**

`NEWS_CONTENT_FORMAT_ERROR`

**title**

内容格式不正确,请重新输入.

**detail**

内容格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10402">ER-10402</a>

**id**

`10402`

**code**

`NEWS_TYPE_FORMAT_ERROR`

**title**

新闻分类格式不正确,请重新输入.

**detail**

新闻分类格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10403">ER-10403</a>

**id**

`10403`

**code**

`NEWS_TYPE_NOT_EXIT`

**title**

新闻分类不存在，请核对后重新输入.

**detail**

新闻分类不存在，请核对后重新输入.

**links**

待补充

### <a name="ER-10404">ER-10404</a>

**id**

`10404`

**code**

`DIMENSION_FORMAT_ERROR`

**title**

公开类型格式不正确,请重新输入.

**detail**

公开类型格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10405">ER-10405</a>

**id**

`10405`

**code**

`HOME_PAGE_SHOW_STATUS_FORMAT_ERROR`

**title**

首页展示状态格式不正确,请重新输入.

**detail**

首页展示状态格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10406">ER-10406</a>

**id**

`10406`

**code**

`BANNER_STATUS_FORMAT_ERROR`

**title**

轮播状态格式不正确,请重新输入

**detail**

轮播状态格式不正确,请重新输入

**links**

待补充

### <a name="ER-10407">ER-10407</a>

**id**

`10407`

**code**

`BANNER_IMAGE_FORMAT_ERROR`

**title**

图片格式不正确,应为jpg,jpeg,png格式,请重新上传.

**detail**

图片格式不正确,应为jpg,jpeg,png格式,请重新上传.

**links**

待补充

### <a name="ER-10408">ER-10408</a>

**id**

`10408`

**code**

`RELEASE_TIME_FORMAT_ERROR`

**title**

发布时间格式不正确,请重新输入.

**detail**

发布时间格式不正确,请重新输入.

**links**

待补充

### ER-10409 - ER-10500 错误预留