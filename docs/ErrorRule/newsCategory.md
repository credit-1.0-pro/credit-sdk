# 新闻分类管理错误提示规范（10301-10400）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* ER-10301: 分类名称格式不正确
* ER-10302: 分类名称已经存在
* ER-10303: 分类等级格式不正确
* ER-10304: 一级分类格式不正确
* ER-10305: 二级分类格式不正确
* ER-10306: 一级分类已经存在
* ER-10307: 二级分类已经存在
* ER-10308: 二级分类不属于一级分类
* ER-10309 - ER-10400: 新闻分类管理错误预留

---

### <a name="ER-10301">ER-10301</a>

**id**

`10301`

**code**

`NEWS_CATEGORY_NAME_FORMAT_ERROR`

**title**

分类名称格式不正确, 应为2-20位字符, 请重新输入.

**detail**

分类名称格式不正确, 应为2-20位字符, 请重新输入.

**links**

待补充

### <a name="ER-10302">ER-10302</a>

**id**

`10302`

**code**

`NEWS_CATEGORY_NAME_IS_UNIQUE`

**title**

分类名称已经存在，请重新输入.

**detail**

分类名称已经存在，请重新输入.

**links**

待补充

### <a name="ER-10303">ER-10303</a>

**id**

`10303`

**code**

`NEWS_CATEGORY_GRADE_FORMAT_ERROR`

**title**

分类等级格式不正确,请重新输入.

**detail**

分类等级格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10304">ER-10304</a>

**id**

`10304`

**code**

`NEWS_CATEGORY_PARENT_CATEGORY_FORMAT_ERROR`

**title**

一级分类格式不正确,请重新输入.

**detail**

一级分类格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10305">ER-10305</a>

**id**

`10305`

**code**

`NEWS_CATEGORY_CATEGORY_FORMAT_ERROR`

**title**

二级分类格式不正确,请重新输入.

**detail**

二级分类格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10306">ER-10306</a>

**id**

`10306`

**code**

`NEWS_CATEGORY_PARENT_CATEGORY_NOT_EXIST`

**title**

一级分类不存在，请核对后重新输入.

**detail**

一级分类不存在，请核对后重新输入.

**links**

待补充

### <a name="ER-10307">ER-10307</a>

**id**

`10307`

**code**

`NEWS_CATEGORY_CATEGORY_NOT_EXIST`

**title**

二级分类不存在，请核对后重新输入.

**detail**

二级分类不存在，请核对后重新输入.

**links**

待补充

### <a name="ER-10308">ER-10308</a>

**id**

`10308`

**code**

`CATEGORY_NOT_BELONG_TO_THE_PARENT_CATEGORY`

**title**

二级分类不属于一级分类，请重新输入.

**detail**

二级分类不属于一级分类，请重新输入.

**links**

待补充

### ER-10309 - ER-10400 错误预留