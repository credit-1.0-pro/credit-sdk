# 委办局管理错误提示规范（10001-10050）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* ER-10001: 委办局名称格式不正确
* ER-10002: 委办局名称已经存在
* ER-10003: 委办局简称格式不正确
* ER-10004: 所属行政区域格式不正确
* ER-10005 - ER-10050: 委办局管理错误预留

---

### <a name="ER-10001">ER-10001</a>

**id**

`10001`

**code**

`USER_GROUP_NAME_FORMAT_ERROR`

**title**

委办局名称格式不正确, 应为2-20位字符, 请重新输入.

**detail**

委办局名称格式不正确, 应为2-20位字符, 请重新输入.

**links**

待补充

### <a name="ER-10002">ER-10002</a>

**id**

`10002`

**code**

`USER_GROUP_NAME_IS_UNIQUE`

**title**

委办局名称已经存在，请重新输入.

**detail**

委办局名称已经存在，请重新输入.

**links**

待补充

### <a name="ER-10003">ER-10003</a>

**id**

`10003`

**code**

`USER_GROUP_SHORT_NAME_FORMAT_ERROR`

**title**

委办局简称格式不正确, 应为1-8位字符, 请重新输入.

**detail**

委办局简称格式不正确, 应为1-8位字符, 请重新输入.

**links**

待补充

### <a name="ER-10004">ER-10004</a>

**id**

`10004`

**code**

`USER_GROUP_ADMINISTRATIVE_AREA_FORMAT_ERROR`

**title**

所属行政区域格式不正确,请重新输入.

**detail**

所属行政区域格式不正确,请重新输入.

**links**

待补充

### ER-10005 - ER-10050 错误预留