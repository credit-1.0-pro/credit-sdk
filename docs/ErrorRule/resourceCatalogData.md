# 资源目录数据管理错误提示规范（10701-10800）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* ER-10701: 有效期限格式不正确
* ER-10702: 目录不存在
* ER-10703: 目录格式不正确
* ER-10704: 资源目录数据格式不正确
* ER-10705: 主体类别格式不正确
* ER-10706: 资源目录数据已经存在

---

### <a name="ER-10701">ER-10701</a>

**id**

`10701`

**code**

`RESOURCE_CATALOG_DATA_EXPIRATION_DATE_FORMAT_ERROR`

**title**

有效期限格式不正确,请重新输入.

**detail**

有效期限格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10702">ER-10702</a>

**id**

`10702`

**code**

`RESOURCE_CATALOG_DATA_TEMPLATE_NOT_EXIT`

**title**

目录不存在,请核对后重新输入.

**detail**

目录不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10703">ER-10703</a>

**id**

`10703`

**code**

`RESOURCE_CATALOG_DATA_TEMPLATE_FORMAT_ERROR`

**title**

目录格式不正确,请重新输入.

**detail**

目录格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10704">ER-10704</a>

**id**

`10704`

**code**

`RESOURCE_CATALOG_DATA_ITEMS_DATA_FORMAT_ERROR`

**title**

资源目录数据格式不正确,请重新输入.

**detail**

资源目录数据格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10705">ER-10705</a>

**id**

`10705`

**code**

`RESOURCE_CATALOG_DATA_SUBJECT_CATEGORY_FORMAT_ERROR`

**title**

主体类别格式不正确,请重新输入.

**detail**

主体类别格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10706">ER-10706</a>

**id**

`10706`

**code**

`RESOURCE_CATALOG_DATA_IS_UNIQUE`

**title**

资源目录数据已经存在,请重新输入.

**detail**

资源目录数据已经存在,请重新输入.

**links**

待补充

### ER-10707 - ER-10800 错误预留
