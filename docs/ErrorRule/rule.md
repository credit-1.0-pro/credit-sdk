# 规则管理错误提示规范（10601-10700）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* ER-10601: 版本描述格式不正确
* ER-10602: 规则格式不正确
* ER-10603: 目录格式不正确
* ER-10604: 版本记录格式不正确
* ER-10605: 规则名称不存在
* ER-10606: 补全规则格式不正确
* ER-10607: 补全数量不正确
* ER-10608: 补全信息项来源格式不正确
* ER-10609: 补全依据格式不正确
* ER-10610: 补全信息项格式不正确
* ER-10611: 比对规则格式不正确
* ER-10612: 比对数量不正确
* ER-10613: 比对信息项来源格式不正确
* ER-10614: 比对依据格式不正确
* ER-10615: 比对信息项格式不正确
* ER-10616: 去重规则格式不正确
* ER-10617: 去重信息项格式不正确
* ER-10618: 去重结果处理格式不正确
* ER-10619: 目录待补全信息项不存在
* ER-10620: 补全信息项来源不存在
* ER-10621: 补全信息项不存在
* ER-10622: 目录待比对信息项不存在
* ER-10623: 比对信息项来源不存在
* ER-10624: 比对信息项不存在
* ER-10625: 比对项为企业名称,比对依据不能选择企业名称信息项
* ER-10626: 比对项为主体代码(统一社会信用代码/身份证号),那比对依据不能选择主体代码(统一社会信用代码/身份证号)信息项
* ER-10627: 去重信息项不存在
* ER-10628: 目标信息项类型不存在
* ER-10629: 类型不能转换
* ER-10630: 长度不能转换
* ER-10631: 公开范围不能转换
* ER-10632: 脱敏状态不能转换
* ER-10633: 脱敏范围不能转换
* ER-10634: 当比对/补全信息项来源为自然人,依据必须包含身份证号
* ER-10635: 目录不存在
* ER-10636: 目录版本记录不存在
* ER-10637: 规则版本记录不存在
* ER-10638: 规则已经存在

---

### <a name="ER-10601">ER-10601</a>

**id**

`10601`

**code**

`RULE_VERSION_DESCRIPTION_FORMAT_ERROR`

**title**

版本描述格式不正确,应为1-2000位字符,请重新输入.

**detail**

版本描述格式不正确,应为1-2000位字符,请重新输入.

**links**

待补充

### <a name="ER-10602">ER-10602</a>

**id**

`10602`

**code**

`RULE_RULES_FORMAT_ERROR`

**title**

规则格式不正确,请重新输入.

**detail**

规则格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10603">ER-10603</a>

**id**

`10603`

**code**

`RULE_TEMPLATE_FORMAT_ERROR`

**title**

目录格式不正确,请重新输入.

**detail**

目录格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10604">ER-10604</a>

**id**

`10604`

**code**

`RULE_VERSION_FORMAT_ERROR`

**title**

规则版本记录格式不正确,请重新输入.

**detail**

规则版本记录格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10605">ER-10605</a>

**id**

`10605`

**code**

`RULE_RULES_NAME_NOT_EXIT`

**title**

规则名称不存在,请核对后重新输入.

**detail**

规则名称不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10606">ER-10606</a>

**id**

`10606`

**code**

`RULE_COMPLETION_RULES_FORMAT_ERROR`

**title**

补全规则格式不正确,请重新输入.

**detail**

补全规则格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10607">ER-10607</a>

**id**

`10607`

**code**

`RULE_COMPLETION_RULES_COUNT_ERROR`

**title**

补全数量不正确,请重新输入.

**detail**

补全数量不正确,请重新输入.

**links**

待补充

### <a name="ER-10608">ER-10608</a>

**id**

`10608`

**code**

`RULE_COMPLETION_RULES_TEMPLATE_FORMAT_ERROR`

**title**

补全信息项来源格式不正确,请重新输入.

**detail**

补全信息项来源格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10609">ER-10609</a>

**id**

`10609`

**code**

`RULE_COMPLETION_RULES_BASE_FORMAT_ERROR`

**title**

补全依据格式不正确,请重新输入.

**detail**

补全依据格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10610">ER-10610</a>

**id**

`10610`

**code**

`RULE_COMPLETION_RULES_TEMPLATE_ITEM_FORMAT_ERROR`

**title**

补全信息项格式不正确,请重新输入.

**detail**

补全信息项格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10611">ER-10611</a>

**id**

`10611`

**code**

`RULE_COMPARISON_RULES_FORMAT_ERROR`

**title**

比对规则格式不正确,请重新输入.

**detail**

比对规则格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10612">ER-10612</a>

**id**

`10612`

**code**

`RULE_COMPARISON_RULES_COUNT_ERROR`

**title**

比对数量不正确,请重新输入.

**detail**

比对数量不正确,请重新输入.

**links**

待补充

### <a name="ER-10613">ER-10613</a>

**id**

`10613`

**code**

`RULE_COMPARISON_RULES_TEMPLATE_FORMAT_ERROR`

**title**

比对信息项来源格式不正确,请重新输入.

**detail**

比对信息项来源格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10614">ER-10614</a>

**id**

`10614`

**code**

`RULE_COMPARISON_RULES_BASE_FORMAT_ERROR`

**title**

比对依据格式不正确,请重新输入.

**detail**

比对依据格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10615">ER-10615</a>

**id**

`10615`

**code**

`RULE_COMPARISON_RULES_TEMPLATE_ITEM_FORMAT_ERROR`

**title**

比对信息项格式不正确,请重新输入.

**detail**

比对信息项格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10616">ER-10616</a>

**id**

`10616`

**code**

`RULE_DE_DUPLICATION_RULES_FORMAT_ERROR`

**title**

去重规则格式不正确,请重新输入.

**detail**

去重规则格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10617">ER-10617</a>

**id**

`10617`

**code**

`RULE_DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR`

**title**

去重信息项格式不正确,请重新输入.

**detail**

去重信息项格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10618">ER-10618</a>

**id**

`10618`

**code**

`RULE_DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR`

**title**

去重结果处理格式不正确,请重新输入.

**detail**

去重结果处理格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10619">ER-10619</a>

**id**

`10619`

**code**

`RULE_COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST`

**title**

目录待补全信息项不存在,请核对后重新输入.

**detail**

目录待补全信息项不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10620">ER-10620</a>

**id**

`10620`

**code**

`RULE_COMPLETION_RULES_TEMPLATE_NOT_EXIST`

**title**

补全信息项来源不存在,请核对后重新输入.

**detail**

补全信息项来源不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10621">ER-10621</a>

**id**

`10621`

**code**

`RULE_COMPLETION_RULES_TEMPLATE_ITEM_NOT_EXIST`

**title**

补全信息项不存在,请核对后重新输入.

**detail**

补全信息项不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10622">ER-10622</a>

**id**

`10622`

**code**

`RULE_COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST`

**title**

目录待比对信息项不存在,请核对后重新输入.

**detail**

目录待比对信息项不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10623">ER-10623</a>

**id**

`10623`

**code**

`RULE_COMPARISON_RULES_TEMPLATE_NOT_EXIST`

**title**

比对信息项来源不存在,请核对后重新输入.

**detail**

比对信息项来源不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10624">ER-10624</a>

**id**

`10624`

**code**

`RULE_COMPARISON_RULES_TEMPLATE_ITEM_NOT_EXIST`

**title**

比对信息项不存在,请核对后重新输入.

**detail**

比对信息项不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10625">ER-10625</a>

**id**

`10625`

**code**

`RULE_COMPARISON_RULES_BASE_CANNOT_NAME`

**title**

比对项为企业名称,比对依据不能选择企业名称信息项.

**detail**

比对项为企业名称,比对依据不能选择企业名称信息项.

**links**

待补充

### <a name="ER-10626">ER-10626</a>

**id**

`10626`

**code**

`RULE_COMPARISON_RULES_BASE_CANNOT_IDENTIFY`

**title**

比对项为主体代码(统一社会信用代码/身份证号),那比对依据不能选择主体代码(统一社会信用代码/身份证号)信息项.

**detail**

比对项为主体代码(统一社会信用代码/身份证号),那比对依据不能选择主体代码(统一社会信用代码/身份证号)信息项.

**links**

待补充

### <a name="ER-10627">ER-10627</a>

**id**

`10627`

**code**

`RULE_DE_DUPLICATION_RULES_ITEMS_NOT_EXIST`

**title**

去重信息项不存在,请核对后重新输入.

**detail**

去重信息项不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10628">ER-10628</a>

**id**

`10628`

**code**

`RULE_TRANSFORMATION_TEMPLATE_ITEM_TYPE_NOT_EXIST`

**title**

目标信息项类型不存在,请核对后重新输入.

**detail**

目标信息项类型不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10629">ER-10629</a>

**id**

`10629`

**code**

`RULE_TYPE_CANNOT_TRANSFORMATION`

**title**

类型不能转换,请核对后重新输入.

**detail**

类型不能转换,请核对后重新输入.

**links**

待补充

### <a name="ER-10630">ER-10630</a>

**id**

`10630`

**code**

`RULE_LENGTH_CANNOT_TRANSFORMATION`

**title**

长度不能转换,请核对后重新输入.

**detail**

长度不能转换,请核对后重新输入.

**links**

待补充

### <a name="ER-10631">ER-10631</a>

**id**

`10631`

**code**

`RULE_DIMENSION_CANNOT_TRANSFORMATION`

**title**

公开范围不能转换,请核对后重新输入.

**detail**

公开范围不能转换,请核对后重新输入.

**links**

待补充

### <a name="ER-10632">ER-10632</a>

**id**

`10632`

**code**

`RULE_IS_MASKED_CANNOT_TRANSFORMATION`

**title**

脱敏状态不能转换,请核对后重新输入.

**detail**

脱敏状态不能转换,请核对后重新输入.

**links**

待补充

### <a name="ER-10633">ER-10633</a>

**id**

`10633`

**code**

`RULE_MASK_RULE_CANNOT_TRANSFORMATION`

**title**

脱敏范围不能转换,请核对后重新输入.

**detail**

脱敏范围不能转换,请核对后重新输入.

**links**

待补充

### <a name="ER-10634">ER-10634</a>

**id**

`10634`

**code**

`RULE_RULES_TEMPLATE_ZRR_NOT_INCLUDE_IDENTIFY`

**title**

当比对/补全信息项来源为自然人,依据必须包含身份证号.

**detail**

当比对/补全信息项来源为自然人,依据必须包含身份证号.

**links**

待补充

### <a name="ER-10635">ER-10635</a>

**id**

`10635`

**code**

`RULE_TEMPLATE_NOT_EXIST`

**title**

目录不存在,请核对后重新输入.

**detail**

目录不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10636">ER-10636</a>

**id**

`10636`

**code**

`RULE_TEMPLATE_VERSION_NOT_EXIST`

**title**

目录版本记录不存在,请核对后重新输入.

**detail**

目录版本记录不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10637">ER-10637</a>

**id**

`10637`

**code**

`RULE_VERSION_NOT_EXIST`

**title**

规则版本记录不存在,请核对后重新输入.

**detail**

规则版本记录不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10638">ER-10638</a>

**id**

`10638`

**code**

`RULE_IS_UNIQUE`

**title**

规则已经存在,请重新输入.

**detail**

规则已经存在,请重新输入.

**links**

待补充

### ER-10639 - ER-10700 错误预留
