# 科室管理错误提示规范（10051-10100）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* ER-10051: 科室名称格式不正确
* ER-10052: 科室名称已经存在
* ER-10053: 所属委办局为空
* ER-10054: 所属委办局格式不正确
* ER-10055 - ER-10100: 科室管理错误预留

---

### <a name="ER-10051">ER-10051</a>

**id**

`10051`

**code**

`DEPARTMENT_NAME_FORMAT_ERROR`

**title**

科室名称格式不正确, 应为2-20位字符, 请重新输入.

**detail**

科室名称格式不正确, 应为2-20位字符, 请重新输入.

**links**

待补充

### <a name="ER-10052">ER-10052</a>

**id**

`10052`

**code**

`DEPARTMENT_NAME_IS_UNIQUE`

**title**

科室名称已经存在,请重新输入.

**detail**

科室名称已经存在,请重新输入.

**links**

待补充

### <a name="ER-10053">ER-10053</a>

**id**

`10053`

**code**

`DEPARTMENT_USER_GROUP_IS_EMPTY`

**title**

所属委办局不能为空,请继续完善信息.

**detail**

所属委办局不能为空,请继续完善信息.

**links**

待补充

### <a name="ER-10054">ER-10054</a>

**id**

`10054`

**code**

`DEPARTMENT_USER_GROUP_FORMAT_ERROR`

**title**

所属委办局格式不正确,请重新输入.

**detail**

所属委办局格式不正确,请重新输入.

**links**

待补充

### ER-10055 - ER-10100 错误预留