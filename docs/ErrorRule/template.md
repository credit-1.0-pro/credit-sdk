# 目录管理错误提示规范（10501-10600）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* ER-10501: 目录名称格式不正确
* ER-10502: 目录标识格式不正确
* ER-10503: 主体类别格式不正确
* ER-10504: 公开范围格式不正确
* ER-10505: 更新频率格式不正确
* ER-10506: 信息分类格式不正确
* ER-10507: 信息类别格式不正确
* ER-10508: 目录描述格式不正确
* ER-10509: 模板信息格式不正确
* ER-10510: 来源委办局格式不正确
* ER-10511: 版本描述格式不正确
* ER-10512: 版本记录格式不正确
* ER-10513: 信息项名称格式不正确
* ER-10514: 数据标识格式不正确
* ER-10515: 主体名称不存在
* ER-10516: 证件号码不存在
* ER-10517: 统一社会信用代码不存在
* ER-10518: 信息项数据类型格式不正确
* ER-10519: 信息项数据长度格式不正确
* ER-10520: 信息项可选范围格式不正确
* ER-10521: 信息项是否必填格式不正确
* ER-10522: 信息项是否脱敏格式不正确
* ER-10523: 信息项脱敏规则格式不正确
* ER-10524: 信息项备注格式不正确
* ER-10525: 版本记录不存在
* ER-10526: 目录标识已经存在
* ER-10527: 信息项公开范围格式不正确
* ER-10528: 信息项名称必须唯一
* ER-10529: 数据标识必须唯一

---

### <a name="ER-10501">ER-10501</a>

**id**

`10501`

**code**

`TEMPLATE_NAME_FORMAT_ERROR`

**title**

目录名称格式不正确,应为1-100位字符,请重新输入.

**detail**

目录名称格式不正确,应为1-100位字符,请重新输入.

**links**

待补充

### <a name="ER-10502">ER-10502</a>

**id**

`10502`

**code**

`TEMPLATE_IDENTIFY_FORMAT_ERROR`

**title**

目录标识格式不正确,应为1-100位大写字母,请重新输入.

**detail**

目录标识格式不正确,应为1-100位大写字母,请重新输入.

**links**

待补充

### <a name="ER-10503">ER-10503</a>

**id**

`10503`

**code**

`TEMPLATE_SUBJECT_CATEGORY_FORMAT_ERROR`

**title**

主体类别格式不正确,请重新输入.

**detail**

主体类别格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10504">ER-10504</a>

**id**

`10504`

**code**

`TEMPLATE_DIMENSION_FORMAT_ERROR`

**title**

公开范围格式不正确,请重新输入.

**detail**

公开范围格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10505">ER-10505</a>

**id**

`10505`

**code**

`TEMPLATE_EXCHANGE_FREQUENCY_FORMAT_ERROR`

**title**

更新频率格式不正确,请重新输入.

**detail**

更新频率格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10506">ER-10506</a>

**id**

`10506`

**code**

`TEMPLATE_INFO_CLASSIFY_FORMAT_ERROR`

**title**

信息分类格式不正确,请重新输入.

**detail**

信息分类格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10507">ER-10507</a>

**id**

`10507`

**code**

`TEMPLATE_INFO_CATEGORY_FORMAT_ERROR`

**title**

信息类别格式不正确,请重新输入.

**detail**

信息类别格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10508">ER-10508</a>

**id**

`10508`

**code**

`TEMPLATE_DESCRIPTION_FORMAT_ERROR`

**title**

目录描述格式不正确,不能超过2000位字符,请重新输入.

**detail**

目录描述格式不正确,不能超过2000位字符,请重新输入.

**links**

待补充

### <a name="ER-10509">ER-10509</a>

**id**

`10509`

**code**

`TEMPLATE_ITEMS_FORMAT_ERROR`

**title**

模板信息格式不正确,请重新输入.

**detail**

模板信息格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10510">ER-10510</a>

**id**

`10510`

**code**

`TEMPLATE_SOURCE_UNITS_FORMAT_ERROR`

**title**

来源委办局格式不正确,请重新输入.

**detail**

来源委办局格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10511">ER-10511</a>

**id**

`10511`

**code**

`TEMPLATE_VERSION_DESCRIPTION_FORMAT_ERROR`

**title**

版本描述格式不正确,应为1-2000位字符,请重新输入.

**detail**

版本描述格式不正确,应为1-2000位字符,请重新输入.

**links**

待补充

### <a name="ER-10512">ER-10512</a>

**id**

`10512`

**code**

`TEMPLATE_VERSION_FORMAT_ERROR`

**title**

版本记录格式不正确,请重新输入.

**detail**

版本记录格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10513">ER-10513</a>

**id**

`10513`

**code**

`TEMPLATE_ITEMS_NAME_FORMAT_ERROR`

**title**

信息项名称格式不正确,应为1-100位字符,请重新输入.

**detail**

信息项名称格式不正确,应为1-100位字符,请重新输入.

**links**

待补充

### <a name="ER-10514">ER-10514</a>

**id**

`10514`

**code**

`TEMPLATE_ITEMS_IDENTIFY_FORMAT_ERROR`

**title**

数据标识格式不正确,应为1-100位大写字母,请重新输入.

**detail**

数据标识格式不正确,应为1-100位大写字母,请重新输入.

**links**

待补充

### <a name="ER-10515">ER-10515</a>

**id**

`10515`

**code**

`TEMPLATE_SUBJECT_NAME_NOT_EXIT`

**title**

主体名称不存在,请核对后重新输入.

**detail**

主体名称不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10516">ER-10516</a>

**id**

`10516`

**code**

`TEMPLATE_CARD_ID_NOT_EXIT`

**title**

证件号码不存在,请核对后重新输入.

**detail**

证件号码不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10517">ER-10517</a>

**id**

`10517`

**code**

`TEMPLATE_UNIFIED_SOCIAL_CREDIT_CODE_NOT_EXIT`

**title**

统一社会信用代码不存在,请核对后重新输入.

**detail**

统一社会信用代码不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10518">ER-10518</a>

**id**

`10518`

**code**

`TEMPLATE_ITEMS_TYPE_FORMAT_ERROR`

**title**

信息项数据类型格式不正确,请重新输入.

**detail**

信息项数据类型格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10519">ER-10519</a>

**id**

`10519`

**code**

`TEMPLATE_ITEMS_LENGTH_FORMAT_ERROR`

**title**

信息项数据长度格式不正确,请重新输入.

**detail**

信息项数据长度格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10520">ER-10520</a>

**id**

`10520`

**code**

`TEMPLATE_ITEMS_OPTIONS_FORMAT_ERROR`

**title**

信息项可选范围格式不正确,请重新输入.

**detail**

信息项可选范围格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10521">ER-10521</a>

**id**

`10521`

**code**

`TEMPLATE_IS_NECESSARY_FORMAT_ERROR`

**title**

信息项是否必填格式不正确,请重新输入.

**detail**

信息项是否必填格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10522">ER-10522</a>

**id**

`10522`

**code**

`TEMPLATE_IS_MASKED_FORMAT_ERROR`

**title**

信息项是否脱敏格式不正确,请重新输入.

**detail**

信息项是否脱敏格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10523">ER-10523</a>

**id**

`10523`

**code**

`TEMPLATE_MASKED_RULE_FORMAT_ERROR`

**title**

信息项脱敏规则格式不正确,请重新输入.

**detail**

信息项脱敏规则格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10524">ER-10524</a>

**id**

`10524`

**code**

`TEMPLATE_REMARKS_FORMAT_ERROR`

**title**

信息项备注格式不正确,不能超过2000位字符,请重新输入.

**detail**

信息项备注格式不正确,不能超过2000位字符,请重新输入.

**links**

待补充

### <a name="ER-10525">ER-10525</a>

**id**

`10525`

**code**

`TEMPLATE_VERSION_NOT_EXIT`

**title**

版本记录不存在,请核对后重新输入.

**detail**

版本记录不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10526">ER-10526</a>

**id**

`10526`

**code**

`TEMPLATE_IDENTIFY_IS_UNIQUE`

**title**

目录标识已经存在,请重新输入.

**detail**

目录标识已经存在,请重新输入.

**links**

待补充

### <a name="ER-10527">ER-10527</a>

**id**

`10527`

**code**

`TEMPLATE_ITEMS_DIMENSION_FORMAT_ERROR`

**title**

信息项公开范围格式不正确,请重新输入.

**detail**

信息项公开范围格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10528">ER-10528</a>

**id**

`10528`

**code**

`TEMPLATE_ITEMS_NAME_IS_UNIQUE`

**title**

信息项名称在模板信息中必须唯一,请重新输入.

**detail**

信息项名称在模板信息中必须唯一,请重新输入.

**links**

待补充

### <a name="ER-10529">ER-10529</a>

**id**

`10529`

**code**

`TEMPLATE_ITEMS_IDENTIFY_IS_UNIQUE`

**title**

数据标识在模板信息中必须唯一,请重新输入.

**detail**

数据标识在模板信息中必须唯一,请重新输入.

**links**

待补充
### ER-10530 - ER-10600 错误预留
