# 前台用户管理错误提示规范（10201-10300）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* ER-10201: 邮箱格式不正确
* ER-10202: 用户名不存在
* ER-10203: 联系地址格式不正确
* ER-10204: 密保问题格式不正确
* ER-10205: 密保答案格式不正确
* ER-10206: 性别格式不正确
* ER-10207: 用户名已经存在
* ER-10208: 邮箱已经存在
* ER-10209: 用户名格式不正确
* ER-10210: 密保答案不正确

---

### <a name="ER-10201">ER-10201</a>

**id**

`10201`

**code**

`EMAIL_FORMAT_ERROR`

**title**

邮箱格式不正确,应为2-30位字符,且必须包含"@"和".",请重新输入.

**detail**

邮箱格式不正确,应为2-30位字符,且必须包含"@"和".",请重新输入.

**links**

待补充

### <a name="ER-10202">ER-10202</a>

**id**

`10202`

**code**

`USER_NAME_NOT_EXIT`

**title**

用户名不存在,请核对后重新输入.

**detail**

用户名不存在,请核对后重新输入.

**links**

待补充

### <a name="ER-10203">ER-10203</a>

**id**

`10203`

**code**

`CONTACT_ADDRESS_FORMAT_ERROR`

**title**

联系地址格式不正确,应为1-255位字符,请重新输入.

**detail**

联系地址格式不正确,应为1-255位字符,请重新输入.

**links**

待补充

### <a name="ER-10204">ER-10204</a>

**id**

`10204`

**code**

`SECURITY_QUESTION_FORMAT_ERROR`

**title**

密保问题格式不正确,请重新输入.

**detail**

密保问题格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10205">ER-10205</a>

**id**

`10205`

**code**

`SECURITY_ANSWER_FORMAT_ERROR`

**title**

密保答案格式不正确,应为1-30位字符,请重新输入.

**detail**

密保答案格式不正确,应为1-30位字符,请重新输入.

**links**

待补充

### <a name="ER-10206">ER-10206</a>

**id**

`10206`

**code**

`GENDER_FORMAT_ERROR`

**title**

性别格式不正确,请重新输入.

**detail**

性别格式不正确,请重新输入.

**links**

待补充

### <a name="ER-10207">ER-10207</a>

**id**

`10207`

**code**

`USER_NAME_EXIST`

**title**

用户名已经存在,请重新输入.

**detail**

用户名已经存在,请重新输入.

**links**

待补充

### <a name="ER-10208">ER-10208</a>

**id**

`10208`

**code**

`EMAIL_EXIST`

**title**

邮箱已经存在,请重新输入.

**detail**

邮箱已经存在,请重新输入.

**links**

待补充

### <a name="ER-10209">ER-10209</a>

**id**

`10209`

**code**

`USER_NAME_FORMAT_ERROR`

**title**

用户名格式不正确,应为2-20位字符,请重新输入.

**detail**

用户名格式不正确,应为2-20位字符,请重新输入.

**links**

待补充

### <a name="ER-10210">ER-10210</a>

**id**

`10210`

**code**

`SECURITY_ANSWER_INCORRECT`

**title**

密保答案不正确,请重新输入.

**detail**

密保答案不正确,请重新输入.

**links**

待补充

### ER-10511 - ER-10300 错误预留