# 新闻分类控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 新闻分类管理

* `NEWS_CATEGORY_NAME` : 新闻分类名称控件
* `NEWS_CATEGORY_GRADE` : 新闻分类等级控件
* `NEWS_CATEGORY_PARENT_CATEGORY` : 一级分类控件
* `NEWS_CATEGORY_CATEGORY` : 二级分类控件

### `NEWS_CATEGORY_NAME`

**新闻分类名称** 控件规范,用于表述新闻分类名称

#### 规则

* string
* min: 2 max: 20
* 同一分类下新闻分类名称唯一

#### 错误提示

* `ER-10301`,新闻分类名称长度应为2-20字符
* `ER-10302`,新闻分类名称唯一

### `NEWS_CATEGORY_GRADE`

**新闻分类等级** 控件规范,用于表述新闻分类等级

#### 规则

* 必填
* int
* 内容必须在以下范围内
    * GRADE['GRADE_ONE'] =>1 一级
    * GRADE['GRADE_TWO'] =>2  二级
    * GRADE['GRADE_THREE'] =>3  三级

#### 错误提示

* `ER-10303`,新闻分类等级格式不正确

### `NEWS_CATEGORY_PARENT_CATEGORY`

**新闻一级分类** 控件规范,用于表述新闻一级分类

#### 规则

* int
* 当分类等级为一级时,一级分类,二级分类必须为空

#### 错误提示

* `ER-10304`,新闻一级分类格式不正确

### `NEWS_CATEGORY_CATEGORY`

**新闻二级分类** 控件规范,用于表述新闻二级分类

#### 规则

* int
* 当分类等级为二级时,二级分类必须为空

#### 错误提示

* `ER-10305`,新闻二级分类格式不正确
