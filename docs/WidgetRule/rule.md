# 规则控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 规则数据搜索控件规范

* `TEMPLATE`: 目录控件规范.
* `VERSION_DESCRIPTION`: 版本描述控件规范.
* `RULES`: 规则控件规范.
* `REJECT_REASON`: 驳回原因控件规范

### `TEMPLATE`

**目录** 用于表述目录

#### 规则

* 不能为空
* int

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 数据格式不正确

### `VERSION_DESCRIPTION`

**版本描述** 用于表述版本描述

#### 规则

* min: 1 max: 2000 

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-10601`, 版本描述格式不正确

### `RULES`

**规则** 用于表述规则

#### 规则

* array
* 数组的键必须为以下三个

    ```
    RULE_NAME = array(
        'COMPLETION_RULE' => 'completionRule',//补全规则
        'COMPARISON_RULE' => 'comparisonRule',//比对规则
        'DE_DUPLICATION_RULE' => 'deDuplicationRule', //去重规则
    );
    ```
* 补全规则completionRule,必须为数组,且每个字段的补全规则数量不能超过两个,每个补全规则必须包含id,base,item,且id的值为int,base必须为数组且数组值必须在1(企业名称),2(身份证号/统一社会信用代码)的范围内,item为字符串
* 比对规则comparisonRule,必须为数组,且每个字段的比对规则数量不能超过两个,每个比对规则必须包含id,base,item,且id的值为int,base必须为数组且数组值必须在1(企业名称),2(身份证号/统一社会信用代码)的范围内,item为字符串
* 去重规则deDuplicationRule,必须为数组,数组中必须包含result,items,result必须为int,且范围在1(丢弃),2(覆盖)之间,items必须是数组

* `ER-0100`, 数据不能为空
* `ER-10602`, 规则格式不正确

### `REJECT_REASON`
* [驳回原因控件规范(DESCRIPTION)](common.md)(必填)