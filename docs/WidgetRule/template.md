# 目录控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 目录数据搜索控件规范

* `NAME`: 目录名称控件规范.
* `IDENTIFY`: 目录标识控件规范.
* `SUBJECT_CATEGORY`: 主体类别控件规范.
* `DIMENSION`: 公开范围控件规范.
* `EXCHANGE_FREQUENCY`: 更新频率控件规范.
* `INFO_CLASSIFY`: 信息分类控件规范.
* `INFO_CATEGORY`: 信息类别控件规范.
* `DESCRIPTION`: 目录描述控件规范.
* `ITEMS`: 模板信息控件规范.
* `SOURCE_UNITS`: 来源委办局控件规范.
* `VERSION_DESCRIPTION`: 版本描述控件规范.
* `REJECT_REASON`: 驳回原因控件规范

### `NAME`

**目录名称** 用于表述目录名称

#### 规则

* 不能为空
* min: 1 max: 100 汉字

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-10501`, 目录名称格式不正确

### `IDENTIFY`

**目录标识** 用于表述目录标识

#### 规则

* 不能为空
* min:1 max:100 大写字母

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-10502`, 目录标识格式不正确

### `SUBJECT_CATEGORY`

**主体类别** 用于表述主体类别

#### 规则

* int, 内容必须在以下范围内

	* FRJFFRZZ => 1 | 法人及非法人组织
	* ZRR => 2 | 自然人
	* GTGSH => 3 | 个体工商户
* 主体范围必须在目录主体类别的范围内 

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-10503`, 主体类别格式不正确

### `DIMENSION`

**公开范围** 用于表述公开范围

#### 规则

* int, 内容必须在以下范围内

	* SHGK => 1 | 社会公开
	* ZWGX => 2 | 政务共享
	* SQCX => 3 | 授权查询

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-10504`, 公开范围格式不正确

### `EXCHANGE_FREQUENCY`

**更新频率** 用于表述更新频率

#### 规则

* int, 内容必须在以下范围内

     * SS  |  实时 1
     * MR  |  每日 2
     * MZ  |  每周 3
     * MY  |  每月 4
     * MJD |  每季度 5
     * MBN |  每半年 6
     * MYN |  每一年 7
     * MLN |  每两年 8
     * MSN |  每三年 9

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-10505`, 更新频率格式不正确

### `INFO_CLASSIFY`

**信息分类** 用于表述信息分类

#### 规则

* int, 内容必须在以下范围内

	* XZXK => 1 | 行政许可
	* XZCF => 2 | 行政处罚
	* HONGMD => 3 | 红名单
	* HEIMD => 4 | 黑名单
	* QT => 5 | 其他

#### 错误提示

* `ER-0100`,信息分类不能为空
* `ER-10506`, 信息分类格式不正确

### `INFO_CATEGORY`

**信息类别** 用于表述信息类别

#### 规则

* int, 内容必须在以下范围内

	* JCXX => 1 | 基础信息
	* SHOUXXX => 2 | 守信信息
	* SHIXXX => 3 | 失信信息
	* QTXX => 4 | 其他信息

#### 错误提示

* `ER-0100`,信息分类不能为空
* `ER-10507`, 信息类别格式不正确

### `DESCRIPTION`

**目录描述** 用于表述目录描述

#### 规则

* min: 1 max: 2000 

#### 错误提示

* `ER-10508`, 目录描述格式不正确

### `ITEMS`

**模板信息** 用于表述模板信息

#### 规则

* array 
* 数组的键必须包含以下

```
	ITEMS_KEYS = array(
		'name',
		'identify',
		'type',
		'length',
		'options',
		'dimension',
		'isNecessary',
		'isMasked',
		'maskRule',
		'remarks'
	)
```
* name必须为1-100的汉字,且值唯一
* identify必须为1-100的大写字母,且值唯一,identify必须存在'ZTMC','TYSHXYDM(主体类别为法人及非法人组织,个体工商户)','ZJHM(主体类别为自然人)'
* type为 int,且值在以下范围内
```
	* ZFX | 字符型 1
	* RQX | 日期型 2
	* ZSX | 整数型 3
	* FDX | 浮点型 4
	* MJX | 枚举型 5
	* JHX | 集合型 6
```
* length 类型为int
* options 类型为array
* dimension 为int,且值在以下范围内
```
	* SHGK => 1 | 社会公开
	* ZWGX => 2 | 政务共享
	* SQCX => 3 | 授权查询
```
* isNecessary 为int,且值在以下范围内
```
     * FOU | 否 0
     * SHI | 是 1，默认
```
* isMasked 为int,且值在以下范围内
```
     * FOU | 否 0，默认
     * SHI | 是 1
```
* maskRule 如果isMasked为是则不为空,且值为数组,如果isMasked为否,则为空
* remarks不能超过2000位字符

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-10509`, 模板信息格式不正确

### `SOURCE_UNITS`

**来源委办局** 用于表述来源委办局

#### 规则

* array

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-10510`, 来源委办局格式不正确

### `VERSION_DESCRIPTION`

**版本描述** 用于表述版本描述

#### 规则

* min: 1 max: 2000 

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-10511`, 版本描述格式不正确

### `REJECT_REASON`
* [驳回原因控件规范(DESCRIPTION)](common.md)(必填)