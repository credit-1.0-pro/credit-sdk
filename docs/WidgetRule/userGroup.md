# 委办局控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 委办局管理

* `USER_GROUP_NAME` : 委办局名称控件
* `USER_GROUP_SHORT_NAME` : 委办局简称控件
* `USER_GROUP_ADMINISTRATIVE_AREA` : 所属行政区域控件
* `UNIFIED_SOCIAL_CREDIT_CODE`: 统一社会信用代码控件规范.

### `USER_GROUP_NAME`

**委办局名称** 控件规范,用于表述委办局名称

#### 规则

* string
* min: 2 max: 20
* 同一行政区域下委办局名称唯一

#### 错误提示

* `ER-10001`,委办局名称长度应为2-20字符
* `ER-10002`,委办局名称唯一

### `USER_GROUP_SHORT_NAME`

**委办局简称** 控件规范,用于表述委办局简称

#### 规则

* string
* min: 1 max: 8

#### 错误提示

* `ER-10003`,委办局简称长度应为1-8字符

### `USER_GROUP_ADMINISTRATIVE_AREA`

**所属行政区域** 控件规范,用于表述所属行政区域

#### 规则

* 必填
* int
* 内容必须在以下范围内
    * ADMINISTRATIVE_AREA['WLCB'] =>0 乌兰察布市
    * ADMINISTRATIVE_AREA['JNQ'] =>100  集宁区
    * ADMINISTRATIVE_AREA['FZS'] =>101  丰镇市
    * ADMINISTRATIVE_AREA['CYQQ'] =>102  察右前旗
    * ADMINISTRATIVE_AREA['CYZQ'] =>103  察右中旗
    * ADMINISTRATIVE_AREA['CYHQ'] =>104  察右后旗
    * ADMINISTRATIVE_AREA['SZWQ'] =>105  四子王旗
    * ADMINISTRATIVE_AREA['ZZX'] =>106  卓资县
    * ADMINISTRATIVE_AREA['LCX'] =>107  凉城县
    * ADMINISTRATIVE_AREA['XHX'] =>108  兴和县
    * ADMINISTRATIVE_AREA['SDX'] =>109  商都县
    * ADMINISTRATIVE_AREA['HDX'] =>110  化德县

#### 错误提示

* `ER-10004`,所属行政区域格式不正确

### `UNIFIED_SOCIAL_CREDIT_CODE`
* [统一社会信用代码控件规范(UNIFIED_SOCIAL_CREDIT_CODE)](common.md)(必填)