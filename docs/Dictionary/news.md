# 新闻字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### title

**新闻标题** 新闻标题的表述.

* string

### source

**来源** 新闻来源的表述.

* string

### content

**内容** 新闻内容的表述.

* string

### description

**描述** 新闻描述的表述.

* string

### parentCategory

**新闻一级分类** 新闻一级分类的表述.

* NewsCategory

### category

**新闻二级分类** 新闻二级分类的表述.

* NewsCategory

### newsType

**新闻三级分类** 新闻三级分类的表述.

* NewsCategory

### dimension

**公开类型** 新闻公开类型的表述.

* int
	* SOCIOLOGY | 社会公开 | 1 
	* GOVERNMENT_AFFAIRS | 政务共享 | 2

### cover

**封面** 新闻封面的表述.

* array

### attachments

**附件** 新闻附件的表述.

* array

### publishUserGroup

**发布委办局** 新闻发布委办局的表述,此处指的是我们系统中的委办局.

* UserGroup

### crew

**发布人** 新闻发布人的表述

* Crew

### bannerStatus

**轮播状态** 新闻设为轮播的状态表述.

* int

	* BANNER_STATUS_DISABLED | 未轮播 | 0
	* BANNER_STATUS_ENABLED | 轮播 |  2

### bannerImage

**轮播图图片** 轮播图图片的表述.

* array

### homePageShowStatus

**首页展示状态** 新闻首页展示状态的表述.

* int

	* HOME_PAGE_SHOW_STATUS_DISABLED | 未展示 | 0
	* HOME_PAGE_SHOW_STATUS_ENABLED | 展示 |  2

### status

**状态** 新闻状态的表述.

* int

	* STATUS_ENABLED | 启用 | 0
	* STATUS_DISABLED | 禁用 | -2

### stick

**置顶状态** 新闻置顶状态的表述.

* int

	* STICK_ENABLED | 置顶 | 2
	* STICK_DISABLED | 未置顶 | 0
	
### releaseTime

**发布时间** 新闻发布时间的表述.

* int
	
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
