# 用户通用词典

### 英文名称

**中文名称** 描述信息

---

### cellphone

**手机号** 手机号的表述.

* string 

### userName

**用户名** 用户名的表述,Crew默认等于手机号.

* string 

### realName

**姓名** 姓名的表述.

* string

### cardId

**身份证号** 身份证号的表述.

* string

### avatar

**头像** 头像的表述.暂时不需要

* array

### password

**密码** 密码的表述.

* string

### confirmPassword

**确认密码** 确认密码的表述

* string

### oldPassword

**旧密码** 旧密码的表述.

* string