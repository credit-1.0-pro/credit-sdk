# 员工管理字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [人通用字典](./docs/Dictionary/user.md "人通用字典")

### category

**用户类型** 用户类型的表述.

* int
	* SUPER_ADMINISTRATOR => 1 | 超级管理员
	* PLATFORM_ADMINISTRATOR => 2 | 平台管理员
	* USER_GROUP_ADMINISTRATOR => 3 | 委办局管理员
	* OPERATOR => 4 | 操作用户

### userGroup

**所属委办局** 员工所属委办局的表述.

* UserGroup

### department

**所属科室** 所属科室的表述.

* Department

### purview

**权限范围** 权限范围的表述.

* array

### status

**状态** 状态的表述.

* int

	* STATUS_ENABLED 0 启用
	* STATUS_DISABLED -2 禁用

### [手机号](user.md)
### [用户名](user.md)
### [姓名](user.md)
### [身份证号](user.md)
### [密码](user.md)
### [确认密码](user.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)