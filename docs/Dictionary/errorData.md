# 失败数据字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### name

**主体名称** 

* string
* 必填项

### identify

**主体代码** 

* string
* 必填项

### task

**任务** 

* Task

### template

**目录**

* Template
* 必填项

### templateVersion

**目录版本记录** 

* TemplateVersion
* 必填项

### itemsData

**资源目录数据** 

* ItemsData

### errorType

**错误类型**

* int 错误类型的表述.

    * ERROR_TYPE['MISSING_DATA'] => 1//数据缺失
    * ERROR_TYPE['COMPARISON_FAILED'] => 2//比对失败
    * ERROR_TYPE['DUPLICATION_DATA'] => 4//数据重复
    * ERROR_TYPE['FORMAT_VALIDATION_FAILED'] => 8//数据格式错误

### errorReason

**错误原因**

* array 错误原因的表述.

```
    array(
        'ZTMC' => array(
            ERROR_REASON[self::ERROR_TYPE['MISSING_DATA']],
            ERROR_REASON[self::ERROR_TYPE['COMPARISON_FAILED']]
        ),
        'TYSHXYDM' => array(
            ERROR_REASON[self::ERROR_TYPE['MISSING_DATA']],
            ERROR_REASON[self::ERROR_TYPE['COMPARISON_FAILED']]
        ),
    )
```

### errorStatus

**失败状态**

* int 失败状态的表述.
    * ERROR_STATUS['NORMAL'] => 0//正常
    * ERROR_STATUS['PROGRAM_EXCEPTION'] => 1//程序异常
    * ERROR_STATUS['STORAGE_EXCEPTION'] => 2, //入库异常

### status

**数据状态**

* int 数据状态的表述.
    * STATUS['UN_PROCESSED'] => 0//待处理
    * STATUS['PROCESSED'] => 2, 已处理