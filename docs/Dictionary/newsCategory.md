# 新闻分类字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### name

**分类名称** 分类名称的表述.

* string

### grade

**分类等级** 新闻分类等级的表述.

* int

    * GRADE['GRADE_ONE'] =>1 //一级
    * GRADE['GRADE_TWO'] =>2 //二级
    * GRADE['GRADE_THREE'] =>3 //三级

### parentCategory

**一级分类** 新闻一级分类的表述.

* int

### category

**二级分类** 新闻二级分类的表述.

* int

### [状态](common.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)