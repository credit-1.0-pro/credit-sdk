# 资源目录数据搜索字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### name

**主体名称** 

* string
* 必填项

### identify

**主体代码** 

* string
* 必填项

### subjectCategory

**主体类别** 

* int
	* FRJFFRZZ => 1 | 法人及非法人组织
	* ZRR => 2 | 自然人
	* GTGSH => 3 | 个体工商户
* 必填项

### dimension

**公开范围** 

* int
	* SHGK => 1 | 社会公开
	* ZWGX => 2 | 政务共享
	* SQCX => 3 | 授权查询
* 必填项

### exchangeFrequency

**更新频率** 

* int
	* SS => 1 | 实时
	* MR => 2 | 每日
	* MZ => 3 | 每周
	* MY => 4 | 每月
	* MJD => 5 | 每季度
	* MBN => 6 | 每半年
	* MYN => 7 | 每一年
	* MLN => 8 | 每两年
	* MSN => 9 | 每三年
* 必填项

### infoClassify

**信息分类** 

* int
	* XZXK => 1 | 行政许可
	* XZCF => 2 | 行政处罚
	* HONGMD => 3 | 红名单
	* HEIMD => 4 | 黑名单
	* QT => 5 | 其他
* 必填项

### infoCategory

**信息类别** 

* int
	* JCXX => 1 | 基础信息
	* SHOUXXX => 2 | 守信信息
	* SHIXXX => 3 | 失信信息
	* QTXX => 4 | 其他信息

* 必填项

### expirationDate

**有效期限** 

* int
* 选填项

### template

**目录**

* Template
* 必填项

### templateVersion

**目录版本记录** 

* TemplateVersion
* 必填项

### ruleService

**规则**

* RuleService
* 必填项

### ruleServiceVersion

**规则版本记录** 

* RuleServiceVersion
* 必填项

### task

**任务** 任务的表述.

* Task
* 选填项

### crew

**发布人** 

* Crew
* 必填项

### publishUserGroup

**发布委办局** 

* UserGroup
* 必填项
  
### itemsData

**资源目录数据** 

* ItemsData
* 必填项

### hash

**唯一标识** 

* string
* 必填项

### status

**状态** 

* int
	* STATUS['NORMAL'] => 0 | 正常
	* STATUS['DELETE'] => -2 | 屏蔽

### applyStatus

**审核状态** 

* int
	* APPLY_STATUS['DEFAULT'] => 0 | 默认
	* APPLY_STATUS['PENDING'] => 2 | 待审核
	* APPLY_STATUS['APPROVE'] => 4 | 审核通过
	* APPLY_STATUS['REJECT'] => -4 | 审核驳回
* 必填项

### [驳回原因](common.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
