# 规则字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### template

**目录**

* Template
* 必填项

### templateVersion

**目录版本记录** 

* TemplateVersion
* 必填项

### ruleVersion

**当前使用的版本记录** 

* RuleVersion
* 必填项

### rules

**规则** 此处为数组,且包含补全规则,比对规则,去重规则

* array
* 必填项
* 例子:

	```
	array(
		'completionRule' =>  array(
			"目录模板字段标识" => array(
				array("id"=>"补全目录id", "base"=>array("补全依据"), "item" => "补全模板字段标识"),
			),
			'ZTMC' => array(
				array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
				array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
			),
			'TYSHXYDM' => array(
				array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
				array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
			),
		);
		'comparisonRule' =>  array(
			"目录模板字段标识" => array(
				array("id"=>"比对目录id", "base"=>array("比对依据"), "item" => "比对模板字段标识"),
			),
			'ZTMC' => array(
				array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
				array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
			),
			'TYSHXYDM' => array(
				array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
				array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
			),
		);
		'deDuplicationRule' => array("result"=>去重结果(1 丢弃, 2 覆盖), "items"=>array("目录模板字段标识"));
	)
	```
* 补全依据
	* COMPLETION_BASE['NAME'] = 1 //企业名称
	* COMPLETION_BASE['IDENTIFY'] = 2 //统一社会信用代码/身份证号
* 比对依据
	* COMPARISON_BASE['NAME'] = 1 //企业名称
	* COMPARISON_BASE['IDENTIFY'] = 2 //统一社会信用代码/身份证号
* 去重结果
	* DE_DUPLICATION_RESULT['DISCARD'] = 1 //丢弃
	* DE_DUPLICATION_RESULT['COVER'] = 2 //覆盖
* 去重规则样例: array('result'=>1, items=>array('ZTMC', 'TYSHXYDM'));

### publishUserGroup

**发布委办局** 新闻发布委办局的表述,此处指的是我们系统中的委办局.

* UserGroup

### crew

**发布人** 新闻发布人的表述

* Crew

### status

**状态** 

* int

	* STATUS_NORMAL | 正常 | 0
	* STATUS_DELETE | 删除 | -2

### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
