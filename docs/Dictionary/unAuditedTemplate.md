# 目录审核字典

### 英文名称

**中文名称** 描述信息

---

### [当前使用的版本记录](template.md)
### [来源委办局](template.md)
### [目录名称](template.md)
### [目录标识](template.md)
### [主体类别](template.md)
### [公开范围](template.md)
### [更新频率](template.md)
### [信息分类](template.md)
### [信息类别](template.md)
### [目录描述](template.md)
### [模板信息](template.md)
### [发布委办局](template.md)
### [发布人](template.md)
### [状态](template.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
### [操作类型](common.md)
### [审核状态](common.md)
### [驳回原因](common.md)

### applyInfoCategory

**可以申请的信息分类** 可以申请的信息分类的表述.

* int
	* APPLY_INFO_CATEGORY['TEMPLATE'] = 1 //目录

### applyInfoType

**可以申请的最小信息分类** 可以申请的最小信息分类的表述(该字段为预留字段预防出现二级三级分类的情况).

* int
	* APPLY_INFO_TYPE['TEMPLATE'] = array(
		目录类型
	)

### applyUserGroup

**审核委办局** 审核委办局即为发布委办局

* UserGroup

### applyCrew

**审核人** 审核人的表述

* Crew

### publishCrew

**发布人** 发布人的表述

* Crew
  
### relation

**关联目录id** 关联目录id

* int
