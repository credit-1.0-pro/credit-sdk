# 规则审核字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [规则字典](./docs/Dictionary/rule.md "规则字典")

### [目录](rule.md)
### [当前目录使用的版本记录](rule.md)
### [当前使用的版本记录](rule.md)
### [规则](rule.md)
### [发布委办局](news.md)
### [发布人](news.md)
### [状态](rule.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
### [操作类型](common.md)
### [驳回原因](common.md)

### applyUserGroup

**审核委办局** 审核委办局即为发布委办局

* UserGroup

### applyCrew

**审核人** 审核人的表述

* Crew

### publishCrew

**发布人** 发布人的表述

* Crew

### relationId

**关联id** 此处为规则的id

* int

### applyStatus 

**审核状态** 表述审核数据的审核状态.

* int
	* APPLY_STATUS['PENDING'] = 0 //待审核
	* APPLY_STATUS['APPROVE'] = 2 //通过
	* APPLY_STATUS['REJECT'] =-2 //驳回
	* APPLY_STATUS['REVOKE'] =-4 //撤销
