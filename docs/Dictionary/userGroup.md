# 委办局管理字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### name

**委办局名称** 委办局名称的表述.

* string

### shortName

**委办局简称** 委办局简称的表述.

* string

### unifiedSocialCreditCode

**统一社会信用代码** 统一社会信用代码的表述.

* string

### administrativeArea

**所属行政区域** 所属行政区域的表述.

* int 

	* ADMINISTRATIVE_AREA['WLCB'] =>0 乌兰察布市
     * ADMINISTRATIVE_AREA['JNQ'] =>100  集宁区
     * ADMINISTRATIVE_AREA['FZS'] =>101  丰镇市
     * ADMINISTRATIVE_AREA['CYQQ'] =>102  察右前旗
     * ADMINISTRATIVE_AREA['CYZQ'] =>103  察右中旗
     * ADMINISTRATIVE_AREA['CYHQ'] =>104  察右后旗
     * ADMINISTRATIVE_AREA['SZWQ'] =>105  四子王旗
     * ADMINISTRATIVE_AREA['ZZX'] =>106  卓资县
     * ADMINISTRATIVE_AREA['LCX'] =>107  凉城县
     * ADMINISTRATIVE_AREA['XHX'] =>108  兴和县
     * ADMINISTRATIVE_AREA['SDX'] =>109  商都县
     * ADMINISTRATIVE_AREA['HDX'] =>110  化德县

### pageScene

**请求分页类型** 请求分页类型的表述.

* Lw=>获取分页数据/MA=>获取全部数据
* string

### [状态](common.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)