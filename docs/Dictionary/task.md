# 资源目录数据导入任务字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### crew

**发布人** 

* Crew

### userGroup

**发布委办局** 

* UserGroup

### administrativeArea

**所属行政区域** 

* int

### total

**总数** 总数的表述.

* int

### successNumber

**成功数** 成功数的表述.

* int

### failureNumber

**失败数** 失败数的表述.

* int

### template

**目录**

* Template
* 必填项

### templateVersion

**目录版本记录** 

* TemplateVersion
* 必填项

### ruleService

**规则**

* RuleService
* 必填项

### ruleServiceVersion

**规则版本记录** 

* RuleServiceVersion
* 必填项

### status

**状态** 状态的表述.

* int

	* DEFAULT 0 进行中,默认
	* SUCCESS 2 成功
	* FAILURE -2 失败
	* FAILURE_FILE_DOWNLOAD -3 失败文件下载成功

### fileName

**文件名** 上传文件文件名的表述.

* string
* 资源目录标识_资源目录id_员工id_文件内容hash.xls