# 新闻审核字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [新闻字典](./docs/Dictionary/news.md "新闻字典")

### [标题](news.md)
### [来源](news.md)
### [内容](news.md)
### [描述](news.md)
### [新闻一级分类](news.md)
### [新闻二级分类](news.md)
### [新闻三级分类](news.md)
### [公开类型](news.md)
### [封面](news.md)
### [附件](news.md)
### [发布委办局](news.md)
### [发布人](news.md)
### [轮播状态](news.md)
### [轮播图图片](news.md)
### [首页展示状态](news.md)
### [状态](news.md)
### [置顶状态](news.md)
### [发布时间](news.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
### [操作类型](common.md)
### [审核状态](common.md)
### [驳回原因](common.md)

### applyInfoCategory

**可以申请的信息分类** 可以申请的信息分类的表述.

* int
	* APPLY_INFO_CATEGORY['NEWS'] = 1 //新闻

### applyInfoType

**可以申请的最小信息分类** 可以申请的最小信息分类的表述(该字段为预留字段预防出现二级三级分类的情况).

* int
	* APPLY_INFO_TYPE['NEWS'] = array(
		新闻类型
	)

### applyUserGroup

**审核委办局** 审核委办局即为发布委办局

* UserGroup

### applyCrew

**审核人** 审核人的表述

* Crew

### publishCrew

**发布人** 发布人的表述

* Crew
  
### relationId

**关联新闻id** 关联新闻id

* int
