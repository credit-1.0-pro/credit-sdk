# 目录版本字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### number

**版本号** 版本号的表述.

* int
* 年月日+四位随机数(2108053456)
* 自动生成

### description

**版本描述** 

* string
* 必填项

### templateId

**目录id** 目录id的表述.

* int 

### info

**版本信息** 版本信息的表述.

* array 

### status

**状态** 状态的表述.

* int
	* STATUS['ENABLED'] = 0 //启用
	* STATUS['DISABLED'] =-2 //禁用, 默认

### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)