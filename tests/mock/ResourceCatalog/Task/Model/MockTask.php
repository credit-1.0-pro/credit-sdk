<?php
namespace Sdk\ResourceCatalog\Task\Model;

class MockTask extends Task
{
    public function fileIsExits(string $filePath) : bool
    {
        return parent::fileIsExits($filePath);
    }
}
