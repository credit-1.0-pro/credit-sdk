<?php
namespace Sdk\ResourceCatalog\Task\Adapter\Task;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

class MockTaskRestfulAdapter extends TaskRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
