<?php
namespace Sdk\ResourceCatalog\Task\Translator;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

class MockTaskTranslator extends TaskTranslator
{
    public function getCrewTranslator() : CrewTranslator
    {
        return parent::getCrewTranslator();
    }

    public function getUserGroupTranslator() : UserGroupTranslator
    {
        return parent::getUserGroupTranslator();
    }
}
