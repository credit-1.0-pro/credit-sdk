<?php
namespace Sdk\ResourceCatalog\Task\Translator;

use Sdk\User\Crew\Translator\CrewRestfulTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

class MockTaskRestfulTranslator extends TaskRestfulTranslator
{
    public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return parent::getUserGroupRestfulTranslator();
    }

    public function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return parent::getCrewRestfulTranslator();
    }
}
