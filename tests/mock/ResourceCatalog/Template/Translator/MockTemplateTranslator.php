<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

class MockTemplateTranslator extends TemplateTranslator
{
    public function getCrewTranslator() : CrewTranslator
    {
        return parent::getCrewTranslator();
    }

    public function getUserGroupTranslator() : UserGroupTranslator
    {
        return parent::getUserGroupTranslator();
    }

    public function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return parent::getTemplateVersionTranslator();
    }
}
