<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Sdk\User\Crew\Translator\CrewRestfulTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

class MockTemplateVersionRestfulTranslator extends TemplateVersionRestfulTranslator
{
    public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return parent::getUserGroupRestfulTranslator();
    }

    public function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return parent::getCrewRestfulTranslator();
    }
}
