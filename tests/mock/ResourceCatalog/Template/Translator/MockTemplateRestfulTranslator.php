<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Sdk\User\Crew\Translator\CrewRestfulTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

class MockTemplateRestfulTranslator extends TemplateRestfulTranslator
{
    public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return parent::getUserGroupRestfulTranslator();
    }

    public function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return parent::getCrewRestfulTranslator();
    }

    public function getTemplateVersionRestfulTranslator() : TemplateVersionRestfulTranslator
    {
        return parent::getTemplateVersionRestfulTranslator();
    }
}
