<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

class MockTemplateVersionTranslator extends TemplateVersionTranslator
{
    public function getCrewTranslator() : CrewTranslator
    {
        return parent::getCrewTranslator();
    }

    public function getUserGroupTranslator() : UserGroupTranslator
    {
        return parent::getUserGroupTranslator();
    }
}
