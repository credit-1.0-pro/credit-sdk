<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Sdk\ResourceCatalog\Template\Model\Template;

class MockTemplateTranslatorTrait
{
    use TemplateTranslatorTrait;

    public function publicGetSubjectCategoryCn(array $subjectCategory) : array
    {
        return $this->getSubjectCategoryCn($subjectCategory);
    }

    public function publicGetDimensionCn(int $dimension) : array
    {
        return $this->getDimensionCn($dimension);
    }

    public function publicGetExchangeFrequencyCn(int $exchangeFrequency) : array
    {
        return $this->getExchangeFrequencyCn($exchangeFrequency);
    }

    public function publicGetInfoClassifyCn(int $infoClassify) : array
    {
        return $this->getInfoClassifyCn($infoClassify);
    }

    public function publicGetInfoCategoryCn(int $infoCategory) : array
    {
        return $this->getInfoCategoryCn($infoCategory);
    }

    public function publicGetItemCn(array $items) : array
    {
        return $this->getItemCn($items);
    }

    public function publicGetInfoCn(array $info) : array
    {
        return $this->getInfoCn($info);
    }
}
