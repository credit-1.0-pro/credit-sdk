<?php
namespace Sdk\ResourceCatalog\Template\Adapter\Template;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\ResourceCatalog\Template\Model\Template;

class MockTemplateRestfulAdapter extends TemplateRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function addAction(Template $template) : bool
    {
        return parent::addAction($template);
    }

    public function editAction(Template $template) : bool
    {
        return parent::editAction($template);
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
