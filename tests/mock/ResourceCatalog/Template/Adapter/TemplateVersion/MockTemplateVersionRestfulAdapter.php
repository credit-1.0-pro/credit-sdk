<?php
namespace Sdk\ResourceCatalog\Template\Adapter\TemplateVersion;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

class MockTemplateVersionRestfulAdapter extends TemplateVersionRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
