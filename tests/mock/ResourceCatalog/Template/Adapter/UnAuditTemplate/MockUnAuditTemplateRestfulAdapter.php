<?php
namespace Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Model\IApplyAble;

use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;

class MockUnAuditTemplateRestfulAdapter extends UnAuditTemplateRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function addAction(UnAuditTemplate $unAuditTemplate) : bool
    {
        return parent::addAction($unAuditTemplate);
    }

    public function editAction(UnAuditTemplate $unAuditTemplate) : bool
    {
        return parent::editAction($unAuditTemplate);
    }

    public function approveAction(IApplyAble $applyAbleObject) : bool
    {
        return parent::approveAction($applyAbleObject);
    }

    public function rejectAction(IApplyAble $applyAbleObject) : bool
    {
        return parent::rejectAction($applyAbleObject);
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
