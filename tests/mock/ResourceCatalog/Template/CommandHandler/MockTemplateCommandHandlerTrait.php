<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;
use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;
use Sdk\ResourceCatalog\Template\Repository\UnAuditTemplateRepository;

use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Repository\TemplateVersionRepository;

use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class MockTemplateCommandHandlerTrait
{
    use TemplateCommandHandlerTrait;

    public function publicGetTemplate() : Template
    {
        return $this->getTemplate();
    }

    public function publicGetUnAuditTemplate() : UnAuditTemplate
    {
        return $this->getUnAuditTemplate();
    }

    public function publicGetRepository() : TemplateRepository
    {
        return $this->getRepository();
    }

    public function publicGetUnAuditTemplateRepository() : UnAuditTemplateRepository
    {
        return $this->getUnAuditTemplateRepository();
    }

    public function publicFetchTemplate(int $id) : Template
    {
        return $this->fetchTemplate($id);
    }

    public function publicFetchUnAuditTemplate(int $id) : UnAuditTemplate
    {
        return $this->fetchUnAuditTemplate($id);
    }

    public function publicGetTemplateVersionRepository() : TemplateVersionRepository
    {
        return $this->getTemplateVersionRepository();
    }

    public function publicFetchTemplateVersion(int $id) : TemplateVersion
    {
        return $this->fetchTemplateVersion($id);
    }

    public function publicGetUserGroupRepository() : UserGroupRepository
    {
        return $this->getUserGroupRepository();
    }

    public function publicFetchSourceUnits(array $ids) : array
    {
        return $this->fetchSourceUnits($ids);
    }

    public function publicTemplateExecuteAction(ICommand $command, Template $template) : Template
    {
        return $this->templateExecuteAction($command, $template);
    }
}
