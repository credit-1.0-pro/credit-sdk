<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate;

use Sdk\Common\Model\IApplyAble;

class MockRejectUnAuditTemplateCommandHandler extends RejectUnAuditTemplateCommandHandler
{
    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
