<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate;

use Sdk\Common\Model\IApplyAble;

class MockApproveUnAuditTemplateCommandHandler extends ApproveUnAuditTemplateCommandHandler
{
    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
