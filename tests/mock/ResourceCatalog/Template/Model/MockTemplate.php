<?php
namespace Sdk\ResourceCatalog\Template\Model;

use Sdk\Common\Adapter\IOperateAbleAdapter;

class MockTemplate extends Template
{
    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }
}
