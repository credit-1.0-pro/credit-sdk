<?php
namespace Sdk\ResourceCatalog\Template\Model;

class MockNullTemplate extends NullTemplate
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
