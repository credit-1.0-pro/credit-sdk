<?php
namespace Sdk\ResourceCatalog\Template\Model;

use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\ResourceCatalog\Template\Repository\UnAuditTemplateRepository;

class MockUnAuditTemplate extends UnAuditTemplate
{
    public function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return parent::getIApplyAbleAdapter();
    }

    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function getUnAuditTemplateRepository() : UnAuditTemplateRepository
    {
        return parent::getUnAuditTemplateRepository();
    }
}
