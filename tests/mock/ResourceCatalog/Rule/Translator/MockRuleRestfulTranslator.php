<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Sdk\User\Crew\Translator\CrewRestfulTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;

class MockRuleRestfulTranslator extends RuleRestfulTranslator
{
    public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return parent::getUserGroupRestfulTranslator();
    }

    public function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return parent::getCrewRestfulTranslator();
    }

    public function getTemplateVersionRestfulTranslator() : TemplateVersionRestfulTranslator
    {
        return parent::getTemplateVersionRestfulTranslator();
    }

    public function getTemplateRestfulTranslator() : TemplateRestfulTranslator
    {
        return parent::getTemplateRestfulTranslator();
    }

    public function getRuleVersionRestfulTranslator() : RuleVersionRestfulTranslator
    {
        return parent::getRuleVersionRestfulTranslator();
    }
}
