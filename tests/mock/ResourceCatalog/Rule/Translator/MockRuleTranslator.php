<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

use Sdk\ResourceCatalog\Template\Translator\TemplateTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionTranslator;

class MockRuleTranslator extends RuleTranslator
{
    public function getCrewTranslator() : CrewTranslator
    {
        return parent::getCrewTranslator();
    }

    public function getUserGroupTranslator() : UserGroupTranslator
    {
        return parent::getUserGroupTranslator();
    }

    public function getRuleVersionTranslator() : RuleVersionTranslator
    {
        return parent::getRuleVersionTranslator();
    }

    public function getTemplateTranslator() : TemplateTranslator
    {
        return parent::getTemplateTranslator();
    }

    public function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return parent::getTemplateVersionTranslator();
    }
}
