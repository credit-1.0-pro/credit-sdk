<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Sdk\ResourceCatalog\Template\Translator\TemplateTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionTranslator;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

class MockRuleVersionTranslator extends RuleVersionTranslator
{
    public function getCrewTranslator() : CrewTranslator
    {
        return parent::getCrewTranslator();
    }

    public function getTemplateTranslator() : TemplateTranslator
    {
        return parent::getTemplateTranslator();
    }

    public function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return parent::getTemplateVersionTranslator();
    }

    public function getUserGroupTranslator() : UserGroupTranslator
    {
        return parent::getUserGroupTranslator();
    }
}
