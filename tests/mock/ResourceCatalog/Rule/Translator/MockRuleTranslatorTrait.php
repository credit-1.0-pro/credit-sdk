<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Sdk\ResourceCatalog\Rule\Model\IRule;

class MockRuleTranslatorTrait
{
    use RuleTranslatorTrait;

    public function publicGetRulesFormatConversion(array $rules) : array
    {
        return $this->getRulesFormatConversion($rules);
    }

    public function publicCompletionRulesFormatConversion(array $rule) : array
    {
        return $this->completionRulesFormatConversion($rule);
    }

    public function publicGetCompletionBaseCn(array $base) : array
    {
        return $this->getCompletionBaseCn($base);
    }

    public function publicComparisonRulesFormatConversion(array $rule) : array
    {
        return $this->comparisonRulesFormatConversion($rule);
    }

    public function publicGetComparisonBaseCn(array $base) : array
    {
        return $this->getComparisonBaseCn($base);
    }

    public function publicDeDuplicationRulesFormatConversion(array $rule) : array
    {
        return $this->deDuplicationRulesFormatConversion($rule);
    }

    public function publicGetDeDuplicationResultCn(int $result) : array
    {
        return $this->getDeDuplicationResultCn($result);
    }

    public function publicGetInfoCn(array $info) : array
    {
        return $this->getInfoCn($info);
    }
}
