<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Sdk\User\Crew\Translator\CrewRestfulTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;

class MockRuleVersionRestfulTranslator extends RuleVersionRestfulTranslator
{
    public function getTemplateVersionRestfulTranslator() : TemplateVersionRestfulTranslator
    {
        return parent::getTemplateVersionRestfulTranslator();
    }

    public function getTemplateRestfulTranslator() : TemplateRestfulTranslator
    {
        return parent::getTemplateRestfulTranslator();
    }

    public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return parent::getUserGroupRestfulTranslator();
    }

    public function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return parent::getCrewRestfulTranslator();
    }
}
