<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\Rule;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\ResourceCatalog\Rule\Model\Rule;

class MockRuleRestfulAdapter extends RuleRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function addAction(Rule $rule) : bool
    {
        return parent::addAction($rule);
    }

    public function editAction(Rule $rule) : bool
    {
        return parent::editAction($rule);
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
