<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\RuleVersion;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

class MockRuleVersionRestfulAdapter extends RuleVersionRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
