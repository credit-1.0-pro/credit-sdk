<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Model\IApplyAble;

use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;

class MockUnAuditRuleRestfulAdapter extends UnAuditRuleRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function addAction(UnAuditRule $unAuditRule) : bool
    {
        return parent::addAction($unAuditRule);
    }

    public function editAction(UnAuditRule $unAuditRule) : bool
    {
        return parent::editAction($unAuditRule);
    }

    public function approveAction(IApplyAble $applyAbleObject) : bool
    {
        return parent::approveAction($applyAbleObject);
    }

    public function rejectAction(IApplyAble $applyAbleObject) : bool
    {
        return parent::rejectAction($applyAbleObject);
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
