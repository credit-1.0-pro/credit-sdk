<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Repository\RuleRepository;
use Sdk\ResourceCatalog\Rule\Repository\UnAuditRuleRepository;

use Sdk\ResourceCatalog\Rule\Model\RuleVersion;
use Sdk\ResourceCatalog\Rule\Repository\RuleVersionRepository;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class MockRuleCommandHandlerTrait
{
    use RuleCommandHandlerTrait;

    public function publicGetRule() : Rule
    {
        return $this->getRule();
    }

    public function publicGetUnAuditRule() : UnAuditRule
    {
        return $this->getUnAuditRule();
    }

    public function publicGetRepository() : RuleRepository
    {
        return $this->getRepository();
    }

    public function publicGetUnAuditRuleRepository() : UnAuditRuleRepository
    {
        return $this->getUnAuditRuleRepository();
    }

    public function publicFetchRule(int $id) : Rule
    {
        return $this->fetchRule($id);
    }

    public function publicFetchUnAuditRule(int $id) : UnAuditRule
    {
        return $this->fetchUnAuditRule($id);
    }

    public function publicGetRuleVersionRepository() : RuleVersionRepository
    {
        return $this->getRuleVersionRepository();
    }

    public function publicFetchRuleVersion(int $id) : RuleVersion
    {
        return $this->fetchRuleVersion($id);
    }

    public function publicGetTemplateRepository() : TemplateRepository
    {
        return $this->getTemplateRepository();
    }

    public function publicFetchTemplate(int $id) : Template
    {
        return $this->fetchTemplate($id);
    }

    public function publicRuleExecuteAction(ICommand $command, Rule $rule) : Rule
    {
        return $this->ruleExecuteAction($command, $rule);
    }
}
