<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule;

use Sdk\Common\Model\IApplyAble;

class MockRejectUnAuditRuleCommandHandler extends RejectUnAuditRuleCommandHandler
{
    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
