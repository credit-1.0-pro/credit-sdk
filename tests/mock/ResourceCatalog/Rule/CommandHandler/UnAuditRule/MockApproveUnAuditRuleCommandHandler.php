<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule;

use Sdk\Common\Model\IApplyAble;

class MockApproveUnAuditRuleCommandHandler extends ApproveUnAuditRuleCommandHandler
{
    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
