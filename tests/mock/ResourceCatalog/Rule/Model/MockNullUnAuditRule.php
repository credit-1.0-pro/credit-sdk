<?php
namespace Sdk\ResourceCatalog\Rule\Model;

class MockNullUnAuditRule extends NullUnAuditRule
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
