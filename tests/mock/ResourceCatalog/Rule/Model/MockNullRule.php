<?php
namespace Sdk\ResourceCatalog\Rule\Model;

class MockNullRule extends NullRule
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
