<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\ResourceCatalog\Rule\Repository\UnAuditRuleRepository;

class MockUnAuditRule extends UnAuditRule
{
    public function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return parent::getIApplyAbleAdapter();
    }

    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function getUnAuditRuleRepository() : UnAuditRuleRepository
    {
        return parent::getUnAuditRuleRepository();
    }
}
