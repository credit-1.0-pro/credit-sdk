<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Sdk\Common\Adapter\IOperateAbleAdapter;

class MockRule extends Rule
{
    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }
}
