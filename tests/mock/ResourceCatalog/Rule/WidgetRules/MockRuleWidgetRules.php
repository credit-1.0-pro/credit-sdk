<?php
namespace Sdk\ResourceCatalog\Rule\WidgetRules;

class MockRuleWidgetRules extends RuleWidgetRules
{
    public function ruleName(string $name) : bool
    {
        return parent::ruleName($name);
    }

    public function ruleFormat($rule) : bool
    {
        return parent::ruleFormat($rule);
    }

    public function validateRule(string $name, array $rule) : bool
    {
        return parent::validateRule($name, $rule);
    }

    public function validateCompletionRules(array $completionRules) : bool
    {
        return parent::validateCompletionRules($completionRules);
    }

    public function validateComparisonRules(array $comparisonRules) : bool
    {
        return parent::validateComparisonRules($comparisonRules);
    }

    public function validateDeDuplicationRules(array $deDuplicationRule) : bool
    {
        return parent::validateDeDuplicationRules($deDuplicationRule);
    }
}
