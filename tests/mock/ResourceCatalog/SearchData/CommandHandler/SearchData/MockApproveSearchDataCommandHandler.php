<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use Sdk\Common\Model\IApplyAble;

class MockApproveSearchDataCommandHandler extends ApproveSearchDataCommandHandler
{
    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
