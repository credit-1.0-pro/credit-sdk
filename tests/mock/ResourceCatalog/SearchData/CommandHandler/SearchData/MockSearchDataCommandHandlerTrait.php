<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\Repository\SearchDataRepository;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;

class MockSearchDataCommandHandlerTrait
{
    use SearchDataCommandHandlerTrait;

    public function publicGetSearchData() : SearchData
    {
        return $this->getSearchData();
    }

    public function publicGetRepository() : SearchDataRepository
    {
        return $this->getRepository();
    }

    public function publicFetchSearchData(int $id) : SearchData
    {
        return $this->fetchSearchData($id);
    }

    public function publicGetTemplateRepository() : TemplateRepository
    {
        return $this->getTemplateRepository();
    }

    public function publicFetchTemplate(int $id) : Template
    {
        return $this->fetchTemplate($id);
    }
}
