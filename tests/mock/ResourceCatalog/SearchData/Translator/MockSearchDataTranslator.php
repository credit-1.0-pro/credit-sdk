<?php
namespace Sdk\ResourceCatalog\SearchData\Translator;

use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionTranslator;
use Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataTranslator;

class MockSearchDataTranslator extends SearchDataTranslator
{
    public function getUserGroupTranslator() : UserGroupTranslator
    {
        return parent::getUserGroupTranslator();
    }

    public function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return parent::getTemplateVersionTranslator();
    }

    public function getItemsDataTranslator() : ItemsDataTranslator
    {
        return parent::getItemsDataTranslator();
    }

    public function typeFormatConversion(int $id, array $typeCn) : array
    {
        return parent::typeFormatConversion($id, $typeCn);
    }

    public function statusFormatConversion(int $id, array $statusCn, array $statusType) : array
    {
        return parent::statusFormatConversion($id, $statusCn, $statusType);
    }
}
