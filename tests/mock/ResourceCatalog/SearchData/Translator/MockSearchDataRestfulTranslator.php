<?php
namespace Sdk\ResourceCatalog\SearchData\Translator;

use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;
use Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataRestfulTranslator;

class MockSearchDataRestfulTranslator extends SearchDataRestfulTranslator
{
    public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return parent::getUserGroupRestfulTranslator();
    }

    public function getTemplateVersionRestfulTranslator() : TemplateVersionRestfulTranslator
    {
        return parent::getTemplateVersionRestfulTranslator();
    }
    
    public function getItemsDataRestfulTranslator() : ItemsDataRestfulTranslator
    {
        return parent::getItemsDataRestfulTranslator();
    }
}
