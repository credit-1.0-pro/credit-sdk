<?php
namespace Sdk\ResourceCatalog\SearchData\Model;

class MockNullSearchData extends NullSearchData
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
