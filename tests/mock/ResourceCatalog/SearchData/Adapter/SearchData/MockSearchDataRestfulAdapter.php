<?php
namespace Sdk\ResourceCatalog\SearchData\Adapter\SearchData;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

class MockSearchDataRestfulAdapter extends SearchDataRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
    
    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function publicMapErrors()
    {
        return parent::mapErrors();
    }
}
