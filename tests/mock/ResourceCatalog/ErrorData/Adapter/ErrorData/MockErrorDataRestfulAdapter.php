<?php
namespace Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

class MockErrorDataRestfulAdapter extends ErrorDataRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function publicMapErrors()
    {
        return parent::mapErrors();
    }
    
    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
