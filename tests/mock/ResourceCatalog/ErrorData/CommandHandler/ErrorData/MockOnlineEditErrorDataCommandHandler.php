<?php
namespace Sdk\ResourceCatalog\ErrorData\CommandHandler\ErrorData;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\ErrorData\Repository\ErrorDataRepository;

class MockOnlineEditErrorDataCommandHandler extends OnlineEditErrorDataCommandHandler
{
    public function getRepository() : ErrorDataRepository
    {
        return parent::getRepository();
    }

    public function fetchErrorData(int $id) : ErrorData
    {
        return parent::fetchErrorData($id);
    }
}
