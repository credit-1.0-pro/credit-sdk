<?php
namespace Sdk\ResourceCatalog\ErrorData\Translator;

use Sdk\ResourceCatalog\Task\Translator\TaskTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionTranslator;
use Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataTranslator;

class MockErrorDataTranslator extends ErrorDataTranslator
{
    public function getTaskTranslator() : TaskTranslator
    {
        return parent::getTaskTranslator();
    }

    public function getTemplateTranslator() : TemplateTranslator
    {
        return parent::getTemplateTranslator();
    }

    public function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return parent::getTemplateVersionTranslator();
    }

    public function getItemsDataTranslator() : ItemsDataTranslator
    {
        return parent::getItemsDataTranslator();
    }
}
