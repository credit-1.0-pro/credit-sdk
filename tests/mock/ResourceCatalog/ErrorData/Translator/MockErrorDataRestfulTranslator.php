<?php
namespace Sdk\ResourceCatalog\ErrorData\Translator;

use Sdk\ResourceCatalog\Task\Translator\TaskRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;
use Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataRestfulTranslator;

class MockErrorDataRestfulTranslator extends ErrorDataRestfulTranslator
{
    public function getTaskRestfulTranslator() : TaskRestfulTranslator
    {
        return parent::getTaskRestfulTranslator();
    }

    public function getTemplateRestfulTranslator() : TemplateRestfulTranslator
    {
        return parent::getTemplateRestfulTranslator();
    }

    public function getTemplateVersionRestfulTranslator() : TemplateVersionRestfulTranslator
    {
        return parent::getTemplateVersionRestfulTranslator();
    }

    public function getItemsDataRestfulTranslator() : ItemsDataRestfulTranslator
    {
        return parent::getItemsDataRestfulTranslator();
    }
}
