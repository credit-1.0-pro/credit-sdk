<?php
namespace Sdk\ResourceCatalog\Attachment\Model;

use Sdk\ResourceCatalog\Task\Repository\TaskRepository;
use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;

class MockAttachment extends Attachment
{
    public function getTaskRepository() : TaskRepository
    {
        return parent::getTaskRepository();
    }

    public function getTemplateRepository() : TemplateRepository
    {
        return parent::getTemplateRepository();
    }

    public function validate() : bool
    {
        return parent::validate();
    }

    public function isCrewExist() : bool
    {
        return parent::isCrewExist();
    }

    public function validateFileIsEmpty() : bool
    {
        return parent::validateFileIsEmpty();
    }

    public function validateFileSize() : bool
    {
        return parent::validateFileSize();
    }

    public function validateFileExtension() : bool
    {
        return parent::validateFileExtension();
    }

    public function validateFileError() : bool
    {
        return parent::validateFileError();
    }

    public function validateFileName() : bool
    {
        return parent::validateFileName();
    }

    public function validateFileIsExist() : bool
    {
        return parent::validateFileIsExist();
    }

    public function generateFilePath() : string
    {
        return parent::generateFilePath();
    }

    public function move(string $destinationFile) : bool
    {
        return parent::move($destinationFile);
    }
}
