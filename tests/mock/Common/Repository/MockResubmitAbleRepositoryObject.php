<?php
namespace Sdk\Common\Repository;

class MockResubmitAbleRepositoryObject
{
    use ResubmitAbleRepositoryTrait;
}
