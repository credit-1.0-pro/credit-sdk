<?php
namespace Sdk\Common\Translator;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

class MockApplyTranslatorTrait
{
    use ApplyTranslatorTrait;

    public function publicGetUserGroupTranslator() : UserGroupTranslator
    {
        return $this->getUserGroupTranslator();
    }

    public function publicGetCrewTranslator() : CrewTranslator
    {
        return $this->getCrewTranslator();
    }
}
