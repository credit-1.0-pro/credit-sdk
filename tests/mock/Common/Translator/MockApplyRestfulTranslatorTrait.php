<?php
namespace Sdk\Common\Translator;

use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

class MockApplyRestfulTranslatorTrait
{
    use ApplyRestfulTranslatorTrait;

    public function publicGetUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return $this->getUserGroupRestfulTranslator();
    }

    public function publicGetCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return $this->getCrewRestfulTranslator();
    }
}
