<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IApplyAble;

class MockApplyAbleRestfulAdapterObject
{
    use ApplyAbleRestfulAdapterTrait;

    protected function getResource() : string
    {
        return '';
    }

    public function approveActionPublic(IApplyAble $applyAbleObject) : bool
    {
        return $this->approveAction($applyAbleObject);
    }

    public function rejectActionPublic(IApplyAble $applyAbleObject) : bool
    {
        return $this->rejectAction($applyAbleObject);
    }
}
