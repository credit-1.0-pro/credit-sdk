<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IResubmitAble;

class MockResubmitAbleAdapter implements IResubmitAbleAdapter
{
    public function resubmit(IResubmitAble $resubmitAbleObject) : bool
    {
        unset($resubmitAbleObject);
        return true;
    }
}
