<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Adapter\IApplyAbleAdapter;

class MockApplyAbleAdapter implements IApplyAbleAdapter
{
    public function approve(IApplyAble $applyAbleObject) : bool
    {
        unset($applyAbleObject);
        return true;
    }

    public function reject(IApplyAble $applyAbleObject) : bool
    {
        unset($applyAbleObject);
        return true;
    }
}
