<?php
namespace Sdk\Common\Adapter;

use Marmot\Interfaces\INull;

class MockFetchAbleRestfulAdapterObject
{
    use FetchAbleRestfulAdapterTrait;

    protected function getResource() : string
    {
        return '';
    }

    protected function getNullObject() : INull
    {
    }

    public function fetchOneActionPublic($id)
    {
        return $this->fetchOneAction($id);
    }

    public function fetchListActionPublic(array $ids)
    {
        return $this->fetchListAction($ids);
    }

    public function searchActionPublic(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->searchAction($filter, $sort, $number, $size);
    }
}
