<?php
namespace Sdk\Common\Adapter;

class MockAsyncFetchAbleRestfulAdapterObject
{
    use AsyncFetchAbleRestfulAdapterTrait;

    protected function getResource() : string
    {
        return '';
    }

    public function fetchOneAsyncActionPublic(int $id)
    {
        return $this->fetchOneAsyncAction($id);
    }

    public function fetchListAsyncActionPublic(array $ids)
    {
        return $this->fetchListAsyncAction($ids);
    }

    public function searchAsyncActionPublic(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) {
        return $this->searchAsyncAction($filter, $sort, $number, $size);
    }
}
