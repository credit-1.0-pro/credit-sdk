<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IOperateAble;

class MockOperateAbleAdapter implements IOperateAbleAdapter
{
    public function add(IOperateAble $operateAbleObject) : bool
    {
        unset($operateAbleObject);
        return true;
    }

    public function edit(IOperateAble $operateAbleObject) : bool
    {
        unset($operateAbleObject);
        return true;
    }
}
