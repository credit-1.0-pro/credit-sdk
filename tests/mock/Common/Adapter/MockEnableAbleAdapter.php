<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IEnableAble;

class MockEnableAbleAdapter implements IEnableAbleAdapter
{
    public function enable(IEnableAble $enableAbleObject) : bool
    {
        unset($enableAbleObject);
        return true;
    }

    public function disable(IEnableAble $enableAbleObject) : bool
    {
        unset($enableAbleObject);
        return true;
    }
}
