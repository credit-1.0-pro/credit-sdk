<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\ITopAble;

class MockTopAbleRestfulAdapterObject
{
    use TopAbleRestfulAdapterTrait;

    protected function getResource() : string
    {
        return '';
    }

    public function topActionPublic(ITopAble $topAbleObject) : bool
    {
        return $this->topAction($topAbleObject);
    }

    public function cancelTopActionPublic(ITopAble $topAbleObject) : bool
    {
        return $this->cancelTopAction($topAbleObject);
    }
}
