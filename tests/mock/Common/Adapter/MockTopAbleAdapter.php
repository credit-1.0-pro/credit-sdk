<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\ITopAble;

class MockTopAbleAdapter implements ITopAbleAdapter
{
    public function top(ITopAble $topAbleObject) : bool
    {
        unset($topAbleObject);
        return true;
    }

    public function cancelTop(ITopAble $topAbleObject) : bool
    {
        unset($topAbleObject);
        return true;
    }
}
