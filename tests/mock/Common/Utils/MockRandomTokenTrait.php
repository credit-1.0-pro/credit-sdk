<?php
namespace Sdk\Common\Utils;

class MockRandomTokenTrait
{
    use RandomTokenTrait;

    public function publicRandom(int $length = 10)
    {
        return $this->random($length);
    }

    public function publicRandomNumber(int $length = 6)
    {
        return $this->randomNumber($length);
    }
}
