<?php
namespace Sdk\Common\Utils;

class MockCaptcha extends Captcha
{
    public function getBuildPhrase($builder)
    {
        return parent::getBuildPhrase($builder);
    }

    public function getPhraseBuilder()
    {
        return parent::getPhraseBuilder();
    }

    public function getCaptchaBuilder($phraseBuilder)
    {
        return parent::getCaptchaBuilder($phraseBuilder);
    }

    public function phraseOutPut($builder)
    {
        return parent::phraseOutPut($builder);
    }
}
