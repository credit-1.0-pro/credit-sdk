<?php
namespace Sdk\Common\Model;

class MockNullEnableAbleObject
{
    use NullEnableAbleTrait;

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
