<?php
namespace Sdk\Common\Model;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Adapter\MockTopAbleAdapter;

class MockTopAbleObject implements ITopAble
{
    use TopAbleTrait;
    
    private $stick;

    public function getStick() : int
    {
        return $this->stick;
    }
    
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
    
    protected function getITopAbleAdapter() : ITopAbleAdapter
    {
        return new MockTopAbleAdapter();
    }
}
