<?php
namespace Sdk\Common\Model;

use Sdk\Common\Model\IResubmitAble;
use Sdk\Common\Adapter\IResubmitAbleAdapter;
use Sdk\Common\Adapter\MockResubmitAbleAdapter;

class MockResubmitAbleObject implements IResubmitAble
{
    use ResubmitAbleTrait;

    public function resubmitActionPublic(): bool
    {
        return $this->resubmitAction();
    }

    protected function getIResubmitAbleAdapter() : IResubmitAbleAdapter
    {
        return new MockResubmitAbleAdapter();
    }
}
