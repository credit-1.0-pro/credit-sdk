<?php
namespace Sdk\Common\Model;

use Sdk\Common\Model\IOperateAble;
use Sdk\Common\Adapter\IOperateAbleAdapter;
use Sdk\Common\Adapter\MockOperateAbleAdapter;

class MockOperateAbleObject implements IOperateAble
{
    use OperateAbleTrait;

    public function addActionPublic(): bool
    {
        return $this->addAction();
    }

    public function editActionPublic(): bool
    {
        return $this->editAction();
    }

    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return new MockOperateAbleAdapter();
    }
}
