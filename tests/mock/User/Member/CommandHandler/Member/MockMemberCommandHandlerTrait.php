<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ITranslator;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Repository\MemberRepository;

class MockMemberCommandHandlerTrait
{
    use MemberCommandHandlerTrait;

    public function publicGetRepository() : MemberRepository
    {
        return $this->getRepository();
    }

    public function publicFetchMember(int $id) : Member
    {
        return $this->fetchMember($id);
    }

    public function publicGetMember() : Member
    {
        return $this->getMember();
    }
}
