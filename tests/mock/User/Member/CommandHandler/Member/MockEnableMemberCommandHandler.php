<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Sdk\Common\Model\IEnableAble;

class MockEnableMemberCommandHandler extends EnableMemberCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
