<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Sdk\Common\Model\IEnableAble;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Repository\MemberRepository;

class MockDisableMemberCommandHandler extends DisableMemberCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
