<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Sdk\User\Member\Repository\MemberRepository;
use Sdk\User\Member\Repository\MemberSessionRepository;

class MockAuthMemberCommandHandler extends AuthMemberCommandHandler
{
    public function getMemberSessionRepository() : MemberSessionRepository
    {
        return parent::getMemberSessionRepository();
    }

    public function getMemberRepository() : MemberRepository
    {
        return parent::getMemberRepository();
    }
}
