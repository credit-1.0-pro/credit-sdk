<?php
namespace Sdk\User\Member\Model;

use Marmot\Framework\Classes\Cookie;

use Sdk\User\Member\Repository\MemberRepository;
use Sdk\User\Member\Repository\MemberSessionRepository;

class MockMember extends Member
{
    public function getRepository() : MemberRepository
    {
        return parent::getRepository();
    }

    public function getCookie() : Cookie
    {
        return parent::getCookie();
    }

    public function getMemberSessionRepository() : MemberSessionRepository
    {
        return parent::getMemberSessionRepository();
    }

    public function isCellphoneExist() : bool
    {
        return parent::isCellphoneExist();
    }

    public function isEmailExist() : bool
    {
        return parent::isEmailExist();
    }

    public function isUserNameExist() : bool
    {
        return parent::isUserNameExist();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }

    public function saveCookie() : bool
    {
        return parent::saveCookie();
    }

    public function saveSession() : bool
    {
        return parent::saveSession();
    }

    public function clearCookie() : bool
    {
        return parent::clearCookie();
    }

    public function clearSession() : bool
    {
        return parent::clearSession();
    }
}
