<?php
namespace Sdk\User\Member\Adapter\Member;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\User\Member\Model\Member;

class MockMemberRestfulAdapter extends MemberRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function addAction(Member $member) : bool
    {
        return parent::addAction($member);
    }

    public function editAction(Member $member) : bool
    {
        return parent::editAction($member);
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
