<?php
namespace Sdk\User\Member\Adapter\Member;

use Sdk\User\Member\Translator\MemberSessionTranslator;
use Sdk\User\Member\Adapter\Member\Query\MemberSessionDataCacheQuery;

class MockMemberSessionAdapter extends MemberSessionAdapter
{
    public function getSession() : MemberSessionDataCacheQuery
    {
        return parent::getSession();
    }

    public function getTranslator() : MemberSessionTranslator
    {
        return parent::getTranslator();
    }

    public function getTTL() : int
    {
        return parent::getTTL();
    }
}
