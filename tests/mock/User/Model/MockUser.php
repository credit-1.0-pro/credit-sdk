<?php
namespace Sdk\User\Model;

use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

use Sdk\User\Crew\Repository\CrewRepository;

class MockUser extends User
{
    public function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return parent::getIEnableAbleAdapter();
    }

    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function getRepository()
    {
        return new CrewRepository();
    }
}
