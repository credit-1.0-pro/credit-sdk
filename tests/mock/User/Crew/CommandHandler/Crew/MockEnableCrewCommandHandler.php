<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Sdk\Common\Model\IEnableAble;

class MockEnableCrewCommandHandler extends EnableCrewCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
