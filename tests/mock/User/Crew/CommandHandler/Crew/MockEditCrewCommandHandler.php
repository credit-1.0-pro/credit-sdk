<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Sdk\User\Crew\Repository\CrewRepository;
use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;
use Sdk\UserGroup\Department\Repository\DepartmentRepository;

class MockEditCrewCommandHandler extends EditCrewCommandHandler
{
    public function getRepository() : CrewRepository
    {
        return parent::getRepository();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getDepartmentRepository() : DepartmentRepository
    {
        return parent::getDepartmentRepository();
    }
}
