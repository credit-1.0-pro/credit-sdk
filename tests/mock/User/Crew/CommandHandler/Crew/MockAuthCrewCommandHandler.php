<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Sdk\User\Crew\Repository\CrewRepository;
use Sdk\User\Crew\Repository\CrewSessionRepository;

class MockAuthCrewCommandHandler extends AuthCrewCommandHandler
{
    public function getCrewSessionRepository() : CrewSessionRepository
    {
        return parent::getCrewSessionRepository();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }
}
