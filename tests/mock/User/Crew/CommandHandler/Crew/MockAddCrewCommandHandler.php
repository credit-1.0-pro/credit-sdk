<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;
use Sdk\UserGroup\Department\Repository\DepartmentRepository;

class MockAddCrewCommandHandler extends AddCrewCommandHandler
{
    public function getCrew() : Crew
    {
        return parent::getCrew();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getDepartmentRepository() : DepartmentRepository
    {
        return parent::getDepartmentRepository();
    }
}
