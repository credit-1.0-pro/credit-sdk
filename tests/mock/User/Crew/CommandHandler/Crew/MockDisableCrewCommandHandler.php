<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Sdk\Common\Model\IEnableAble;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Repository\CrewRepository;

class MockDisableCrewCommandHandler extends DisableCrewCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }

    public function getRepository() : CrewRepository
    {
        return parent::getRepository();
    }

    public function fetchCrew(int $id) : Crew
    {
        return parent::fetchCrew($id);
    }
}
