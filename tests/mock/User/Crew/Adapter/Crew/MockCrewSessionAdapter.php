<?php
namespace Sdk\User\Crew\Adapter\Crew;

use Sdk\User\Crew\Translator\CrewSessionTranslator;
use Sdk\User\Crew\Adapter\Crew\Query\CrewSessionDataCacheQuery;

class MockCrewSessionAdapter extends CrewSessionAdapter
{
    public function getSession() : CrewSessionDataCacheQuery
    {
        return parent::getSession();
    }

    public function getTranslator() : CrewSessionTranslator
    {
        return parent::getTranslator();
    }

    public function getTTL() : int
    {
        return parent::getTTL();
    }
}
