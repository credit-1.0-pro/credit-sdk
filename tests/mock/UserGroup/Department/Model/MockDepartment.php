<?php
namespace Sdk\UserGroup\Department\Model;

use Sdk\Common\Adapter\IOperateAbleAdapter;

class MockDepartment extends Department
{
    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }
    
    public function nameIsExist() : bool
    {
        return parent::nameIsExist();
    }
}
