<?php
namespace Sdk\UserGroup\Department\Adapter;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\UserGroup\Department\Model\Department;

class MockDepartmentRestfulAdapter extends DepartmentRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function addAction(Department $department) : bool
    {
        return parent::addAction($department);
    }

    public function editAction(Department $department) : bool
    {
        return parent::editAction($department);
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
