<?php
namespace Sdk\UserGroup\Department\CommandHandler;

use Sdk\UserGroup\Department\Model\Department;
use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;

class MockAddDepartmentCommandHandler extends AddDepartmentCommandHandler
{
    public function getDepartment() : Department
    {
        return parent::getDepartment();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }
}
