<?php
namespace Sdk\UserGroup\Department\CommandHandler;

use Sdk\UserGroup\Department\Model\Department;
use Sdk\UserGroup\Department\Repository\DepartmentRepository;

class MockEditDepartmentCommandHandler extends EditDepartmentCommandHandler
{
    public function getRepository() : DepartmentRepository
    {
        return parent::getRepository();
    }

    public function fetchDepartment(int $id) : Department
    {
        return parent::fetchDepartment($id);
    }
}
