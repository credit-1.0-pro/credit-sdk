<?php
namespace Sdk\UserGroup\UserGroup\CommandHandler;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;

class MockEditUserGroupCommandHandler extends EditUserGroupCommandHandler
{
    public function getRepository() : UserGroupRepository
    {
        return parent::getRepository();
    }

    public function fetchUserGroup(int $id) : UserGroup
    {
        return parent::fetchUserGroup($id);
    }
}
