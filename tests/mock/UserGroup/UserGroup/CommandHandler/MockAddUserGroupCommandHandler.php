<?php
namespace Sdk\UserGroup\UserGroup\CommandHandler;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

class MockAddUserGroupCommandHandler extends AddUserGroupCommandHandler
{
    public function getUserGroup() : UserGroup
    {
        return parent::getUserGroup();
    }
}
