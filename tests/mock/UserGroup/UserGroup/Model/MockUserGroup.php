<?php
namespace Sdk\UserGroup\UserGroup\Model;

use Sdk\Common\Adapter\IOperateAbleAdapter;

class MockUserGroup extends UserGroup
{
    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function nameIsExist() : bool
    {
        return parent::nameIsExist();
    }
    
    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }
}
