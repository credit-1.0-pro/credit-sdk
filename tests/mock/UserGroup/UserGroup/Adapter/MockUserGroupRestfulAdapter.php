<?php
namespace Sdk\UserGroup\UserGroup\Adapter;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

class MockUserGroupRestfulAdapter extends UserGroupRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function addAction(UserGroup $userGroup) : bool
    {
        return parent::addAction($userGroup);
    }

    public function editAction(UserGroup $userGroup) : bool
    {
        return parent::editAction($userGroup);
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
