<?php
namespace Sdk\News\NewsCategory\CommandHandler\NewsCategory;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Repository\NewsCategoryRepository;

class MockEditNewsCategoryCommandHandler extends EditNewsCategoryCommandHandler
{
    public function getRepository() : NewsCategoryRepository
    {
        return parent::getRepository();
    }

    public function fetchNewsCategory(int $id) : NewsCategory
    {
        return parent::fetchNewsCategory($id);
    }
}
