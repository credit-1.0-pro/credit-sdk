<?php
namespace Sdk\News\NewsCategory\CommandHandler\NewsCategory;

use Sdk\News\NewsCategory\Model\NewsCategory;

class MockAddNewsCategoryCommandHandler extends AddNewsCategoryCommandHandler
{
    public function getNewsCategory() : NewsCategory
    {
        return parent::getNewsCategory();
    }
}
