<?php
namespace Sdk\News\NewsCategory\Adapter\NewsCategory;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\News\NewsCategory\Model\NewsCategory;

class MockNewsCategoryRestfulAdapter extends NewsCategoryRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function addAction(NewsCategory $newsCategory) : bool
    {
        return parent::addAction($newsCategory);
    }

    public function editAction(NewsCategory $newsCategory) : bool
    {
        return parent::editAction($newsCategory);
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
