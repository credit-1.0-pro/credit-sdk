<?php
namespace Sdk\News\NewsCategory\Model;

use Sdk\Common\Adapter\IOperateAbleAdapter;

class MockNewsCategory extends NewsCategory
{
    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function fetchOne($id) : NewsCategory
    {
        return parent::fetchOne($id);
    }

    public function fetchParentCategory() : NewsCategory
    {
        return parent::fetchParentCategory();
    }

    public function fetchCategory() : NewsCategory
    {
        return parent::fetchCategory();
    }

    public function validate() : bool
    {
        return parent::validate();
    }

    public function isParentCategoryExist(NewsCategory $parentCategory) : bool
    {
        return parent::isParentCategoryExist($parentCategory);
    }

    public function isCategoryExist(NewsCategory $category) : bool
    {
        return parent::isCategoryExist($category);
    }

    public function isBelongToParentCategory(NewsCategory $parentCategory, NewsCategory $category) : bool
    {
        return parent::isBelongToParentCategory($parentCategory, $category);
    }

    public function nameIsExist() : bool
    {
        return parent::nameIsExist();
    }
    
    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }
}
