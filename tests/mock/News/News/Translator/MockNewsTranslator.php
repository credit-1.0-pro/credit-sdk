<?php
namespace Sdk\News\News\Translator;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;
use Sdk\News\NewsCategory\Translator\NewsCategoryTranslator;

class MockNewsTranslator extends NewsTranslator
{
    public function getCrewTranslator() : CrewTranslator
    {
        return parent::getCrewTranslator();
    }

    public function getUserGroupTranslator() : UserGroupTranslator
    {
        return parent::getUserGroupTranslator();
    }

    public function getNewsCategoryTranslator() : NewsCategoryTranslator
    {
        return parent::getNewsCategoryTranslator();
    }
}
