<?php
namespace Sdk\News\News\Translator;

use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\News\NewsCategory\Translator\NewsCategoryRestfulTranslator;

class MockNewsRestfulTranslator extends NewsRestfulTranslator
{
    public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return parent::getUserGroupRestfulTranslator();
    }

    public function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return parent::getCrewRestfulTranslator();
    }

    public function getNewsCategoryRestfulTranslator() : NewsCategoryRestfulTranslator
    {
        return parent::getNewsCategoryRestfulTranslator();
    }
}
