<?php
namespace Sdk\News\News\CommandHandler\News;

use Sdk\Common\Model\IEnableAble;

class MockDisableNewsCommandHandler extends DisableNewsCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
