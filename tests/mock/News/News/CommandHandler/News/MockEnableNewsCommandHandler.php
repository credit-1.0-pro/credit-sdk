<?php
namespace Sdk\News\News\CommandHandler\News;

use Sdk\Common\Model\IEnableAble;

class MockEnableNewsCommandHandler extends EnableNewsCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
