<?php
namespace Sdk\News\News\CommandHandler\News;

use Marmot\Interfaces\ICommand;

use Sdk\News\News\Model\News;
use Sdk\News\News\Repository\NewsRepository;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Repository\NewsCategoryRepository;

class MockNewsCommandHandlerTrait
{
    use NewsCommandHandlerTrait;
    
    public function publicGetNews() : News
    {
        return $this->getNews();
    }

    public function publicGetRepository() : NewsRepository
    {
        return $this->getRepository();
    }

    public function publicFetchNews(int $id) : News
    {
        return $this->fetchNews($id);
    }

    public function publicGetNewsCategoryRepository() : NewsCategoryRepository
    {
        return $this->getNewsCategoryRepository();
    }

    public function publicFetchNewsCategory(int $id) : NewsCategory
    {
        return $this->fetchNewsCategory($id);
    }

    public function publicNewsExecuteAction(ICommand $command, News $news) : News
    {
        return $this->newsExecuteAction($command, $news);
    }
}
