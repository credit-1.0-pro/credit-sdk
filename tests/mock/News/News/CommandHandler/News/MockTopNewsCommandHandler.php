<?php
namespace Sdk\News\News\CommandHandler\News;

use Sdk\Common\Model\ITopAble;

class MockTopNewsCommandHandler extends TopNewsCommandHandler
{
    public function fetchITopObject($id) : ITopAble
    {
        return parent::fetchITopObject($id);
    }
}
