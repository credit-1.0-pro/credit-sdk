<?php
namespace Sdk\News\News\CommandHandler\News;

use Sdk\Common\Model\ITopAble;

class MockCancelTopNewsCommandHandler extends CancelTopNewsCommandHandler
{
    public function fetchITopObject($id) : ITopAble
    {
        return parent::fetchITopObject($id);
    }
}
