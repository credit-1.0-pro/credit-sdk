<?php
namespace Sdk\News\News\CommandHandler\UnAuditNews;

use Sdk\Common\Model\IApplyAble;

class MockApproveUnAuditNewsCommandHandler extends ApproveUnAuditNewsCommandHandler
{
    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
