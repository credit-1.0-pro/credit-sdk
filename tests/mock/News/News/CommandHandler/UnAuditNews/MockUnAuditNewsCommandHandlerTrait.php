<?php
namespace Sdk\News\News\CommandHandler\UnAuditNews;

use Sdk\News\News\Model\UnAuditNews;
use Sdk\News\News\Repository\UnAuditNewsRepository;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Repository\NewsCategoryRepository;

class MockUnAuditNewsCommandHandlerTrait
{
    use UnAuditNewsCommandHandlerTrait;

    public function publicGetRepository() : UnAuditNewsRepository
    {
        return $this->getRepository();
    }

    public function publicFetchUnAuditNews(int $id) : UnAuditNews
    {
        return $this->fetchUnAuditNews($id);
    }

    public function publicGetNewsCategoryRepository() : NewsCategoryRepository
    {
        return $this->getNewsCategoryRepository();
    }

    public function publicFetchNewsCategory(int $id) : NewsCategory
    {
        return $this->fetchNewsCategory($id);
    }
}
