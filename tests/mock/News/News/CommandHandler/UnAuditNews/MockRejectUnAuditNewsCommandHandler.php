<?php
namespace Sdk\News\News\CommandHandler\UnAuditNews;

use Sdk\Common\Model\IApplyAble;

class MockRejectUnAuditNewsCommandHandler extends RejectUnAuditNewsCommandHandler
{
    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
