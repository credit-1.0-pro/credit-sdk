<?php
namespace Sdk\News\News\Adapter\UnAuditNews;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Model\IApplyAble;

use Sdk\News\News\Model\UnAuditNews;

class MockUnAuditNewsRestfulAdapter extends UnAuditNewsRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function addAction(UnAuditNews $unAuditNews) : bool
    {
        return parent::addAction($unAuditNews);
    }

    public function editAction(UnAuditNews $unAuditNews) : bool
    {
        return parent::editAction($unAuditNews);
    }

    public function approveAction(IApplyAble $applyAbleObject) : bool
    {
        return parent::approveAction($applyAbleObject);
    }

    public function rejectAction(IApplyAble $applyAbleObject) : bool
    {
        return parent::rejectAction($applyAbleObject);
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
