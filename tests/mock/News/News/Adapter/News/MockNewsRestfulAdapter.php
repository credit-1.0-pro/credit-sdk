<?php
namespace Sdk\News\News\Adapter\News;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\Model\IEnableAble;

use Sdk\News\News\Model\News;

class MockNewsRestfulAdapter extends NewsRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function addAction(News $news) : bool
    {
        return parent::addAction($news);
    }

    public function editAction(News $news) : bool
    {
        return parent::editAction($news);
    }

    public function enableAction(IEnableAble $enableAbleObject) : bool
    {
        return parent::enableAction($enableAbleObject);
    }

    public function disableAction(IEnableAble $enableAbleObject) : bool
    {
        return parent::disableAction($enableAbleObject);
    }

    public function topAction(ITopAble $topAbleObject) : bool
    {
        return parent::topAction($topAbleObject);
    }

    public function cancelTopAction(ITopAble $topAbleObject) : bool
    {
        return parent::cancelTopAction($topAbleObject);
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
