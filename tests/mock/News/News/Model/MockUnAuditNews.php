<?php
namespace Sdk\News\News\Model;

use Sdk\Common\Adapter\IOperateAbleAdapter;
use Sdk\Common\Adapter\IApplyAbleAdapter;

use Sdk\News\News\Repository\UnAuditNewsRepository;

class MockUnAuditNews extends UnAuditNews
{
    public function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return parent::getIApplyAbleAdapter();
    }

    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function getUnAuditNewsRepository() : UnAuditNewsRepository
    {
        return parent::getUnAuditNewsRepository();
    }
}
