<?php
namespace Sdk\News\News\Model;

use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

class MockNews extends News
{
    public function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return parent::getIEnableAbleAdapter();
    }

    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function getITopAbleAdapter() : ITopAbleAdapter
    {
        return parent::getITopAbleAdapter();
    }
}
