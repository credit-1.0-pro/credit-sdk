<?php
namespace Sdk\ResourceCatalog\SearchData\Command\SearchData;

use PHPUnit\Framework\TestCase;

class RejectSearchDataCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->stub = new RejectSearchDataCommand(
            '驳回原因',
            $faker->randomNumber()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsOperationCommand()
    {
        $this->assertInstanceof('Sdk\Common\Command\RejectCommand', $this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
