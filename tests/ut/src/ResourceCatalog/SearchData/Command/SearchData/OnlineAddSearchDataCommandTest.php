<?php
namespace Sdk\ResourceCatalog\SearchData\Command\SearchData;

use PHPUnit\Framework\TestCase;

class OnlineAddSearchDataCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'itemsData' => array($faker->md5()),
            'expirationDate' => $faker->randomDigit(1),
            'template' => $faker->randomDigit(1),
            'id' => $faker->randomDigit()
        );

        $this->command = new OnlineAddSearchDataCommand(
            $this->fakerData['itemsData'],
            $this->fakerData['expirationDate'],
            $this->fakerData['template'],
            $this->fakerData['id']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testItemsDataParameter()
    {
        $this->assertEquals($this->fakerData['itemsData'], $this->command->itemsData);
    }

    public function testTemplateParameter()
    {
        $this->assertEquals($this->fakerData['template'], $this->command->template);
    }

    public function testExpirationDateParameter()
    {
        $this->assertEquals($this->fakerData['expirationDate'], $this->command->expirationDate);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}
