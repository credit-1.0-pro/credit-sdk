<?php
namespace Sdk\ResourceCatalog\SearchData\Command\SearchData;

use PHPUnit\Framework\TestCase;

class DeletedSearchDataCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'id' => $faker->randomDigit()
        );

        $this->command = new DeletedSearchDataCommand(
            $this->fakerData['id']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}
