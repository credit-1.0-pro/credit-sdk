<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\Command\SearchData\DeletedSearchDataCommand;

class DeletedSearchDataCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(DeletedSearchDataCommandHandler::class)
                                ->setMethods(['fetchSearchData'])
                                ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $command = new DeletedSearchDataCommand(1);

        $searchData = $this->prophesize(SearchData::class);
        $searchData->deleted()->shouldBeCalledTimes(1)->willReturn(true);
        $this->commandHandler->expects($this->once())->method('fetchSearchData')->willReturn($searchData->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
