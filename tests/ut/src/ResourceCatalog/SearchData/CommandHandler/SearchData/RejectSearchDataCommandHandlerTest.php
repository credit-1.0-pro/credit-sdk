<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use PHPUnit\Framework\TestCase;

class RejectSearchDataCommandHandlerTest extends TestCase
{
    private $rejectStub;

    public function setUp()
    {
        $this->rejectStub = $this->getMockBuilder(MockRejectSearchDataCommandHandler::class)
                        ->setMethods(['fetchSearchData'])
                        ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\RejectCommandHandler',
            $this->rejectStub
        );
    }

    public function testFetchIApplyObject()
    {
        $id = 1;
        $searchData = \Sdk\ResourceCatalog\SearchData\Utils\MockFactory::generateSearchData($id);

        $this->rejectStub->expects($this->exactly(1))->method('fetchSearchData')->willReturn($searchData);

        $result = $this->rejectStub->fetchIApplyObject($id);

        $this->assertEquals($result, $searchData);
    }
}
