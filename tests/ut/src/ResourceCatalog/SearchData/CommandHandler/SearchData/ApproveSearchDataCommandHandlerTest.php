<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use PHPUnit\Framework\TestCase;

class ApproveSearchDataCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveSearchDataCommandHandler::class)
            ->setMethods(['fetchSearchData'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\ApproveCommandHandler',
            $this->stub
        );
    }

    public function testFetchIApplyObject()
    {
        $id = 1;
        $searchData = \Sdk\ResourceCatalog\SearchData\Utils\MockFactory::generateSearchData($id);

        $this->stub->expects($this->exactly(1))->method('fetchSearchData')->willReturn($searchData);

        $result = $this->stub->fetchIApplyObject($id);

        $this->assertEquals($result, $searchData);
    }
}
