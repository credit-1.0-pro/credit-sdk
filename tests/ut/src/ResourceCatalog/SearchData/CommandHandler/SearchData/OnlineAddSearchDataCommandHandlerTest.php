<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\ResourceCatalogData\Model\ItemsData;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\Command\SearchData\OnlineAddSearchDataCommand;

class OnlineAddSearchDataCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(OnlineAddSearchDataCommandHandler::class)
                                ->setMethods(['fetchTemplate', 'getSearchData'])
                                ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $templateId = 1;
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate($templateId);
        $expirationDate = 1651234567;
        $itemsData = array('itemsData');
        
        $command = new OnlineAddSearchDataCommand(
            $itemsData,
            $expirationDate,
            $templateId
        );

        $this->commandHandler->expects($this->once())->method(
            'fetchTemplate'
        )->with($templateId)->willReturn($template);
        $itemsDataObject = $this->prophesize(ItemsData::class);
        $itemsDataObject->setData($itemsData)->shouldBeCalledTimes(1);

        $searchData = $this->prophesize(SearchData::class);
        $searchData->setTemplate($template)->shouldBeCalledTimes(1);
        $searchData->setExpirationDate($expirationDate)->shouldBeCalledTimes(1);
        $searchData->getItemsData()->shouldBeCalledTimes(1)->willReturn($itemsDataObject->reveal());
        $searchData->onlineAdd()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())->method('getSearchData')->willReturn($searchData->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
