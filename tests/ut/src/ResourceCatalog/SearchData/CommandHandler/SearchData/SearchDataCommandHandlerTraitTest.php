<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;
use Sdk\ResourceCatalog\SearchData\Repository\SearchDataRepository;

class SearchDataCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockSearchDataCommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetSearchData()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\Model\SearchData',
            $this->trait->publicGetSearchData()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\Repository\SearchDataRepository',
            $this->trait->publicGetRepository()
        );
    }

    public function testGetTemplateRepository()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Repository\TemplateRepository',
            $this->trait->publicGetTemplateRepository()
        );
    }

    public function testFetchSearch()
    {
        $trait = $this->getMockBuilder(MockSearchDataCommandHandlerTrait::class)
                    ->setMethods(['getRepository'])
                    ->getMock();

        $id = 1;
        $search = \Sdk\ResourceCatalog\SearchData\Utils\MockFactory::generateSearchData($id);

        $repository = $this->prophesize(SearchDataRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($search);
        $trait->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchSearchData($id);

        $this->assertEquals($result, $search);
    }

    public function testFetchTemplate()
    {
        $trait = $this->getMockBuilder(MockSearchDataCommandHandlerTrait::class)
                    ->setMethods(['getTemplateRepository'])
                    ->getMock();

        $id = 1;
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate($id);

        $repository = $this->prophesize(TemplateRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($template);
        $trait->expects($this->exactly(1))->method('getTemplateRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchTemplate($id);

        $this->assertEquals($result, $template);
    }
}
