<?php
namespace Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\SearchData\Command\SearchData\RejectSearchDataCommand;
use Sdk\ResourceCatalog\SearchData\Command\SearchData\ApproveSearchDataCommand;
use Sdk\ResourceCatalog\SearchData\Command\SearchData\DeletedSearchDataCommand;
use Sdk\ResourceCatalog\SearchData\Command\SearchData\OnlineAddSearchDataCommand;

class SearchDataCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new SearchDataCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testDeletedSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DeletedSearchDataCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData\DeletedSearchDataCommandHandler',
            $commandHandler
        );
    }

    public function testOnlineAddSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new OnlineAddSearchDataCommand(
                array($this->faker->title()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData\OnlineAddSearchDataCommandHandler',
            $commandHandler
        );
    }

    public function testApproveSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ApproveSearchDataCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData\ApproveSearchDataCommandHandler',
            $commandHandler
        );
    }

    public function testRejectSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RejectSearchDataCommand(
                $this->faker->title(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\CommandHandler\SearchData\RejectSearchDataCommandHandler',
            $commandHandler
        );
    }
}
