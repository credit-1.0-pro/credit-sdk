<?php
namespace Sdk\ResourceCatalog\SearchData\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\SearchData\Utils\SearchDataUtils;

class SearchDataTranslatorTest extends TestCase
{
    use SearchDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new SearchDataTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\ResourceCatalog\SearchData\Model\NullSearchData', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $searchData = \Sdk\ResourceCatalog\SearchData\Utils\MockFactory::generateSearchData(1);

        $expression = $this->translator->objectToArray($searchData);
    
        $this->compareArrayAndObjectSearchData($expression, $searchData);
    }

    public function testObjectToArrayFail()
    {
        $searchData = null;

        $expression = $this->translator->objectToArray($searchData);
        $this->assertEquals(array(), $expression);
    }
}
