<?php
namespace Sdk\ResourceCatalog\SearchData\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\SearchData\Utils\SearchDataRestfulUtils;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;

use Sdk\ResourceCatalog\ResourceCatalogData\Model\ItemsData;
use Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataRestfulTranslator;

class SearchDataRestfulTranslatorTest extends TestCase
{
    use SearchDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new SearchDataRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $translator = new MockSearchDataRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator',
            $translator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetTemplateVersionRestfulTranslator()
    {
        $translator = new MockSearchDataRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator',
            $translator->getTemplateVersionRestfulTranslator()
        );
    }

    public function testGetItemsDataRestfulTranslator()
    {
        $translator = new MockSearchDataRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataRestfulTranslator',
            $translator->getItemsDataRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $searchData = \Sdk\ResourceCatalog\SearchData\Utils\MockFactory::generateSearchData(1);

        $expression['data']['id'] = $searchData->getId();
        $expression['data']['attributes']['name'] = $searchData->getName();
        $expression['data']['attributes']['identify'] = $searchData->getIdentify();
        $expression['data']['attributes']['expirationDate'] = $searchData->getExpirationDate();
        $expression['data']['attributes']['subjectCategory'] = $searchData->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $searchData->getDimension();
        $expression['data']['attributes']['exchangeFrequency'] = $searchData->getExchangeFrequency();
        $expression['data']['attributes']['infoClassify'] = $searchData->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $searchData->getInfoCategory();
        $expression['data']['attributes']['applyStatus'] = $searchData->getApplyStatus();
        $expression['data']['attributes']['rejectReason'] = $searchData->getRejectReason();
        $expression['data']['attributes']['createTime'] = $searchData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $searchData->getUpdateTime();
        $expression['data']['attributes']['status'] = $searchData->getStatus();
        $expression['data']['attributes']['statusTime'] = $searchData->getStatusTime();

        $searchDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\SearchData\Model\SearchData', $searchDataObject);
        $this->compareArrayAndObjectCommon($expression, $searchDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $searchData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\SearchData\Model\NullSearchData', $searchData);
    }

    public function testObjectToArray()
    {
        $searchData = \Sdk\ResourceCatalog\SearchData\Utils\MockFactory::generateSearchData(1);

        $expression = $this->translator->objectToArray($searchData);

        $this->compareArrayAndObjectCommon($expression, $searchData);
    }

    public function testObjectToArrayFail()
    {
        $searchData = null;

        $expression = $this->translator->objectToArray($searchData);
        $this->assertEquals(array(), $expression);
    }

    public function testArrayToObjectWithIncluded()
    {
        $this->translator = $this->getMockBuilder(MockSearchDataRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getUserGroupRestfulTranslator',
                        'getTemplateVersionRestfulTranslator',
                        'getItemsDataRestfulTranslator'
                    ])
                    ->getMock();
        //初始化
        $expression = [
            'data'=>['relationships'=>'mock','id' => 1],
            'included'=>'mock'
        ];
        $relationships = [
            'publishUserGroup'=>['data'=>'mockPublishUserGroup'],
            'templateVersion'=>['data'=>'mockTemplateVersion'],
            'itemsData'=>['data'=>'mockItemsData']
        ];

        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];
        $templateVersionInfo = ['mockTemplateVersionInfo'];
        $itemsDataInfo = ['mockItemsDataInfo'];

        $publishUserGroup = new UserGroup(1);
        $templateVersion = new TemplateVersion(1);
        $itemsData = new ItemsData(1);

        //预言
        $this->translator->expects($this->once())->method('relationship')->with(
            $expression['included'],
            $expression['data']['relationships']
        )->willReturn($relationships);

        $this->translator->expects($this->exactly(3))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['publishUserGroup']['data']],
                 [$relationships['templateVersion']['data']],
                 [$relationships['itemsData']['data']]
             ) ->will($this->onConsecutiveCalls(
                 $publishUserGroupInfo,
                 $templateVersionInfo,
                 $itemsDataInfo
             ));

        $this->arrayToObjectWithIncludedPublishUserGroup($publishUserGroupInfo, $publishUserGroup);
        $this->arrayToObjectWithIncludedTemplateVersion($templateVersionInfo, $templateVersion);
        $this->arrayToObjectWithIncludedItemsData($itemsDataInfo, $itemsData);

        //揭示
        $searchData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\SearchData\Model\SearchData', $searchData);
        $this->assertEquals($publishUserGroup, $searchData->getPublishUserGroup());
        $this->assertEquals($templateVersion, $searchData->getTemplateVersion());
        $this->assertEquals($itemsData, $searchData->getItemsData());
    }

    private function arrayToObjectWithIncludedPublishUserGroup($publishUserGroupInfo, $publishUserGroup)
    {
        $translator = $this->prophesize(UserGroupRestfulTranslator::class);
        $translator->arrayToObject(
            Argument::exact($publishUserGroupInfo)
        )->shouldBeCalledTimes(1)->willReturn($publishUserGroup);

        $this->translator->expects($this->exactly(1))->method(
            'getUserGroupRestfulTranslator'
        )->willReturn($translator->reveal());
    }

    private function arrayToObjectWithIncludedTemplateVersion($templateVersionInfo, $templateVersion)
    {
        $translator = $this->prophesize(TemplateVersionRestfulTranslator::class);
        $translator->arrayToObject(
            Argument::exact($templateVersionInfo)
        )->shouldBeCalledTimes(1)->willReturn($templateVersion);

        $this->translator->expects($this->exactly(1))->method(
            'getTemplateVersionRestfulTranslator'
        )->willReturn($translator->reveal());
    }

    private function arrayToObjectWithIncludedItemsData($itemsDataInfo, $itemsData)
    {
        $translator = $this->prophesize(ItemsDataRestfulTranslator::class);
        $translator->arrayToObject(Argument::exact($itemsDataInfo))->shouldBeCalledTimes(1)->willReturn($itemsData);
        //绑定
        $this->translator->expects($this->exactly(1))->method(
            'getItemsDataRestfulTranslator'
        )->willReturn($translator->reveal());
    }
}
