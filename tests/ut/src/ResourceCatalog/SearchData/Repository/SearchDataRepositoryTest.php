<?php
namespace Sdk\ResourceCatalog\SearchData\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\SearchData\Adapter\SearchData\SearchDataRestfulAdapter;

class SearchDataRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(SearchDataRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsISearchDataAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\Adapter\SearchData\ISearchDataAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\Adapter\SearchData\SearchDataRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\Adapter\SearchData\SearchDataMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(SearchDataRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testOnlineAdd()
    {
        $searchData = \Sdk\ResourceCatalog\SearchData\Utils\MockFactory::generateSearchData(1);

        $adapter = $this->prophesize(SearchDataRestfulAdapter::class);
        $adapter->onlineAdd(Argument::exact($searchData))->shouldBeCalledTimes(1);
        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->onlineAdd($searchData);
    }

    public function testDeleted()
    {
        $searchData = \Sdk\ResourceCatalog\SearchData\Utils\MockFactory::generateSearchData(1);

        $adapter = $this->prophesize(SearchDataRestfulAdapter::class);
        $adapter->deleted(Argument::exact($searchData))->shouldBeCalledTimes(1);
        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->deleted($searchData);
    }
}
