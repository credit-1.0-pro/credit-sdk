<?php
namespace Sdk\ResourceCatalog\SearchData\Adapter\SearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\Model\NullSearchData;
use Sdk\ResourceCatalog\SearchData\Translator\SearchDataRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class SearchDataRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockSearchDataRestfulAdapter::class)
                           ->setMethods([
                               'commonMapErrors',
                               'getTranslator',
                               'isSuccess',
                               'post',
                               'patch',
                               'translateToObject'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new SearchDataRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsISearchDataAdapter()
    {
        $adapter = new SearchDataRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\Adapter\SearchData\ISearchDataAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockSearchDataRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\Translator\SearchDataRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockSearchDataRestfulAdapter();
        $this->assertEquals('resourceCatalogs/searchData', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockSearchDataRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\Model\NullSearchData',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockSearchDataRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'SEARCH_DATA_LIST',
                SearchDataRestfulAdapter::SCENARIOS['SEARCH_DATA_LIST']
            ],
            [
                'SEARCH_DATA_FETCH_ONE',
                SearchDataRestfulAdapter::SCENARIOS['SEARCH_DATA_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    /**
     * 为SearchDataRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$searchData,$keys,$searchDataArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareSearchDataTranslator(
        SearchData $searchData,
        array $keys,
        array $searchDataArray
    ) {
        $translator = $this->prophesize(SearchDataRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($searchData),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($searchDataArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(SearchData $searchData)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($searchData);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR,
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
        ];

        $this->adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = SearchDataRestfulAdapter::MAP_ERROR;
        
        $result = $this->adapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    protected function initMapErrors(array $mapErrors)
    {
        $adapter = $this->getMockBuilder(MockSearchDataRestfulAdapter::class)
                           ->setMethods([
                               'lastErrorId',
                               'lastErrorPointer',
                               'getMapErrors'
                            ])
                           ->getMock();

        $adapter->expects($this->once())->method('lastErrorId')->willReturn(1101);
        $adapter->expects($this->once())->method('lastErrorPointer')->willReturn('itemsData');
        $adapter->expects($this->once())->method('getMapErrors')->willReturn($mapErrors);

        $adapter->publicMapErrors();
        
        $this->assertEquals(RESOURCE_CATALOG_DATA_ITEMS_DATA_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer'=>'itemsData'), Core::getLastError()->getSource());
    }

    public function testMapErrors()
    {
        $mapErrors = array(
            1101 => RESOURCE_CATALOG_DATA_ITEMS_DATA_FORMAT_ERROR
        );

        $this->initMapErrors($mapErrors);
    }

    public function testMapErrorsArray()
    {
        $mapErrors = array(
            1101 => array(
                'itemsData' => RESOURCE_CATALOG_DATA_ITEMS_DATA_FORMAT_ERROR
            )
        );

        $this->initMapErrors($mapErrors);
    }

    public function testOnlineAddSuccess()
    {
        $searchData = \Sdk\ResourceCatalog\SearchData\Utils\MockFactory::generateSearchData(1);
        $searchDataArray = array();

        $this->prepareSearchDataTranslator(
            $searchData,
            array('expirationDate', 'crew', 'itemsData', 'template'),
            $searchDataArray
        );

        $this->adapter->method('post')->with('resourceCatalogs/searchData', $searchDataArray);

        $this->success($searchData);
        $result = $this->adapter->onlineAdd($searchData);
        $this->assertTrue($result);
    }

    public function testOnlineAddFailure()
    {
        $searchData = \Sdk\ResourceCatalog\SearchData\Utils\MockFactory::generateSearchData(1);
        $searchDataArray = array();

        $this->prepareSearchDataTranslator($searchData, array(
            'expirationDate', 'crew', 'itemsData', 'template'
        ), $searchDataArray);

        $this->adapter->method('post')->with('resourceCatalogs/searchData', $searchDataArray);
        
        $this->failure($searchData);
        $result = $this->adapter->onlineAdd($searchData);
        $this->assertFalse($result);
    }

    public function testDeletedSuccess()
    {
        $id = 1;
        $searchData = \Sdk\ResourceCatalog\SearchData\Utils\MockFactory::generateSearchData($id);
    
        $this->adapter->method('patch')->with('resourceCatalogs/searchData/'.$id.'/delete');
        $this->success($searchData);
        $result = $this->adapter->deleted($searchData);
        $this->assertTrue($result);
    }

    public function testDeletedFailure()
    {
        $id = 1;
        $searchData = \Sdk\ResourceCatalog\SearchData\Utils\MockFactory::generateSearchData($id);
    
        $this->adapter->method('patch')->with('resourceCatalogs/searchData/'.$id.'/delete');

        $this->failure($searchData);
        $result = $this->adapter->deleted($searchData);
        $this->assertFalse($result);
    }
}
