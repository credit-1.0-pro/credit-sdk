<?php
namespace Sdk\ResourceCatalog\SearchData\Adapter\SearchData;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;

class SearchDataMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new SearchDataMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\Model\SearchData',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\SearchData\Model\SearchData',
                $each
            );
        }
    }

    public function testFetchOneAsync()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\Model\SearchData',
            $this->adapter->fetchOneAsync(1)
        );
    }

    public function testFetchListAsync()
    {
        $list = $this->adapter->fetchListAsync([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\SearchData\Model\SearchData',
                $each
            );
        }
    }

    public function testSearch()
    {
        list($list, $count) = $this->adapter->search(['filter'], ['sort'], 1, 2);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\SearchData\Model\SearchData',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }

    public function testSearchAsync()
    {
        list($list, $count) = $this->adapter->searchAsync(['filter'], ['sort'], 1, 2);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\SearchData\Model\SearchData',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }

    public function testOnlineAdd()
    {
        $this->assertTrue($this->adapter->onlineAdd(new SearchData()));
    }

    public function testDeleted()
    {
        $this->assertTrue($this->adapter->deleted(new SearchData()));
    }
}
