<?php
namespace Sdk\ResourceCatalog\SearchData\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullSearchDataTest extends TestCase
{
    private $nullSearchData;

    public function setUp()
    {
        $this->nullSearchData = NullSearchData::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullSearchData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsSearchData()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\Model\SearchData',
            $this->nullSearchData
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullSearchData
        );
    }

    public function testResourceNotExist()
    {
        $nullSearchData = new MockNullSearchData();

        $result = $nullSearchData->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    protected function initResourceNotExist($method)
    {
        $nullSearchData = $this->getMockBuilder(MockNullSearchData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();

        $nullSearchData->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $nullSearchData->$method();
        $this->assertFalse($result);
    }

    public function testDeleted()
    {
        $this->initResourceNotExist('deleted');
    }

    public function testApprove()
    {
        $this->initResourceNotExist('approve');
    }

    public function testReject()
    {
        $this->initResourceNotExist('reject');
    }

    public function testOnlineAdd()
    {
        $this->initResourceNotExist('onlineAdd');
    }
}
