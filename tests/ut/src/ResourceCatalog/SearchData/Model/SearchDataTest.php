<?php
namespace Sdk\ResourceCatalog\SearchData\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\SearchData\Repository\SearchDataRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class SearchDataTest extends TestCase
{
    private $searchData;

    public function setUp()
    {
        $this->searchData = new SearchData();
    }

    public function tearDown()
    {
        unset($this->searchData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }


    public function testExtendsResourceCatalogData()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ResourceCatalogData\Model\ResourceCatalogData',
            $this->searchData
        );
    }

    public function testSearchDataConstructor()
    {
        $this->assertEquals(SearchData::STATUS['NORMAL'], $this->searchData->getStatus());
        $this->assertEquals(SearchData::DATA_APPLY_STATUS['DEFAULT'], $this->searchData->getApplyStatus());
        $this->assertEmpty($this->searchData->getRejectReason());
    }
    //applyStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setApplyStatus() 是否符合预定范围
     *
     * @dataProvider applyStatusProvider
     */
    public function testSetApplyStatus($actual, $expected)
    {
        $this->searchData->setApplyStatus($actual);
        $this->assertEquals($expected, $this->searchData->getApplyStatus());
    }
    /**
     * 循环测试 DispatchDepartment setApplyStatus() 数据构建器
     */
    public function applyStatusProvider()
    {
        return array(
            array(SearchData::DATA_APPLY_STATUS['DEFAULT'],SearchData::DATA_APPLY_STATUS['DEFAULT']),
            array(SearchData::DATA_APPLY_STATUS['PENDING'],SearchData::DATA_APPLY_STATUS['PENDING']),
            array(SearchData::DATA_APPLY_STATUS['APPROVE'],SearchData::DATA_APPLY_STATUS['APPROVE']),
            array(SearchData::DATA_APPLY_STATUS['REJECT'],SearchData::DATA_APPLY_STATUS['REJECT']),
            array(999, SearchData::DATA_APPLY_STATUS['DEFAULT'])
        );
    }
    /**
     * 设置 setApplyStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyStatusWrongType()
    {
        $this->searchData->setApplyStatus('applyStatus');
    }
    //applyStatus 测试 ------------------------------------------------------   end
    
    //rejectReason 测试 -------------------------------------------------------- start
    /**
     * 设置 SearchData setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $this->searchData->setRejectReason('rejectReason');
        $this->assertEquals('rejectReason', $this->searchData->getRejectReason());
    }

    /**
     * 设置 SearchData setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonWrongType()
    {
        $this->searchData->setRejectReason(array('rejectReason'));
    }
    //rejectReason 测试 --------------------------------------------------------   end
    
    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->searchData->setStatus($actual);
        $this->assertEquals($expected, $this->searchData->getStatus());
    }
    /**
     * 循环测试 DispatchDepartment setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(SearchData::STATUS['NORMAL'],SearchData::STATUS['NORMAL']),
            array(SearchData::STATUS['DELETED'],SearchData::STATUS['DELETED']),
            array(999, SearchData::STATUS['NORMAL'])
        );
    }
    /**
     * 设置 setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->searchData->setStatus('status');
    }
    //status 测试 ------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\SearchData\Repository\SearchDataRepository',
            $this->searchData->getRepository()
        );
    }

    protected function initOperation($method)
    {
        $this->stub = $this->getMockBuilder(SearchData::class)->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(SearchDataRepository::class);
        $repository->$method(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->$method();
        $this->assertTrue($result);
    }

    public function testOnlineAdd()
    {
        $this->initOperation('onlineAdd');
    }

    public function testApprove()
    {
        $this->initOperation('approve');
    }

    public function testReject()
    {
        $this->initOperation('reject');
    }

    public function testDeleted()
    {
        $this->initOperation('deleted');
    }
}
