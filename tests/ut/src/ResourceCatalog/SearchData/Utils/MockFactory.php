<?php
namespace Sdk\ResourceCatalog\SearchData\Utils;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\SearchData\Model\SearchData;

class MockFactory
{
    public static function generateSearchData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : SearchData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $searchData = new SearchData($id);
        $searchData->setId($id);

        //name
        self::generateName($searchData, $faker, $value);
        //identify
        self::generateIdentify($searchData, $faker, $value);
        //expirationDate
        self::generateExpirationDate($searchData, $faker, $value);
        //subjectCategory
        self::generateSubjectCategory($searchData, $faker, $value);
        //dimension
        self::generateDimension($searchData, $faker, $value);
        //exchangeFrequency
        self::generateExchangeFrequency($searchData, $faker, $value);
        //infoClassify
        self::generateInfoClassify($searchData, $faker, $value);
        //infoCategory
        self::generateInfoCategory($searchData, $faker, $value);
        //publishUserGroup
        self::generatePublishUserGroup($searchData, $faker, $value);
        //templateVersion
        self::generateTemplateVersion($searchData, $faker, $value);
        //itemsData
        self::generateItemsData($searchData, $faker, $value);
        //applyStatus
        self::generateApplyStatus($searchData, $faker, $value);
        //rejectReason
        self::generateRejectReason($searchData, $faker, $value);
        //status
        self::generateStatus($searchData, $faker, $value);
        $searchData->setCreateTime($faker->unixTime());
        $searchData->setUpdateTime($faker->unixTime());
        $searchData->setStatusTime($faker->unixTime());

        return $searchData;
    }

    protected static function generateName($searchData, $faker, $value) : void
    {
        $name = isset($value['name']) ? $value['name'] : $faker->word();
        
        $searchData->setName($name);
    }

    protected static function generateIdentify($searchData, $faker, $value) : void
    {
        $identify = isset($value['identify']) ? $value['identify'] : $faker->regexify('[A-Z_]{1,100}');
        
        $searchData->setIdentify($identify);
    }

    protected static function generateExpirationDate($searchData, $faker, $value) : void
    {
        $expirationDate = isset($value['expirationDate']) ?
            $value['expirationDate'] :
            $faker->unixTime();
        
        $searchData->setExpirationDate($expirationDate);
    }

    protected static function generateSubjectCategory($searchData, $faker, $value) : void
    {
        $subjectCategory = isset($value['subjectCategory']) ?
            $value['subjectCategory'] :
            $faker->randomElement(Template::SUBJECT_CATEGORY);
        
        $searchData->setSubjectCategory($subjectCategory);
    }

    protected static function generateDimension($searchData, $faker, $value) : void
    {
        $dimension = isset($value['dimension']) ?
            $value['dimension'] :
            $faker->randomElement(
                Template::DIMENSION
            );
        
        $searchData->setDimension($dimension);
    }

    protected static function generateExchangeFrequency($searchData, $faker, $value) : void
    {
        $exchangeFrequency = isset($value['exchangeFrequency']) ?
            $value['exchangeFrequency'] :
            $faker->randomElement(
                Template::EXCHANGE_FREQUENCY
            );
        
        $searchData->setExchangeFrequency($exchangeFrequency);
    }

    protected static function generateInfoClassify($searchData, $faker, $value) : void
    {
        $infoClassify = isset($value['infoClassify']) ?
            $value['infoClassify'] :
            $faker->randomElement(
                Template::INFO_CLASSIFY
            );
        
        $searchData->setInfoClassify($infoClassify);
    }

    protected static function generateInfoCategory(SearchData $searchData, $faker, $value) : void
    {
        $infoCategory = isset($value['infoCategory']) ?
            $value['infoCategory'] :
            $faker->randomElement(
                Template::INFO_CATEGORY
            );
        
        $searchData->setInfoCategory($infoCategory);
    }

    protected static function generatePublishUserGroup($searchData, $faker, $value) : void
    {
        $userGroup = \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup($faker->randomDigit());
        $userGroup = isset($value['userGroup']) ? $value['userGroup'] : $userGroup;
        
        $searchData->setPublishUserGroup($userGroup);
    }

    protected static function generateTemplateVersion($searchData, $faker, $value) : void
    {
        $templateVersion = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplateVersion(
            $faker->randomDigit()
        );
        $templateVersion = isset($value['templateVersion']) ? $value['templateVersion'] : $templateVersion;
        
        $searchData->setTemplateVersion($templateVersion);
    }

    protected static function generateItemsData($searchData, $faker, $value) : void
    {
        $itemsData = isset($value['itemsData']) ?
                        $value['itemsData'] :
                        \Sdk\ResourceCatalog\ResourceCatalogData\Utils\MockFactory::generateItemsData(
                            $faker->randomDigit()
                        );
        
        $searchData->setItemsData($itemsData);
    }

    protected static function generateApplyStatus($searchData, $faker, $value) : void
    {
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : $faker->randomElement(
            SearchData::DATA_APPLY_STATUS
        );
        
        $searchData->setApplyStatus($applyStatus);
    }

    protected static function generateRejectReason($searchData, $faker, $value) : void
    {
        $rejectReason = isset($value['rejectReason']) ? $value['rejectReason'] : $faker->title();
        
        $searchData->setRejectReason($rejectReason);
    }

    protected static function generateStatus($searchData, $faker, $value) : void
    {
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(
            SearchData::STATUS
        );
        
        $searchData->setStatus($status);
    }
}
