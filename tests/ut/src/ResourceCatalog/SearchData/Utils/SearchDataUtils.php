<?php
namespace Sdk\ResourceCatalog\SearchData\Utils;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionTranslator;
use Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataTranslator;

trait SearchDataUtils
{
    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getItemsDataTranslator() : ItemsDataTranslator
    {
        return new ItemsDataTranslator();
    }

    protected function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return new TemplateVersionTranslator();
    }

    private function compareArrayAndObjectSearchData(
        array $expectedArray,
        $searchData
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($searchData->getId()));
        $this->assertEquals($expectedArray['name'], $searchData->getName());
        $this->assertEquals($expectedArray['identify'], $searchData->getIdentify());
        $this->assertEquals($expectedArray['rejectReason'], $searchData->getRejectReason());
        $this->assertEquals($expectedArray['expirationDate'], $searchData->getExpirationDate());
        $this->assertEquals(
            $expectedArray['expirationDateFormat'],
            date('Y年m月d日', $searchData->getExpirationDate())
        );
        $this->assertEquals($expectedArray['updateTime'], $searchData->getUpdateTime());
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $searchData->getUpdateTime())
        );
        $this->assertEquals($expectedArray['statusTime'], $searchData->getStatusTime());
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $searchData->getStatusTime())
        );
        $this->assertEquals($expectedArray['createTime'], $searchData->getCreateTime());
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $searchData->getCreateTime())
        );

        $this->assertEquals(
            $expectedArray['publishUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($searchData->getPublishUserGroup())
        );
        $this->assertEquals(
            $expectedArray['itemsData'],
            $this->getItemsDataTranslator()->objectToArray($searchData->getItemsData())
        );
        $this->assertEquals(
            $expectedArray['templateVersion'],
            $this->getTemplateVersionTranslator()->objectToArray($searchData->getTemplateVersion())
        );
        $this->subjectCategoryEquals($expectedArray, $searchData);
        $this->dimensionEquals($expectedArray, $searchData);
        $this->exchangeFrequencyEquals($expectedArray, $searchData);
        $this->infoClassifyEquals($expectedArray, $searchData);
        $this->infoCategoryEquals($expectedArray, $searchData);
        $this->statusEquals($expectedArray, $searchData);
        $this->applyStatusEquals($expectedArray, $searchData);
    }

    private function subjectCategoryEquals($expectedArray, $searchData)
    {
        $this->assertEquals($expectedArray['subjectCategory']['id'], marmot_encode($searchData->getSubjectCategory()));
        $this->assertEquals(
            $expectedArray['subjectCategory']['name'],
            Template::SUBJECT_CATEGORY_CN[$searchData->getSubjectCategory()]
        );
    }

    private function dimensionEquals($expectedArray, $searchData)
    {
        $this->assertEquals($expectedArray['dimension']['id'], marmot_encode($searchData->getDimension()));
        $this->assertEquals(
            $expectedArray['dimension']['name'],
            Template::DIMENSION_CN[$searchData->getDimension()]
        );
    }

    private function exchangeFrequencyEquals($expectedArray, $searchData)
    {
        $this->assertEquals(
            $expectedArray['exchangeFrequency']['id'],
            marmot_encode($searchData->getExchangeFrequency())
        );
        $this->assertEquals(
            $expectedArray['exchangeFrequency']['name'],
            Template::EXCHANGE_FREQUENCY_CN[$searchData->getExchangeFrequency()]
        );
    }

    private function infoClassifyEquals($expectedArray, $searchData)
    {
        $this->assertEquals($expectedArray['infoClassify']['id'], marmot_encode($searchData->getInfoClassify()));
        $this->assertEquals(
            $expectedArray['infoClassify']['name'],
            Template::INFO_CLASSIFY_CN[$searchData->getInfoClassify()]
        );
    }

    private function infoCategoryEquals($expectedArray, $searchData)
    {
        $this->assertEquals($expectedArray['infoCategory']['id'], marmot_encode($searchData->getInfoCategory()));
        $this->assertEquals(
            $expectedArray['infoCategory']['name'],
            Template::INFO_CATEGORY_CN[$searchData->getInfoCategory()]
        );
    }

    private function applyStatusEquals($expectedArray, $searchData)
    {
        $this->assertEquals($expectedArray['applyStatus']['id'], marmot_encode($searchData->getApplyStatus()));
        $this->assertEquals(
            $expectedArray['applyStatus']['name'],
            SearchData::DATA_APPLY_STATUS_CN[$searchData->getApplyStatus()]
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['type'],
            SearchData::DATA_APPLY_STATUS_TYPE[$searchData->getApplyStatus()]
        );
    }

    private function statusEquals($expectedArray, $searchData)
    {
        $this->assertEquals($expectedArray['status']['id'], marmot_encode($searchData->getStatus()));
        $this->assertEquals($expectedArray['status']['name'], SearchData::STATUS_CN[$searchData->getStatus()]);
        $this->assertEquals($expectedArray['status']['type'], SearchData::STATUS_TYPE[$searchData->getStatus()]);
    }
}
