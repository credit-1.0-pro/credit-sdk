<?php
namespace Sdk\ResourceCatalog\SearchData\Utils;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait SearchDataRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $searchData
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $searchData->getId());
        }

        $attributes = isset($expectedArray['data']['attributes']) ? $expectedArray['data']['attributes'] : array();
        
        if (isset($attributes['name'])) {
            $this->assertEquals($attributes['name'], $searchData->getName());
        }
        if (isset($attributes['identify'])) {
            $this->assertEquals($attributes['identify'], $searchData->getIdentify());
        }
        if (isset($attributes['expirationDate'])) {
            $this->assertEquals($attributes['expirationDate'], $searchData->getExpirationDate());
        }
        if (isset($attributes['subjectCategory'])) {
            $this->assertEquals($attributes['subjectCategory'], $searchData->getSubjectCategory());
        }
        if (isset($attributes['dimension'])) {
            $this->assertEquals($attributes['dimension'], $searchData->getDimension());
        }
        if (isset($attributes['exchangeFrequency'])) {
            $this->assertEquals($attributes['exchangeFrequency'], $searchData->getExchangeFrequency());
        }
        if (isset($attributes['infoClassify'])) {
            $this->assertEquals($attributes['infoClassify'], $searchData->getInfoClassify());
        }
        if (isset($attributes['infoCategory'])) {
            $this->assertEquals($attributes['infoCategory'], $searchData->getInfoCategory());
        }
        if (isset($attributes['applyStatus'])) {
            $this->assertEquals($attributes['applyStatus'], $searchData->getApplyStatus());
        }
        if (isset($attributes['rejectReason'])) {
            $this->assertEquals($attributes['rejectReason'], $searchData->getRejectReason());
        }
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $searchData->getCreateTime());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $searchData->getStatusTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $searchData->getUpdateTime());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $searchData->getStatus());
        }

        $this->comparePublishUserGroup($expectedArray, $searchData);
        $this->compareTemplateVersion($expectedArray, $searchData);
        $this->compareItemsData($expectedArray, $searchData);
        $this->compareCrew($expectedArray, $searchData);
    }

    private function compareCrew(array $expectedArray, $searchData)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['crew']['data'])) {
                $this->assertEquals(
                    $relationships['crew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['crew']['data'][0]['id'],
                    $searchData->getCrew()->getId()
                );
            }
        }
    }

    private function comparePublishUserGroup(array $expectedArray, $searchData)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['publishUserGroup']['data'])) {
                $this->assertEquals(
                    $relationships['publishUserGroup']['data'][0]['type'],
                    'userGroups'
                );
                $this->assertEquals(
                    $relationships['publishUserGroup']['data'][0]['id'],
                    $searchData->getPublishUserGroup()->getId()
                );
            }
        }
    }

    private function compareTemplateVersion(array $expectedArray, $searchData)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['templateVersion']['data'])) {
                $this->assertEquals(
                    $relationships['templateVersion']['data'][0]['type'],
                    'templateVersions'
                );
                $this->assertEquals(
                    $relationships['templateVersion']['data'][0]['id'],
                    $searchData->getTemplateVersion()->getId()
                );
            }
        }
    }

    private function compareItemsData(array $expectedArray, $searchData)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['itemsData']['data'][0])) {
                $itemsDataArray = $relationships['itemsData']['data'][0];
                $this->assertEquals($itemsDataArray['type'], 'itemsData');

                if (isset($itemsDataArray['id'])) {
                    $this->assertEquals($itemsDataArray['id'], $searchData->getItemsData()->getId());
                }
                if (isset($itemsDataArray['attributes']['data'])) {
                    $this->assertEquals($itemsDataArray['attributes']['data'], $searchData->getItemsData()->getData());
                }
            }
        }
    }
}
