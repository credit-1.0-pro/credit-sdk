<?php
namespace Sdk\ResourceCatalog\ErrorData\Utils;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\Task\Translator\TaskTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionTranslator;
use Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataTranslator;

trait ErrorDataUtils
{
    protected function getTaskTranslator() : TaskTranslator
    {
        return new TaskTranslator();
    }

    protected function getTemplateTranslator() : TemplateTranslator
    {
        return new TemplateTranslator();
    }

    protected function getItemsDataTranslator() : ItemsDataTranslator
    {
        return new ItemsDataTranslator();
    }

    protected function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return new TemplateVersionTranslator();
    }

    private function compareArrayAndObjectErrorData(
        array $expectedArray,
        $errorData
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($errorData->getId()));
        $this->assertEquals($expectedArray['name'], $errorData->getName());
        $this->assertEquals($expectedArray['identify'], $errorData->getIdentify());
        $this->assertEquals($expectedArray['errorType'], $errorData->getErrorType());
        $this->assertEquals($expectedArray['errorReason'], $errorData->getErrorReason());
        $this->assertEquals($expectedArray['errorStatus'], $errorData->getErrorStatus());
        $this->assertEquals($expectedArray['status']['id'], marmot_encode($errorData->getStatus()));
        $this->assertEquals($expectedArray['status']['name'], ErrorData::STATUS_CN[$errorData->getStatus()]);
        $this->assertEquals($expectedArray['status']['type'], ErrorData::STATUS_TYPE[$errorData->getStatus()]);
        $this->assertEquals($expectedArray['updateTime'], $errorData->getUpdateTime());
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $errorData->getUpdateTime())
        );
                    
        $this->assertEquals($expectedArray['statusTime'], $errorData->getStatusTime());
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $errorData->getStatusTime())
        );
                    
        $this->assertEquals($expectedArray['createTime'], $errorData->getCreateTime());
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $errorData->getCreateTime())
        );

        $this->assertEquals(
            $expectedArray['task'],
            $this->getTaskTranslator()->objectToArray($errorData->getTask())
        );
        $this->assertEquals(
            $expectedArray['itemsData'],
            $this->getItemsDataTranslator()->objectToArray($errorData->getItemsData())
        );
        $this->assertEquals(
            $expectedArray['templateVersion'],
            $this->getTemplateVersionTranslator()->objectToArray($errorData->getTemplateVersion())
        );
        $this->assertEquals(
            $expectedArray['template'],
            $this->getTemplateTranslator()->objectToArray($errorData->getTemplate())
        );
    }
}
