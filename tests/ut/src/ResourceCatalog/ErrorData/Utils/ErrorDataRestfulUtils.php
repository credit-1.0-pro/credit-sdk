<?php
namespace Sdk\ResourceCatalog\ErrorData\Utils;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait ErrorDataRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $errorData
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $errorData->getId());
        }
        
        if (isset($expectedArray['data']['attributes']['name'])) {
            $this->assertEquals($expectedArray['data']['attributes']['name'], $errorData->getName());
        }
        if (isset($expectedArray['data']['attributes']['identify'])) {
            $this->assertEquals($expectedArray['data']['attributes']['identify'], $errorData->getIdentify());
        }
        if (isset($expectedArray['data']['attributes']['errorType'])) {
            $this->assertEquals($expectedArray['data']['attributes']['errorType'], $errorData->getErrorType());
        }
        if (isset($expectedArray['data']['attributes']['errorReason'])) {
            $this->assertEquals($expectedArray['data']['attributes']['errorReason'], $errorData->getErrorReason());
        }
        if (isset($expectedArray['data']['attributes']['errorStatus'])) {
            $this->assertEquals($expectedArray['data']['attributes']['errorStatus'], $errorData->getErrorStatus());
        }
        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $errorData->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $errorData->getStatusTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $errorData->getUpdateTime());
        }
        if (isset($expectedArray['data']['attributes']['status'])) {
            $this->assertEquals($expectedArray['data']['attributes']['status'], $errorData->getStatus());
        }

        if (isset($expectedArray['data']['attributes']['expirationDate'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['expirationDate'],
                $errorData->getExpirationDate()
            );
        }
        $this->compareTask($expectedArray, $errorData);
        $this->compareTemplateVersion($expectedArray, $errorData);
        $this->compareTemplate($expectedArray, $errorData);
        $this->compareErrorItemsData($expectedArray, $errorData);
        $this->compareCrew($expectedArray, $errorData);
        $this->compareItemsData($expectedArray, $errorData);
    }

    private function compareCrew(array $expectedArray, $errorData)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['crew']['data'])) {
                $this->assertEquals(
                    $relationships['crew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['crew']['data'][0]['id'],
                    $errorData->getCrew()->getId()
                );
            }
        }
    }

    private function compareTask(array $expectedArray, $errorData)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['task']['data'])) {
                $this->assertEquals(
                    $relationships['task']['data'][0]['type'],
                    'tasks'
                );
                $this->assertEquals(
                    $relationships['task']['data'][0]['id'],
                    $errorData->getTask()->getId()
                );
            }
        }
    }

    private function compareTemplateVersion(array $expectedArray, $errorData)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['templateVersion']['data'])) {
                $this->assertEquals(
                    $relationships['templateVersion']['data'][0]['type'],
                    'templateVersions'
                );
                $this->assertEquals(
                    $relationships['templateVersion']['data'][0]['id'],
                    $errorData->getTemplateVersion()->getId()
                );
            }
        }
    }

    private function compareTemplate(array $expectedArray, $errorData)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['template']['data'])) {
                $this->assertEquals(
                    $relationships['template']['data'][0]['type'],
                    'templates'
                );
                $this->assertEquals(
                    $relationships['template']['data'][0]['id'],
                    $errorData->getTemplate()->getId()
                );
            }
        }
    }

    private function compareErrorItemsData(array $expectedArray, $errorData)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['errorItemsData']['data'])) {
                $this->assertEquals(
                    $relationships['errorItemsData']['data'][0]['type'],
                    'itemsData'
                );
                $this->assertEquals(
                    $relationships['errorItemsData']['data'][0]['id'],
                    $errorData->getItemsData()->getId()
                );
            }
        }
    }

    private function compareItemsData(array $expectedArray, $errorData)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['itemsData']['data'])) {
                $this->assertEquals(
                    $relationships['itemsData']['data'][0]['type'],
                    'itemsData'
                );
                $this->assertEquals(
                    $relationships['itemsData']['data'][0]['attributes']['data'],
                    $errorData->getItemsData()->getData()
                );
            }
        }
    }
}
