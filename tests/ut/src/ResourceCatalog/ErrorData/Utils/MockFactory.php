<?php
namespace Sdk\ResourceCatalog\ErrorData\Utils;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;

class MockFactory
{
    public static function generateErrorData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ErrorData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $errorData = new ErrorData($id);
        $errorData->setId($id);

        //name
        self::generateName($errorData, $faker, $value);
        //identify
        self::generateIdentify($errorData, $faker, $value);
        //task
        self::generateTask($errorData, $faker, $value);
        //template,templateVersion
        self::generateTemplateAndTemplateVersion($errorData, $faker, $value);
        //itemsData
        self::generateErrorItemsData($errorData, $faker, $value);
        //errorType
        self::generateErrorType($errorData, $faker, $value);
        $errorData->setErrorReason(array(ErrorData::ERROR_REASON[$errorData->getErrorType()]));
        //errorStatus
        self::generateErrorStatus($errorData, $faker, $value);
        //status
        self::generateStatus($errorData, $faker, $value);
        $errorData->setCreateTime($faker->unixTime());
        $errorData->setUpdateTime($faker->unixTime());
        $errorData->setStatusTime($faker->unixTime());

        return $errorData;
    }

    protected static function generateName($errorData, $faker, $value) : void
    {
        $name = isset($value['name']) ? $value['name'] : $faker->word();
        
        $errorData->setName($name);
    }

    protected static function generateIdentify($errorData, $faker, $value) : void
    {
        $identify = isset($value['identify']) ? $value['identify'] : $faker->regexify('[A-Z_]{1,100}');
        
        $errorData->setIdentify($identify);
    }

    protected static function generateErrorType($errorData, $faker, $value) : void
    {
        $errorType = isset($value['errorType']) ?
            $value['errorType'] :
            $faker->randomElement(
                ErrorData::ERROR_TYPE
            );
        
        $errorData->setErrorType($errorType);
    }

    protected static function generateTask($errorData, $faker, $value) : void
    {
        $task = isset($value['task']) ?
        $value['task'] :
        \Sdk\ResourceCatalog\Task\Utils\MockFactory::generateTask($faker->randomDigit());
        
        $errorData->setTask($task);
    }

    protected static function generateTemplateAndTemplateVersion($errorData, $faker, $value) : void
    {
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate($faker->randomDigit());
        $template = isset($value['template']) ? $value['template'] : $template;
        
        $errorData->setTemplate($template);
        $errorData->setTemplateVersion($template->getTemplateVersion());
    }

    protected static function generateStatus($errorData, $faker, $value) : void
    {
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(
            ErrorData::STATUS
        );
        
        $errorData->setStatus($status);
    }

    protected static function generateErrorStatus($errorData, $faker, $value) : void
    {
        $errorStatus = isset($value['errorStatus']) ? $value['errorStatus'] : $faker->randomElement(
            ErrorData::ERROR_STATUS
        );
        
        $errorData->setErrorStatus($errorStatus);
    }

    protected static function generateErrorItemsData($errorData, $faker, $value) : void
    {
        $itemsData = isset($value['itemsData']) ?
                        $value['itemsData'] :
                        \Sdk\ResourceCatalog\ResourceCatalogData\Utils\MockFactory::generateItemsData(
                            $faker->randomDigit()
                        );
        
        $errorData->setItemsData($itemsData);
    }
}
