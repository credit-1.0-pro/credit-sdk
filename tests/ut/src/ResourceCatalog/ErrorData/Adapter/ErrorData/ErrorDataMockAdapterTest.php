<?php
namespace Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;

class ErrorDataMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new ErrorDataMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ErrorData\Model\ErrorData',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\ErrorData\Model\ErrorData',
                $each
            );
        }
    }

    public function testFetchOneAsync()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ErrorData\Model\ErrorData',
            $this->adapter->fetchOneAsync(1)
        );
    }

    public function testFetchListAsync()
    {
        $list = $this->adapter->fetchListAsync([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\ErrorData\Model\ErrorData',
                $each
            );
        }
    }

    public function testSearch()
    {
        list($list, $count) = $this->adapter->search(['filter'], ['sort'], 1, 2);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\ErrorData\Model\ErrorData',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }

    public function testSearchAsync()
    {
        list($list, $count) = $this->adapter->searchAsync(['filter'], ['sort'], 1, 2);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\ErrorData\Model\ErrorData',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }

    public function testOnlineEdit()
    {
        $this->assertTrue($this->adapter->onlineEdit(new ErrorData()));
    }
}
