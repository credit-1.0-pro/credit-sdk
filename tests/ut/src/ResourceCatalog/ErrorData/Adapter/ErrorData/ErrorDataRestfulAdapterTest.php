<?php
namespace Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\ErrorData\Model\NullErrorData;
use Sdk\ResourceCatalog\ErrorData\Translator\ErrorDataRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ErrorDataRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockErrorDataRestfulAdapter::class)
                           ->setMethods([
                               'commonMapErrors',
                               'getTranslator',
                               'isSuccess',
                               'patch',
                               'translateToObject'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new ErrorDataRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIErrorDataAdapter()
    {
        $adapter = new ErrorDataRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData\IErrorDataAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockErrorDataRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ErrorData\Translator\ErrorDataRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockErrorDataRestfulAdapter();
        $this->assertEquals('resourceCatalogs/errorData', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockErrorDataRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ErrorData\Model\NullErrorData',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockErrorDataRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'ERROR_DATA_LIST',
                ErrorDataRestfulAdapter::SCENARIOS['ERROR_DATA_LIST']
            ],
            [
                'ERROR_DATA_FETCH_ONE',
                ErrorDataRestfulAdapter::SCENARIOS['ERROR_DATA_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    /**
     * 为ErrorDataRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$errorData,$keys,$errorDataArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareErrorDataTranslator(
        ErrorData $errorData,
        array $keys,
        array $errorDataArray
    ) {
        $translator = $this->prophesize(ErrorDataRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($errorData),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($errorDataArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(ErrorData $errorData)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($errorData);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR,
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
        ];

        $this->adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = ErrorDataRestfulAdapter::MAP_ERROR;
        
        $result = $this->adapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    protected function initMapErrors(array $mapErrors)
    {
        $adapter = $this->getMockBuilder(MockErrorDataRestfulAdapter::class)
                           ->setMethods([
                               'lastErrorId',
                               'lastErrorPointer',
                               'getMapErrors'
                            ])
                           ->getMock();

        $adapter->expects($this->once())->method('lastErrorId')->willReturn(103);
        $adapter->expects($this->once())->method('lastErrorPointer')->willReturn('itemsData');
        $adapter->expects($this->once())->method('getMapErrors')->willReturn($mapErrors);

        $adapter->publicMapErrors();
        
        $this->assertEquals(RESOURCE_CATALOG_DATA_IS_UNIQUE, Core::getLastError()->getId());
        $this->assertEquals(array('pointer'=>'itemsData'), Core::getLastError()->getSource());
    }

    public function testMapErrors()
    {
        $mapErrors = array(
            103 => RESOURCE_CATALOG_DATA_IS_UNIQUE
        );

        $this->initMapErrors($mapErrors);
    }

    public function testMapErrorsArray()
    {
        $mapErrors = array(
            103 => array(
                'itemsData' => RESOURCE_CATALOG_DATA_IS_UNIQUE
            )
        );

        $this->initMapErrors($mapErrors);
    }

    public function testOnlineEditSuccess()
    {
        $errorData = \Sdk\ResourceCatalog\ErrorData\Utils\MockFactory::generateErrorData(1);
        $errorDataArray = array();

        $this->prepareErrorDataTranslator($errorData, array('expirationDate', 'crew', 'itemsData'), $errorDataArray);
        $id = $errorData->getId();

        $this->adapter
            ->method('patch')
            ->with(
                'resourceCatalogs/errorData/'.$id.'/onlineEdit',
                $errorDataArray
            );

        $this->success($errorData);
        $result = $this->adapter->onlineEdit($errorData);
        $this->assertTrue($result);
    }

    public function testOnlineEditFailure()
    {
        $errorData = \Sdk\ResourceCatalog\ErrorData\Utils\MockFactory::generateErrorData(1);
        $errorDataArray = array();

        $this->prepareErrorDataTranslator($errorData, array('expirationDate', 'crew', 'itemsData'), $errorDataArray);

        $id = $errorData->getId();

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/errorData/'.$id.'/onlineEdit', $errorDataArray);
        
        $this->failure($errorData);
        $result = $this->adapter->onlineEdit($errorData);
        $this->assertFalse($result);
    }
}
