<?php
namespace Sdk\ResourceCatalog\ErrorData\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\ErrorData\Utils\ErrorDataRestfulUtils;

use Sdk\ResourceCatalog\Task\Model\Task;
use Sdk\ResourceCatalog\Task\Translator\TaskRestfulTranslator;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator;

use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;

use Sdk\ResourceCatalog\ResourceCatalogData\Model\ItemsData;
use Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataRestfulTranslator;

class ErrorDataRestfulTranslatorTest extends TestCase
{
    use ErrorDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ErrorDataRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testGetTaskRestfulTranslator()
    {
        $translator = new MockErrorDataRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Task\Translator\TaskRestfulTranslator',
            $translator->getTaskRestfulTranslator()
        );
    }

    public function testGetTemplateRestfulTranslator()
    {
        $translator = new MockErrorDataRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator',
            $translator->getTemplateRestfulTranslator()
        );
    }

    public function testGetTemplateVersionRestfulTranslator()
    {
        $translator = new MockErrorDataRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator',
            $translator->getTemplateVersionRestfulTranslator()
        );
    }

    public function testGetItemsDataRestfulTranslator()
    {
        $translator = new MockErrorDataRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ResourceCatalogData\Translator\ItemsDataRestfulTranslator',
            $translator->getItemsDataRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $errorData = \Sdk\ResourceCatalog\ErrorData\Utils\MockFactory::generateErrorData(1);

        $expression['data']['id'] = $errorData->getId();
        $expression['data']['attributes']['name'] = $errorData->getName();
        $expression['data']['attributes']['identify'] = $errorData->getIdentify();
        $expression['data']['attributes']['errorType'] = $errorData->getErrorType();
        $expression['data']['attributes']['errorReason'] = $errorData->getErrorReason();
        $expression['data']['attributes']['errorStatus'] = $errorData->getErrorStatus();
        $expression['data']['attributes']['createTime'] = $errorData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $errorData->getUpdateTime();
        $expression['data']['attributes']['status'] = $errorData->getStatus();
        $expression['data']['attributes']['statusTime'] = $errorData->getStatusTime();

        $errorDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\ErrorData\Model\ErrorData', $errorDataObject);
        $this->compareArrayAndObjectCommon($expression, $errorDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $errorData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\ErrorData\Model\NullErrorData', $errorData);
    }

    public function testObjectToArray()
    {
        $errorData = \Sdk\ResourceCatalog\ErrorData\Utils\MockFactory::generateErrorData(1);

        $expression = $this->translator->objectToArray($errorData);

        $this->compareArrayAndObjectCommon($expression, $errorData);
    }

    public function testObjectToArrayFail()
    {
        $errorData = null;

        $expression = $this->translator->objectToArray($errorData);
        $this->assertEquals(array(), $expression);
    }

    public function testArrayToObjectWithIncluded()
    {
        $this->translator = $this->getMockBuilder(MockErrorDataRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getTaskRestfulTranslator',
                        'getTemplateVersionRestfulTranslator',
                        'getTemplateRestfulTranslator',
                        'getItemsDataRestfulTranslator'
                    ])
                    ->getMock();
        //初始化
        $expression = [
            'data'=>['relationships'=>'mock','id' => 1],
            'included'=>'mock'
        ];
        $relationships = [
            'task'=>['data'=>'mockTask'],
            'templateVersion'=>['data'=>'mockTemplateVersion'],
            'template'=>['data'=>'mockTemplate'],
            'errorItemsData'=>['data'=>'mockErrorItemsData']
        ];

        $taskInfo = ['mockTaskInfo'];
        $templateVersionInfo = ['mockTemplateVersionInfo'];
        $templateInfo = ['mockTemplateInfo'];
        $errorItemsDataInfo = ['mockErrorItemsDataInfo'];

        $task = new Task(1);
        $templateVersion = new TemplateVersion(1);
        $template = new Template(1);
        $errorItemsData = new ItemsData(1);

        //预言
        $this->translator->expects($this->once())->method('relationship')->with(
            $expression['included'],
            $expression['data']['relationships']
        )->willReturn($relationships);

        $this->translator->expects($this->exactly(4))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['task']['data']],
                 [$relationships['templateVersion']['data']],
                 [$relationships['template']['data']],
                 [$relationships['errorItemsData']['data']]
             ) ->will($this->onConsecutiveCalls(
                 $taskInfo,
                 $templateVersionInfo,
                 $templateInfo,
                 $errorItemsDataInfo
             ));

        $this->arrayToObjectWithIncludedTask($taskInfo, $task);
        $this->arrayToObjectWithIncludedTemplateVersion($templateVersionInfo, $templateVersion);
        $this->arrayToObjectWithIncludedTemplate($templateInfo, $template);
        $this->arrayToObjectWithIncludedItemsData($errorItemsDataInfo, $errorItemsData);

        //揭示
        $errorData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\ErrorData\Model\ErrorData', $errorData);
        $this->assertEquals($task, $errorData->getTask());
        $this->assertEquals($templateVersion, $errorData->getTemplateVersion());
        $this->assertEquals($template, $errorData->getTemplate());
        $this->assertEquals($errorItemsData, $errorData->getItemsData());
    }

    private function arrayToObjectWithIncludedTask($taskInfo, $task)
    {
        $taskRestfulTranslator = $this->prophesize(TaskRestfulTranslator::class);
        $taskRestfulTranslator->arrayToObject(Argument::exact($taskInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($task);

        $this->translator->expects($this->exactly(1))->method(
            'getTaskRestfulTranslator'
        )->willReturn($taskRestfulTranslator->reveal());
    }

    private function arrayToObjectWithIncludedTemplateVersion($templateVersionInfo, $templateVersion)
    {
        $translator = $this->prophesize(TemplateVersionRestfulTranslator::class);
        $translator->arrayToObject(Argument::exact($templateVersionInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($templateVersion);

        $this->translator->expects($this->exactly(1))->method(
            'getTemplateVersionRestfulTranslator'
        )->willReturn($translator->reveal());
    }

    private function arrayToObjectWithIncludedTemplate($templateInfo, $template)
    {
        $templateRestfulTranslator = $this->prophesize(TemplateRestfulTranslator::class);
        $templateRestfulTranslator->arrayToObject(Argument::exact($templateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($template);

        $this->translator->expects($this->exactly(1))->method(
            'getTemplateRestfulTranslator'
        )->willReturn($templateRestfulTranslator->reveal());
    }

    private function arrayToObjectWithIncludedItemsData($errorItemsDataInfo, $errorItemsData)
    {
        $itemsDataRestfulTranslator = $this->prophesize(ItemsDataRestfulTranslator::class);
        $itemsDataRestfulTranslator->arrayToObject(Argument::exact($errorItemsDataInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($errorItemsData);
        //绑定
        $this->translator->expects($this->exactly(1))->method(
            'getItemsDataRestfulTranslator'
        )->willReturn($itemsDataRestfulTranslator->reveal());
    }
}
