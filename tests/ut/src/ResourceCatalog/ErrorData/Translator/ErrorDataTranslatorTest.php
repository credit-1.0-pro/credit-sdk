<?php
namespace Sdk\ResourceCatalog\ErrorData\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\ErrorData\Utils\ErrorDataUtils;

class ErrorDataTranslatorTest extends TestCase
{
    use ErrorDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ErrorDataTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\ResourceCatalog\ErrorData\Model\NullErrorData', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $errorData = \Sdk\ResourceCatalog\ErrorData\Utils\MockFactory::generateErrorData(1);

        $expression = $this->translator->objectToArray($errorData);
    
        $this->compareArrayAndObjectErrorData($expression, $errorData);
    }

    public function testObjectToArrayFail()
    {
        $errorData = null;

        $expression = $this->translator->objectToArray($errorData);
        $this->assertEquals(array(), $expression);
    }
}
