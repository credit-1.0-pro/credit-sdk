<?php
namespace Sdk\ResourceCatalog\ErrorData\CommandHandler\ErrorData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\ResourceCatalogData\Model\ItemsData;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\ErrorData\Repository\ErrorDataRepository;
use Sdk\ResourceCatalog\ErrorData\Command\ErrorData\OnlineEditErrorDataCommand;

class OnlineEditErrorDataCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(OnlineEditErrorDataCommandHandler::class)
                                ->setMethods(['fetchErrorData'])
                                ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    public function testGetRepository()
    {
        $commandHandler = new MockOnlineEditErrorDataCommandHandler();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ErrorData\Repository\ErrorDataRepository',
            $commandHandler->getRepository()
        );
    }

    public function testFetchErrorData()
    {
        $commandHandler = $this->getMockBuilder(MockOnlineEditErrorDataCommandHandler::class)
                                ->setMethods(['getRepository'])
                                ->getMock();

        $id = 1;
        $errorData = \Sdk\ResourceCatalog\ErrorData\Utils\MockFactory::generateErrorData($id);

        $repository = $this->prophesize(ErrorDataRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($errorData);

        $commandHandler->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $commandHandler->fetchErrorData($id);

        $this->assertEquals($result, $errorData);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $id = 1;
        $expirationDate = 1651234567;
        $itemsData = array('itemsData');
        
        $command = new OnlineEditErrorDataCommand(
            $itemsData,
            $expirationDate,
            $id
        );

        $itemsDataObject = $this->prophesize(ItemsData::class);
        $itemsDataObject->setData($itemsData)->shouldBeCalledTimes(1);

        $errorData = $this->prophesize(ErrorData::class);
        $errorData->setExpirationDate($expirationDate)->shouldBeCalledTimes(1);
        $errorData->getItemsData()->shouldBeCalledTimes(1)->willReturn($itemsDataObject->reveal());
        $errorData->onlineEdit()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())->method('fetchErrorData')->willReturn($errorData->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
