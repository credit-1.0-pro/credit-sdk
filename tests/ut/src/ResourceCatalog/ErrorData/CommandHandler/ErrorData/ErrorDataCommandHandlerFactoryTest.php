<?php
namespace Sdk\ResourceCatalog\ErrorData\CommandHandler\ErrorData;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\ErrorData\Command\ErrorData\OnlineEditErrorDataCommand;

class ErrorDataCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new ErrorDataCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testOnlineEditErrorDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new OnlineEditErrorDataCommand(
                array($this->faker->title()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ErrorData\CommandHandler\ErrorData\OnlineEditErrorDataCommandHandler',
            $commandHandler
        );
    }
}
