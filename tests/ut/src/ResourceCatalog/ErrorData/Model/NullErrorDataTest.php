<?php
namespace Sdk\ResourceCatalog\ErrorData\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullErrorDataTest extends TestCase
{
    private $nullErrorData;

    public function setUp()
    {
        $this->nullErrorData = NullErrorData::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullErrorData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsErrorData()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ErrorData\Model\ErrorData',
            $this->nullErrorData
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullErrorData
        );
    }

    public function testOnlineEdit()
    {
        $result = $this->nullErrorData->onlineEdit();
        
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
