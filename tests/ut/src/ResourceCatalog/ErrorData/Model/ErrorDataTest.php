<?php
namespace Sdk\ResourceCatalog\ErrorData\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\ErrorData\Repository\ErrorDataRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class ErrorDataTest extends TestCase
{
    private $errorData;

    public function setUp()
    {
        $this->errorData = new ErrorData();
    }

    public function tearDown()
    {
        unset($this->errorData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }


    public function testExtendsResourceCatalogData()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ResourceCatalogData\Model\ResourceCatalogData',
            $this->errorData
        );
    }

    public function testErrorDataConstructor()
    {
        $this->assertEmpty($this->errorData->getErrorStatus());
        $this->assertEmpty($this->errorData->getErrorType());
        $this->assertEmpty($this->errorData->getErrorReason());
        $this->assertEquals(ErrorData::STATUS['UN_PROCESSED'], $this->errorData->getStatus());
    }

    //errorType 测试 ------------------------------------------------------ start
    /**
     * 循环测试 ErrorData setErrorType() 是否符合预定范围
     *
     * @dataProvider errorTypeProvider
     */
    public function testSetErrorType($actual, $expected)
    {
        $this->errorData->setErrorType($actual);
        $this->assertEquals($expected, $this->errorData->getErrorType());
    }

    /**
     * 循环测试 ErrorData setErrorType() 数据构建器
     */
    public function errorTypeProvider()
    {
        return array(
            array(ErrorData::ERROR_TYPE['MISSING_DATA'], ErrorData::ERROR_TYPE['MISSING_DATA']),
            array(ErrorData::ERROR_TYPE['COMPARISON_FAILED'], ErrorData::ERROR_TYPE['COMPARISON_FAILED']),
            array(ErrorData::ERROR_TYPE['DUPLICATION_DATA'], ErrorData::ERROR_TYPE['DUPLICATION_DATA']),
            array(
                ErrorData::ERROR_TYPE['FORMAT_VALIDATION_FAILED'],
                ErrorData::ERROR_TYPE['FORMAT_VALIDATION_FAILED']
            ),
        );
    }

    /**
     * 设置 ErrorData setErrorType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetErrorTypeWrongType()
    {
        $this->errorData->setErrorType('string');
    }
    //errorType 测试 ------------------------------------------------------   end
    //errorReason 测试 -------------------------------------------------------- start
    /**
     * 设置 ErrorData setErrorReason() 正确的传参类型,期望传值正确
     */
    public function testSetErrorReasonCorrectType()
    {
        $this->errorData->setErrorReason(array('string'));
        $this->assertEquals(array('string'), $this->errorData->getErrorReason());
    }

    /**
     * 设置 ErrorData setErrorReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetErrorReasonWrongType()
    {
        $this->errorData->setErrorReason('errorReason');
    }
    //errorReason 测试 --------------------------------------------------------   end
    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->errorData->setStatus($actual);
        $this->assertEquals($expected, $this->errorData->getStatus());
    }
    /**
     * 循环测试 DispatchDepartment setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(ErrorData::STATUS['UN_PROCESSED'],ErrorData::STATUS['UN_PROCESSED']),
            array(ErrorData::STATUS['PROCESSED'],ErrorData::STATUS['PROCESSED']),
            array(999, ErrorData::STATUS['UN_PROCESSED'])
        );
    }
    /**
     * 设置 setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->errorData->setStatus('status');
    }
    //status 测试 ------------------------------------------------------   end
    //errorStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 ErrorData setErrorStatus() 是否符合预定范围
     *
     * @dataProvider errorStatusProvider
     */
    public function testSetErrorStatus($actual, $expected)
    {
        $this->errorData->setErrorStatus($actual);
        $this->assertEquals($expected, $this->errorData->getErrorStatus());
    }

    /**
     * 循环测试 ErrorData setErrorStatus() 数据构建器
     */
    public function errorStatusProvider()
    {
        return array(
            array(ErrorData::ERROR_STATUS['NORMAL'], ErrorData::ERROR_STATUS['NORMAL']),
            array(ErrorData::ERROR_STATUS['PROGRAM_EXCEPTION'], ErrorData::ERROR_STATUS['PROGRAM_EXCEPTION']),
            array(ErrorData::ERROR_STATUS['STORAGE_EXCEPTION'], ErrorData::ERROR_STATUS['STORAGE_EXCEPTION']),
        );
    }

    /**
     * 设置 ErrorData setErrorStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetErrorStatusWrongType()
    {
        $this->errorData->setErrorStatus('string');
    }
    //errorStatus 测试 ------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ErrorData\Repository\ErrorDataRepository',
            $this->errorData->getRepository()
        );
    }

    public function testOnlineEdit()
    {
        $this->stub = $this->getMockBuilder(ErrorData::class)->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(ErrorDataRepository::class);
        $repository->onlineEdit(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->onlineEdit();
        $this->assertTrue($result);
    }
}
