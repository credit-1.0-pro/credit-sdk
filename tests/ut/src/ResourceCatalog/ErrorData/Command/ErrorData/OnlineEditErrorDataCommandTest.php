<?php
namespace Sdk\ResourceCatalog\ErrorData\Command\ErrorData;

use PHPUnit\Framework\TestCase;

class OnlineEditErrorDataCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'itemsData' => array($faker->md5()),
            'expirationDate' => $faker->randomDigit(1),
            'id' => $faker->randomDigit()
        );

        $this->command = new OnlineEditErrorDataCommand(
            $this->fakerData['itemsData'],
            $this->fakerData['expirationDate'],
            $this->fakerData['id']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testItemsDataParameter()
    {
        $this->assertEquals($this->fakerData['itemsData'], $this->command->itemsData);
    }

    public function testExpirationDateParameter()
    {
        $this->assertEquals($this->fakerData['expirationDate'], $this->command->expirationDate);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}
