<?php
namespace Sdk\ResourceCatalog\ErrorData\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData\ErrorDataRestfulAdapter;

class ErrorDataRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(ErrorDataRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIErrorDataAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData\IErrorDataAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData\ErrorDataRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData\ErrorDataMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(ErrorDataRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testOnlineEdit()
    {
        $errorData = \Sdk\ResourceCatalog\ErrorData\Utils\MockFactory::generateErrorData(1);

        $adapter = $this->prophesize(ErrorDataRestfulAdapter::class);
        $adapter->onlineEdit(Argument::exact($errorData))->shouldBeCalledTimes(1);
        $this->repository->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $this->repository->onlineEdit($errorData);
    }
}
