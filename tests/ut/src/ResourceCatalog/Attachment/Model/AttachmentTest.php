<?php
namespace Sdk\ResourceCatalog\Attachment\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Model\Guest;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\NullTemplate;
use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;

use Sdk\ResourceCatalog\Task\Repository\TaskRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class AttachmentTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(Attachment::class)
                           ->setMethods([
                               'validate',
                               'generateFilePath',
                               'move'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    /**
     * Attachment 领域对象,测试构造函数
     */
    public function testAttachmentConstructor()
    {
        $this->assertEmpty($this->stub->getName());
        $this->assertEmpty($this->stub->getTmpName());
        $this->assertEmpty($this->stub->getSize());
        $this->assertEmpty($this->stub->getError());
        $this->assertEmpty($this->stub->getHash());
        $this->assertEquals(
            Core::$container->get('attachment.resourceCatalog.upload.path'),
            $this->stub->getPath()
        );
        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Crew',
            $this->stub->getCrew()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\Template',
            $this->stub->getTemplate()
        );
    }

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 Attachment setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->stub->setName('name');
        $this->assertEquals('name', $this->stub->getName());
    }

    /**
     * 设置 Attachment setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setName(array('name'));
    }
    //name 测试 --------------------------------------------------------   end

    //tmpName 测试 -------------------------------------------------------- start
    /**
     * 设置 Attachment setTmpName() 正确的传参类型,期望传值正确
     */
    public function testSetTmpNameCorrectType()
    {
        $this->stub->setTmpName('attachmentTmpName');
        $this->assertEquals('attachmentTmpName', $this->stub->getTmpName());
    }

    /**
     * 设置 Attachment setTmpName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTmpNameWrongType()
    {
        $this->stub->setTmpName(array('attachmentTmpName'));
    }
    //tmpName 测试 --------------------------------------------------------   end

    //size 测试 -------------------------------------------------------- start
    /**
     * 设置 Attachment setSize() 正确的传参类型,期望传值正确
     */
    public function testSetSizeCorrectType()
    {
        $this->stub->setSize(1);
        $this->assertEquals(1, $this->stub->getSize());
    }

    /**
     * 设置 Attachment setSize() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSizeWrongType()
    {
        $this->stub->setSize(array('size'));
    }
    //size 测试 --------------------------------------------------------   end

    //error 测试 -------------------------------------------------------- start
    /**
     * 设置 Attachment setError() 正确的传参类型,期望传值正确
     */
    public function testSetErrorCorrectType()
    {
        $this->stub->setError(1);
        $this->assertEquals(1, $this->stub->getError());
    }

    /**
     * 设置 Attachment setError() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetErrorWrongType()
    {
        $this->stub->setError(array('error'));
    }
    //error 测试 --------------------------------------------------------   end

    //hash 测试 -------------------------------------------------------- start
    /**
     * 设置 Attachment setHash() 正确的传参类型,期望传值正确
     */
    public function testSetHashCorrectType()
    {
        $this->stub->setHash('hash');
        $this->assertEquals('hash', $this->stub->getHash());
    }

    /**
     * 设置 Attachment setHash() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetHashWrongType()
    {
        $this->stub->setHash(array('hash'));
    }
    //hash 测试 --------------------------------------------------------   end

    public function testGetTaskRepository()
    {
        $stub = new MockAttachment();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Task\Repository\TaskRepository',
            $stub->getTaskRepository()
        );
    }

    public function testGetTemplateRepository()
    {
        $stub = new MockAttachment();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Repository\TemplateRepository',
            $stub->getTemplateRepository()
        );
    }

    public function testGenerateHash()
    {
        $tmpName = '/var/www/html/tests/mock/ResourceCatalog/Attachment/Model/MockFile';
        $this->stub->setTmpName($tmpName);
        $this->stub->setSize(1024);
        $this->stub->generateHash();

        $hash = md5(md5_file($this->stub->getTmpName()).$this->stub->getSize());

        $this->assertEquals($hash, $this->stub->getHash());
    }
    //template 测试 ---------------------------------------------------- start
    /**
     * 设置 setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $template = new Template();

        $this->stub->setTemplate($template);
        $this->assertEquals($template, $this->stub->getTemplate());
    }

    /**
     * 设置 setTemplate() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $template = array(1);

        $this->stub->setTemplate($template);
    }
    //template 测试 ----------------------------------------------------   end
    
    public function testUploadValidateFalse()
    {
        $this->stub->expects($this->exactly(1))->method('validate')->willReturn(false);
        $result = $this->stub->upload();

        $this->assertFalse($result);
    }

    public function testUploadValidate()
    {
        $destinationFile = 'fileName.xls';

        $this->stub->expects($this->exactly(1))->method('validate')->willReturn(true);
        $this->stub->expects($this->exactly(1))->method('generateFilePath')->willReturn($destinationFile);
        $this->stub->expects($this->exactly(1))->method('move')->with($destinationFile)->willReturn(true);

        $result = $this->stub->upload();
        
        $this->assertTrue($result);
    }

    public function testValidate()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)
                           ->setMethods([
                               'isCrewExist',
                               'validateFileIsEmpty',
                               'validateFileSize',
                               'validateFileExtension',
                               'validateFileError',
                               'validateFileName',
                               'validateFileIsExist'
                            ])->getMock();

        $stub->expects($this->exactly(1))->method('isCrewExist')->willReturn(true);
        $stub->expects($this->exactly(1))->method('validateFileIsEmpty')->willReturn(true);
        $stub->expects($this->exactly(1))->method('validateFileSize')->willReturn(true);
        $stub->expects($this->exactly(1))->method('validateFileExtension')->willReturn(true);
        $stub->expects($this->exactly(1))->method('validateFileError')->willReturn(true);
        $stub->expects($this->exactly(1))->method('validateFileName')->willReturn(true);
        $stub->expects($this->exactly(1))->method('validateFileIsExist')->willReturn(true);

        $result = $stub->validate();
        
        $this->assertTrue($result);
    }

    public function testIsCrewExistCrewNullFailure()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getCrew'])->getMock();
        $crew = Guest::getInstance();

        $stub->expects($this->exactly(1))->method('getCrew')->willReturn($crew);

        $result = $stub->isCrewExist();
        
        $this->assertFalse($result);
        $this->assertEquals(NEED_SIGNIN, Core::getLastError()->getId());
    }

    public function testIsCrewExistCrewIdNullFailure()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getCrew'])->getMock();
        $crew = new Crew(0);
        Core::$container->set('crew', $crew);

        $stub->expects($this->exactly(1))->method('getCrew')->willReturn($crew);

        $result = $stub->isCrewExist();
        
        $this->assertFalse($result);
        $this->assertEquals(NEED_SIGNIN, Core::getLastError()->getId());
    }

    public function testIsCrewExistSuccess()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getCrew'])->getMock();
        $crew = new Crew(1);
        Core::$container->set('crew', $crew);

        $stub->expects($this->exactly(1))->method('getCrew')->willReturn($crew);

        $result = $stub->isCrewExist();
        
        $this->assertTrue($result);
    }

    public function testValidateFileIsEmptyFailure()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getName'])->getMock();
        $stub->expects($this->exactly(1))->method('getName')->willReturn('');

        $result = $stub->validateFileIsEmpty();
        
        $this->assertFalse($result);
        $this->assertEquals(FILE_IS_EMPTY, Core::getLastError()->getId());
    }

    public function testValidateFileIsEmptySuccess()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getName'])->getMock();
        $stub->expects($this->exactly(1))->method('getName')->willReturn('name');

        $result = $stub->validateFileIsEmpty();
        
        $this->assertTrue($result);
    }

    public function testValidateFileSizeFailure()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getSize'])->getMock();
        $stub->expects($this->exactly(1))->method('getSize')->willReturn(Attachment::MAX_FILE_SIZE+1);

        $result = $stub->validateFileSize();
        
        $this->assertFalse($result);
        $this->assertEquals(FILE_SIZE_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function testValidateFileSizeSuccess()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getSize'])->getMock();
        $stub->expects($this->exactly(1))->method('getSize')->willReturn(Attachment::MAX_FILE_SIZE-1);

        $result = $stub->validateFileSize();
        
        $this->assertTrue($result);
    }

    public function testValidateFileExtensionFailure()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getName'])->getMock();
        $stub->expects($this->exactly(1))->method('getName')->willReturn('1.jpg');

        $result = $stub->validateFileExtension();
        
        $this->assertFalse($result);
        $this->assertEquals(FILE_EXTENSION_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function testValidateFileExtensionSuccess()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getName'])->getMock();
        $stub->expects($this->exactly(1))->method('getName')->willReturn('1.xls');

        $result = $stub->validateFileExtension();
        
        $this->assertTrue($result);
    }

    public function testValidateFileErrorFailure()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getError'])->getMock();
        $stub->expects($this->exactly(1))->method('getError')->willReturn(Attachment::DEFAULT_ERROR_CODE+1);

        $result = $stub->validateFileError();
        
        $this->assertFalse($result);
        $this->assertEquals(FILE_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function testValidateFileErrorSuccess()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getError'])->getMock();
        $stub->expects($this->exactly(1))->method('getError')->willReturn(Attachment::DEFAULT_ERROR_CODE);

        $result = $stub->validateFileError();
        
        $this->assertTrue($result);
    }

    public function testValidateFileNameFormatFailure()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getName'])->getMock();
        $stub->expects($this->exactly(1))->method('getName')->willReturn('1.xls');

        $result = $stub->validateFileName();
        
        $this->assertFalse($result);
        $this->assertEquals(FILE_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function testValidateFileNameTemplateIdUnequalFailure()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods(['getName'])->getMock();
        $template = new Template(2);
        $stub->setTemplate($template);
        $stub->expects($this->exactly(1))->method('getName')->willReturn('xk_MA.xls');

        $result = $stub->validateFileName();
        
        $this->assertFalse($result);
        $this->assertEquals(FILE_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function testValidateFileNameTemplateNullFailure()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)
                ->setMethods([
                    'getName',
                    'getTemplateRepository'
                ])->getMock();

        $template = new Template(1);
        $nullTemplate = new NullTemplate();
        $stub->setTemplate($template);
        $stub->expects($this->exactly(1))->method('getName')->willReturn('xk_MA.xls');

        $repository = $this->prophesize(TemplateRepository::class);
        $repository->fetchOne(1)->shouldBeCalledTimes(1)->willReturn($nullTemplate);
        $stub->expects($this->exactly(1))->method('getTemplateRepository')->willReturn($repository->reveal());

        $result = $stub->validateFileName();
        
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals(array('pointer'=>'template'), Core::getLastError()->getSource());
    }

    public function testValidateFileNameTemplateIdentifyUnequalFailure()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)
                ->setMethods([
                    'getName',
                    'getTemplateRepository'
                ])->getMock();

        $template = new Template(1);
        $template->setIdentify('XK');
        $stub->setTemplate($template);
        $stub->expects($this->exactly(1))->method('getName')->willReturn('cf_MA.xls');

        $repository = $this->prophesize(TemplateRepository::class);
        $repository->fetchOne(1)->shouldBeCalledTimes(1)->willReturn($template);
        $stub->expects($this->exactly(1))->method('getTemplateRepository')->willReturn($repository->reveal());

        $result = $stub->validateFileName();
        
        $this->assertFalse($result);
        $this->assertEquals(FILE_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function testValidateFileNameSuccess()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)
                ->setMethods([
                    'getName',
                    'getTemplateRepository'
                ])->getMock();

        $template = new Template(1);
        $template->setIdentify('XK');
        $stub->setTemplate($template);
        $stub->expects($this->exactly(1))->method('getName')->willReturn('XK_MA.xls');

        $repository = $this->prophesize(TemplateRepository::class);
        $repository->fetchOne(1)->shouldBeCalledTimes(1)->willReturn($template);
        $stub->expects($this->exactly(1))->method('getTemplateRepository')->willReturn($repository->reveal());

        $result = $stub->validateFileName();
        
        $this->assertTrue($result);
    }

    protected function initValidateFileIsExist(int $count, array $list)
    {
        $this->stub = $this->getMockBuilder(MockAttachment::class)
                ->setMethods([
                    'generateHash',
                    'getHash',
                    'getTaskRepository'
                ])->getMock();

        $hash = md5('hash');
        $this->stub->expects($this->exactly(1))->method('generateHash');
        $this->stub->expects($this->exactly(1))->method('getHash')->willReturn($hash);

        $filter['hash'] = $hash;
        $sort = ['updateTime'];

        $repository = $this->prophesize(TaskRepository::class);
        $repository->search($filter, $sort, PAGE, PAGE)->shouldBeCalledTimes(1)->willReturn([$count, $list]);
        $this->stub->expects($this->exactly(1))->method('getTaskRepository')->willReturn($repository->reveal());
    }

    public function testValidateFileIsExistFailure()
    {
        $count = 1;
        $list = array('list');
        $this->initValidateFileIsExist($count, $list);

        $result = $this->stub->validateFileIsExist();
        
        $this->assertFalse($result);
        $this->assertEquals(FILE_IS_UNIQUE, Core::getLastError()->getId());
    }

    public function testValidateFileIsExistSuccess()
    {
        $count = 0;
        $list = array();
        $this->initValidateFileIsExist($count, $list);

        $result = $this->stub->validateFileIsExist();
        
        $this->assertTrue($result);
    }

    public function testMove()
    {
        $stub = new MockAttachment();

        $filePath = 'filePath';
        $result = $stub->move($$filePath);
        $this->assertFalse($result);
    }

    public function testGenerateFilePath()
    {
        $stub = $this->getMockBuilder(MockAttachment::class)->setMethods([
                    'getName',
                    'getTemplate',
                    'getCrew',
                    'getHash'
                ])->getMock();

        $template = new Template(1);
        $template->setIdentify('XK');
        $crew = new Crew(1);
        $hash = md5('hash');
        $stub->expects($this->exactly(1))->method('getName')->willReturn('XK_MA.xls');
        $stub->expects($this->exactly(2))->method('getTemplate')->willReturn($template);
        $stub->expects($this->exactly(1))->method('getCrew')->willReturn($crew);
        $stub->expects($this->exactly(1))->method('getHash')->willReturn($hash);
        $filePathExpect = $stub->getPath().'XK_1_1_'.$hash.'.xls';
        
        $result = $stub->generateFilePath();
        
        $this->assertEquals($result, $filePathExpect);
    }
}
