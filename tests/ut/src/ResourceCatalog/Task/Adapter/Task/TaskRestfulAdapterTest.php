<?php
namespace Sdk\ResourceCatalog\Task\Adapter\Task;

use PHPUnit\Framework\TestCase;

class TaskRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockTaskRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new TaskRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsITaskAdapter()
    {
        $adapter = new TaskRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Task\Adapter\Task\ITaskAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Task\Translator\TaskRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('resourceCatalogs/tasks', $this->adapter->getResource());
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Task\Model\NullTask',
            $this->adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'TASK_LIST',
                TaskRestfulAdapter::SCENARIOS['TASK_LIST']
            ],
            [
                'TASK_FETCH_ONE',
                TaskRestfulAdapter::SCENARIOS['TASK_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
}
