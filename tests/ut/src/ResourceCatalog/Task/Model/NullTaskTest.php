<?php
namespace Sdk\ResourceCatalog\Task\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullTaskTest extends TestCase
{
    private $nullTask;

    public function setUp()
    {
        $this->nullTask = NullTask::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullTask);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsTask()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Task\Model\Task',
            $this->nullTask
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullTask
        );
    }
}
