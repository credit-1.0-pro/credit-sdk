<?php
namespace Sdk\ResourceCatalog\Task\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class TaskTest extends TestCase
{
    private $task;

    public function setUp()
    {
        $this->task = new MockTask();
    }

    public function tearDown()
    {
        unset($this->task);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testTaskConstructor()
    {
        $this->assertEquals(0, $this->task->getId());
        $this->assertEquals(0, $this->task->getAdministrativeArea());
        $this->assertEquals(0, $this->task->getTotal());
        $this->assertEquals(0, $this->task->getSuccessNumber());
        $this->assertEquals(0, $this->task->getFailureNumber());
        $this->assertEquals(0, $this->task->getTemplate());
        $this->assertEquals(0, $this->task->getTemplateVersion());
        $this->assertEquals(0, $this->task->getRule());
        $this->assertEquals(0, $this->task->getRuleVersion());
        $this->assertEquals(0, $this->task->getErrorNumber());
        $this->assertEquals('', $this->task->getFileName());
        $this->assertEquals(TASK::STATUS['DEFAULT'], $this->task->getStatus());
        $this->assertEquals(0, $this->task->getUpdateTime());
        $this->assertEquals(0, $this->task->getCreateTime());
        $this->assertEquals(0, $this->task->getStatusTime());
        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Crew',
            $this->task->getCrew()
        );
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Model\UserGroup',
            $this->task->getUserGroup()
        );
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Task setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->task->setId(1);
        $this->assertEquals(1, $this->task->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //crew 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $expectedCrew = new Crew();

        $this->task->setCrew($expectedCrew);
        $this->assertEquals($expectedCrew, $this->task->getCrew());
    }

    /**
     * 设置 Task setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->task->setCrew('crew');
    }
    //crew 测试 --------------------------------------------------------   end

    //userGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->task->setUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->task->getUserGroup());
    }

    /**
     * 设置 Task setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->task->setUserGroup('userGroup');
    }
    //userGroup 测试 --------------------------------------------------------   end

    //administrativeArea 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setAdministrativeArea() 正确的传参类型,期望传值正确
     */
    public function testSetAdministrativeAreaCorrectType()
    {
        $this->task->setAdministrativeArea(1);
        $this->assertEquals(1, $this->task->getAdministrativeArea());
    }

    /**
     * 设置 Task setAdministrativeArea() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAdministrativeAreaWrongType()
    {
        $this->task->setAdministrativeArea(array(1,2,3));
    }
    //administrativeArea 测试 --------------------------------------------------------   end

    //total 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setTotal() 正确的传参类型,期望传值正确
     */
    public function testSetTotalCorrectType()
    {
        $this->task->setTotal(1);
        $this->assertEquals(1, $this->task->getTotal());
    }

    /**
     * 设置 Task setTotal() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalWrongType()
    {
        $this->task->setTotal(array(1,2,3));
    }
    //total 测试 --------------------------------------------------------   end

    //successNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setSuccessNumber() 正确的传参类型,期望传值正确
     */
    public function testSetSuccessNumberCorrectType()
    {
        $this->task->setSuccessNumber(1);
        $this->assertEquals(1, $this->task->getSuccessNumber());
    }

    /**
     * 设置 Task setSuccessNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSuccessNumberWrongType()
    {
        $this->task->setSuccessNumber(array(1,2,3));
    }
    //successNumber 测试 --------------------------------------------------------   end

    //failureNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setFailureNumber() 正确的传参类型,期望传值正确
     */
    public function testSetFailureNumberCorrectType()
    {
        $this->task->setFailureNumber(1);
        $this->assertEquals(1, $this->task->getFailureNumber());
    }

    /**
     * 设置 Task setFailureNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFailureNumberWrongType()
    {
        $this->task->setFailureNumber(array(1,2,3));
    }
    //failureNumber 测试 --------------------------------------------------------   end

    //template 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $this->task->setTemplate(1);
        $this->assertEquals(1, $this->task->getTemplate());
    }

    /**
     * 设置 Task setTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $this->task->setTemplate(array(1,2,3));
    }
    //template 测试 --------------------------------------------------------   end

    //templateVersion 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setTemplateVersion() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateVersionCorrectType()
    {
        $this->task->setTemplateVersion(1);
        $this->assertEquals(1, $this->task->getTemplateVersion());
    }

    /**
     * 设置 Task setTemplateVersion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateVersionWrongType()
    {
        $this->task->setTemplateVersion(array(1,2,3));
    }
    //templateVersion 测试 --------------------------------------------------------   end

    //rule 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setRule() 正确的传参类型,期望传值正确
     */
    public function testSetRuleCorrectType()
    {
        $this->task->setRule(1);
        $this->assertEquals(1, $this->task->getRule());
    }

    /**
     * 设置 Task setRule() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRuleWrongType()
    {
        $this->task->setRule(array(1,2,3));
    }
    //rule 测试 --------------------------------------------------------   end

    //ruleVersion 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setRuleVersion() 正确的传参类型,期望传值正确
     */
    public function testSetRuleVersionCorrectType()
    {
        $this->task->setRuleVersion(1);
        $this->assertEquals(1, $this->task->getRuleVersion());
    }

    /**
     * 设置 Task setRuleVersion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRuleVersionWrongType()
    {
        $this->task->setRuleVersion(array(1,2,3));
    }
    //ruleVersion 测试 --------------------------------------------------------   end

    //errorNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setErrorNumber() 正确的传参类型,期望传值正确
     */
    public function testSetErrorNumberCorrectType()
    {
        $this->task->setErrorNumber(1);
        $this->assertEquals(1, $this->task->getErrorNumber());
    }

    /**
     * 设置 Task setErrorNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetErrorNumberWrongType()
    {
        $this->task->setErrorNumber(array(1,2,3));
    }
    //errorNumber 测试 --------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Task setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->task->setStatus($actual);
        $this->assertEquals($expected, $this->task->getStatus());
    }

    /**
     * 循环测试 Task setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(Task::STATUS['DEFAULT'], Task::STATUS['DEFAULT']),
            array(Task::STATUS['SUCCESS'], Task::STATUS['SUCCESS']),
            array(Task::STATUS['FAILURE'], Task::STATUS['FAILURE']),
            array(Task::STATUS['FAILURE_FILE_DOWNLOAD'], Task::STATUS['FAILURE_FILE_DOWNLOAD']),
            array(999, Task::STATUS['DEFAULT']),
        );
    }

    /**
     * 设置 Task setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->task->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end

    //fileName 测试 -------------------------------------------------------- start
    /**
     * 设置 Task setFileName() 正确的传参类型,期望传值正确
     */
    public function testSetFileNameCorrectType()
    {
        $this->task->setFileName('fileName');
        $this->assertEquals('fileName', $this->task->getFileName());
    }

    /**
     * 设置 Task setFileName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFileNameWrongType()
    {
        $this->task->setFileName(array(1,2,3));
    }
    //fileName 测试 --------------------------------------------------------   end

    public function testGetFailureFileName()
    {
        $task = $this->getMockBuilder(MockTask::class)->setMethods(['fileIsExits'])->getMock();

        $fileName = 'fileName.xls';
        $task->setStatus(Task::STATUS['FAILURE_FILE_DOWNLOAD']);
        $task->setFileName($fileName);
        $failureFileName = Core::$container->get('attachment.resourceCatalog.download.path').$fileName;

        $task->expects($this->exactly(1))->method('fileIsExits')->willReturn(true);

        $result = $task->getFailureFileName();
        $this->assertEquals($failureFileName, $result);
    }

    public function testFileIsExits()
    {
        $task = new MockTask();
        $result = $task->fileIsExits('');

        $this->assertFalse($result);
    }
}
