<?php
namespace Sdk\ResourceCatalog\Task\Utils;

use Sdk\ResourceCatalog\Task\Model\Task;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

trait TaskUtils
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    private function compareArrayAndObjectTask(
        array $expectedArray,
        $task
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($task->getId()));
        $this->assertEquals($expectedArray['administrativeArea'], $task->getAdministrativeArea());
        $this->assertEquals($expectedArray['total'], $task->getTotal());
        $this->assertEquals($expectedArray['successNumber'], $task->getSuccessNumber());
        $this->assertEquals($expectedArray['failureNumber'], $task->getFailureNumber());
        $this->assertEquals($expectedArray['template'], $task->getTemplate());
        $this->assertEquals($expectedArray['templateVersion'], $task->getTemplateVersion());
        $this->assertEquals($expectedArray['rule'], $task->getRule());
        $this->assertEquals($expectedArray['ruleVersion'], $task->getRuleVersion());
        $this->assertEquals($expectedArray['errorNumber'], $task->getErrorNumber());
        $this->assertEquals($expectedArray['fileName'], $task->getFileName());
        $this->assertEquals($expectedArray['failureFileName'], $task->getFailureFileName());
        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($task->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            Task::STATUS_CN[$task->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            Task::STATUS_TYPE[$task->getStatus()]
        );
        $this->assertEquals($expectedArray['updateTime'], $task->getUpdateTime());
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $task->getUpdateTime())
        );
                    
        $this->assertEquals($expectedArray['statusTime'], $task->getStatusTime());
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $task->getStatusTime())
        );
                    
        $this->assertEquals($expectedArray['createTime'], $task->getCreateTime());
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $task->getCreateTime())
        );

        $this->assertEquals(
            $expectedArray['crew'],
            $this->getCrewTranslator()->objectToArray($task->getCrew(), ['id', 'realName'])
        );
        $this->assertEquals(
            $expectedArray['userGroup'],
            $this->getUserGroupTranslator()->objectToArray($task->getUserGroup(), ['id', 'name'])
        );
    }
}
