<?php
namespace Sdk\ResourceCatalog\Task\Utils;

use Sdk\ResourceCatalog\Task\Model\Task;

class MockFactory
{
    public static function generateTask(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Task {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $task = new Task($id);
        $task->setId($id);

        //crew,userGroup,administrativeArea
        self::generateCrew($task, $faker, $value);
        
        //total,successNumber,failureNumber
        self::generateNumber($task, $faker, $value);
        //
        //template,templateVersion
        self::generateTemplate($task, $faker, $value);
        //
        //rule,ruleVersion
        self::generateRule($task, $faker, $value);
        //errorNumber
        self::generateErrorNumber($task, $faker, $value);
        //fileName
        self::generateFileName($task, $faker, $value);
        //status
        self::generateStatus($task, $faker, $value);

        $task->setCreateTime($faker->unixTime());
        $task->setUpdateTime($faker->unixTime());
        $task->setStatusTime($faker->unixTime());

        return $task;
    }

    protected static function generateCrew($task, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Sdk\User\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        $userGroup = $crew->getUserGroup();
        
        $task->setCrew($crew);
        $task->setUserGroup($userGroup);
        $task->setAdministrativeArea($userGroup->getAdministrativeArea());
    }

    protected static function generateNumber($task, $faker, $value) : void
    {
        $total = isset($value['total']) ? $value['total'] : $faker->randomDigit(2);
        $successNumber = isset($value['successNumber']) ? $value['successNumber'] : $faker->randomDigit(1);
        $failureNumber = isset($value['failureNumber']) ? $value['failureNumber'] : $total - $successNumber;
        
        $task->setTotal($total);
        $task->setSuccessNumber($successNumber);
        $task->setFailureNumber($failureNumber);
    }

    protected static function generateTemplate($task, $faker, $value) : void
    {
        $template = isset($value['template']) ? $value['template'] : $faker->randomDigit(1);
        $templateVersion = isset($value['templateVersion']) ? $value['templateVersion'] : $faker->randomDigit(1);
        
        $task->setTemplate($template);
        $task->setTemplateVersion($templateVersion);
    }

    protected static function generateRule($task, $faker, $value) : void
    {
        $rule = isset($value['rule']) ? $value['rule'] : $faker->randomDigit(1);
        $ruleVersion = isset($value['ruleVersion']) ? $value['ruleVersion'] : $faker->randomDigit(1);
        
        $task->setRule($rule);
        $task->setRuleVersion($ruleVersion);
    }

    protected static function generateErrorNumber($task, $faker, $value) : void
    {
        $errorNumber = isset($value['errorNumber']) ? $value['errorNumber'] : $faker->randomDigit(1);
        
        $task->setErrorNumber($errorNumber);
    }
    
    protected static function generateFileName($task, $faker, $value) : void
    {
        $fileName = isset($value['fileName']) ? $value['fileName'] : $faker->title().'xls';
        
        $task->setFileName($fileName);
    }

    protected static function generateStatus($task, $faker, $value) : void
    {
        $status = isset($value['status']) ? $value['status'] :  $faker->randomElement(Task::STATUS);
        
        $task->setStatus($status);
    }
}
