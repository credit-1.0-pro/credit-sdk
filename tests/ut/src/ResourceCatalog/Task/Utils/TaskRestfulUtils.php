<?php
namespace Sdk\ResourceCatalog\Task\Utils;

trait TaskRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $task
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $task->getId());
        }
        
        $attributes = $expectedArray['data']['attributes'];
        $this->assertEquals($attributes['administrativeArea'], $task->getAdministrativeArea());
        $this->assertEquals($attributes['total'], $task->getTotal());
        $this->assertEquals($attributes['successNumber'], $task->getSuccessNumber());
        $this->assertEquals($attributes['failureNumber'], $task->getFailureNumber());
        $this->assertEquals($attributes['template'], $task->getTemplate());
        $this->assertEquals($attributes['templateVersion'], $task->getTemplateVersion());
        $this->assertEquals($attributes['rule'], $task->getRule());
        $this->assertEquals($attributes['ruleVersion'], $task->getRuleVersion());
        $this->assertEquals($attributes['errorNumber'], $task->getErrorNumber());
        $this->assertEquals($attributes['fileName'], $task->getFileName());
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $task->getCreateTime());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $task->getStatusTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $task->getUpdateTime());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $task->getStatus());
        }

        $this->compareCrew($expectedArray, $task);
        $this->comparePublishUserGroup($expectedArray, $task);
    }

    private function compareCrew(array $expectedArray, $task)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['crew']['data'])) {
                $this->assertEquals(
                    $relationships['crew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['crew']['data'][0]['id'],
                    $task->getCrew()->getId()
                );
            }
        }
    }

    private function comparePublishUserGroup(array $expectedArray, $task)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['publishUserGroup']['data'])) {
                $this->assertEquals(
                    $relationships['publishUserGroup']['data'][0]['type'],
                    'userGroups'
                );
                $this->assertEquals(
                    $relationships['publishUserGroup']['data'][0]['id'],
                    $task->getUserGroup()->getId()
                );
            }
        }
    }
}
