<?php
namespace Sdk\ResourceCatalog\Task\Translator;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Task\Utils\TaskRestfulUtils;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

class TaskRestfulTranslatorTest extends TestCase
{
    use TaskRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new TaskRestfulTranslator();

        Core::$container->set('crew', new Crew());
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $translator = new MockTaskRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator',
            $translator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $translator = new MockTaskRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\User\Crew\Translator\CrewRestfulTranslator',
            $translator->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $task = \Sdk\ResourceCatalog\Task\Utils\MockFactory::generateTask(1);

        $expression['data']['id'] = $task->getId();
        $expression['data']['attributes']['administrativeArea'] = $task->getAdministrativeArea();
        $expression['data']['attributes']['total'] = $task->getTotal();
        $expression['data']['attributes']['successNumber'] = $task->getSuccessNumber();
        $expression['data']['attributes']['failureNumber'] = $task->getFailureNumber();
        $expression['data']['attributes']['template'] = $task->getTemplate();
        $expression['data']['attributes']['templateVersion'] = $task->getTemplateVersion();
        $expression['data']['attributes']['rule'] = $task->getRule();
        $expression['data']['attributes']['ruleVersion'] = $task->getRuleVersion();
        $expression['data']['attributes']['errorNumber'] = $task->getErrorNumber();
        $expression['data']['attributes']['fileName'] = $task->getFileName();
        $expression['data']['attributes']['createTime'] = $task->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $task->getUpdateTime();
        $expression['data']['attributes']['status'] = $task->getStatus();
        $expression['data']['attributes']['statusTime'] = $task->getStatusTime();

        $taskObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Task\Model\Task', $taskObject);
        $this->compareArrayAndObjectCommon($expression, $taskObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $task = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Task\Model\NullTask', $task);
    }

    public function testObjectToArray()
    {
        $task = null;

        $expression = $this->translator->objectToArray($task);
        $this->assertEquals(array(), $expression);
    }

    public function testArrayToObjectWithIncluded()
    {
        $translator = $this->getMockBuilder(MockTaskRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getCrewRestfulTranslator',
                        'getUserGroupRestfulTranslator'
                    ])
                    ->getMock();


        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup']
        ];

        $crewInfo = ['mockCrewInfo'];
        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];

        $crew = new Crew();
        $publishUserGroup = new UserGroup();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(2))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['publishUserGroup']['data']]
             ) ->will($this->onConsecutiveCalls(
                 $crewInfo,
                 $publishUserGroupInfo
             ));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);
                                    
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($publishUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishUserGroup);

        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        //揭示
        $task = $translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Task\Model\Task', $task);
        $this->assertEquals($crew, $task->getCrew());
        $this->assertEquals($publishUserGroup, $task->getUserGroup());
    }
}
