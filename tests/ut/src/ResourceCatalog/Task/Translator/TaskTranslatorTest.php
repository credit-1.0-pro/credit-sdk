<?php
namespace Sdk\ResourceCatalog\Task\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Task\Utils\TaskUtils;

class TaskTranslatorTest extends TestCase
{
    use TaskUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new TaskTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\ResourceCatalog\Task\Model\NullTask', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $task = \Sdk\ResourceCatalog\Task\Utils\MockFactory::generateTask(1);

        $expression = $this->translator->objectToArray($task);
    
        $this->compareArrayAndObjectTask($expression, $task);
    }

    public function testObjectToArrayFail()
    {
        $task = null;

        $expression = $this->translator->objectToArray($task);
        $this->assertEquals(array(), $expression);
    }
}
