<?php
namespace Sdk\ResourceCatalog\Rule\Utils;

trait RuleRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $rule
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $rule->getId());
        }
        
        if (isset($expectedArray['data']['attributes']['versionDescription'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['versionDescription'],
                $rule->getVersionDescription()
            );
        }
        $this->assertEquals($expectedArray['data']['attributes']['rules'], $rule->getRules());

        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $rule->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $rule->getStatusTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $rule->getUpdateTime());
        }
        if (isset($expectedArray['data']['attributes']['status'])) {
            $this->assertEquals($expectedArray['data']['attributes']['status'], $rule->getStatus());
        }

        $this->compareCrew($expectedArray, $rule);
        $this->comparePublishUserGroup($expectedArray, $rule);
        $this->compareRuleVersion($expectedArray, $rule);
        $this->compareTemplate($expectedArray, $rule);
        $this->compareTemplateVersion($expectedArray, $rule);
    }

    private function compareCrew(array $expectedArray, $rule)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['crew']['data'])) {
                $this->assertEquals(
                    $relationships['crew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['crew']['data'][0]['id'],
                    $rule->getCrew()->getId()
                );
            }
        }
    }

    private function comparePublishUserGroup(array $expectedArray, $rule)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['publishUserGroup']['data'])) {
                $this->assertEquals(
                    $relationships['publishUserGroup']['data'][0]['type'],
                    'userGroups'
                );
                $this->assertEquals(
                    $relationships['publishUserGroup']['data'][0]['id'],
                    $rule->getUserGroup()->getId()
                );
            }
        }
    }

    private function compareRuleVersion(array $expectedArray, $rule)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['ruleVersion']['data'])) {
                $this->assertEquals(
                    $relationships['ruleVersion']['data'][0]['type'],
                    'ruleVersions'
                );
                $this->assertEquals(
                    $relationships['ruleVersion']['data'][0]['id'],
                    $rule->getRuleVersion()->getId()
                );
            }
        }
    }

    private function compareTemplateVersion(array $expectedArray, $rule)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['templateVersion']['data'])) {
                $this->assertEquals(
                    $relationships['templateVersion']['data'][0]['type'],
                    'templateVersions'
                );
                $this->assertEquals(
                    $relationships['templateVersion']['data'][0]['id'],
                    $rule->getTemplateVersion()->getId()
                );
            }
        }
    }

    private function compareTemplate(array $expectedArray, $rule)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['template']['data'])) {
                $this->assertEquals(
                    $relationships['template']['data'][0]['type'],
                    'templates'
                );
                $this->assertEquals(
                    $relationships['template']['data'][0]['id'],
                    $rule->getTemplate()->getId()
                );
            }
        }
    }

    private function compareArrayAndObjectCommonUnAuditRule(
        array $expectedArray,
        $unAuditedRule
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditedRule);
        
        if (isset($expectedArray['data']['attributes']['applyStatus'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['applyStatus'],
                $unAuditedRule->getApplyStatus()
            );
        }
        if (isset($expectedArray['data']['attributes']['operationType'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['operationType'],
                $unAuditedRule->getOperationType()
            );
        }
        if (isset($expectedArray['data']['attributes']['applyInfoType'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['applyInfoType'],
                $unAuditedRule->getApplyInfoType()
            );
        }
        if (isset($expectedArray['data']['attributes']['relationId'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['relationId'],
                $unAuditedRule->getRelationId()
            );
        }

        $this->assertEquals(
            $expectedArray['data']['attributes']['rejectReason'],
            $unAuditedRule->getRejectReason()
        );

        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['applyCrew']['data'])) {
                $this->assertEquals(
                    $relationships['applyCrew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['applyCrew']['data'][0]['id'],
                    $unAuditedRule->getApplyCrew()->getId()
                );
            }
        }
    }

    private function compareArrayAndObjectRuleVersion(
        array $expectedArray,
        $ruleVersion
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $ruleVersion->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['number'], $ruleVersion->getNumber());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $ruleVersion->getDescription());
        $this->assertEquals($expectedArray['data']['attributes']['ruleId'], $ruleVersion->getRuleId());
        $this->assertEquals($expectedArray['data']['attributes']['info'], $ruleVersion->getInfo());
       
        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $ruleVersion->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $ruleVersion->getStatusTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $ruleVersion->getUpdateTime());
        }
        if (isset($expectedArray['data']['attributes']['status'])) {
            $this->assertEquals($expectedArray['data']['attributes']['status'], $ruleVersion->getStatus());
        }

        $this->compareCrew($expectedArray, $ruleVersion);
        $this->comparePublishUserGroup($expectedArray, $ruleVersion);
        $this->compareTemplate($expectedArray, $ruleVersion);
        $this->compareTemplateVersion($expectedArray, $ruleVersion);
    }
}
