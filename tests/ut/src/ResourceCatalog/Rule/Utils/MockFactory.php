<?php
namespace Sdk\ResourceCatalog\Rule\Utils;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Model\RuleVersion;
use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;

use Sdk\Common\Model\IApplyAble;

class MockFactory
{
    public static function generateCommon(
        Rule $rule,
        int $seed = 0,
        array $value = array()
    ) {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        //versionDescription
        self::generateVersionDescription($rule, $faker, $value);
        //template,templateVersion
        self::generateTemplateAndTemplateVersion($rule, $faker, $value);
        //rules
        self::generateRules($rule, $faker, $value);
        //ruleVersion
        self::generateRuleVersion(1);
        //crew,publishUserGroup
        self::generateCrewAndPublishUserGroup($rule, $faker, $value);
        //status
        self::generateStatus($rule, $faker, $value);
        $rule->setCreateTime($faker->unixTime());
        $rule->setUpdateTime($faker->unixTime());
        $rule->setStatusTime($faker->unixTime());

        return $rule;
    }

    protected static function generateVersionDescription($rule, $faker, $value) : void
    {
        $description = isset($value['description']) ?
            $value['description'] :
            $faker->sentence;
        
        $rule->setVersionDescription($description);
    }

    protected static function generateTemplateAndTemplateVersion($rule, $faker, $value) : void
    {
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate($faker->randomDigit());
        $template = isset($value['template']) ? $value['template'] : $template;
        
        $rule->setTemplate($template);
        $rule->setTemplateVersion($template->getTemplateVersion());
    }

    protected static function generateRules($rule, $faker, $value) : void
    {
        unset($faker);
        $rules = array(
            'completionRule' =>  array(
                'ZTMC' => array(
                    array('id'=>1, 'versionId'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                    array('id'=>2, 'versionId'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                ),
                'TYSHXYDM' => array(
                    array('id'=>1, 'versionId'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                    array('id'=>2, 'versionId'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                ),
            ),
            'comparisonRule' =>  array(
                'ZTMC' => array(
                    array('id'=>1, 'versionId'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                    array('id'=>2, 'versionId'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                ),
                'TYSHXYDM' => array(
                    array('id'=>1, 'versionId'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                    array('id'=>2, 'versionId'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                ),
            ),
            'deDuplicationRule' => array("result"=>1, "items"=>array("ZTMC", "TYSHXYDM")),
        );

        $rules = isset($value['rules']) ? $value['rules'] : $rules;

        $rule->setRules($rules);
    }

    protected static function generateCrewAndPublishUserGroup($rule, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Sdk\User\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $rule->setCrew($crew);
        $rule->setUserGroup($crew->getUserGroup());
    }

    protected static function generateStatus($rule, $faker, $value) : void
    {
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(
            Rule::STATUS
        );

        $rule->setStatus($status);
    }

    public static function generateRule(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Rule {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $rule = new Rule($id);
        $rule->setId($id);

        $rule = self::generateCommon($rule, $seed, $value);

        return $rule;
    }

    public static function generateUnAuditRule(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditRule {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditRule = new UnAuditRule($id);
        
        $unAuditRule->setId($id);

        $unAuditRule = self::generateCommon($unAuditRule, $seed, $value);

        $unAuditRule->setRelationId($id);
        $unAuditRule->setApplyUserGroup($unAuditRule->getUserGroup());
        $unAuditRule->setPublishCrew($unAuditRule->getCrew());
        $unAuditRule->setApplyCrew($unAuditRule->getCrew());
         //rejectReason
        $rejectReason = isset($value['rejectReason']) ? $value['rejectReason'] : '内容不合理';
        $unAuditRule->setRejectReason($rejectReason);

        //applyStatus
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : $faker->randomElement(
            IApplyAble::APPLY_STATUS
        );
        $unAuditRule->setApplyStatus($applyStatus);

        //operationType
        $operationType = isset($value['operationType']) ? $value['operationType'] : $faker->randomElement(
            IApplyAble::OPERATION_TYPE
        );
        $unAuditRule->setOperationType($operationType);
        
        return $unAuditRule;
    }

    public static function generateRuleVersion(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : RuleVersion {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $ruleVersion = new RuleVersion($id);

        $ruleVersion->setCreateTime($faker->unixTime());
        $ruleVersion->setUpdateTime($faker->unixTime());
        $ruleVersion->setStatusTime($faker->unixTime());

        //number
        $number = isset($value['number']) ? $value['number'] : date(
            'ymd',
            $ruleVersion->getCreateTime()
        ).rand(0000, 9999);
        $ruleVersion->setNumber($number);
        //description
        $description = isset($value['description']) ? $value['description'] : $faker->sentence();
        $ruleVersion->setDescription($description);
        //ruleId
        $ruleId = isset($value['ruleId']) ? $value['ruleId'] : $faker->randomDigit();
        $ruleVersion->setRuleId($ruleId);
        //info
        $info = isset($value['info']) ? $value['info'] : array('info');
        $ruleVersion->setInfo($info);
        //crew
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Sdk\User\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $ruleVersion->setCrew($crew);
        $ruleVersion->setUserGroup($crew->getUserGroup());

        //template
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate($faker->randomDigit());
        
        $ruleVersion->setTemplate($template);
        $ruleVersion->setTemplateVersion($template->getTemplateVersion());

        //status
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(
            RuleVersion::STATUS
        );
        $ruleVersion->setStatus($status);

        return $ruleVersion;
    }
}
