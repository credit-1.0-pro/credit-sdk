<?php
namespace Sdk\ResourceCatalog\Rule\Utils;

use Sdk\Common\Model\IApplyAble;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Model\RuleVersion;
use Sdk\ResourceCatalog\Rule\Translator\RuleTranslatorTrait;
use Sdk\ResourceCatalog\Rule\Translator\RuleVersionTranslator;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

use Sdk\ResourceCatalog\Template\Translator\TemplateTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionTranslator;

trait RuleUtils
{
    use RuleTranslatorTrait;

    protected function getTemplateTranslator() : TemplateTranslator
    {
        return new TemplateTranslator();
    }

    protected function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return new TemplateVersionTranslator();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getRuleVersionTranslator() : RuleVersionTranslator
    {
        return new RuleVersionTranslator();
    }

    private function compareArrayAndObjectRule(
        array $expectedArray,
        $rule
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $rule);
    }

    private function compareArrayAndObjectUnAuditedRule(
        array $expectedArray,
        $unAuditedRule
    ) {
        $this->assertEquals($expectedArray['rejectReason'], $unAuditedRule->getRejectReason());
        $this->assertEquals($expectedArray['relationId'], marmot_encode($unAuditedRule->getRelationId()));
        $this->assertEquals(
            $expectedArray['applyStatus']['id'],
            marmot_encode($unAuditedRule->getApplyStatus())
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['name'],
            IApplyAble::APPLY_STATUS_CN[$unAuditedRule->getApplyStatus()]
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['type'],
            IApplyAble::APPLY_STATUS_TAG_TYPE[$unAuditedRule->getApplyStatus()]
        );
        $this->assertEquals(
            $expectedArray['operationType']['id'],
            marmot_encode($unAuditedRule->getOperationType())
        );
        $this->assertEquals(
            $expectedArray['operationType']['name'],
            IApplyAble::OPERATION_TYPE_CN[$unAuditedRule->getOperationType()]
        );
        $this->assertEquals(
            $expectedArray['publishCrew'],
            $this->getCrewTranslator()->objectToArray($unAuditedRule->getPublishCrew())
        );
        $this->assertEquals(
            $expectedArray['applyCrew'],
            $this->getCrewTranslator()->objectToArray($unAuditedRule->getApplyCrew())
        );
        $this->assertEquals(
            $expectedArray['applyUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($unAuditedRule->getApplyUserGroup())
        );
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $rule
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($rule->getId()));
        $this->rulesEquals($expectedArray, $rule);

        $this->assertEquals(
            $expectedArray['crew'],
            $this->getCrewTranslator()->objectToArray($rule->getCrew())
        );
        $this->assertEquals(
            $expectedArray['publishUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($rule->getUserGroup())
        );
        $this->assertEquals(
            $expectedArray['ruleVersion'],
            $this->getRuleVersionTranslator()->objectToArray($rule->getRuleVersion())
        );
        $this->assertEquals(
            $expectedArray['templateVersion'],
            $this->getTemplateVersionTranslator()->objectToArray($rule->getTemplateVersion())
        );
        $this->assertEquals(
            $expectedArray['template'],
            $this->getTemplateTranslator()->objectToArray($rule->getTemplate())
        );
        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($rule->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            Rule::STATUS_CN[$rule->getStatus()]
        );
        $this->assertEquals($expectedArray['updateTime'], $rule->getUpdateTime());
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $rule->getUpdateTime())
        );
                    
        $this->assertEquals($expectedArray['statusTime'], $rule->getStatusTime());
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $rule->getStatusTime())
        );
                    
        $this->assertEquals($expectedArray['createTime'], $rule->getCreateTime());
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $rule->getCreateTime())
        );
    }

    private function rulesEquals($expectedArray, $rule)
    {
        $rules = array();

        if (is_string($expectedArray['rules'])) {
            $rules = json_decode($expectedArray['rules'], true);
        }
        if (is_array($expectedArray['rules'])) {
            $rules = $expectedArray['rules'];
        }

        $this->assertEquals($rules, $this->getRulesFormatConversion($rule->getRules()));
    }

    private function compareArrayAndObjectRuleVersion(
        array $expectedArray,
        $ruleVersion
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($ruleVersion->getId()));
        $this->assertEquals($expectedArray['number'], $ruleVersion->getNumber());
        $this->assertEquals($expectedArray['description'], $ruleVersion->getDescription());
        $this->assertEquals($expectedArray['ruleId'], marmot_encode($ruleVersion->getRuleId()));
        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($ruleVersion->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            RuleVersion::STATUS_CN[$ruleVersion->getStatus()]
        );
        $this->infoEquals($expectedArray, $ruleVersion);

        $this->assertEquals(
            $expectedArray['templateVersion'],
            $this->getTemplateVersionTranslator()->objectToArray($ruleVersion->getTemplateVersion())
        );
        $this->assertEquals(
            $expectedArray['template'],
            $this->getTemplateTranslator()->objectToArray($ruleVersion->getTemplate())
        );
        $this->assertEquals(
            $expectedArray['crew'],
            $this->getCrewTranslator()->objectToArray($ruleVersion->getCrew())
        );
        $this->assertEquals(
            $expectedArray['publishUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($ruleVersion->getUserGroup())
        );
        $this->assertEquals($expectedArray['updateTime'], $ruleVersion->getUpdateTime());
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $ruleVersion->getUpdateTime())
        );
                    
        $this->assertEquals($expectedArray['statusTime'], $ruleVersion->getStatusTime());
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $ruleVersion->getStatusTime())
        );
                    
        $this->assertEquals($expectedArray['createTime'], $ruleVersion->getCreateTime());
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $ruleVersion->getCreateTime())
        );
    }

    private function infoEquals($expectedArray, $ruleVersion)
    {
        $info = array();

        if (is_string($expectedArray['info'])) {
            $info = json_decode($expectedArray['info'], true);
        }
        if (is_array($expectedArray['info'])) {
            $info = $expectedArray['info'];
        }

        $this->assertEquals($info, $this->getInfoCn($ruleVersion->getInfo()));
    }
}
