<?php
namespace Sdk\ResourceCatalog\Rule\WidgetRules;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Sdk\Common\Utils\StringGenerate;

use Sdk\ResourceCatalog\Rule\Model\Rule;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleWidgetRulesTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new MockRuleWidgetRules();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //versionDescription -- start
    /**
     * @dataProvider invalidVersionDescriptionProvider
     */
    public function testVersionDescriptionInvalid($actual, $expected)
    {
        $result = $this->widgetRule->versionDescription($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(RULE_VERSION_DESCRIPTION_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidVersionDescriptionProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(RuleWidgetRules::VERSION_DESCRIPTION_MIN_LENGTH-1), false),
            array(StringGenerate::generate(RuleWidgetRules::VERSION_DESCRIPTION_MAX_LENGTH+1), false),
            array(StringGenerate::generate(RuleWidgetRules::VERSION_DESCRIPTION_MIN_LENGTH), true),
            array(StringGenerate::generate(RuleWidgetRules::VERSION_DESCRIPTION_MIN_LENGTH+1), true),
            array(StringGenerate::generate(RuleWidgetRules::VERSION_DESCRIPTION_MAX_LENGTH), true),
            array(StringGenerate::generate(RuleWidgetRules::VERSION_DESCRIPTION_MAX_LENGTH-1), true)
        );
    }
    //versionDescription -- end
    //rule
    public function testRulesSuccess()
    {
        $rules = ['ruleName'=>['rules']];

        $this->widgetRule = $this->getMockBuilder(MockRuleWidgetRules::class)
            ->setMethods([
                'ruleName',
                'ruleFormat',
                'validateRule'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                   ->method('ruleName')
                   ->willReturn(true);

        $this->widgetRule->expects($this->once())
                   ->method('ruleFormat')
                   ->willReturn(true);

        $this->widgetRule->expects($this->once())
                   ->method('validateRule')
                   ->with('ruleName', ['rules'])
                   ->willReturn(true);

        $result = $this->widgetRule->rules($rules);
        $this->assertTrue($result);
    }

    public function testRulesFailNotArray()
    {
        $rules = 'rules';
        $result = $this->widgetRule->rules($rules);
        $this->assertFalse($result);
        $this->assertEquals(RULE_RULES_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function testRulesFail()
    {
        $rules = ['ruleName'=>['rules']];

        $this->widgetRule = $this->getMockBuilder(MockRuleWidgetRules::class)
            ->setMethods([
                'ruleName',
                'ruleFormat',
                'validateRule'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                   ->method('ruleName')
                   ->willReturn(false);

        $this->widgetRule->expects($this->exactly(0))
                   ->method('ruleFormat');

        $this->widgetRule->expects($this->exactly(0))
                   ->method('validateRule')
                   ->with('ruleName', ['rules']);

        $result = $this->widgetRule->rules($rules);
        $this->assertFalse($result);
    }

    //validateRule
    public function testValidateRuleCompletionRules()
    {
        $name = 'completionRule';
        $rule = ['rule'];

        $this->widgetRule = $this->getMockBuilder(MockRuleWidgetRules::class)
            ->setMethods([
                'validateCompletionRules'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                   ->method('validateCompletionRules')
                   ->with($rule)
                   ->willReturn(true);

        $result = $this->widgetRule->validateRule($name, $rule);
        $this->assertTrue($result);
    }

    public function testValidateRuleComparisonRules()
    {
        $name = 'comparisonRule';
        $rule = ['rule'];

        $this->widgetRule = $this->getMockBuilder(MockRuleWidgetRules::class)
            ->setMethods([
                'validateComparisonRules'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                   ->method('validateComparisonRules')
                   ->with($rule)
                   ->willReturn(true);

        $result = $this->widgetRule->validateRule($name, $rule);
        $this->assertTrue($result);
    }

    public function testValidateDeDuplicationRules()
    {
        $name = 'deDuplicationRule';
        $rule = ['rule'];

        $this->widgetRule = $this->getMockBuilder(MockRuleWidgetRules::class)
            ->setMethods([
                'validateDeDuplicationRules'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                   ->method('validateDeDuplicationRules')
                   ->with($rule)
                   ->willReturn(true);

        $result = $this->widgetRule->validateRule($name, $rule);
        $this->assertTrue($result);
    }
    public function testValidateRuleFalse()
    {
        $name = 'name';
        $rule = ['rule'];

        $result = $this->widgetRule->validateRule($name, $rule);
        $this->assertFalse($result);
    }

    //ruleName
    public function testRuleNameSuccess()
    {
        $name = 'deDuplicationRule';
        $result = $this->widgetRule->ruleName($name);
        $this->assertTrue($result);
    }

    public function testRuleNameFail()
    {
        $name = 'name';
        $result = $this->widgetRule->ruleName($name);
        $this->assertFalse($result);
        $this->assertEquals(RULE_RULES_NAME_NOT_EXIT, Core::getLastError()->getId());
    }

    //ruleFormat
    public function testRuleFormatSuccess()
    {
        $rule = ['rule'];
        $result = $this->widgetRule->ruleFormat($rule);
        $this->assertTrue($result);
    }

    public function testRuleFormatFail()
    {
        $rule = 'rule';
        $result = $this->widgetRule->ruleFormat($rule);
        $this->assertFalse($result);
        $this->assertEquals(RULE_RULES_FORMAT_ERROR, Core::getLastError()->getId());
    }

    //validateCompletionAndComparisonSuccess
    private function validateCompletionAndComparisonSuccess($function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];
        $result = $this->widgetRule->$function($rules);
        $this->assertTrue($result);
    }

    public function testValidateCompletionRulesSuccess()
    {
        $this->validateCompletionAndComparisonSuccess('validateCompletionRules');
    }

    public function testValidateComparisonRulesSuccess()
    {
        $this->validateCompletionAndComparisonSuccess('validateComparisonRules');
    }

    private function prepareCompletionAndComparisonRulesFail($rules, $errorId, $function)
    {
        $result = $this->widgetRule->$function($rules);
        $this->assertFalse($result);
        $this->assertEquals($errorId, Core::getLastError()->getId());
    }

    //validateCompletionAndComparisonRulesFailNotArray
    private function validateCompletionAndComparisonRulesFailNotArray($errorId, $function)
    {
        $rules = [
        'ZTMC' => 'rule',
        'TYSHXYDM' => array(
            array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
            array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
        )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $errorId, $function);
    }

    public function testValidateCompletionRulesFailNotArray()
    {
        $this->validateCompletionAndComparisonRulesFailNotArray(
            RULE_COMPLETION_RULES_FORMAT_ERROR,
            'validateCompletionRules'
        );
    }

    public function testValidateComparisonRulesFailNotArray()
    {
        $this->validateCompletionAndComparisonRulesFailNotArray(
            RULE_COMPARISON_RULES_FORMAT_ERROR,
            'validateComparisonRules'
        );
    }

    //validateCompletionAndComparisonRulesFailMaxRules
    private function validateCompletionAndComparisonRulesFailMaxRules($errorId, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                array('id'=>3, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            ),
            'TEST' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $errorId, $function);
    }

    public function testValidateCompletionRulesFailMaxRules()
    {
        $this->validateCompletionAndComparisonRulesFailMaxRules(
            RULE_COMPLETION_RULES_COUNT_ERROR,
            'validateCompletionRules'
        );
    }

    public function testValidateComparisonRulesFailMaxRules()
    {
        $this->validateCompletionAndComparisonRulesFailMaxRules(
            RULE_COMPARISON_RULES_COUNT_ERROR,
            'validateComparisonRules'
        );
    }

    //validateCompletionAndComparisonRulesFailSetKey
    private function validateCompletionAndComparisonRulesFailSetKey($errorId, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $errorId, $function);
    }

    public function testValidateCompletionRulesFailNotSetKey()
    {
        $this->validateCompletionAndComparisonRulesFailSetKey(
            RULE_COMPLETION_RULES_FORMAT_ERROR,
            'validateCompletionRules'
        );
    }

    public function testValidateComparisonRulesFailNotSetKey()
    {
        $this->validateCompletionAndComparisonRulesFailSetKey(
            RULE_COMPARISON_RULES_FORMAT_ERROR,
            'validateComparisonRules'
        );
    }

    //validateCompletionAndComparisonRulesFailIdNotNumber
    private function validateCompletionAndComparisonRulesFailIdNotNumber($errorId, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>'id', 'base'=> array(1,2), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $errorId, $function);
    }

    public function testValidateCompletionRulesFailIdNotNumber()
    {
        $this->validateCompletionAndComparisonRulesFailIdNotNumber(
            RULE_COMPLETION_RULES_TEMPLATE_FORMAT_ERROR,
            'validateCompletionRules'
        );
    }

    public function testValidateComparisonRulesFailIdNotNumber()
    {
        $this->validateCompletionAndComparisonRulesFailIdNotNumber(
            RULE_COMPARISON_RULES_TEMPLATE_FORMAT_ERROR,
            'validateComparisonRules'
        );
    }

    //validateCompletionAndComparisonRulesFailIdBaseNotArray
    private function validateCompletionAndComparisonRulesFailIdBaseNotArray($errorId, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> 'base', 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $errorId, $function);
    }

    public function testValidateCompletionRulesFailBaseNotArray()
    {
        $this->validateCompletionAndComparisonRulesFailIdBaseNotArray(
            RULE_COMPLETION_RULES_BASE_FORMAT_ERROR,
            'validateCompletionRules'
        );
    }

    public function testValidateComparisonRulesFailBaseNotArray()
    {
        $this->validateCompletionAndComparisonRulesFailIdBaseNotArray(
            RULE_COMPARISON_RULES_BASE_FORMAT_ERROR,
            'validateComparisonRules'
        );
    }

    //validateCompletionAndComparisonRulesFailIdItemNotString
    private function validateCompletionAndComparisonRulesFailIdItemNotString($errorId, $function)
    {
        $rules = [
           'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>['ZTMC']),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $errorId, $function);
    }

    public function testValidateCompletionRulesFailItemNotString()
    {
        $this->validateCompletionAndComparisonRulesFailIdItemNotString(
            RULE_COMPLETION_RULES_TEMPLATE_ITEM_FORMAT_ERROR,
            'validateCompletionRules'
        );
    }

    public function testValidateComparisonRulesFailItemNotString()
    {
         $this->validateCompletionAndComparisonRulesFailIdItemNotString(
             RULE_COMPARISON_RULES_TEMPLATE_ITEM_FORMAT_ERROR,
             'validateComparisonRules'
         );
    }

    //validateCompletionAndComparisonRulesFailBaseOutOfRange
    private function validateCompletionAndComparisonRulesFailBaseOutOfRange($errorId, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,3), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $errorId, $function);
    }

    public function testValidateCompletionRulesFailBaseOutOfRange()
    {
        $this->validateCompletionAndComparisonRulesFailBaseOutOfRange(
            RULE_COMPLETION_RULES_BASE_FORMAT_ERROR,
            'validateCompletionRules'
        );
    }

    public function testValidateComparisonRulesFailBaseOutOfRange()
    {
        $this->validateCompletionAndComparisonRulesFailBaseOutOfRange(
            RULE_COMPARISON_RULES_BASE_FORMAT_ERROR,
            'validateComparisonRules'
        );
    }

    //validateDeDuplicationRules
    public function testValidateDeDuplicationRulesSuccess()
    {
        $rules = [
             'deDuplicationRule' => array("result"=>1, "items"=>array("ZTMC", "TYSHXYDM"))
        ];

        $result = $this->widgetRule->validateDeDuplicationRules($rules['deDuplicationRule']);
        $this->assertTrue($result);
    }

    public function testValidateDeDuplicationRulesFailNotSetResult()
    {
        $rules = [
             'deDuplicationRule' => array("items"=>array("ZTMC", "TYSHXYDM"))
        ];

        $result = $this->widgetRule->validateDeDuplicationRules($rules['deDuplicationRule']);
        $this->assertFalse($result);
        $this->assertEquals(RULE_DE_DUPLICATION_RULES_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function testValidateDeDuplicationRulesFailItemsNotArray()
    {
        $rules = [
             'deDuplicationRule' => array("result"=>1, "items"=>"items")
        ];

        $result = $this->widgetRule->validateDeDuplicationRules($rules['deDuplicationRule']);
        $this->assertFalse($result);
        $this->assertEquals(RULE_DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function testValidateDeDuplicationRulesFailResultOtOfRange()
    {
        $rules = [
             'deDuplicationRule' => array("result"=>3, "items"=>array("ZTMC", "TYSHXYDM"))
        ];

        $result = $this->widgetRule->validateDeDuplicationRules($rules['deDuplicationRule']);
        $this->assertFalse($result);
        $this->assertEquals(RULE_DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR, Core::getLastError()->getId());
    }
}
