<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Common\Model\IApplyAble;

use Sdk\ResourceCatalog\Rule\Repository\UnAuditRuleRepository;

class UnAuditRuleTest extends TestCase
{
    private $unAuditRule;

    public function setUp()
    {
        $this->unAuditRule = new MockUnAuditRule();
    }

    public function tearDown()
    {
        unset($this->unAuditRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals('', $this->unAuditRule->getRejectReason());
        $this->assertEquals(0, $this->unAuditRule->getApplyInfoType());
        $this->assertEquals(0, $this->unAuditRule->getRelationId());
        $this->assertInstanceOf('Sdk\User\Crew\Model\Crew', $this->unAuditRule->getPublishCrew());
        $this->assertInstanceOf('Sdk\User\Crew\Model\Crew', $this->unAuditRule->getApplyCrew());
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Model\UserGroup',
            $this->unAuditRule->getPublishUserGroup()
        );
        $this->assertInstanceOf('Sdk\UserGroup\UserGroup\Model\UserGroup', $this->unAuditRule->getApplyUserGroup());
        $this->assertEquals(IApplyAble::OPERATION_TYPE['NULL'], $this->unAuditRule->getOperationType());
        $this->assertEquals(IApplyAble::APPLY_STATUS['PENDING'], $this->unAuditRule->getApplyStatus());
    }

    public function testGetUnAuditRuleRepository()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Repository\UnAuditRuleRepository',
            $this->unAuditRule->getUnAuditRuleRepository()
        );
    }

    public function testGetIApplyAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Repository\UnAuditRuleRepository',
            $this->unAuditRule->getIApplyAbleAdapter()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Repository\UnAuditRuleRepository',
            $this->unAuditRule->getIOperateAbleAdapter()
        );
    }
    //isPending -- 开始
    public function testIsPendingTrue()
    {
        $this->unAuditRule->setApplyStatus(IApplyAble::APPLY_STATUS['PENDING']);
        $result = $this->unAuditRule->isPending();
        $this->assertTrue($result);
    }
 
    public function testIsPendingFail()
    {
        $this->unAuditRule->setApplyStatus(IApplyAble::APPLY_STATUS['REVOKED']);
        $result = $this->unAuditRule->isPending();
        $this->assertFalse($result);
    }
     //isPending -- 结束

    public function testRevoke()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditRule::class)
            ->setMethods(['getUnAuditRuleRepository', 'isPending'])->getMock();

        $this->stub->expects($this->exactly(1))->method('isPending')->willReturn(true);

        $repository = $this->prophesize(UnAuditRuleRepository::class);
        $repository->revoke(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))->method('getUnAuditRuleRepository')->willReturn($repository->reveal());

        $result = $this->stub->revoke();
        $this->assertTrue($result);
    }

    public function testRevokeFail()
    {
        $this->stub = $this->getMockBuilder(MockUnAuditRule::class)
            ->setMethods(['isPending'])->getMock();

        $this->stub->expects($this->exactly(1))->method('isPending')->willReturn(false);

        $result = $this->stub->revoke();
        $this->assertFalse($result);
        $this->assertEquals(APPLY_STATUE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }
}
