<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullRuleTest extends TestCase
{
    private $nullRule;

    public function setUp()
    {
        $this->nullRule = NullRule::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsRule()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\Rule',
            $this->nullRule
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullRule
        );
    }

    public function testResourceNotExist()
    {
        $nullRule = new MockNullRule();

        $result = $nullRule->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testVersionRestore()
    {
        $nullRule = new MockNullRule();

        $result = $nullRule->versionRestore();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testDelete()
    {
        $nullRule = new MockNullRule();

        $result = $nullRule->delete();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
