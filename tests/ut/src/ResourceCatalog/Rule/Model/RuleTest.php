<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;

use Sdk\ResourceCatalog\Rule\Repository\RuleRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class RuleTest extends TestCase
{
    private $rule;

    public function setUp()
    {
        $this->rule = new MockRule();
    }

    public function tearDown()
    {
        unset($this->rule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testRuleConstructor()
    {
        $this->assertEquals(0, $this->rule->getId());
        $this->assertEquals('', $this->rule->getVersionDescription());
        $this->assertEquals(array(), $this->rule->getRules());
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\TemplateVersion',
            $this->rule->getTemplateVersion()
        );

        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\Template',
            $this->rule->getTemplate()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\RuleVersion',
            $this->rule->getRuleVersion()
        );
        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Crew',
            $this->rule->getCrew()
        );
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Model\UserGroup',
            $this->rule->getUserGroup()
        );
        $this->assertEquals(Rule::STATUS['NORMAL'], $this->rule->getStatus());
        $this->assertEquals(0, $this->rule->getUpdateTime());
        $this->assertEquals(0, $this->rule->getCreateTime());
        $this->assertEquals(0, $this->rule->getStatusTime());
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Rule setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->rule->setId(1);
        $this->assertEquals(1, $this->rule->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //versionDescription 测试 ----------------------------------------------------- start
    /**
     * 设置 Rule setVersionDescription() 正确的传参类型,期望传值正确
     */
    public function testSetVersionDescriptionCorrectType()
    {
        $this->rule->setVersionDescription('string');
        $this->assertEquals('string', $this->rule->getVersionDescription());
    }

    /**
     * 设置 Rule setVersionDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVersionDescriptionWrongType()
    {
        $this->rule->setVersionDescription(array(1, 2, 3));
    }
    //versionDescription 测试 -----------------------------------------------------   end

    //rules 测试 ------------------------------------------------------ start
    public function testSetRulesCorrectType()
    {
        $this->rule->setRules(array('rules'));
        $this->assertEquals(array('rules'), $this->rule->getRules());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetRulesWrongType()
    {
        $this->rule->setRules('rules');
    }
    //rules 测试 ------------------------------------------------------   end

    //templateVersion 测试 --------------------------------------------- start
    /**
     * 设置 Rule setTemplateVersion() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateVersionCorrectType()
    {
        $object = new TemplateVersion();
        $this->rule->setTemplateVersion($object);
        $this->assertSame($object, $this->rule->getTemplateVersion());
    }

    /**
     * 设置 Rule setTemplateVersion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateVersionWrongType()
    {
        $this->rule->setTemplateVersion('string');
    }
    //templateVersion 测试 ---------------------------------------------   end

    //template 测试 --------------------------------------------- start
    /**
     * 设置 Rule setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $object = new Template();
        $this->rule->setTemplate($object);
        $this->assertSame($object, $this->rule->getTemplate());
    }

    /**
     * 设置 Rule setTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $this->rule->setTemplate('string');
    }
    //template 测试 ---------------------------------------------   end

    //ruleVersion 测试 --------------------------------------------- start
    /**
     * 设置 Rule setRuleVersion() 正确的传参类型,期望传值正确
     */
    public function testSetRuleVersionCorrectType()
    {
        $object = new RuleVersion();
        $this->rule->setRuleVersion($object);
        $this->assertSame($object, $this->rule->getRuleVersion());
    }

    /**
     * 设置 Rule setRuleVersion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRuleVersionWrongType()
    {
        $this->rule->setRuleVersion('string');
    }
    //ruleVersion 测试 ---------------------------------------------   end

    //crew 测试 --------------------------------------------- start
    /**
     * 设置 Rule setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();
        $this->rule->setCrew($object);
        $this->assertSame($object, $this->rule->getCrew());
    }

    /**
     * 设置 Rule setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->rule->setCrew('string');
    }
    //crew 测试 ---------------------------------------------   end

    //userGroup 测试 --------------------------------------------- start
    /**
     * 设置 Rule setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $object = new UserGroup();
        $this->rule->setUserGroup($object);
        $this->assertSame($object, $this->rule->getUserGroup());
    }

    /**
     * 设置 Rule setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->rule->setUserGroup('string');
    }
    //userGroup 测试 ---------------------------------------------   end

    //status 测试 ----------------------------------------------------- start
    /**
     * 循环测试 Rule setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->rule->setStatus($actual);
        $this->assertEquals($expected, $this->rule->getStatus());
    }
    /**
     * 循环测试 Rule setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(Rule::STATUS['NORMAL'],Rule::STATUS['NORMAL']),
            array(Rule::STATUS['DELETED'],Rule::STATUS['DELETED']),
            array(999,Rule::STATUS['NORMAL']),
        );
    }
    /**
     * 设置 Rule setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->rule->setStatus('string');
    }
    //status 测试 -----------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Repository\RuleRepository',
            $this->rule->getRepository()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Repository\RuleRepository',
            $this->rule->getIOperateAbleAdapter()
        );
    }

    public function testVersionRestore()
    {
        $this->stub = $this->getMockBuilder(MockRule::class)
            ->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(RuleRepository::class);
        $repository->versionRestore(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->versionRestore();
        $this->assertTrue($result);
    }

     //isNormal -- 开始
    public function testIsNormalTrue()
    {
        $this->rule->setStatus(Rule::STATUS['NORMAL']);
        $result = $this->rule->isNormal();
        $this->assertTrue($result);
    }
 
    public function testIsNormalFail()
    {
        $this->rule->setStatus(Rule::STATUS['DELETED']);
        $result = $this->rule->isNormal();
        $this->assertFalse($result);
    }
     //isNormal -- 结束

    public function testDelete()
    {
        $this->stub = $this->getMockBuilder(MockRule::class)
            ->setMethods(['getRepository', 'isNormal'])->getMock();

        $this->stub->expects($this->exactly(1))->method('isNormal')->willReturn(true);

        $repository = $this->prophesize(RuleRepository::class);
        $repository->deleted(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->delete();
        $this->assertTrue($result);
    }

    public function testDeleteFail()
    {
        $this->stub = $this->getMockBuilder(MockRule::class)
            ->setMethods(['isNormal'])->getMock();

        $this->stub->expects($this->exactly(1))->method('isNormal')->willReturn(false);

        $result = $this->stub->delete();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }
}
