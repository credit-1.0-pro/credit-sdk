<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Model\NullCrew;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

use Sdk\ResourceCatalog\Rule\Adapter\RuleVersion\IRuleVersionAdapter;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleVersionTest extends TestCase
{
    private $ruleVersion;

    public function setUp()
    {
        $this->ruleVersion = new RuleVersion();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->ruleVersion);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->ruleVersion
        );
    }

    public function testRuleVersionConstructor()
    {
        $this->assertEquals(0, $this->ruleVersion->getId());
        $this->assertEmpty($this->ruleVersion->getNumber());
        $this->assertEquals('', $this->ruleVersion->getDescription());
        $this->assertEmpty($this->ruleVersion->getRuleId());
        $this->assertEquals(array(), $this->ruleVersion->getInfo());

        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Crew',
            $this->ruleVersion->getCrew()
        );
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Model\UserGroup',
            $this->ruleVersion->getUserGroup()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\TemplateVersion',
            $this->ruleVersion->getTemplateVersion()
        );

        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\Template',
            $this->ruleVersion->getTemplate()
        );
        $this->assertEquals(RuleVersion::STATUS['DISABLED'], $this->ruleVersion->getStatus());
        $this->assertEquals(0, $this->ruleVersion->getUpdateTime());
        $this->assertEquals(0, $this->ruleVersion->getCreateTime());
        $this->assertEquals(0, $this->ruleVersion->getStatusTime());
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 RuleVersion setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->ruleVersion->setId(1);
        $this->assertEquals(1, $this->ruleVersion->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //number 测试 ------------------------------------------------------- start
    /**
     * 设置 RuleVersion setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->ruleVersion->setNumber('string');
        $this->assertEquals('string', $this->ruleVersion->getNumber());
    }

    /**
     * 设置 RuleVersion setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->ruleVersion->setNumber(array(1, 2, 3));
    }
    //number 测试 -------------------------------------------------------   end

    //description 测试 ----------------------------------------------------- start
    /**
     * 设置 RuleVersion setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->ruleVersion->setDescription('string');
        $this->assertEquals('string', $this->ruleVersion->getDescription());
    }

    /**
     * 设置 RuleVersion setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->ruleVersion->setDescription(array(1, 2, 3));
    }
    //description 测试 -----------------------------------------------------   end

    //info 测试 ------------------------------------------------------ start
    public function testSetInfoCorrectType()
    {
        $this->ruleVersion->setInfo(array('info'));
        $this->assertEquals(array('info'), $this->ruleVersion->getInfo());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetInfoWrongType()
    {
        $this->ruleVersion->setInfo('info');
    }
    //info 测试 ------------------------------------------------------   end

    //crew 测试 --------------------------------------------- start
    /**
     * 设置 RuleVersion setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();
        $this->ruleVersion->setCrew($object);
        $this->assertSame($object, $this->ruleVersion->getCrew());
    }

    /**
     * 设置 RuleVersion setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->ruleVersion->setCrew('string');
    }
    //crew 测试 ---------------------------------------------   end

    //userGroup 测试 --------------------------------------------- start
    /**
     * 设置 RuleVersion setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $object = new UserGroup();
        $this->ruleVersion->setUserGroup($object);
        $this->assertSame($object, $this->ruleVersion->getUserGroup());
    }

    /**
     * 设置 RuleVersion setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->ruleVersion->setUserGroup('string');
    }
    //userGroup 测试 ---------------------------------------------   end

    //ruleId 测试 ----------------------------------------------------- start
    /**
     * 设置 RuleVersion setRuleId() 正确的传参类型,期望传值正确
     */
    public function testSetRuleIdCorrectType()
    {
        $this->ruleVersion->setRuleId(1);
        $this->assertEquals(1, $this->ruleVersion->getRuleId());
    }

    /**
     * 设置 RuleVersion setRuleId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRuleIdWrongType()
    {
        $this->ruleVersion->setRuleId(array(1, 2, 3));
    }
    //ruleId 测试 -----------------------------------------------------   end

    //templateVersion 测试 --------------------------------------------- start
    /**
     * 设置 setTemplateVersion() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateVersionCorrectType()
    {
        $object = new TemplateVersion();
        $this->ruleVersion->setTemplateVersion($object);
        $this->assertSame($object, $this->ruleVersion->getTemplateVersion());
    }

    /**
     * 设置 setTemplateVersion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateVersionWrongType()
    {
        $this->ruleVersion->setTemplateVersion('string');
    }
    //templateVersion 测试 ---------------------------------------------   end

    //template 测试 --------------------------------------------- start
    /**
     * 设置 setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $object = new Template();
        $this->ruleVersion->setTemplate($object);
        $this->assertSame($object, $this->ruleVersion->getTemplate());
    }

    /**
     * 设置 setTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $this->ruleVersion->setTemplate('string');
    }
    //template 测试 ---------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 RuleVersion setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->ruleVersion->setStatus($actual);
        $this->assertEquals($expected, $this->ruleVersion->getStatus());
    }

    /**
     * 循环测试 RuleVersion setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(RuleVersion::STATUS['ENABLED'],RuleVersion::STATUS['ENABLED']),
            array(RuleVersion::STATUS['DISABLED'],RuleVersion::STATUS['DISABLED']),
            array(9999,RuleVersion::STATUS['DISABLED']),
        );
    }

    /**
     * 设置 RuleVersion setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->ruleVersion->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end
}
