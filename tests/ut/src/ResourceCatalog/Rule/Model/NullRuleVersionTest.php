<?php
namespace Sdk\ResourceCatalog\Rule\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullRuleVersionTest extends TestCase
{
    private $nullRuleVersion;

    public function setUp()
    {
        $this->nullRuleVersion = NullRuleVersion::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullRuleVersion);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsRuleVersion()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\RuleVersion',
            $this->nullRuleVersion
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullRuleVersion
        );
    }
}
