<?php
namespace Sdk\ResourceCatalog\Rule\Command;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Command\RuleCommandDataTrait;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class OperationCommandTest extends TestCase
{
    use RuleCommandDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = $this->getRequestCommonData();

        $this->command = new OperationCommand(
            $this->fakerData['versionDescription'],
            $this->fakerData['rules'],
            1
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }

    public function testIdParameter()
    {
        $this->assertEquals(1, $this->command->id);
    }

    public function testVersionDescriptionParameter()
    {
        $this->assertEquals($this->fakerData['versionDescription'], $this->command->versionDescription);
    }

    public function testRulesParameter()
    {
        $this->assertEquals($this->fakerData['rules'], $this->command->rules);
    }
}
