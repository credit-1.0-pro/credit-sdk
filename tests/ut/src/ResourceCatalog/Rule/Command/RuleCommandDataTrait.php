<?php
namespace Sdk\ResourceCatalog\Rule\Command;

trait RuleCommandDataTrait
{
    public function getRequestCommonData() : array
    {
        $data = array(
            'versionDescription' => '行政许可信息规则设置',
            "template"=> 1,
            "rules"=>array(
                "completionRule"=>array(
                    'ZTMC'=>array(
                        array('id'=>1, 'base'=>array(1), 'item'=>'ZTMC')
                    )
                ),
                "comparisonRule"=>array(
                    'TYSHXYDM'=>array(
                        array('id'=>1, 'base'=>array(2), 'item'=>'TYSHXYDM')
                    )
                ),
                "deDuplicationRule"=>array('result'=>1, 'items'=>array('ZTMC', 'TYSHXYDM'))
            )
        );

        return $data;
    }
}
