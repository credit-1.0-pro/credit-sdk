<?php
namespace Sdk\ResourceCatalog\Rule\Command\Rule;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Command\RuleCommandDataTrait;

class AddRuleCommandTest extends TestCase
{
    use RuleCommandDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = $this->getRequestCommonData();

        $this->command = new AddRuleCommand(
            $this->fakerData['versionDescription'],
            $this->fakerData['rules'],
            $this->fakerData['template']
        );
    }

    public function testCorrectExtendsOperationCommand()
    {
        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Command\OperationCommand', $this->command);
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }

    public function testTemplateParameter()
    {
        $this->assertEquals($this->fakerData['template'], $this->command->template);
    }
}
