<?php
namespace Sdk\ResourceCatalog\Rule\Command\Rule;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Command\RuleCommandDataTrait;

class EditRuleCommandTest extends TestCase
{
    use RuleCommandDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = $this->getRequestCommonData();

        $this->command = new EditRuleCommand(
            $this->fakerData['versionDescription'],
            $this->fakerData['rules'],
            $faker->randomNumber()
        );
    }

    public function testCorrectExtendsOperationCommand()
    {
        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Command\OperationCommand', $this->command);
    }
    
    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
