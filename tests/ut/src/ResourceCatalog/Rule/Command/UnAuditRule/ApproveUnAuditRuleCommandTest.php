<?php
namespace Sdk\ResourceCatalog\Rule\Command\UnAuditRule;

use PHPUnit\Framework\TestCase;

class ApproveUnAuditRuleCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->stub = new ApproveUnAuditRuleCommand(
            $faker->randomNumber()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsOperationCommand()
    {
        $this->assertInstanceof('Sdk\Common\Command\ApproveCommand', $this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
