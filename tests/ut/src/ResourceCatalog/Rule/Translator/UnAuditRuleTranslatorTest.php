<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Utils\RuleUtils;

class UnAuditRuleTranslatorTest extends TestCase
{
    use RuleUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditRuleTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Model\NullUnAuditRule', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $unAuditRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule(1);

        $expression = $this->translator->objectToArray($unAuditRule);
    
        $this->compareArrayAndObjectUnAuditedRule($expression, $unAuditRule);
    }

    public function testObjectToArrayFail()
    {
        $unAuditRule = null;

        $expression = $this->translator->objectToArray($unAuditRule);
        $this->assertEquals(array(), $expression);
    }
}
