<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Model\RuleVersion;
use Sdk\ResourceCatalog\Rule\Utils\RuleRestfulUtils;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class RuleVersionRestfulTranslatorTest extends TestCase
{
    use RuleRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new RuleVersionRestfulTranslator();

        Core::$container->set('crew', new Crew());
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $translator = new MockRuleVersionRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator',
            $translator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $translator = new MockRuleVersionRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\User\Crew\Translator\CrewRestfulTranslator',
            $translator->getCrewRestfulTranslator()
        );
    }

    public function testGetTemplateRestfulTranslator()
    {
        $translator = new MockRuleVersionRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator',
            $translator->getTemplateRestfulTranslator()
        );
    }

    public function testGetTemplateVersionRestfulTranslator()
    {
        $translator = new MockRuleVersionRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator',
            $translator->getTemplateVersionRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $ruleVersion = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRuleVersion(1);

        $expression['data']['id'] = $ruleVersion->getId();
        $expression['data']['attributes']['number'] = $ruleVersion->getNumber();
        $expression['data']['attributes']['description'] = $ruleVersion->getDescription();
        $expression['data']['attributes']['ruleId'] = $ruleVersion->getRuleId();
        $expression['data']['attributes']['info'] = $ruleVersion->getInfo();
        $expression['data']['attributes']['status'] = $ruleVersion->getStatus();
        $expression['data']['attributes']['createTime'] = $ruleVersion->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $ruleVersion->getUpdateTime();
        $expression['data']['attributes']['statusTime'] = $ruleVersion->getStatusTime();

        $ruleVersionObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Model\RuleVersion', $ruleVersionObject);
        $this->compareArrayAndObjectRuleVersion($expression, $ruleVersionObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $ruleVersion = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Model\NullRuleVersion', $ruleVersion);
    }

    public function testObjectToArray()
    {
        $ruleVersion = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRuleVersion(1);

        $expression = $this->translator->objectToArray($ruleVersion);

        $this->compareArrayAndObjectRuleVersion($expression, $ruleVersion);
    }

    public function testObjectToArrayFail()
    {
        $ruleVersion = null;

        $expression = $this->translator->objectToArray($ruleVersion);
        $this->assertEquals(array(), $expression);
    }

    public function testArrayToObjectWithIncluded()
    {
        $translator = $this->getMockBuilder(MockRuleVersionRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getCrewRestfulTranslator',
                        'getUserGroupRestfulTranslator',
                        'getTemplateRestfulTranslator',
                        'getTemplateVersionRestfulTranslator',
                    ])->getMock();

        //初始化
        $expression = ['data'=>['relationships'=>'mock', 'id' => 1], 'included'=>'mock'];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'template'=>['data'=>'mockTemplate'],
            'templateVersion'=>['data'=>'mockTemplateVersion'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup']
        ];

        $crewInfo = ['mockCrewInfo'];
        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];
        $templateInfo = ['mockTemplateInfo'];
        $templateVersionInfo = ['mockTemplateVersionInfo'];

        $crew = new Crew(0);
        $publishUserGroup = new UserGroup(0);
        $template = new Template();
        $templateVersion = new TemplateVersion();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(4))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['template']['data']],
                 [$relationships['templateVersion']['data']],
                 [$relationships['publishUserGroup']['data']]
             ) ->will($this->onConsecutiveCalls(
                 $crewInfo,
                 $templateInfo,
                 $templateVersionInfo,
                 $publishUserGroupInfo
             ));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($publishUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishUserGroup);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $templateRestfulTranslator = $this->prophesize(TemplateRestfulTranslator::class);
        $templateRestfulTranslator->arrayToObject(Argument::exact($templateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($template);

        $restfulTranslator = $this->prophesize(TemplateVersionRestfulTranslator::class);
        $restfulTranslator->arrayToObject(Argument::exact($templateVersionInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($templateVersion);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTemplateRestfulTranslator')
            ->willReturn($templateRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTemplateVersionRestfulTranslator')
            ->willReturn($restfulTranslator->reveal());

        //揭示
        $ruleVersion = $translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Model\RuleVersion', $ruleVersion);
        $this->assertEquals($crew, $ruleVersion->getCrew());
        $this->assertEquals($publishUserGroup, $ruleVersion->getUserGroup());
        $this->assertEquals($template, $ruleVersion->getTemplate());
        $this->assertEquals($templateVersion, $ruleVersion->getTemplateVersion());
    }
}
