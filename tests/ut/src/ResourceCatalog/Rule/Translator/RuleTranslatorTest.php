<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Utils\RuleUtils;

class RuleTranslatorTest extends TestCase
{
    use RuleUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new RuleTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Model\NullRule', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule(1);

        $expression = $this->translator->objectToArray($rule);
    
        $this->compareArrayAndObjectRule($expression, $rule);
    }

    public function testObjectToArrayFail()
    {
        $rule = null;

        $expression = $this->translator->objectToArray($rule);
        $this->assertEquals(array(), $expression);
    }
}
