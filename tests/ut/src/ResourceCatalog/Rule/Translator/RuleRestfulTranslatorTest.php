<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Model\RuleVersion;
use Sdk\ResourceCatalog\Rule\Utils\RuleRestfulUtils;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class RuleRestfulTranslatorTest extends TestCase
{
    use RuleRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new RuleRestfulTranslator();

        Core::$container->set('crew', new Crew());
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $translator = new MockRuleRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator',
            $translator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetTemplateVersionRestfulTranslator()
    {
        $translator = new MockRuleRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator',
            $translator->getTemplateVersionRestfulTranslator()
        );
    }

    public function testGetTemplateRestfulTranslator()
    {
        $translator = new MockRuleRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator',
            $translator->getTemplateRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $translator = new MockRuleRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\User\Crew\Translator\CrewRestfulTranslator',
            $translator->getCrewRestfulTranslator()
        );
    }

    public function testGetRuleVersionRestfulTranslator()
    {
        $translator = new MockRuleRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Translator\RuleVersionRestfulTranslator',
            $translator->getRuleVersionRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule(1);

        $expression['data']['id'] = $rule->getId();
        $expression['data']['attributes']['rules'] = $rule->getRules();
        $expression['data']['attributes']['createTime'] = $rule->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $rule->getUpdateTime();
        $expression['data']['attributes']['status'] = $rule->getStatus();
        $expression['data']['attributes']['statusTime'] = $rule->getStatusTime();

        $ruleObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Model\Rule', $ruleObject);
        $this->compareArrayAndObjectCommon($expression, $ruleObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $rule = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Model\NullRule', $rule);
    }

    public function testObjectToArray()
    {
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule(1);

        $expression = $this->translator->objectToArray($rule);

        $this->compareArrayAndObjectCommon($expression, $rule);
    }

    public function testObjectToArrayFail()
    {
        $rule = null;

        $expression = $this->translator->objectToArray($rule);
        $this->assertEquals(array(), $expression);
    }

    private function includedInfo() : array
    {
        $info['crewInfo'] = ['mockCrewInfo'];
        $info['publishUserGroupInfo'] = ['mockPublishUserGroupInfo'];
        $info['ruleVersionInfo'] = ['mockRuleVersionInfo'];
        $info['templateVersionInfo'] = ['mockTemplateVersionInfo'];
        $info['templateInfo'] = ['mockTemplateInfo'];

        return $info;
    }

    private function includedObject() : array
    {
        $objectList['crew'] = new Crew();
        $objectList['publishUserGroup'] = new UserGroup();
        $objectList['ruleVersion'] = new RuleVersion();
        $objectList['templateVersion'] = new TemplateVersion();
        $objectList['template'] = new Template();

        return $objectList;
    }

    public function testArrayToObjectWithIncluded()
    {
        $translator = $this->getMockBuilder(MockRuleRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getCrewRestfulTranslator',
                        'getUserGroupRestfulTranslator',
                        'getRuleVersionRestfulTranslator',
                        'getTemplateVersionRestfulTranslator',
                        'getTemplateRestfulTranslator'
                    ])->getMock();

        $expression = ['data'=>['relationships'=>'mock', 'id' => 1], 'included'=>'mock'];

        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup'],
            'template'=>['data'=>'mockTemplate'],
            'templateVersion'=>['data'=>'mockTemplateVersion'],
            'ruleVersion'=>['data'=>'mockRuleVersion']
        ];

        $info = $this->includedInfo();
        $objectList = $this->includedObject();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with($expression['included'], $expression['data']['relationships'])
            ->willReturn($relationships);

        $translator->expects($this->exactly(5))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['publishUserGroup']['data']],
                 [$relationships['template']['data']],
                 [$relationships['templateVersion']['data']],
                 [$relationships['ruleVersion']['data']]
             ) ->will($this->onConsecutiveCalls(
                 $info['crewInfo'],
                 $info['publishUserGroupInfo'],
                 $info['templateInfo'],
                 $info['templateVersionInfo'],
                 $info['ruleVersionInfo']
             ));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($info['publishUserGroupInfo']))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($objectList['publishUserGroup']);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($info['crewInfo']))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($objectList['crew']);

        $versionRestfulTranslator = $this->prophesize(RuleVersionRestfulTranslator::class);
        $versionRestfulTranslator->arrayToObject(Argument::exact($info['ruleVersionInfo']))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($objectList['ruleVersion']);

        $templateVersionTranslator = $this->prophesize(TemplateVersionRestfulTranslator::class);
        $templateVersionTranslator->arrayToObject(Argument::exact($info['templateVersionInfo']))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($objectList['templateVersion']);

        $templateTranslator = $this->prophesize(TemplateRestfulTranslator::class);
        $templateTranslator->arrayToObject(Argument::exact($info['templateInfo']))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($objectList['template']);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getRuleVersionRestfulTranslator')
            ->willReturn($versionRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTemplateVersionRestfulTranslator')
            ->willReturn($templateVersionTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTemplateRestfulTranslator')
            ->willReturn($templateTranslator->reveal());

        //揭示
        $rule = $translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Model\Rule', $rule);
        $this->assertEquals($objectList['crew'], $rule->getCrew());
        $this->assertEquals($objectList['publishUserGroup'], $rule->getUserGroup());
        $this->assertEquals($objectList['ruleVersion'], $rule->getRuleVersion());
        $this->assertEquals($objectList['templateVersion'], $rule->getTemplateVersion());
        $this->assertEquals($objectList['template'], $rule->getTemplate());
    }
}
