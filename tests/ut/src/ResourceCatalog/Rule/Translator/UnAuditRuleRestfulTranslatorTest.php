<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Model\RuleVersion;
use Sdk\ResourceCatalog\Rule\Utils\RuleRestfulUtils;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class UnAuditRuleRestfulTranslatorTest extends TestCase
{
    use RuleRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditRuleRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $unAuditRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule(1);

        $expression['data']['id'] = $unAuditRule->getId();

        $expression['data']['attributes']['rules'] = $unAuditRule->getRules();
        $expression['data']['attributes']['relationId'] = $unAuditRule->getRelationId();
        $expression['data']['attributes']['applyStatus'] = $unAuditRule->getApplyStatus();
        $expression['data']['attributes']['rejectReason'] = $unAuditRule->getRejectReason();
        $expression['data']['attributes']['operationType'] = $unAuditRule->getOperationType();
        $expression['data']['attributes']['applyInfoType'] = $unAuditRule->getApplyInfoType();
        $expression['data']['attributes']['createTime'] = $unAuditRule->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $unAuditRule->getUpdateTime();
        $expression['data']['attributes']['status'] = $unAuditRule->getStatus();
        $expression['data']['attributes']['statusTime'] = $unAuditRule->getStatusTime();

        $unAuditRuleObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Model\UnAuditRule', $unAuditRuleObject);
        $this->compareArrayAndObjectCommonUnAuditRule($expression, $unAuditRuleObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $unAuditRule = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Model\NullUnAuditRule', $unAuditRule);
    }

    public function testObjectToArray()
    {
        $unAuditRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule(1);

        $expression = $this->translator->objectToArray($unAuditRule);
     
        $this->compareArrayAndObjectCommonUnAuditRule($expression, $unAuditRule);
    }

    public function testObjectToArrayFail()
    {
        $unAuditRule = null;

        $expression = $this->translator->objectToArray($unAuditRule);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        $translator = $this->getMockBuilder(UnAuditRuleRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getTemplateRestfulTranslator',
                        'getTemplateVersionRestfulTranslator',
                        'getCrewRestfulTranslator',
                        'getUserGroupRestfulTranslator',
                        'getRuleVersionRestfulTranslator',
                    ])
                    ->getMock();

        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup'],
            'template'=>['data'=>'mockTemplate'],
            'templateVersion'=>['data'=>'mockTemplateVersion'],
            'ruleVersion'=>['data'=>'mockRuleVersion'],
            'applyCrew'=>['data'=>'mockApplyCrew'],
            'applyUserGroup'=>['data'=>'mockApplyUserGroup'],
            'publishCrew'=>['data'=>'mockPublishCrew'],
        ];

        $publishCrewInfo = ['mockPublishCrewInfo'];
        $crewInfo = ['mockCrewInfo'];
        $relationInfo = ['mockRelationInfo'];
        $applyCrewInfo = ['mockApplyCrewInfo'];
        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];
        $applyUserGroupInfo = ['mockApplyUserGroupInfo'];
        $ruleVersionInfo = ['mockRuleVersionInfo'];
        $templateVersionInfo = ['mockTemplateVersionInfo'];
        $templateInfo = ['mockTemplateInfo'];

        $crew = new Crew();
        $applyCrew = new Crew();
        $publishCrew = new Crew();
        $publishUserGroup = new UserGroup();
        $applyUserGroup = new UserGroup();
        $ruleVersion = new RuleVersion();
        $templateVersion = new TemplateVersion();
        $template = new Template();

        //预言
        $translator->expects($this->exactly(2))
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用5次, 依次入参 department, userGroup
        //依次返回 $departmentInfo 和 $userGroupInfo
        $translator->expects($this->exactly(8))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['publishUserGroup']['data']],
                 [$relationships['template']['data']],
                 [$relationships['templateVersion']['data']],
                 [$relationships['ruleVersion']['data']],
                 [$relationships['applyCrew']['data']],
                 [$relationships['applyUserGroup']['data']],
                 [$relationships['publishCrew']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $crewInfo,
                $publishUserGroupInfo,
                $templateInfo,
                $templateVersionInfo,
                $ruleVersionInfo,
                $applyCrewInfo,
                $applyUserGroupInfo,
                $publishCrewInfo
            ));
  
        $templateRestfulTranslator = $this->prophesize(TemplateRestfulTranslator::class);
        $templateRestfulTranslator->arrayToObject(Argument::exact($templateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($template);
        
        $restfulTranslator = $this->prophesize(TemplateVersionRestfulTranslator::class);
        $restfulTranslator->arrayToObject(Argument::exact($templateVersionInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($templateVersion);

        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($publishUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishUserGroup);

        $userGroupRestfulTranslator->arrayToObject(Argument::exact($applyUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyUserGroup);

        $versionRestfulTranslator = $this->prophesize(RuleVersionRestfulTranslator::class);
        $versionRestfulTranslator->arrayToObject(Argument::exact($ruleVersionInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($ruleVersion);
                                    
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $crewRestfulTranslator->arrayToObject(Argument::exact($publishCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishCrew);

        $crewRestfulTranslator->arrayToObject(Argument::exact($applyCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyCrew);

        //绑定
        $translator->expects($this->exactly(2))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(3))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTemplateRestfulTranslator')
            ->willReturn($templateRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getRuleVersionRestfulTranslator')
            ->willReturn($versionRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTemplateVersionRestfulTranslator')
            ->willReturn($restfulTranslator->reveal());
       
        //揭示
        $unAuditRule = $translator->arrayToObject($expression);
         
        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Model\UnAuditRule', $unAuditRule);
        $this->assertEquals($crew, $unAuditRule->getCrew());
        $this->assertEquals($publishCrew, $unAuditRule->getPublishCrew());
        $this->assertEquals($applyCrew, $unAuditRule->getApplyCrew());
        $this->assertEquals($publishUserGroup, $unAuditRule->getPublishUserGroup());
        $this->assertEquals($templateVersion, $unAuditRule->getTemplateVersion());
        $this->assertEquals($template, $unAuditRule->getTemplate());
        $this->assertEquals($applyUserGroup, $unAuditRule->getApplyUserGroup());
        $this->assertEquals($ruleVersion, $unAuditRule->getRuleVersion());
    }
}
