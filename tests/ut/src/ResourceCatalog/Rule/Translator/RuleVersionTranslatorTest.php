<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Utils\RuleUtils;

class RuleVersionTranslatorTest extends TestCase
{
    use RuleUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new RuleVersionTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\ResourceCatalog\Rule\Model\NullRuleVersion', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $ruleVersion = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRuleVersion(1);

        $expression = $this->translator->objectToArray($ruleVersion);
    
        $this->compareArrayAndObjectRuleVersion($expression, $ruleVersion);
    }

    public function testObjectToArrayFail()
    {
        $ruleVersion = null;

        $expression = $this->translator->objectToArray($ruleVersion);
        $this->assertEquals(array(), $expression);
    }
}
