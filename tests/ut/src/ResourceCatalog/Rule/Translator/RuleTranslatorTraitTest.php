<?php
namespace Sdk\ResourceCatalog\Rule\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Model\IRule;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class RuleTranslatorTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockRuleTranslatorTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetRulesFormatConversion()
    {
        $this->trait = $this->getMockBuilder(MockRuleTranslatorTrait::class)
                            ->setMethods([
                                'deDuplicationRulesFormatConversion'
                            ])
                            ->getMock();

        $rules = array(
            "deDuplicationRule"=>array(
                'result'=>IRule::DE_DUPLICATION_RESULT['DISCARD'],
                'items'=>array('ZTMC', 'TYSHXYDM')
            )
        );

        $deDuplicationRule = array(
            'result'=> array(
                'id' => marmot_encode(IRule::DE_DUPLICATION_RESULT['DISCARD']),
                'name'=>IRule::DE_DUPLICATION_RESULT_CN[IRule::DE_DUPLICATION_RESULT['DISCARD']]
            ),
            'items'=>array('ZTMC', 'TYSHXYDM')
        );
        $rulesResult = array("deDuplicationRule" => $deDuplicationRule);

        $this->trait->expects($this->once())->method(
            'deDuplicationRulesFormatConversion'
        )->willReturn($deDuplicationRule);

        $result = $this->trait->publicGetRulesFormatConversion($rules);

        $this->assertEquals($rulesResult, $result);
    }

    public function testGetRulesFormatConversionFail()
    {
        $rules = array("rules"=>array('rules'));
        $result = $this->trait->publicGetRulesFormatConversion($rules);

        $this->assertEquals($rules, $result);
    }

    public function testCompletionRulesFormatConversion()
    {
        $this->trait = $this->getMockBuilder(MockRuleTranslatorTrait::class)
                            ->setMethods([
                                'getCompletionBaseCn'
                            ])
                            ->getMock();

        $completionRule = array(
            'ZTMC' => array(
                array('id'=>1, 'versionId'=>1, 'base'=> array(1), 'item'=>'ZTMC'),
            )
        );

        $base = array('MA');

        $completionRuleResult = array(
            'ZTMC' => array(
                array('id'=>"MA", 'versionId'=>"MA", 'base'=> $base, 'item'=>'ZTMC'),
            )
        );

        $this->trait->expects($this->once())->method('getCompletionBaseCn')->willReturn($base);

        $result = $this->trait->publicCompletionRulesFormatConversion($completionRule);

        $this->assertEquals($completionRuleResult, $result);
    }

    public function testGetCompletionBaseCn()
    {
        $base = array(IRule::COMPLETION_BASE['NAME']);

        $data = array(
            array(
                'id' => marmot_encode(IRule::COMPLETION_BASE['NAME']),
                'name' => IRule::COMPLETION_BASE_CN[IRule::COMPLETION_BASE['NAME']]
            )
        );

        $result = $this->trait->publicGetCompletionBaseCn($base);

        $this->assertEquals($result, $data);
    }

    public function testComparisonRulesFormatConversion()
    {
        $this->trait = $this->getMockBuilder(MockRuleTranslatorTrait::class)
                            ->setMethods([
                                'getComparisonBaseCn'
                            ])
                            ->getMock();

        $comparisonRule = array(
            'ZTMC' => array(
                array('id'=>1, 'versionId'=>1, 'base'=> array(1), 'item'=>'ZTMC'),
            )
        );

        $base = array('MA');

        $comparisonRuleResult = array(
            'ZTMC' => array(
                array('id'=>"MA", 'versionId'=>"MA", 'base'=> $base, 'item'=>'ZTMC'),
            )
        );

        $this->trait->expects($this->once())->method('getComparisonBaseCn')->willReturn($base);

        $result = $this->trait->publicComparisonRulesFormatConversion($comparisonRule);

        $this->assertEquals($comparisonRuleResult, $result);
    }

    public function testGetComparisonBaseCn()
    {
        $base = array(IRule::COMPARISON_BASE['NAME']);

        $data = array(
            array(
                'id' => marmot_encode(IRule::COMPARISON_BASE['NAME']),
                'name' => IRule::COMPARISON_BASE_CN[IRule::COMPARISON_BASE['NAME']]
            )
        );

        $result = $this->trait->publicGetComparisonBaseCn($base);

        $this->assertEquals($result, $data);
    }

    public function testDeDuplicationRulesFormatConversion()
    {
        $this->trait = $this->getMockBuilder(MockRuleTranslatorTrait::class)
                            ->setMethods([
                                'getDeDuplicationResultCn'
                            ])
                            ->getMock();

        $deDuplicationRule = array('result'=>IRule::DE_DUPLICATION_RESULT['DISCARD'], 'items'=>array('ZTMC'));
        $result = array('MA');
        $deDuplicationRuleResult = array('result'=>$result, 'items'=>array('ZTMC'));

        $this->trait->expects($this->once())->method('getDeDuplicationResultCn')->willReturn($result);

        $result = $this->trait->publicDeDuplicationRulesFormatConversion($deDuplicationRule);

        $this->assertEquals($deDuplicationRuleResult, $result);
    }

    public function testGetDeDuplicationResultCn()
    {
        $result = IRule::DE_DUPLICATION_RESULT['DISCARD'];

        $data = array(
            'id' => marmot_encode(IRule::DE_DUPLICATION_RESULT['DISCARD']),
            'name' => IRule::DE_DUPLICATION_RESULT_CN[IRule::DE_DUPLICATION_RESULT['DISCARD']]
        );

        $result = $this->trait->publicGetDeDuplicationResultCn($result);

        $this->assertEquals($result, $data);
    }

    public function testGetInfoCn()
    {
        $this->trait = $this->getMockBuilder(MockRuleTranslatorTrait::class)
                            ->setMethods([
                                'getRulesFormatConversion'
                            ])
                            ->getMock();

        $info = array('rules'=>array('rules'));
        $rules = array('rules'=>'rules');
        $infoResult = array('rules'=>$rules);

        $this->trait->expects($this->once())->method('getRulesFormatConversion')->willReturn($rules);

        $result = $this->trait->publicGetInfoCn($info);

        $this->assertEquals($infoResult, $result);
    }

    public function testGetInfoCnEmpty()
    {
        $info = array();
        $infoResult = array('rules'=>array());

        $result = $this->trait->publicGetInfoCn($info);

        $this->assertEquals($infoResult, $result);
    }
}
