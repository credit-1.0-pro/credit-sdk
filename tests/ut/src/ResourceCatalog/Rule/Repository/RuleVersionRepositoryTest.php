<?php
namespace Sdk\ResourceCatalog\Rule\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Adapter\RuleVersion\RuleVersionRestfulAdapter;

class RuleVersionRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(RuleVersionRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIRuleVersionAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Adapter\RuleVersion\IRuleVersionAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Adapter\RuleVersion\RuleVersionRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Adapter\RuleVersion\RuleVersionMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(RuleVersionRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
