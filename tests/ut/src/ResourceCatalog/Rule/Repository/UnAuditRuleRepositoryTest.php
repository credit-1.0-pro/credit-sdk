<?php
namespace Sdk\ResourceCatalog\Rule\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule\UnAuditRuleRestfulAdapter;

class UnAuditRuleRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UnAuditRuleRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIUnAuditRuleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule\IUnAuditRuleAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule\UnAuditRuleRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule\UnAuditRuleMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(UnAuditRuleRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testRevoke()
    {
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule(1);

        $adapter = $this->prophesize(UnAuditRuleRestfulAdapter::class);
        $adapter->revoke(Argument::exact($rule))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->revoke($rule);
    }
}
