<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Command\UnAuditRule\RevokeUnAuditRuleCommand;

class RevokeUnAuditRuleCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(RevokeUnAuditRuleCommandHandler::class)
            ->setMethods(['fetchUnAuditRule'])
            ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $id = 1;
        
        $command = new RevokeUnAuditRuleCommand($id);

        $rule = $this->prophesize(UnAuditRule::class);
        $rule->revoke()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())->method('fetchUnAuditRule')->willReturn($rule->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
