<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule;

use PHPUnit\Framework\TestCase;

class RejectUnAuditRuleCommandHandlerTest extends TestCase
{
    private $rejectStub;

    public function setUp()
    {
        $this->rejectStub = $this->getMockBuilder(MockRejectUnAuditRuleCommandHandler::class)
            ->setMethods(['fetchUnAuditRule'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\RejectCommandHandler',
            $this->rejectStub
        );
    }

    public function testFetchIApplyObject()
    {
        $id = 1;
        $unAuditRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule($id);

        $this->rejectStub->expects($this->exactly(1))
                    ->method('fetchUnAuditRule')
                    ->willReturn($unAuditRule);

        $result = $this->rejectStub->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditRule);
    }
}
