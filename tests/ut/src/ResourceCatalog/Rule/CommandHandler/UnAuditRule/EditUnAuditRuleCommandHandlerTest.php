<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Command\RuleCommandDataTrait;
use Sdk\ResourceCatalog\Rule\Command\UnAuditRule\EditUnAuditRuleCommand;

class EditUnAuditRuleCommandHandlerTest extends TestCase
{
    use RuleCommandDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditUnAuditRuleCommandHandler::class)
            ->setMethods(['fetchUnAuditRule', 'ruleExecuteAction'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->fakerData = $this->getRequestCommonData();
        $id = 1;
        
        $command = new EditUnAuditRuleCommand(
            $this->fakerData['versionDescription'],
            $this->fakerData['rules'],
            $id
        );

        $unAuditedRule = $this->prophesize(UnAuditRule::class);
        $unAuditedRule->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
            ->method('ruleExecuteAction')
            ->willReturn($unAuditedRule->reveal());

        $this->commandHandler->expects($this->once())
            ->method('fetchUnAuditRule')
            ->willReturn($unAuditedRule->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
