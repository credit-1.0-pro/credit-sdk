<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule;

use PHPUnit\Framework\TestCase;

class ApproveUnAuditRuleCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveUnAuditRuleCommandHandler::class)
            ->setMethods(['fetchUnAuditRule'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\ApproveCommandHandler',
            $this->stub
        );
    }

    public function testFetchIApplyObject()
    {
        $id = 1;
        $unAuditRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule($id);

        $this->stub->expects($this->exactly(1))
                    ->method('fetchUnAuditRule')
                    ->willReturn($unAuditRule);

        $result = $this->stub->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditRule);
    }
}
