<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Sdk\ResourceCatalog\Rule\Command\UnAuditRule\EditUnAuditRuleCommand;
use Sdk\ResourceCatalog\Rule\Command\UnAuditRule\RevokeUnAuditRuleCommand;
use Sdk\ResourceCatalog\Rule\Command\UnAuditRule\RejectUnAuditRuleCommand;
use Sdk\ResourceCatalog\Rule\Command\UnAuditRule\ApproveUnAuditRuleCommand;

class UnAuditRuleCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new UnAuditRuleCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testEditUnAuditRuleCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditUnAuditRuleCommand(
                $this->faker->title(),
                array($this->faker->title()),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule\EditUnAuditRuleCommandHandler',
            $commandHandler
        );
    }

    public function testRejectUnAuditRuleCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RejectUnAuditRuleCommand(
                $this->faker->title(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule\RejectUnAuditRuleCommandHandler',
            $commandHandler
        );
    }

    public function testApproveUnAuditRuleCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ApproveUnAuditRuleCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule\ApproveUnAuditRuleCommandHandler',
            $commandHandler
        );
    }

    public function testRevokeUnAuditRuleCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RevokeUnAuditRuleCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\CommandHandler\UnAuditRule\RevokeUnAuditRuleCommandHandler',
            $commandHandler
        );
    }
}
