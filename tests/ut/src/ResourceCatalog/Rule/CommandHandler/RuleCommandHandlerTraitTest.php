<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Repository\RuleRepository;
use Sdk\ResourceCatalog\Rule\Command\Rule\AddRuleCommand;
use Sdk\ResourceCatalog\Rule\Repository\UnAuditRuleRepository;

use Sdk\ResourceCatalog\Rule\Model\RuleVersion;
use Sdk\ResourceCatalog\Rule\Repository\RuleVersionRepository;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class RuleCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockRuleCommandHandlerTrait::class)
                        ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetRule()
    {
        $trait = new MockRuleCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\Rule',
            $trait->publicGetRule()
        );
    }

    public function testGetUnAuditRule()
    {
        $trait = new MockRuleCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\UnAuditRule',
            $trait->publicGetUnAuditRule()
        );
    }

    public function testGetUnAuditRuleRepository()
    {
        $trait = new MockRuleCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Repository\UnAuditRuleRepository',
            $trait->publicGetUnAuditRuleRepository()
        );
    }

    public function testGetRepository()
    {
        $trait = new MockRuleCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Repository\RuleRepository',
            $trait->publicGetRepository()
        );
    }

    public function testFetchRule()
    {
        $trait = $this->getMockBuilder(MockRuleCommandHandlerTrait::class)
                 ->setMethods(['getRepository'])->getMock();

        $id = 1;

        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule($id);

        $repository = $this->prophesize(RuleRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($rule);

        $trait->expects($this->exactly(1))
                         ->method('getRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchRule($id);

        $this->assertEquals($result, $rule);
    }

    public function testFetchUnAuditRule()
    {
        $trait = $this->getMockBuilder(MockRuleCommandHandlerTrait::class)
                 ->setMethods(['getUnAuditRuleRepository'])->getMock();

        $id = 1;

        $unAuditRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule($id);

        $repository = $this->prophesize(UnAuditRuleRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditRule);

        $trait->expects($this->exactly(1))->method('getUnAuditRuleRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchUnAuditRule($id);

        $this->assertEquals($result, $unAuditRule);
    }

    public function testGetRuleVersionRepository()
    {
        $trait = new MockRuleCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Repository\RuleVersionRepository',
            $trait->publicGetRuleVersionRepository()
        );
    }

    public function testFetchRuleVersion()
    {
        $trait = $this->getMockBuilder(MockRuleCommandHandlerTrait::class)
                 ->setMethods(['getRuleVersionRepository'])->getMock();

        $id = 1;
        $ruleVersion = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRuleVersion($id);

        $repository = $this->prophesize(RuleVersionRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($ruleVersion);

        $trait->expects($this->exactly(1))
                         ->method('getRuleVersionRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchRuleVersion($id);

        $this->assertEquals($result, $ruleVersion);
    }

    public function testGetTemplateRepository()
    {
        $trait = new MockRuleCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Repository\TemplateRepository',
            $trait->publicGetTemplateRepository()
        );
    }

    public function testFetchTemplate()
    {
        $trait = $this->getMockBuilder(MockRuleCommandHandlerTrait::class)
                 ->setMethods(['getTemplateRepository'])->getMock();

        $id = 1;

        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate($id);

        $repository = $this->prophesize(TemplateRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($template);

        $trait->expects($this->exactly(1))
                         ->method('getTemplateRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchTemplate($id);

        $this->assertEquals($result, $template);
    }

    public function testRuleExecuteAction()
    {
        $trait = new MockRuleCommandHandlerTrait();
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule(1);

        $command = new AddRuleCommand(
            $rule->getVersionDescription(),
            $rule->getRules(),
            $rule->getTemplate()->getId()
        );

        $result = $trait->publicRuleExecuteAction($command, $rule);

        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\Rule',
            $result
        );
        $this->assertEquals($result, $rule);
    }
}
