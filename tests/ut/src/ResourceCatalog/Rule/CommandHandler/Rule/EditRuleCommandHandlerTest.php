<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\Rule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Command\RuleCommandDataTrait;
use Sdk\ResourceCatalog\Rule\Command\Rule\EditRuleCommand;

class EditRuleCommandHandlerTest extends TestCase
{
    use RuleCommandDataTrait;

    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditRuleCommandHandler::class)
            ->setMethods(['fetchRule', 'ruleExecuteAction'])
            ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->fakerData = $this->getRequestCommonData();
        $id = 1;
        
        $command = new EditRuleCommand(
            $this->fakerData['versionDescription'],
            $this->fakerData['rules'],
            $id
        );

        $rule = $this->prophesize(Rule::class);
        $rule->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
            ->method('ruleExecuteAction')
            ->willReturn($rule->reveal());

        $this->commandHandler->expects($this->once())
            ->method('fetchRule')
            ->willReturn($rule->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
