<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\Rule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Command\Rule\VersionRestoreRuleCommand;

class VersionRestoreRuleCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(VersionRestoreRuleCommandHandler::class)
            ->setMethods(['fetchRuleVersion', 'fetchRule'])
            ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $id = $ruleVersionId = 1;
        
        $command = new VersionRestoreRuleCommand(
            $ruleVersionId,
            $id
        );

        $ruleVersion = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRuleVersion(1);
        $this->commandHandler->expects($this->once())->method('fetchRuleVersion')->willReturn($ruleVersion);

        $rule = $this->prophesize(Rule::class);
        $rule->setRuleVersion(Argument::exact($ruleVersion))->shouldBeCalledTimes(1);
        $rule->versionRestore()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())->method('fetchRule')->willReturn($rule->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
