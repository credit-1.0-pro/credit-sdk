<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\Rule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Command\Rule\AddRuleCommand;
use Sdk\ResourceCatalog\Rule\Command\RuleCommandDataTrait;

class AddRuleCommandHandlerTest extends TestCase
{
    use RuleCommandDataTrait;

    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddRuleCommandHandler::class)
            ->setMethods(['getRule', 'ruleExecuteAction', 'fetchTemplate'])
            ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecuteSuccess()
    {
        $this->fakerData = $this->getRequestCommonData();
        
        $command = new AddRuleCommand(
            $this->fakerData['versionDescription'],
            $this->fakerData['rules'],
            $this->fakerData['template']
        );

        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate($command->template);

        $this->commandHandler->expects($this->once())
            ->method('fetchTemplate')
            ->willReturn($template);
            
        $rule = $this->prophesize(Rule::class);
        $rule->setTemplate($template)->shouldBeCalledTimes(1);
        $rule->add()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
            ->method('ruleExecuteAction')
            ->willReturn($rule->reveal());

        $this->commandHandler->expects($this->once())
            ->method('getRule')
            ->willReturn($rule->reveal());

        $result = $this->commandHandler->execute($command);

        $this->assertTrue($result);
    }
}
