<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\Rule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Command\Rule\DeleteRuleCommand;

class DeleteRuleCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(DeleteRuleCommandHandler::class)
            ->setMethods(['fetchRule'])
            ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $id = 1;
        
        $command = new DeleteRuleCommand($id);

        $rule = $this->prophesize(Rule::class);
        $rule->delete()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())->method('fetchRule')->willReturn($rule->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
