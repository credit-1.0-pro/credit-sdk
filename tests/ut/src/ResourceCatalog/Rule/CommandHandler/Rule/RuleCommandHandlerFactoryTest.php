<?php
namespace Sdk\ResourceCatalog\Rule\CommandHandler\Rule;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Sdk\ResourceCatalog\Rule\Command\Rule\AddRuleCommand;
use Sdk\ResourceCatalog\Rule\Command\Rule\EditRuleCommand;
use Sdk\ResourceCatalog\Rule\Command\Rule\DeleteRuleCommand;
use Sdk\ResourceCatalog\Rule\Command\Rule\VersionRestoreRuleCommand;

class RuleCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new RuleCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddRuleCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddRuleCommand(
                $this->faker->title(),
                array($this->faker->title()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\CommandHandler\Rule\AddRuleCommandHandler',
            $commandHandler
        );
    }

    public function testEditRuleCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditRuleCommand(
                $this->faker->text(),
                array($this->faker->title()),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\CommandHandler\Rule\EditRuleCommandHandler',
            $commandHandler
        );
    }

    public function testVersionRestoreRuleCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new VersionRestoreRuleCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\CommandHandler\Rule\VersionRestoreRuleCommandHandler',
            $commandHandler
        );
    }

    public function testDeleteRuleCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DeleteRuleCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\CommandHandler\Rule\DeleteRuleCommandHandler',
            $commandHandler
        );
    }
}
