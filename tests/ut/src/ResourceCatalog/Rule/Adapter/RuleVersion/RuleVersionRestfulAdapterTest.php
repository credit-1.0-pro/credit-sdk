<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\RuleVersion;

use PHPUnit\Framework\TestCase;

class RuleVersionRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockRuleVersionRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new RuleVersionRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIRuleVersionAdapter()
    {
        $adapter = new RuleVersionRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Adapter\RuleVersion\IRuleVersionAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockRuleVersionRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Translator\RuleVersionRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockRuleVersionRestfulAdapter();
        $this->assertEquals('resourceCatalogs/ruleVersions', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockRuleVersionRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\NullRuleVersion',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockRuleVersionRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'RULE_VERSION_LIST',
                RuleVersionRestfulAdapter::SCENARIOS['RULE_VERSION_LIST']
            ],
            [
                'RULE_VERSION_FETCH_ONE',
                RuleVersionRestfulAdapter::SCENARIOS['RULE_VERSION_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
}
