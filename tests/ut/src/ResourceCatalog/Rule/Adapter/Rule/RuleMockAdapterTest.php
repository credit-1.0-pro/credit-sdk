<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\Rule;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Rule\Model\Rule;

class RuleMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new RuleMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\Rule',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\Rule\Model\Rule',
                $each
            );
        }
    }

    public function testFetchOneAsync()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\Rule',
            $this->adapter->fetchOneAsync(1)
        );
    }

    public function testFetchListAsync()
    {
        $list = $this->adapter->fetchListAsync([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\Rule\Model\Rule',
                $each
            );
        }
    }

    public function testSearch()
    {
        list($list, $count) = $this->adapter->search(['filter'], ['sort'], 1, 2);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\Rule\Model\Rule',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }

    public function testSearchAsync()
    {
        list($list, $count) = $this->adapter->searchAsync(['filter'], ['sort'], 1, 2);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\Rule\Model\Rule',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }

    public function testVersionRestore()
    {
        $this->assertTrue($this->adapter->versionRestore(new Rule()));
    }

    public function testDeleted()
    {
        $this->assertTrue($this->adapter->deleted(new Rule()));
    }
}
