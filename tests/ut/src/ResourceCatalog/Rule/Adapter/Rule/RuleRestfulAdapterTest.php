<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\Rule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Model\NullRule;
use Sdk\ResourceCatalog\Rule\Translator\RuleRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockRuleRestfulAdapter::class)
                           ->setMethods([
                               'commonMapErrors',
                               'getTranslator',
                               'post',
                               'isSuccess',
                               'patch',
                               'delete',
                               'translateToObject'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new RuleRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIRuleAdapter()
    {
        $adapter = new RuleRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Adapter\Rule\IRuleAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockRuleRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Translator\RuleRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockRuleRestfulAdapter();
        $this->assertEquals('resourceCatalogs/rules', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockRuleRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\NullRule',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockRuleRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'RULE_LIST',
                RuleRestfulAdapter::SCENARIOS['RULE_LIST']
            ],
            [
                'RULE_FETCH_ONE',
                RuleRestfulAdapter::SCENARIOS['RULE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    /**
     * 为RuleRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$rule,$keys,$ruleArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareRuleTranslator(
        Rule $rule,
        array $keys,
        array $ruleArray
    ) {
        $translator = $this->prophesize(RuleRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($rule),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($ruleArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Rule $rule)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($rule);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator(
            $rule,
            array(
                'rules',
                'versionDescription',
                'template',
                'crew'
            ),
            $ruleArray
        );

        $this->adapter
            ->method('post')
            ->with('resourceCatalogs/rules', $ruleArray);

        $this->success($rule);
        $result = $this->adapter->add($rule);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator(
            $rule,
            array(
                'rules',
                'versionDescription',
                'template',
                'crew'
            ),
            $ruleArray
        );

        $this->adapter
            ->method('post')
            ->with('resourceCatalogs/rules', $ruleArray);
        
            $this->failure($rule);
            $result = $this->adapter->add($rule);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareRuleTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator($rule, array(
            'rules',
            'versionDescription',
            'crew'
        ), $ruleArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/rules/'.$rule->getId(), $ruleArray);

        $this->success($rule);
        $result = $this->adapter->edit($rule);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareRuleTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator($rule, array(
            'rules',
            'versionDescription',
            'crew'
        ), $ruleArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/rules/'.$rule->getId(), $ruleArray);
        
            $this->failure($rule);
            $result = $this->adapter->edit($rule);
            $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR,
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
        ];

        $this->adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = RuleRestfulAdapter::MAP_ERROR;
        
        $result = $this->adapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    public function testVersionRestoreSuccess()
    {
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator($rule, array('crew'), $ruleArray);
        $id = $rule->getId();
        $ruleVersionId = $rule->getRuleVersion()->getId();

        $this->adapter
            ->method('patch')
            ->with(
                'resourceCatalogs/rules/'.$id.'/versionRestore/'.$ruleVersionId,
                $ruleArray
            );

        $this->success($rule);
        $result = $this->adapter->versionRestore($rule);
        $this->assertTrue($result);
    }

    public function testVersionRestoreFailure()
    {
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator($rule, array('crew'), $ruleArray);

        $id = $rule->getId();
        $ruleVersionId = $rule->getRuleVersion()->getId();

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/rules/'.$id.'/versionRestore/'.$ruleVersionId, $ruleArray);
        
        $this->failure($rule);
        $result = $this->adapter->versionRestore($rule);
        $this->assertFalse($result);
    }

    public function testDeletedSuccess()
    {
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator($rule, array('crew'), $ruleArray);
        $id = $rule->getId();

        $this->adapter
            ->method('delete')
            ->with(
                'resourceCatalogs/rules/'.$id.'/delete',
                $ruleArray
            );

        $this->success($rule);
        $result = $this->adapter->deleted($rule);
        $this->assertTrue($result);
    }

    public function testDeletedFailure()
    {
        $rule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator($rule, array('crew'), $ruleArray);

        $id = $rule->getId();
        $ruleVersionId = $rule->getRuleVersion()->getId();

        $this->adapter
            ->method('delete')
            ->with('resourceCatalogs/rules/'.$id.'/delete', $ruleArray);
        
        $this->failure($rule);
        $result = $this->adapter->deleted($rule);
        $this->assertFalse($result);
    }
}
