<?php
namespace Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Model\NullUnAuditRule;
use Sdk\ResourceCatalog\Rule\Adapter\Rule\RuleRestfulAdapter;
use Sdk\ResourceCatalog\Rule\Translator\UnAuditRuleRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UnAuditRuleRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockUnAuditRuleRestfulAdapter::class)
                           ->setMethods([
                               'commonMapErrors',
                               'getTranslator',
                               'isSuccess',
                               'patch',
                               'translateToObject'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new UnAuditRuleRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIUnAuditRuleAdapter()
    {
        $adapter = new UnAuditRuleRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule\IUnAuditRuleAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockUnAuditRuleRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Translator\UnAuditRuleRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockUnAuditRuleRestfulAdapter();
        $this->assertEquals('resourceCatalogs/unAuditedRules', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockUnAuditRuleRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\NullUnAuditRule',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockUnAuditRuleRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UN_AUDIT_RULE_LIST',
                UnAuditRuleRestfulAdapter::SCENARIOS['UN_AUDIT_RULE_LIST']
            ],
            [
                'UN_AUDIT_RULE_FETCH_ONE',
                UnAuditRuleRestfulAdapter::SCENARIOS['UN_AUDIT_RULE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    /**
     * 为UnAuditRuleRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$rule,$keys,$ruleArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareUnAuditRuleTranslator(
        UnAuditRule $unAuditedRule,
        array $keys,
        array $ruleArray
    ) {
        $translator = $this->prophesize(UnAuditRuleRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($unAuditedRule),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($ruleArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(UnAuditRule $unAuditedRule)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($unAuditedRule);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }

    public function testAdd()
    {
        $result = $this->adapter->add(new UnAuditRule());
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareRuleTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $unAuditedRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule(1);
        $ruleArray = array();

        $this->prepareUnAuditRuleTranslator($unAuditedRule, array(
            'versionDescription',
            'rules',
            'crew'
        ), $ruleArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedRules/'.$unAuditedRule->getId().'/resubmit', $ruleArray);

        $this->success($unAuditedRule);
        $result = $this->adapter->edit($unAuditedRule);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareRuleTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $unAuditedRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule(1);
        $ruleArray = array();

        $this->prepareUnAuditRuleTranslator($unAuditedRule, array(
            'versionDescription',
            'rules',
            'crew'
        ), $ruleArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedRules/'.$unAuditedRule->getId().'/resubmit', $ruleArray);
        
            $this->failure($unAuditedRule);
            $result = $this->adapter->edit($unAuditedRule);
            $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = RuleRestfulAdapter::MAP_ERROR;
        
        $result = $this->adapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    public function testApproveSuccess()
    {
        $unAuditedRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule(1);
        $ruleArray = array();

        $this->prepareUnAuditRuleTranslator($unAuditedRule, array('applyCrew'), $ruleArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedRules/'.$unAuditedRule->getId().'/approve', $ruleArray);

        $this->success($unAuditedRule);
        $result = $this->adapter->approve($unAuditedRule);
        $this->assertTrue($result);
    }

    public function testApproveFailure()
    {
        $unAuditedRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule(1);
        $ruleArray = array();

        $this->prepareUnAuditRuleTranslator($unAuditedRule, array('applyCrew'), $ruleArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedRules/'.$unAuditedRule->getId().'/approve', $ruleArray);
        
            $this->failure($unAuditedRule);
            $result = $this->adapter->approve($unAuditedRule);
            $this->assertFalse($result);
    }

    public function testRejectSuccess()
    {
        $unAuditedRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule(1);
        $ruleArray = array();

        $this->prepareUnAuditRuleTranslator($unAuditedRule, array('rejectReason','applyCrew'), $ruleArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedRules/'.$unAuditedRule->getId().'/reject', $ruleArray);

        $this->success($unAuditedRule);
        $result = $this->adapter->reject($unAuditedRule);
        $this->assertTrue($result);
    }

    public function testRejectFailure()
    {
        $unAuditedRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule(1);
        $ruleArray = array();

        $this->prepareUnAuditRuleTranslator($unAuditedRule, array('rejectReason','applyCrew'), $ruleArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedRules/'.$unAuditedRule->getId().'/reject', $ruleArray);
        
            $this->failure($unAuditedRule);
            $result = $this->adapter->reject($unAuditedRule);
            $this->assertFalse($result);
    }

    public function testRevokeSuccess()
    {
        $unAuditedRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule(1);
        $unAuditedRuleArray = array();

        $this->prepareUnAuditRuleTranslator($unAuditedRule, array('crew'), $unAuditedRuleArray);
        $id = $unAuditedRule->getId();

        $this->adapter
            ->method('patch')
            ->with(
                'resourceCatalogs/unAuditedRules/'.$id.'/revoke',
                $unAuditedRuleArray
            );

        $this->success($unAuditedRule);
        $result = $this->adapter->revoke($unAuditedRule);
        $this->assertTrue($result);
    }

    public function testRevokeFailure()
    {
        $unAuditedRule = \Sdk\ResourceCatalog\Rule\Utils\MockFactory::generateUnAuditRule(1);
        $unAuditedRuleArray = array();

        $this->prepareUnAuditRuleTranslator($unAuditedRule, array('crew'), $unAuditedRuleArray);

        $id = $unAuditedRule->getId();

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedRules/'.$id.'/revoke', $unAuditedRuleArray);
        
        $this->failure($unAuditedRule);
        $result = $this->adapter->revoke($unAuditedRule);
        $this->assertFalse($result);
    }
}
