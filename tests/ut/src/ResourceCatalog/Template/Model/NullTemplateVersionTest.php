<?php
namespace Sdk\ResourceCatalog\Template\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullTemplateVersionTest extends TestCase
{
    private $nullTemplateVersion;

    public function setUp()
    {
        $this->nullTemplateVersion = NullTemplateVersion::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullTemplateVersion);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsTemplateVersion()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\TemplateVersion',
            $this->nullTemplateVersion
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullTemplateVersion
        );
    }
}
