<?php
namespace Sdk\ResourceCatalog\Template\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Common\Model\IApplyAble;

class UnAuditTemplateTest extends TestCase
{
    private $unAuditTemplate;

    public function setUp()
    {
        $this->unAuditTemplate = new MockUnAuditTemplate();
    }

    public function tearDown()
    {
        unset($this->unAuditTemplate);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals('', $this->unAuditTemplate->getRejectReason());
        $this->assertEquals(0, $this->unAuditTemplate->getApplyInfoType());
        $this->assertEquals(0, $this->unAuditTemplate->getRelationId());
        $this->assertInstanceOf('Sdk\User\Crew\Model\Crew', $this->unAuditTemplate->getPublishCrew());
        $this->assertInstanceOf('Sdk\User\Crew\Model\Crew', $this->unAuditTemplate->getApplyCrew());
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Model\UserGroup',
            $this->unAuditTemplate->getPublishUserGroup()
        );
        $this->assertInstanceOf('Sdk\UserGroup\UserGroup\Model\UserGroup', $this->unAuditTemplate->getApplyUserGroup());
        $this->assertEquals(IApplyAble::OPERATION_TYPE['NULL'], $this->unAuditTemplate->getOperationType());
        $this->assertEquals(IApplyAble::APPLY_STATUS['PENDING'], $this->unAuditTemplate->getApplyStatus());
    }

    public function testGetUnAuditTemplateRepository()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Repository\UnAuditTemplateRepository',
            $this->unAuditTemplate->getUnAuditTemplateRepository()
        );
    }

    public function testGetIApplyAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Repository\UnAuditTemplateRepository',
            $this->unAuditTemplate->getIApplyAbleAdapter()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Repository\UnAuditTemplateRepository',
            $this->unAuditTemplate->getIOperateAbleAdapter()
        );
    }
}
