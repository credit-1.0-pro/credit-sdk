<?php
namespace Sdk\ResourceCatalog\Template\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;

use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class TemplateTest extends TestCase
{
    private $template;

    public function setUp()
    {
        $this->template = new MockTemplate();
    }

    public function tearDown()
    {
        unset($this->template);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testTemplateConstructor()
    {
        $this->assertEquals(0, $this->template->getId());
        $this->assertEmpty($this->template->getName());
        $this->assertEmpty($this->template->getIdentify());
        $this->assertEquals(array(), $this->template->getSubjectCategory());
        $this->assertEquals(Template::DIMENSION['SHGK'], $this->template->getDimension());
        $this->assertEmpty($this->template->getExchangeFrequency());
        $this->assertEmpty($this->template->getInfoClassify());
        $this->assertEmpty($this->template->getInfoCategory());
        $this->assertEquals('', $this->template->getDescription());
        $this->assertEquals('', $this->template->getVersionDescription());
        $this->assertEquals(array(), $this->template->getSourceUnits());
        $this->assertEquals(array(), $this->template->getItems());

        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\TemplateVersion',
            $this->template->getTemplateVersion()
        );
        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Crew',
            $this->template->getCrew()
        );
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Model\UserGroup',
            $this->template->getUserGroup()
        );
        $this->assertEquals(Template::STATUS_NORMAL, $this->template->getStatus());
        $this->assertEquals(0, $this->template->getUpdateTime());
        $this->assertEquals(0, $this->template->getCreateTime());
        $this->assertEquals(0, $this->template->getStatusTime());
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Template setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->template->setId(1);
        $this->assertEquals(1, $this->template->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 ------------------------------------------------------- start
    /**
     * 设置 Template setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->template->setName('string');
        $this->assertEquals('string', $this->template->getName());
    }

    /**
     * 设置 Template setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->template->setName(array(1, 2, 3));
    }
    //name 测试 -------------------------------------------------------   end

    //identify 测试 ------------------------------------------------------ start
    /**
     * 设置 Journal setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->template->setIdentify('string');
        $this->assertEquals('string', $this->template->getIdentify());
    }

    /**
     * 设置 Journal setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->template->setIdentify(array(1, 2, 3));
    }
    //identify 测试 ------------------------------------------------------   end

    //subjectCategory 测试 ------------------------------------------------------ start
    public function testSetSubjectCategoryCorrectType()
    {
        $this->template->setSubjectCategory(array('subjectCategory'));
        $this->assertEquals(array('subjectCategory'), $this->template->getSubjectCategory());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetSubjectCategoryWrongType()
    {
        $this->template->setSubjectCategory('subjectCategory');
    }
    //subjectCategory 测试 ------------------------------------------------------   end

    //dimension 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Template setDimension() 是否符合预定范围
     *
     * @dataProvider dimensionProvider
     */
    public function testSetDimension($actual, $expected)
    {
        $this->template->setDimension($actual);
        $this->assertEquals($expected, $this->template->getDimension());
    }

    /**
     * 循环测试 Template setDimension() 数据构建器
     */
    public function dimensionProvider()
    {
        return array(
            array(Template::DIMENSION['SHGK'],Template::DIMENSION['SHGK']),
            array(Template::DIMENSION['ZWGX'],Template::DIMENSION['ZWGX']),
            array(Template::DIMENSION['SQCX'],Template::DIMENSION['SQCX']),
            array(999,Template::DIMENSION['SHGK']),
        );
    }

    /**
     * 设置 Template setDimension() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDimensionWrongType()
    {
        $this->template->setDimension('string');
    }
    //dimension 测试 ------------------------------------------------------   end

    //exchangeFrequency 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Template setExchangeFrequency() 是否符合预定范围
     *
     * @dataProvider exchangeFrequencyProvider
     */
    public function testSetExchangeFrequency($actual, $expected)
    {
        $this->template->setExchangeFrequency($actual);
        $this->assertEquals($expected, $this->template->getExchangeFrequency());
    }

    /**
     * 循环测试 Template setExchangeFrequency() 数据构建器
     */
    public function exchangeFrequencyProvider()
    {
        return array(
            array(Template::EXCHANGE_FREQUENCY['SS'],Template::EXCHANGE_FREQUENCY['SS']),
            array(Template::EXCHANGE_FREQUENCY['MR'],Template::EXCHANGE_FREQUENCY['MR']),
            array(Template::EXCHANGE_FREQUENCY['MZ'],Template::EXCHANGE_FREQUENCY['MZ']),
            array(Template::EXCHANGE_FREQUENCY['MY'],Template::EXCHANGE_FREQUENCY['MY']),
            array(Template::EXCHANGE_FREQUENCY['MJD'],Template::EXCHANGE_FREQUENCY['MJD']),
            array(Template::EXCHANGE_FREQUENCY['MBN'],Template::EXCHANGE_FREQUENCY['MBN']),
            array(Template::EXCHANGE_FREQUENCY['MYN'],Template::EXCHANGE_FREQUENCY['MYN']),
            array(Template::EXCHANGE_FREQUENCY['MLN'],Template::EXCHANGE_FREQUENCY['MLN']),
            array(Template::EXCHANGE_FREQUENCY['MSN'],Template::EXCHANGE_FREQUENCY['MSN']),
        );
    }

    /**
     * 设置 Template setExchangeFrequency() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetExchangeFrequencyWrongType()
    {
        $this->template->setExchangeFrequency('string');
    }
    //exchangeFrequency 测试 ------------------------------------------------------   end

    //infoClassify 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Template setInfoClassify() 是否符合预定范围
     *
     * @dataProvider infoClassifyProvider
     */
    public function testSetInfoClassify($actual, $expected)
    {
        $this->template->setInfoClassify($actual);
        $this->assertEquals($expected, $this->template->getInfoClassify());
    }

    /**
     * 循环测试 Template setInfoClassify() 数据构建器
     */
    public function infoClassifyProvider()
    {
        return array(
            array(Template::INFO_CLASSIFY['XZXK'],Template::INFO_CLASSIFY['XZXK']),
            array(Template::INFO_CLASSIFY['XZCF'],Template::INFO_CLASSIFY['XZCF']),
            array(Template::INFO_CLASSIFY['HONGMD'],Template::INFO_CLASSIFY['HONGMD']),
            array(Template::INFO_CLASSIFY['HEIMD'],Template::INFO_CLASSIFY['HEIMD']),
            array(Template::INFO_CLASSIFY['QT'],Template::INFO_CLASSIFY['QT']),
        );
    }

    /**
     * 设置 Template setInfoClassify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoClassifyWrongType()
    {
        $this->template->setInfoClassify('string');
    }
    //infoClassify 测试 ------------------------------------------------------   end

    //infoCategory 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Template setInfoCategory() 是否符合预定范围
     *
     * @dataProvider infoCategoryProvider
     */
    public function testSetInfoCategory($actual, $expected)
    {
        $this->template->setInfoCategory($actual);
        $this->assertEquals($expected, $this->template->getInfoCategory());
    }

    /**
     * 循环测试 Template setInfoCategory() 数据构建器
     */
    public function infoCategoryProvider()
    {
        return array(
            array(Template::INFO_CATEGORY['JCXX'],Template::INFO_CATEGORY['JCXX']),
            array(Template::INFO_CATEGORY['SHOUXXX'],Template::INFO_CATEGORY['SHOUXXX']),
            array(Template::INFO_CATEGORY['SHIXXX'],Template::INFO_CATEGORY['SHIXXX']),
            array(Template::INFO_CATEGORY['QTXX'],Template::INFO_CATEGORY['QTXX']),
        );
    }

    /**
     * 设置 Template setInfoCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoCategoryWrongType()
    {
        $this->template->setInfoCategory('string');
    }
    //infoCategory 测试 ------------------------------------------------------   end

    //description 测试 ----------------------------------------------------- start
    /**
     * 设置 Template setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->template->setDescription('string');
        $this->assertEquals('string', $this->template->getDescription());
    }

    /**
     * 设置 Template setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->template->setDescription(array(1, 2, 3));
    }
    //description 测试 -----------------------------------------------------   end

    //versionDescription 测试 ----------------------------------------------------- start
    /**
     * 设置 Template setVersionDescription() 正确的传参类型,期望传值正确
     */
    public function testSetVersionDescriptionCorrectType()
    {
        $this->template->setVersionDescription('string');
        $this->assertEquals('string', $this->template->getVersionDescription());
    }

    /**
     * 设置 Template setVersionDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVersionDescriptionWrongType()
    {
        $this->template->setVersionDescription(array(1, 2, 3));
    }
    //versionDescription 测试 -----------------------------------------------------   end

    //sourceUnits 测试 --------------------------------------------- start
    /**
     * 设置 Template addSourceUnit() 正确的传参类型,期望传值正确
     */
    public function testAddSourceUnitCorrectType()
    {
        $object = new UserGroup(1);
        $this->template->addSourceUnit($object);
        $this->assertSame([$object], $this->template->getSourceUnits());
    }

    /**
     * 设置 Template addSourceUnit() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testAddSourceUnitWrongType()
    {
        $this->template->addSourceUnit('string');
    }

    public function testClearSourceUnits()
    {
        $this->template->clearSourceUnits();
        $this->assertEmpty($this->template->getSourceUnits());
    }
    //sourceUnits 测试 ---------------------------------------------   end

    //items 测试 ------------------------------------------------------ start
    public function testSetItemsCorrectType()
    {
        $this->template->setItems(array('items'));
        $this->assertEquals(array('items'), $this->template->getItems());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetItemsWrongType()
    {
        $this->template->setItems('items');
    }
    //items 测试 ------------------------------------------------------   end

    //templateVersion 测试 --------------------------------------------- start
    /**
     * 设置 Template setTemplateVersion() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateVersionCorrectType()
    {
        $object = new TemplateVersion();
        $this->template->setTemplateVersion($object);
        $this->assertSame($object, $this->template->getTemplateVersion());
    }

    /**
     * 设置 Template setTemplateVersion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateVersionWrongType()
    {
        $this->template->setTemplateVersion('string');
    }
    //templateVersion 测试 ---------------------------------------------   end

    //crew 测试 --------------------------------------------- start
    /**
     * 设置 Template setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();
        $this->template->setCrew($object);
        $this->assertSame($object, $this->template->getCrew());
    }

    /**
     * 设置 Template setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->template->setCrew('string');
    }
    //crew 测试 ---------------------------------------------   end

    //userGroup 测试 --------------------------------------------- start
    /**
     * 设置 Template setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $object = new UserGroup();
        $this->template->setUserGroup($object);
        $this->assertSame($object, $this->template->getUserGroup());
    }

    /**
     * 设置 Template setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->template->setUserGroup('string');
    }
    //userGroup 测试 ---------------------------------------------   end

    //status 测试 ----------------------------------------------------- start
    /**
     * 设置 Template setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->template->setStatus(1);
        $this->assertEquals(1, $this->template->getStatus());
    }

    /**
     * 设置 Template setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->template->setStatus(array(1, 2, 3));
    }
    //status 测试 -----------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Repository\TemplateRepository',
            $this->template->getRepository()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Repository\TemplateRepository',
            $this->template->getIOperateAbleAdapter()
        );
    }

    public function testVersionRestore()
    {
        $this->stub = $this->getMockBuilder(MockTemplate::class)
            ->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(TemplateRepository::class);
        $repository->versionRestore(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->versionRestore();
        $this->assertTrue($result);
    }
}
