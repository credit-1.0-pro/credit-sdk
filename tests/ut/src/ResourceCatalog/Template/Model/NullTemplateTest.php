<?php
namespace Sdk\ResourceCatalog\Template\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullTemplateTest extends TestCase
{
    private $nullTemplate;

    public function setUp()
    {
        $this->nullTemplate = NullTemplate::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullTemplate);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\Template',
            $this->nullTemplate
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullTemplate
        );
    }

    public function testResourceNotExist()
    {
        $nullTemplate = new MockNullTemplate();

        $result = $nullTemplate->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testVersionRestore()
    {
        $nullTemplate = new MockNullTemplate();

        $result = $nullTemplate->versionRestore();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
