<?php
namespace Sdk\ResourceCatalog\Template\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullUnAuditTemplateTest extends TestCase
{
    private $nullUnAuditTemplate;

    public function setUp()
    {
        $this->nullUnAuditTemplate = NullUnAuditTemplate::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullUnAuditTemplate);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsUnAuditTemplate()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\UnAuditTemplate',
            $this->nullUnAuditTemplate
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullUnAuditTemplate
        );
    }

    public function testResourceNotExist()
    {
        $nullUnAuditTemplate = new MockNullUnAuditTemplate();

        $result = $nullUnAuditTemplate->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
