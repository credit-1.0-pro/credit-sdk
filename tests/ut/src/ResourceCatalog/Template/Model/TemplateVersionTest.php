<?php
namespace Sdk\ResourceCatalog\Template\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Model\NullCrew;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

use Sdk\ResourceCatalog\Template\Adapter\TemplateVersion\ITemplateVersionAdapter;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class TemplateVersionTest extends TestCase
{
    private $templateVersion;

    public function setUp()
    {
        $this->templateVersion = new TemplateVersion();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->templateVersion);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->templateVersion
        );
    }

    public function testTemplateVersionConstructor()
    {
        $this->assertEquals(0, $this->templateVersion->getId());
        $this->assertEmpty($this->templateVersion->getNumber());
        $this->assertEquals('', $this->templateVersion->getDescription());
        $this->assertEmpty($this->templateVersion->getTemplateId());
        $this->assertEquals(array(), $this->templateVersion->getInfo());

        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Crew',
            $this->templateVersion->getCrew()
        );
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Model\UserGroup',
            $this->templateVersion->getUserGroup()
        );
        $this->assertEquals(TemplateVersion::STATUS['DISABLED'], $this->templateVersion->getStatus());
        $this->assertEquals(0, $this->templateVersion->getUpdateTime());
        $this->assertEquals(0, $this->templateVersion->getCreateTime());
        $this->assertEquals(0, $this->templateVersion->getStatusTime());
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 TemplateVersion setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->templateVersion->setId(1);
        $this->assertEquals(1, $this->templateVersion->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //number 测试 ------------------------------------------------------- start
    /**
     * 设置 TemplateVersion setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->templateVersion->setNumber('string');
        $this->assertEquals('string', $this->templateVersion->getNumber());
    }

    /**
     * 设置 TemplateVersion setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->templateVersion->setNumber(array(1, 2, 3));
    }
    //number 测试 -------------------------------------------------------   end

    //description 测试 ----------------------------------------------------- start
    /**
     * 设置 TemplateVersion setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->templateVersion->setDescription('string');
        $this->assertEquals('string', $this->templateVersion->getDescription());
    }

    /**
     * 设置 TemplateVersion setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->templateVersion->setDescription(array(1, 2, 3));
    }
    //description 测试 -----------------------------------------------------   end

    //info 测试 ------------------------------------------------------ start
    public function testSetInfoCorrectType()
    {
        $this->templateVersion->setInfo(array('info'));
        $this->assertEquals(array('info'), $this->templateVersion->getInfo());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetInfoWrongType()
    {
        $this->templateVersion->setInfo('info');
    }
    //info 测试 ------------------------------------------------------   end

    //crew 测试 --------------------------------------------- start
    /**
     * 设置 TemplateVersion setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();
        $this->templateVersion->setCrew($object);
        $this->assertSame($object, $this->templateVersion->getCrew());
    }

    /**
     * 设置 TemplateVersion setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->templateVersion->setCrew('string');
    }
    //crew 测试 ---------------------------------------------   end

    //userGroup 测试 --------------------------------------------- start
    /**
     * 设置 TemplateVersion setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $object = new UserGroup();
        $this->templateVersion->setUserGroup($object);
        $this->assertSame($object, $this->templateVersion->getUserGroup());
    }

    /**
     * 设置 TemplateVersion setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->templateVersion->setUserGroup('string');
    }
    //userGroup 测试 ---------------------------------------------   end

    //templateId 测试 ----------------------------------------------------- start
    /**
     * 设置 TemplateVersion setTemplateId() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateIdCorrectType()
    {
        $this->templateVersion->setTemplateId(1);
        $this->assertEquals(1, $this->templateVersion->getTemplateId());
    }

    /**
     * 设置 TemplateVersion setTemplateId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateIdWrongType()
    {
        $this->templateVersion->setTemplateId(array(1, 2, 3));
    }
    //templateId 测试 -----------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 TemplateVersion setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->templateVersion->setStatus($actual);
        $this->assertEquals($expected, $this->templateVersion->getStatus());
    }

    /**
     * 循环测试 TemplateVersion setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(TemplateVersion::STATUS['ENABLED'],TemplateVersion::STATUS['ENABLED']),
            array(TemplateVersion::STATUS['DISABLED'],TemplateVersion::STATUS['DISABLED']),
            array(9999,TemplateVersion::STATUS['DISABLED']),
        );
    }

    /**
     * 设置 TemplateVersion setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->templateVersion->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end
}
