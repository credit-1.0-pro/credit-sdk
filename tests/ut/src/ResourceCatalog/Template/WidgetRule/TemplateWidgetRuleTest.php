<?php
namespace Sdk\ResourceCatalog\Template\WidgetRules;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Sdk\Common\Utils\StringGenerate;

use Sdk\ResourceCatalog\Template\Model\Template;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class TemplateWidgetRulesTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new TemplateWidgetRules();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //name -- start
    /**
     * @dataProvider invalidNameProvider
     */
    public function testNameInvalid($actual, $expected)
    {
        $result = $this->widgetRule->name($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TEMPLATE_NAME_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidNameProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(TemplateWidgetRules::NAME_MIN_LENGTH-1), false),
            array(StringGenerate::generate(TemplateWidgetRules::NAME_MAX_LENGTH+1), false),
            array(StringGenerate::generate(TemplateWidgetRules::NAME_MIN_LENGTH), true),
            array(StringGenerate::generate(TemplateWidgetRules::NAME_MIN_LENGTH+1), true),
            array(StringGenerate::generate(TemplateWidgetRules::NAME_MAX_LENGTH), true),
            array(StringGenerate::generate(TemplateWidgetRules::NAME_MAX_LENGTH-1), true)
        );
    }
    //name -- end

    //identify -- start
    /**
     * @dataProvider invalidIdentifyProvider
     */
    public function testIdentifyInvalid($actual, $expected)
    {
        $result = $this->widgetRule->identify($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TEMPLATE_IDENTIFY_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidIdentifyProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array('', false),
            array($faker->regexify('[A-Z][A-Z_]{101,200}[A-Z]'), false),
            array($faker->regexify('[_][A-Z_]{1,100}'), false),
            array($faker->regexify('[A-Z_]{-100,0}'), false),
            array($faker->regexify('[A-Z][A-Z_]{0,98}[A-Z]$'), true),
        );
    }
    //identify -- end

    //description -- start
    /**
     * @dataProvider invalidDescriptionProvider
     */
    public function testDescriptionInvalid($actual, $expected)
    {
        $result = $this->widgetRule->description($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TEMPLATE_DESCRIPTION_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidDescriptionProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(TemplateWidgetRules::DESCRIPTION_MIN_LENGTH-1), false),
            array(StringGenerate::generate(TemplateWidgetRules::DESCRIPTION_MAX_LENGTH+1), false),
            array(StringGenerate::generate(TemplateWidgetRules::DESCRIPTION_MIN_LENGTH), true),
            array(StringGenerate::generate(TemplateWidgetRules::DESCRIPTION_MIN_LENGTH+1), true),
            array(StringGenerate::generate(TemplateWidgetRules::DESCRIPTION_MAX_LENGTH), true),
            array(StringGenerate::generate(TemplateWidgetRules::DESCRIPTION_MAX_LENGTH-1), true)
        );
    }
    //description -- end

    //subjectCategory -- start
    /**
     * @dataProvider invalidSubjectCategoryProvider
     */
    public function testSubjectCategoryInvalid($actual, $expected)
    {
        $result = $this->widgetRule->subjectCategory($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TEMPLATE_SUBJECT_CATEGORY_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidSubjectCategoryProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array(array(), false),
            array($faker->word, false),
            array($faker->randomElement(Template::SUBJECT_CATEGORY, 1), false),
            array($faker->randomElements(array(7,8,9), 1), false),
            array($faker->randomElements(Template::SUBJECT_CATEGORY, 1), true),
            array($faker->randomElements(Template::SUBJECT_CATEGORY, 2), true),
            array($faker->randomElements(Template::SUBJECT_CATEGORY, 3), true)
        );
    }
    //subjectCategory -- end

    //sourceUnitIds -- start
    /**
     * @dataProvider invalidSourceUnitIdsProvider
     */
    public function testSourceUnitIdsInvalid($actual, $expected)
    {
        $result = $this->widgetRule->sourceUnitIds($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TEMPLATE_SOURCE_UNITS_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidSourceUnitIdsProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array(array(), false),
            array($faker->word(), false),
            array($faker->randomElements(array(7,8,9), 2), true),
            array($faker->randomElements(array('a', 'b', 'c'), 2), false)
        );
    }
    //sourceUnitIds -- end

    //dimension -- start
    /**
     * @dataProvider invalidDimensionProvider
     */
    public function testDimensionInvalid($actual, $expected)
    {
        $result = $this->widgetRule->dimension($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TEMPLATE_DIMENSION_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidDimensionProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array('', false),
            array($faker->word, false),
            array($faker->randomElements(Template::DIMENSION, 1), false),
            array($faker->randomElement(Template::DIMENSION, 1), true),
        );
    }
    //dimension -- end

    //exchangeFrequency -- start
    /**
     * @dataProvider invalidExchangeFrequencyProvider
     */
    public function testExchangeFrequencyInvalid($actual, $expected)
    {
        $result = $this->widgetRule->exchangeFrequency($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TEMPLATE_EXCHANGE_FREQUENCY_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidExchangeFrequencyProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array('', false),
            array($faker->word, false),
            array($faker->randomElements(Template::EXCHANGE_FREQUENCY, 1), false),
            array($faker->randomElement(Template::EXCHANGE_FREQUENCY, 1), true),
        );
    }
    //exchangeFrequency -- end

    //infoClassify -- start
    /**
     * @dataProvider invalidInfoClassifyProvider
     */
    public function testInfoClassifyInvalid($actual, $expected)
    {
        $result = $this->widgetRule->infoClassify($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TEMPLATE_INFO_CLASSIFY_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidInfoClassifyProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array('', false),
            array($faker->word, false),
            array($faker->randomElements(Template::INFO_CLASSIFY, 1), false),
            array($faker->randomElement(Template::INFO_CLASSIFY, 1), true),
        );
    }
    //infoClassify -- end

    //infoCategory -- start
    /**
     * @dataProvider invalidInfoCategoryProvider
     */
    public function testInfoCategoryInvalid($actual, $expected)
    {
        $result = $this->widgetRule->infoCategory($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TEMPLATE_INFO_CATEGORY_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidInfoCategoryProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array('', false),
            array($faker->word, false),
            array($faker->randomElements(Template::INFO_CATEGORY, 1), false),
            array($faker->randomElement(Template::INFO_CATEGORY, 1), true),
        );
    }
    //infoCategory -- end

    //items -- start
    /**
     * @dataProvider invalidItemsProvider
     */
    public function testItemsInvalid($subjectCategory, $actual, $expected, $errorId)
    {
        $result = $this->widgetRule->items($subjectCategory, $actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals($errorId, Core::getLastError()->getId());
    }

    public function invalidItemsProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array(Template::SUBJECT_CATEGORY, array(), false, TEMPLATE_ITEMS_FORMAT_ERROR),
            array(Template::SUBJECT_CATEGORY, $faker->word, false, TEMPLATE_ITEMS_FORMAT_ERROR),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false, TEMPLATE_ITEMS_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false, TEMPLATE_ITEMS_NAME_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => '',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false, TEMPLATE_ITEMS_IDENTIFY_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 0,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false, TEMPLATE_ITEMS_TYPE_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false, TEMPLATE_ITEMS_LENGTH_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 6,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false, TEMPLATE_ITEMS_OPTIONS_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 6,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false, TEMPLATE_ITEMS_DIMENSION_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 2,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(1, 2),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false, TEMPLATE_IS_NECESSARY_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 2,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false, TEMPLATE_IS_MASKED_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false, TEMPLATE_MASKED_RULE_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => StringGenerate::generate(TemplateWidgetRules::REMARKS_MAX_LENGTH+1),    //备注
                    )
                ),
                false, TEMPLATE_REMARKS_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '统一社会信用代码',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                ),
                false, TEMPLATE_CARD_ID_NOT_EXIT
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '身份证号',    //信息项名称
                        "identify" => 'ZJHM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                ),
                false, TEMPLATE_UNIFIED_SOCIAL_CREDIT_CODE_NOT_EXIT
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'NAME',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false, TEMPLATE_SUBJECT_NAME_NOT_EXIT
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '认定依据',    //信息项名称
                        "identify" => 'RDYJ',    //数据标识
                        "type" => 3,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(
                            '认定依据'
                        ),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '认定依据',    //备注
                    )
                ),
                false, TEMPLATE_ITEMS_OPTIONS_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '认定依据',    //信息项名称
                        "identify" => 'RDYJ',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(1, 2),    //脱敏规则
                        "remarks" => '认定依据',    //备注
                    )
                ),
                false, TEMPLATE_MASKED_RULE_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY['FRJFFRZZ'],
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '统一社会信用代码',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '认定机关',    //信息项名称
                        "identify" => 'RDJG',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '',    //备注
                    ),
                    array(
                        "name" => '认定机关统一社会信用代码',    //信息项名称
                        "identify" => 'RDJG',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '',    //备注
                    ),
                ),
                false, TEMPLATE_ITEMS_IDENTIFY_IS_UNIQUE
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '统一社会信用代码',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '18',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '',    //备注
                    ),
                    array(
                        "name" => '公开范围',    //信息项名称
                        "identify" => 'GKFW',    //数据标识
                        "type" => 5,    //数据类型
                        "length" => '20',    //数据长度
                        "options" => array(
                            "社会公开",
                            "政务共享",
                            "授权查询"
                        ),    //可选范围
                        "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则，即左边保留1位字符，右边保留2位字符
                        "remarks" => '',    //备注
                    )
                ),
                false, TEMPLATE_CARD_ID_NOT_EXIT
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '18',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '',    //备注
                    ),
                    array(
                        "name" => '公开范围',    //信息项名称
                        "identify" => 'GKFW',    //数据标识
                        "type" => 5,    //数据类型
                        "length" => '20',    //数据长度
                        "options" => array(
                            "社会公开",
                            "政务共享"
                        ),    //可选范围
                        "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则，即左边保留1位字符，右边保留2位字符
                        "remarks" => '支持单选',    //备注
                    )
                ),
                false, TEMPLATE_ITEMS_NAME_IS_UNIQUE
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '18',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '信用主体代码',    //备注
                    )
                ),
                false, TEMPLATE_ITEMS_NAME_IS_UNIQUE
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '统一社会信用代码',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '18',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '统一社会信用代码',    //备注
                    ),
                    array(
                        "name" => '认定日期',    //信息项名称
                        "identify" => 'GKFW',    //数据标识
                        "type" => 5,    //数据类型
                        "length" => '20',    //数据长度
                        "options" => array(
                            "社会公开",
                            "政务共享",
                            "授权查询"
                        ),    //可选范围
                        "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则，即左边保留1位字符，右边保留2位字符
                        "remarks" => '',    //备注
                    )
                ),
                false, TEMPLATE_CARD_ID_NOT_EXIT
            ),
            array(
                array(),
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false, TEMPLATE_ITEMS_IDENTIFY_FORMAT_ERROR
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '统一社会信用代码',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '18',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '信用主体代码',    //备注
                    ),
                    array(
                        "name" => '身份证号',    //信息项名称
                        "identify" => 'ZJHM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '18',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '信用主体代码',    //备注
                    ),
                    array(
                        "name" => '认定日期',    //信息项名称
                        "identify" => 'RDRQ',    //数据标识
                        "type" => 2,    //数据类型
                        "length" => '',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '认定日期',    //备注
                    )
                ),
                true, ''
            )
        );
    }
    //items -- end
}
