<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Utils\TemplateUtils;

class UnAuditTemplateTranslatorTest extends TestCase
{
    use TemplateUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditTemplateTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Model\NullUnAuditTemplate', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $unAuditTemplate = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateUnAuditTemplate(1);

        $expression = $this->translator->objectToArray($unAuditTemplate);
    
        $this->compareArrayAndObjectUnAuditedTemplate($expression, $unAuditTemplate);
    }

    public function testObjectToArrayFail()
    {
        $unAuditTemplate = null;

        $expression = $this->translator->objectToArray($unAuditTemplate);
        $this->assertEquals(array(), $expression);
    }
}
