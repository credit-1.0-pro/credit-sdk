<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Utils\TemplateRestfulUtils;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

class TemplateRestfulTranslatorTest extends TestCase
{
    use TemplateRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new TemplateRestfulTranslator();

        Core::$container->set('crew', new Crew());
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $translator = new MockTemplateRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator',
            $translator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $translator = new MockTemplateRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\User\Crew\Translator\CrewRestfulTranslator',
            $translator->getCrewRestfulTranslator()
        );
    }

    public function testGetTemplateVersionRestfulTranslator()
    {
        $translator = new MockTemplateRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator',
            $translator->getTemplateVersionRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate(1);

        $expression['data']['id'] = $template->getId();
        $expression['data']['attributes']['name'] = $template->getName();
        $expression['data']['attributes']['identify'] = $template->getIdentify();
        $expression['data']['attributes']['description'] = $template->getDescription();
        $expression['data']['attributes']['subjectCategory'] = $template->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $template->getDimension();
        $expression['data']['attributes']['exchangeFrequency'] = $template->getExchangeFrequency();
        $expression['data']['attributes']['infoClassify'] = $template->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $template->getInfoCategory();
        $expression['data']['attributes']['items'] = $template->getItems();
        $expression['data']['attributes']['createTime'] = $template->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $template->getUpdateTime();
        $expression['data']['attributes']['status'] = $template->getStatus();
        $expression['data']['attributes']['statusTime'] = $template->getStatusTime();

        $templateObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Model\Template', $templateObject);
        $this->compareArrayAndObjectCommon($expression, $templateObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $template = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Model\NullTemplate', $template);
    }

    public function testObjectToArray()
    {
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate(1);

        $expression = $this->translator->objectToArray($template);

        $this->compareArrayAndObjectCommon($expression, $template);
    }

    public function testObjectToArrayFail()
    {
        $template = null;

        $expression = $this->translator->objectToArray($template);
        $this->assertEquals(array(), $expression);
    }

    public function testArrayToObjectWithIncluded()
    {
        $translator = $this->getMockBuilder(MockTemplateRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getCrewRestfulTranslator',
                        'getUserGroupRestfulTranslator',
                        'getTemplateVersionRestfulTranslator'
                    ])
                    ->getMock();


        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup'],
            'templateVersion'=>['data'=>'mockTemplateVersion'],
            'sourceUnits'=>['data'=>array('mockSourceUnits')]
        ];

        $crewInfo = ['mockCrewInfo'];
        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];
        $templateVersionInfo = ['mockTemplateVersionInfo'];
        $sourceUnitInfo = ['mockSourceUnitInfo'];

        $crew = new Crew();
        $publishUserGroup = new UserGroup();
        $templateVersion = new TemplateVersion();
        $sourceUnit = new UserGroup(1);
        $sourceUnits = array($sourceUnit);

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(4))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['publishUserGroup']['data']],
                 [$relationships['templateVersion']['data']],
                 [$relationships['sourceUnits']['data'][0]]
             ) ->will($this->onConsecutiveCalls(
                 $crewInfo,
                 $publishUserGroupInfo,
                 $templateVersionInfo,
                 $sourceUnitInfo
             ));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($publishUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishUserGroup);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($sourceUnitInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceUnit);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $versionRestfulTranslator = $this->prophesize(TemplateVersionRestfulTranslator::class);
        $versionRestfulTranslator->arrayToObject(Argument::exact($templateVersionInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($templateVersion);
        //绑定
        $translator->expects($this->exactly(2))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTemplateVersionRestfulTranslator')
            ->willReturn($versionRestfulTranslator->reveal());

        //揭示
        $template = $translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Model\Template', $template);
        $this->assertEquals($crew, $template->getCrew());
        $this->assertEquals($publishUserGroup, $template->getUserGroup());
        $this->assertEquals($templateVersion, $template->getTemplateVersion());
        $this->assertEquals($sourceUnits, $template->getSourceUnits());
    }
}
