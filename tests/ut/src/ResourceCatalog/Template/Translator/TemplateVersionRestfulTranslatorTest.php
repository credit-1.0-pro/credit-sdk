<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Utils\TemplateRestfulUtils;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

class TemplateVersionRestfulTranslatorTest extends TestCase
{
    use TemplateRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new TemplateVersionRestfulTranslator();

        Core::$container->set('crew', new Crew());
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $translator = new MockTemplateVersionRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator',
            $translator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $translator = new MockTemplateVersionRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\User\Crew\Translator\CrewRestfulTranslator',
            $translator->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $templateVersion = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplateVersion(1);

        $expression['data']['id'] = $templateVersion->getId();
        $expression['data']['attributes']['number'] = $templateVersion->getNumber();
        $expression['data']['attributes']['description'] = $templateVersion->getDescription();
        $expression['data']['attributes']['templateId'] = $templateVersion->getTemplateId();
        $expression['data']['attributes']['info'] = $templateVersion->getInfo();
        $expression['data']['attributes']['status'] = $templateVersion->getStatus();
        $expression['data']['attributes']['createTime'] = $templateVersion->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $templateVersion->getUpdateTime();
        $expression['data']['attributes']['statusTime'] = $templateVersion->getStatusTime();

        $templateVersionObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Model\TemplateVersion', $templateVersionObject);
        $this->compareArrayAndObjectTemplateVersion($expression, $templateVersionObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $templateVersion = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Model\NullTemplateVersion', $templateVersion);
    }

    public function testObjectToArray()
    {
        $templateVersion = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplateVersion(1);

        $expression = $this->translator->objectToArray($templateVersion);

        $this->compareArrayAndObjectTemplateVersion($expression, $templateVersion);
    }

    public function testObjectToArrayFail()
    {
        $templateVersion = null;

        $expression = $this->translator->objectToArray($templateVersion);
        $this->assertEquals(array(), $expression);
    }

    public function testArrayToObjectWithIncluded()
    {
        $translator = $this->getMockBuilder(MockTemplateVersionRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getCrewRestfulTranslator',
                        'getUserGroupRestfulTranslator',
                    ])
                    ->getMock();


        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup']
        ];

        $crewInfo = ['mockCrewInfo'];
        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];

        $crew = new Crew(0);
        $publishUserGroup = new UserGroup(0);

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(2))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['publishUserGroup']['data']]
             ) ->will($this->onConsecutiveCalls(
                 $crewInfo,
                 $publishUserGroupInfo
             ));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($publishUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishUserGroup);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        //揭示
        $templateVersion = $translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Model\TemplateVersion', $templateVersion);
        $this->assertEquals($crew, $templateVersion->getCrew());
        $this->assertEquals($publishUserGroup, $templateVersion->getUserGroup());
    }
}
