<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Utils\TemplateRestfulUtils;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

class UnAuditTemplateRestfulTranslatorTest extends TestCase
{
    use TemplateRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditTemplateRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $unAuditTemplate = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateUnAuditTemplate(1);

        $expression['data']['id'] = $unAuditTemplate->getId();

        $expression['data']['attributes']['name'] = $unAuditTemplate->getName();
        $expression['data']['attributes']['identify'] = $unAuditTemplate->getIdentify();
        $expression['data']['attributes']['description'] = $unAuditTemplate->getDescription();
        $expression['data']['attributes']['subjectCategory'] = $unAuditTemplate->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $unAuditTemplate->getDimension();
        $expression['data']['attributes']['exchangeFrequency'] = $unAuditTemplate->getExchangeFrequency();
        $expression['data']['attributes']['infoClassify'] = $unAuditTemplate->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $unAuditTemplate->getInfoCategory();
        $expression['data']['attributes']['items'] = $unAuditTemplate->getItems();
        $expression['data']['attributes']['relationId'] = $unAuditTemplate->getRelationId();
        $expression['data']['attributes']['applyStatus'] = $unAuditTemplate->getApplyStatus();
        $expression['data']['attributes']['rejectReason'] = $unAuditTemplate->getRejectReason();
        $expression['data']['attributes']['operationType'] = $unAuditTemplate->getOperationType();
        $expression['data']['attributes']['applyInfoType'] = $unAuditTemplate->getApplyInfoType();
        $expression['data']['attributes']['createTime'] = $unAuditTemplate->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $unAuditTemplate->getUpdateTime();
        $expression['data']['attributes']['status'] = $unAuditTemplate->getStatus();
        $expression['data']['attributes']['statusTime'] = $unAuditTemplate->getStatusTime();

        $unAuditTemplateObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Model\UnAuditTemplate', $unAuditTemplateObject);
        $this->compareArrayAndObjectCommonUnAuditTemplate($expression, $unAuditTemplateObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $unAuditTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Model\NullUnAuditTemplate', $unAuditTemplate);
    }

    public function testObjectToArray()
    {
        $unAuditTemplate = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateUnAuditTemplate(1);

        $expression = $this->translator->objectToArray($unAuditTemplate);
     
        $this->compareArrayAndObjectCommonUnAuditTemplate($expression, $unAuditTemplate);
    }

    public function testObjectToArrayFail()
    {
        $unAuditTemplate = null;

        $expression = $this->translator->objectToArray($unAuditTemplate);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        $translator = $this->getMockBuilder(UnAuditTemplateRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getCrewRestfulTranslator',
                        'getUserGroupRestfulTranslator',
                        'getTemplateVersionRestfulTranslator',
                    ])
                    ->getMock();

        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'publishCrew'=>['data'=>'mockPublishCrew'],
            'applyCrew'=>['data'=>'mockApplyCrew'],
            'applyUserGroup'=>['data'=>'mockApplyUserGroup'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup'],
            'crew'=>['data'=>'mockCrew'],
            'templateVersion'=>['data'=>'mockTemplateVersion'],
            'sourceUnits'=>['data'=>array('mockSourceUnits')]
        ];

        $publishCrewInfo = ['mockPublishCrewInfo'];
        $crewInfo = ['mockCrewInfo'];
        $relationInfo = ['mockRelationInfo'];
        $applyCrewInfo = ['mockApplyCrewInfo'];
        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];
        $applyUserGroupInfo = ['mockApplyUserGroupInfo'];
        $templateVersionInfo = ['mockTemplateVersionInfo'];
        $sourceUnitInfo = ['mockSourceUnitInfo'];

        $crew = new Crew();
        $applyCrew = new Crew();
        $publishCrew = new Crew();
        $publishUserGroup = new UserGroup();
        $applyUserGroup = new UserGroup();
        $templateVersion = new TemplateVersion();
        $sourceUnit = new UserGroup(1);
        $sourceUnits = array($sourceUnit);

        //预言
        $translator->expects($this->exactly(2))
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用5次, 依次入参 department, userGroup
        //依次返回 $departmentInfo 和 $userGroupInfo
        $translator->expects($this->exactly(7))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['publishUserGroup']['data']],
                 [$relationships['templateVersion']['data']],
                 [$relationships['sourceUnits']['data'][0]],
                 [$relationships['applyCrew']['data']],
                 [$relationships['applyUserGroup']['data']],
                 [$relationships['publishCrew']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $crewInfo,
                $publishUserGroupInfo,
                $templateVersionInfo,
                $sourceUnitInfo,
                $applyCrewInfo,
                $applyUserGroupInfo,
                $publishCrewInfo
            ));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($publishUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishUserGroup);

        $userGroupRestfulTranslator->arrayToObject(Argument::exact($applyUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyUserGroup);
                                    
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($sourceUnitInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceUnit);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $crewRestfulTranslator->arrayToObject(Argument::exact($publishCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishCrew);

        $crewRestfulTranslator->arrayToObject(Argument::exact($applyCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyCrew);
        
        $versionRestfulTranslator = $this->prophesize(TemplateVersionRestfulTranslator::class);
        $versionRestfulTranslator->arrayToObject(Argument::exact($templateVersionInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($templateVersion);
        //绑定
        $translator->expects($this->exactly(3))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(3))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getTemplateVersionRestfulTranslator')
            ->willReturn($versionRestfulTranslator->reveal());
       
        //揭示
        $unAuditTemplate = $translator->arrayToObject($expression);
         
        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Model\UnAuditTemplate', $unAuditTemplate);
        $this->assertEquals($crew, $unAuditTemplate->getCrew());
        $this->assertEquals($publishCrew, $unAuditTemplate->getPublishCrew());
        $this->assertEquals($applyCrew, $unAuditTemplate->getApplyCrew());
        $this->assertEquals($publishUserGroup, $unAuditTemplate->getPublishUserGroup());
        $this->assertEquals($applyUserGroup, $unAuditTemplate->getApplyUserGroup());
        $this->assertEquals($templateVersion, $unAuditTemplate->getTemplateVersion());
        $this->assertEquals($sourceUnits, $unAuditTemplate->getSourceUnits());
    }
}
