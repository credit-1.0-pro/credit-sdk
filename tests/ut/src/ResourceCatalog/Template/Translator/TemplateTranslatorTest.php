<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Utils\TemplateUtils;

class TemplateTranslatorTest extends TestCase
{
    use TemplateUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new TemplateTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Model\NullTemplate', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate(1);

        $expression = $this->translator->objectToArray($template);
    
        $this->compareArrayAndObjectTemplate($expression, $template);
    }

    public function testObjectToArrayFail()
    {
        $template = null;

        $expression = $this->translator->objectToArray($template);
        $this->assertEquals(array(), $expression);
    }
}
