<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Utils\TemplateUtils;

class TemplateVersionTranslatorTest extends TestCase
{
    use TemplateUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new TemplateVersionTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Model\NullTemplateVersion', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $templateVersion = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplateVersion(1);

        $expression = $this->translator->objectToArray($templateVersion);
    
        $this->compareArrayAndObjectTemplateVersion($expression, $templateVersion);
    }

    public function testObjectToArrayFail()
    {
        $templateVersion = null;

        $expression = $this->translator->objectToArray($templateVersion);
        $this->assertEquals(array(), $expression);
    }
}
