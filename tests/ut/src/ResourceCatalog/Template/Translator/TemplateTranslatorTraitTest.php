<?php
namespace Sdk\ResourceCatalog\Template\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Model\Template;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class TemplateTranslatorTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockTemplateTranslatorTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetSubjectCategoryCn()
    {
        $subjectCategory = array(Template::SUBJECT_CATEGORY['FRJFFRZZ']);
        $data = array(
            array(
                'id' => marmot_encode(Template::SUBJECT_CATEGORY['FRJFFRZZ']),
                'name' => Template::SUBJECT_CATEGORY_CN[Template::SUBJECT_CATEGORY['FRJFFRZZ']]
            )
        );

        $result = $this->trait->publicGetSubjectCategoryCn($subjectCategory);

        $this->assertEquals($result, $data);
    }

    public function testGetDimensionCn()
    {
        $dimension = Template::DIMENSION['SHGK'];
        $data = array(
            'id' => marmot_encode($dimension),
            'name' => Template::DIMENSION_CN[Template::DIMENSION['SHGK']]
        );

        $result = $this->trait->publicGetDimensionCn($dimension);

        $this->assertEquals($result, $data);
    }

    public function testGetExchangeFrequencyCn()
    {
        $exchangeFrequency = Template::EXCHANGE_FREQUENCY['SS'];
        $data = array(
            'id' => marmot_encode($exchangeFrequency),
            'name' => Template::EXCHANGE_FREQUENCY_CN[Template::EXCHANGE_FREQUENCY['SS']]
        );

        $result = $this->trait->publicGetExchangeFrequencyCn($exchangeFrequency);

        $this->assertEquals($result, $data);
    }

    public function testGetInfoClassifyCn()
    {
        $infoClassify = Template::INFO_CLASSIFY['XZXK'];
        $data = array(
            'id' => marmot_encode($infoClassify),
            'name' => Template::INFO_CLASSIFY_CN[Template::INFO_CLASSIFY['XZXK']]
        );

        $result = $this->trait->publicGetInfoClassifyCn($infoClassify);

        $this->assertEquals($result, $data);
    }

    public function testGetInfoCategoryCn()
    {
        $infoCategory = Template::INFO_CATEGORY['JCXX'];
        $data = array(
            'id' => marmot_encode($infoCategory),
            'name' => Template::INFO_CATEGORY_CN[Template::INFO_CATEGORY['JCXX']]
        );

        $result = $this->trait->publicGetInfoCategoryCn($infoCategory);

        $this->assertEquals($result, $data);
    }

    public function testGetItemCn()
    {
        $items = array(
            array(
                'isNecessary' => Template::IS_NECESSARY['FOU'],
                'type' => Template::TYPE['ZFX'],
                'isMasked' => Template::IS_MASKED['FOU'],
                'dimension' => Template::DIMENSION['SHGK'],
            ),
            array(
                'name' => 'name',
                'identify' => 'identify'
            )
        );

        $data = array(
            array(
                'isNecessary' => array(
                    'id' => marmot_encode(Template::IS_NECESSARY['FOU']),
                    'name' => Template::IS_NECESSARY_CN[Template::IS_NECESSARY['FOU']]
                ),
                'type' => array(
                    'id' => marmot_encode(Template::TYPE['ZFX']),
                    'name' => Template::TYPE_CN[Template::TYPE['ZFX']]
                ),
                'isMasked' => array(
                    'id' => marmot_encode(Template::IS_MASKED['FOU']),
                    'name' => Template::IS_MASKED_CN[Template::IS_MASKED['FOU']]
                ),
                'dimension' => array(
                    'id' => marmot_encode(Template::DIMENSION['SHGK']),
                    'name' => Template::DIMENSION_CN[Template::DIMENSION['SHGK']]
                )
            ),
            array(
                'name' => 'name',
                'identify' => 'identify',
                'isNecessary' => array(),
                'type' => array(),
                'isMasked' => array(),
                'dimension' => array(),
            )
        );

        $result = $this->trait->publicGetItemCn($items);

        $this->assertEquals($result, $data);
    }

    public function testGetInfoCnEmpty()
    {
        $info = array();

        $data = array(
            'items' => array(),
            'dimension' => array(),
            'infoCategory' => array(),
            'infoClassify' => array(),
            'subjectCategory' => array(),
            'exchangeFrequency' => array(),
            'sourceUnits' => array()
        );
        
        $result = $this->trait->publicGetInfoCn($info);

        $this->assertEquals($result, $data);
    }

    public function testGetInfoCn()
    {
        $info = array(
            'items' => array(array(
                'name' => 'name',
                'identify' => 'identify',
                'isNecessary' => Template::IS_NECESSARY['SHI'],
                'type' => Template::TYPE['ZFX'],
                'isMasked' => Template::IS_MASKED['SHI'],
                'dimension' => Template::DIMENSION['SHGK'],
            )),
            'dimension' => Template::DIMENSION['SHGK'],
            'info_category' => Template::INFO_CATEGORY['JCXX'],
            'info_classify' => Template::INFO_CLASSIFY['XZXK'],
            'exchange_frequency' => Template::EXCHANGE_FREQUENCY['SS'],
            'subject_category' => array(Template::SUBJECT_CATEGORY['FRJFFRZZ']),
            'source_units' => array('source_units'),
        );

        $data = array(
            'items' => array(array(
                'name' => 'name',
                'identify' => 'identify',
                'isNecessary' => array(
                    'id' => marmot_encode(Template::IS_NECESSARY['SHI']),
                    'name' => Template::IS_NECESSARY_CN[Template::IS_NECESSARY['SHI']]
                ),
                'type' => array(
                    'id' => marmot_encode(Template::TYPE['ZFX']),
                    'name' => Template::TYPE_CN[Template::TYPE['ZFX']]
                ),
                'isMasked' => array(
                    'id' => marmot_encode(Template::IS_MASKED['SHI']),
                    'name' => Template::IS_MASKED_CN[Template::IS_MASKED['SHI']]
                ),
                'dimension' => array(
                    'id' => marmot_encode(Template::DIMENSION['SHGK']),
                    'name' => Template::DIMENSION_CN[Template::DIMENSION['SHGK']]
                )
            )),
            'dimension' => array(
                'id' => marmot_encode(Template::DIMENSION['SHGK']),
                'name' => Template::DIMENSION_CN[Template::DIMENSION['SHGK']]
            ),
            'infoCategory' => array(
                'id' => marmot_encode(Template::INFO_CATEGORY['JCXX']),
                'name' => Template::INFO_CATEGORY_CN[Template::INFO_CATEGORY['JCXX']]
            ),
            'infoClassify' => array(
                'id' => marmot_encode(Template::INFO_CLASSIFY['XZXK']),
                'name' => Template::INFO_CLASSIFY_CN[Template::INFO_CLASSIFY['XZXK']]
            ),
            'exchangeFrequency' => array(
                'id' => marmot_encode(Template::EXCHANGE_FREQUENCY['SS']),
                'name' => Template::EXCHANGE_FREQUENCY_CN[Template::EXCHANGE_FREQUENCY['SS']]
            ),
            'subjectCategory' => array(
                array(
                    'id' => marmot_encode(Template::SUBJECT_CATEGORY['FRJFFRZZ']),
                    'name' => Template::SUBJECT_CATEGORY_CN[Template::SUBJECT_CATEGORY['FRJFFRZZ']]
                )
            ),
            'sourceUnits' => array('source_units'),
        );

        $result = $this->trait->publicGetInfoCn($info);

        $this->assertEquals($result, $data);
    }
}
