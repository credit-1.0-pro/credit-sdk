<?php
namespace Sdk\ResourceCatalog\Template\Adapter\Template;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\NullTemplate;
use Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class TemplateRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockTemplateRestfulAdapter::class)
                           ->setMethods([
                               'commonMapErrors',
                               'getTranslator',
                               'post',
                               'isSuccess',
                               'patch',
                               'translateToObject'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new TemplateRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsITemplateAdapter()
    {
        $adapter = new TemplateRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Adapter\Template\ITemplateAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockTemplateRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Translator\TemplateRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockTemplateRestfulAdapter();
        $this->assertEquals('resourceCatalogs/templates', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockTemplateRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\NullTemplate',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockTemplateRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'TEMPLATE_LIST',
                TemplateRestfulAdapter::SCENARIOS['TEMPLATE_LIST']
            ],
            [
                'TEMPLATE_FETCH_ONE',
                TemplateRestfulAdapter::SCENARIOS['TEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    /**
     * 为TemplateRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$template,$keys,$templateArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareTemplateTranslator(
        Template $template,
        array $keys,
        array $templateArray
    ) {
        $translator = $this->prophesize(TemplateRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($template),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($templateArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Template $template)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($template);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate(1);
        $templateArray = array();

        $this->prepareTemplateTranslator(
            $template,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'versionDescription',
                'items',
                'sourceUnits',
                'crew'
            ),
            $templateArray
        );

        $this->adapter
            ->method('post')
            ->with('resourceCatalogs/templates', $templateArray);

        $this->success($template);
        $result = $this->adapter->add($template);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate(1);
        $templateArray = array();

        $this->prepareTemplateTranslator(
            $template,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'versionDescription',
                'items',
                'sourceUnits',
                'crew'
            ),
            $templateArray
        );

        $this->adapter
            ->method('post')
            ->with('resourceCatalogs/templates', $templateArray);
        
            $this->failure($template);
            $result = $this->adapter->add($template);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareTemplateTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate(1);
        $templateArray = array();

        $this->prepareTemplateTranslator($template, array(
            'name',
            'identify',
            'subjectCategory',
            'dimension',
            'exchangeFrequency',
            'infoClassify',
            'infoCategory',
            'description',
            'versionDescription',
            'items',
            'sourceUnits',
            'crew'
        ), $templateArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/templates/'.$template->getId(), $templateArray);

        $this->success($template);
        $result = $this->adapter->edit($template);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareTemplateTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate(1);
        $templateArray = array();

        $this->prepareTemplateTranslator($template, array(
            'name',
            'identify',
            'subjectCategory',
            'dimension',
            'exchangeFrequency',
            'infoClassify',
            'infoCategory',
            'description',
            'versionDescription',
            'items',
            'sourceUnits',
            'crew'
        ), $templateArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/templates/'.$template->getId(), $templateArray);
        
            $this->failure($template);
            $result = $this->adapter->edit($template);
            $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR,
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
        ];

        $this->adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = TemplateRestfulAdapter::MAP_ERROR;
        
        $result = $this->adapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    public function testVersionRestoreSuccess()
    {
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate(1);
        $templateArray = array();

        $this->prepareTemplateTranslator($template, array('crew'), $templateArray);
        $id = $template->getId();
        $templateVersionId = $template->getTemplateVersion()->getId();

        $this->adapter
            ->method('patch')
            ->with(
                'resourceCatalogs/templates/'.$id.'/versionRestore/'.$templateVersionId,
                $templateArray
            );

        $this->success($template);
        $result = $this->adapter->versionRestore($template);
        $this->assertTrue($result);
    }

    public function testVersionRestoreFailure()
    {
        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate(1);
        $templateArray = array();

        $this->prepareTemplateTranslator($template, array('crew'), $templateArray);

        $id = $template->getId();
        $templateVersionId = $template->getTemplateVersion()->getId();

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/templates/'.$id.'/versionRestore/'.$templateVersionId, $templateArray);
        
        $this->failure($template);
        $result = $this->adapter->versionRestore($template);
        $this->assertFalse($result);
    }
}
