<?php
namespace Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;
use Sdk\ResourceCatalog\Template\Model\NullUnAuditTemplate;
use Sdk\ResourceCatalog\Template\Adapter\Template\TemplateRestfulAdapter;
use Sdk\ResourceCatalog\Template\Translator\UnAuditTemplateRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UnAuditTemplateRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockUnAuditTemplateRestfulAdapter::class)
                           ->setMethods([
                               'commonMapErrors',
                               'getTranslator',
                               'isSuccess',
                               'patch',
                               'translateToObject'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new UnAuditTemplateRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIUnAuditTemplateAdapter()
    {
        $adapter = new UnAuditTemplateRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate\IUnAuditTemplateAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockUnAuditTemplateRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Translator\UnAuditTemplateRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockUnAuditTemplateRestfulAdapter();
        $this->assertEquals('resourceCatalogs/unAuditedTemplates', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockUnAuditTemplateRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\NullUnAuditTemplate',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockUnAuditTemplateRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UN_AUDIT_TEMPLATE_LIST',
                UnAuditTemplateRestfulAdapter::SCENARIOS['UN_AUDIT_TEMPLATE_LIST']
            ],
            [
                'UN_AUDIT_TEMPLATE_FETCH_ONE',
                UnAuditTemplateRestfulAdapter::SCENARIOS['UN_AUDIT_TEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    /**
     * 为UnAuditTemplateRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$template,$keys,$templateArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareUnAuditTemplateTranslator(
        UnAuditTemplate $unAuditedTemplate,
        array $keys,
        array $templateArray
    ) {
        $translator = $this->prophesize(UnAuditTemplateRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($unAuditedTemplate),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($templateArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(UnAuditTemplate $unAuditedTemplate)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($unAuditedTemplate);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }

    public function testAdd()
    {
        $result = $this->adapter->add(new UnAuditTemplate());
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareTemplateTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $unAuditedTemplate = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateUnAuditTemplate(1);
        $templateArray = array();

        $this->prepareUnAuditTemplateTranslator($unAuditedTemplate, array(
            'name',
            'identify',
            'subjectCategory',
            'dimension',
            'exchangeFrequency',
            'infoClassify',
            'infoCategory',
            'description',
            'versionDescription',
            'items',
            'sourceUnits',
            'crew'
        ), $templateArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedTemplates/'.$unAuditedTemplate->getId().'/resubmit', $templateArray);

        $this->success($unAuditedTemplate);
        $result = $this->adapter->edit($unAuditedTemplate);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareTemplateTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $unAuditedTemplate = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateUnAuditTemplate(1);
        $templateArray = array();

        $this->prepareUnAuditTemplateTranslator($unAuditedTemplate, array(
            'name',
            'identify',
            'subjectCategory',
            'dimension',
            'exchangeFrequency',
            'infoClassify',
            'infoCategory',
            'description',
            'versionDescription',
            'items',
            'sourceUnits',
            'crew'
        ), $templateArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedTemplates/'.$unAuditedTemplate->getId().'/resubmit', $templateArray);
        
            $this->failure($unAuditedTemplate);
            $result = $this->adapter->edit($unAuditedTemplate);
            $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = TemplateRestfulAdapter::MAP_ERROR;
        
        $result = $this->adapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    public function testApproveSuccess()
    {
        $unAuditedTemplate = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateUnAuditTemplate(1);
        $templateArray = array();

        $this->prepareUnAuditTemplateTranslator($unAuditedTemplate, array('applyCrew'), $templateArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedTemplates/'.$unAuditedTemplate->getId().'/approve', $templateArray);

        $this->success($unAuditedTemplate);
        $result = $this->adapter->approve($unAuditedTemplate);
        $this->assertTrue($result);
    }

    public function testApproveFailure()
    {
        $unAuditedTemplate = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateUnAuditTemplate(1);
        $templateArray = array();

        $this->prepareUnAuditTemplateTranslator($unAuditedTemplate, array('applyCrew'), $templateArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedTemplates/'.$unAuditedTemplate->getId().'/approve', $templateArray);
        
            $this->failure($unAuditedTemplate);
            $result = $this->adapter->approve($unAuditedTemplate);
            $this->assertFalse($result);
    }

    public function testRejectSuccess()
    {
        $unAuditedTemplate = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateUnAuditTemplate(1);
        $templateArray = array();

        $this->prepareUnAuditTemplateTranslator($unAuditedTemplate, array('rejectReason','applyCrew'), $templateArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedTemplates/'.$unAuditedTemplate->getId().'/reject', $templateArray);

        $this->success($unAuditedTemplate);
        $result = $this->adapter->reject($unAuditedTemplate);
        $this->assertTrue($result);
    }

    public function testRejectFailure()
    {
        $unAuditedTemplate = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateUnAuditTemplate(1);
        $templateArray = array();

        $this->prepareUnAuditTemplateTranslator($unAuditedTemplate, array('rejectReason','applyCrew'), $templateArray);

        $this->adapter
            ->method('patch')
            ->with('resourceCatalogs/unAuditedTemplates/'.$unAuditedTemplate->getId().'/reject', $templateArray);
        
            $this->failure($unAuditedTemplate);
            $result = $this->adapter->reject($unAuditedTemplate);
            $this->assertFalse($result);
    }
}
