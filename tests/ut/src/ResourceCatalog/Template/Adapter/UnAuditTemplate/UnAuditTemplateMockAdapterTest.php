<?php
namespace Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;

class UnAuditTemplateMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new UnAuditTemplateMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\UnAuditTemplate',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\Template\Model\UnAuditTemplate',
                $each
            );
        }
    }

    public function testFetchOneAsync()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\UnAuditTemplate',
            $this->adapter->fetchOneAsync(1)
        );
    }

    public function testFetchListAsync()
    {
        $list = $this->adapter->fetchListAsync([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\Template\Model\UnAuditTemplate',
                $each
            );
        }
    }

    public function testSearch()
    {
        list($list, $count) = $this->adapter->search(['filter'], ['sort'], 1, 2);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\Template\Model\UnAuditTemplate',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }

    public function testSearchAsync()
    {
        list($list, $count) = $this->adapter->searchAsync(['filter'], ['sort'], 1, 2);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\ResourceCatalog\Template\Model\UnAuditTemplate',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
