<?php
namespace Sdk\ResourceCatalog\Template\Adapter\TemplateVersion;

use PHPUnit\Framework\TestCase;

class TemplateVersionRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockTemplateVersionRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new TemplateVersionRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsITemplateVersionAdapter()
    {
        $adapter = new TemplateVersionRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Adapter\TemplateVersion\ITemplateVersionAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockTemplateVersionRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Translator\TemplateVersionRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockTemplateVersionRestfulAdapter();
        $this->assertEquals('resourceCatalogs/templateVersions', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockTemplateVersionRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\NullTemplateVersion',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockTemplateVersionRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'TEMPLATE_VERSION_LIST',
                TemplateVersionRestfulAdapter::SCENARIOS['TEMPLATE_VERSION_LIST']
            ],
            [
                'TEMPLATE_VERSION_FETCH_ONE',
                TemplateVersionRestfulAdapter::SCENARIOS['TEMPLATE_VERSION_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
}
