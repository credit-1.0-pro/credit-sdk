<?php
namespace Sdk\ResourceCatalog\Template\Utils;

use Sdk\Common\Model\IApplyAble;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Translator\TemplateTranslatorTrait;
use Sdk\ResourceCatalog\Template\Translator\TemplateVersionTranslator;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

trait TemplateUtils
{
    use TemplateTranslatorTrait;

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getTemplateVersionTranslator() : TemplateVersionTranslator
    {
        return new TemplateVersionTranslator();
    }

    private function compareArrayAndObjectTemplate(
        array $expectedArray,
        $template
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $template);
    }

    private function compareArrayAndObjectUnAuditedTemplate(
        array $expectedArray,
        $unAuditedTemplate
    ) {
        $this->assertEquals($expectedArray['rejectReason'], $unAuditedTemplate->getRejectReason());
        $this->assertEquals($expectedArray['relationId'], marmot_encode($unAuditedTemplate->getRelationId()));
        $this->assertEquals($expectedArray['applyInfoType'], marmot_encode($unAuditedTemplate->getApplyInfoType()));
        $this->assertEquals(
            $expectedArray['applyStatus']['id'],
            marmot_encode($unAuditedTemplate->getApplyStatus())
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['name'],
            IApplyAble::APPLY_STATUS_CN[$unAuditedTemplate->getApplyStatus()]
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['type'],
            IApplyAble::APPLY_STATUS_TAG_TYPE[$unAuditedTemplate->getApplyStatus()]
        );
        $this->assertEquals(
            $expectedArray['operationType']['id'],
            marmot_encode($unAuditedTemplate->getOperationType())
        );
        $this->assertEquals(
            $expectedArray['operationType']['name'],
            IApplyAble::OPERATION_TYPE_CN[$unAuditedTemplate->getOperationType()]
        );
        $this->assertEquals(
            $expectedArray['publishCrew'],
            $this->getCrewTranslator()->objectToArray($unAuditedTemplate->getPublishCrew())
        );
        $this->assertEquals(
            $expectedArray['applyCrew'],
            $this->getCrewTranslator()->objectToArray($unAuditedTemplate->getApplyCrew())
        );
        $this->assertEquals(
            $expectedArray['applyUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($unAuditedTemplate->getApplyUserGroup())
        );
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $template
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($template->getId()));
        $this->assertEquals($expectedArray['name'], $template->getName());
        $this->assertEquals($expectedArray['identify'], $template->getIdentify());
        $this->assertEquals($expectedArray['description'], $template->getDescription());
        $this->assertEquals(
            $expectedArray['dimension']['id'],
            marmot_encode($template->getDimension())
        );
        $this->assertEquals(
            $expectedArray['dimension']['name'],
            Template::DIMENSION_CN[$template->getDimension()]
        );
        $this->assertEquals(
            $expectedArray['exchangeFrequency']['id'],
            marmot_encode($template->getExchangeFrequency())
        );
        $this->assertEquals(
            $expectedArray['exchangeFrequency']['name'],
            Template::EXCHANGE_FREQUENCY_CN[$template->getExchangeFrequency()]
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['id'],
            marmot_encode($template->getInfoClassify())
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['name'],
            Template::INFO_CLASSIFY_CN[$template->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['id'],
            marmot_encode($template->getInfoCategory())
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['name'],
            Template::INFO_CATEGORY_CN[$template->getInfoCategory()]
        );
        $this->subjectCategoryEquals($expectedArray, $template);
        $this->itemsEquals($expectedArray, $template);

        $this->assertEquals(
            $expectedArray['crew'],
            $this->getCrewTranslator()->objectToArray($template->getCrew())
        );
        $this->assertEquals(
            $expectedArray['publishUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($template->getUserGroup())
        );
        $this->assertEquals(
            $expectedArray['templateVersion'],
            $this->getTemplateVersionTranslator()->objectToArray($template->getTemplateVersion())
        );

        $sourceUnitsArray = array();
        $sourceUnits = $template->getSourceUnits();
        foreach ($sourceUnits as $sourceUnit) {
            $sourceUnitsArray[] = $this->getUserGroupTranslator()->objectToArray(
                $sourceUnit
            );
        }

        $this->assertEquals($expectedArray['sourceUnits'], $sourceUnitsArray);

        $this->assertEquals($expectedArray['status'], $template->getStatus());

        $this->assertEquals($expectedArray['updateTime'], $template->getUpdateTime());
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $template->getUpdateTime())
        );
                    
        $this->assertEquals($expectedArray['statusTime'], $template->getStatusTime());
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $template->getStatusTime())
        );
                    
        $this->assertEquals($expectedArray['createTime'], $template->getCreateTime());
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $template->getCreateTime())
        );
    }

    private function subjectCategoryEquals($expectedArray, $template)
    {
        $subjectCategory = array();

        if (is_string($expectedArray['subjectCategory'])) {
            $subjectCategory = json_decode($expectedArray['subjectCategory'], true);
        }
        if (is_array($expectedArray['subjectCategory'])) {
            $subjectCategory = $expectedArray['subjectCategory'];
        }

        $this->assertEquals($subjectCategory, $this->getSubjectCategoryCn($template->getSubjectCategory()));
    }

    private function itemsEquals($expectedArray, $template)
    {
        $items = array();

        if (is_string($expectedArray['items'])) {
            $items = json_decode($expectedArray['items'], true);
        }
        if (is_array($expectedArray['items'])) {
            $items = $expectedArray['items'];
        }

        $this->assertEquals($items, $this->getItemCn($template->getItems()));
    }

    private function compareArrayAndObjectTemplateVersion(
        array $expectedArray,
        $templateVersion
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($templateVersion->getId()));
        $this->assertEquals($expectedArray['number'], $templateVersion->getNumber());
        $this->assertEquals($expectedArray['description'], $templateVersion->getDescription());
        $this->assertEquals($expectedArray['templateId'], marmot_encode($templateVersion->getTemplateId()));
        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($templateVersion->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            TemplateVersion::STATUS_CN[$templateVersion->getStatus()]
        );
        $this->infoEquals($expectedArray, $templateVersion);

        $this->assertEquals(
            $expectedArray['crew'],
            $this->getCrewTranslator()->objectToArray($templateVersion->getCrew())
        );
        $this->assertEquals(
            $expectedArray['publishUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($templateVersion->getUserGroup())
        );
        $this->assertEquals($expectedArray['updateTime'], $templateVersion->getUpdateTime());
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $templateVersion->getUpdateTime())
        );
                    
        $this->assertEquals($expectedArray['statusTime'], $templateVersion->getStatusTime());
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $templateVersion->getStatusTime())
        );
                    
        $this->assertEquals($expectedArray['createTime'], $templateVersion->getCreateTime());
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $templateVersion->getCreateTime())
        );
    }

    private function infoEquals($expectedArray, $templateVersion)
    {
        $info = array();

        if (is_string($expectedArray['info'])) {
            $info = json_decode($expectedArray['info'], true);
        }
        if (is_array($expectedArray['info'])) {
            $info = $expectedArray['info'];
        }

        $this->assertEquals($info, $this->getInfoCn($templateVersion->getInfo()));
    }
}
