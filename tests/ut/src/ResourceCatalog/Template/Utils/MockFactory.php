<?php
namespace Sdk\ResourceCatalog\Template\Utils;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;

use Sdk\Common\Model\IApplyAble;

class MockFactory
{
    public static function generateCommon(
        Template $template,
        int $seed = 0,
        array $value = array()
    ) {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        //name
        self::generateName($template, $faker, $value);
        //identify
        self::generateIdentify($template, $faker, $value);
        //subjectCategory
        self::generateSubjectCategory($template, $faker, $value);
        //dimension
        self::generateDimension($template, $faker, $value);
        //exchangeFrequency
        self::generateExchangeFrequency($template, $faker, $value);
        //infoClassify
        self::generateInfoClassify($template, $faker, $value);
        //infoCategory
        self::generateInfoCategory($template, $faker, $value);
        //description
        self::generateDescription($template, $faker, $value);
        //sourceUnits
        self::generateSourceUnits($template, $faker, $value);
        //items
        self::generateItems($template, $faker, $value);
        //templateVersion
        self::generateTemplateVersion(1);
        //crew
        self::generateCrew($template, $faker, $value);
        //publishUserGroup
        $template->setUserGroup($template->getCrew()->getUserGroup());
        //status
        self::generateStatus($template, $faker, $value);
        $template->setCreateTime($faker->unixTime());
        $template->setUpdateTime($faker->unixTime());
        $template->setStatusTime($faker->unixTime());

        return $template;
    }

    protected static function generateName($template, $faker, $value) : void
    {
        $name = isset($value['name']) ? $value['name'] : $faker->word();
        
        $template->setName($name);
    }

    protected static function generateIdentify($template, $faker, $value) : void
    {
        $identify = isset($value['identify']) ?
            $value['identify'] :
            $faker->regexify('[A-Z_]{1,100}');
        
        $template->setIdentify($identify);
    }

    protected static function generateSubjectCategory($template, $faker, $value) : void
    {
        $subjectCategory = isset($value['subjectCategory']) ?
            $value['subjectCategory'] :
            $faker->randomElements(Template::SUBJECT_CATEGORY, 2);
        
        $template->setSubjectCategory($subjectCategory);
    }

    protected static function generateDimension($template, $faker, $value) : void
    {
        $dimension = isset($value['dimension']) ?
            $value['dimension'] :
            $faker->randomElement(
                Template::DIMENSION
            );
        
        $template->setDimension($dimension);
    }

    protected static function generateExchangeFrequency($template, $faker, $value) : void
    {
        $exchangeFrequency = isset($value['exchangeFrequency']) ?
            $value['exchangeFrequency'] :
            $faker->randomElement(
                Template::EXCHANGE_FREQUENCY
            );
        
        $template->setExchangeFrequency($exchangeFrequency);
    }

    protected static function generateInfoClassify($template, $faker, $value) : void
    {
        $infoClassify = isset($value['infoClassify']) ?
            $value['infoClassify'] :
            $faker->randomElement(
                Template::INFO_CLASSIFY
            );
        
        $template->setInfoClassify($infoClassify);
    }

    protected static function generateInfoCategory($template, $faker, $value) : void
    {
        $infoCategory = isset($value['infoCategory']) ?
            $value['infoCategory'] :
            $faker->randomElement(
                Template::INFO_CATEGORY
            );
        
        $template->setInfoCategory($infoCategory);
    }

    protected static function generateDescription($template, $faker, $value) : void
    {
        $description = isset($value['description']) ?
            $value['description'] :
            $faker->sentence;
        
        $template->setDescription($description);
        $template->setVersionDescription($description);
    }

    protected static function generateItems($template, $faker, $value) : void
    {
        unset($faker);
        $items = isset($value['items']) ?
            $value['items'] :
            array(
                array(
                    "name" => '主体名称',
                    "identify" => 'ZTMC',
                    "type" => 1,
                    "length" => '200',
                    "options" => array(),
                    "dimension" => 1,
                    "isNecessary" => 1,
                    "isMasked" => 1,
                    "maskRule" => array(),
                    "remarks" => '信用主体名称',
                )
            );
        
        $template->setItems($items);
    }

    protected static function generateSourceUnits($template, $faker, $value) : void
    {
        $userGroup = \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup($faker->randomDigit());
        $userGroupList = isset($value['sourceUnits']) ? $value['sourceUnits'] : array($userGroup);
        
        foreach ($userGroupList as $userGroup) {
            $template->addSourceUnit($userGroup);
        }
    }

    protected static function generateCrew($template, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Sdk\User\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $template->setCrew($crew);
    }

    protected static function generateStatus($template, $faker, $value) : void
    {
        unset($faker);
        $status = isset($value['status']) ? $value['status'] : Template::STATUS_NORMAL;
        
        $template->setStatus($status);
    }

    public static function generateTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Template {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $template = new Template($id);
        $template->setId($id);

        $template = self::generateCommon($template, $seed, $value);

        return $template;
    }

    public static function generateUnAuditTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditTemplate = new UnAuditTemplate($id);
        
        $unAuditTemplate->setId($id);

        $unAuditTemplate = self::generateCommon($unAuditTemplate, $seed, $value);

        $unAuditTemplate->setRelationId($id);
        $unAuditTemplate->setApplyUserGroup($unAuditTemplate->getUserGroup());
        $unAuditTemplate->setPublishCrew($unAuditTemplate->getCrew());
        $unAuditTemplate->setApplyCrew($unAuditTemplate->getCrew());
         //rejectReason
        $rejectReason = isset($value['rejectReason']) ? $value['rejectReason'] : '内容不合理';
        $unAuditTemplate->setRejectReason($rejectReason);

        //applyStatus
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : $faker->randomElement(
            IApplyAble::APPLY_STATUS
        );
        $unAuditTemplate->setApplyStatus($applyStatus);

        //operationType
        $operationType = isset($value['operationType']) ? $value['operationType'] : $faker->randomElement(
            IApplyAble::OPERATION_TYPE
        );
        $unAuditTemplate->setOperationType($operationType);
        
        return $unAuditTemplate;
    }

    public static function generateTemplateVersion(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : TemplateVersion {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $templateVersion = new TemplateVersion($id);

        $templateVersion->setCreateTime($faker->unixTime());
        $templateVersion->setUpdateTime($faker->unixTime());
        $templateVersion->setStatusTime($faker->unixTime());

        //number
        $number = isset($value['number']) ? $value['number'] : date(
            'ymd',
            $templateVersion->getCreateTime()
        ).rand(0000, 9999);
        $templateVersion->setNumber($number);
        //description
        $description = isset($value['description']) ? $value['description'] : $faker->sentence();
        $templateVersion->setDescription($description);
        //templateId
        $templateId = isset($value['templateId']) ? $value['templateId'] : $faker->randomDigit();
        $templateVersion->setTemplateId($templateId);
        //info
        $info = isset($value['info']) ? $value['info'] : array('info');
        $templateVersion->setInfo($info);
        //crew
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Sdk\User\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $templateVersion->setCrew($crew);
        $templateVersion->setUserGroup($crew->getUserGroup());

        //status
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(
            TemplateVersion::STATUS
        );
        $templateVersion->setStatus($status);

        return $templateVersion;
    }
}
