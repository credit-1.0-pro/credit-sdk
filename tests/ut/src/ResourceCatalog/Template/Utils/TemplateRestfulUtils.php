<?php
namespace Sdk\ResourceCatalog\Template\Utils;

trait TemplateRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $object
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $object->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['name'], $object->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $object->getIdentify());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $object->getDescription());

        if (isset($expectedArray['data']['attributes']['versionDescription'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['versionDescription'],
                $object->getVersionDescription()
            );
        }
        $this->assertEquals($expectedArray['data']['attributes']['subjectCategory'], $object->getSubjectCategory());
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $object->getDimension());
        $this->assertEquals(
            $expectedArray['data']['attributes']['exchangeFrequency'],
            $object->getExchangeFrequency()
        );
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $object->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $object->getInfoCategory());
        $this->assertEquals($expectedArray['data']['attributes']['items'], $object->getItems());

        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $object->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $object->getStatusTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $object->getUpdateTime());
        }
        if (isset($expectedArray['data']['attributes']['status'])) {
            $this->assertEquals($expectedArray['data']['attributes']['status'], $object->getStatus());
        }

        $this->compareCrew($expectedArray, $object);
        $this->comparePublishUserGroup($expectedArray, $object);
        $this->compareTemplateVersion($expectedArray, $object);
        $this->compareSourceUnits($expectedArray, $object);
    }

    private function compareCrew(array $expectedArray, $object)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['crew']['data'])) {
                $this->assertEquals(
                    $relationships['crew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['crew']['data'][0]['id'],
                    $object->getCrew()->getId()
                );
            }
        }
    }

    private function comparePublishUserGroup(array $expectedArray, $object)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['publishUserGroup']['data'])) {
                $this->assertEquals(
                    $relationships['publishUserGroup']['data'][0]['type'],
                    'userGroups'
                );
                $this->assertEquals(
                    $relationships['publishUserGroup']['data'][0]['id'],
                    $object->getUserGroup()->getId()
                );
            }
        }
    }

    private function compareTemplateVersion(array $expectedArray, $object)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['templateVersion']['data'])) {
                $this->assertEquals(
                    $relationships['templateVersion']['data'][0]['type'],
                    'templateVersions'
                );
                $this->assertEquals(
                    $relationships['templateVersion']['data'][0]['id'],
                    $object->getTemplateVersion()->getId()
                );
            }
        }
    }

    private function compareSourceUnits(array $expectedArray, $object)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $sourceUnitIds = array();
            $sourceUnits = $object->getSourceUnits();

            foreach ($sourceUnits as $sourceUnit) {
                $sourceUnitIds[] = $sourceUnit->getId();
            }
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['sourceUnits']['data'])) {
                foreach ($relationships['sourceUnits']['data'] as $key => $value) {
                    $this->assertEquals($value['type'], 'userGroups');
                    $this->assertEquals($value['id'], $sourceUnitIds[$key]);
                }
            }
        }
    }

    private function compareArrayAndObjectCommonUnAuditTemplate(
        array $expectedArray,
        $unAuditedTemplate
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditedTemplate);
        
        if (isset($expectedArray['data']['attributes']['applyStatus'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['applyStatus'],
                $unAuditedTemplate->getApplyStatus()
            );
        }
        if (isset($expectedArray['data']['attributes']['operationType'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['operationType'],
                $unAuditedTemplate->getOperationType()
            );
        }
        if (isset($expectedArray['data']['attributes']['applyInfoType'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['applyInfoType'],
                $unAuditedTemplate->getApplyInfoType()
            );
        }
        if (isset($expectedArray['data']['attributes']['relationId'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['relationId'],
                $unAuditedTemplate->getRelationId()
            );
        }

        $this->assertEquals(
            $expectedArray['data']['attributes']['rejectReason'],
            $unAuditedTemplate->getRejectReason()
        );

        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['applyCrew']['data'])) {
                $this->assertEquals(
                    $relationships['applyCrew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['applyCrew']['data'][0]['id'],
                    $unAuditedTemplate->getApplyCrew()->getId()
                );
            }
        }
    }

    private function compareArrayAndObjectTemplateVersion(
        array $expectedArray,
        $templateVersion
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $templateVersion->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['number'], $templateVersion->getNumber());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $templateVersion->getDescription());
        $this->assertEquals($expectedArray['data']['attributes']['templateId'], $templateVersion->getTemplateId());
        $this->assertEquals($expectedArray['data']['attributes']['info'], $templateVersion->getInfo());
       
        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $templateVersion->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $templateVersion->getStatusTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $templateVersion->getUpdateTime());
        }
        if (isset($expectedArray['data']['attributes']['status'])) {
            $this->assertEquals($expectedArray['data']['attributes']['status'], $templateVersion->getStatus());
        }

        $this->compareCrew($expectedArray, $templateVersion);
        $this->comparePublishUserGroup($expectedArray, $templateVersion);
    }
}
