<?php
namespace Sdk\ResourceCatalog\Template\Command\UnAuditTemplate;

use PHPUnit\Framework\TestCase;

class ApproveUnAuditTemplateCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->stub = new ApproveUnAuditTemplateCommand(
            $faker->randomNumber()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectExtendsOperationCommand()
    {
        $this->assertInstanceof('Sdk\Common\Command\ApproveCommand', $this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
