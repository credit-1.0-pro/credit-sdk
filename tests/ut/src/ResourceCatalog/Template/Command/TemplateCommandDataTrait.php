<?php
namespace Sdk\ResourceCatalog\Template\Command;

trait TemplateCommandDataTrait
{
    public function getRequestCommonData() : array
    {
        $data = array(
            'name' => '行政许可信息',
            'identify' => 'XZXKXX',
            'description' => '行政许可信息描述',
            'versionDescription' => '行政许可信息归集',
            'subjectCategory' => array(1, 3),
            'items' => array('items'),
            'sourceUnitIds' => array(1, 2, 3),
            "dimension"=> 1,
            "exchangeFrequency"=> 1,
            "infoClassify"=> 1,
            "infoCategory"=> 1
        );

        return $data;
    }
}
