<?php
namespace Sdk\ResourceCatalog\Template\Command\Template;

use PHPUnit\Framework\TestCase;

class VersionRestoreTemplateCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->stub = new VersionRestoreTemplateCommand(
            $faker->randomNumber(),
            $faker->randomNumber()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
