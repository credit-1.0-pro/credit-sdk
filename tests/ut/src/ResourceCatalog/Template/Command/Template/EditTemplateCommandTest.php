<?php
namespace Sdk\ResourceCatalog\Template\Command\Template;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Command\TemplateCommandDataTrait;

class EditTemplateCommandTest extends TestCase
{
    use TemplateCommandDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = $this->getRequestCommonData();

        $this->command = new EditTemplateCommand(
            $this->fakerData['name'],
            $this->fakerData['identify'],
            $this->fakerData['description'],
            $this->fakerData['versionDescription'],
            $this->fakerData['subjectCategory'],
            $this->fakerData['items'],
            $this->fakerData['sourceUnitIds'],
            $this->fakerData['dimension'],
            $this->fakerData['exchangeFrequency'],
            $this->fakerData['infoClassify'],
            $this->fakerData['infoCategory'],
            $faker->randomNumber()
        );
    }

    public function testCorrectExtendsOperationCommand()
    {
        $this->assertInstanceof('Sdk\ResourceCatalog\Template\Command\OperationCommand', $this->command);
    }
    
    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
