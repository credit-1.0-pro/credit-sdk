<?php
namespace Sdk\ResourceCatalog\Template\Command;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Command\TemplateCommandDataTrait;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class OperationCommandTest extends TestCase
{
    use TemplateCommandDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = $this->getRequestCommonData();

        $this->command = new OperationCommand(
            $this->fakerData['name'],
            $this->fakerData['identify'],
            $this->fakerData['description'],
            $this->fakerData['versionDescription'],
            $this->fakerData['subjectCategory'],
            $this->fakerData['items'],
            $this->fakerData['sourceUnitIds'],
            $this->fakerData['dimension'],
            $this->fakerData['exchangeFrequency'],
            $this->fakerData['infoClassify'],
            $this->fakerData['infoCategory'],
            1
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }

    public function testIdParameter()
    {
        $this->assertEquals(1, $this->command->id);
    }

    public function testNameParameter()
    {
        $this->assertEquals($this->fakerData['name'], $this->command->name);
    }

    public function testIdentifyParameter()
    {
        $this->assertEquals($this->fakerData['identify'], $this->command->identify);
    }

    public function testDescriptionParameter()
    {
        $this->assertEquals($this->fakerData['description'], $this->command->description);
    }

    public function testVersionDescriptionParameter()
    {
        $this->assertEquals($this->fakerData['versionDescription'], $this->command->versionDescription);
    }

    public function testSubjectCategoryParameter()
    {
        $this->assertEquals($this->fakerData['subjectCategory'], $this->command->subjectCategory);
    }

    public function testItemsParameter()
    {
        $this->assertEquals($this->fakerData['items'], $this->command->items);
    }

    public function testSourceUnitIdsParameter()
    {
        $this->assertEquals($this->fakerData['sourceUnitIds'], $this->command->sourceUnitIds);
    }

    public function testDimensionParameter()
    {
        $this->assertEquals($this->fakerData['dimension'], $this->command->dimension);
    }

    public function testExchangeFrequencyParameter()
    {
        $this->assertEquals($this->fakerData['exchangeFrequency'], $this->command->exchangeFrequency);
    }

    public function testInfoClassifyParameter()
    {
        $this->assertEquals($this->fakerData['infoClassify'], $this->command->infoClassify);
    }

    public function testInfoCategoryParameter()
    {
        $this->assertEquals($this->fakerData['infoCategory'], $this->command->infoCategory);
    }
}
