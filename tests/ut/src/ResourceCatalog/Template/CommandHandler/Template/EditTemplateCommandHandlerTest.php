<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\Template;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Command\TemplateCommandDataTrait;
use Sdk\ResourceCatalog\Template\Command\Template\EditTemplateCommand;

class EditTemplateCommandHandlerTest extends TestCase
{
    use TemplateCommandDataTrait;

    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditTemplateCommandHandler::class)
            ->setMethods(['fetchTemplate', 'templateExecuteAction'])
            ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->fakerData = $this->getRequestCommonData();
        $id = 1;
        
        $command = new EditTemplateCommand(
            $this->fakerData['name'],
            $this->fakerData['identify'],
            $this->fakerData['description'],
            $this->fakerData['versionDescription'],
            $this->fakerData['subjectCategory'],
            $this->fakerData['items'],
            $this->fakerData['sourceUnitIds'],
            $this->fakerData['dimension'],
            $this->fakerData['exchangeFrequency'],
            $this->fakerData['infoClassify'],
            $this->fakerData['infoCategory'],
            $id
        );

        $template = $this->prophesize(Template::class);
        $template->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
            ->method('templateExecuteAction')
            ->willReturn($template->reveal());

        $this->commandHandler->expects($this->once())
            ->method('fetchTemplate')
            ->willReturn($template->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
