<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\Template;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Sdk\ResourceCatalog\Template\Command\Template\AddTemplateCommand;
use Sdk\ResourceCatalog\Template\Command\Template\EditTemplateCommand;
use Sdk\ResourceCatalog\Template\Command\Template\VersionRestoreTemplateCommand;

class TemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new TemplateCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddTemplateCommand(
                $this->faker->title(),
                $this->faker->title(),
                $this->faker->text(),
                $this->faker->text(),
                array($this->faker->title()),
                array($this->faker->title()),
                array($this->faker->title()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(2),
                $this->faker->randomNumber(3),
                $this->faker->randomNumber(4),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\CommandHandler\Template\AddTemplateCommandHandler',
            $commandHandler
        );
    }

    public function testEditTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditTemplateCommand(
                $this->faker->name(),
                $this->faker->title(),
                $this->faker->text(),
                $this->faker->text(),
                array($this->faker->name()),
                array($this->faker->name()),
                array($this->faker->name()),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(2),
                $this->faker->randomNumber(3),
                $this->faker->randomNumber(4),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\CommandHandler\Template\EditTemplateCommandHandler',
            $commandHandler
        );
    }

    public function testVersionRestoreTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new VersionRestoreTemplateCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\CommandHandler\Template\VersionRestoreTemplateCommandHandler',
            $commandHandler
        );
    }
}
