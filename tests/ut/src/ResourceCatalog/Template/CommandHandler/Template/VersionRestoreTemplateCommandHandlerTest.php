<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\Template;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Command\Template\VersionRestoreTemplateCommand;

class VersionRestoreTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(VersionRestoreTemplateCommandHandler::class)
            ->setMethods(['fetchTemplateVersion', 'fetchTemplate'])
            ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $id = $templateVersionId = 1;
        
        $command = new VersionRestoreTemplateCommand(
            $templateVersionId,
            $id
        );

        $templateVersion = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplateVersion(1);
        $this->commandHandler->expects($this->once())->method('fetchTemplateVersion')->willReturn($templateVersion);

        $template = $this->prophesize(Template::class);
        $template->setTemplateVersion(Argument::exact($templateVersion))->shouldBeCalledTimes(1);
        $template->versionRestore()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())->method('fetchTemplate')->willReturn($template->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
