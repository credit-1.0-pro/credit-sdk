<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;
use Sdk\ResourceCatalog\Template\Repository\TemplateRepository;
use Sdk\ResourceCatalog\Template\Command\Template\AddTemplateCommand;
use Sdk\ResourceCatalog\Template\Repository\UnAuditTemplateRepository;

use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Repository\TemplateVersionRepository;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class TemplateCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockTemplateCommandHandlerTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetTemplate()
    {
        $trait = new MockTemplateCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\Template',
            $trait->publicGetTemplate()
        );
    }

    public function testGetUnAuditTemplate()
    {
        $trait = new MockTemplateCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\UnAuditTemplate',
            $trait->publicGetUnAuditTemplate()
        );
    }

    public function testGetUnAuditTemplateRepository()
    {
        $trait = new MockTemplateCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Repository\UnAuditTemplateRepository',
            $trait->publicGetUnAuditTemplateRepository()
        );
    }

    public function testGetRepository()
    {
        $trait = new MockTemplateCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Repository\TemplateRepository',
            $trait->publicGetRepository()
        );
    }

    public function testFetchTemplate()
    {
        $trait = $this->getMockBuilder(MockTemplateCommandHandlerTrait::class)
                 ->setMethods(['getRepository'])->getMock();

        $id = 1;

        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate($id);

        $repository = $this->prophesize(TemplateRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($template);

        $trait->expects($this->exactly(1))
                         ->method('getRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchTemplate($id);

        $this->assertEquals($result, $template);
    }

    public function testFetchUnAuditTemplate()
    {
        $trait = $this->getMockBuilder(MockTemplateCommandHandlerTrait::class)
                 ->setMethods(['getUnAuditTemplateRepository'])->getMock();

        $id = 1;

        $unAuditTemplate = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateUnAuditTemplate($id);

        $repository = $this->prophesize(UnAuditTemplateRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditTemplate);

        $trait->expects($this->exactly(1))->method('getUnAuditTemplateRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchUnAuditTemplate($id);

        $this->assertEquals($result, $unAuditTemplate);
    }

    public function testGetTemplateVersionRepository()
    {
        $trait = new MockTemplateCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Repository\TemplateVersionRepository',
            $trait->publicGetTemplateVersionRepository()
        );
    }

    public function testFetchTemplateVersion()
    {
        $trait = $this->getMockBuilder(MockTemplateCommandHandlerTrait::class)
                 ->setMethods(['getTemplateVersionRepository'])->getMock();

        $id = 1;
        $templateVersion = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplateVersion($id);

        $repository = $this->prophesize(TemplateVersionRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($templateVersion);

        $trait->expects($this->exactly(1))
                         ->method('getTemplateVersionRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchTemplateVersion($id);

        $this->assertEquals($result, $templateVersion);
    }

    public function testGetUserGroupRepository()
    {
        $trait = new MockTemplateCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Repository\UserGroupRepository',
            $trait->publicGetUserGroupRepository()
        );
    }

    public function testFetchSourceUnits()
    {
        $trait = $this->getMockBuilder(MockTemplateCommandHandlerTrait::class)
                 ->setMethods(['getUserGroupRepository'])->getMock();

        $ids = array(1);
        $userGroup = \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $sourceUnits = array(1 => $userGroup);

        $repository = $this->prophesize(UserGroupRepository::class);
        $repository->fetchList(Argument::exact($ids))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($sourceUnits);

        $trait->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchSourceUnits($ids);

        $this->assertEquals($result, $sourceUnits);
    }

    public function testTemplateExecuteAction()
    {
        $trait = $this->getMockBuilder(MockTemplateCommandHandlerTrait::class)
                 ->setMethods(['fetchSourceUnits'])->getMock();
        $id = 1;
        $sourceUnitIds = array();

        $template = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateTemplate($id);
        $sourceUnits = $template->getSourceUnits();
        $count = count($sourceUnits);

        foreach ($sourceUnits as $sourceUnit) {
            $sourceUnitIds[] = $sourceUnit->getId();
        }

        $command = new AddTemplateCommand(
            $template->getName(),
            $template->getIdentify(),
            $template->getDescription(),
            $template->getVersionDescription(),
            $sourceUnitIds,
            $template->getItems(),
            $template->getSourceUnits(),
            $template->getDimension(),
            $template->getExchangeFrequency(),
            $template->getInfoClassify(),
            $template->getInfoCategory()
        );
        //$trait->expects($this->exactly(1))
        $trait->expects($this->exactly(1))
                         ->method('fetchSourceUnits')
                         ->with(array_unique($command->sourceUnitIds))
                         ->willReturn([$count, $sourceUnits]);
                         
        $result = $trait->publicTemplateExecuteAction($command, $template);

        $this->assertEquals($result, $template);
    }
}
