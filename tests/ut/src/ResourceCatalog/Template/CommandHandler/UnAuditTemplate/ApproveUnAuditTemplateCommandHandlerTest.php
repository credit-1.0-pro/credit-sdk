<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate;

use PHPUnit\Framework\TestCase;

class ApproveUnAuditTemplateCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveUnAuditTemplateCommandHandler::class)
            ->setMethods(['fetchUnAuditTemplate'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\ApproveCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $unAuditTemplate = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateUnAuditTemplate($id);

        $this->stub->expects($this->exactly(1))
                    ->method('fetchUnAuditTemplate')
                    ->willReturn($unAuditTemplate);

        $result = $this->stub->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditTemplate);
    }
}
