<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate;

use PHPUnit\Framework\TestCase;

class RejectUnAuditTemplateCommandHandlerTest extends TestCase
{
    private $rejectStub;

    public function setUp()
    {
        $this->rejectStub = $this->getMockBuilder(MockRejectUnAuditTemplateCommandHandler::class)
            ->setMethods(['fetchUnAuditTemplate'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\RejectCommandHandler',
            $this->rejectStub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $unAuditTemplate = \Sdk\ResourceCatalog\Template\Utils\MockFactory::generateUnAuditTemplate($id);

        $this->rejectStub->expects($this->exactly(1))
                    ->method('fetchUnAuditTemplate')
                    ->willReturn($unAuditTemplate);

        $result = $this->rejectStub->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditTemplate);
    }
}
