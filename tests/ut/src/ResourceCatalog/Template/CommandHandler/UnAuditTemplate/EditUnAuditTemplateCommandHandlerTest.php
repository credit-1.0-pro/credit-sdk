<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;
use Sdk\ResourceCatalog\Template\Command\TemplateCommandDataTrait;
use Sdk\ResourceCatalog\Template\Command\UnAuditTemplate\EditUnAuditTemplateCommand;

class EditUnAuditTemplateCommandHandlerTest extends TestCase
{
    use TemplateCommandDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditUnAuditTemplateCommandHandler::class)
            ->setMethods(['fetchUnAuditTemplate', 'templateExecuteAction'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->fakerData = $this->getRequestCommonData();
        $id = 1;
        
        $command = new EditUnAuditTemplateCommand(
            $this->fakerData['name'],
            $this->fakerData['identify'],
            $this->fakerData['description'],
            $this->fakerData['versionDescription'],
            $this->fakerData['subjectCategory'],
            $this->fakerData['items'],
            $this->fakerData['sourceUnitIds'],
            $this->fakerData['dimension'],
            $this->fakerData['exchangeFrequency'],
            $this->fakerData['infoClassify'],
            $this->fakerData['infoCategory'],
            $id
        );

        $unAuditedTemplate = $this->prophesize(UnAuditTemplate::class);
        $unAuditedTemplate->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
            ->method('templateExecuteAction')
            ->willReturn($unAuditedTemplate->reveal());

        $this->commandHandler->expects($this->once())
            ->method('fetchUnAuditTemplate')
            ->willReturn($unAuditedTemplate->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
