<?php
namespace Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Sdk\ResourceCatalog\Template\Command\UnAuditTemplate\EditUnAuditTemplateCommand;
use Sdk\ResourceCatalog\Template\Command\UnAuditTemplate\RejectUnAuditTemplateCommand;
use Sdk\ResourceCatalog\Template\Command\UnAuditTemplate\ApproveUnAuditTemplateCommand;

class UnAuditTemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new UnAuditTemplateCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testEditUnAuditTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditUnAuditTemplateCommand(
                $this->faker->name(),
                $this->faker->title(),
                $this->faker->text(),
                $this->faker->text(),
                array($this->faker->name()),
                array($this->faker->name()),
                array($this->faker->name()),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate\EditUnAuditTemplateCommandHandler',
            $commandHandler
        );
    }

    public function testRejectUnAuditTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RejectUnAuditTemplateCommand(
                $this->faker->title(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate\RejectUnAuditTemplateCommandHandler',
            $commandHandler
        );
    }

    public function testApproveUnAuditTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ApproveUnAuditTemplateCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\CommandHandler\UnAuditTemplate\ApproveUnAuditTemplateCommandHandler',
            $commandHandler
        );
    }
}
