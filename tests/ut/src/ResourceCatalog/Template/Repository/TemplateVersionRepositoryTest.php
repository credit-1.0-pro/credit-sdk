<?php
namespace Sdk\ResourceCatalog\Template\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Template\Adapter\TemplateVersion\TemplateVersionRestfulAdapter;

class TemplateVersionRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(TemplateVersionRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsITemplateVersionAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Adapter\TemplateVersion\ITemplateVersionAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Adapter\TemplateVersion\TemplateVersionRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Adapter\TemplateVersion\TemplateVersionMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(TemplateVersionRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
