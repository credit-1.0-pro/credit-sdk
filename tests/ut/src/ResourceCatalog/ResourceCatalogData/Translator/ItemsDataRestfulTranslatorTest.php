<?php
namespace Sdk\ResourceCatalog\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\ResourceCatalogData\Utils\ItemsDataRestfulUtils;

class ItemsDataRestfulTranslatorTest extends TestCase
{
    use ItemsDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ItemsDataRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $itemsData = \Sdk\ResourceCatalog\ResourceCatalogData\Utils\MockFactory::generateItemsData(1);

        $expression['data']['id'] = $itemsData->getId();
        $expression['data']['attributes']['data'] = $itemsData->getData();
        
        $itemsDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\ResourceCatalogData\Model\ItemsData', $itemsDataObject);
        $this->compareArrayAndObjectCommon($expression, $itemsDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $itemsData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\ResourceCatalog\ResourceCatalogData\Model\NullItemsData', $itemsData);
    }

    public function testObjectToArray()
    {
        $itemsData = null;

        $expression = $this->translator->objectToArray($itemsData);
        $this->assertEquals(array(), $expression);
    }
}
