<?php
namespace Sdk\ResourceCatalog\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\ResourceCatalogData\Utils\ItemsDataUtils;

class ItemsDataTranslatorTest extends TestCase
{
    use ItemsDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ItemsDataTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\ResourceCatalog\ResourceCatalogData\Model\NullItemsData', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $itemsData = \Sdk\ResourceCatalog\ResourceCatalogData\Utils\MockFactory::generateItemsData(1);

        $expression = $this->translator->objectToArray($itemsData);
    
        $this->compareArrayAndObjectItemsData($expression, $itemsData);
    }

    public function testObjectToArrayFail()
    {
        $itemsData = null;

        $expression = $this->translator->objectToArray($itemsData);
        $this->assertEquals(array(), $expression);
    }
}
