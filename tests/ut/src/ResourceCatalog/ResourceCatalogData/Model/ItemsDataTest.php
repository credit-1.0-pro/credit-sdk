<?php
namespace Sdk\ResourceCatalog\ResourceCatalogData\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ItemsDataTest extends TestCase
{
    private $itemsData;

    public function setUp()
    {
        $this->itemsData = new ItemsData();
    }

    public function tearDown()
    {
        unset($this->itemsData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testItemsDataConstructor()
    {
        $this->assertEquals(0, $this->itemsData->getId());
        $this->assertEquals([], $this->itemsData->getData());
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 ItemsData setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->itemsData->setId(1);
        $this->assertEquals(1, $this->itemsData->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //data 测试 -------------------------------------------------------- start
    /**
     * 设置 ItemsData setData() 正确的传参类型,期望传值正确
     */
    public function testSetDataCorrectType()
    {
        $this->itemsData->setData(array('data'));
        $this->assertEquals(array('data'), $this->itemsData->getData());
    }

    /**
     * 设置 ItemsData setData() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDataWrongType()
    {
        $this->itemsData->setData(1);
    }
    //data 测试 --------------------------------------------------------   end
}
