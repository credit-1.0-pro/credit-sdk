<?php
namespace Sdk\ResourceCatalog\ResourceCatalogData\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Model\RuleVersion;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;

use Sdk\ResourceCatalog\Task\Model\Task;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ResourceCatalogDataTest extends TestCase
{
    private $resourceCatalogData;

    private $faker;

    public function setUp()
    {
        $this->resourceCatalogData = $this->getMockBuilder(MockResourceCatalogData::class)->getMockForAbstractClass();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->resourceCatalogData);
        unset($this->faker);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->resourceCatalogData
        );
    }

    public function testResourceCatalogDataConstructor()
    {
        $this->assertEquals(0, $this->resourceCatalogData->getId());
        $this->assertEmpty($this->resourceCatalogData->getName());
        $this->assertEmpty($this->resourceCatalogData->getIdentify());
        $this->assertEquals(
            ResourceCatalogData::EXPIRATION_DATE_DEFAULT,
            $this->resourceCatalogData->getExpirationDate()
        );
        $this->assertEquals(0, $this->resourceCatalogData->getSubjectCategory());
        $this->assertEquals(0, $this->resourceCatalogData->getDimension());
        $this->assertEquals(0, $this->resourceCatalogData->getExchangeFrequency());
        $this->assertEquals(0, $this->resourceCatalogData->getInfoClassify());
        $this->assertEquals(0, $this->resourceCatalogData->getInfoCategory());
        $this->assertEquals(0, $this->resourceCatalogData->getStatusTime());
        $this->assertEquals(0, $this->resourceCatalogData->getCreateTime());
        $this->assertEquals(0, $this->resourceCatalogData->getUpdateTime());

        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\Rule',
            $this->resourceCatalogData->getRule()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Rule\Model\RuleVersion',
            $this->resourceCatalogData->getRuleVersion()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\Template',
            $this->resourceCatalogData->getTemplate()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Template\Model\TemplateVersion',
            $this->resourceCatalogData->getTemplateVersion()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Task\Model\Task',
            $this->resourceCatalogData->getTask()
        );
        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Crew',
            $this->resourceCatalogData->getCrew()
        );
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Model\UserGroup',
            $this->resourceCatalogData->getPublishUserGroup()
        );
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ResourceCatalogData\Model\ItemsData',
            $this->resourceCatalogData->getItemsData()
        );
    }

    public function testSetId()
    {
        $id = $this->faker->randomNumber();
        $this->resourceCatalogData->setId($id);
        $this->assertEquals($id, $this->resourceCatalogData->getId());
    }

    //name 测试 ------------------------------------------------------- start
    /**
     * 设置 setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->resourceCatalogData->setName('string');
        $this->assertEquals('string', $this->resourceCatalogData->getName());
    }

    /**
     * 设置 setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->resourceCatalogData->setName(array(1, 2, 3));
    }
    //name 测试 -------------------------------------------------------   end

    //identify 测试 ------------------------------------------------------ start
    /**
     * 设置 setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->resourceCatalogData->setIdentify('string');
        $this->assertEquals('string', $this->resourceCatalogData->getIdentify());
    }

    /**
     * 设置 setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->resourceCatalogData->setIdentify(array(1, 2, 3));
    }
    //identify 测试 ------------------------------------------------------   end

    //expirationDate 测试 ------------------------------------------------------- start
    /**
     * 设置 setExpirationDate() 正确的传参类型,期望传值正确
     */
    public function testSetExpirationDateCorrectType()
    {
        $expirationDate = $this->faker->unixTime();

        $this->resourceCatalogData->setExpirationDate($expirationDate);
        $this->assertEquals($expirationDate, $this->resourceCatalogData->getExpirationDate());
    }

    /**
     * 设置 setExpirationDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetExpirationDateWrongType()
    {
        $this->resourceCatalogData->setExpirationDate($this->faker->words(3, false));
    }
    //expirationDate 测试 -------------------------------------------------------   end
            
    //subjectCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 setSubjectCategory() 正确的传参类型,期望传值正确
     */
    public function testSetSubjectCategoryCorrectType()
    {
        $this->resourceCatalogData->setSubjectCategory(1);
        $this->assertEquals(1, $this->resourceCatalogData->getSubjectCategory());
    }

    /**
     * 设置 setSubjectCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubjectCategoryWrongType()
    {
        $this->resourceCatalogData->setSubjectCategory(array(1,2,3));
    }
    //subjectCategory 测试 --------------------------------------------------------   end
            
    //dimension 测试 -------------------------------------------------------- start
    /**
     * 设置 setDimension() 正确的传参类型,期望传值正确
     */
    public function testSetDimensionCorrectType()
    {
        $this->resourceCatalogData->setDimension(1);
        $this->assertEquals(1, $this->resourceCatalogData->getDimension());
    }

    /**
     * 设置 setDimension() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDimensionWrongType()
    {
        $this->resourceCatalogData->setDimension(array(1,2,3));
    }
    //dimension 测试 --------------------------------------------------------   end
  
    //exchangeFrequency 测试 -------------------------------------------------------- start
    /**
     * 设置 setExchangeFrequency() 正确的传参类型,期望传值正确
     */
    public function testSetExchangeFrequencyCorrectType()
    {
        $this->resourceCatalogData->setExchangeFrequency(1);
        $this->assertEquals(1, $this->resourceCatalogData->getExchangeFrequency());
    }

    /**
     * 设置 setExchangeFrequency() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetExchangeFrequencyWrongType()
    {
        $this->resourceCatalogData->setExchangeFrequency(array(1,2,3));
    }
    //exchangeFrequency 测试 --------------------------------------------------------   end

    //infoClassify 测试 -------------------------------------------------------- start
    /**
     * 设置 setInfoClassify() 正确的传参类型,期望传值正确
     */
    public function testSetInfoClassifyCorrectType()
    {
        $this->resourceCatalogData->setInfoClassify(1);
        $this->assertEquals(1, $this->resourceCatalogData->getInfoClassify());
    }

    /**
     * 设置 setInfoClassify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoClassifyWrongType()
    {
        $this->resourceCatalogData->setInfoClassify(array(1,2,3));
    }
    //infoClassify 测试 --------------------------------------------------------   end

    //infoCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 setInfoCategory() 正确的传参类型,期望传值正确
     */
    public function testSetInfoCategoryCorrectType()
    {
        $this->resourceCatalogData->setInfoCategory(1);
        $this->assertEquals(1, $this->resourceCatalogData->getInfoCategory());
    }

    /**
     * 设置 setInfoCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoCategoryWrongType()
    {
        $this->resourceCatalogData->setInfoCategory(array(1,2,3));
    }
    //infoCategory 测试 --------------------------------------------------------   end

    //rule 测试 ---------------------------------------------------- start
    /**
     * 设置 setRule() 正确的传参类型,期望传值正确
     */
    public function testSetRuleCorrectType()
    {
        $rule = new Rule();

        $this->resourceCatalogData->setRule($rule);
        $this->assertEquals($rule, $this->resourceCatalogData->getRule());
    }

    /**
     * 设置 setRule() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRuleWrongType()
    {
        $rule = array($this->faker->randomNumber());

        $this->resourceCatalogData->setRule($rule);
    }
    //rule 测试 ----------------------------------------------------   end
    
    //ruleVersion 测试 ---------------------------------------------------- start
    /**
     * 设置 setRuleVersion() 正确的传参类型,期望传值正确
     */
    public function testSetRuleVersionCorrectType()
    {
        $ruleVersion = new RuleVersion();

        $this->resourceCatalogData->setRuleVersion($ruleVersion);
        $this->assertEquals($ruleVersion, $this->resourceCatalogData->getRuleVersion());
    }

    /**
     * 设置 setRuleVersion() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRuleVersionWrongType()
    {
        $ruleVersion = array($this->faker->randomNumber());

        $this->resourceCatalogData->setRuleVersion($ruleVersion);
    }
    //ruleVersion 测试 ----------------------------------------------------   end
    
    //crew 测试 ---------------------------------------------------- start
    /**
     * 设置 setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $crew = new Crew();

        $this->resourceCatalogData->setCrew($crew);
        $this->assertEquals($crew, $this->resourceCatalogData->getCrew());
    }

    /**
     * 设置 setCrew() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $crew = array($this->faker->randomNumber());

        $this->resourceCatalogData->setCrew($crew);
    }
    //crew 测试 ----------------------------------------------------   end
    
    //publishUserGroup 测试 ---------------------------------------------------- start
    /**
     * 设置 setPublishUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetPublishUserGroupCorrectType()
    {
        $publishUserGroup = new UserGroup();

        $this->resourceCatalogData->setPublishUserGroup($publishUserGroup);
        $this->assertEquals($publishUserGroup, $this->resourceCatalogData->getPublishUserGroup());
    }

    /**
     * 设置 setPublishUserGroup() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPublishUserGroupWrongType()
    {
        $publishUserGroup = array($this->faker->randomNumber());

        $this->resourceCatalogData->setPublishUserGroup($publishUserGroup);
    }
    //publishUserGroup 测试 ----------------------------------------------------   end
    
    //template 测试 ---------------------------------------------------- start
    /**
     * 设置 setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $template = new Template();

        $this->resourceCatalogData->setTemplate($template);
        $this->assertEquals($template, $this->resourceCatalogData->getTemplate());
    }

    /**
     * 设置 setTemplate() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $template = array($this->faker->randomNumber());

        $this->resourceCatalogData->setTemplate($template);
    }
    //template 测试 ----------------------------------------------------   end
    
    //templateVersion 测试 ---------------------------------------------------- start
    /**
     * 设置 setTemplateVersion() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateVersionCorrectType()
    {
        $templateVersion = new TemplateVersion();

        $this->resourceCatalogData->setTemplateVersion($templateVersion);
        $this->assertEquals($templateVersion, $this->resourceCatalogData->getTemplateVersion());
    }
    /**
     * 设置 setTemplateVersion() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateVersionWrongType()
    {
        $templateVersion = array($this->faker->randomNumber());

        $this->resourceCatalogData->setTemplateVersion($templateVersion);
    }
    //templateVersion 测试 ----------------------------------------------------   end

    //task 测试 ---------------------------------------------------- start
    /**
     * 设置 setTask() 正确的传参类型,期望传值正确
     */
    public function testSetTaskCorrectType()
    {
        $task = new Task();

        $this->resourceCatalogData->setTask($task);
        $this->assertEquals($task, $this->resourceCatalogData->getTask());
    }

    /**
     * 设置 setTask() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTaskWrongType()
    {
        $task = array($this->faker->randomNumber());

        $this->resourceCatalogData->setTask($task);
    }
    //task 测试 ----------------------------------------------------   end
    
    //itemsData 测试 ---------------------------------------------------- start
    /**
     * 设置 setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $itemsData = new ItemsData();

        $this->resourceCatalogData->setItemsData($itemsData);
        $this->assertEquals($itemsData, $this->resourceCatalogData->getItemsData());
    }

    /**
     * 设置 setItemsData() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $itemsData = array($this->faker->randomNumber());

        $this->resourceCatalogData->setItemsData($itemsData);
    }
    //itemsData 测试 ----------------------------------------------------   end
        
    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->resourceCatalogData->setStatus(0);
        $this->assertEquals(0, $this->resourceCatalogData->getStatus());
    }

    /**
     * 设置 setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->resourceCatalogData->setStatus(array(1,2,3));
    }
    //status 测试 --------------------------------------------------------   end
}
