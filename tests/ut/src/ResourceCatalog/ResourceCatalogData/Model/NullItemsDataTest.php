<?php
namespace Sdk\ResourceCatalog\ResourceCatalogData\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullItemsDataTest extends TestCase
{
    private $nullItemsData;

    public function setUp()
    {
        $this->nullItemsData = NullItemsData::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullItemsData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsItemsData()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\ResourceCatalogData\Model\ItemsData',
            $this->nullItemsData
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullItemsData
        );
    }
}
