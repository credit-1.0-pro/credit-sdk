<?php
namespace Sdk\ResourceCatalog\ResourceCatalogData\Utils;

trait ItemsDataUtils
{
    private function compareArrayAndObjectItemsData(
        array $expectedArray,
        $itemsData
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($itemsData->getId()));
        $this->assertEquals($expectedArray['data'], $itemsData->getData());
    }
}
