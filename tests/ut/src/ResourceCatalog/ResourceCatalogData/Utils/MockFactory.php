<?php
namespace Sdk\ResourceCatalog\ResourceCatalogData\Utils;

use Sdk\ResourceCatalog\ResourceCatalogData\Model\ItemsData;

class MockFactory
{
    public static function generateItemsData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ItemsData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $itemsData = new ItemsData($id);
        $itemsData->setId($id);

        $data = isset($value['data']) ? $value['data'] : array($faker->word());
        $itemsData->setData($data);

        return $itemsData;
    }
}
