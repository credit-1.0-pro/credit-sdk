<?php
namespace Sdk\ResourceCatalog\ResourceCatalogData\Utils;

trait ItemsDataRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $itemsData
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $itemsData->getId());
        }
        
        $attributes = $expectedArray['data']['attributes'];
        $this->assertEquals($attributes['data'], $itemsData->getData());
    }
}
