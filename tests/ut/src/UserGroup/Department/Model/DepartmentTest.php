<?php
namespace Sdk\UserGroup\Department\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\Department\Repository\DepartmentRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class DepartmentTest extends TestCase
{
    private $department;

    public function setUp()
    {
        $this->department = new MockDepartment();
    }

    public function tearDown()
    {
        unset($this->department);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->department
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\IOperateAble',
            $this->department
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $department = new MockDepartment();
        $this->assertInstanceOf('Sdk\Common\Adapter\IOperateAbleAdapter', $department->getIOperateAbleAdapter());
        $this->assertInstanceOf(
            'Sdk\UserGroup\Department\Repository\DepartmentRepository',
            $department->getIOperateAbleAdapter()
        );
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->department->getId());

        $this->assertEmpty($this->department->getName());

        $this->assertInstanceof('Sdk\UserGroup\UserGroup\Model\UserGroup', $this->department->getUserGroup());

        $this->assertEquals(0, $this->department->getUpdateTime());

        $this->assertInstanceof(
            'Sdk\UserGroup\Department\Repository\DepartmentRepository',
            $this->department->getIOperateAbleAdapter()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Department setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->department->setId(1);
        $this->assertEquals(1, $this->department->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 Department setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->department->setName('string');
        $this->assertEquals('string', $this->department->getName());
    }

    /**
     * 设置 Department setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->department->setName(array(1,2,3));
    }
    //name 测试 --------------------------------------------------------   end

    //userGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 Department setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->department->setUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->department->getUserGroup());
    }

    /**
     * 设置 Department setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->department->setUserGroup('userGroup');
    }
    //userGroup 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 Department setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->department->setStatus(0);
        $this->assertEquals(0, $this->department->getStatus());
    }

    /**
     * 设置 Department setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->department->setStatus(array(1,2,3));
    }
    //status 测试 --------------------------------------------------------   end

    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getIOperateAbleAdapter 被调用一次
     * 3. 期望 DepartmentRepository 调用 add 并且传参 $department 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['getIOperateAbleAdapter', 'nameIsExist'])
                           ->getMock();

        $department->expects($this->once())
                   ->method('nameIsExist')
                   ->willReturn(true);

        //预言 DepartmentRepository
        $repository = $this->prophesize(DepartmentRepository::class);
        $repository->add(Argument::exact($department))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $department->expects($this->once())
             ->method('getIOperateAbleAdapter')
             ->willReturn($repository->reveal());

        //验证
        $result = $department->add();
        $this->assertTrue($result);
    }

    /**
     * 测试添加失败-名称重复
     * 1. 期望返回false
     * 2. 期望nameIsExist被调用一次,并返回false
     * 3. 调用add,期望返回false
     */
    public function testAddFailNameExist()
    {
        $department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['nameIsExist'])
                           ->getMock();

        $department->expects($this->once())
                   ->method('nameIsExist')
                   ->willReturn(false);

        $result = $department->add();

        $this->assertFalse($result);
    }

    //add()
    /**
     * 测试添加失败
     * 1. 期望返回false
     * 2. 期望 getIOperateAbleAdapter 被调用一次
     * 3. 期望 DepartmentRepository 调用 add 并且传参 $department 对象, 返回 false
     */
    public function testAddFail()
    {
        //初始化
        $department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['nameIsExist', 'getIOperateAbleAdapter'])
                           ->getMock();

        $department->expects($this->once())
                   ->method('nameIsExist')
                   ->willReturn(true);

        //预言 DepartmentRepository
        $repository = $this->prophesize(DepartmentRepository::class);
        $repository->add(Argument::exact($department))->shouldBeCalledTimes(1)->willReturn(false);

        //绑定
        $department->expects($this->once())->method('getIOperateAbleAdapter')->willReturn($repository->reveal());

        //验证
        $result = $department->add();
        $this->assertFalse($result);
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望更新 name, updateTime
     * 2. 期望 getIOperateAbleAdapter 被调用一次
     * 3. 期望 DepartmentRepository 调用 edit 并且传参 $department 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['nameIsExist', 'getIOperateAbleAdapter'])
                           ->getMock();

        $department->expects($this->once())
                   ->method('nameIsExist')
                   ->willReturn(true);
                   
        //预言 DepartmentRepository
        $repository = $this->prophesize(DepartmentRepository::class);
        $repository->edit(
            Argument::exact($department)
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $department->expects($this->once())
             ->method('getIOperateAbleAdapter')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $department->edit();
        $this->assertTrue($result);
    }

    /**
     * 测试编辑失败-名称重复
     * 1. 期望返回false
     * 2. 期望nameIsExist被调用一次,并返回false
     * 3. 调用edit,期望返回false
     */
    public function testEditFailNameExist()
    {
        //初始化
        $department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['nameIsExist'])
                           ->getMock();

        $department->expects($this->once())
                   ->method('nameIsExist')
                   ->willReturn(false);

        //验证
        $result = $department->edit();

        $this->assertFalse($result);
    }
    //edit
    /**
     * 期望编辑失败
     * 1. 期望更新 name, updateTime
     * 2. 期望 getIOperateAbleAdapter 被调用一次
     * 3. 期望 DepartmentRepository 调用 edit 并且传参 $department 对象, 和相应字段, 返回false
     */
    public function testEditFail()
    {
        //初始化
        $department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['nameIsExist', 'getIOperateAbleAdapter'])
                           ->getMock();

        $department->expects($this->once())
                   ->method('nameIsExist')
                   ->willReturn(true);

        //预言 DepartmentRepository
        $repository = $this->prophesize(DepartmentRepository::class);
        $repository->edit(
            Argument::exact($department)
        )->shouldBeCalledTimes(1)
         ->willReturn(false);

        //绑定
        $department->expects($this->once())
             ->method('getIOperateAbleAdapter')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $department->edit();
        $this->assertFalse($result);
    }

    private function initialNameIsExist($result)
    {
        $this->department = $this->getMockBuilder(MockDepartment::class)
                           ->setMethods(['getIOperateAbleAdapter'])
                           ->getMock();

        $id = 1;
        
        $sort = ['-updateTime'];
        $department = \Sdk\UserGroup\Department\Utils\MockDepartmentFactory::generateDepartment($id);

        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($department);
        }

        $this->department->setId($department->getId());
        $this->department->setName($department->getName());
        $this->department->setUserGroup($department->getUserGroup());

        $filter['id'] = $department->getId();
        $filter['unique'] = $department->getName();
        $filter['userGroup'] = $department->getUserGroup()->getId();

        $repository = $this->prophesize(DepartmentRepository::class);

        $repository->scenario(Argument::exact(DepartmentRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort)
        )->shouldBeCalledTimes(1)->willReturn([$count, $list]);

        $this->department->expects($this->any())
                   ->method('getIOperateAbleAdapter')
                   ->willReturn($repository->reveal());
    }

    public function testNameIsExistFailure()
    {
        $this->initialNameIsExist(false);

        $result = $this->department->nameIsExist();

        $this->assertFalse($result);
        $this->assertEquals(DEPARTMENT_NAME_IS_UNIQUE, Core::getLastError()->getId());
    }

    public function testNameIsExistSuccess()
    {
        $this->initialNameIsExist(true);
        
        $result = $this->department->nameIsExist();

        $this->assertTrue($result);
    }
}
