<?php
namespace Sdk\UserGroup\Department\Adapter;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\UserGroup\Department\Model\Department;
use Sdk\UserGroup\Department\Model\NullDepartment;
use Sdk\UserGroup\Department\Translator\DepartmentRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class DepartmentRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockDepartmentRestfulAdapter::class)
                           ->setMethods([
                               'commonMapErrors',
                               'getTranslator',
                               'post',
                               'isSuccess',
                               'patch',
                               'translateToObject'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new DepartmentRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIDepartmentAdapter()
    {
        $adapter = new DepartmentRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\UserGroup\Department\Adapter\IDepartmentAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockDepartmentRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\UserGroup\Department\Translator\DepartmentRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockDepartmentRestfulAdapter();
        $this->assertEquals('userGroups/departments', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockDepartmentRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\UserGroup\Department\Model\NullDepartment',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockDepartmentRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'DEPARTMENT_LIST',
                DepartmentRestfulAdapter::SCENARIOS['DEPARTMENT_LIST']
            ],
            [
                'DEPARTMENT_FETCH_ONE',
                DepartmentRestfulAdapter::SCENARIOS['DEPARTMENT_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    /**
     * 为DepartmentRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$department,$keys,$departmentArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareDepartmentTranslator(
        Department $department,
        array $keys,
        array $departmentArray
    ) {
        $translator = $this->prophesize(DepartmentRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($department),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($departmentArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Department $department)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($department);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $department = \Sdk\UserGroup\Department\Utils\MockDepartmentFactory::generateDepartment(1);
        $departmentArray = array();

        $this->prepareDepartmentTranslator(
            $department,
            array('name', 'userGroup'),
            $departmentArray
        );

        $this->adapter
            ->method('post')
            ->with('userGroups/departments', $departmentArray);

        $this->success($department);
        $result = $this->adapter->add($department);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $department = \Sdk\UserGroup\Department\Utils\MockDepartmentFactory::generateDepartment(1);
        $departmentArray = array();

        $this->prepareDepartmentTranslator(
            $department,
            array('name', 'userGroup'),
            $departmentArray
        );

        $this->adapter
            ->method('post')
            ->with('userGroups/departments', $departmentArray);
        
            $this->failure($department);
            $result = $this->adapter->add($department);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $department = \Sdk\UserGroup\Department\Utils\MockDepartmentFactory::generateDepartment(1);
        $departmentArray = array();

        $this->prepareDepartmentTranslator(
            $department,
            array('name'),
            $departmentArray
        );

        $this->adapter
            ->method('patch')
            ->with('userGroups/departments/'.$department->getId(), $departmentArray);

        $this->success($department);
        $result = $this->adapter->edit($department);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $department = \Sdk\UserGroup\Department\Utils\MockDepartmentFactory::generateDepartment(1);
        $departmentArray = array();

        $this->prepareDepartmentTranslator(
            $department,
            array('name'),
            $departmentArray
        );

        $this->adapter
            ->method('patch')
            ->with('userGroups/departments/'.$department->getId(), $departmentArray);
        
            $this->failure($department);
            $result = $this->adapter->edit($department);
            $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [
            100 => [
                'userGroupId'=>DEPARTMENT_USER_GROUP_IS_EMPTY
            ],
            101 => [
                'name'=>DEPARTMENT_NAME_FORMAT_ERROR,
                'userGroupId' => DEPARTMENT_USER_GROUP_FORMAT_ERROR
            ],
            103 => [
                'departmentName'=>DEPARTMENT_NAME_IS_UNIQUE
            ]
        ];
        
        $result = $this->adapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
