<?php
namespace Sdk\UserGroup\Department\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\UserGroup\Department\Adapter\DepartmentRestfulAdapter;

class DepartmentRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(DepartmentRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsIDepartmentAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\Department\Adapter\IDepartmentAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\Department\Adapter\DepartmentRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\Department\Adapter\DepartmentMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(DepartmentRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
