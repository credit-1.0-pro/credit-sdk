<?php
namespace Sdk\UserGroup\Department\CommandHandler;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\UserGroup\Department\Command\AddDepartmentCommand;
use Sdk\UserGroup\Department\Command\EditDepartmentCommand;

class DepartmentCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new DepartmentCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddDepartmentCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddDepartmentCommand(
                $this->faker->name(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\UserGroup\Department\CommandHandler\AddDepartmentCommandHandler',
            $commandHandler
        );
    }

    public function testEditDepartmentCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditDepartmentCommand(
                $this->faker->randomNumber(),
                $this->faker->name()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\UserGroup\Department\CommandHandler\EditDepartmentCommandHandler',
            $commandHandler
        );
    }
}
