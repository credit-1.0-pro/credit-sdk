<?php
namespace Sdk\UserGroup\Department\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\UserGroup\Department\Utils\DepartmentUtils;

class DepartmentTranslatorTest extends TestCase
{
    use DepartmentUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new DepartmentTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\UserGroup\Department\Model\NullDepartment', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $department = \Sdk\UserGroup\Department\Utils\MockDepartmentFactory::generateDepartment(1);

        $expression = $this->translator->objectToArray($department);

        $this->compareArrayAndObject($expression, $department);
    }

    public function testObjectToArrayFail()
    {
        $department = null;

        $expression = $this->translator->objectToArray($department);
        $this->assertEquals(array(), $expression);
    }
}
