<?php
namespace Sdk\UserGroup\Department\Command;

use PHPUnit\Framework\TestCase;

class EditDepartmentCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'id' => $faker->randomNumber(),
            'name' => $faker->name()
        );

        $this->command = new EditDepartmentCommand(
            $this->fakerData['id'],
            $this->fakerData['name']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testDepartmentNameParameter()
    {
        $this->assertEquals($this->fakerData['name'], $this->command->name);
    }
}
