<?php
namespace Sdk\UserGroup\Department\Utils;

trait DepartmentUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $department
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($department->getId()));
        $this->assertEquals($expectedArray['name'], $department->getName());
        $this->assertEquals($expectedArray['userGroup']['id'], marmot_encode($department->getUserGroup()->getId()));
        $this->assertEquals($expectedArray['userGroup']['name'], $department->getUserGroup()->getName());

        $this->assertEquals($expectedArray['createTime'], $department->getCreateTime());
        $this->assertEquals($expectedArray['updateTime'], $department->getUpdateTime());
        $this->assertEquals($expectedArray['updateTimeFormat'], date('Y年m月d日 H点i分', $department->getUpdateTime()));
    }
}
