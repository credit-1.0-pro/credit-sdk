<?php
namespace Sdk\UserGroup\UserGroup\Utils;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

trait UserGroupUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $userGroup
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($userGroup->getId()));
        $this->assertEquals($expectedArray['name'], $userGroup->getName());
        $this->assertEquals($expectedArray['shortName'], $userGroup->getShortName());
        $this->assertEquals($expectedArray['unifiedSocialCreditCode'], $userGroup->getUnifiedSocialCreditCode());
        $this->assertEquals(
            $expectedArray['administrativeArea']['id'],
            marmot_encode($userGroup->getAdministrativeArea())
        );
        $this->assertEquals(
            $expectedArray['administrativeArea']['name'],
            UserGroup::ADMINISTRATIVE_AREA_CN[$userGroup->getAdministrativeArea()]
        );
        $this->assertEquals($expectedArray['updateTime'], $userGroup->getUpdateTime());
        $this->assertEquals($expectedArray['updateTimeFormat'], date('Y年m月d日 H点i分', $userGroup->getUpdateTime()));
    }
}
