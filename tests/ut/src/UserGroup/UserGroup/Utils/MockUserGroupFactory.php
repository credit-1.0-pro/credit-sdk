<?php
namespace Sdk\UserGroup\UserGroup\Utils;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

class MockUserGroupFactory
{
    public static function generateUserGroup(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UserGroup {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $userGroup = new UserGroup($id);
        $userGroup->setId($id);

        //name
        self::generateName($userGroup, $faker, $value);

        //shortName
        self::generateShortName($userGroup, $faker, $value);
        //unifiedSocialCreditCode
        self::generateUnifiedSocialCreditCode($userGroup, $faker, $value);

        //administrativeArea
        self::generateAdministrativeArea($userGroup, $faker, $value);
        
        $userGroup->setStatus(0);
        $userGroup->setCreateTime($faker->unixTime());
        $userGroup->setUpdateTime($faker->unixTime());
        $userGroup->setStatusTime($faker->unixTime());

        return $userGroup;
    }

    private static function generateName($userGroup, $faker, $value) : void
    {
        $name = isset($value['name']) ?
            $value['name'] :
            $faker->title;
        
        $userGroup->setName($name);
    }

    private static function generateShortName($userGroup, $faker, $value) : void
    {
        $shortName = isset($value['shortName']) ?
            $value['shortName'] :
            $faker->title;
        
        $userGroup->setShortName($shortName);
    }

    private static function generateUnifiedSocialCreditCode($userGroup, $faker, $value) : void
    {
        $unifiedSocialCreditCode = isset($value['unifiedSocialCreditCode']) ?
                                    $value['unifiedSocialCreditCode'] :
                                    $faker->bothify('##############????');
        
        $userGroup->setUnifiedSocialCreditCode($unifiedSocialCreditCode);
    }

    private static function generateAdministrativeArea($userGroup, $faker, $value) : void
    {
        $administrativeArea = isset($value['administrativeArea']) ?
                                $value['administrativeArea'] :
                                $faker->randomDigit();
        
        $userGroup->setAdministrativeArea($administrativeArea);
    }
}
