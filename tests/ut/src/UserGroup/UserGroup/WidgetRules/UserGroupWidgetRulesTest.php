<?php
namespace Sdk\UserGroup\UserGroup\WidgetRules;

use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Sdk\Common\Utils\StringGenerate;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

class UserGroupWidgetRulesTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = UserGroupWidgetRules::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //userGroupName -- start
    /**
     * @dataProvider invalidUserGroupNameProvider
     */
    public function testUserGroupNameInvalid($actual, $expected)
    {
        $result = $this->widgetRule->userGroupName($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(USER_GROUP_NAME_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidUserGroupNameProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(UserGroupWidgetRules::NAME_MIN_LENGTH-1), false),
            array(StringGenerate::generate(UserGroupWidgetRules::NAME_MAX_LENGTH+1), false),
            array(StringGenerate::generate(UserGroupWidgetRules::NAME_MIN_LENGTH), true),
            array(StringGenerate::generate(UserGroupWidgetRules::NAME_MIN_LENGTH+1), true),
            array(StringGenerate::generate(UserGroupWidgetRules::NAME_MAX_LENGTH), true),
            array(StringGenerate::generate(UserGroupWidgetRules::NAME_MAX_LENGTH-1), true)
        );
    }
    //userGroupName -- end
        //shortName -- start
    /**
     * @dataProvider invalidShortNameProvider
     */
    public function testShortNameInvalid($actual, $expected)
    {
        $result = $this->widgetRule->shortName($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(USER_GROUP_SHORT_NAME_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidShortNameProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(UserGroupWidgetRules::SHORT_NAME_MIN_LENGTH-1), false),
            array(StringGenerate::generate(UserGroupWidgetRules::SHORT_NAME_MAX_LENGTH+1), false),
            array(StringGenerate::generate(UserGroupWidgetRules::SHORT_NAME_MIN_LENGTH), true),
            array(StringGenerate::generate(UserGroupWidgetRules::SHORT_NAME_MIN_LENGTH+1), true),
            array(StringGenerate::generate(UserGroupWidgetRules::SHORT_NAME_MAX_LENGTH), true),
            array(StringGenerate::generate(UserGroupWidgetRules::SHORT_NAME_MAX_LENGTH-1), true)
        );
    }
    //shortName -- end

    //administrativeArea -- start
    /**
     * @dataProvider invalidAdministrativeAreaProvider
     */
    public function testAdministrativeAreaInvalid($actual, $expected)
    {
        $result = $this->widgetRule->administrativeArea($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(USER_GROUP_ADMINISTRATIVE_AREA_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidAdministrativeAreaProvider()
    {
        return array(
            array('', false),
            array(UserGroup::ADMINISTRATIVE_AREA['WLCB'], true),
            array(UserGroup::ADMINISTRATIVE_AREA['JNQ'], true),
            array(999, false),
        );
    }
    //administrativeArea -- end
}
