<?php
namespace Sdk\UserGroup\UserGroup\Command;

use PHPUnit\Framework\TestCase;

class AddUserGroupCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'name' => $faker->name(),
            'shortName' => $faker->name(),
            'unifiedSocialCreditCode' => $faker->bothify('##############????'),
            'administrativeArea' => $faker->randomNumber()
        );

        $this->command = new AddUserGroupCommand(
            $this->fakerData['name'],
            $this->fakerData['shortName'],
            $this->fakerData['unifiedSocialCreditCode'],
            $this->fakerData['administrativeArea']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }

    public function testUserGroupNameParameter()
    {
        $this->assertEquals($this->fakerData['name'], $this->command->name);
    }

    public function testShortNameParameter()
    {
        $this->assertEquals($this->fakerData['shortName'], $this->command->shortName);
    }

    public function testUnifiedSocialCreditCodeParameter()
    {
        $this->assertEquals($this->fakerData['unifiedSocialCreditCode'], $this->command->unifiedSocialCreditCode);
    }

    public function testAdministrativeAreaParameter()
    {
        $this->assertEquals($this->fakerData['administrativeArea'], $this->command->administrativeArea);
    }
}
