<?php
namespace Sdk\UserGroup\UserGroup\Command;

use PHPUnit\Framework\TestCase;

class EditUserGroupCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'name' => $faker->name(),
            'shortName' => $faker->name(),
            'unifiedSocialCreditCode' => $faker->bothify('##############????'),
            'id' => $faker->randomNumber()
        );

        $this->command = new EditUserGroupCommand(
            $this->fakerData['name'],
            $this->fakerData['shortName'],
            $this->fakerData['unifiedSocialCreditCode'],
            $this->fakerData['id']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testUserGroupNameParameter()
    {
        $this->assertEquals($this->fakerData['name'], $this->command->name);
    }

    public function testUnifiedSocialCreditCodeParameter()
    {
        $this->assertEquals($this->fakerData['unifiedSocialCreditCode'], $this->command->unifiedSocialCreditCode);
    }

    public function testShortNameParameter()
    {
        $this->assertEquals($this->fakerData['shortName'], $this->command->shortName);
    }
}
