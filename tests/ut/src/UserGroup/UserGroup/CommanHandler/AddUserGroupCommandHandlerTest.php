<?php
namespace Sdk\UserGroup\UserGroup\CommandHandler;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Command\AddUserGroupCommand;

class AddUserGroupCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddUserGroupCommandHandler::class)
                                     ->setMethods(['getUserGroup'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetUserGroup()
    {
        $commandHandler = new MockAddUserGroupCommandHandler();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Model\UserGroup',
            $commandHandler->getUserGroup()
        );
    }

    public function initialExecute($result)
    {
        $name = 'name';
        $shortName = 'shortName';
        $administrativeArea = 1;
        $unifiedSocialCreditCode = 'unifiedSocialCreditCode';

        $command = new AddUserGroupCommand(
            $name,
            $shortName,
            $unifiedSocialCreditCode,
            $administrativeArea
        );

        $userGroup = $this->prophesize(UserGroup::class);
        $userGroup->setName(Argument::exact($name))->shouldBeCalledTimes(1);
        $userGroup->setShortName(Argument::exact($shortName))->shouldBeCalledTimes(1);
        $userGroup->setUnifiedSocialCreditCode(Argument::exact($unifiedSocialCreditCode))->shouldBeCalledTimes(1);
        $userGroup->setAdministrativeArea(Argument::exact($administrativeArea))->shouldBeCalledTimes(1);
        $userGroup->add()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->any())
            ->method('getUserGroup')
            ->willReturn($userGroup->reveal());
                    
        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);
        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }
}
