<?php
namespace Sdk\UserGroup\UserGroup\CommandHandler;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\UserGroup\UserGroup\Command\AddUserGroupCommand;
use Sdk\UserGroup\UserGroup\Command\EditUserGroupCommand;

class UserGroupCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new UserGroupCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddUserGroupCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddUserGroupCommand(
                $this->faker->name(),
                $this->faker->name(),
                $this->faker->bothify('##############????'),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\CommandHandler\AddUserGroupCommandHandler',
            $commandHandler
        );
    }

    public function testEditUserGroupCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditUserGroupCommand(
                $this->faker->name(),
                $this->faker->name(),
                $this->faker->bothify('##############????'),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\CommandHandler\EditUserGroupCommandHandler',
            $commandHandler
        );
    }
}
