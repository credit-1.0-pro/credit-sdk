<?php
namespace Sdk\UserGroup\UserGroup\CommandHandler;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Command\EditUserGroupCommand;
use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;

class EditUserGroupCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditUserGroupCommandHandler::class)
                                     ->setMethods(['getRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockEditUserGroupCommandHandler();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Repository\UserGroupRepository',
            $commandHandler->getRepository()
        );
    }

    public function initialExecute($result)
    {
        $command = new EditUserGroupCommand(
            $this->faker->name,
            $this->faker->name,
            $this->faker->bothify('##############????'),
            $this->faker->randomNumber()
        );

        $userGroup = $this->prophesize(UserGroup::class);
        $userGroup->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $userGroup->setShortName(Argument::exact($command->shortName))->shouldBeCalledTimes(1);
        $userGroup->setUnifiedSocialCreditCode(
            Argument::exact($command->unifiedSocialCreditCode)
        )->shouldBeCalledTimes(1);
        $userGroup->edit()->shouldBeCalledTimes(1)->willReturn($result);

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($userGroup->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($userGroupRepository->reveal());

        return $command;
    }
    
    public function testExecute()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }
}
