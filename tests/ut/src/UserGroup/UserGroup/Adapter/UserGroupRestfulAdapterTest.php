<?php
namespace Sdk\UserGroup\UserGroup\Adapter;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Model\NullUserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UserGroupRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockUserGroupRestfulAdapter::class)
                           ->setMethods([
                               'commonMapErrors',
                               'getTranslator',
                               'post',
                               'isSuccess',
                               'patch',
                               'translateToObject'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new UserGroupRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIUserGroupAdapter()
    {
        $adapter = new UserGroupRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Adapter\IUserGroupAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockUserGroupRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockUserGroupRestfulAdapter();
        $this->assertEquals('userGroups', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockUserGroupRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Model\NullUserGroup',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockUserGroupRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'USER_GROUP_LIST',
                UserGroupRestfulAdapter::SCENARIOS['USER_GROUP_LIST']
            ],
            [
                'USER_GROUP_FETCH_ONE',
                UserGroupRestfulAdapter::SCENARIOS['USER_GROUP_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    /**
     * 为UserGroupRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$userGroup,$keys,$userGroupArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareUserGroupTranslator(
        UserGroup $userGroup,
        array $keys,
        array $userGroupArray
    ) {
        $translator = $this->prophesize(UserGroupRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($userGroup),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($userGroupArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(UserGroup $userGroup)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($userGroup);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUserGroupTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $userGroup = \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $userGroupArray = array();

        $this->prepareUserGroupTranslator(
            $userGroup,
            array('name', 'shortName', 'administrativeArea', 'unifiedSocialCreditCode'),
            $userGroupArray
        );

        $this->adapter
            ->method('post')
            ->with('userGroups', $userGroupArray);

        $this->success($userGroup);
        $result = $this->adapter->add($userGroup);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUserGroupTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $userGroup = \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $userGroupArray = array();

        $this->prepareUserGroupTranslator(
            $userGroup,
            array('name', 'shortName', 'administrativeArea', 'unifiedSocialCreditCode'),
            $userGroupArray
        );

        $this->adapter
            ->method('post')
            ->with('userGroups', $userGroupArray);
        
            $this->failure($userGroup);
            $result = $this->adapter->add($userGroup);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUserGroupTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $userGroup = \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $userGroupArray = array();

        $this->prepareUserGroupTranslator(
            $userGroup,
            array('name', 'shortName', 'unifiedSocialCreditCode'),
            $userGroupArray
        );

        $this->adapter
            ->method('patch')
            ->with('userGroups/'.$userGroup->getId(), $userGroupArray);

        $this->success($userGroup);
        $result = $this->adapter->edit($userGroup);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUserGroupTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $userGroup = \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $userGroupArray = array();

        $this->prepareUserGroupTranslator(
            $userGroup,
            array('name', 'shortName', 'unifiedSocialCreditCode'),
            $userGroupArray
        );

        $this->adapter
            ->method('patch')
            ->with('userGroups/'.$userGroup->getId(), $userGroupArray);
        
            $this->failure($userGroup);
            $result = $this->adapter->edit($userGroup);
            $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [
            101 => [
                'name'=>USER_GROUP_NAME_FORMAT_ERROR,
                'shortName'=>USER_GROUP_SHORT_NAME_FORMAT_ERROR,
                'administrativeArea' => USER_GROUP_ADMINISTRATIVE_AREA_FORMAT_ERROR,
                'unifiedSocialCreditCode' => UNIFIED_SOCIAL_CREDIT_CODE_FORMAT_ERROR
            ],
            103 => [
                'userGroupName'=>USER_GROUP_NAME_IS_UNIQUE
            ]
        ];
        
        $result = $this->adapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
