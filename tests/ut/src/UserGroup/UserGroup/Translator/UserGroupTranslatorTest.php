<?php
namespace Sdk\UserGroup\UserGroup\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\UserGroup\UserGroup\Utils\UserGroupUtils;

class UserGroupTranslatorTest extends TestCase
{
    use UserGroupUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UserGroupTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\UserGroup\UserGroup\Model\NullUserGroup', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $userGroup = \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);

        $expression = $this->translator->objectToArray($userGroup);

        $this->compareArrayAndObject($expression, $userGroup);
    }

    public function testObjectToArrayFail()
    {
        $userGroup = null;

        $expression = $this->translator->objectToArray($userGroup);
        $this->assertEquals(array(), $expression);
    }
}
