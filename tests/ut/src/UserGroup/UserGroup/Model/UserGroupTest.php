<?php
namespace Sdk\UserGroup\UserGroup\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UserGroupTest extends TestCase
{
    private $userGroup;

    public function setUp()
    {
        $this->userGroup = new UserGroup();
    }

    public function tearDown()
    {
        unset($this->userGroup);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->userGroup
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\IOperateAble',
            $this->userGroup
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $userGroup = new MockUserGroup();
        $this->assertInstanceOf('Sdk\Common\Adapter\IOperateAbleAdapter', $userGroup->getIOperateAbleAdapter());
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Repository\UserGroupRepository',
            $userGroup->getIOperateAbleAdapter()
        );
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->userGroup->getId());

        $this->assertEmpty($this->userGroup->getName());

        $this->assertEmpty($this->userGroup->getShortName());

        $this->assertEmpty($this->userGroup->getUnifiedSocialCreditCode());

        $this->assertEquals(UserGroup::ADMINISTRATIVE_AREA['WLCB'], $this->userGroup->getAdministrativeArea());

        $this->assertEquals(0, $this->userGroup->getUpdateTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 UserGroup setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->userGroup->setId(1);
        $this->assertEquals(1, $this->userGroup->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 UserGroup setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->userGroup->setName('string');
        $this->assertEquals('string', $this->userGroup->getName());
    }

    /**
     * 设置 UserGroup setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->userGroup->setName(array(1,2,3));
    }
    //name 测试 --------------------------------------------------------   end

    //shortName 测试 -------------------------------------------------------- start
    /**
     * 设置 UserGroup setShortName() 正确的传参类型,期望传值正确
     */
    public function testSetShortNameCorrectType()
    {
        $this->userGroup->setShortName('string');
        $this->assertEquals('string', $this->userGroup->getShortName());
    }

    /**
     * 设置 UserGroup setShortName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetShortNameWrongType()
    {
        $this->userGroup->setShortName(array(1,2,3));
    }
    //shortName 测试 --------------------------------------------------------   end
    
    //unifiedSocialCreditCode 测试 -------------------------------------------------------- start
    /**
     * 设置 UserGroup setUnifiedSocialCreditCode() 正确的传参类型,期望传值正确
     */
    public function testSetUnifiedSocialCreditCodeCorrectType()
    {
        $this->userGroup->setUnifiedSocialCreditCode('string');
        $this->assertEquals('string', $this->userGroup->getUnifiedSocialCreditCode());
    }

    /**
     * 设置 UserGroup setUnifiedSocialCreditCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUnifiedSocialCreditCodeWrongType()
    {
        $this->userGroup->setUnifiedSocialCreditCode(array(1,2,3));
    }
    //unifiedSocialCreditCode 测试 --------------------------------------------------------   end
    
    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 UserGroup setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->userGroup->setStatus(0);
        $this->assertEquals(0, $this->userGroup->getStatus());
    }

    /**
     * 设置 UserGroup setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->userGroup->setStatus(array(1,2,3));
    }
    //status 测试 --------------------------------------------------------   end
    //administrativeArea 测试 ------------------------------------------------------ start
    /**
     * 循环测试 UserGroup setAdministrativeArea() 是否符合预定范围
     *
     * @dataProvider administrativeAreaProvider
     */
    public function testSetAdministrativeArea($actual, $expected)
    {
        $this->userGroup->setAdministrativeArea($actual);
        $this->assertEquals($expected, $this->userGroup->getAdministrativeArea());
    }

    /**
     * 循环测试 UserGroup setAdministrativeArea() 数据构建器
     */
    public function administrativeAreaProvider()
    {
        return array(
            array(UserGroup::ADMINISTRATIVE_AREA['WLCB'],UserGroup::ADMINISTRATIVE_AREA['WLCB']),
            array(UserGroup::ADMINISTRATIVE_AREA['JNQ'],UserGroup::ADMINISTRATIVE_AREA['JNQ']),
            array(UserGroup::ADMINISTRATIVE_AREA['FZS'],UserGroup::ADMINISTRATIVE_AREA['FZS']),
            array(UserGroup::ADMINISTRATIVE_AREA['CYQQ'],UserGroup::ADMINISTRATIVE_AREA['CYQQ']),
            array(UserGroup::ADMINISTRATIVE_AREA['CYZQ'],UserGroup::ADMINISTRATIVE_AREA['CYZQ']),
            array(UserGroup::ADMINISTRATIVE_AREA['CYHQ'],UserGroup::ADMINISTRATIVE_AREA['CYHQ']),
            array(UserGroup::ADMINISTRATIVE_AREA['SZWQ'],UserGroup::ADMINISTRATIVE_AREA['SZWQ']),
            array(UserGroup::ADMINISTRATIVE_AREA['ZZX'],UserGroup::ADMINISTRATIVE_AREA['ZZX']),
            array(UserGroup::ADMINISTRATIVE_AREA['LCX'],UserGroup::ADMINISTRATIVE_AREA['LCX']),
            array(UserGroup::ADMINISTRATIVE_AREA['XHX'],UserGroup::ADMINISTRATIVE_AREA['XHX']),
            array(UserGroup::ADMINISTRATIVE_AREA['SDX'],UserGroup::ADMINISTRATIVE_AREA['SDX']),
            array(UserGroup::ADMINISTRATIVE_AREA['HDX'],UserGroup::ADMINISTRATIVE_AREA['HDX']),
            array(999,UserGroup::ADMINISTRATIVE_AREA['WLCB']),
        );
    }

    /**
     * 设置 UserGroup setAdministrativeArea() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAdministrativeAreaWrongType()
    {
        $this->userGroup->setAdministrativeArea('string');
    }
    //administrativeArea 测试 ------------------------------------------------------   end

    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getIOperateAbleAdapter 被调用一次
     * 3. 期望 UserGroupRepository 调用 add 并且传参 $userGroup 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $userGroup = $this->getMockBuilder(MockUserGroup::class)
                           ->setMethods(['getIOperateAbleAdapter', 'nameIsExist'])
                           ->getMock();

        $userGroup->expects($this->once())->method('nameIsExist')->willReturn(true);

        //预言 UserGroupRepository
        $repository = $this->prophesize(UserGroupRepository::class);
        $repository->add(Argument::exact($userGroup))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $userGroup->expects($this->once())
             ->method('getIOperateAbleAdapter')
             ->willReturn($repository->reveal());

        //验证
        $result = $userGroup->add();
        $this->assertTrue($result);
    }

    /**
     * 测试添加失败-名称重复
     * 1. 期望返回false
     * 2. 期望nameIsExist被调用一次,并返回false
     * 3. 调用add,期望返回false
     */
    public function testAddFailNameExist()
    {
        $userGroup = $this->getMockBuilder(MockUserGroup::class)
                           ->setMethods(['nameIsExist'])
                           ->getMock();

        $userGroup->expects($this->once())->method('nameIsExist')->willReturn(false);

        $result = $userGroup->add();

        $this->assertFalse($result);
    }

    //add()
    /**
     * 测试添加失败
     * 1. 期望返回false
     * 2. 期望 getIOperateAbleAdapter 被调用一次
     * 3. 期望 UserGroupRepository 调用 add 并且传参 $userGroup 对象, 返回 false
     */
    public function testAddFail()
    {
        //初始化
        $userGroup = $this->getMockBuilder(MockUserGroup::class)
                           ->setMethods(['nameIsExist', 'getIOperateAbleAdapter'])
                           ->getMock();

        $userGroup->expects($this->once())->method('nameIsExist')->willReturn(true);

        //预言 UserGroupRepository
        $repository = $this->prophesize(UserGroupRepository::class);
        $repository->add(Argument::exact($userGroup))->shouldBeCalledTimes(1)->willReturn(false);

        //绑定
        $userGroup->expects($this->once())->method('getIOperateAbleAdapter')->willReturn($repository->reveal());

        //验证
        $result = $userGroup->add();
        $this->assertFalse($result);
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望更新 name, updateTime
     * 2. 期望 getIOperateAbleAdapter 被调用一次
     * 3. 期望 UserGroupRepository 调用 edit 并且传参 $userGroup 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $userGroup = $this->getMockBuilder(MockUserGroup::class)
                           ->setMethods(['nameIsExist', 'getIOperateAbleAdapter'])
                           ->getMock();

        $userGroup->expects($this->once())->method('nameIsExist')->willReturn(true);
                   
        //预言 UserGroupRepository
        $repository = $this->prophesize(UserGroupRepository::class);
        $repository->edit(
            Argument::exact($userGroup)
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $userGroup->expects($this->once())
             ->method('getIOperateAbleAdapter')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $userGroup->edit();
        $this->assertTrue($result);
    }

    /**
     * 测试编辑失败-名称重复
     * 1. 期望返回false
     * 2. 期望nameIsExist被调用一次,并返回false
     * 3. 调用edit,期望返回false
     */
    public function testEditFailNameExist()
    {
        //初始化
        $userGroup = $this->getMockBuilder(MockUserGroup::class)
                           ->setMethods(['nameIsExist'])
                           ->getMock();

        $userGroup->expects($this->once())->method('nameIsExist')->willReturn(false);

        //验证
        $result = $userGroup->edit();

        $this->assertFalse($result);
    }
    //edit
    /**
     * 期望编辑失败
     * 1. 期望更新 name, updateTime
     * 2. 期望 getIOperateAbleAdapter 被调用一次
     * 3. 期望 UserGroupRepository 调用 edit 并且传参 $userGroup 对象, 和相应字段, 返回false
     */
    public function testEditFail()
    {
        //初始化
        $userGroup = $this->getMockBuilder(MockUserGroup::class)
                           ->setMethods(['nameIsExist', 'getIOperateAbleAdapter'])
                           ->getMock();

        $userGroup->expects($this->once())->method('nameIsExist')->willReturn(true);

        //预言 UserGroupRepository
        $repository = $this->prophesize(UserGroupRepository::class);
        $repository->edit(
            Argument::exact($userGroup)
        )->shouldBeCalledTimes(1)
         ->willReturn(false);

        //绑定
        $userGroup->expects($this->once())
             ->method('getIOperateAbleAdapter')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $userGroup->edit();
        $this->assertFalse($result);
    }

    private function initialNameIsExist($result)
    {
        $this->userGroup = $this->getMockBuilder(MockUserGroup::class)
                           ->setMethods(['getIOperateAbleAdapter'])
                           ->getMock();

        $id = 1;
        
        $sort = ['-updateTime'];
        $userGroup = \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup($id);

        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($userGroup);
        }

        $this->userGroup->setId($userGroup->getId());
        $this->userGroup->setName($userGroup->getName());
        $this->userGroup->setAdministrativeArea($userGroup->getAdministrativeArea());

        $filter['id'] = $userGroup->getId();
        $filter['unique'] = $userGroup->getName();
        $filter['administrativeArea'] = $userGroup->getAdministrativeArea();

        $repository = $this->prophesize(UserGroupRepository::class);

        $repository->scenario(Argument::exact(UserGroupRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort)
        )->shouldBeCalledTimes(1)->willReturn([$count, $list]);

        $this->userGroup->expects($this->once())
                   ->method('getIOperateAbleAdapter')
                   ->willReturn($repository->reveal());
    }

    public function testNameIsExistFailure()
    {
        $this->initialNameIsExist(false);

        $result = $this->userGroup->nameIsExist();

        $this->assertFalse($result);
        $this->assertEquals(USER_GROUP_NAME_IS_UNIQUE, Core::getLastError()->getId());
    }

    public function testNameIsExistSuccess()
    {
        $this->initialNameIsExist(true);
        
        $result = $this->userGroup->nameIsExist();

        $this->assertTrue($result);
    }
}
