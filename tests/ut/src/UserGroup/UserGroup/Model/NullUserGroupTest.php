<?php
namespace Sdk\UserGroup\UserGroup\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullUserGroupTest extends TestCase
{
    private $userGroup;
    
    private $childUserGroup;

    public function setUp()
    {
        $this->userGroup = NullUserGroup::getInstance();
        $this->childUserGroup = new class extends NullUserGroup
        {
            public function resourceNotExist() : bool
            {
                return parent::resourceNotExist();
            }
        };
    }

    public function tearDown()
    {
        unset($this->userGroup);
        unset($this->childUserGroup);
    }

    public function testExtendsUserGroup()
    {
        $this->assertInstanceof('Sdk\UserGroup\UserGroup\Model\UserGroup', $this->userGroup);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->userGroup);
    }

    public function testResourceNotExist()
    {
        $result = $this->childUserGroup->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
