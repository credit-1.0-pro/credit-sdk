<?php
namespace Sdk\UserGroup\UserGroup\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\UserGroup\UserGroup\Adapter\UserGroupRestfulAdapter;

class UserGroupRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UserGroupRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsIUserGroupAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Adapter\IUserGroupAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Adapter\UserGroupRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Adapter\UserGroupMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(UserGroupRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
