<?php
namespace Sdk\User\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\User\Utils\UserUtils;

class UserTranslatorTest extends TestCase
{
    use UserUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder('Sdk\User\Translator\UserTranslator')->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjects()
    {

        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $user = \Sdk\User\Member\Utils\MockFactory::generateMember(1);

        $expression = $this->translator->objectToArray($user);

        $this->compareArrayAndObject($expression, $user);
    }

    public function testObjectToArrayFail()
    {
        $user = null;

        $expression = $this->translator->objectToArray($user);
        $this->assertEquals(array(), $expression);
    }
}
