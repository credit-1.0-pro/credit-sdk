<?php
namespace Sdk\User\Member\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\User\Member\Utils\MemberRestfulUtils;

class MemberRestfulTranslatorTest extends TestCase
{
    use MemberRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new MemberRestfulTranslator();
        $this->childTranslator = new class extends MemberRestfulTranslator
        {
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $member = \Sdk\User\Member\Utils\MockFactory::generateMember(1);

        $expression['data']['id'] = $member->getId();
        $expression['data']['attributes']['gender'] = $member->getGender();
        $expression['data']['attributes']['userName'] = $member->getUserName();
        $expression['data']['attributes']['realName'] = $member->getRealName();
        $expression['data']['attributes']['cellphone'] = $member->getCellphone();
        $expression['data']['attributes']['email'] = $member->getEmail();
        $expression['data']['attributes']['cardId'] = $member->getCardId();
        $expression['data']['attributes']['contactAddress'] = $member->getContactAddress();
        $expression['data']['attributes']['securityQuestion'] = $member->getSecurityQuestion();
        
        $expression['data']['attributes']['createTime'] = $member->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $member->getUpdateTime();
        $expression['data']['attributes']['status'] = $member->getStatus();
        $expression['data']['attributes']['statusTime'] = $member->getStatusTime();

        $memberObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\User\Member\Model\Member', $memberObject);
        $this->compareArrayAndObject($expression, $memberObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $member = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\User\Member\Model\NullMember', $member);
    }

    public function testObjectToArray()
    {
        $member = \Sdk\User\Member\Utils\MockFactory::generateMember(1);

        $expression = $this->translator->objectToArray($member);

        $this->compareArrayAndObject($expression, $member);
    }

    public function testObjectToArrayFail()
    {
        $member = null;

        $expression = $this->translator->objectToArray($member);
        $this->assertEquals(array(), $expression);
    }
}
