<?php
namespace Sdk\User\Member\Adapter\Member\Query\Persistence;

use PHPUnit\Framework\TestCase;

class MemberSessionCacheTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MemberSessionCache();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->stub);
    }
}
