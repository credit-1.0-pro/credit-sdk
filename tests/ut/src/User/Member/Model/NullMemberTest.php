<?php
namespace Sdk\User\Member\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NullMemberTest extends TestCase
{
    private $member;

    public function setUp()
    {
        $this->member = NullMember::getInstance();
    }

    public function tearDown()
    {
        unset($this->member);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsMember()
    {
        $this->assertInstanceOf(
            'Sdk\User\Member\Model\Member',
            $this->member
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->member
        );
    }

    public function testResourceNotExist()
    {
        $member = new MockNullMember();

        $result = $member->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testIsCellphoneExist()
    {
        $result = $this->member->isCellphoneExist();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testIsEmailExist()
    {
        $result = $this->member->isEmailExist();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testIsUserNameExist()
    {
        $result = $this->member->isUserNameExist();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testResetPassword()
    {
        $result = $this->member->resetPassword();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testUpdatePassword()
    {
        $result = $this->member->updatePassword();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testValidateSecurity()
    {
        $result = $this->member->validateSecurity();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testSignIn()
    {
        $result = $this->member->signIn();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testSaveCookie()
    {
        $result = $this->member->saveCookie();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testSaveSession()
    {
        $result = $this->member->saveSession();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testSignOut()
    {
        $result = $this->member->signOut();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testClearCookie()
    {
        $result = $this->member->clearCookie();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testClearSession()
    {
        $result = $this->member->clearSession();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testValidateIdentify()
    {
        $result = $this->member->validateIdentify('identify');

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
