<?php
namespace Sdk\User\Member\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Marmot\Framework\Classes\Cookie;

use Sdk\Common\Model\IEnableAble;

use Sdk\User\Member\Repository\MemberRepository;
use Sdk\User\Member\Repository\MemberSessionRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class MemberTest extends TestCase
{
    private $member;

    public function setUp()
    {
        $this->member = new MockMember();
    }

    public function tearDown()
    {
        unset($this->member);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals('', $this->member->getEmail());
        $this->assertEquals('', $this->member->getContactAddress());
        $this->assertEquals(Member::SECURITY_QUESTION['QUESTION_ONE'], $this->member->getSecurityQuestion());
        $this->assertEquals('', $this->member->getSecurityAnswer());
        $this->assertEquals(IEnableAble::STATUS['ENABLED'], $this->member->getStatus());
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Cookie',
            $this->member->getCookie()
        );
        $this->assertInstanceOf(
            'Sdk\User\Member\Repository\MemberRepository',
            $this->member->getRepository()
        );
        $this->assertInstanceOf(
            'Sdk\User\Member\Repository\MemberSessionRepository',
            $this->member->getMemberSessionRepository()
        );
    }

    //email 测试 -------------------------------------------------------- start
    /**
     * 设置 member setEmail() 正确的传参类型,期望传值正确
     */
    public function testSetEmailCorrectType()
    {
        $this->member->setEmail('997945678@qq.com');
        $this->assertEquals('997945678@qq.com', $this->member->getEmail());
    }

    /**
     * 设置 member setEmail() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEmailWrongType()
    {
        $this->member->setEmail(['email']);
    }
    //email 测试 --------------------------------------------------------   end

    //contactAddress 测试 -------------------------------------------------------- start
    /**
     * 设置 member setContactAddress() 正确的传参类型,期望传值正确
     */
    public function testSetContactAddressCorrectType()
    {
        $this->member->setContactAddress('雁塔区长延堡街道');
        $this->assertEquals('雁塔区长延堡街道', $this->member->getContactAddress());
    }

    /**
     * 设置 member setContactAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContactAddressWrongType()
    {
        $this->member->setContactAddress(['contactAddress']);
    }
    //contactAddress 测试 --------------------------------------------------------   end

    //securityQuestion 测试 ------------------------------------------------------ start
    /**
     * 循环测试 member setSecurityQuestion() 是否符合预定范围
     *
     * @dataProvider securityQuestionProvider
     */
    public function testSetSecurityQuestion($actual, $expected)
    {
        $this->member->setSecurityQuestion($actual);
        $this->assertEquals($expected, $this->member->getSecurityQuestion());
    }

    /**
     * 循环测试 Member setSecurityQuestion() 数据构建器
     */
    public function securityQuestionProvider()
    {
        return array(
            array(Member::SECURITY_QUESTION['QUESTION_ONE'], Member::SECURITY_QUESTION['QUESTION_ONE']),
            array(Member::SECURITY_QUESTION['QUESTION_TWO'], Member::SECURITY_QUESTION['QUESTION_TWO']),
            array(Member::SECURITY_QUESTION['QUESTION_THREE'], Member::SECURITY_QUESTION['QUESTION_THREE']),
            array(Member::SECURITY_QUESTION['QUESTION_FOUR'], Member::SECURITY_QUESTION['QUESTION_FOUR']),
            array(Member::SECURITY_QUESTION['QUESTION_FIVE'], Member::SECURITY_QUESTION['QUESTION_FIVE']),
        );
    }


    /**
     * 设置 Member setSecurityQuestion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSecurityQuestionWrongType()
    {
        $this->member->setSecurityQuestion('string');
    }
    //securityQuestion 测试 ------------------------------------------------------   end

    //securityAnswer 测试 -------------------------------------------------------- start
    /**
     * 设置 member setSecurityAnswer() 正确的传参类型,期望传值正确
     */
    public function testSetSecurityAnswerCorrectType()
    {
        $this->member->setSecurityAnswer('securityAnswer');
        $this->assertEquals('securityAnswer', $this->member->getSecurityAnswer());
    }

    /**
     * 设置 member setSecurityAnswer() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSecurityAnswerWrongType()
    {
        $this->member->setSecurityAnswer(['securityAnswer']);
    }
    //securityAnswer 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\User\Member\Repository\MemberRepository',
            $this->member->getRepository()
        );
    }

    public function testGetCookie()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Cookie',
            $this->member->getCookie()
        );
    }

    public function testGetMemberSessionRepository()
    {
        $this->assertInstanceOf(
            'Sdk\User\Member\Repository\MemberSessionRepository',
            $this->member->getMemberSessionRepository()
        );
    }

    private function initialIsCellphoneExist($result)
    {
        $this->member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $id = 1;

        $member = \Sdk\User\Member\Utils\MockFactory::generateMember(1);

        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($member);
        }

        $this->member->setCellphone($member->getCellphone());

        $filter['cellphone'] = $member->getCellphone();
        $sort = ['-updateTime'];

        $repository = $this->prophesize(MemberRepository::class);

        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort)
        )->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->member->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());
    }

    public function testIsCellphoneExistFailure()
    {
        $this->initialIsCellphoneExist(false);

        $result = $this->member->isCellphoneExist();

        $this->assertFalse($result);
        $this->assertEquals(CELLPHONE_EXIST, Core::getLastError()->getId());
    }

    public function testIsCellphoneExistSuccess()
    {
        $this->initialIsCellphoneExist(true);
        
        $result = $this->member->isCellphoneExist();

        $this->assertTrue($result);
    }

    private function initialIsEmailExist($result)
    {
        $this->member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $id = 1;
        
        $member = \Sdk\User\Member\Utils\MockFactory::generateMember(1);

        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($member);
        }

        $this->member->setEmail($member->getEmail());

        $filter['email'] = $member->getEmail();
        $sort = ['-updateTime'];

        $repository = $this->prophesize(MemberRepository::class);

        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort)
        )->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->member->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());
    }

    public function testIsEmailExistFailure()
    {
        $this->initialIsEmailExist(false);

        $result = $this->member->isEmailExist();

        $this->assertFalse($result);
        $this->assertEquals(EMAIL_EXIST, Core::getLastError()->getId());
    }

    public function testIsEmailExistSuccess()
    {
        $this->initialIsEmailExist(true);
        
        $result = $this->member->isEmailExist();

        $this->assertTrue($result);
    }

    private function initialIsUserNameExist($result)
    {
        $this->member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $id = 1;
        
        $member = \Sdk\User\Member\Utils\MockFactory::generateMember(1);

        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($member);
        }

        $this->member->setUserName($member->getUserName());

        $filter['userName'] = $member->getUserName();
        $sort = ['-updateTime'];

        $repository = $this->prophesize(MemberRepository::class);

        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort)
        )->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->member->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());
    }

    public function testIsUserNameExistFailure()
    {
        $this->initialIsUserNameExist(false);

        $result = $this->member->isUserNameExist();

        $this->assertFalse($result);
        $this->assertEquals(USER_NAME_EXIST, Core::getLastError()->getId());
    }

    public function testIsUserNameExistSuccess()
    {
        $this->initialIsUserNameExist(true);
        
        $result = $this->member->isUserNameExist();

        $this->assertTrue($result);
    }

    public function testAddSuccess()
    {
        //初始化
        $member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository', 'isCellphoneExist', 'isEmailExist', 'isUserNameExist'])
                           ->getMock();

        $member->expects($this->once())->method('isCellphoneExist')->willReturn(true);
        $member->expects($this->once())->method('isEmailExist')->willReturn(true);
        $member->expects($this->once())->method('isUserNameExist')->willReturn(true);

        //预言 MemberRepository
        $repository = $this->prophesize(MemberRepository::class);
        $repository->add(Argument::exact($member))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $member->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $member->add();
        $this->assertTrue($result);
    }

    public function testEditSuccess()
    {
        //初始化
        $member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 MemberRepository
        $repository = $this->prophesize(MemberRepository::class);
        $repository->edit(Argument::exact($member))->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $member->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $member->edit();
        $this->assertTrue($result);
    }

    public function testResetPasswordSuccess()
    {
        //初始化
        $member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 MemberRepository
        $repository = $this->prophesize(MemberRepository::class);
        $repository->resetPassword(Argument::exact($member))->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $member->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $member->resetPassword();
        $this->assertTrue($result);
    }

    public function testUpdatePasswordSuccess()
    {
        //初始化
        $member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 MemberRepository
        $repository = $this->prophesize(MemberRepository::class);
        $repository->updatePassword(Argument::exact($member))->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $member->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $member->updatePassword();
        $this->assertTrue($result);
    }

    public function testValidateSecuritySuccess()
    {
        //初始化
        $member = $this->getMockBuilder(MockMember::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 MemberRepository
        $repository = $this->prophesize(MemberRepository::class);
        $repository->validateSecurity(Argument::exact($member))->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $member->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $member->validateSecurity();
        $this->assertTrue($result);
    }

    public function testSignInFail()
    {
        $member = $this->getMockBuilder(MockMember::class)
                     ->setMethods(['getRepository'])
                     ->getMock();

        $repository = $this->prophesize(MemberRepository::class);
        $repository->signIn(Argument::exact($member))->shouldBeCalledTimes(1)->willReturn(false);

        $member->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());
                     
        $result = $member->signIn();
        $this->assertFalse($result);
    }

    public function testSignInSuccess()
    {
        $member = $this->getMockBuilder(MockMember::class)
                     ->setMethods(['getRepository', 'saveCookie', 'saveSession'])
                     ->getMock();

        $repository = $this->prophesize(MemberRepository::class);
        $repository->signIn(Argument::exact($member))->shouldBeCalledTimes(1)->willReturn(true);

        $member->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());
                     
        $member->expects($this->once())->method('saveCookie')->willReturn(true);
        $member->expects($this->once())->method('saveSession')->willReturn(true);

        $result = $member->signIn();
        $this->assertTrue($result);
    }

    public function testSignOut()
    {
        $member = $this->getMockBuilder(MockMember::class)
                     ->setMethods(['clearCookie', 'clearSession'])
                     ->getMock();
                     
        $member->expects($this->once())->method('clearCookie')->willReturn(true);
        $member->expects($this->once())->method('clearSession')->willReturn(true);

        $result = $member->signOut();
        $this->assertTrue($result);
    }

    public function testSaveSessionSuccess()
    {
        $member = $this->getMockBuilder(MockMember::class)
                     ->setMethods(['getMemberSessionRepository'])
                     ->getMock();
        
        $id = 1;
        $member->setId($id);

        $memberSessionRepository = $this->prophesize(MemberSessionRepository::class);
        $memberSessionRepository->save(Argument::exact($member))->shouldBeCalledTimes(1)->willReturn(true);
        $memberSessionRepository->get(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($member);

        $member->expects($this->exactly(2))
            ->method('getMemberSessionRepository')
            ->willReturn($memberSessionRepository->reveal());

        $result = $member->saveSession();
        $this->assertTrue($result);
        $this->assertEquals($member, Core::$container->get('member'));
    }

    public function testSaveSessionFail()
    {
        $member = $this->getMockBuilder(MockMember::class)
                     ->setMethods(['getMemberSessionRepository'])
                     ->getMock();

        $memberSessionRepository = $this->prophesize(MemberSessionRepository::class);
        $memberSessionRepository->save(Argument::exact($member))->shouldBeCalledTimes(1)->willReturn(false);

        $member->expects($this->exactly(1))
            ->method('getMemberSessionRepository')
            ->willReturn($memberSessionRepository->reveal());

        $result = $member->saveSession();
        $this->assertFalse($result);
    }

    public function testClearSession()
    {
        $member = $this->getMockBuilder(MockMember::class)
                     ->setMethods(['getMemberSessionRepository'])
                     ->getMock();

        $memberSessionRepository = $this->prophesize(MemberSessionRepository::class);
        $memberSessionRepository->clear(Argument::exact($member->getId()))->shouldBeCalledTimes(1)->willReturn(false);

        $member->expects($this->exactly(1))
            ->method('getMemberSessionRepository')
            ->willReturn($memberSessionRepository->reveal());

        $result = $member->clearSession();
        $this->assertFalse($result);
    }

    public function testSaveCookie()
    {
        $member = $this->getMockBuilder(MockMember::class)
                     ->setMethods(['getCookie'])
                     ->getMock();

        $cookie = $this->prophesize(Cookie::class);
        $cookie->add()->shouldBeCalledTimes(1)->willReturn(true);

        $member->expects($this->exactly(1))
            ->method('getCookie')
            ->willReturn($cookie->reveal());

        $result = $member->saveCookie();
        $this->assertTrue($result);
    }

    public function testClearCookie()
    {
        $member = $this->getMockBuilder(MockMember::class)
                     ->setMethods(['getCookie'])
                     ->getMock();

        $cookie = $this->prophesize(Cookie::class);
        $cookie->add()->shouldBeCalledTimes(1)->willReturn(true);

        $member->expects($this->exactly(1))
            ->method('getCookie')
            ->willReturn($cookie->reveal());

        $result = $member->clearCookie();
        $this->assertTrue($result);
    }

    public function testValidateIdentifyFailure()
    {
        $this->member->setIdentify('identify');

        $result = $this->member->validateIdentify('identify');
        $this->assertTrue($result);
    }

    public function testValidateIdentifySuccess()
    {
        $member = $this->getMockBuilder(MockMember::class)
                     ->setMethods(['clearCookie'])
                     ->getMock();

        $member->setIdentify('identify');
        $member->expects($this->once())->method('clearCookie')->willReturn(true);

        $result = $member->validateIdentify('string');
        $this->assertFalse($result);
    }
}
