<?php
namespace Sdk\User\Member\Command\Member;

use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class AddMemberCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'userName' => $faker->name(),
            'realName' => $faker->name(),
            'cardId' => $faker->creditCardNumber(),
            'cellphone' => $faker->phoneNumber(),
            'email' => $faker->email(),
            'contactAddress' => $faker->address(),
            'securityAnswer' => $faker->word(),
            'password' => md5($faker->password()),
            'securityQuestion' => $faker->randomNumber(),
            'id' => $faker->randomNumber(),
        );

        $this->command = new AddMemberCommand(
            $this->fakerData['userName'],
            $this->fakerData['realName'],
            $this->fakerData['cardId'],
            $this->fakerData['cellphone'],
            $this->fakerData['email'],
            $this->fakerData['contactAddress'],
            $this->fakerData['securityAnswer'],
            $this->fakerData['password'],
            $this->fakerData['securityQuestion'],
            $this->fakerData['id']
        );
    }
    
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testUserNameParameter()
    {
        $this->assertEquals($this->fakerData['userName'], $this->command->userName);
    }

    public function testRealNameParameter()
    {
        $this->assertEquals($this->fakerData['realName'], $this->command->realName);
    }

    public function testCardIdParameter()
    {
        $this->assertEquals($this->fakerData['cardId'], $this->command->cardId);
    }

    public function testCellphoneParameter()
    {
        $this->assertEquals($this->fakerData['cellphone'], $this->command->cellphone);
    }

    public function testEmailParameter()
    {
        $this->assertEquals($this->fakerData['email'], $this->command->email);
    }

    public function testContactAddressParameter()
    {
        $this->assertEquals($this->fakerData['contactAddress'], $this->command->contactAddress);
    }

    public function testSecurityAnswerParameter()
    {
        $this->assertEquals($this->fakerData['securityAnswer'], $this->command->securityAnswer);
    }

    public function testPasswordParameter()
    {
        $this->assertEquals($this->fakerData['password'], $this->command->password);
    }

    public function testSecurityQuestionParameter()
    {
        $this->assertEquals($this->fakerData['securityQuestion'], $this->command->securityQuestion);
    }
}
