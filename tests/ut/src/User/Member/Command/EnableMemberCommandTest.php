<?php
namespace Sdk\User\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class EnableMemberCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new EnableMemberCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
