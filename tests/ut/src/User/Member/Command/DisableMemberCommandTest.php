<?php
namespace Sdk\User\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class DisableMemberCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new DisableMemberCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
