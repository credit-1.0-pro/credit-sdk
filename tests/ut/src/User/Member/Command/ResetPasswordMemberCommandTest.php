<?php
namespace Sdk\User\Member\Command\Member;

use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class ResetPasswordMemberCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'userName' => $faker->name(),
            'securityAnswer' => $faker->word(),
            'password' => md5($faker->password()),
            'id' => $faker->randomNumber(),
        );

        $this->command = new ResetPasswordMemberCommand(
            $this->fakerData['userName'],
            $this->fakerData['securityAnswer'],
            $this->fakerData['password'],
            $this->fakerData['id']
        );
    }
    
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testUserNameParameter()
    {
        $this->assertEquals($this->fakerData['userName'], $this->command->userName);
    }

    public function testSecurityAnswerParameter()
    {
        $this->assertEquals($this->fakerData['securityAnswer'], $this->command->securityAnswer);
    }

    public function testPasswordParameter()
    {
        $this->assertEquals($this->fakerData['password'], $this->command->password);
    }
}
