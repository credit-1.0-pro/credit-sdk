<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Command\Member\ValidateSecurityMemberCommand;

class ValidateSecurityMemberCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ValidateSecurityMemberCommandHandler::class)
                                     ->setMethods(['fetchMember'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $command = new ValidateSecurityMemberCommand(
            $this->faker->word(),
            $this->faker->randomNumber()
        );

        $member = \Sdk\User\Member\Utils\MockFactory::generateMember(1);

        $member = $this->prophesize(Member::class);
        $member->setSecurityAnswer(Argument::exact($command->securityAnswer))->shouldBeCalledTimes(1);
        $member->validateSecurity()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
             ->method('fetchMember')
             ->willReturn($member->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}
