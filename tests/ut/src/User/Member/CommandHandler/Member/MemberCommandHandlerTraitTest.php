<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Repository\MemberRepository;

class MemberCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockMemberCommandHandlerTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetRepository()
    {
        $trait = new MockMemberCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\User\Member\Repository\MemberRepository',
            $trait->publicGetRepository()
        );
    }

    public function testFetchMember()
    {
        $trait = $this->getMockBuilder(MockMemberCommandHandlerTrait::class)
                 ->setMethods(['getRepository'])->getMock();

        $id = 1;

        $member = \Sdk\User\Member\Utils\MockFactory::generateMember($id);

        $repository = $this->prophesize(MemberRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($member);

        $trait->expects($this->exactly(1))
                         ->method('getRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchMember($id);

        $this->assertEquals($result, $member);
    }

    public function testGetMember()
    {
        $trait = new MockMemberCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\User\Member\Model\Member',
            $trait->publicGetMember()
        );
    }
}
