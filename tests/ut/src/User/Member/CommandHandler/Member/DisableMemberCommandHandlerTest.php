<?php
namespace Sdk\User\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

class DisableMemberCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDisableMemberCommandHandler::class)
            ->setMethods(['fetchMember'])
            ->getMock();
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\DisableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $member = \Sdk\User\Member\Utils\MockFactory::generateMember($id);

        $this->stub->expects($this->once())
             ->method('fetchMember')
             ->with($id)
             ->willReturn($member);

        $result = $this->stub->fetchIEnableObject($id);

        $this->assertEquals($result, $member);
    }
}
