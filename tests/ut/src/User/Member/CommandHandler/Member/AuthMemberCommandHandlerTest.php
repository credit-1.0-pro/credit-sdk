<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Model\NullMember;
use Sdk\User\Member\Repository\MemberRepository;
use Sdk\User\Member\Repository\MemberSessionRepository;
use Sdk\User\Member\Command\Member\AuthMemberCommand;

class AuthMemberCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = new MockAuthMemberCommandHandler();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetMemberSessionRepository()
    {
        $this->assertInstanceOf(
            'Sdk\User\Member\Repository\MemberSessionRepository',
            $this->commandHandler->getMemberSessionRepository()
        );
    }

    public function testGetMemberRepository()
    {
        $this->assertInstanceOf(
            'Sdk\User\Member\Repository\MemberRepository',
            $this->commandHandler->getMemberRepository()
        );
    }
    
    public function testExecute()
    {
        $commandHandler = $this->getMockBuilder(MockAuthMemberCommandHandler::class)
        ->setMethods([
            'getMemberSessionRepository',
            'getMemberRepository'
         ])
        ->getMock();

        $command = new AuthMemberCommand(
            1,
            'identify'
        );

        $nullMember = NullMember::getInstance();
        $member = $this->prophesize(Member::class);
        $member->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $member->validateIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1)->willReturn(true);

        $memberSessionRepository = $this->prophesize(MemberSessionRepository::class);
        $memberSessionRepository->get(
            Argument::exact($command->id)
        )->shouldBeCalledTimes(1)->willReturn($nullMember);
        $memberSessionRepository->save(Argument::exact($member->reveal()))->shouldBeCalledTimes(1)->willReturn(true);

        $commandHandler->expects($this->any())
            ->method('getMemberSessionRepository')
            ->willReturn($memberSessionRepository->reveal());

        $memberRepository = $this->prophesize(MemberRepository::class);
        $memberRepository->scenario(Argument::exact(MemberRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($memberRepository->reveal());
        $memberRepository->fetchOne(
            Argument::exact($command->id)
        )->shouldBeCalledTimes(1)->willReturn($member->reveal());

        $commandHandler->expects($this->exactly(1))
            ->method('getMemberRepository')
            ->willReturn($memberRepository->reveal());

        Core::$container->set('member', $member);

        $result = $commandHandler->execute($command);
        $this->assertTrue($result);
        $this->assertEquals($member->reveal(), Core::$container->get('member'));
    }
}
