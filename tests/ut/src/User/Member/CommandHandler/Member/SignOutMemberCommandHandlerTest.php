<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Interfaces\ICommand;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Command\Member\SignOutMemberCommand;

class SignOutMemberCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        Core::$container->set('member', new Member());
        $this->commandHandler = $this->getMockBuilder(MockSignOutMemberCommandHandler::class)
                                     ->setMethods(['getMember'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetMember()
    {
        $commandHandler = new MockSignOutMemberCommandHandler();

        $member = \Sdk\User\Member\Utils\MockFactory::generateMember(1);
        Core::$container->set('member', $member);
        
        $this->assertInstanceOf(
            'Sdk\User\Member\Model\Member',
            $commandHandler->getMember()
        );
    }

    public function testExecute()
    {
        $command = new SignOutMemberCommand();

        $member = $this->prophesize(Member::class);
        $member->signOut()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->any())
            ->method('getMember')
            ->willReturn($member->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = new SignOutMemberCommand();

        $member = $this->prophesize(Member::class);
        $member->signOut()->shouldBeCalledTimes(1)->willReturn(false);

        $this->commandHandler->expects($this->any())
            ->method('getMember')
            ->willReturn($member->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }
}
