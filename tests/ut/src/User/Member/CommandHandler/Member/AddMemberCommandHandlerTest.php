<?php
namespace Sdk\User\Member\CommandHandler\Member;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Command\Member\AddMemberCommand;

class AddMemberCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddMemberCommandHandler::class)
                                     ->setMethods(['getMember'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $faker = \Faker\Factory::create('zh_CN');
        $userName = $faker->name();
        $realName = $faker->name();
        $cardId = $faker->creditCardNumber();
        $cellphone = $faker->phoneNumber();
        $email = $faker->email();
        $contactAddress = $faker->address();
        $securityAnswer = $faker->word();
        $password = md5($faker->password());
        $securityQuestion = $faker->randomNumber();

        $command = new AddMemberCommand(
            $userName,
            $realName,
            $cardId,
            $cellphone,
            $email,
            $contactAddress,
            $securityAnswer,
            $password,
            $securityQuestion
        );

        $member = $this->prophesize(Member::class);
        $member->setUserName(Argument::exact($command->userName))->shouldBeCalledTimes(1);
        $member->setRealName(Argument::exact($command->realName))->shouldBeCalledTimes(1);
        $member->setCellphone(Argument::exact($command->cellphone))->shouldBeCalledTimes(1);
        $member->setEmail(Argument::exact($command->email))->shouldBeCalledTimes(1);
        $member->setCardId(Argument::exact($command->cardId))->shouldBeCalledTimes(1);
        $member->setContactAddress(Argument::exact($command->contactAddress))->shouldBeCalledTimes(1);
        $member->setSecurityQuestion(Argument::exact($command->securityQuestion))->shouldBeCalledTimes(1);
        $member->setSecurityAnswer(Argument::exact($command->securityAnswer))->shouldBeCalledTimes(1);
        $member->setPassword(Argument::exact($command->password))->shouldBeCalledTimes(1);
        $member->add()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->exactly(1))
            ->method('getMember')
            ->willReturn($member->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
