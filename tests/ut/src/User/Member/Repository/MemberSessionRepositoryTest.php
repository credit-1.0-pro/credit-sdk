<?php
namespace Sdk\User\Member\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Adapter\Member\MemberSessionAdapter;

class MemberSessionRepositoryTest extends TestCase
{
    private $repository;
    private $childRepository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MemberSessionRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
        $this->childRepository = new class extends MemberSessionRepository
        {
            public function getAdapter() : MemberSessionAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->childRepository);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\User\Member\Adapter\Member\MemberSessionAdapter',
            $this->childRepository->getAdapter()
        );
    }

    public function testSave()
    {
        $member = \Sdk\User\Member\Utils\MockFactory::generateMember(1);

        $adapter = $this->prophesize(MemberSessionAdapter::class);
        $adapter->save(Argument::exact($member))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->save($member);
    }

    public function testGet()
    {
        $id = 1;

        $adapter = $this->prophesize(MemberSessionAdapter::class);
        $adapter->get(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->get($id);
    }

    public function testClear()
    {
        $id = 1;

        $adapter = $this->prophesize(MemberSessionAdapter::class);
        $adapter->del(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->clear($id);
    }
}
