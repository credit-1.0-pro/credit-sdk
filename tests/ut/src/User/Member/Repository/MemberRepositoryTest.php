<?php
namespace Sdk\User\Member\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\Member\Adapter\Member\MemberRestfulAdapter;

class MemberRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MemberRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
        
        $this->childRepository = new class extends MemberRepository
        {
            public function getActualAdapter()
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter()
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->childRepository);
    }

    public function testImplementsIMemberAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\User\Member\Adapter\Member\IMemberAdapter',
            $this->repository
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(MemberRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\User\Member\Adapter\Member\MemberRestfulAdapter',
            $this->childRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\User\Member\Adapter\Member\MemberMockAdapter',
            $this->childRepository->getMockAdapter()
        );
    }

    public function testSignIn()
    {
        $member = \Sdk\User\Member\Utils\MockFactory::generateMember(1);

        $adapter = $this->prophesize(MemberRestfulAdapter::class);
        $adapter->signIn(Argument::exact($member))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->signIn($member);
    }

    public function testResetPassword()
    {
        $member = \Sdk\User\Member\Utils\MockFactory::generateMember(1);

        $adapter = $this->prophesize(MemberRestfulAdapter::class);
        $adapter->resetPassword(Argument::exact($member))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->resetPassword($member);
    }

    public function testUpdatePassword()
    {
        $member = \Sdk\User\Member\Utils\MockFactory::generateMember(1);

        $adapter = $this->prophesize(MemberRestfulAdapter::class);
        $adapter->updatePassword(Argument::exact($member))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->updatePassword($member);
    }

    public function testValidateSecurity()
    {
        $member = \Sdk\User\Member\Utils\MockFactory::generateMember(1);

        $adapter = $this->prophesize(MemberRestfulAdapter::class);
        $adapter->validateSecurity(Argument::exact($member))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->validateSecurity($member);
    }
}
