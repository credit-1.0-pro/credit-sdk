<?php
namespace Sdk\User\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Server;

use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\IOperateAbleAdapter;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class UserTest extends TestCase
{
    private $user;

    private $childUser;

    public function setUp()
    {
        $this->user = $this->getMockBuilder('Sdk\User\Model\User')->getMockForAbstractClass();
        $this->stub = $this->getMockBuilder(User::class)
        ->setMethods([
            'getRepository'
        ])->getMock();
    }

    public function tearDown()
    {
        unset($this->user);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceof("Marmot\Common\Model\IObject", $this->user);
    }

    public function testImplementsIEnableAble()
    {
        $this->assertInstanceof("Sdk\Common\Model\IEnableAble", $this->user);
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceof("Sdk\Common\Model\IOperateAble", $this->user);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->user->getId());

        $this->assertEmpty($this->user->getCellphone());

        $this->assertEmpty($this->user->getRealName());

        $this->assertEmpty($this->user->getUserName());

        $this->assertEmpty($this->user->getPassword());

        $this->assertEmpty($this->user->getOldPassword());

        $this->assertEquals(array(), $this->user->getAvatar());

        $this->assertEquals(User::GENDER['GENDER_MALE'], $this->user->getGender());

        $this->assertEmpty($this->user->getIdentify());

        $this->assertEmpty($this->user->getCardId());

        $this->assertEquals(0, $this->user->getCreateTime());

        $this->assertEquals(0, $this->user->getUpdateTime());

        $this->assertEquals(IEnableAble::STATUS['ENABLED'], $this->user->getStatus());

        $this->assertEquals(0, $this->user->getStatusTime());
    }

    public function testSetId()
    {
        $this->user->setId(1);
        $this->assertEquals(1, $this->user->getId());
    }

    //cellphone 测试 --------------------------------------------------- start
    /**
     * 设置 User setCellphone() 正确的传参类型,期望传值正确
     */
    public function testSetCellphoneCorrectType()
    {
        $this->user->setCellphone('15202939435');
        $this->assertEquals('15202939435', $this->user->getCellphone());
    }
    
    /**
     * 设置 User setCellphone() 正确的传参类型,但是不属于手机格式,期望返回空.
     */
    public function testSetCellphoneCorrectTypeButNotCellphone()
    {
        $this->user->setCellphone('15202939435'.'a');
        $this->assertEquals('', $this->user->getCellphone());
    }
    //cellphone 测试 ---------------------------------------------------   end

    //realName 测试 ---------------------------------------------------- start
    /**
     * 设置 User setRealName() 正确的传参类型,期望传值正确
     */
    public function testSetRealNameCorrectType()
    {
        $this->user->setRealName('string');
        $this->assertEquals('string', $this->user->getRealName());
    }

    /**
     * 设置 User setRealName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRealNameWrongType()
    {
        $this->user->setRealName(array(1,2,3));
    }
    //realName 测试 ----------------------------------------------------   end

    //userName 测试 ---------------------------------------------------- start
    /**
     * 设置 User setUserName() 正确的传参类型,期望传值正确
     */
    public function testSetUserNameCorrectType()
    {
        $this->user->setUserName('string');
        $this->assertEquals('string', $this->user->getUserName());
    }

    /**
     * 设置 User setUserName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserNameWrongType()
    {
        $this->user->setUserName(array(1,2,3));
    }
    //userName 测试 ----------------------------------------------------   end

    //password 测试 ---------------------------------------------------- start
    /**
     * 设置 User setPassword() 正确的传参类型,期望传值正确
     */
    public function testSetPasswordCorrectType()
    {
        $this->user->setPassword('string');
        $this->assertEquals('string', $this->user->getPassword());
    }

    /**
     * 设置 User setPassword() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPasswordWrongType()
    {
        $this->user->setPassword(array(1,2,3));
    }
    //password 测试 ----------------------------------------------------   end

    //oldPassword 测试 ---------------------------------------------------- start
    /**
     * 设置 User setOldPassword() 正确的传参类型,期望传值正确
     */
    public function testSetOldPasswordCorrectType()
    {
        $this->user->setOldPassword('string');
        $this->assertEquals('string', $this->user->getOldPassword());
    }

    /**
     * 设置 User setOldPassword() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOldPasswordWrongType()
    {
        $this->user->setOldPassword(array(1,2,3));
    }
    //oldPassword 测试 ----------------------------------------------------   end

    //avatar 测试 -------------------------------------------------------- start
    /**
     * 设置 User setAvatar() 正确的传参类型,期望传值正确
     */
    public function testSetAvatarCorrectType()
    {
        $this->user->setAvatar(array(1));
        $this->assertEquals(array(1), $this->user->getAvatar());
    }

    /**
     * 设置 User setAvatar() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAvatarWrongType()
    {
        $this->user->setAvatar('string');
    }
    //avatar 测试 --------------------------------------------------------   end

    //gender 测试 -------------------------------------------------------- start
    /**
     * 设置 User setGender() 正确的传参类型,期望传值正确
     */
    public function testSetGenderCorrectType()
    {
        $this->user->setGender(1);
        $this->assertEquals(1, $this->user->getGender());
    }

    /**
     * 设置 User setGender() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGenderWrongType()
    {
        $this->user->setGender('string');
    }
    //gender 测试 --------------------------------------------------------   end

    //identify 测试 ---------------------------------------------------- start
    /**
     * 设置 User setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->user->setIdentify('string');
        $this->assertEquals('string', $this->user->getIdentify());
    }

    /**
     * 设置 User setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->user->setIdentify(array(1,2,3));
    }
    //identify 测试 ----------------------------------------------------   end

    //cardId 测试 ---------------------------------------------------- start
    /**
     * 设置 User setCardId() 正确的传参类型,期望传值正确
     */
    public function testSetCardIdCorrectType()
    {
        $this->user->setCardId('string');
        $this->assertEquals('string', $this->user->getCardId());
    }

    /**
     * 设置 User setCardId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCardIdWrongType()
    {
        $this->user->setCardId(array(1,2,3));
    }
    //cardId 测试 ----------------------------------------------------   end

    public function testGetIEnableAbleAdapter()
    {
        $user = $this->getMockBuilder(MockUser::class)->getMockForAbstractClass();
        $this->assertInstanceOf(
            'Sdk\User\Crew\Repository\CrewRepository',
            $user->getIEnableAbleAdapter()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $user = $this->getMockBuilder(MockUser::class)->getMockForAbstractClass();
        $this->assertInstanceOf(
            'Sdk\User\Crew\Repository\CrewRepository',
            $user->getIOperateAbleAdapter()
        );
    }

    //generateIdentify 测试 ---------------------------------------------  start
    public function testGenerateIdentify()
    {
        $result = $this->user->generateIdentify();

        $identify = md5(serialize(Server::get('marmot')).Core::$container->get('time'));

        $this->assertEquals(32, strlen($result));
        $this->assertEquals($result, $identify);
    }
    //generateIdentify 测试 ----------------------------------------------  end
}
