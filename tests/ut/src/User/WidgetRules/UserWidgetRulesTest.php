<?php
namespace Sdk\User\WidgetRules;

use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Sdk\Common\Utils\StringGenerate;

use Sdk\Common\Persistence\UtilsSession;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class UserWidgetRulesTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = UserWidgetRules::getInstance();

        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //realName -- start
    /**
     * @dataProvider invalidRealNameProvider
     */
    public function testRealNameInvalid($actual, $expected)
    {
        $result = $this->widgetRule->realName($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(REAL_NAME_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidRealNameProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(UserWidgetRules::REAL_NAME_MIN_LENGTH-1), false),
            array(StringGenerate::generate(UserWidgetRules::REAL_NAME_MAX_LENGTH+1), false),
            array(StringGenerate::generate(UserWidgetRules::REAL_NAME_MIN_LENGTH), true),
            array(StringGenerate::generate(UserWidgetRules::REAL_NAME_MIN_LENGTH+1), true),
            array(StringGenerate::generate(UserWidgetRules::REAL_NAME_MAX_LENGTH), true),
            array(StringGenerate::generate(UserWidgetRules::REAL_NAME_MAX_LENGTH-1), true)
        );
    }
    //realName -- end
    
    //cardId -- start
    /**
     * @dataProvider invalidCardIdProvider
     */
    public function testCardIdInvalid($actual, $expected)
    {
        $result = $this->widgetRule->cardId($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CARD_ID_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidCardIdProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array($faker->bothify(''), false),
            array($faker->bothify('##??'), false),//四位混编
            array($faker->bothify('??????#####今???'), false),//十五位
            array($faker->bothify('??????#########?#'), false),//十六位
            array($faker->bothify('???###???####????##'), false),//十九位
            array($faker->bothify('###############'), true),//十五位
            array($faker->bothify('##################'), true),//十八位
            array($faker->bothify('#################?'), false),//十八位
        );
    }
    //cardId -- end

     //cellphone -- start
    /**
     * @dataProvider invalidCellphoneProvider
     */
    public function testCellphoneInvalid($actual, $expected)
    {
        $result = $this->widgetRule->cellphone($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CELLPHONE_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidCellphoneProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array('1520293932', false),
            array($faker->phoneNumber(), true),
        );
    }
    //cellphone -- end
    
    //password -- start
    /**
     * @dataProvider invalidPasswordProvider
     */
    public function testPasswordInvalid($actual, $pointer, $expected)
    {
        $result = $this->widgetRule->password($actual, $pointer);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PASSWORD_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidPasswordProvider()
    {
        return array(
            array('', 'password', false),
            array(StringGenerate::generate(5), 'password', false),
            array(StringGenerate::generate(31), 'aaa', false),
            array('0000000000', 'password', false),
            array('aaaaaaaaaa', 'password', false),
            array('PXpassword0000*', 'password', true)
        );
    }
    //password -- end
    
    //confirmPassword -- start
    /**
     * @dataProvider invalidConfirmPasswordProvider
     */
    public function testConfirmPasswordInvalid($actual, $confirmActual, $expected)
    {
        $result = $this->widgetRule->confirmPassword($actual, $confirmActual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PASSWORD_INCONSISTENCY, Core::getLastError()->getId());
    }

    public function invalidConfirmPasswordProvider()
    {
        return array(
            array('password', 'confirmPassword', false),
            array('0000000000', 'confirmPassword', false),
            array('aaaaaaaaaa', 'confirmPassword', false),
            array('confirmPassword', 'confirmPassword', true)
        );
    }
    //confirmPassword -- end

    public function testHashSuccess()
    {
        $session = new UtilsSession();
        $hash = 'hash';

        $session->save('cryptojs_hash', $hash);

        $result = $this->widgetRule->hash();
        $this->assertTrue($result);
    }

    public function testHashFail()
    {
        $session = new UtilsSession();
        $hash = '';

        $session->save('cryptojs_hash', $hash);

        $result = $this->widgetRule->hash();

        $this->assertFalse($result);
        $this->assertEquals(HASH_INVALID, Core::getLastError()->getId());
    }
}
