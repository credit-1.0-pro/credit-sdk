<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use PHPUnit\Framework\TestCase;

class EnableCrewCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockEnableCrewCommandHandler::class)
            ->setMethods(['fetchCrew'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\EnableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew($id);

        $this->stub->expects($this->once())
             ->method('fetchCrew')
             ->with($id)
             ->willReturn($crew);

        $result = $this->stub->fetchIEnableObject($id);

        $this->assertEquals($result, $crew);
    }
}
