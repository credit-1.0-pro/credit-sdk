<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Model\Guest;
use Sdk\User\Crew\Repository\CrewRepository;
use Sdk\User\Crew\Repository\CrewSessionRepository;
use Sdk\User\Crew\Command\Crew\AuthCrewCommand;

class AuthCrewCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = new MockAuthCrewCommandHandler();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetCrewSessionRepository()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Repository\CrewSessionRepository',
            $this->commandHandler->getCrewSessionRepository()
        );
    }

    public function testGetCrewRepository()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Repository\CrewRepository',
            $this->commandHandler->getCrewRepository()
        );
    }
    
    public function testExecute()
    {
        $commandHandler = $this->getMockBuilder(MockAuthCrewCommandHandler::class)
        ->setMethods([
            'getCrewSessionRepository',
            'getCrewRepository'
         ])
        ->getMock();

        $command = new AuthCrewCommand(
            1,
            'identify'
        );

        // $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew($command->id);

        $crew = $this->prophesize(Crew::class);
        $crew->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $crew->validateIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1)->willReturn(true);

        $crewSessionRepository = $this->prophesize(CrewSessionRepository::class);
        $crewSessionRepository->get(
            Argument::exact($command->id)
        )->shouldBeCalledTimes(1)->willReturn($crew->reveal());
        $crewSessionRepository->save(Argument::exact($crew->reveal()))->shouldBeCalledTimes(1)->willReturn(true);

        $commandHandler->expects($this->any())
            ->method('getCrewSessionRepository')
            ->willReturn($crewSessionRepository->reveal());

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->scenario(Argument::exact(CrewRepository::FETCH_ONE_MODEL_UN))
            ->willReturn($crewRepository->reveal());
        $crewRepository->fetchOne(Argument::exact($command->id))->shouldBeCalledTimes(1)->willReturn($crew->reveal());

        $commandHandler->expects($this->exactly(1))
            ->method('getCrewRepository')
            ->willReturn($crewRepository->reveal());

        Core::$container->set('crew', $crew);

        $result = $commandHandler->execute($command);
        $this->assertTrue($result);
        $this->assertEquals($crew->reveal(), Core::$container->get('crew'));
    }
}
