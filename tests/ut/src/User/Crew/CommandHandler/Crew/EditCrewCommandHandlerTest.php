<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Repository\CrewRepository;
use Sdk\User\Crew\Command\Crew\EditCrewCommand;

use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;
use Sdk\UserGroup\Department\Repository\DepartmentRepository;

class EditCrewCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditCrewCommandHandler::class)
            ->setMethods(['getRepository', 'getUserGroupRepository', 'getDepartmentRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetUserGroupRepository()
    {
        $commandHandler = new MockEditCrewCommandHandler();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Repository\UserGroupRepository',
            $commandHandler->getUserGroupRepository()
        );
    }

    public function testGetDepartmentRepository()
    {
        $commandHandler = new MockEditCrewCommandHandler();
        $this->assertInstanceOf(
            'Sdk\UserGroup\Department\Repository\DepartmentRepository',
            $commandHandler->getDepartmentRepository()
        );
    }

    public function testGetRepository()
    {
        $commandHandler = new MockEditCrewCommandHandler();
        $this->assertInstanceOf(
            'Sdk\User\Crew\Repository\CrewRepository',
            $commandHandler->getRepository()
        );
    }

    public function testExecute()
    {
        $realName = 'name';
        $cardId = 'cardId';
        $userGroupId = 1;
        $category = 1;
        $departmentId = 1;
        $purview = [];
        $id = 1;
        
        $command = new EditCrewCommand(
            $realName,
            $cardId,
            $userGroupId,
            $category,
            $departmentId,
            $purview,
            $id
        );

        $userGroup = \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $userGroupId = $userGroupId;
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))
            ->shouldBeCalledTimes(1)->willReturn($userGroup);
        $this->commandHandler->expects($this->exactly(1))
                   ->method('getUserGroupRepository')
                   ->willReturn($userGroupRepository->reveal());
    
        $department = \Sdk\UserGroup\Department\Utils\MockDepartmentFactory::generateDepartment(1);
        $departmentId = $departmentId;
        $departmentRepository = $this->prophesize(DepartmentRepository::class);
        $departmentRepository->fetchOne(Argument::exact($departmentId))
            ->shouldBeCalledTimes(1)->willReturn($department);
        $this->commandHandler->expects($this->exactly(1))
                    ->method('getDepartmentRepository')
                    ->willReturn($departmentRepository->reveal());

        $crew = $this->prophesize(Crew::class);
        $crew->setCardId(Argument::exact($command->cardId))->shouldBeCalledTimes(1);
        $crew->setRealName(Argument::exact($command->realName))->shouldBeCalledTimes(1);
        $crew->setCategory(Argument::exact($command->category))->shouldBeCalledTimes(1);
        $crew->setUserGroup(Argument::exact($userGroup))->shouldBeCalledTimes(1);
        $crew->setDepartment(Argument::exact($department))->shouldBeCalledTimes(1);
        $crew->setPurview(Argument::exact($command->purview))->shouldBeCalledTimes(1);
        $crew->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $crewRepository = $this->prophesize(CrewRepository::class);
        $crewRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($crew->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($crewRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
