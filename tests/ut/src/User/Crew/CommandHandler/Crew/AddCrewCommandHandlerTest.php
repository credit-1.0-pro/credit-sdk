<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Command\Crew\AddCrewCommand;

use Sdk\UserGroup\UserGroup\Repository\UserGroupRepository;
use Sdk\UserGroup\Department\Repository\DepartmentRepository;

class AddCrewCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddCrewCommandHandler::class)
            ->setMethods(['getCrew', 'getUserGroupRepository', 'getDepartmentRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetCrew()
    {
        $commandHandler = new MockAddCrewCommandHandler();
        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Crew',
            $commandHandler->getCrew()
        );
    }

    public function testGetUserGroupRepository()
    {
        $commandHandler = new MockAddCrewCommandHandler();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Repository\UserGroupRepository',
            $commandHandler->getUserGroupRepository()
        );
    }

    public function testGetDepartmentRepository()
    {
        $commandHandler = new MockAddCrewCommandHandler();
        $this->assertInstanceOf(
            'Sdk\UserGroup\Department\Repository\DepartmentRepository',
            $commandHandler->getDepartmentRepository()
        );
    }

    public function initialExecute()
    {
        $realName = 'name';
        $cellphone = '110';
        $password = 'password';
        $cardId = 'cardId';
        $userGroupId = 1;
        $category = 1;
        $departmentId = 1;
        $purview = [];
        $id = 1;
        
        $command = new AddCrewCommand(
            $realName,
            $cellphone,
            $password,
            $cardId,
            $userGroupId,
            $category,
            $departmentId,
            $purview,
            $id
        );
    
        $department = \Sdk\UserGroup\Department\Utils\MockDepartmentFactory::generateDepartment(1);
        $departmentId = $departmentId;
        $departmentRepository = $this->prophesize(DepartmentRepository::class);
        $departmentRepository->fetchOne(Argument::exact($departmentId))
            ->shouldBeCalledTimes(1)->willReturn($department);
        $this->commandHandler->expects($this->exactly(1))
                    ->method('getDepartmentRepository')
                    ->willReturn($departmentRepository->reveal());

        $userGroup = \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $userGroupId = $userGroupId;
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))
            ->shouldBeCalledTimes(1)->willReturn($userGroup);
        $this->commandHandler->expects($this->exactly(1))
                    ->method('getUserGroupRepository')
                    ->willReturn($userGroupRepository->reveal());

        $crew = $this->prophesize(Crew::class);
        $crew->setCellphone(Argument::exact($command->cellphone))->shouldBeCalledTimes(1);
        $crew->setRealName(Argument::exact($command->realName))->shouldBeCalledTimes(1);
        $crew->setPassword(Argument::exact($command->password))->shouldBeCalledTimes(1);
        $crew->setUserGroup(Argument::exact($userGroup))->shouldBeCalledTimes(1);
        $crew->setCardId(Argument::exact($command->cardId))->shouldBeCalledTimes(1);
        $crew->setCategory(Argument::exact($command->category))->shouldBeCalledTimes(1);
        $crew->setPurview(Argument::exact($command->purview))->shouldBeCalledTimes(1);
        $crew->setDepartment(Argument::exact($department))->shouldBeCalledTimes(1);
        $crew->add()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->any())
            ->method('getCrew')
            ->willReturn($crew->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);

        $this->assertTrue($result);
    }
}
