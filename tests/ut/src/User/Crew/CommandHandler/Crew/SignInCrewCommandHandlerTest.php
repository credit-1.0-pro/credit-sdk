<?php
namespace Sdk\User\Crew\CommandHandler\Crew;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\User\Crew\Model\Guest;
use Sdk\User\Crew\Command\Crew\SignInCrewCommand;

class SignInCrewCommandHandlerTest extends TestCase
{
    private $commandHandler;
    private $childCommandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(SignInCrewCommandHandler::class)
                                     ->setMethods(['getCrew'])
                                     ->getMock();
        $this->childCommandHandler = new class extends SignInCrewCommandHandler
        {
            public function getCrew()
            {
                return parent::getCrew();
            }
        };

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->childCommandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetCrew()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Guest',
            $this->childCommandHandler->getCrew()
        );
    }

    public function testExecute()
    {
        $command = new SignInCrewCommand(
            $this->faker->name,
            $this->faker->password
        );

        $crew = $this->prophesize(Guest::class);
        $crew->setUserName(Argument::exact($command->userName))->shouldBeCalledTimes(1);
        $crew->setPassword(Argument::exact($command->password))->shouldBeCalledTimes(1);
        $crew->signIn()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->any())
            ->method('getCrew')
            ->willReturn($crew->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = new SignInCrewCommand(
            $this->faker->name,
            $this->faker->password
        );

        $crew = $this->prophesize(Guest::class);
        $crew->setUserName(Argument::exact($command->userName))->shouldBeCalledTimes(1);
        $crew->setPassword(Argument::exact($command->password))->shouldBeCalledTimes(1);
        $crew->signIn()->shouldBeCalledTimes(1)->willReturn(false);

        $this->commandHandler->expects($this->any())
            ->method('getCrew')
            ->willReturn($crew->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }
}
