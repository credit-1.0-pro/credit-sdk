<?php
namespace Sdk\User\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class AuthCrewCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'id' => $faker->randomNumber(),
            'identify' => $faker->name()
        );

        $this->command = new AuthCrewCommand(
            $this->fakerData['id'],
            $this->fakerData['identify']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
