<?php
namespace Sdk\User\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class EnableCrewCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new EnableCrewCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
