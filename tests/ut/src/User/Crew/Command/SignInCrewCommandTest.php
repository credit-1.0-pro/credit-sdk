<?php
namespace Sdk\User\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class SignInCrewCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'userName' => $faker->name(),
            'password' => $faker->name(),
            'id' => $faker->randomNumber()
        );

        $this->command = new SignInCrewCommand(
            $this->fakerData['userName'],
            $this->fakerData['password'],
            $this->fakerData['id']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }

    public function testUserNameParameter()
    {
        $this->assertEquals($this->fakerData['userName'], $this->command->userName);
    }

    public function testPasswordParameter()
    {
        $this->assertEquals($this->fakerData['password'], $this->command->password);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testConstructWithDefault()
    {
        $command = new SignInCrewCommand(
            $this->fakerData['userName'],
            $this->fakerData['password'],
            0
        );

        $this->assertEquals(0, $command->id);
    }
}
