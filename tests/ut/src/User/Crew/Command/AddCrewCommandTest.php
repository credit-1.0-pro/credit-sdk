<?php
namespace Sdk\User\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class AddCrewCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'realName' => $faker->name(),
            'cellphone' => $faker->name(),
            'password' => $faker->name(),
            'cardId' => $faker->name(),
            'userGroupId' => $faker->randomNumber(),
            'category' => $faker->randomNumber(),
            'departmentId' => $faker->randomNumber(),
            'purview' => array($faker->randomNumber()),
            'id' => $faker->randomNumber()
        );

        $this->command = new AddCrewCommand(
            $this->fakerData['realName'],
            $this->fakerData['cellphone'],
            $this->fakerData['password'],
            $this->fakerData['cardId'],
            $this->fakerData['userGroupId'],
            $this->fakerData['category'],
            $this->fakerData['departmentId'],
            $this->fakerData['purview'],
            $this->fakerData['id']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
