<?php
namespace Sdk\User\Crew\Adapter\Crew;

use Sdk\User\Crew\Model\Crew;
use PHPUnit\Framework\TestCase;

class CrewMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new CrewMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testSignIn()
    {
        $this->assertTrue($this->adapter->signIn(new Crew()));
    }

    public function testUpdatePassword()
    {
        $this->assertTrue($this->adapter->updatePassword(new Crew()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Crew',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\User\Crew\Model\Crew',
                $each
            );
        }
    }

    public function testFetchOneAsync()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Crew',
            $this->adapter->fetchOneAsync(1)
        );
    }

    public function testFetchListAsync()
    {
        $list = $this->adapter->fetchListAsync([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\User\Crew\Model\Crew',
                $each
            );
        }
    }

    public function testSearch()
    {
        list($list, $count) = $this->adapter->search(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\User\Crew\Model\Crew',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }

    public function testSearchAsync()
    {
        list($list, $count) = $this->adapter->searchAsync(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\User\Crew\Model\Crew',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
