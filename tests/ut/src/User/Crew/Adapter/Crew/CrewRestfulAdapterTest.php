<?php
namespace Sdk\User\Crew\Adapter\Crew;

use Prophecy\Argument;
use Marmot\Interfaces\INull;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Model\Guest;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class CrewRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(CrewRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors'
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends CrewRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getCrewMapErrors() : array
            {
                return parent::getMapErrors();
            }
            public function getNullObject() : INull
            {
                return parent::getNullObject();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsICrewAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Adapter\Crew\ICrewAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('users/crews', $this->childAdapter->getResource());
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->childAdapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Guest',
            $this->childAdapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'CREW_LIST',
                CrewRestfulAdapter::SCENARIOS['CREW_LIST']
            ],
            [
                'CREW_FETCH_ONE',
                CrewRestfulAdapter::SCENARIOS['CREW_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    /**
     * 为CrewRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$crew,$keys,$crewArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareCrewTranslator(
        Crew $crew,
        array $keys,
        array $crewArray
    ) {
        $translator = $this->prophesize(CrewRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($crew),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($crewArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Crew $crew)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($crew);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCrewTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew(1);
        $crewArray = array();

        $this->prepareCrewTranslator(
            $crew,
            array(
                'realName',
                'cellphone',
                'password',
                'cardId',
                'userGroup',
                'category',
                'department',
                'purview'
            ),
            $crewArray
        );

        $this->adapter
            ->method('post')
            ->with('', $crewArray);

        $this->success($crew);
        $result = $this->adapter->add($crew);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCrewTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew(1);
        $crewArray = array();

        $this->prepareCrewTranslator(
            $crew,
            array(
                'realName',
                'cellphone',
                'password',
                'cardId',
                'userGroup',
                'category',
                'department',
                'purview'
            ),
            $crewArray
        );

        $this->adapter
            ->method('post')
            ->with('', $crewArray);
        
            $this->failure($crew);
            $result = $this->adapter->add($crew);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCrewTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew(1);
        $crewArray = array();

        $this->prepareCrewTranslator(
            $crew,
            array(
                'realName',
                'cardId',
                'userGroup',
                'category',
                'department',
                'purview'
            ),
            $crewArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$crew->getId(), $crewArray);

        $this->success($crew);
        $result = $this->adapter->edit($crew);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCrewTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew(1);
        $crewArray = array();

        $this->prepareCrewTranslator(
            $crew,
            array(
                'realName',
                'cardId',
                'userGroup',
                'category',
                'department',
                'purview'
            ),
            $crewArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$crew->getId(), $crewArray);
        
            $this->failure($crew);
            $result = $this->adapter->edit($crew);
            $this->assertFalse($result);
    }

/**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCrewTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行signIn（）
     * 判断 result 是否为true
     */
    public function testSignInSuccess()
    {
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew(1);
        $crewArray = array();

        $this->prepareCrewTranslator(
            $crew,
            array(
                'userName',
                'password'
            ),
            $crewArray
        );

        $this->adapter
            ->method('post')
            ->with('/signIn', $crewArray);

        $this->success($crew);
        $result = $this->adapter->signIn($crew);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCrewTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行signIn（）
     * 判断 result 是否为false
     */
    public function testSignInFailure()
    {
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew(1);
        $crewArray = array();

        $this->prepareCrewTranslator(
            $crew,
            array(
                'userName',
                'password'
            ),
            $crewArray
        );

        $this->adapter
            ->method('post')
            ->with('/signIn', $crewArray);
        
        $this->failure($crew);
        $result = $this->adapter->signIn($crew);
        $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [
            10 => CELLPHONE_NOT_EXIST,
            101 => [
                'category'=>CREW_CATEGORY_NOT_EXIST,
                'purview'=>CREW_PURVIEW_FORMAT_ERROR,
                'userGroupId'=>CREW_USER_GROUP_FORMAT_ERROR,
                'departmentId'=>CREW_DEPARTMENT_FORMAT_ERROR
            ],
            102 => [
                'status'=>STATUS_CAN_NOT_MODIFY,
                'crewStatus'=>USER_STATUS_DISABLE,
            ],
            103 => [
                'cellphone'=>CELLPHONE_EXIST
            ],
            501 => REAL_NAME_FORMAT_ERROR,
            502 => CELLPHONE_FORMAT_ERROR,
            503 => PASSWORD_FORMAT_ERROR,
            504 => CARD_ID_FORMAT_ERROR,
            505 => PASSWORD_INCORRECT,
            1001 => DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP
        ];

        $result = $this->childAdapter->getCrewMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
