<?php
namespace Sdk\User\Crew\Adapter\Crew;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Model\Guest;
use Sdk\User\Crew\Translator\CrewSessionTranslator;
use Sdk\User\Crew\Adapter\Crew\Query\CrewSessionDataCacheQuery;

class CrewSessionAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockCrewSessionAdapter();
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Translator\CrewSessionTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetSession()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Adapter\Crew\Query\CrewSessionDataCacheQuery',
            $this->adapter->getSession()
        );
    }

    public function testGetTTLNull()
    {
        $result = $this->adapter->getTTL();

        $this->assertEquals($result, 300);
    }

    public function testGetTTL()
    {
        $ttl = 300;

        Core::$container->set('cache.session.ttl', $ttl);
        $result = $this->adapter->getTTL();

        $this->assertEquals($result, $ttl);
    }

    public function testGet()
    {
        $this->adapter = $this->getMockBuilder(MockCrewSessionAdapter::class)
        ->setMethods([
            'getSession',
            'getTranslator'
         ])
        ->getMock();

        $id = 1;
        $info = array($id);
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew($id);
        
        $session = $this->prophesize(CrewSessionDataCacheQuery::class);
        $session->get(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($info);

        $this->adapter->expects($this->exactly(1))
             ->method('getSession')
             ->willReturn($session->reveal());
        
        $translator = $this->prophesize(CrewSessionTranslator::class);
        $translator->arrayToObject(Argument::exact($info))->shouldBeCalledTimes(1)->willReturn($crew);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
        
        $result = $this->adapter->get($id);

        $this->assertEquals($result, $crew);
    }

    public function testGetNull()
    {
        $this->adapter = $this->getMockBuilder(MockCrewSessionAdapter::class)
        ->setMethods([
            'getSession'
         ])
        ->getMock();

        $id = 1;
        $info = array();
        
        $session = $this->prophesize(CrewSessionDataCacheQuery::class);
        $session->get(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($info);

        $this->adapter->expects($this->exactly(1))
             ->method('getSession')
             ->willReturn($session->reveal());
        
        $result = $this->adapter->get($id);

        $this->assertEquals($result, Guest::getInstance());
    }

    public function testSave()
    {
        $adapter = $this->getMockBuilder(MockCrewSessionAdapter::class)
        ->setMethods([
            'getSession',
            'getTranslator',
            'getTTL'
         ])
        ->getMock();

        $id = 1;
        $info = array($id);
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew($id);
        $ttl = 100;
        
        $translator = $this->prophesize(CrewSessionTranslator::class);
        $translator->objectToArray(Argument::exact($crew))->shouldBeCalledTimes(1)->willReturn($info);

        $adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $adapter->expects($this->exactly(1))
            ->method('getTTL')
            ->willReturn($ttl);

        $session = $this->prophesize(CrewSessionDataCacheQuery::class);
        $session->save(
            Argument::exact($id),
            Argument::exact($info),
            Argument::exact($ttl)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $adapter->expects($this->exactly(1))
             ->method('getSession')
             ->willReturn($session->reveal());
        
        $result = $adapter->save($crew);

        $this->assertTrue($result);
    }

    public function testDel()
    {
        $adapter = $this->getMockBuilder(MockCrewSessionAdapter::class)
        ->setMethods([
            'getSession'
         ])
        ->getMock();

        $id = 1;

        $session = $this->prophesize(CrewSessionDataCacheQuery::class);
        $session->del(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn(true);

        $adapter->expects($this->exactly(1))
             ->method('getSession')
             ->willReturn($session->reveal());
        
        $result = $adapter->del($id);

        $this->assertTrue($result);
    }
}
