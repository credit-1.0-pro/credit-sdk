<?php
namespace Sdk\User\Crew\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\User\Crew\Utils\CrewSessionUtils;

class CrewSessionTranslatorTest extends TestCase
{
    use CrewSessionUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new CrewSessionTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew(1);

        $expression['id'] = $crew->getId();
        $expression['cellphone'] = $crew->getCellphone();
        $expression['userName'] = $crew->getUserName();
        $expression['realName'] = $crew->getRealName();
        $expression['identify'] = $crew->getIdentify();
        $expression['category'] = $crew->getCategory();
        $expression['purview'] = $crew->getPurview();
        $expression['userGroup'] = array(
            'id' => $crew->getUserGroup()->getId(),
            'name' => $crew->getUserGroup()->getName()
        );

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\User\Crew\Model\Crew', $objects);
    }

    public function testObjectToArray()
    {
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew(1);

        $expression = $this->translator->objectToArray($crew);

        $this->compareArrayAndObject($expression, $crew);
    }

    public function testArrayToObjects()
    {
        $translator = $this->getMockBuilder(CrewSessionTranslator::class)
                     ->setMethods(['arrayToObject'])
                     ->getMock();

        $info = array('info');
        $expression = array(
            'id' => 1,
            'info' => $info
        );

        $translator->expects($this->once())
                   ->method('arrayToObject')
                   ->willReturn($info);

        $objects = $translator->arrayToObjects($expression);

        $this->assertEquals([1=>$info], $objects);
    }
}
