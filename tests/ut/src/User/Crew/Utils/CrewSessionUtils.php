<?php
namespace Sdk\User\Crew\Utils;

trait CrewSessionUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $crew
    ) {
        $this->assertEquals($expectedArray['id'], $crew->getId());
        $this->assertEquals($expectedArray['userName'], $crew->getUserName());
        $this->assertEquals($expectedArray['cellphone'], $crew->getCellphone());
        $this->assertEquals($expectedArray['realName'], $crew->getRealName());
        $this->assertEquals($expectedArray['identify'], $crew->getIdentify());
        $this->assertEquals($expectedArray['category'], $crew->getCategory());
        $this->assertEquals($expectedArray['purview'], $crew->getPurview());
        $this->assertEquals($expectedArray['userGroup']['id'], marmot_encode($crew->getUserGroup()->getId()));
        $this->assertEquals($expectedArray['userGroup']['name'], $crew->getUserGroup()->getName());
    }
}
