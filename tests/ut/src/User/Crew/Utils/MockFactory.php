<?php
namespace Sdk\User\Crew\Utils;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Model\Guest;
use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\Department\Model\Department;

class MockFactory
{
    public static function generateCrew(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Crew {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $crew = new Crew($id);
        $crew->setId($id);

        //cellphone
        self::generateCellphone($crew, $faker, $value);
        //realName
        self::generateRealName($crew, $faker, $value);
        //cardId
        self::generateCardId($crew, $faker, $value);
        //category
        self::generateCategory($crew, $faker, $value);
        //userGroup
        self::generateUserGroup($crew, $faker, $value);
        //department
        self::generateDepartment($crew, $faker, $value);
        //password
        self::generatePassword($crew, $faker, $value);
        //identify
        self::generateIdentify($crew, $faker, $value);

        //status
        $crew->setCreateTime($faker->unixTime());
        $crew->setUpdateTime($faker->unixTime());
        $crew->setStatusTime($faker->unixTime());

        return $crew;
    }

    private static function generateCellphone($crew, $faker, $value) : void
    {
        $cellphone = isset($value['cellphone']) ?
            $value['cellphone'] :
            $faker->phoneNumber();
        
        $crew->setUserName($cellphone);
        $crew->setCellphone($cellphone);
    }

    private static function generateRealName($crew, $faker, $value) : void
    {
        $realName = isset($value['realName']) ?
            $value['realName'] :
            $faker->name();
        
        $crew->setRealName($realName);
    }

    private static function generateCardId($crew, $faker, $value) : void
    {
        $cardId = isset($value['cardId']) ? $value['cardId'] : $faker->creditCardNumber();
        $crew->setCardId($cardId);
    }

    private static function generateCategory($crew, $faker, $value) : void
    {
        $category = isset($value['category']) ? $value['category'] : $faker->randomElement(Crew::CATEGORY);
        $crew->setCategory($category);
    }

    private static function generateUserGroup($crew, $faker, $value) : void
    {
        $userGroup = isset($value['userGroup']) ?
            $value['userGroup'] :
            \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup($faker->randomDigitNotNull());
        $crew->setUserGroup($userGroup);
    }

    private static function generateDepartment($crew, $faker, $value) : void
    {
        $department = isset($value['department']) ?
            $value['department'] :
            \Sdk\UserGroup\Department\Utils\MockDepartmentFactory::generateDepartment($faker->randomDigitNotNull());
        $crew->setDepartment($department);
    }

    private static function generatePassword($crew, $faker, $value) : void
    {
        $password = isset($value['password']) ?
            $value['password'] :
            md5($faker->randomNumber());
        $crew->setPassword($password);
    }

    private static function generateIdentify($crew, $faker, $value) : void
    {
        unset($faker);
        $guest = Guest::getInstance();

        $identify = isset($value['identify']) ? $value['identify'] : $guest->generateIdentify();
        $crew->setIdentify($identify);
    }
}
