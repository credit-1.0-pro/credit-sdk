<?php
namespace Sdk\User\Crew\Utils;

trait CrewRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $crew
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $crew->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['category'], $crew->getCategory());
        $this->assertEquals($expectedArray['data']['attributes']['purview'], $crew->getPurview());
        
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['userGroup']['data'])) {
                $this->assertEquals(
                    $relationships['userGroup']['data'][0]['type'],
                    'userGroups'
                );
                $this->assertEquals(
                    $relationships['userGroup']['data'][0]['id'],
                    $crew->getUserGroup()->getId()
                );
            }
            if (isset($relationships['department']['data'])) {
                $this->assertEquals(
                    $relationships['department']['data'][0]['type'],
                    'departments'
                );
                $this->assertEquals(
                    $relationships['department']['data'][0]['id'],
                    $crew->getDepartment()->getId()
                );
            }
        }
    }
}
