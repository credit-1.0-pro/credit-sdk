<?php
namespace Sdk\User\Crew\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\Department\Model\Department;
use Sdk\User\Crew\Adapter\Crew\ICrewAdapter;
use Sdk\User\Crew\Repository\CrewRepository;
use Sdk\User\Crew\Repository\CrewSessionRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class CrewTest extends TestCase
{
    private $crew;

    public function setUp()
    {
        $this->crew = new MockCrew();
    }

    public function tearDown()
    {
        unset($this->crew);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf('Sdk\UserGroup\UserGroup\Model\UserGroup', $this->crew->getUserGroup());
        $this->assertEquals(array(), $this->crew->getPurview());
        $this->assertEquals(Crew::CATEGORY['NULL'], $this->crew->getCategory());
        $this->assertInstanceOf('Sdk\UserGroup\Department\Model\Department', $this->crew->getDepartment());
        $this->assertInstanceOf(
            'Sdk\User\Crew\Repository\CrewRepository',
            $this->crew->getRepository()
        );
        $this->assertInstanceOf(
            'Sdk\User\Crew\Repository\CrewSessionRepository',
            $this->crew->getCrewSessionRepository()
        );
    }

    //UserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 Crew setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->crew->setUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->crew->getUserGroup());
    }

    /**
     * 设置 Crew setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->crew->setUserGroup('userGroup');
    }
    //UserGroup 测试 --------------------------------------------------------   end

    //purview 测试 -------------------------------------------------------- start
    /**
     * 设置 Crew setPurview() 正确的传参类型,期望传值正确
     */
    public function testSetPurviewCorrectType()
    {
        $this->crew->setPurview(['purview']);
        $this->assertEquals(['purview'], $this->crew->getPurview());
    }

    /**
     * 设置 Crew setPurview() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPurviewWrongType()
    {
        $this->crew->setPurview('purview');
    }
    //purview 测试 --------------------------------------------------------   end

    //category 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Crew setCategory() 是否符合预定范围
     *
     * @dataProvider categoryProvider
     */
    public function testSetCategory($actual, $expected)
    {
        $this->crew->setCategory($actual);
        $this->assertEquals($expected, $this->crew->getCategory());
    }

    /**
     * 循环测试 Crew setCategory() 数据构建器
     */
    public function categoryProvider()
    {
        return array(
            array(Crew::CATEGORY['SUPERTUBE'], Crew::CATEGORY['SUPERTUBE']),
            array(Crew::CATEGORY['PLATFORM_ADMINISTRATORS'], Crew::CATEGORY['PLATFORM_ADMINISTRATORS']),
            array(Crew::CATEGORY['USERGROUP_ADMINISTRATORS'], Crew::CATEGORY['USERGROUP_ADMINISTRATORS']),
            array(Crew::CATEGORY['USERGROUP_OPERATION'], Crew::CATEGORY['USERGROUP_OPERATION']),
            array(999, Crew::CATEGORY['NULL']),
        );
    }

    /**
     * 设置 Crew setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->crew->setCategory('string');
    }
    //category 测试 ------------------------------------------------------   end

    //Department 测试 -------------------------------------------------------- start
    /**
     * 设置 Crew setDepartment() 正确的传参类型,期望传值正确
     */
    public function testSetDepartmentCorrectType()
    {
        $expectedDepartment = new Department();

        $this->crew->setDepartment($expectedDepartment);
        $this->assertEquals($expectedDepartment, $this->crew->getDepartment());
    }

    /**
     * 设置 Crew setDepartment() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDepartmentWrongType()
    {
        $this->crew->setDepartment('department');
    }
    //Department 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Repository\CrewRepository',
            $this->crew->getRepository()
        );
    }

    public function testGetCrewSessionRepository()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Repository\CrewSessionRepository',
            $this->crew->getCrewSessionRepository()
        );
    }
    
    public function testClearSessionSuccess()
    {
        $crew = $this->getMockBuilder(MockCrew::class)
                     ->setMethods(['getCrewSessionRepository'])
                     ->getMock();

        $repository = $this->prophesize(CrewSessionRepository::class);
        $repository->clear(Argument::exact($crew->getId()))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $crew->expects($this->once())
             ->method('getCrewSessionRepository')
             ->willReturn($repository->reveal());

        $result = $crew->clearSession();
        $this->assertTrue($result);
    }

    public function testClearSessionFail()
    {
        $crew = $this->getMockBuilder(MockCrew::class)
                     ->setMethods(['getCrewSessionRepository'])
                     ->getMock();

        $repository = $this->prophesize(CrewSessionRepository::class);
        $repository->clear(Argument::exact($crew->getId()))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(false);

        $crew->expects($this->once())
             ->method('getCrewSessionRepository')
             ->willReturn($repository->reveal());

        $result = $crew->clearSession();
        $this->assertFalse($result);
    }

    public function testSignOut()
    {
        $crew = $this->getMockBuilder(MockCrew::class)
                     ->setMethods(['clearSession'])
                     ->getMock();

        $crew->expects($this->once())
                   ->method('clearSession')
                   ->willReturn(true);

        $result = $crew->signOut();
        $this->assertTrue($result);
    }
    
    public function testValidateIdentifyFailure()
    {
        $this->crew->setIdentify('identify');
    
        $result = $this->crew->validateIdentify('1');
        $this->assertFalse($result);
        $this->assertEquals('', Core::$container->get('jwt'));
    }

    public function testValidateIdentifySuccess()
    {
        $this->crew->setIdentify('identify');

        $result = $this->crew->validateIdentify('identify');
        $this->assertTrue($result);
    }

    public function testSaveSessionSuccess()
    {
        $crew = $this->getMockBuilder(MockCrew::class)
                     ->setMethods(['getCrewSessionRepository'])
                     ->getMock();
        
        $id = 1;
        $crew->setId($id);

        $crewSessionRepository = $this->prophesize(CrewSessionRepository::class);
        $crewSessionRepository->save(Argument::exact($crew))->shouldBeCalledTimes(1)->willReturn(true);
        $crewSessionRepository->get(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($crew);

        $crew->expects($this->exactly(2))
            ->method('getCrewSessionRepository')
            ->willReturn($crewSessionRepository->reveal());

        $result = $crew->saveSession();
        $this->assertTrue($result);
        $this->assertEquals($crew, Core::$container->get('crewSession'));
    }

    public function testSaveSessionFail()
    {
        $crew = $this->getMockBuilder(MockCrew::class)
                     ->setMethods(['getCrewSessionRepository'])
                     ->getMock();

        $crewSessionRepository = $this->prophesize(CrewSessionRepository::class);
        $crewSessionRepository->save(Argument::exact($crew))->shouldBeCalledTimes(1)->willReturn(false);

        $crew->expects($this->exactly(1))
            ->method('getCrewSessionRepository')
            ->willReturn($crewSessionRepository->reveal());

        $result = $crew->saveSession();
        $this->assertFalse($result);
    }

    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 CrewRepository 调用 add 并且传参 $crew 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $crew = $this->getMockBuilder(MockCrew::class)
                           ->setMethods(['getRepository', 'isCellphoneExist'])
                           ->getMock();

        $crew->expects($this->once())
                   ->method('isCellphoneExist')
                   ->willReturn(true);

        //预言 CrewRepository
        $repository = $this->prophesize(CrewRepository::class);
        $repository->add(Argument::exact($crew))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $crew->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $crew->add();
        $this->assertTrue($result);
    }

    /**
     * 测试添加失败-手机号重复
     * 1. 期望返回false
     * 2. 期望isCellphoneExist被调用一次,并返回false
     * 3. 调用add,期望返回false
     */
    public function testAddFailNameExist()
    {
        $crew = $this->getMockBuilder(MockCrew::class)
                           ->setMethods(['isCellphoneExist'])
                           ->getMock();

        $crew->expects($this->once())
                   ->method('isCellphoneExist')
                   ->willReturn(false);

        $result = $crew->add();

        $this->assertFalse($result);
    }

    private function initialIsCellphoneExist($result)
    {
        $this->crew = $this->getMockBuilder(MockCrew::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $id = 1;
        
        $sort = ['-updateTime'];
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew($id);

        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($crew);
        }

        $this->crew->setCellphone($crew->getCellphone());

        $filter['cellphone'] = $crew->getCellphone();

        $repository = $this->prophesize(CrewRepository::class);

        $repository->scenario(Argument::exact(CrewRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort)
        )->shouldBeCalledTimes(1)->willReturn([$count, $list]);

        $this->crew->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());
    }

    public function testIsCellphoneExistFailure()
    {
        $this->initialIsCellphoneExist(false);

        $result = $this->crew->isCellphoneExist();

        $this->assertFalse($result);
        $this->assertEquals(CELLPHONE_EXIST, Core::getLastError()->getId());
        $this->assertEquals('cellphone', Core::getLastError()->getSource()['pointer']);
    }

    public function testIsCellphoneExistSuccess()
    {
        $this->initialIsCellphoneExist(true);
        
        $result = $this->crew->isCellphoneExist();

        $this->assertTrue($result);
    }
}
