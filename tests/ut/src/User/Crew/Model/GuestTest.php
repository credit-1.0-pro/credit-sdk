<?php
namespace Sdk\User\Crew\Model;

use Marmot\Core;
use Marmot\Framework\Classes\Server;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Firebase\JWT\JWT;

use Sdk\User\Crew\Repository\CrewRepository;
use Sdk\User\Crew\Repository\CrewSessionRepository;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class GuestTest extends TestCase
{
    private $guest;

    public function setUp()
    {
        $this->guest = Guest::getInstance();
    }

    public function tearDown()
    {
        unset($this->guest);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsCrew()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Model\Crew',
            $this->guest
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->guest
        );
    }

    public function testResourceNotExist()
    {
        $guest = new MockGuest();

        $result = $guest->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testGenerateIdentify()
    {
        $guest = new MockGuest();

        $identify = md5(serialize(Server::get('marmot')).Core::$container->get('time'));

        $result = $guest->generateIdentify();
        $this->assertEquals($identify, $result);
    }

    public function testSignInFail()
    {
        $guest = $this->getMockBuilder(MockGuest::class)
                     ->setMethods(['getRepository'])
                     ->getMock();

        $repository = $this->prophesize(CrewRepository::class);
        $repository->signIn(Argument::exact($guest))->shouldBeCalledTimes(1)->willReturn(false);

        $guest->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());
                     
        $result = $guest->signIn();
        $this->assertFalse($result);
    }

    public function testSignInSuccess()
    {
        $guest = $this->getMockBuilder(MockGuest::class)
                     ->setMethods(['getRepository', 'saveIdentify', 'saveSession', 'registerGlobalCrew'])
                     ->getMock();

        $repository = $this->prophesize(CrewRepository::class);
        $repository->signIn(Argument::exact($guest))->shouldBeCalledTimes(1)->willReturn(true);

        $guest->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());
                     
        $guest->expects($this->once())->method('saveIdentify')->willReturn(true);
        $guest->expects($this->once())->method('saveSession')->willReturn(true);
        $guest->expects($this->once())->method('registerGlobalCrew')->willReturn(true);

        $result = $guest->signIn();
        $this->assertTrue($result);
    }

    public function testRegisterGlobalCrew()
    {
        $guest = new MockGuest();

        $result = $guest->registerGlobalCrew();
        $this->assertTrue($result);
        $this->assertEquals($guest, Core::$container->get('crew'));
    }
        
    public function testValidateIdentifyFailure()
    {
        $this->guest->setIdentify('identify');
    
        $result = $this->guest->validateIdentify('1');
        $this->assertFalse($result);
        $this->assertEquals('', Core::$container->get('jwt'));
    }

    public function testValidateIdentifySuccess()
    {
        $this->guest->setIdentify('identify');

        $result = $this->guest->validateIdentify('identify');
        $this->assertTrue($result);
    }

    public function testSaveSessionSuccess()
    {
        $guest = $this->getMockBuilder(MockGuest::class)
                     ->setMethods(['getCrewSessionRepository'])
                     ->getMock();
        
        $id = 1;
        $guest->setId($id);

        $crewSessionRepository = $this->prophesize(CrewSessionRepository::class);
        $crewSessionRepository->save(Argument::exact($guest))->shouldBeCalledTimes(1)->willReturn(true);
        $crewSessionRepository->get(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($guest);

        $guest->expects($this->exactly(2))
            ->method('getCrewSessionRepository')
            ->willReturn($crewSessionRepository->reveal());

        $result = $guest->saveSession();
        $this->assertTrue($result);
        $this->assertEquals($guest, Core::$container->get('crewSession'));
    }

    public function testSaveSessionFail()
    {
        $guest = $this->getMockBuilder(MockGuest::class)
                     ->setMethods(['getCrewSessionRepository'])
                     ->getMock();

        $crewSessionRepository = $this->prophesize(CrewSessionRepository::class);
        $crewSessionRepository->save(Argument::exact($guest))->shouldBeCalledTimes(1)->willReturn(false);

        $guest->expects($this->exactly(1))
            ->method('getCrewSessionRepository')
            ->willReturn($crewSessionRepository->reveal());

        $result = $guest->saveSession();
        $this->assertFalse($result);
    }

    public function testSaveIdentify()
    {
        $guest = $this->getMockBuilder(MockGuest::class)
                     ->setMethods(['generateIdentify'])
                     ->getMock();
        $id = 1;
        $guest->setId($id);
        $identify = 'identify';

        $guest->expects($this->once())->method('generateIdentify')->willReturn(true)->willReturn($identify);

        $nowTime = Core::$container->get('time');

        Core::$container->set('jwt.iss', 1);
        Core::$container->set('jwt.aud', 1);
        Core::$container->set('jwt.nbf', 1);
        Core::$container->set('jwt.exp', 1);
        Core::$container->set('jwt.key', 1);

        $token = [
            'iss' => Core::$container->get('jwt.iss'), //签发者
            'aud' => Core::$container->get('jwt.aud'), //jwt所面向的用户
            'iat' => $nowTime, //签发时间
            'nbf' => $nowTime + Core::$container->get('jwt.nbf'), //在什么时间之后该jwt才可用
            'exp' => $nowTime + Core::$container->get('jwt.exp'), //过期时间-10min
            'data' => [
                'crewId' => $id,
                'identify' => $identify
            ]
        ];

        $jwt = JWT::encode($token, Core::$container->get('jwt.key'));
       
        $res['jwt'] = $jwt;

        $result = $guest->saveIdentify();
        $this->assertTrue($result);
        $this->assertEquals($res, Core::$container->get('jwt'));
    }
}
