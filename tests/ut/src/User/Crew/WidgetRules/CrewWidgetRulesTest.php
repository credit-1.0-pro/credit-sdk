<?php
namespace Sdk\User\Crew\WidgetRules;

use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Sdk\Common\Utils\StringGenerate;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Repository\CrewRepository;

class CrewWidgetRulesTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = CrewWidgetRules::getInstance();

        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //category -- start
    /**
     * @dataProvider invalidCategoryProvider
     */
    public function testCategoryInvalid($actual, $expected)
    {
        $result = $this->widgetRule->category($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(CREW_CATEGORY_NOT_EXIST, Core::getLastError()->getId());
    }

    public function invalidCategoryProvider()
    {
        return array(
            array(Crew::CATEGORY['SUPERTUBE'], true),
            array(Crew::CATEGORY['PLATFORM_ADMINISTRATORS'], true),
            array(Crew::CATEGORY['USERGROUP_ADMINISTRATORS'], true),
            array(Crew::CATEGORY['USERGROUP_OPERATION'], true),
            array(999, false),
        );
    }
    //category -- end

    //purview -- start
    /**
     * @dataProvider invalidPurviewProvider
     */
    public function testPurviewInvalid($actual, $expected)
    {
        $result = $this->widgetRule->purview($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CREW_PURVIEW_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidPurviewProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed(0);

        return array(
            array('', false),
            array(0, false),
            array(array('purview'), false),
            array(array($faker->randomDigitNot(0)), true)
        );
    }
    //purview -- end

    public function testPurviewHierarchySuccess()
    {
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew(1);
        $crew->setCategory(Crew::CATEGORY['SUPERTUBE']);
        Core::$container->set('crew', $crew);

        $category = Crew::CATEGORY['PLATFORM_ADMINISTRATORS'];

        $result = $this->widgetRule->purviewHierarchy($category);
        $this->assertTrue($result);
    }

    public function testPurviewHierarchyFail()
    {
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew(1);
        $crew->setCategory(Crew::CATEGORY['USERGROUP_ADMINISTRATORS']);
        Core::$container->set('crew', $crew);

        $category = Crew::CATEGORY['PLATFORM_ADMINISTRATORS'];

        $result = $this->widgetRule->purviewHierarchy($category);

        $this->assertFalse($result);
        $this->assertEquals(CREW_PURVIEW_HIERARCHY_FORMAT_ERROR, Core::getLastError()->getId());
    }
}
