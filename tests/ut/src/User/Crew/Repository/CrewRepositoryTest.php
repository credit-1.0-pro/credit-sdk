<?php
namespace Sdk\User\Crew\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Adapter\Crew\CrewRestfulAdapter;

class CrewRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(CrewRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsICrewAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Adapter\Crew\ICrewAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Adapter\Crew\CrewRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Adapter\Crew\CrewMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(CrewRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testSignIn()
    {
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew(1);

        $adapter = $this->prophesize(CrewRestfulAdapter::class);
        $adapter->signIn(Argument::exact($crew))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->signIn($crew);
    }
}
