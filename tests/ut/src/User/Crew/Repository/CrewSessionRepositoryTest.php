<?php
namespace Sdk\User\Crew\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Adapter\Crew\CrewSessionAdapter;

class CrewSessionRepositoryTest extends TestCase
{
    private $repository;
    private $childRepository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(CrewSessionRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
        $this->childRepository = new class extends CrewSessionRepository
        {
            public function getAdapter() : CrewSessionAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->childRepository);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Adapter\Crew\CrewSessionAdapter',
            $this->childRepository->getAdapter()
        );
    }

    public function testSave()
    {
        $crew = \Sdk\User\Crew\Utils\MockFactory::generateCrew(1);

        $adapter = $this->prophesize(CrewSessionAdapter::class);
        $adapter->save(Argument::exact($crew))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->save($crew);
    }

    public function testGet()
    {
        $id = 1;

        $adapter = $this->prophesize(CrewSessionAdapter::class);
        $adapter->get(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->get($id);
    }

    public function testClear()
    {
        $id = 1;

        $adapter = $this->prophesize(CrewSessionAdapter::class);
        $adapter->del(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->clear($id);
    }
}
