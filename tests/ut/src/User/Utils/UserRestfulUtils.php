<?php
namespace Sdk\User\Utils;

trait UserRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $user
    ) {
        $this->assertEquals($expectedArray['data']['id'], $user->getId());
        $this->assertEquals($expectedArray['data']['attributes']['cellphone'], $user->getCellphone());
        $this->assertEquals($expectedArray['data']['attributes']['realName'], $user->getRealName());
        $this->assertEquals($expectedArray['data']['attributes']['userName'], $user->getUserName());
        $this->assertEquals($expectedArray['data']['attributes']['password'], $user->getPassword());
        $this->assertEquals($expectedArray['data']['attributes']['oldPassword'], $user->getOldPassword());
        $this->assertEquals($expectedArray['data']['attributes']['avatar'], $user->getAvatar());
        $this->assertEquals($expectedArray['data']['attributes']['cardId'], $user->getCardId());
        $this->assertEquals($expectedArray['data']['attributes']['gender'], $user->getGender());
    }
}
