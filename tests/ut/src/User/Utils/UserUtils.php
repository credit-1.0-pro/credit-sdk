<?php
namespace Sdk\User\Utils;

use Sdk\Common\Model\IEnableAble;

trait UserUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $user
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($user->getId()));
        $this->assertEquals($expectedArray['cellphone'], $user->getCellphone());
        $this->assertEquals($expectedArray['userName'], $user->getUserName());
        $this->assertEquals($expectedArray['realName'], $user->getRealName());
        $this->assertEquals($expectedArray['cardId'], $user->getCardId());
        $this->assertEquals($expectedArray['avatar'], $user->getAvatar());

        $this->assertEquals($expectedArray['createTime'], $user->getCreateTime());
        $this->assertEquals($expectedArray['updateTime'], $user->getUpdateTime());
        $this->assertEquals($expectedArray['updateTimeFormat'], date('Y年m月d日 H点i分', $user->getUpdateTime()));
        $this->assertEquals($expectedArray['status']['id'], marmot_encode($user->getStatus()));
        $this->assertEquals($expectedArray['status']['name'], IEnableAble::STATUS_CN[$user->getStatus()]);
        $this->assertEquals($expectedArray['status']['type'], IEnableAble::STATUS_TAG_TYPE[$user->getStatus()]);
        $this->assertEquals($expectedArray['statusTime'], $user->getStatusTime());
    }
}
