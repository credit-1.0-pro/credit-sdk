<?php
namespace Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Sdk\Common\Model\IOperateAble;

class OperateAbleMockAdapterTraitTest extends TestCase
{
    public function testAdd()
    {
        $object = $this->getMockBuilder(IOperateAble::class)->getMock();
        $trait = $this->getMockForTrait(OperateAbleMockAdapterTrait::class);

        $result = $trait->add($object);
        $this->assertTrue($result);
    }

    public function testEdit()
    {
        $object = $this->getMockBuilder(IOperateAble::class)->getMock();
        $trait = $this->getMockForTrait(OperateAbleMockAdapterTrait::class);

        $result = $trait->edit($object);
        $this->assertTrue($result);
    }
}
