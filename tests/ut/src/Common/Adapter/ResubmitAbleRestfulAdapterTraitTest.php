<?php
namespace Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Sdk\Common\Model\IResubmitAble;

class ResubmitAbleRestfulAdapterTraitTest extends TestCase
{
    public function testResubmit()
    {
        $object = $this->getMockBuilder(IResubmitAble::class)->getMock();
        $trait = $this->getMockForTrait(ResubmitAbleRestfulAdapterTrait::class);

        $trait->expects($this->any())
             ->method('resubmitAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->resubmit($object));
    }
}
