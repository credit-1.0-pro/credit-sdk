<?php
namespace Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\MockApplyAbleObject;
use Sdk\Common\Translator\MockRestfulTranslator;

class ApplyAbleRestfulAdapterTraitTest extends TestCase
{
    public function testApprove()
    {
        $object = $this->getMockBuilder(IApplyAble::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockApplyAbleRestfulAdapterObject::class)
                      ->setMethods(['approveAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('approveAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->approve($object));
    }

    public function testApproveActionPublic()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockApplyAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockApplyAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(true);

        $trait->expects($this->exactly(1))
             ->method('translateToObject')
             ->willReturn(true);
             
        $this->assertTrue($trait->approveActionPublic($object));
    }

    public function testApproveActionPublicFail()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockApplyAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockApplyAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
             
        $this->assertFalse($trait->approveActionPublic($object));
    }

    public function testReject()
    {
        $object = $this->getMockBuilder(IApplyAble::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockApplyAbleRestfulAdapterObject::class)
                      ->setMethods(['rejectAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('rejectAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->reject($object));
    }

    public function testRejectActionPublic()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockApplyAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockApplyAbleRestfulAdapterObject::class)
                      ->setMethods(['getTranslator', 'getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $data = array();
        $translator = $this->prophesize(MockRestfulTranslator::class);
        $translator->objectToArray($object, array('rejectReason'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $trait->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(true);

        $trait->expects($this->exactly(1))
             ->method('translateToObject')
             ->willReturn(true);
            
        $this->assertTrue($trait->rejectActionPublic($object));
    }

    public function testRejectActionPublicFail()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockApplyAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockApplyAbleRestfulAdapterObject::class)
                      ->setMethods(['getTranslator', 'getResource', 'patch', 'isSuccess'])
                      ->getMock();

        $data = array();
        $translator = $this->prophesize(MockRestfulTranslator::class);
        $translator->objectToArray($object, array('rejectReason'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $trait->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
             
        $this->assertFalse($trait->rejectActionPublic($object));
    }
}
