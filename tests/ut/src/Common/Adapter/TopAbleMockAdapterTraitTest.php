<?php
namespace Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Sdk\Common\Model\MockTopAbleObject;

class TopAbleMockAdapterTraitTest extends TestCase
{
    use TopAbleMockAdapterTrait;

    public function testTop()
    {
        $object = new MockTopAbleObject();

        $result = $this->top($object);
        $this->assertTrue($result);
    }

    public function testCancelTop()
    {
        $object = new MockTopAbleObject();

        $result = $this->cancelTop($object);
        $this->assertTrue($result);
    }
}
