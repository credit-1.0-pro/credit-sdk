<?php
namespace Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

class CommonMapErrorsTraitTest extends TestCase
{
    use CommonMapErrorsTrait;

    public function testCommonMapErrors()
    {
        $data = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->assertEquals($data, $this->commonMapErrors());
    }
}
