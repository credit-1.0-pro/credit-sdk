<?php
namespace Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\INull;

class ResourcesFetchAbleAdapterTraitTest extends TestCase
{
    public function testFetchOne()
    {
        $id = 1;
        $resources = 'resources';
        $object = $this->getMockBuilder(INull::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockResourcesFetchAbleAdapterObject::class)
                      ->setMethods(['fetchOneAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('fetchOneAction')
             ->with($this->equalTo($id), $this->equalTo($resources), $this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->fetchOne($id, $resources, $object));
    }

    public function testFetchOneActionPublic()
    {
        $id = 1;
        $resources = 'resources';
        $object = $this->getMockBuilder(INull::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockResourcesFetchAbleAdapterObject::class)
                      ->setMethods(['getResource', 'get', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(true);

        $trait->expects($this->exactly(1))
             ->method('translateToObject')
             ->willReturn(true);
             
        $this->assertTrue($trait->fetchOneActionPublic($id, $resources, $object));
    }

    public function testFetchOneActionPublicFail()
    {
        $id = 1;
        $resources = 'resources';
        $object = $this->getMockBuilder(INull::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockResourcesFetchAbleAdapterObject::class)
                      ->setMethods(['getResource', 'get', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
        
        $result = $trait->fetchOneActionPublic($id, $resources, $object);
        $this->assertEquals($object, $result);
    }

    public function testSearch()
    {
        $resources = 'resources';
        $filter = array();
        $number = 0;
        $size = 20;

        $trait = $this->getMockBuilder(MockResourcesFetchAbleAdapterObject::class)
                      ->setMethods(['searchAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('searchAction')
             ->with(
                 $this->equalTo($resources),
                 $this->equalTo($filter),
                 $this->equalTo($number),
                 $this->equalTo($size)
             )->willReturn(array());
             
        $this->assertArraySubset(array(), $trait->search($resources, $filter, $number, $size));
    }

    public function testSearchActionPublic()
    {
        $resources = 'resources';
        $filter = array();
        $number = 0;
        $size = 20;

        $trait = $this->getMockBuilder(MockResourcesFetchAbleAdapterObject::class)
                      ->setMethods(['getResource', 'get', 'isSuccess', 'translateToObjects'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(true);

        $trait->expects($this->exactly(1))
             ->method('translateToObjects')
             ->willReturn(array());
        
        $result = $trait->searchActionPublic($resources, $filter, $number, $size);
        $this->assertEquals(array(), $result);
    }

    public function testSearchActionPublicFail()
    {
        $resources = 'resources';
        $filter = array();
        $number = 0;
        $size = 20;

        $trait = $this->getMockBuilder(MockResourcesFetchAbleAdapterObject::class)
                      ->setMethods(['getResource', 'get', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
        
        $result = $trait->searchActionPublic($resources, $filter, $number, $size);
        $this->assertEquals(array(0, array()), $result);
    }
}
