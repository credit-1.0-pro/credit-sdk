<?php
namespace Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\MockEnableAbleObject;

class EnableAbleRestfulAdapterTraitTest extends TestCase
{
    public function testEnable()
    {
        $object = $this->getMockBuilder(IEnableAble::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockEnableAbleRestfulAdapterObject::class)
                      ->setMethods(['enableAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('enableAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->enable($object));
    }

    public function testEnableActionPublic()
    {
        $this->initActionPublic(true, 'enableActionPublic');
    }

    public function testEnableActionPublicFail()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockEnableAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockEnableAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
             
        $this->assertFalse($trait->enableActionPublic($object));
    }

    public function testDisable()
    {
        $object = $this->getMockBuilder(IEnableAble::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockEnableAbleRestfulAdapterObject::class)
                      ->setMethods(['disableAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('disableAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->disable($object));
    }

    public function testDisableActionPublic()
    {
        $this->initActionPublic(true, 'disableActionPublic');
    }

    public function testDisableActionPublicFail()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockEnableAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockEnableAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
             
        $this->assertFalse($trait->disableActionPublic($object));
    }

    protected function initActionPublic(bool $result, string $method)
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockEnableAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockEnableAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn($result);

        $trait->expects($this->exactly(1))
             ->method('translateToObject')
             ->willReturn($result);
             
        $this->assertTrue($trait->$method($object));
    }
}
