<?php
namespace Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Sdk\Common\Model\MockApplyAbleObject;

class ApplyAbleMockAdapterTraitTest extends TestCase
{
    use ApplyAbleMockAdapterTrait;

    public function testApprove()
    {
        $object = new MockApplyAbleObject();

        $result = $this->approve($object);
        $this->assertTrue($result);
    }

    public function testReject()
    {
        $object = new MockApplyAbleObject();

        $result = $this->reject($object);
        $this->assertTrue($result);
    }
}
