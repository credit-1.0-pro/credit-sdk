<?php
namespace Sdk\Common\CommandHandler;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\Command\CancelTopCommand;

use PHPUnit\Framework\TestCase;

class CancelTopCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CancelTopCommandHandler::class)
                    ->setMethods(['fetchITopObject'])
                    ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->stub
        );
    }

    public function testExecute()
    {
        $id =1;

        $command = new class($id) extends CancelTopCommand
        {
        };

        $cancelTopAble = $this->prophesize(ITopAble::class);
        $cancelTopAble->cancelTop()->shouldBeCalledTimes(1)->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('fetchITopObject')
            ->with($id)
            ->willReturn($cancelTopAble->reveal());

        $result = $this->stub->execute($command);
        $this->assertTrue($result);
    }
}
