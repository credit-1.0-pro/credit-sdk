<?php
namespace Sdk\Common\CommandHandler;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\Command\TopCommand;

use PHPUnit\Framework\TestCase;

class TopCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(TopCommandHandler::class)
                    ->setMethods(['fetchITopObject'])
                    ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->stub
        );
    }

    public function testExecute()
    {
        $id =1;

        $command = new class($id) extends TopCommand
        {

        };

        $topAble = $this->prophesize(ITopAble::class);
        $topAble->top()->shouldBeCalledTimes(1)->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('fetchITopObject')
            ->with($id)
            ->willReturn($topAble->reveal());

        $result = $this->stub->execute($command);
        $this->assertTrue($result);
    }
}
