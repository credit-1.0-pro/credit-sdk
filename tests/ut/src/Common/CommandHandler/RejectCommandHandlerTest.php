<?php
namespace Sdk\Common\CommandHandler;

use Sdk\Common\Command\RejectCommand;
use Sdk\Common\Model\MockApplyAbleObject;

use PHPUnit\Framework\TestCase;

class RejectCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(RejectCommandHandler::class)
            ->setMethods(['fetchIApplyObject'])
            ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->stub
        );
    }

    public function testExecute()
    {
        $rejectReason = 'rejectReason';
        $id = 1;

        $command = new class($rejectReason, $id) extends RejectCommand
        {
        };

        $rejectAble = $this->prophesize(MockApplyAbleObject::class);
        $rejectAble->setRejectReason($rejectReason)->shouldBeCalledTimes(1);
        $rejectAble->reject()->shouldBeCalledTimes(1)->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('fetchIApplyObject')
            ->with($id)
            ->willReturn($rejectAble->reveal());

        $result = $this->stub->execute($command);
        $this->assertTrue($result);
    }
}
