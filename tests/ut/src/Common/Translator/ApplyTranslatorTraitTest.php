<?php
namespace Sdk\Common\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Common\Utils\ApplyUtils;

class ApplyTranslatorTraitTest extends TestCase
{
    use ApplyUtils;

    private $trait;

    public function setUp()
    {
        $this->trait = new MockApplyTranslatorTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testApplyObjectToArray()
    {
        $apply = \Sdk\Common\Utils\MockApplyFactory::generateApply(1);

        $expression = $this->trait->applyObjectToArray($apply);
    
        $this->compareArrayAndObject($expression, $apply);
    }

    public function testApplyObjectToArrayFail()
    {
        $apply = null;

        $expression = $this->trait->applyObjectToArray($apply);
        $this->assertEquals(array(), $expression);
    }
}
