<?php
namespace Sdk\Common\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Common\Utils\ApplyRestfulUtils;
use Sdk\Common\Model\MockApplyAbleObject;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

class ApplyRestfulTranslatorTraitTest extends TestCase
{
    use ApplyRestfulUtils;

    private $trait;

    public function setUp()
    {
        $this->trait = new MockApplyRestfulTranslatorTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->trait->publicGetUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\User\Crew\Translator\CrewRestfulTranslator',
            $this->trait->publicGetCrewRestfulTranslator()
        );
    }

    public function testApplyTranslateToObject()
    {
        $apply = \Sdk\Common\Utils\MockApplyFactory::generateApply(1);

        $expression['data']['attributes']['relationId'] = $apply->getRelationId();
        $expression['data']['attributes']['applyStatus'] = $apply->getApplyStatus();
        $expression['data']['attributes']['rejectReason'] = $apply->getRejectReason();
        $expression['data']['attributes']['operationType'] = $apply->getOperationType();
        $expression['data']['attributes']['applyInfoType'] = $apply->getApplyInfoType();

        $applyObject = $this->trait->applyTranslateToObject($expression, new MockApplyAbleObject());
        $this->compareArrayAndObjectCommon($expression, $applyObject);
    }

    public function testObjectToArray()
    {
        $apply = \Sdk\Common\Utils\MockApplyFactory::generateApply(1);

        $expression = $this->trait->applyObjectToArray($apply);
     
        $this->compareArrayAndObjectCommon($expression, $apply);
    }

    public function testObjectToArrayFail()
    {
        $apply = null;

        $expression = $this->trait->applyObjectToArray($apply);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        $translator = $this->getMockBuilder(MockApplyRestfulTranslatorTrait::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getCrewRestfulTranslator',
                        'getUserGroupRestfulTranslator'
                    ])
                    ->getMock();

        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'applyCrew'=>['data'=>'mockApplyCrew'],
            'applyUserGroup'=>['data'=>'mockApplyUserGroup'],
            'publishCrew'=>['data'=>'mockPublishCrew']
        ];

        $publishCrewInfo = ['mockPublishCrewInfo'];
        $applyCrewInfo = ['mockApplyCrewInfo'];
        $applyUserGroupInfo = ['mockApplyUserGroupInfo'];

        $applyCrew = new Crew();
        $publishCrew = new Crew();
        $applyUserGroup = new UserGroup();

        //预言
        $translator->expects($this->exactly(1))
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(3))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['applyCrew']['data']],
                 [$relationships['applyUserGroup']['data']],
                 [$relationships['publishCrew']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $applyCrewInfo,
                $applyUserGroupInfo,
                $publishCrewInfo
            ));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);

        $userGroupRestfulTranslator->arrayToObject(Argument::exact($applyUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyUserGroup);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);

        $crewRestfulTranslator->arrayToObject(Argument::exact($applyCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyCrew);

        $crewRestfulTranslator->arrayToObject(Argument::exact($publishCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishCrew);

        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(2))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
       
        //揭示
        $apply = $translator->applyTranslateToObject($expression, new MockApplyAbleObject());
         
        $this->assertEquals($publishCrew, $apply->getPublishCrew());
        $this->assertEquals($applyCrew, $apply->getApplyCrew());
        $this->assertEquals($applyUserGroup, $apply->getApplyUserGroup());
    }
}
