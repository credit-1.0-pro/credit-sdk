<?php
namespace Sdk\Common\Model;

use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    private $category;

    public function setUp()
    {
        $this->category = new MockCategory(1, 'name');
    }

    /**
     * 1. 测试初始化赋值
     * 2. 测试初始化赋值是否正确
     */
    public function testConstructWithId()
    {
        $id = 2;
        $name = 'name';

        $this->category = new MockCategory($id, $name);
        $this->assertEquals($name, $this->category->getName());
        $this->assertEquals($id, $this->category->getId());
    }
}
