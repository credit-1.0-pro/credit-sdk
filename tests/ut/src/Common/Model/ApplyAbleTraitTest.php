<?php
namespace Sdk\Common\Model;

use PHPUnit\Framework\TestCase;

use Sdk\User\Crew\Model\Crew;
use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\Common\Adapter\MockApplyAbleAdapter;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class ApplyAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockApplyAbleObject::class)
                            ->setMethods(['getIApplyAbleAdapter'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }
    
    /**
     * @dataProvider applyStatusDataProvider
     */
    public function testSetApplyStatus($actual, $expected)
    {
        $this->trait->setApplyStatus($actual);
        
        $result = $this->trait->getApplyStatus();
        $this->assertEquals($expected, $result);
    }

    public function applyStatusDataProvider()
    {
        return [
            [IApplyAble::APPLY_STATUS['PENDING'], IApplyAble::APPLY_STATUS['PENDING']],
            [IApplyAble::APPLY_STATUS['APPROVE'], IApplyAble::APPLY_STATUS['APPROVE']],
            [IApplyAble::APPLY_STATUS['REJECT'], IApplyAble::APPLY_STATUS['REJECT']],
            [999, IApplyAble::APPLY_STATUS['PENDING']]
        ];
    }

    //rejectReason 测试 -------------------------------------------------------- start
    /**
     * 设置 setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $this->trait->setRejectReason('rejectReason');
        $this->assertEquals('rejectReason', $this->trait->getRejectReason());
    }

    /**
     * 设置 setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonWrongType()
    {
        $this->trait->setRejectReason(array('rejectReason'));
    }
    //rejectReason 测试 --------------------------------------------------------   end

    //operationType 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setOperationType() 是否符合预定范围
     *
     * @dataProvider operationTypeProvider
     */
    public function testSetOperationType($actual, $expected)
    {
        $this->trait->setOperationType($actual);
        $this->assertEquals($expected, $this->trait->getOperationType());
    }

    /**
     * 循环测试 setOperationType() 数据构建器
     */
    public function operationTypeProvider()
    {
        return array(
            array(IApplyAble::OPERATION_TYPE['NULL'], IApplyAble::OPERATION_TYPE['NULL']),
            array(IApplyAble::OPERATION_TYPE['ADD'], IApplyAble::OPERATION_TYPE['ADD']),
            array(IApplyAble::OPERATION_TYPE['EDIT'], IApplyAble::OPERATION_TYPE['EDIT']),
            array(IApplyAble::OPERATION_TYPE['ENABLED'], IApplyAble::OPERATION_TYPE['ENABLED']),
            array(IApplyAble::OPERATION_TYPE['DISABLED'], IApplyAble::OPERATION_TYPE['DISABLED']),
            array(IApplyAble::OPERATION_TYPE['TOP'], IApplyAble::OPERATION_TYPE['TOP']),
            array(IApplyAble::OPERATION_TYPE['CANCEL_TOP'], IApplyAble::OPERATION_TYPE['CANCEL_TOP']),
            array(IApplyAble::OPERATION_TYPE['MOVE'], IApplyAble::OPERATION_TYPE['MOVE']),
        );
    }

    /**
     * 设置 setOperationType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOperationTypeWrongType()
    {
        $this->trait->setOperationType(['string']);
    }
    //operationType 测试 ------------------------------------------------------   end

    //applyInfoType 测试 -------------------------------------------------------- start
    /**
     * 设置 setApplyInfoType() 正确的传参类型,期望传值正确
     */
    public function testSetApplyInfoTypeCorrectType()
    {
        $this->trait->setApplyInfoType(1);
        $this->assertEquals(1, $this->trait->getApplyInfoType());
    }

    /**
     * 设置 setApplyInfoType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyInfoTypeWrongType()
    {
        $this->trait->setApplyInfoType(['string']);
    }
    //applyInfoType 测试 --------------------------------------------------------   end

    //relationId 测试 -------------------------------------------------------- start
    /**
     * 设置 setRelationId() 正确的传参类型,期望传值正确
     */
    public function testSetRelationIdCorrectType()
    {
        $this->trait->setRelationId(1);
        $this->assertEquals(1, $this->trait->getRelationId());
    }

    /**
     * 设置 setRelationId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRelationIdWrongType()
    {
        $this->trait->setRelationId(['string']);
    }
    //relationId 测试 --------------------------------------------------------   end

    //publishCrew 测试 -------------------------------------------------------- start
    /**
     * 设置 setPublishCrew() 正确的传参类型,期望传值正确
     */
    public function testSetPublishCrewCorrectType()
    {
        $expectedCrew = new Crew();

        $this->trait->setPublishCrew($expectedCrew);
        $this->assertEquals($expectedCrew, $this->trait->getPublishCrew());
    }

    /**
     * 设置 setPublishCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPublishCrewWrongType()
    {
        $this->trait->setPublishCrew('Crew');
    }
    //publishCrew 测试 --------------------------------------------------------   end

    //applyCrew 测试 -------------------------------------------------------- start
    /**
     * 设置 setApplyCrew() 正确的传参类型,期望传值正确
     */
    public function testSetApplyCrewCorrectType()
    {
        $expectedCrew = new Crew();

        $this->trait->setApplyCrew($expectedCrew);
        $this->assertEquals($expectedCrew, $this->trait->getApplyCrew());
    }

    /**
     * 设置 setApplyCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyCrewWrongType()
    {
        $this->trait->setApplyCrew('Crew');
    }
    //applyCrew 测试 --------------------------------------------------------   end

    //publishUserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 setPublishUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetPublishUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->trait->setPublishUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->trait->getPublishUserGroup());
    }

    /**
     * 设置 setPublishUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function tesSetPublishUserGroupWrongType()
    {
        $this->trait->setPublishUserGroup('publishUserGroup');
    }
    //publishUserGroup 测试 --------------------------------------------------------   end

    //applyUserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 setApplyUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetApplyUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->trait->setApplyUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->trait->getApplyUserGroup());
    }

    /**
     * 设置 setApplyUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function tesSetApplyUserGroupWrongType()
    {
        $this->trait->setApplyUserGroup('applyUserGroup');
    }
    //applyUserGroup 测试 --------------------------------------------------------   end

    public function testApprove()
    {
        $trait = $this->getMockBuilder(MockApplyAbleObject::class)
                            ->setMethods(['approveAction'])
                            ->getMock();

        $trait->expects($this->exactly(1))
             ->method('approveAction')
             ->willReturn(true);
             
        $this->assertTrue($trait->approve());
    }

    public function testApproveActionPublic()
    {
        $mockApplyAbleAdapter = $this->prophesize(MockApplyAbleAdapter::class);
        $mockApplyAbleAdapter->approve($this->trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $this->trait->expects($this->exactly(1))
            ->method('getIApplyAbleAdapter')
            ->willReturn($mockApplyAbleAdapter->reveal());
             
        $this->assertTrue($this->trait->approveActionPublic());
    }

    public function testApproveActionPublicFail()
    {
        $mockApplyAbleAdapter = $this->prophesize(MockApplyAbleAdapter::class);
        $mockApplyAbleAdapter->approve($this->trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(false);
        $this->trait->expects($this->exactly(1))
            ->method('getIApplyAbleAdapter')
            ->willReturn($mockApplyAbleAdapter->reveal());
             
        $this->assertFalse($this->trait->approveActionPublic());
    }

    public function testReject()
    {
        $trait = $this->getMockBuilder(MockApplyAbleObject::class)
                            ->setMethods(['rejectAction'])
                            ->getMock();

        $trait->expects($this->any())
             ->method('rejectAction')
             ->willReturn(true);
             
        $this->assertTrue($trait->reject());
    }

    public function testRejectActionPublic()
    {
        $mockApplyAbleAdapter = $this->prophesize(MockApplyAbleAdapter::class);
        $mockApplyAbleAdapter->reject($this->trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $this->trait->expects($this->exactly(1))
            ->method('getIApplyAbleAdapter')
            ->willReturn($mockApplyAbleAdapter->reveal());
             
        $this->assertTrue($this->trait->rejectActionPublic());
    }

    public function testRejectActionPublicFail()
    {
        $mockApplyAbleAdapter = $this->prophesize(MockApplyAbleAdapter::class);
        $mockApplyAbleAdapter->reject($this->trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(false);
        $this->trait->expects($this->exactly(1))
            ->method('getIApplyAbleAdapter')
            ->willReturn($mockApplyAbleAdapter->reveal());
             
        $this->assertFalse($this->trait->rejectActionPublic());
    }
}
