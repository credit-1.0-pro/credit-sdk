<?php
namespace Sdk\Common\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\Adapter\ITopAbleAdapter;

class TopAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockTopAbleObject();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    /**
     * @dataProvider stickDataProvider
     */
    public function testSetStick($actual, $expected)
    {
        $this->trait->setStick($actual);
        
        $result = $this->trait->getStick();
        $this->assertEquals($expected, $result);
    }

    public function stickDataProvider()
    {
        return [
            [ITopAble::STICK['ENABLED'], ITopAble::STICK['ENABLED']],
            [ITopAble::STICK['DISABLED'], ITopAble::STICK['DISABLED']],
            [999, ITopAble::STICK['DISABLED']]
        ];
    }

    public function testTopSuccess()
    {
        $trait = $this->getMockBuilder(MockTopAbleObject::class)
                      ->setMethods(['getITopAbleAdapter'])
                      ->getMock();
                      
        $trait->setStick(ITopAble::STICK['DISABLED']);

        $adapter = $this->prophesize(ITopAbleAdapter::class);
        $adapter->top($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $trait->expects($this->exactly(1))
            ->method('getITopAbleAdapter')
            ->willReturn($adapter->reveal());
             
        $this->assertTrue($trait->top());
    }

    public function testTopFail()
    {
        $this->trait->setStick(ITopAble::STICK['ENABLED']);
        
        $result = $this->trait->top();

        $this->assertFalse($result);
        $this->assertEquals(Core::getLastError()->getId(), STATUS_CAN_NOT_MODIFY);
    }

    public function testCancelTopSuccess()
    {
        $trait = $this->getMockBuilder(MockTopAbleObject::class)
                      ->setMethods(['getITopAbleAdapter'])
                      ->getMock();

        $trait->setStick(ITopAble::STICK['ENABLED']);
        
        $adapter = $this->prophesize(ITopAbleAdapter::class);
        $adapter->cancelTop($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $trait->expects($this->exactly(1))
            ->method('getITopAbleAdapter')
            ->willReturn($adapter->reveal());
             
        $this->assertTrue($trait->cancelTop());
    }

    public function testCancelTopFail()
    {
        $this->trait->setStick(ITopAble::STICK['DISABLED']);

        $result = $this->trait->cancelTop();

        $this->assertFalse($result);
        $this->assertEquals(Core::getLastError()->getId(), STATUS_CAN_NOT_MODIFY);
    }
}
