<?php
namespace Sdk\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\Adapter\ITopAbleAdapter;

class TopAbleRepositoryTraitTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MockTopAbleRepositoryObject::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testTop()
    {
        $topAbleObject = $this->getMockBuilder(ITopAble::class)
                                ->getMock();

        $adapter = $this->prophesize(ITopAbleAdapter::class);
        $adapter->top(Argument::exact($topAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->top($topAbleObject);
    }

    public function testCancelTop()
    {
        $topAbleObject = $this->getMockBuilder(ITopAble::class)
                                ->getMock();

        $adapter = $this->prophesize(ITopAbleAdapter::class);
        $adapter->cancelTop(Argument::exact($topAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->cancelTop($topAbleObject);
    }
}
