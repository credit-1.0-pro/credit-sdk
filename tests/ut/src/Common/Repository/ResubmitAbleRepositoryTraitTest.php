<?php
namespace Sdk\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Common\Model\IResubmitAble;
use Sdk\Common\Adapter\IResubmitAbleAdapter;

class ResubmitAbleRepositoryTraitTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MockResubmitAbleRepositoryObject::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testResubmit()
    {
        $resubmitAbleObject = $this->getMockBuilder(IResubmitAble::class)
                                ->getMock();

        $adapter = $this->prophesize(IResubmitAbleAdapter::class);
        $adapter->resubmit(Argument::exact($resubmitAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->resubmit($resubmitAbleObject);
    }
}
