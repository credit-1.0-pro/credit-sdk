<?php
namespace Sdk\Common\Command;

use PHPUnit\Framework\TestCase;

class RejectCommandTest extends TestCase
{
    private $command;

    /**
     * 初始化测试场景
     */
    public function setUp()
    {
        $this->command = new MockRejectCommand('rejectReason', 1);
    }

    public function tearDown()
    {
        unset($this->command);
    }

    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    /**
     * 1. 测试初始化赋值
     * 2. 测试初始化赋值是否正确
     */
    public function testConstruct()
    {
        $rejectReason = 'rejectReason';
        $id = 2;

        $this->command = new MockRejectCommand($rejectReason, $id);
        $this->assertEquals($rejectReason, $this->command->rejectReason);
        $this->assertEquals($id, $this->command->id);
    }
}
