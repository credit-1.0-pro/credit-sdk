<?php
namespace Sdk\Common\Utils;

use Sdk\Common\Model\IApplyAble;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;

trait ApplyUtils
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    private function compareArrayAndObject(
        array $expectedArray,
        $apply
    ) {
        $this->assertEquals($expectedArray['rejectReason'], $apply->getRejectReason());
        $this->assertEquals($expectedArray['relationId'], marmot_encode($apply->getRelationId()));
        $this->assertEquals($expectedArray['applyInfoType'], marmot_encode($apply->getApplyInfoType()));
        $this->assertEquals(
            $expectedArray['applyStatus']['id'],
            marmot_encode($apply->getApplyStatus())
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['name'],
            IApplyAble::APPLY_STATUS_CN[$apply->getApplyStatus()]
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['type'],
            IApplyAble::APPLY_STATUS_TAG_TYPE[$apply->getApplyStatus()]
        );
        $this->assertEquals(
            $expectedArray['operationType']['id'],
            marmot_encode($apply->getOperationType())
        );
        $this->assertEquals(
            $expectedArray['operationType']['name'],
            IApplyAble::OPERATION_TYPE_CN[$apply->getOperationType()]
        );
        $this->assertEquals(
            $expectedArray['publishCrew'],
            $this->getCrewTranslator()->objectToArray($apply->getPublishCrew())
        );
        $this->assertEquals(
            $expectedArray['applyCrew'],
            $this->getCrewTranslator()->objectToArray($apply->getApplyCrew())
        );
        $this->assertEquals(
            $expectedArray['applyUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($apply->getApplyUserGroup())
        );
    }
}
