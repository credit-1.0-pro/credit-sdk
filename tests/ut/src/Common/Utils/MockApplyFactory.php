<?php
namespace Sdk\Common\Utils;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\MockApplyAbleObject;

class MockApplyFactory
{
    public static function generateApply(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : MockApplyAbleObject {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $apply = new MockApplyAbleObject($id);
        $apply->setId($id);

         //rejectReason
         $rejectReason = isset($value['rejectReason']) ? $value['rejectReason'] : $faker->title();
         $apply->setRejectReason($rejectReason);

        //operationType
        $operationType = isset($value['operationType']) ? $value['operationType'] : $faker->randomElement(
            IApplyAble::OPERATION_TYPE
        );
        $apply->setOperationType($operationType);

        $apply->setApplyInfoType($id);
        $apply->setRelationId($id);

        //publishCrew,applyCrew
        self::generateCrew($apply, $faker, $value);
        //applyUserGroup,publishUserGroup
        self::generateUserGroup($apply, $faker, $value);

        //applyStatus
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : $faker->randomElement(
            IApplyAble::APPLY_STATUS
        );
        $apply->setApplyStatus($applyStatus);

        
        return $apply;
    }

    protected static function generateCrew($apply, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
                $value['crew'] :
                \Sdk\User\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());

        $apply->setPublishCrew($crew);
        $apply->setApplyCrew($crew);
    }

    protected static function generateUserGroup($apply, $faker, $value) : void
    {
        $userGroup = isset($value['userGroup']) ?
        $value['userGroup'] :
        \Sdk\UserGroup\UserGroup\Utils\MockUserGroupFactory::generateUserGroup($faker->randomDigit());
        
        $apply->setApplyUserGroup($userGroup);
        $apply->setPublishUserGroup($userGroup);
    }
}
