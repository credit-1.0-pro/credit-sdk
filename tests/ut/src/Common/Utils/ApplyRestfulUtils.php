<?php
namespace Sdk\Common\Utils;

trait ApplyRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $apply
    ) {
        if (isset($expectedArray['data']['attributes']['applyStatus'])) {
            $this->assertEquals($expectedArray['data']['attributes']['applyStatus'], $apply->getApplyStatus());
        }
        if (isset($expectedArray['data']['attributes']['operationType'])) {
            $this->assertEquals($expectedArray['data']['attributes']['operationType'], $apply->getOperationType());
        }
        if (isset($expectedArray['data']['attributes']['applyInfoType'])) {
            $this->assertEquals($expectedArray['data']['attributes']['applyInfoType'], $apply->getApplyInfoType());
        }
        if (isset($expectedArray['data']['attributes']['relationId'])) {
            $this->assertEquals($expectedArray['data']['attributes']['relationId'], $apply->getRelationId());
        }

        $this->assertEquals($expectedArray['data']['attributes']['rejectReason'], $apply->getRejectReason());

        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['applyCrew']['data'])) {
                $this->assertEquals(
                    $relationships['applyCrew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['applyCrew']['data'][0]['id'],
                    $apply->getApplyCrew()->getId()
                );
            }
        }
    }
}
