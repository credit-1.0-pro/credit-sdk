<?php
namespace Sdk\Purview\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class PurviewCategoryFactoryTest extends TestCase
{
    private $factory;

    public function setUp()
    {
        $this->factory = new PurviewCategoryFactory();
    }

    public function tearDown()
    {
        unset($this->factory);
    }

    public function testNullCategory()
    {
        $this->assertEquals(0, $this->factory->getCategory(''));
    }

    public function testGetCategory()
    {
        foreach (PurviewCategoryFactory::MAPS as $key => $factory) {
            $this->assertEquals(
                $factory,
                $this->factory->getCategory($key)
            );
        }
    }
}
