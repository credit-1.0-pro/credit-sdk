<?php
namespace Sdk\News\NewsCategory\Utils;

trait NewsCategoryRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $newsCategory
    ) {
        $this->assertEquals($expectedArray['data']['id'], $newsCategory->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $newsCategory->getName());
        $this->assertEquals($expectedArray['data']['attributes']['grade'], $newsCategory->getGrade());
        $this->assertEquals($expectedArray['data']['attributes']['parentCategory'], $newsCategory->getParentCategory());
        $this->assertEquals($expectedArray['data']['attributes']['category'], $newsCategory->getCategory());
        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $newsCategory->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $newsCategory->getUpdateTime());
        }
        if (isset($expectedArray['data']['attributes']['status'])) {
            $this->assertEquals($expectedArray['data']['attributes']['status'], $newsCategory->getStatus());
        }
    }
}
