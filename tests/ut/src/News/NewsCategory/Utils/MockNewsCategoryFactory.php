<?php
namespace Sdk\News\NewsCategory\Utils;

use Sdk\News\NewsCategory\Model\NewsCategory;

class MockNewsCategoryFactory
{
    public static function generateNewsCategory(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : NewsCategory {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $newsCategory = new NewsCategory($id);
        $newsCategory->setId($id);

        //name
        self::generateName($newsCategory, $faker, $value);
        //grade
        self::generateGrade($newsCategory, $faker, $value);
        //parentCategory
        self::generateParentCategory($newsCategory, $faker, $value);
        //category
        self::generateCategory($newsCategory, $faker, $value);

        $newsCategory->setStatus(0);
        $newsCategory->setCreateTime($faker->unixTime());
        $newsCategory->setUpdateTime($faker->unixTime());
        $newsCategory->setStatusTime($faker->unixTime());

        return $newsCategory;
    }

    private static function generateName($newsCategory, $faker, $value) : void
    {
        $name = isset($value['name']) ?$value['name'] :$faker->title;
        
        $newsCategory->setName($name);
    }

    private static function generateGrade($newsCategory, $faker, $value) : void
    {
        $grade = isset($value['grade']) ? $value['grade'] : $faker->randomDigit();
        
        $newsCategory->setGrade($grade);
    }

    private static function generateParentCategory($newsCategory, $faker, $value) : void
    {
        $parentCategory = isset($value['parentCategory']) ? $value['parentCategory'] : $faker->randomDigit();
        
        $newsCategory->setParentCategory($parentCategory);
    }

    private static function generateCategory($newsCategory, $faker, $value) : void
    {
        $category = isset($value['category']) ? $value['category'] : $faker->randomDigit();
        
        $newsCategory->setCategory($category);
    }
}
