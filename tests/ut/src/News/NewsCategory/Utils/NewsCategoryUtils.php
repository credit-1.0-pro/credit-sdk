<?php
namespace Sdk\News\NewsCategory\Utils;

use Sdk\News\NewsCategory\Model\NewsCategory;

trait NewsCategoryUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $newsCategory
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($newsCategory->getId()));
        $this->assertEquals($expectedArray['name'], $newsCategory->getName());
        $this->assertEquals($expectedArray['parentCategory'], marmot_encode($newsCategory->getParentCategory()));
        $this->assertEquals($expectedArray['category'], marmot_encode($newsCategory->getCategory()));
        $this->assertEquals($expectedArray['grade']['id'], marmot_encode($newsCategory->getGrade()));
        $this->assertEquals($expectedArray['grade']['name'], NewsCategory::GRADE_CN[$newsCategory->getGrade()]);
        $this->assertEquals($expectedArray['updateTime'], $newsCategory->getUpdateTime());
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H点i分', $newsCategory->getUpdateTime())
        );
    }
}
