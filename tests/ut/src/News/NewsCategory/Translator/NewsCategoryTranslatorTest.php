<?php
namespace Sdk\News\NewsCategory\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\News\NewsCategory\Utils\NewsCategoryUtils;

class NewsCategoryTranslatorTest extends TestCase
{
    use NewsCategoryUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new NewsCategoryTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\News\NewsCategory\Model\NullNewsCategory', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $newsCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);

        $expression = $this->translator->objectToArray($newsCategory);

        $this->compareArrayAndObject($expression, $newsCategory);
    }

    public function testObjectToArrayFail()
    {
        $newsCategory = null;

        $expression = $this->translator->objectToArray($newsCategory);
        $this->assertEquals(array(), $expression);
    }
}
