<?php
namespace Sdk\News\NewsCategory\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Utils\NewsCategoryRestfulUtils;

class NewsCategoryRestfulTranslatorTest extends TestCase
{
    use NewsCategoryRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new NewsCategoryRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $newsCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);

        $expression['data']['id'] = $newsCategory->getId();
        $expression['data']['attributes']['name'] = $newsCategory->getName();
        $expression['data']['attributes']['grade'] = $newsCategory->getGrade();
        $expression['data']['attributes']['parentCategory'] = $newsCategory->getParentCategory();
        $expression['data']['attributes']['category'] = $newsCategory->getCategory();
        $expression['data']['attributes']['createTime'] = $newsCategory->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $newsCategory->getUpdateTime();
        $expression['data']['attributes']['status'] = $newsCategory->getStatus();

        $newsCategoryObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\News\NewsCategory\Model\NewsCategory', $newsCategoryObject);
        $this->compareArrayAndObject($expression, $newsCategoryObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $department = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\News\NewsCategory\Model\NullNewsCategory', $department);
    }

    public function testObjectToArray()
    {
        $newsCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);

        $expression = $this->translator->objectToArray($newsCategory);

        $this->compareArrayAndObject($expression, $newsCategory);
    }

    public function testObjectToArrayFail()
    {
        $newsCategory = null;

        $expression = $this->translator->objectToArray($newsCategory);
        $this->assertEquals(array(), $expression);
    }
}
