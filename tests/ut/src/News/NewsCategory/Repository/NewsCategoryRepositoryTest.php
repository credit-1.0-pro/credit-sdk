<?php
namespace Sdk\News\NewsCategory\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\News\NewsCategory\Adapter\NewsCategory\NewsCategoryRestfulAdapter;

class NewsCategoryRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(NewsCategoryRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsINewsCategoryAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\Adapter\NewsCategory\INewsCategoryAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\Adapter\NewsCategory\NewsCategoryRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\Adapter\NewsCategory\NewsCategoryMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(NewsCategoryRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
