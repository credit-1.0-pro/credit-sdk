<?php
namespace Sdk\News\NewsCategory\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\News\NewsCategory\Repository\NewsCategoryRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class NewsCategoryTest extends TestCase
{
    private $newsCategory;

    public function setUp()
    {
        $this->newsCategory = new NewsCategory();
    }

    public function tearDown()
    {
        unset($this->newsCategory);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->newsCategory
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\IOperateAble',
            $this->newsCategory
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $newsCategory = new MockNewsCategory();
        $this->assertInstanceOf('Sdk\Common\Adapter\IOperateAbleAdapter', $newsCategory->getIOperateAbleAdapter());
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\Repository\NewsCategoryRepository',
            $newsCategory->getIOperateAbleAdapter()
        );
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->newsCategory->getId());

        $this->assertEmpty($this->newsCategory->getName());

        $this->assertEmpty($this->newsCategory->getParentCategory());

        $this->assertEmpty($this->newsCategory->getCategory());

        $this->assertEquals(NewsCategory::GRADE['GRADE_ONE'], $this->newsCategory->getGrade());
        $this->assertEquals(NewsCategory::STATUS_NORMAL, $this->newsCategory->getStatus());

        $this->assertEquals(0, $this->newsCategory->getUpdateTime());
        $this->assertEquals(0, $this->newsCategory->getCreateTime());
        $this->assertEquals(0, $this->newsCategory->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 NewsCategory setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->newsCategory->setId(1);
        $this->assertEquals(1, $this->newsCategory->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 NewsCategory setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->newsCategory->setName('string');
        $this->assertEquals('string', $this->newsCategory->getName());
    }

    /**
     * 设置 NewsCategory setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->newsCategory->setName(array(1,2,3));
    }
    //name 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 NewsCategory setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->newsCategory->setStatus(0);
        $this->assertEquals(0, $this->newsCategory->getStatus());
    }

    /**
     * 设置 NewsCategory setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->newsCategory->setStatus(array(1,2,3));
    }
    //status 测试 --------------------------------------------------------   end
        
    //parentCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 NewsCategory setParentCategory() 正确的传参类型,期望传值正确
     */
    public function testSetParentCategoryCorrectType()
    {
        $this->newsCategory->setParentCategory(1);
        $this->assertEquals(1, $this->newsCategory->getParentCategory());
    }

    /**
     * 设置 NewsCategory setParentCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetParentCategoryWrongType()
    {
        $this->newsCategory->setParentCategory(array(1,2,3));
    }
    //parentCategory 测试 --------------------------------------------------------   end

    //category 测试 -------------------------------------------------------- start
    /**
     * 设置 NewsCategory setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $this->newsCategory->setCategory(1);
        $this->assertEquals(1, $this->newsCategory->getCategory());
    }

    /**
     * 设置 NewsCategory setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->newsCategory->setCategory(array(1,2,3));
    }
    //category 测试 --------------------------------------------------------   end

    //grade 测试 ------------------------------------------------------ start
    /**
     * 循环测试 NewsCategory setGrade() 是否符合预定范围
     *
     * @dataProvider gradeProvider
     */
    public function testSetGrade($actual, $expected)
    {
        $this->newsCategory->setGrade($actual);
        $this->assertEquals($expected, $this->newsCategory->getGrade());
    }

    /**
     * 循环测试 NewsCategory setGrade() 数据构建器
     */
    public function gradeProvider()
    {
        return array(
            array(NewsCategory::GRADE['GRADE_ONE'],NewsCategory::GRADE['GRADE_ONE']),
            array(NewsCategory::GRADE['GRADE_TWO'],NewsCategory::GRADE['GRADE_TWO']),
            array(NewsCategory::GRADE['GRADE_THREE'],NewsCategory::GRADE['GRADE_THREE']),
            array(999,NewsCategory::GRADE['GRADE_ONE'])
        );
    }

    /**
     * 设置 NewsCategory setGrade() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGradeWrongType()
    {
        $this->newsCategory->setGrade('string');
    }
    //grade 测试 --------------------------

    public function testFetchOne()
    {
        //初始化
        $model = $this->getMockBuilder(MockNewsCategory::class)
                           ->setMethods(['getIOperateAbleAdapter'])
                           ->getMock();
        $id = 1;
        $newsCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory($id);

        //预言 NewsCategoryRepository
        $repository = $this->prophesize(NewsCategoryRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($newsCategory);

        //绑定
        $model->expects($this->once())->method('getIOperateAbleAdapter')->willReturn($repository->reveal());

        //验证
        $result = $model->fetchOne($id);
        $this->assertInstanceOf('Sdk\News\NewsCategory\Model\NewsCategory', $result);
        $this->assertEquals($result, $newsCategory);
    }

    public function testValidate()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                           ->setMethods([
                               'fetchParentCategory',
                               'fetchCategory',
                               'isParentCategoryExist',
                               'isCategoryExist',
                               'isBelongToParentCategory',
                               'nameIsExist',
                            ])->getMock();

        $parentCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);
        $category = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(2);

        $newsCategory->expects($this->once())->method('fetchParentCategory')->willReturn($parentCategory);
        $newsCategory->expects($this->once())->method('fetchCategory')->willReturn($category);
        $newsCategory->expects($this->once())->method(
            'isParentCategoryExist'
        )->with($parentCategory)->willReturn(true);
        $newsCategory->expects($this->once())->method('isCategoryExist')->with($category)->willReturn(true);
        $newsCategory->expects($this->once())->method(
            'isBelongToParentCategory'
        )->with($parentCategory, $category)->willReturn(true);
        $newsCategory->expects($this->once())->method('nameIsExist')->willReturn(true);
                   
        $result = $newsCategory->validate();
        $this->assertTrue($result);
    }

    public function testFetchParentCategory()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['fetchOne'])->getMock();

        $parentCategoryId = 1;
        $newsCategory->setParentCategory($parentCategoryId);
        $parentCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory($parentCategoryId);

        $newsCategory->expects($this->once())->method(
            'fetchOne'
        )->with($parentCategoryId)->willReturn($parentCategory);
                   
        $result = $newsCategory->fetchParentCategory();
        $this->assertInstanceOf('Sdk\News\NewsCategory\Model\NewsCategory', $result);
        $this->assertEquals($result, $parentCategory);
    }

    public function testFetchParentCategoryNull()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['getParentCategory'])->getMock();
               
        $newsCategory->expects($this->once())->method('getParentCategory')->willReturn(0);

        $result = $newsCategory->fetchParentCategory();

        $this->assertInstanceOf('Sdk\News\NewsCategory\Model\NullNewsCategory', $result);
    }

    public function testFetchCategory()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['fetchOne'])->getMock();

        $categoryId = 1;
        $newsCategory->setCategory($categoryId);
        $category = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory($categoryId);

        $newsCategory->expects($this->once())->method(
            'fetchOne'
        )->with($categoryId)->willReturn($category);
                   
        $result = $newsCategory->fetchCategory();
        $this->assertInstanceOf('Sdk\News\NewsCategory\Model\NewsCategory', $result);
        $this->assertEquals($result, $category);
    }

    public function testFetchCategoryNull()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['getCategory'])->getMock();
               
        $newsCategory->expects($this->once())->method('getCategory')->willReturn(0);

        $result = $newsCategory->fetchCategory();

        $this->assertInstanceOf('Sdk\News\NewsCategory\Model\NullNewsCategory', $result);
    }

    public function testIsParentCategoryExistFail()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['getParentCategory'])->getMock();
               
        $newsCategory->expects($this->once())->method('getParentCategory')->willReturn(1);
        $parentCategory = NullNewsCategory::getInstance();

        $result = $newsCategory->isParentCategoryExist($parentCategory);

        $this->assertFalse($result);
        $this->assertEquals(NEWS_CATEGORY_PARENT_CATEGORY_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testIsParentCategoryExistSuccess()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['getParentCategory'])->getMock();
               
        $newsCategory->expects($this->once())->method('getParentCategory')->willReturn(1);
        $parentCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);

        $result = $newsCategory->isParentCategoryExist($parentCategory);

        $this->assertTrue($result);
    }

    public function testIsParentCategoryExistEmptySuccess()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['getParentCategory'])->getMock();
               
        $newsCategory->expects($this->once())->method('getParentCategory')->willReturn(0);
        $parentCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);

        $result = $newsCategory->isParentCategoryExist($parentCategory);

        $this->assertTrue($result);
    }

    public function testIsCategoryExistFail()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['getCategory'])->getMock();
               
        $newsCategory->expects($this->once())->method('getCategory')->willReturn(1);
        $category = NullNewsCategory::getInstance();

        $result = $newsCategory->isCategoryExist($category);

        $this->assertFalse($result);
        $this->assertEquals(NEWS_CATEGORY_CATEGORY_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testIsCategoryExistSuccess()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['getCategory'])->getMock();
               
        $newsCategory->expects($this->once())->method('getCategory')->willReturn(1);
        $category = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);

        $result = $newsCategory->isCategoryExist($category);

        $this->assertTrue($result);
    }

    public function testIsCategoryExistEmptySuccess()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['getCategory'])->getMock();
               
        $newsCategory->expects($this->once())->method('getCategory')->willReturn(0);
        $category = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);

        $result = $newsCategory->isCategoryExist($category);

        $this->assertTrue($result);
    }

    public function testIsBelongToParentCategoryFail()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['getCategory', 'getParentCategory'])->getMock();
               
        $newsCategory->expects($this->once())->method('getParentCategory')->willReturn(1);
        $newsCategory->expects($this->once())->method('getCategory')->willReturn(1);

        $category = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);
        $category->setParentCategory(2);
        $parentCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);

        $result = $newsCategory->isBelongToParentCategory($parentCategory, $category);

        $this->assertFalse($result);
        $this->assertEquals(CATEGORY_NOT_BELONG_TO_THE_PARENT_CATEGORY, Core::getLastError()->getId());
    }

    public function testIsBelongToParentCategorySuccess()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['getCategory', 'getParentCategory'])->getMock();
               
        $newsCategory->expects($this->once())->method('getParentCategory')->willReturn(1);
        $newsCategory->expects($this->once())->method('getCategory')->willReturn(1);

        $category = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);
        $parentCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(
            $category->getParentCategory()
        );

        $result = $newsCategory->isBelongToParentCategory($parentCategory, $category);

        $this->assertTrue($result);
    }

    public function testIsBelongToParentCategoryParentCategoryEmptySuccess()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['getParentCategory'])->getMock();
               
        $newsCategory->expects($this->once())->method('getParentCategory')->willReturn(0);
        $category = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);

        $result = $newsCategory->isBelongToParentCategory($category, $category);

        $this->assertTrue($result);
    }

    public function testIsBelongToParentCategoryCategoryEmptySuccess()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                             ->setMethods(['getParentCategory', 'getCategory'])->getMock();
               
        $newsCategory->expects($this->once())->method('getParentCategory')->willReturn(1);
        $newsCategory->expects($this->once())->method('getCategory')->willReturn(0);
        $category = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);

        $result = $newsCategory->isBelongToParentCategory($category, $category);

        $this->assertTrue($result);
    }

    private function initialNameIsExist($result)
    {
        $this->newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                           ->setMethods(['getIOperateAbleAdapter'])
                           ->getMock();

        $id = 1;
        
        $sort = ['-updateTime'];
        $newsCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory($id);

        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($newsCategory);
        }

        $this->newsCategory->setId($newsCategory->getId());
        $this->newsCategory->setName($newsCategory->getName());
        $this->newsCategory->setParentCategory($newsCategory->getParentCategory());
        $this->newsCategory->setCategory($newsCategory->getCategory());

        $filter['id'] = $newsCategory->getId();
        $filter['unique'] = $newsCategory->getName();
        $filter['parentCategory'] = $newsCategory->getParentCategory();
        $filter['category'] = $newsCategory->getCategory();

        $repository = $this->prophesize(NewsCategoryRepository::class);

        $repository->scenario(Argument::exact(NewsCategoryRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort)
        )->shouldBeCalledTimes(1)->willReturn([$count, $list]);

        $this->newsCategory->expects($this->once())
                   ->method('getIOperateAbleAdapter')
                   ->willReturn($repository->reveal());
    }

    public function testNameIsExistFailure()
    {
        $this->initialNameIsExist(false);

        $result = $this->newsCategory->nameIsExist();

        $this->assertFalse($result);
        $this->assertEquals(NEWS_CATEGORY_NAME_IS_UNIQUE, Core::getLastError()->getId());
    }

    public function testNameIsExistSuccess()
    {
        $this->initialNameIsExist(true);
        
        $result = $this->newsCategory->nameIsExist();

        $this->assertTrue($result);
    }

    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getIOperateAbleAdapter 被调用一次
     * 3. 期望 NewsCategoryRepository 调用 add 并且传参 $newsCategory 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                           ->setMethods(['getIOperateAbleAdapter', 'validate'])
                           ->getMock();

        $newsCategory->expects($this->once())->method('validate')->willReturn(true);

        //预言 NewsCategoryRepository
        $repository = $this->prophesize(NewsCategoryRepository::class);
        $repository->add(Argument::exact($newsCategory))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $newsCategory->expects($this->once())
             ->method('getIOperateAbleAdapter')
             ->willReturn($repository->reveal());

        //验证
        $result = $newsCategory->add();
        $this->assertTrue($result);
    }

    /**
     * 测试添加失败-名称重复
     * 1. 期望返回false
     * 2. 期望validate被调用一次,并返回false
     * 3. 调用add,期望返回false
     */
    public function testAddFailNameExist()
    {
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                           ->setMethods(['validate'])
                           ->getMock();

        $newsCategory->expects($this->once())->method('validate')->willReturn(false);

        $result = $newsCategory->add();

        $this->assertFalse($result);
    }

    //add()
    /**
     * 测试添加失败
     * 1. 期望返回false
     * 2. 期望 getIOperateAbleAdapter 被调用一次
     * 3. 期望 NewsCategoryRepository 调用 add 并且传参 $newsCategory 对象, 返回 false
     */
    public function testAddFail()
    {
        //初始化
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                           ->setMethods(['validate', 'getIOperateAbleAdapter'])
                           ->getMock();

        $newsCategory->expects($this->once())->method('validate')->willReturn(true);

        //预言 NewsCategoryRepository
        $repository = $this->prophesize(NewsCategoryRepository::class);
        $repository->add(Argument::exact($newsCategory))->shouldBeCalledTimes(1)->willReturn(false);

        //绑定
        $newsCategory->expects($this->once())->method('getIOperateAbleAdapter')->willReturn($repository->reveal());

        //验证
        $result = $newsCategory->add();
        $this->assertFalse($result);
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望更新 name, updateTime
     * 2. 期望 getIOperateAbleAdapter 被调用一次
     * 3. 期望 NewsCategoryRepository 调用 edit 并且传参 $newsCategory 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                           ->setMethods(['nameIsExist', 'getIOperateAbleAdapter'])
                           ->getMock();

        $newsCategory->expects($this->once())->method('nameIsExist')->willReturn(true);
                   
        //预言 NewsCategoryRepository
        $repository = $this->prophesize(NewsCategoryRepository::class);
        $repository->edit(
            Argument::exact($newsCategory)
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $newsCategory->expects($this->once())
             ->method('getIOperateAbleAdapter')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $newsCategory->edit();
        $this->assertTrue($result);
    }

    /**
     * 测试编辑失败-名称重复
     * 1. 期望返回false
     * 2. 期望nameIsExist被调用一次,并返回false
     * 3. 调用edit,期望返回false
     */
    public function testEditFailNameExist()
    {
        //初始化
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                           ->setMethods(['nameIsExist'])
                           ->getMock();

        $newsCategory->expects($this->once())->method('nameIsExist')->willReturn(false);

        //验证
        $result = $newsCategory->edit();

        $this->assertFalse($result);
    }
    //edit
    /**
     * 期望编辑失败
     * 1. 期望更新 name, updateTime
     * 2. 期望 getIOperateAbleAdapter 被调用一次
     * 3. 期望 NewsCategoryRepository 调用 edit 并且传参 $newsCategory 对象, 和相应字段, 返回false
     */
    public function testEditFail()
    {
        //初始化
        $newsCategory = $this->getMockBuilder(MockNewsCategory::class)
                           ->setMethods(['nameIsExist', 'getIOperateAbleAdapter'])
                           ->getMock();

        $newsCategory->expects($this->once())->method('nameIsExist')->willReturn(true);

        //预言 NewsCategoryRepository
        $repository = $this->prophesize(NewsCategoryRepository::class);
        $repository->edit(
            Argument::exact($newsCategory)
        )->shouldBeCalledTimes(1)
         ->willReturn(false);

        //绑定
        $newsCategory->expects($this->once())
             ->method('getIOperateAbleAdapter')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $newsCategory->edit();
        $this->assertFalse($result);
    }
}
