<?php
namespace Sdk\News\NewsCategory\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullNewsCategoryTest extends TestCase
{
    private $newsCategory;
    
    private $childNewsCategory;

    public function setUp()
    {
        $this->newsCategory = NullNewsCategory::getInstance();
        $this->childNewsCategory = new class extends NullNewsCategory
        {
            public function resourceNotExist() : bool
            {
                return parent::resourceNotExist();
            }
        };
    }

    public function tearDown()
    {
        unset($this->newsCategory);
        unset($this->childNewsCategory);
    }

    public function testExtendsNewsCategory()
    {
        $this->assertInstanceof('Sdk\News\NewsCategory\Model\NewsCategory', $this->newsCategory);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->newsCategory);
    }

    public function testResourceNotExist()
    {
        $result = $this->childNewsCategory->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
