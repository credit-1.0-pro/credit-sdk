<?php
namespace Sdk\News\NewsCategory\CommandHandler\NewsCategory;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\News\NewsCategory\Command\NewsCategory\AddNewsCategoryCommand;
use Sdk\News\NewsCategory\Command\NewsCategory\EditNewsCategoryCommand;

class NewsCategoryCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new NewsCategoryCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddNewsCategoryCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddNewsCategoryCommand(
                $this->faker->name(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\CommandHandler\NewsCategory\AddNewsCategoryCommandHandler',
            $commandHandler
        );
    }

    public function testEditNewsCategoryCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditNewsCategoryCommand(
                $this->faker->name(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\CommandHandler\NewsCategory\EditNewsCategoryCommandHandler',
            $commandHandler
        );
    }
}
