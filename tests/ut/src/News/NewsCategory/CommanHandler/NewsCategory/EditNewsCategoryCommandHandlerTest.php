<?php
namespace Sdk\News\NewsCategory\CommandHandler\NewsCategory;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Repository\NewsCategoryRepository;
use Sdk\News\NewsCategory\Command\NewsCategory\EditNewsCategoryCommand;

class EditNewsCategoryCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditNewsCategoryCommandHandler::class)
                                     ->setMethods(['getRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockEditNewsCategoryCommandHandler();
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\Repository\NewsCategoryRepository',
            $commandHandler->getRepository()
        );
    }

    public function initialExecute($result)
    {
        $command = new EditNewsCategoryCommand(
            $this->faker->name,
            $this->faker->randomNumber()
        );

        $newsCategory = $this->prophesize(NewsCategory::class);
        $newsCategory->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $newsCategory->edit()->shouldBeCalledTimes(1)->willReturn($result);

        $newsCategoryRepository = $this->prophesize(NewsCategoryRepository::class);
        $newsCategoryRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($newsCategory->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($newsCategoryRepository->reveal());

        return $command;
    }
    
    public function testExecute()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }
}
