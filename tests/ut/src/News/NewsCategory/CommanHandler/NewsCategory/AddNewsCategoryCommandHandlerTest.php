<?php
namespace Sdk\News\NewsCategory\CommandHandler\NewsCategory;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Command\NewsCategory\AddNewsCategoryCommand;

class AddNewsCategoryCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddNewsCategoryCommandHandler::class)
                                     ->setMethods(['getNewsCategory'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetNewsCategory()
    {
        $commandHandler = new MockAddNewsCategoryCommandHandler();
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\Model\NewsCategory',
            $commandHandler->getNewsCategory()
        );
    }

    public function initialExecute($result)
    {
        $name = 'name';
        $grade = 1;
        $category = 1;
        $parentCategory = 1;

        $command = new AddNewsCategoryCommand(
            $name,
            $grade,
            $parentCategory,
            $category
        );

        $newsCategory = $this->prophesize(NewsCategory::class);
        $newsCategory->setName(Argument::exact($name))->shouldBeCalledTimes(1);
        $newsCategory->setGrade(Argument::exact($grade))->shouldBeCalledTimes(1);
        $newsCategory->setParentCategory(Argument::exact($parentCategory))->shouldBeCalledTimes(1);
        $newsCategory->setCategory(Argument::exact($category))->shouldBeCalledTimes(1);
        $newsCategory->add()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->any())
            ->method('getNewsCategory')
            ->willReturn($newsCategory->reveal());
                    
        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);
        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }
}
