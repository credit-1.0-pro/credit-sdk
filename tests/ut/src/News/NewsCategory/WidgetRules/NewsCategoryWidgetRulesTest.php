<?php
namespace Sdk\News\NewsCategory\WidgetRules;

use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Sdk\Common\Utils\StringGenerate;

use Sdk\News\NewsCategory\Model\NewsCategory;

class NewsCategoryWidgetRulesTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = NewsCategoryWidgetRules::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //newsCategoryName -- start
    /**
     * @dataProvider invalidNewsCategoryNameProvider
     */
    public function testNewsCategoryNameInvalid($actual, $expected)
    {
        $result = $this->widgetRule->newsCategoryName($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(NEWS_CATEGORY_NAME_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidNewsCategoryNameProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(NewsCategoryWidgetRules::NAME_MIN_LENGTH-1), false),
            array(StringGenerate::generate(NewsCategoryWidgetRules::NAME_MAX_LENGTH+1), false),
            array(StringGenerate::generate(NewsCategoryWidgetRules::NAME_MIN_LENGTH), true),
            array(StringGenerate::generate(NewsCategoryWidgetRules::NAME_MIN_LENGTH+1), true),
            array(StringGenerate::generate(NewsCategoryWidgetRules::NAME_MAX_LENGTH), true),
            array(StringGenerate::generate(NewsCategoryWidgetRules::NAME_MAX_LENGTH-1), true)
        );
    }
    //newsCategoryName -- end
       
    //grade -- start
    /**
     * @dataProvider invalidGradeProvider
     */
    public function testGradeInvalid($actual, $expected)
    {
        $result = $this->widgetRule->grade($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(NEWS_CATEGORY_GRADE_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidGradeProvider()
    {
        return array(
            array('', false),
            array(NewsCategory::GRADE['GRADE_ONE'], true),
            array(NewsCategory::GRADE['GRADE_TWO'], true),
            array(NewsCategory::GRADE['GRADE_THREE'], true),
            array(999, false),
        );
    }
    //grade -- end

    //parentCategory -- start
    /**
     * @dataProvider invalidParentCategoryProvider
     */
    public function testParentCategoryInvalid($actual, $expected)
    {
        $result = $this->widgetRule->parentCategory($actual['grade'], $actual['parentCategory']);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(NEWS_CATEGORY_PARENT_CATEGORY_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidParentCategoryProvider()
    {
        return array(
            array(['grade' => NewsCategory::GRADE['GRADE_ONE'], 'parentCategory' => ''], false),
            array(['grade' => NewsCategory::GRADE['GRADE_ONE'], 'parentCategory' => 1], false),
            array(['grade' => NewsCategory::GRADE['GRADE_ONE'], 'parentCategory' => 0], true),
            array(['grade' => NewsCategory::GRADE['GRADE_THREE'], 'parentCategory' => 1], true),
        );
    }
    //parentCategory -- end

    //category -- start
    /**
     * @dataProvider invalidCategoryProvider
     */
    public function testCategoryInvalid($actual, $expected)
    {
        $result = $this->widgetRule->category($actual['grade'], $actual['category']);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(NEWS_CATEGORY_CATEGORY_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidCategoryProvider()
    {
        return array(
            array(['grade' => NewsCategory::GRADE['GRADE_ONE'], 'category' => ''], false),
            array(['grade' => NewsCategory::GRADE['GRADE_ONE'], 'category' => 1], false),
            array(['grade' => NewsCategory::GRADE['GRADE_ONE'], 'category' => 0], true),
            array(['grade' => NewsCategory::GRADE['GRADE_THREE'], 'category' => 1], true),
            array(['grade' => NewsCategory::GRADE['GRADE_TWO'], 'category' => 1], false),
            array(['grade' => NewsCategory::GRADE['GRADE_TWO'], 'category' => 0], true),
            array(['grade' => NewsCategory::GRADE['GRADE_THREE'], 'category' => 0], true),
        );
    }
    //category -- end
}
