<?php
namespace Sdk\News\NewsCategory\Command\NewsCategory;

use PHPUnit\Framework\TestCase;

class AddNewsCategoryCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'name' => $faker->name(),
            'grade' => $faker->randomNumber(),
            'parentCategory' => $faker->randomNumber(),
            'category' => $faker->randomNumber(),
            'id' => $faker->randomNumber()
        );

        $this->command = new AddNewsCategoryCommand(
            $this->fakerData['name'],
            $this->fakerData['grade'],
            $this->fakerData['parentCategory'],
            $this->fakerData['category'],
            $this->fakerData['id']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }

    public function testNewsCategoryNameParameter()
    {
        $this->assertEquals($this->fakerData['name'], $this->command->name);
    }

    public function testGradeParameter()
    {
        $this->assertEquals($this->fakerData['grade'], $this->command->grade);
    }

    public function testParentCategoryParameter()
    {
        $this->assertEquals($this->fakerData['parentCategory'], $this->command->parentCategory);
    }

    public function testCategoryParameter()
    {
        $this->assertEquals($this->fakerData['category'], $this->command->category);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}
