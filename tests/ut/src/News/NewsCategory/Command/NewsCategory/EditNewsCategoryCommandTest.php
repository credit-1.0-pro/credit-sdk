<?php
namespace Sdk\News\NewsCategory\Command\NewsCategory;

use PHPUnit\Framework\TestCase;

class EditNewsCategoryCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'name' => $faker->name(),
            'id' => $faker->randomNumber()
        );

        $this->command = new EditNewsCategoryCommand(
            $this->fakerData['name'],
            $this->fakerData['id']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testNewsCategoryNameParameter()
    {
        $this->assertEquals($this->fakerData['name'], $this->command->name);
    }
}
