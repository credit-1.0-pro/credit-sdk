<?php
namespace Sdk\News\NewsCategory\Adapter\NewsCategory;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Model\NullNewsCategory;
use Sdk\News\NewsCategory\Translator\NewsCategoryRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class NewsCategoryRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockNewsCategoryRestfulAdapter::class)
                           ->setMethods([
                               'commonMapErrors',
                               'getTranslator',
                               'post',
                               'isSuccess',
                               'patch',
                               'translateToObject'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new NewsCategoryRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsINewsCategoryAdapter()
    {
        $adapter = new NewsCategoryRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\Adapter\NewsCategory\INewsCategoryAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockNewsCategoryRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\Translator\NewsCategoryRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockNewsCategoryRestfulAdapter();
        $this->assertEquals('news/categories', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockNewsCategoryRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\Model\NullNewsCategory',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockNewsCategoryRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'NEWS_CATEGORY_LIST',
                NewsCategoryRestfulAdapter::SCENARIOS['NEWS_CATEGORY_LIST']
            ],
            [
                'NEWS_CATEGORY_FETCH_ONE',
                NewsCategoryRestfulAdapter::SCENARIOS['NEWS_CATEGORY_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    /**
     * 为NewsCategoryRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$newsCategory,$keys,$newsCategoryArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareNewsCategoryTranslator(
        NewsCategory $newsCategory,
        array $keys,
        array $newsCategoryArray
    ) {
        $translator = $this->prophesize(NewsCategoryRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($newsCategory),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($newsCategoryArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(NewsCategory $newsCategory)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($newsCategory);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsCategoryTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $newsCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);
        $newsCategoryArray = array();

        $this->prepareNewsCategoryTranslator(
            $newsCategory,
            array('name', 'grade', 'parentCategory', 'category'),
            $newsCategoryArray
        );

        $this->adapter
            ->method('post')
            ->with('news/categories', $newsCategoryArray);

        $this->success($newsCategory);
        $result = $this->adapter->add($newsCategory);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsCategoryTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $newsCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);
        $newsCategoryArray = array();

        $this->prepareNewsCategoryTranslator(
            $newsCategory,
            array('name', 'grade', 'parentCategory', 'category'),
            $newsCategoryArray
        );

        $this->adapter
            ->method('post')
            ->with('news/categories', $newsCategoryArray);
        
            $this->failure($newsCategory);
            $result = $this->adapter->add($newsCategory);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsCategoryTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $newsCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);
        $newsCategoryArray = array();

        $this->prepareNewsCategoryTranslator($newsCategory, array('name'), $newsCategoryArray);

        $this->adapter
            ->method('patch')
            ->with('news/categories/'.$newsCategory->getId(), $newsCategoryArray);

        $this->success($newsCategory);
        $result = $this->adapter->edit($newsCategory);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsCategoryTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $newsCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);
        $newsCategoryArray = array();

        $this->prepareNewsCategoryTranslator($newsCategory, array('name'), $newsCategoryArray);

        $this->adapter
            ->method('patch')
            ->with('news/categories/'.$newsCategory->getId(), $newsCategoryArray);
        
            $this->failure($newsCategory);
            $result = $this->adapter->edit($newsCategory);
            $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [
            100 => [
                'parentCategory' => NEWS_CATEGORY_PARENT_CATEGORY_NOT_EXIST,
                'category' => NEWS_CATEGORY_CATEGORY_NOT_EXIST
            ],
            101 => [
                'name' => NEWS_CATEGORY_NAME_FORMAT_ERROR,
                'grade' => NEWS_CATEGORY_GRADE_FORMAT_ERROR,
                'parentCategory' => NEWS_CATEGORY_PARENT_CATEGORY_FORMAT_ERROR,
                'category' => NEWS_CATEGORY_CATEGORY_FORMAT_ERROR
            ],
            103 => [
                'newsCategoryName' => NEWS_CATEGORY_NAME_IS_UNIQUE
            ],
            104 => [
                'categoryNotBelongToParentCategory' => CATEGORY_NOT_BELONG_TO_THE_PARENT_CATEGORY
            ]
        ];
        
        $result = $this->adapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
