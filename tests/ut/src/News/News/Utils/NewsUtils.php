<?php
namespace Sdk\News\News\Utils;

use Marmot\Framework\Classes\Filter;

use Sdk\News\News\Model\News;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\IEnableAble;

use Sdk\User\Crew\Translator\CrewTranslator;
use Sdk\UserGroup\UserGroup\Translator\UserGroupTranslator;
use Sdk\News\NewsCategory\Translator\NewsCategoryTranslator;

trait NewsUtils
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getNewsCategoryTranslator() : NewsCategoryTranslator
    {
        return new NewsCategoryTranslator();
    }

    private function compareArrayAndObjectNews(
        array $expectedArray,
        $news
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $news);
    }

    private function compareArrayAndObjectUnAuditedNews(
        array $expectedArray,
        $unAuditedNews
    ) {
        $this->assertEquals($expectedArray['rejectReason'], $unAuditedNews->getRejectReason());
        $this->assertEquals($expectedArray['relationId'], marmot_encode($unAuditedNews->getRelationId()));
        $this->assertEquals($expectedArray['applyInfoType'], marmot_encode($unAuditedNews->getApplyInfoType()));
        $this->assertEquals(
            $expectedArray['applyStatus']['id'],
            marmot_encode($unAuditedNews->getApplyStatus())
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['name'],
            IApplyAble::APPLY_STATUS_CN[$unAuditedNews->getApplyStatus()]
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['type'],
            IApplyAble::APPLY_STATUS_TAG_TYPE[$unAuditedNews->getApplyStatus()]
        );
        $this->assertEquals(
            $expectedArray['operationType']['id'],
            marmot_encode($unAuditedNews->getOperationType())
        );
        $this->assertEquals(
            $expectedArray['operationType']['name'],
            IApplyAble::OPERATION_TYPE_CN[$unAuditedNews->getOperationType()]
        );
        $this->assertEquals(
            $expectedArray['publishCrew'],
            $this->getCrewTranslator()->objectToArray($unAuditedNews->getPublishCrew())
        );
        $this->assertEquals(
            $expectedArray['applyCrew'],
            $this->getCrewTranslator()->objectToArray($unAuditedNews->getApplyCrew())
        );
        $this->assertEquals(
            $expectedArray['applyUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($unAuditedNews->getApplyUserGroup())
        );
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $news
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($news->getId()));
        $this->assertEquals($expectedArray['title'], $news->getTitle());
        $this->assertEquals($expectedArray['source'], $news->getSource());
        $this->coverEquals($expectedArray, $news);
        $this->attachmentsEquals($expectedArray, $news);
        $this->assertEquals(
            $expectedArray['content'],
            Filter::dhtmlspecialchars(str_replace("ℑ", "", $news->getContent()))
        );
        $this->assertEquals(
            $expectedArray['description'],
            $news->getDescription()
        );
        $this->assertEquals(
            $expectedArray['crew'],
            $this->getCrewTranslator()->objectToArray($news->getCrew())
        );
        $this->assertEquals(
            $expectedArray['publishUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($news->getUserGroup())
        );
        $this->assertEquals(
            $expectedArray['newsType'],
            $this->getNewsCategoryTranslator()->objectToArray($news->getNewsType())
        );
        $this->bannerImageEquals($expectedArray, $news);
        $this->assertEquals(
            $expectedArray['dimension']['id'],
            marmot_encode($news->getDimension())
        );
        $this->assertEquals(
            $expectedArray['dimension']['name'],
            News::DIMENSION_CN[$news->getDimension()]
        );

        $this->assertEquals(
            $expectedArray['stick']['id'],
            marmot_encode($news->getStick())
        );
        $this->assertEquals(
            $expectedArray['stick']['name'],
            ITopAble::STICK_CN[$news->getStick()]
        );
        $this->assertEquals(
            $expectedArray['stick']['type'],
            ITopAble::STICK_TAG_TYPE[$news->getStick()]
        );
        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($news->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            IEnableAble::STATUS_CN[$news->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            IEnableAble::STATUS_TAG_TYPE[$news->getStatus()]
        );

        $this->assertEquals(
            $expectedArray['bannerStatus']['id'],
            marmot_encode($news->getBannerStatus())
        );
        $this->assertEquals(
            $expectedArray['bannerStatus']['name'],
            News::BANNER_STATUS_CN[$news->getBannerStatus()]
        );
        $this->assertEquals(
            $expectedArray['bannerStatus']['type'],
            News::BANNER_STATUS_TAG_TYPE[$news->getBannerStatus()]
        );
        $this->assertEquals(
            $expectedArray['homePageShowStatus']['id'],
            marmot_encode($news->getHomePageShowStatus())
        );
        $this->assertEquals(
            $expectedArray['homePageShowStatus']['name'],
            News::HOME_PAGE_SHOW_STATUS_CN[$news->getHomePageShowStatus()]
        );
        $this->assertEquals(
            $expectedArray['homePageShowStatus']['type'],
            News::HOME_PAGE_SHOW_STATUS_TAG_TYPE[$news->getHomePageShowStatus()]
        );

        $this->assertEquals(
            $expectedArray['releaseTime'],
            $news->getReleaseTime()
        );
        $this->assertEquals(
            $expectedArray['releaseTimeFormat'],
            date('Y年m月d日', $news->getReleaseTime())
        );
        $this->assertEquals(
            $expectedArray['updateTime'],
            $news->getUpdateTime()
        );
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $news->getUpdateTime())
        );
                    
        $this->assertEquals(
            $expectedArray['statusTime'],
            $news->getStatusTime()
        );
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $news->getStatusTime())
        );
                    
        $this->assertEquals(
            $expectedArray['createTime'],
            $news->getCreateTime()
        );
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $news->getCreateTime())
        );
    }

    private function coverEquals($expectedArray, $news)
    {
        $cover = array();

        if (is_string($expectedArray['cover'])) {
            $cover = json_decode($expectedArray['cover'], true);
        }
        if (is_array($expectedArray['cover'])) {
            $cover = $expectedArray['cover'];
        }

        $this->assertEquals($cover, $news->getCover());
    }

    private function attachmentsEquals($expectedArray, $news)
    {
        $attachments = array();

        if (is_string($expectedArray['attachments'])) {
            $attachments = json_decode($expectedArray['attachments'], true);
        }
        if (is_array($expectedArray['attachments'])) {
            $attachments = $expectedArray['attachments'];
        }

        $this->assertEquals($attachments, $news->getAttachments());
    }

    private function bannerImageEquals($expectedArray, $news)
    {
        $bannerImage = array();

        if (is_string($expectedArray['bannerImage'])) {
            $bannerImage = json_decode($expectedArray['bannerImage'], true);
        }
        if (is_array($expectedArray['bannerImage'])) {
            $bannerImage = $expectedArray['bannerImage'];
        }

        $this->assertEquals($bannerImage, $news->getBannerImage());
    }
}
