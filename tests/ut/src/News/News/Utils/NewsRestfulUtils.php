<?php
namespace Sdk\News\News\Utils;

trait NewsRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $object
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $object->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['title'], $object->getTitle());
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $object->getDimension());
        if (isset($expectedArray['data']['attributes']['description'])) {
            $this->assertEquals($expectedArray['data']['attributes']['description'], $object->getDescription());
        }
        $this->assertEquals($expectedArray['data']['attributes']['releaseTime'], $object->getReleaseTime());
        $this->assertEquals($expectedArray['data']['attributes']['bannerImage'], $object->getBannerImage());
        $this->assertEquals($expectedArray['data']['attributes']['content'], $object->getContent());
        $this->assertEquals($expectedArray['data']['attributes']['stick'], $object->getStick());
        $this->assertEquals($expectedArray['data']['attributes']['attachments'], $object->getAttachments());
        $this->assertEquals($expectedArray['data']['attributes']['cover'], $object->getCover());
        $this->assertEquals($expectedArray['data']['attributes']['source'], $object->getSource());
        $this->assertEquals($expectedArray['data']['attributes']['bannerStatus'], $object->getBannerStatus());
        $this->assertEquals(
            $expectedArray['data']['attributes']['homePageShowStatus'],
            $object->getHomePageShowStatus()
        );

        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $object->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['statusTime'], $object->getStatusTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $object->getUpdateTime());
        }
        $this->assertEquals($expectedArray['data']['attributes']['status'], $object->getStatus());

        $this->compareCrew($expectedArray, $object);
        $this->compareNewsType($expectedArray, $object);
    }

    private function compareCrew(array $expectedArray, $object)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['crew']['data'])) {
                $this->assertEquals(
                    $relationships['crew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['crew']['data'][0]['id'],
                    $object->getCrew()->getId()
                );
            }
        }
    }

    private function compareNewsType(array $expectedArray, $object)
    {
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['newsType']['data'])) {
                $this->assertEquals(
                    $relationships['newsType']['data'][0]['type'],
                    'newsCategories'
                );
                $this->assertEquals(
                    $relationships['newsType']['data'][0]['id'],
                    $object->getNewsType()->getId()
                );
            }
        }
    }

    private function compareArrayAndObjectCommonUnAuditNews(
        array $expectedArray,
        $object
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $object);
        
        if (isset($expectedArray['data']['attributes']['applyStatus'])) {
            $this->assertEquals($expectedArray['data']['attributes']['applyStatus'], $object->getApplyStatus());
        }
        if (isset($expectedArray['data']['attributes']['operationType'])) {
            $this->assertEquals($expectedArray['data']['attributes']['operationType'], $object->getOperationType());
        }
        if (isset($expectedArray['data']['attributes']['applyInfoType'])) {
            $this->assertEquals($expectedArray['data']['attributes']['applyInfoType'], $object->getApplyInfoType());
        }
        if (isset($expectedArray['data']['attributes']['relationId'])) {
            $this->assertEquals($expectedArray['data']['attributes']['relationId'], $object->getRelationId());
        }

        $this->assertEquals($expectedArray['data']['attributes']['rejectReason'], $object->getRejectReason());

        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['applyCrew']['data'])) {
                $this->assertEquals(
                    $relationships['applyCrew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['applyCrew']['data'][0]['id'],
                    $object->getApplyCrew()->getId()
                );
            }
        }
    }
}
