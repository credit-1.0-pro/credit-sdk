<?php
namespace Sdk\News\News\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\News\News\Utils\NewsUtils;

class UnAuditNewsTranslatorTest extends TestCase
{
    use NewsUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditNewsTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Sdk\News\News\Model\NullUnAuditNews', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $unAuditNews = \Sdk\News\News\Utils\MockFactory::generateUnAuditNews(1);

        $expression = $this->translator->objectToArray($unAuditNews);
    
        $this->compareArrayAndObjectUnAuditedNews($expression, $unAuditNews);
    }

    public function testObjectToArrayFail()
    {
        $unAuditNews = null;

        $expression = $this->translator->objectToArray($unAuditNews);
        $this->assertEquals(array(), $expression);
    }
}
