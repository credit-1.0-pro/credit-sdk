<?php
namespace Sdk\News\News\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\News\News\Utils\NewsRestfulUtils;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Translator\NewsCategoryRestfulTranslator;

class UnAuditNewsRestfulTranslatorTest extends TestCase
{
    use NewsRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditNewsRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $unAuditNews = \Sdk\News\News\Utils\MockFactory::generateUnAuditNews(1);

        $expression['data']['id'] = $unAuditNews->getId();
        $expression['data']['attributes']['title'] = $unAuditNews->getTitle();
        $expression['data']['attributes']['description'] = $unAuditNews->getDescription();
        $expression['data']['attributes']['relationId'] = $unAuditNews->getRelationId();
        $expression['data']['attributes']['applyStatus'] = $unAuditNews->getApplyStatus();
        $expression['data']['attributes']['rejectReason'] = $unAuditNews->getRejectReason();
        $expression['data']['attributes']['operationType'] = $unAuditNews->getOperationType();
        $expression['data']['attributes']['applyInfoType'] = $unAuditNews->getApplyInfoType();
        $expression['data']['attributes']['dimension'] = $unAuditNews->getDimension();
        $expression['data']['attributes']['attachments'] = $unAuditNews->getAttachments();
        $expression['data']['attributes']['newsType'] = $unAuditNews->getNewsType();
        $expression['data']['attributes']['bannerImage'] = $unAuditNews->getBannerImage();
        $expression['data']['attributes']['content'] = $unAuditNews->getContent();
        $expression['data']['attributes']['stick'] = $unAuditNews->getStick();
        $expression['data']['attributes']['cover'] = $unAuditNews->getCover();
        $expression['data']['attributes']['source'] = $unAuditNews->getSource();
        $expression['data']['attributes']['bannerStatus'] = $unAuditNews->getBannerStatus();
        $expression['data']['attributes']['homePageShowStatus'] = $unAuditNews->getHomePageShowStatus();
        $expression['data']['attributes']['releaseTime'] = $unAuditNews->getReleaseTime();

        $expression['data']['attributes']['createTime'] = $unAuditNews->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $unAuditNews->getUpdateTime();
        $expression['data']['attributes']['status'] = $unAuditNews->getStatus();
        $expression['data']['attributes']['statusTime'] = $unAuditNews->getStatusTime();

        $unAuditNewsObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\News\News\Model\UnAuditNews', $unAuditNewsObject);
        $this->compareArrayAndObjectCommonUnAuditNews($expression, $unAuditNewsObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $unAuditNews = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\News\News\Model\NullUnAuditNews', $unAuditNews);
    }

    public function testObjectToArray()
    {
        $unAuditNews = \Sdk\News\News\Utils\MockFactory::generateUnAuditNews(1);

        $expression = $this->translator->objectToArray($unAuditNews);
     
        $this->compareArrayAndObjectCommonUnAuditNews($expression, $unAuditNews);
    }

    public function testObjectToArrayFail()
    {
        $unAuditNews = null;

        $expression = $this->translator->objectToArray($unAuditNews);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        $translator = $this->getMockBuilder(UnAuditNewsRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getCrewRestfulTranslator',
                        'getUserGroupRestfulTranslator',
                        'getNewsCategoryRestfulTranslator',
                    ])
                    ->getMock();

        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'publishCrew'=>['data'=>'mockPublishCrew'],
            'applyCrew'=>['data'=>'mockApplyCrew'],
            'applyUserGroup'=>['data'=>'mockApplyUserGroup'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup'],
            'crew'=>['data'=>'mockCrew'],
            'newsType'=>['data'=>'mockNewsType'],
        ];

        $publishCrewInfo = ['mockPublishCrewInfo'];
        $crewInfo = ['mockCrewInfo'];
        $relationInfo = ['mockRelationInfo'];
        $applyCrewInfo = ['mockApplyCrewInfo'];
        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];
        $applyUserGroupInfo = ['mockApplyUserGroupInfo'];
        $newsTypeInfo = ['mockNewsTypeInfo'];

        $crew = new Crew();
        $applyCrew = new Crew();
        $publishCrew = new Crew();
        $publishUserGroup = new UserGroup();
        $applyUserGroup = new UserGroup();
        $newsType = new NewsCategory();

        //预言
        $translator->expects($this->exactly(2))
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用5次, 依次入参 department, userGroup
        //依次返回 $departmentInfo 和 $userGroupInfo
        $translator->expects($this->exactly(6))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['publishUserGroup']['data']],
                 [$relationships['newsType']['data']],
                 [$relationships['applyCrew']['data']],
                 [$relationships['applyUserGroup']['data']],
                 [$relationships['publishCrew']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $crewInfo,
                $publishUserGroupInfo,
                $newsTypeInfo,
                $applyCrewInfo,
                $applyUserGroupInfo,
                $publishCrewInfo
            ));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($publishUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishUserGroup);

        $userGroupRestfulTranslator->arrayToObject(Argument::exact($applyUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyUserGroup);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $crewRestfulTranslator->arrayToObject(Argument::exact($publishCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishCrew);

        $crewRestfulTranslator->arrayToObject(Argument::exact($applyCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyCrew);
        
        $newsCategoryRestfulTranslator = $this->prophesize(NewsCategoryRestfulTranslator::class);
        $newsCategoryRestfulTranslator->arrayToObject(Argument::exact($newsTypeInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($newsType);
        //绑定
        $translator->expects($this->exactly(2))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(3))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getNewsCategoryRestfulTranslator')
            ->willReturn($newsCategoryRestfulTranslator->reveal());
       
        //揭示
        $unAuditNews = $translator->arrayToObject($expression);
         
        $this->assertInstanceof('Sdk\News\News\Model\UnAuditNews', $unAuditNews);
        $this->assertEquals($crew, $unAuditNews->getCrew());
        $this->assertEquals($publishCrew, $unAuditNews->getPublishCrew());
        $this->assertEquals($applyCrew, $unAuditNews->getApplyCrew());
        $this->assertEquals($publishUserGroup, $unAuditNews->getPublishUserGroup());
        $this->assertEquals($applyUserGroup, $unAuditNews->getApplyUserGroup());
        $this->assertEquals($newsType, $unAuditNews->getNewsType());
    }
}
