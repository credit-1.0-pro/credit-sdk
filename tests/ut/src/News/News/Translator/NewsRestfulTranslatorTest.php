<?php
namespace Sdk\News\News\Translator;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\News\News\Utils\NewsRestfulUtils;

use Sdk\User\Crew\Model\Crew;
use Sdk\User\Crew\Translator\CrewRestfulTranslator;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\Translator\NewsCategoryRestfulTranslator;

class NewsRestfulTranslatorTest extends TestCase
{
    use NewsRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new NewsRestfulTranslator();

        Core::$container->set('crew', new Crew());
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $translator = new MockNewsRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Translator\UserGroupRestfulTranslator',
            $translator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $translator = new MockNewsRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\User\Crew\Translator\CrewRestfulTranslator',
            $translator->getCrewRestfulTranslator()
        );
    }

    public function testGetNewsCategoryRestfulTranslator()
    {
        $translator = new MockNewsRestfulTranslator();
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\Translator\NewsCategoryRestfulTranslator',
            $translator->getNewsCategoryRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);

        $expression['data']['id'] = $news->getId();
        $expression['data']['attributes']['title'] = $news->getTitle();
        $expression['data']['attributes']['description'] = $news->getDescription();
        $expression['data']['attributes']['dimension'] = $news->getDimension();
        $expression['data']['attributes']['attachments'] = $news->getAttachments();
        $expression['data']['attributes']['bannerImage'] = $news->getBannerImage();
        $expression['data']['attributes']['content'] = $news->getContent();
        $expression['data']['attributes']['stick'] = $news->getStick();
        $expression['data']['attributes']['cover'] = $news->getCover();
        $expression['data']['attributes']['source'] = $news->getSource();
        $expression['data']['attributes']['bannerStatus'] = $news->getBannerStatus();
        $expression['data']['attributes']['homePageShowStatus'] = $news->getHomePageShowStatus();
        $expression['data']['attributes']['releaseTime'] = $news->getReleaseTime();

        $expression['data']['attributes']['createTime'] = $news->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $news->getUpdateTime();
        $expression['data']['attributes']['status'] = $news->getStatus();
        $expression['data']['attributes']['statusTime'] = $news->getStatusTime();

        $newsObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\News\News\Model\News', $newsObject);
        $this->compareArrayAndObjectCommon($expression, $newsObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $news = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\News\News\Model\NullNews', $news);
    }

    public function testObjectToArray()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);

        $expression = $this->translator->objectToArray($news);

        $this->compareArrayAndObjectCommon($expression, $news);
    }

    public function testObjectToArrayFail()
    {
        $news = null;

        $expression = $this->translator->objectToArray($news);
        $this->assertEquals(array(), $expression);
    }

    public function testArrayToObjectWithIncluded()
    {
        $translator = $this->getMockBuilder(MockNewsRestfulTranslator::class)
                    ->setMethods([
                        'relationship',
                        'changeArrayFormat',
                        'getCrewRestfulTranslator',
                        'getUserGroupRestfulTranslator',
                        'getNewsCategoryRestfulTranslator'
                    ])
                    ->getMock();


        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup'],
            'newsType'=>['data'=>'mockNewsType']
        ];

        $crewInfo = ['mockCrewInfo'];
        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];
        $newsTypeInfo = ['mockNewsTypeInfo'];

        $crew = new Crew();
        $publishUserGroup = new UserGroup();
        $newsType = new NewsCategory();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        $translator->expects($this->exactly(3))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['publishUserGroup']['data']],
                 [$relationships['newsType']['data']]
             )
            ->will($this->onConsecutiveCalls($crewInfo, $publishUserGroupInfo, $newsTypeInfo));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($publishUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishUserGroup);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $newsCategoryRestfulTranslator = $this->prophesize(NewsCategoryRestfulTranslator::class);
        $newsCategoryRestfulTranslator->arrayToObject(Argument::exact($newsTypeInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($newsType);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getNewsCategoryRestfulTranslator')
            ->willReturn($newsCategoryRestfulTranslator->reveal());

        //揭示
        $news = $translator->arrayToObject($expression);
        $this->assertInstanceof('Sdk\News\News\Model\News', $news);
        $this->assertEquals($crew, $news->getCrew());
        $this->assertEquals($publishUserGroup, $news->getUserGroup());
        $this->assertEquals($newsType, $news->getNewsType());
    }
}
