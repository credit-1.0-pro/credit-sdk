<?php
namespace Sdk\News\News\Adapter\UnAuditNews;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\News\News\Model\UnAuditNews;
use Sdk\News\News\Model\NullUnAuditNews;
use Sdk\News\News\Translator\UnAuditNewsRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UnAuditNewsRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockUnAuditNewsRestfulAdapter::class)
                           ->setMethods([
                               'commonMapErrors',
                               'getTranslator',
                               'isSuccess',
                               'patch',
                               'translateToObject'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new UnAuditNewsRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsIUnAuditNewsAdapter()
    {
        $adapter = new UnAuditNewsRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\News\News\Adapter\UnAuditNews\IUnAuditNewsAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockUnAuditNewsRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\News\News\Translator\UnAuditNewsRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockUnAuditNewsRestfulAdapter();
        $this->assertEquals('news/unAuditedNews', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockUnAuditNewsRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\News\News\Model\NullUnAuditNews',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockUnAuditNewsRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UN_AUDIT_NEWS_LIST',
                UnAuditNewsRestfulAdapter::SCENARIOS['UN_AUDIT_NEWS_LIST']
            ],
            [
                'UN_AUDIT_NEWS_FETCH_ONE',
                UnAuditNewsRestfulAdapter::SCENARIOS['UN_AUDIT_NEWS_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    /**
     * 为UnAuditNewsRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$news,$keys,$newsArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareUnAuditNewsTranslator(
        UnAuditNews $unAuditedNews,
        array $keys,
        array $newsArray
    ) {
        $translator = $this->prophesize(UnAuditNewsRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($unAuditedNews),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($newsArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(UnAuditNews $unAuditedNews)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($unAuditedNews);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }

    public function testAdd()
    {
        $result = $this->adapter->add(new UnAuditNews());
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $unAuditedNews = \Sdk\News\News\Utils\MockFactory::generateUnAuditNews(1);
        $newsArray = array();

        $this->prepareUnAuditNewsTranslator($unAuditedNews, array(
            'title',
            'source',
            'cover',
            'attachments',
            'content',
            'newsType',
            'dimension',
            'status',
            'stick',
            'bannerStatus',
            'bannerImage',
            'homePageShowStatus',
            'releaseTime',
            'crew'
        ), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/unAuditedNews/'.$unAuditedNews->getId().'/resubmit', $newsArray);

        $this->success($unAuditedNews);
        $result = $this->adapter->edit($unAuditedNews);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $unAuditedNews = \Sdk\News\News\Utils\MockFactory::generateUnAuditNews(1);
        $newsArray = array();

        $this->prepareUnAuditNewsTranslator($unAuditedNews, array(
            'title',
            'source',
            'cover',
            'attachments',
            'content',
            'newsType',
            'dimension',
            'status',
            'stick',
            'bannerStatus',
            'bannerImage',
            'homePageShowStatus',
            'releaseTime',
            'crew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/unAuditedNews/'.$unAuditedNews->getId().'/resubmit', $newsArray);
        
            $this->failure($unAuditedNews);
            $result = $this->adapter->edit($unAuditedNews);
            $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [
            101 => array(
                'title' => TITLE_FORMAT_ERROR,
                'source' => SOURCE_FORMAT_ERROR,
                'cover' => IMAGE_FORMAT_ERROR,
                'content' => NEWS_CONTENT_FORMAT_ERROR,
                'attachments' => ATTACHMENT_FORMAT_ERROR,
                'status' => STATUS_FORMAT_ERROR,
                'stick' => STICK_FORMAT_ERROR,
                'newsType' => NEWS_TYPE_FORMAT_ERROR,
                'dimension' => DIMENSION_FORMAT_ERROR,
                'homePageShowStatus' => HOME_PAGE_SHOW_STATUS_FORMAT_ERROR,
                'bannerStatus' => BANNER_STATUS_FORMAT_ERROR,
                'bannerImage' => BANNER_IMAGE_FORMAT_ERROR,
                'applyCrewId' => APPLY_CREW_FORMAT_ERROR,
                'crewId' => CREW_ID_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
                'releaseTime' => RELEASE_TIME_FORMAT_ERROR
            ),
            102 => array(
                'status' => STATUS_CAN_NOT_MODIFY,
                'stick' => STATUS_CAN_NOT_MODIFY,
                'applyStatus' => APPLY_STATUE_CAN_NOT_MODIFY,
            ),
            100 => array(
                'crew' => PARAMETER_IS_EMPTY,
                'publishCrew' => PARAMETER_IS_EMPTY,
                'newsType' => NEWS_TYPE_NOT_EXIT,
            )
        ];
        
        $result = $this->adapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    public function testApproveSuccess()
    {
        $unAuditedNews = \Sdk\News\News\Utils\MockFactory::generateUnAuditNews(1);
        $newsArray = array();

        $this->prepareUnAuditNewsTranslator($unAuditedNews, array('applyCrew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/unAuditedNews/'.$unAuditedNews->getId().'/approve', $newsArray);

        $this->success($unAuditedNews);
        $result = $this->adapter->approve($unAuditedNews);
        $this->assertTrue($result);
    }

    public function testApproveFailure()
    {
        $unAuditedNews = \Sdk\News\News\Utils\MockFactory::generateUnAuditNews(1);
        $newsArray = array();

        $this->prepareUnAuditNewsTranslator($unAuditedNews, array('applyCrew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/unAuditedNews/'.$unAuditedNews->getId().'/approve', $newsArray);
        
            $this->failure($unAuditedNews);
            $result = $this->adapter->approve($unAuditedNews);
            $this->assertFalse($result);
    }

    public function testRejectSuccess()
    {
        $unAuditedNews = \Sdk\News\News\Utils\MockFactory::generateUnAuditNews(1);
        $newsArray = array();

        $this->prepareUnAuditNewsTranslator($unAuditedNews, array('rejectReason','applyCrew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/unAuditedNews/'.$unAuditedNews->getId().'/reject', $newsArray);

        $this->success($unAuditedNews);
        $result = $this->adapter->reject($unAuditedNews);
        $this->assertTrue($result);
    }

    public function testRejectFailure()
    {
        $unAuditedNews = \Sdk\News\News\Utils\MockFactory::generateUnAuditNews(1);
        $newsArray = array();

        $this->prepareUnAuditNewsTranslator($unAuditedNews, array('rejectReason','applyCrew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/unAuditedNews/'.$unAuditedNews->getId().'/reject', $newsArray);
        
            $this->failure($unAuditedNews);
            $result = $this->adapter->reject($unAuditedNews);
            $this->assertFalse($result);
    }
}
