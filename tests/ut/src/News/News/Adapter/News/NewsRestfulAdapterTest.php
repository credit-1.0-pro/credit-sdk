<?php
namespace Sdk\News\News\Adapter\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\News\News\Model\News;
use Sdk\News\News\Model\NullNews;
use Sdk\News\News\Translator\NewsRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class NewsRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockNewsRestfulAdapter::class)
                           ->setMethods([
                               'commonMapErrors',
                               'getTranslator',
                               'post',
                               'isSuccess',
                               'patch',
                               'translateToObject'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $adapter = new NewsRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $adapter
        );
    }

    public function testImplementsINewsAdapter()
    {
        $adapter = new NewsRestfulAdapter();
        $this->assertInstanceOf(
            'Sdk\News\News\Adapter\News\INewsAdapter',
            $adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockNewsRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
        $this->assertInstanceOf(
            'Sdk\News\News\Translator\NewsRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $adapter = new MockNewsRestfulAdapter();
        $this->assertEquals('news', $adapter->getResource());
    }

    public function testGetNullObject()
    {
        $adapter = new MockNewsRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
        $this->assertInstanceOf(
            'Sdk\News\News\Model\NullNews',
            $adapter->getNullObject()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $adapter = new MockNewsRestfulAdapter();
        $adapter->scenario($expect);
        $this->assertEquals($actual, $adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'NEWS_LIST',
                NewsRestfulAdapter::SCENARIOS['NEWS_LIST']
            ],
            [
                'NEWS_FETCH_ONE',
                NewsRestfulAdapter::SCENARIOS['NEWS_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    /**
     * 为NewsRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$news,$keys,$newsArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareNewsTranslator(
        News $news,
        array $keys,
        array $newsArray
    ) {
        $translator = $this->prophesize(NewsRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($news),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($newsArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(News $news)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($news);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'releaseTime',
                'crew'),
            $newsArray
        );

        $this->adapter
            ->method('post')
            ->with('news', $newsArray);

        $this->success($news);
        $result = $this->adapter->add($news);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'releaseTime',
                'crew'),
            $newsArray
        );

        $this->adapter
            ->method('post')
            ->with('news', $newsArray);
        
            $this->failure($news);
            $result = $this->adapter->add($news);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator($news, array(
            'title',
            'source',
            'cover',
            'attachments',
            'content',
            'newsType',
            'dimension',
            'status',
            'stick',
            'bannerStatus',
            'bannerImage',
            'homePageShowStatus',
            'releaseTime',
            'crew'
        ), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/'.$news->getId(), $newsArray);

        $this->success($news);
        $result = $this->adapter->edit($news);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator($news, array(
            'title',
            'source',
            'cover',
            'attachments',
            'content',
            'newsType',
            'dimension',
            'status',
            'stick',
            'bannerStatus',
            'bannerImage',
            'homePageShowStatus',
            'releaseTime',
            'crew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/'.$news->getId(), $newsArray);
        
            $this->failure($news);
            $result = $this->adapter->edit($news);
            $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->once())->method('commonMapErrors')->willReturn($commonMapErrors);

        $mapError = [
            101 => array(
                'stick' => STICK_FORMAT_ERROR,
                'status' => STATUS_FORMAT_ERROR,
                'title' => TITLE_FORMAT_ERROR,
                'source' => SOURCE_FORMAT_ERROR,
                'cover' => IMAGE_FORMAT_ERROR,
                'attachments' => ATTACHMENT_FORMAT_ERROR,
                'content' => NEWS_CONTENT_FORMAT_ERROR,
                'newsType' => NEWS_TYPE_FORMAT_ERROR,
                'dimension' => DIMENSION_FORMAT_ERROR,
                'homePageShowStatus' => HOME_PAGE_SHOW_STATUS_FORMAT_ERROR,
                'bannerStatus' => BANNER_STATUS_FORMAT_ERROR,
                'bannerImage' => BANNER_IMAGE_FORMAT_ERROR,
                'applyCrewId' => APPLY_CREW_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
                'crewId' => CREW_ID_FORMAT_ERROR,
                'releaseTime' => RELEASE_TIME_FORMAT_ERROR
            ),
            100 => array(
                'crew' => PARAMETER_IS_EMPTY,
                'publishCrew' => PARAMETER_IS_EMPTY,
                'newsType' => NEWS_TYPE_NOT_EXIT,
            ),
            102 => array(
                'stick' => STATUS_CAN_NOT_MODIFY,
                'status' => STATUS_CAN_NOT_MODIFY,
                'applyStatus' => APPLY_STATUE_CAN_NOT_MODIFY,
            )
        ];
        
        $result = $this->adapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    public function testEnableSuccess()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator($news, array('crew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/'.$news->getId().'/enable', $newsArray);

        $this->success($news);
        $result = $this->adapter->enable($news);
        $this->assertTrue($result);
    }

    public function testEnableFailure()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator($news, array('crew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/'.$news->getId().'/enable', $newsArray);
        
            $this->failure($news);
            $result = $this->adapter->enable($news);
            $this->assertFalse($result);
    }

    public function testDisableSuccess()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator($news, array('crew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/'.$news->getId().'/disable', $newsArray);

        $this->success($news);
        $result = $this->adapter->disable($news);
        $this->assertTrue($result);
    }

    public function testDisableFailure()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator($news, array('crew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/'.$news->getId().'/disable', $newsArray);
        
            $this->failure($news);
            $result = $this->adapter->disable($news);
            $this->assertFalse($result);
    }

    public function testTopSuccess()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator($news, array('crew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/'.$news->getId().'/top', $newsArray);

        $this->success($news);
        $result = $this->adapter->top($news);
        $this->assertTrue($result);
    }

    public function testTopFailure()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator($news, array('crew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/'.$news->getId().'/top', $newsArray);
        
            $this->failure($news);
            $result = $this->adapter->top($news);
            $this->assertFalse($result);
    }

    public function testCancelTopSuccess()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator($news, array('crew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/'.$news->getId().'/cancelTop', $newsArray);

        $this->success($news);
        $result = $this->adapter->cancelTop($news);
        $this->assertTrue($result);
    }

    public function testCancelTopFailure()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator($news, array('crew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/'.$news->getId().'/cancelTop', $newsArray);
        
            $this->failure($news);
            $result = $this->adapter->cancelTop($news);
            $this->assertFalse($result);
    }

    public function testMoveSuccess()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator($news, array('crew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/'.$news->getId().'/move/'.$news->getNewsType()->getId(), $newsArray);

        $this->success($news);
        $result = $this->adapter->move($news);
        $this->assertTrue($result);
    }

    public function testMoveFailure()
    {
        $news = \Sdk\News\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator($news, array('crew'), $newsArray);

        $this->adapter
            ->method('patch')
            ->with('news/'.$news->getId().'/move/'.$news->getNewsType()->getId(), $newsArray);
        
            $this->failure($news);
            $result = $this->adapter->move($news);
            $this->assertFalse($result);
    }
}
