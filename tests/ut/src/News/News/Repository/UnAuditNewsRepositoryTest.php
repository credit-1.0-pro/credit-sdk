<?php
namespace Sdk\News\News\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\News\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter;

class UnAuditNewsRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UnAuditNewsRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIUnAuditNewsAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\News\News\Adapter\UnAuditNews\IUnAuditNewsAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\News\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\News\News\Adapter\UnAuditNews\UnAuditNewsMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(UnAuditNewsRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
