<?php
namespace Sdk\News\News\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullNewsTest extends TestCase
{
    private $nullNews;

    public function setUp()
    {
        $this->nullNews = NullNews::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullNews);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsNews()
    {
        $this->assertInstanceOf(
            'Sdk\News\News\Model\News',
            $this->nullNews
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullNews
        );
    }

    public function testResourceNotExist()
    {
        $nullNews = new MockNullNews();

        $result = $nullNews->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testMove()
    {
        $nullNews = new MockNullNews();

        $result = $nullNews->move();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
