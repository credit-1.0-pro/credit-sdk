<?php
namespace Sdk\News\News\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Common\Model\IApplyAble;

class UnAuditNewsTest extends TestCase
{
    private $unAuditNews;

    public function setUp()
    {
        $this->unAuditNews = new MockUnAuditNews();
    }

    public function tearDown()
    {
        unset($this->unAuditNews);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals('', $this->unAuditNews->getRejectReason());
        $this->assertEquals(0, $this->unAuditNews->getApplyInfoType());
        $this->assertEquals(0, $this->unAuditNews->getRelationId());
        $this->assertInstanceOf('Sdk\User\Crew\Model\Crew', $this->unAuditNews->getPublishCrew());
        $this->assertInstanceOf('Sdk\User\Crew\Model\Crew', $this->unAuditNews->getApplyCrew());
        $this->assertInstanceOf(
            'Sdk\UserGroup\UserGroup\Model\UserGroup',
            $this->unAuditNews->getPublishUserGroup()
        );
        $this->assertInstanceOf('Sdk\UserGroup\UserGroup\Model\UserGroup', $this->unAuditNews->getApplyUserGroup());
        $this->assertEquals(IApplyAble::OPERATION_TYPE['NULL'], $this->unAuditNews->getOperationType());
        $this->assertEquals(IApplyAble::APPLY_STATUS['PENDING'], $this->unAuditNews->getApplyStatus());
    }

    public function testGetUnAuditNewsRepository()
    {
        $this->assertInstanceOf(
            'Sdk\News\News\Repository\UnAuditNewsRepository',
            $this->unAuditNews->getUnAuditNewsRepository()
        );
    }

    public function testGetIApplyAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\News\News\Repository\UnAuditNewsRepository',
            $this->unAuditNews->getIApplyAbleAdapter()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\News\News\Repository\UnAuditNewsRepository',
            $this->unAuditNews->getIOperateAbleAdapter()
        );
    }
}
