<?php
namespace Sdk\News\News\Command;

trait NewsCommandDataTrait
{
    public function getRequestCommonData() : array
    {
        $data = array(
            'title' => '多地出台评价标准',
            'source' => '北京发改委',
            "cover"=> array('name' => '封面名称', 'identify' => '封面地址.jpg'),
            "attachments"=> array(
                array('name' => 'name', 'identify' => 'identify.doc'),
                array('name' => 'name', 'identify' => 'identify.doc'),
                array('name' => 'name', 'identify' => 'identify.doc')
            ),
            "content"=> "内容",
            "newsType"=> 1,
            "dimension"=> 1,
            "status"=>0,
            "stick"=>0,
            "bannerStatus"=>0,
            "bannerImage"=>array('name' => '轮播图图片名称', 'identify' => '轮播图图片地址.jpg'),
            "homePageShowStatus"=>0,
            "releaseTime"=>1654675964,
            "crew"=>1,
        );

        return $data;
    }
}
