<?php
namespace Sdk\News\News\Command\UnAuditNews;

use PHPUnit\Framework\TestCase;

use Sdk\News\News\Command\NewsCommandDataTrait;

class EditUnAuditNewsCommandTest extends TestCase
{

    use NewsCommandDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = $this->getRequestCommonData();

        $this->command = new EditUnAuditNewsCommand(
            $this->fakerData['title'],
            $this->fakerData['source'],
            $this->fakerData['content'],
            $this->fakerData['newsType'],
            $this->fakerData['dimension'],
            $this->fakerData['status'],
            $this->fakerData['stick'],
            $this->fakerData['bannerStatus'],
            $this->fakerData['homePageShowStatus'],
            $this->fakerData['releaseTime'],
            $this->fakerData['cover'],
            $this->fakerData['attachments'],
            $this->fakerData['bannerImage'],
            $faker->randomNumber()
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
