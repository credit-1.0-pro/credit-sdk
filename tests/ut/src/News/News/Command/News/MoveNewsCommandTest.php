<?php
namespace Sdk\News\News\Command\News;

use PHPUnit\Framework\TestCase;

use Sdk\News\News\Command\NewsCommandDataTrait;

class MoveNewsCommandTest extends TestCase
{
    use NewsCommandDataTrait;

    private $stub;

    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = $this->getRequestCommonData();

        $this->stub = new MoveNewsCommand(
            $this->fakerData['newsType'],
            $faker->randomNumber()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
