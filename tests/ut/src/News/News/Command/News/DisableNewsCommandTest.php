<?php
namespace Sdk\News\News\Command\News;

use PHPUnit\Framework\TestCase;

use Sdk\News\News\Command\NewsCommandDataTrait;

class DisableNewsCommandTest extends TestCase
{
    use NewsCommandDataTrait;

    private $stub;

    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = $this->getRequestCommonData();

        $this->stub = new DisableNewsCommand(
            $faker->randomNumber()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
