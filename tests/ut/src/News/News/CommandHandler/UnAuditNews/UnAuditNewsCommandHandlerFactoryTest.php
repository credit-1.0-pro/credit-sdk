<?php
namespace Sdk\News\News\CommandHandler\UnAuditNews;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use Sdk\News\News\Command\UnAuditNews\EditUnAuditNewsCommand;
use Sdk\News\News\Command\UnAuditNews\RejectUnAuditNewsCommand;
use Sdk\News\News\Command\UnAuditNews\ApproveUnAuditNewsCommand;

class UnAuditNewsCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new UnAuditNewsCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testEditUnAuditNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditUnAuditNewsCommand(
                $this->faker->title(),
                $this->faker->text(),
                $this->faker->text(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(2),
                $this->faker->randomNumber(1),
                $this->faker->randomNumber(),
                array($this->faker->md5()),
                array($this->faker->title()),
                array($this->faker->title()),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\News\News\CommandHandler\UnAuditNews\EditUnAuditNewsCommandHandler',
            $commandHandler
        );
    }

    public function testRejectUnAuditNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RejectUnAuditNewsCommand(
                $this->faker->title(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\News\News\CommandHandler\UnAuditNews\RejectUnAuditNewsCommandHandler',
            $commandHandler
        );
    }

    public function testApproveUnAuditNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ApproveUnAuditNewsCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\News\News\CommandHandler\UnAuditNews\ApproveUnAuditNewsCommandHandler',
            $commandHandler
        );
    }
}
