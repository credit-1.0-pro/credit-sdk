<?php
namespace Sdk\News\News\CommandHandler\UnAuditNews;

use PHPUnit\Framework\TestCase;

class RejectUnAuditNewsCommandHandlerTest extends TestCase
{
    private $rejectStub;

    public function setUp()
    {
        $this->rejectStub = $this->getMockBuilder(MockRejectUnAuditNewsCommandHandler::class)
            ->setMethods(['fetchUnAuditNews'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\RejectCommandHandler',
            $this->rejectStub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $unAuditNews = \Sdk\News\News\Utils\MockFactory::generateUnAuditNews($id);

        $this->rejectStub->expects($this->exactly(1))
                    ->method('fetchUnAuditNews')
                    ->willReturn($unAuditNews);

        $result = $this->rejectStub->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditNews);
    }
}
