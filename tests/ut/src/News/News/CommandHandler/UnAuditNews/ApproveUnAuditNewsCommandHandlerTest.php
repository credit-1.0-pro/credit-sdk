<?php
namespace Sdk\News\News\CommandHandler\UnAuditNews;

use PHPUnit\Framework\TestCase;

class ApproveUnAuditNewsCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveUnAuditNewsCommandHandler::class)
            ->setMethods(['fetchUnAuditNews'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\ApproveCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $unAuditNews = \Sdk\News\News\Utils\MockFactory::generateUnAuditNews($id);

        $this->stub->expects($this->exactly(1))
                    ->method('fetchUnAuditNews')
                    ->willReturn($unAuditNews);

        $result = $this->stub->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditNews);
    }
}
