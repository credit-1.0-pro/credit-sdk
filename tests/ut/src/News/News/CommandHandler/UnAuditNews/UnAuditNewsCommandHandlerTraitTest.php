<?php
namespace Sdk\News\News\CommandHandler\UnAuditNews;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\News\News\Repository\UnAuditNewsRepository;

use Sdk\News\NewsCategory\Repository\NewsCategoryRepository;

class UnAuditNewsCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockUnAuditNewsCommandHandlerTrait::class)->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetRepository()
    {
        $trait = new MockUnAuditNewsCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\News\News\Repository\UnAuditNewsRepository',
            $trait->publicGetRepository()
        );
    }

    public function testFetchUnAuditNews()
    {
        $trait = $this->getMockBuilder(MockUnAuditNewsCommandHandlerTrait::class)
                 ->setMethods(['getRepository'])->getMock();

        $id = 1;

        $news = \Sdk\News\News\Utils\MockFactory::generateUnAuditNews($id);

        $repository = $this->prophesize(UnAuditNewsRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($news);

        $trait->expects($this->exactly(1))
                         ->method('getRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchUnAuditNews($id);

        $this->assertEquals($result, $news);
    }

    public function testGetNewsCategoryRepository()
    {
        $trait = new MockUnAuditNewsCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\Repository\NewsCategoryRepository',
            $trait->publicGetNewsCategoryRepository()
        );
    }

    public function testFetchNewsCategory()
    {
        $stub = $this->getMockBuilder(MockUnAuditNewsCommandHandlerTrait::class)
                 ->setMethods(['getNewsCategoryRepository'])->getMock();

        $id = 1;
        $newsCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory($id);

        $repository = $this->prophesize(NewsCategoryRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($newsCategory);

        $stub->expects($this->exactly(1))
                         ->method('getNewsCategoryRepository')
                         ->willReturn($repository->reveal());

        $result = $stub->publicFetchNewsCategory($id);

        $this->assertEquals($result, $newsCategory);
    }
}
