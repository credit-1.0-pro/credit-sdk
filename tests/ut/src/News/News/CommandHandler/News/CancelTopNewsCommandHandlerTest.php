<?php
namespace Sdk\News\News\CommandHandler\News;

use PHPUnit\Framework\TestCase;

class CancelTopNewsCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockCancelTopNewsCommandHandler::class)
            ->setMethods(['fetchNews'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\CancelTopCommandHandler',
            $this->stub
        );
    }

    public function testFetchITopObject()
    {
        $id = 1;
        $news = \Sdk\News\News\Utils\MockFactory::generateNews($id);

        $this->stub->expects($this->exactly(1))
                    ->method('fetchNews')
                    ->willReturn($news);

        $result = $this->stub->fetchITopObject($id);

        $this->assertEquals($result, $news);
    }
}
