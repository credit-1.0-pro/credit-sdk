<?php
namespace Sdk\News\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\News\News\Model\News;
use Sdk\News\News\Command\News\AddNewsCommand;
use Sdk\News\News\Command\NewsCommandDataTrait;

class AddNewsCommandHandlerTest extends TestCase
{
    use NewsCommandDataTrait;

    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddNewsCommandHandler::class)
            ->setMethods(['getNews', 'newsExecuteAction'])
            ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecuteSuccess()
    {
        $this->fakerData = $this->getRequestCommonData();
        
        $command = new AddNewsCommand(
            $this->fakerData['title'],
            $this->fakerData['source'],
            $this->fakerData['content'],
            $this->fakerData['newsType'],
            $this->fakerData['dimension'],
            $this->fakerData['status'],
            $this->fakerData['stick'],
            $this->fakerData['bannerStatus'],
            $this->fakerData['homePageShowStatus'],
            $this->fakerData['releaseTime'],
            $this->fakerData['cover'],
            $this->fakerData['attachments'],
            $this->fakerData['bannerImage']
        );

        $news = $this->prophesize(News::class);
        $news->add()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
            ->method('newsExecuteAction')
            ->willReturn($news->reveal());

        $this->commandHandler->expects($this->once())
            ->method('getNews')
            ->willReturn($news->reveal());

        $result = $this->commandHandler->execute($command);

        $this->assertTrue($result);
    }
}
