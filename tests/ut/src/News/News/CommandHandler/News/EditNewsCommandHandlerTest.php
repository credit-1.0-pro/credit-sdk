<?php
namespace Sdk\News\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Sdk\News\News\Model\News;
use Sdk\News\News\Command\News\EditNewsCommand;
use Sdk\News\News\Command\NewsCommandDataTrait;

class EditNewsCommandHandlerTest extends TestCase
{
    use NewsCommandDataTrait;

    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditNewsCommandHandler::class)
            ->setMethods(['fetchNews', 'newsExecuteAction'])
            ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->fakerData = $this->getRequestCommonData();
        $id = 1;
        
        $command = new EditNewsCommand(
            $this->fakerData['title'],
            $this->fakerData['source'],
            $this->fakerData['content'],
            $this->fakerData['newsType'],
            $this->fakerData['dimension'],
            $this->fakerData['status'],
            $this->fakerData['stick'],
            $this->fakerData['bannerStatus'],
            $this->fakerData['homePageShowStatus'],
            $this->fakerData['releaseTime'],
            $this->fakerData['cover'],
            $this->fakerData['attachments'],
            $this->fakerData['bannerImage'],
            $id
        );

        $news = $this->prophesize(News::class);
        $news->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
            ->method('newsExecuteAction')
            ->willReturn($news->reveal());

        $this->commandHandler->expects($this->once())
            ->method('fetchNews')
            ->willReturn($news->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
