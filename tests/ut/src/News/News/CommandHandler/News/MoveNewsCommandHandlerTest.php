<?php
namespace Sdk\News\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Sdk\News\News\Model\News;
use Sdk\News\News\Command\News\MoveNewsCommand;
use Sdk\News\News\Command\NewsCommandDataTrait;

class MoveNewsCommandHandlerTest extends TestCase
{
    use NewsCommandDataTrait;

    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(MoveNewsCommandHandler::class)
            ->setMethods(['fetchNewsCategory', 'fetchNews'])
            ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->fakerData = $this->getRequestCommonData();
        $id = 1;
        
        $command = new MoveNewsCommand(
            $this->fakerData['newsType'],
            $id
        );

        $newsType = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory(1);
        $this->commandHandler->expects($this->once())->method('fetchNewsCategory')->willReturn($newsType);

        $news = $this->prophesize(News::class);
        $news->setNewsType(Argument::exact($newsType))->shouldBeCalledTimes(1);
        $news->move()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())->method('fetchNews')->willReturn($news->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
