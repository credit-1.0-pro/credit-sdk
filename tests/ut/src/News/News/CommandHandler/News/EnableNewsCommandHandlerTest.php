<?php
namespace Sdk\News\News\CommandHandler\News;

use PHPUnit\Framework\TestCase;

class EnableNewsCommandHandlerTest extends TestCase
{
    private $enableStub;

    public function setUp()
    {
        $this->enableStub = $this->getMockBuilder(MockEnableNewsCommandHandler::class)
            ->setMethods(['fetchNews'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\EnableCommandHandler',
            $this->enableStub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $news = \Sdk\News\News\Utils\MockFactory::generateNews($id);

        $this->enableStub->expects($this->exactly(1))
                    ->method('fetchNews')
                    ->willReturn($news);

        $result = $this->enableStub->fetchIEnableObject($id);

        $this->assertEquals($result, $news);
    }
}
