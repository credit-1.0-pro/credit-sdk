<?php
namespace Sdk\News\News\CommandHandler\News;

use PHPUnit\Framework\TestCase;

class DisableNewsCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDisableNewsCommandHandler::class)
            ->setMethods(['fetchNews'])
            ->getMock();
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'Sdk\Common\CommandHandler\DisableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $news = \Sdk\News\News\Utils\MockFactory::generateNews($id);

        $this->stub->expects($this->exactly(1))
                    ->method('fetchNews')
                    ->willReturn($news);

        $result = $this->stub->fetchIEnableObject($id);

        $this->assertEquals($result, $news);
    }
}
