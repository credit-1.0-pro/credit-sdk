<?php
namespace Sdk\News\News\CommandHandler\News;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\News\News\Repository\NewsRepository;
use Sdk\News\News\Command\News\AddNewsCommand;

use Sdk\News\NewsCategory\Repository\NewsCategoryRepository;

class NewsCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNewsCommandHandlerTrait::class)->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetNews()
    {
        $trait = new MockNewsCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\News\News\Model\News',
            $trait->publicGetNews()
        );
    }

    public function testGetRepository()
    {
        $trait = new MockNewsCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\News\News\Repository\NewsRepository',
            $trait->publicGetRepository()
        );
    }

    public function testFetchNews()
    {
        $trait = $this->getMockBuilder(MockNewsCommandHandlerTrait::class)
                 ->setMethods(['getRepository'])->getMock();

        $id = 1;

        $news = \Sdk\News\News\Utils\MockFactory::generateNews($id);

        $repository = $this->prophesize(NewsRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($news);

        $trait->expects($this->exactly(1))
                         ->method('getRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchNews($id);

        $this->assertEquals($result, $news);
    }

    public function testGetNewsCategoryRepository()
    {
        $trait = new MockNewsCommandHandlerTrait();
        $this->assertInstanceOf(
            'Sdk\News\NewsCategory\Repository\NewsCategoryRepository',
            $trait->publicGetNewsCategoryRepository()
        );
    }

    public function testFetchNewsCategory()
    {
        $trait = $this->getMockBuilder(MockNewsCommandHandlerTrait::class)
                 ->setMethods(['getNewsCategoryRepository'])->getMock();

        $id = 1;
        $newsCategory = \Sdk\News\NewsCategory\Utils\MockNewsCategoryFactory::generateNewsCategory($id);

        $repository = $this->prophesize(NewsCategoryRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($newsCategory);

        $trait->expects($this->exactly(1))
                         ->method('getNewsCategoryRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchNewsCategory($id);

        $this->assertEquals($result, $newsCategory);
    }

    public function testNewsExecuteAction()
    {
        $trait = $this->getMockBuilder(MockNewsCommandHandlerTrait::class)
                 ->setMethods(['fetchNewsCategory'])->getMock();
        $id = 1;

        $news = \Sdk\News\News\Utils\MockFactory::generateNews($id);

        $command = new AddNewsCommand(
            $news->getTitle(),
            $news->getSource(),
            $news->getContent(),
            $news->getNewsType()->getId(),
            $news->getDimension(),
            $news->getStatus(),
            $news->getStick(),
            $news->getBannerStatus(),
            $news->getHomePageShowStatus(),
            $news->getReleaseTime(),
            $news->getCover(),
            $news->getAttachments(),
            $news->getBannerImage()
        );

        $trait->expects($this->exactly(1))
                         ->method('fetchNewsCategory')
                         ->with($news->getNewsType()->getId())
                         ->willReturn($news->getNewsType());
                         
        $result = $trait->publicNewsExecuteAction($command, $news);

        $this->assertEquals($result, $news);
    }
}
