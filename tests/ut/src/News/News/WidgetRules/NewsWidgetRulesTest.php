<?php
namespace Sdk\News\News\WidgetRules;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\News\News\Model\News;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class NewsWidgetRulesTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = NewsWidgetRules::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //dimension -- start
    /**
     * @dataProvider invalidDimensionProvider
     */
    public function testDimensionInvalid($actual, $expected)
    {
        $result = $this->widgetRule->dimension($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(DIMENSION_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidDimensionProvider()
    {
        return array(
            array('', false),
            array(News::DIMENSION['SOCIOLOGY'], true),
            array(News::DIMENSION['GOVERNMENT_AFFAIRS'], true),
            array(999, false),
        );
    }
    //dimension -- end
    
    //homePageShowStatus -- start
    /**
     * @dataProvider invalidHomePageShowStatusProvider
     */
    public function testHomePageShowStatusInvalid($actual, $expected)
    {
        $result = $this->widgetRule->homePageShowStatus($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(HOME_PAGE_SHOW_STATUS_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidHomePageShowStatusProvider()
    {
        return array(
            array('', false),
            array(News::HOME_PAGE_SHOW_STATUS['DISABLED'], true),
            array(News::HOME_PAGE_SHOW_STATUS['ENABLED'], true),
            array(999, false),
        );
    }
    //homePageShowStatus -- end
    
    //bannerStatus -- start
    /**
     * @dataProvider invalidBannerStatusProvider
     */
    public function testBannerStatusInvalid($actual, $expected)
    {
        $result = $this->widgetRule->bannerStatus($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(BANNER_STATUS_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidBannerStatusProvider()
    {
        return array(
            array('', false),
            array(News::BANNER_STATUS['DISABLED'], true),
            array(News::BANNER_STATUS['ENABLED'], true),
            array(999, false),
        );
    }
    //bannerStatus -- end

    //bannerImage -- start
    /**
     * @dataProvider invalidBannerImageProvider
     */
    public function testBannerImageInvalid($actual, $expected)
    {
        $result = $this->widgetRule->bannerImage($actual['bannerStatus'], $actual['bannerImage']);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(BANNER_IMAGE_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidBannerImageProvider()
    {
        return array(
            array(['bannerStatus' => News::BANNER_STATUS['ENABLED'], 'bannerImage' => ''], false),
            array(['bannerStatus' => News::BANNER_STATUS['ENABLED'], 'bannerImage' => 1], false),
            array(
                ['bannerStatus' => News::BANNER_STATUS['ENABLED'],
                'bannerImage' => ['name'=>'name', 'identify'=>'1.jpg']
                ], true),
            array(['bannerStatus' => News::BANNER_STATUS['DISABLED'], 'bannerImage' => ''], true),
            array(['bannerStatus' => News::BANNER_STATUS['DISABLED'], 'bannerImage' => 1], false)
        );
    }
    //bannerImage -- end

    //releaseTime -- start
    /**
     * @dataProvider invalidReleaseTimeProvider
     */
    public function testReleaseTime($actual, $expected)
    {
        $result = $this->widgetRule->releaseTime($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(RELEASE_TIME_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidReleaseTimeProvider()
    {
        return array(
            array('1652949496', true),
            array('string', false)
        );
    }
    //releaseTime -- end

    //content -- start
    /**
     * @dataProvider invalidContentProvider
     */
    public function testContent($actual, $expected)
    {
        $result = $this->widgetRule->content($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(NEWS_CONTENT_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidContentProvider()
    {
        return array(
            array('1652949496', true),
            array('string', true),
            array('', false),
            array(array('string'), false),
        );
    }
    //content -- end
}
