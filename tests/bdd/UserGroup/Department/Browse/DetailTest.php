<?php
namespace Sdk\UserGroup\Department\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\UserGroup\Department\Model\Department;
use Sdk\UserGroup\Department\DepartmentDataTrait;
use Sdk\UserGroup\Department\DepartmentRestfulUtils;
use Sdk\UserGroup\Department\Adapter\DepartmentRestfulAdapter;

/**
 * @Feature: 我是平台管理员,当我需要查看所有的科室数据时,在委办局管理下的科室管理中,可以查看到所有委办局下的科室数据,
 *           通过列表和详情的形式查看到我所有的科室信息,以便于我可以管理所有的科室数据
 * @Scenario: 查看科室数据详情
 */
class DetailTest extends TestCase
{
    use DepartmentDataTrait, DepartmentRestfulUtils;

    private $department;

    private $mock;

    public function setUp()
    {
        $this->department = new Department();
    }

    public function tearDown()
    {
        unset($this->department);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条科室数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getDepartmentDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条科室数据详情时
    */
    protected function fetchDepartment($id)
    {

        $adapter = new DepartmentRestfulAdapter();

        $this->department = $adapter->fetchOne($id);

        return $this->department;
    }

    /**
     * @Then 我可以看见科室详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchDepartment($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'userGroups/departments/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getDepartmentDetailData()['data'];

        $this->compareArrayAndObject($data, $this->department);
    }
}
