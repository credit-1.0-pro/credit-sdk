<?php


namespace Sdk\UserGroup\Department\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\UserGroup\Department\Model\Department;
use Sdk\UserGroup\Department\DepartmentDataTrait;
use Sdk\UserGroup\Department\DepartmentRestfulUtils;

/**
 * @Feature: 我是平台管理员,当我需要查看所有的科室数据时,在委办局管理下的科室管理中,可以查看到所有委办局下的科室数据,
 *           通过列表和详情的形式查看到我所有的科室信息,以便于我可以管理所有的科室数据
 * @Scenario: 查看科室数据列表
 */
class ListTest extends TestCase
{
    use DepartmentDataTrait, DepartmentRestfulUtils;

    private $department;

    private $mock;

    public function setUp()
    {
        $this->department = new Department();
    }

    public function tearDown()
    {
        unset($this->department);
        unset($this->mock);
    }

    /**
     * @Given: 存在科室数据
     */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getDepartmentListData());

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看科室列表时
     */
    public function fetchDepartmentList() : array
    {
        $filter = [];
        return $this->getDepartmentList($filter);
    }

    /**
     * @Then  我可以看见科室数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchDepartmentList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('userGroups/departments', $request->getUri()->getPath());
    }

    private function response()
    {
        $departmentList = $this->fetchDepartmentList();
        $departmentListArray = $this->getDepartmentListData()['data'];

        foreach ($departmentList as $department) {
            foreach ($departmentListArray as $departmentArray) {
                if ($departmentArray['id'] == $department->getId()) {
                    $this->compareArrayAndObject($departmentArray, $department);
                }
            }
        }
    }
}
