<?php


namespace Sdk\UserGroup\Department\Add;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\UserGroup\Department\Model\Department;
use Sdk\UserGroup\Department\DepartmentDataTrait;
use Sdk\UserGroup\Department\DepartmentRestfulUtils;

/**
 * @Feature: 我是平台管理员,当我需要新增科室时,在委办局管理下的科室管理中,新增对应的科室数据
 *           根据我归集到的科室数据新增,以便于我能更好的管理科室信息
 * @Scenario: 正常新增科室数据
 */
class SuccessTest extends TestCase
{
    use DepartmentDataTrait, DepartmentRestfulUtils, ErrorDataTrait;

    private $addDepartment;

    private $mock;

    public function setUp()
    {
        $this->addDepartment = new Department();
    }

    public function tearDown()
    {
        unset($this->addDepartment);
        unset($this->mock);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();
        $errorData = json_encode($data);

        $data = $this->getDepartmentDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $data = $this->getDepartmentDetailData()['data'];

        $this->addDepartment = new Department();
        $this->addDepartment->setName($data['attributes']['name']);
        $this->addDepartment->getUserGroup()->setId(1);

        return $this->addDepartment->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }


    private function request()
    {
        $data = $this->getDepartmentAddRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/userGroups/departments', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getDepartmentDetailData()['data'];

        $this->compareArrayAndObject($data, $this->addDepartment);
    }
}
