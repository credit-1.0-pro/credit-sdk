<?php
namespace Sdk\UserGroup\Department;

trait DepartmentRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $department
    ) {
        $this->assertEquals($expectedArray['id'], $department->getId());
        $this->assertEquals($expectedArray['attributes']['name'], $department->getName());
        if (isset($expectedArray['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['attributes']['createTime'], $department->getCreateTime());
        }
        if (isset($expectedArray['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['attributes']['updateTime'], $department->getUpdateTime());
        }
        if (isset($expectedArray['attributes']['status'])) {
            $this->assertEquals($expectedArray['attributes']['status'], $department->getStatus());
        }

        if (isset($expectedArray['relationships']['userGroup'])) {
            $this->assertEquals($expectedArray['relationships']['userGroup']['data']['type'], 'userGroups');
            $this->assertEquals(
                $expectedArray['relationships']['userGroup']['data']['id'],
                $department->getUserGroup()->getId()
            );
        }
    }
}
