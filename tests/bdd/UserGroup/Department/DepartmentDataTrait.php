<?php


namespace Sdk\UserGroup\Department;

use Sdk\UserGroup\Department\Adapter\DepartmentRestfulAdapter;

trait DepartmentDataTrait
{
    protected function getDepartmentList(array $filter = []) : array
    {
        $adapter = new DepartmentRestfulAdapter();

        list($count, $departmentList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $departmentList;
    }

    protected function getDepartmentListData(): array
    {
        return [
            "meta" => [
                "count" =>25,
                "links" => [
                    "first" =>1,
                    "last" =>2,
                    "prev" =>null,
                    "next" =>2
                ]
            ],
            "links" => [
                "first" =>"api.base.qixinyun.com/departments/?include=userGroup&page[number]=1&page[size]=20",
                "last" =>"api.base.qixinyun.com/departments/?include=userGroup&page[number]=2&page[size]=20",
                "prev" =>null,
                "next" =>"api.base.qixinyun.com/departments/?include=userGroup&page[number]=2&page[size]=20"
            ],
            "data" =>[
                [
                    "type" =>"departments",
                    "id" =>"1",
                    "attributes" => [
                        "name" =>"主任办公室",
                        "status" =>0,
                        "createTime" =>1597798915,
                        "updateTime" =>1597798915,
                        "statusTime" =>0
                    ],
                    "relationships" => [
                        "userGroup" => [
                            "data" => [
                                "type" =>"userGroups",
                                "id" =>"1"
                            ]
                        ]
                    ],
                    "links" => [
                        "self" =>"api.base.qixinyun.com/departments/1"
                    ]
                ]
            ],
            "included" =>[
                [
                    "type" =>"userGroups",
                    "id" =>"1",
                    "attributes" => [
                        "name" => "乌兰察布发展和改革委员会",
                        "shortName" => "市发改委",
                        "unifiedSocialCreditCode" => "123456789012345678",
                        "administrativeArea" => 0,
                        "status" => 0,
                        "createTime" => 1652949496,
                        "updateTime" => 1653114283,
                        "statusTime" => 0
                    ]
                ]
            ]
        ];
    }

    protected function getDepartmentDetailData(
        int $id = 1
    ) : array {
         return [
            "meta" => [],
            "data" => [
                "type" => "departments",
                "id" => $id,
                "attributes" => [
                    "name" => "财务部门",
                    "status" => 0,
                    "createTime" => 1652949895,
                    "updateTime" => 1653031589,
                    "statusTime" => 0
                ],
                "relationships" => [
                    "userGroup" => [
                        "data" => [
                            "type" => "userGroups",
                            "id" => "1"
                        ]
                    ]
                ],
                "links" => [
                    "self" => "127.0.0.1 =>8080/departments/1"
                ]
            ],
            "included" => [
                [
                    "type" => "userGroups",
                    "id" => "1",
                    "attributes" => [
                        "name" => "乌兰察布发展和改革委员会",
                        "shortName" => "市发改委",
                        "unifiedSocialCreditCode" => "123456789012345678",
                        "administrativeArea" => 0,
                        "status" => 0,
                        "createTime" => 1652949496,
                        "updateTime" => 1653114283,
                        "statusTime" => 0
                    ]
                ]
            ]
         ];
    }

    protected function getDepartmentAddRequestData() : array
    {
        return array(
            "data" => array(
                "type" => "departments",
                "attributes" => array(
                    "name" => '财务部门'
                ),
                "relationships" => array(
                    "userGroup" => array(
                        "data" => array(
                            array("type" => "userGroups", "id" => 1)
                        )
                    )
                )
            )
        );
    }

    protected function getDepartmentEditRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"departments",
                "attributes"=>array(
                    "name"=>"人事部门"
                ),
            )
        );
    }
}
