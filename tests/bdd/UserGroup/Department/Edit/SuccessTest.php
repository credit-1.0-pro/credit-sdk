<?php


namespace Sdk\UserGroup\Department\Edit;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\UserGroup\Department\Model\Department;
use Sdk\UserGroup\Department\DepartmentDataTrait;
use Sdk\UserGroup\Department\DepartmentRestfulUtils;

/**
 * @Feature: 我是平台管理员,当我需要编辑科室时,在委办局管理下的科室管理中,编辑对应的科室数据
 *           根据我归集到的科室数据编辑,以便于我能更好的管理科室信息
 * @Scenario: 正常编辑科室数据
 */
class SuccessTest extends TestCase
{
    use DepartmentDataTrait, DepartmentRestfulUtils, ErrorDataTrait;

    private $department;

    private $mock;

    public function setUp()
    {
        $this->department = new Department();
    }

    public function tearDown()
    {
        unset($this->department);
        unset($this->mock);
    }

    /**
     * @Given: 我编辑数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();
        $errorData = json_encode($data);

        $data = $this->getDepartmentDetailData();
        $requestDataAttributes = $this->getDepartmentEditRequestData()['data']['attributes'];

        $data['data']['attributes']['name'] = $requestDataAttributes['name'];

        $editJsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorData),
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $editJsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用编辑函数,期待返回true
     */
    protected function edit()
    {
        $data = $this->getDepartmentEditRequestData()['data'];

        $this->department = new Department();
        $this->department->setId(1);
        $this->department->setName($data['attributes']['name']);

        return $this->department->edit();
    }

    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->edit();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getDepartmentEditRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/userGroups/departments/1', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getDepartmentEditRequestData()['data'];
        $data['id'] = 1;

        $this->compareArrayAndObject($data, $this->department);
    }
}
