<?php


namespace Sdk\UserGroup\UserGroup\Edit;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\UserGroupDataTrait;
use Sdk\UserGroup\UserGroup\UserGroupRestfulUtils;

/**
 * @Feature: 我是平台管理员,当我需要编辑委办局时,在委办局管理中,编辑对应的委办局数据
 *           根据我归集到的委办局数据编辑,以便于我能更好的管理委办局信息
 * @Scenario: 正常编辑委办局数据
 */
class SuccessTest extends TestCase
{
    use UserGroupDataTrait, UserGroupRestfulUtils, ErrorDataTrait;

    private $userGroup;

    private $mock;

    public function setUp()
    {
        $this->userGroup = new UserGroup();
    }

    public function tearDown()
    {
        unset($this->userGroup);
        unset($this->mock);
    }

    /**
     * @Given: 我编辑数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();
        $errorData = json_encode($data);

        $data = $this->getUserGroupDetailData();
        $requestDataAttributes = $this->getUserGroupEditRequestData()['data']['attributes'];

        $data['data']['attributes']['name'] = $requestDataAttributes['name'];
        $data['data']['attributes']['shortName'] = $requestDataAttributes['shortName'];
        $data['data']['attributes']['unifiedSocialCreditCode'] = $requestDataAttributes['unifiedSocialCreditCode'];

        $editJsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorData),
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $editJsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用编辑函数,期待返回true
     */
    protected function edit()
    {
        $data = $this->getUserGroupEditRequestData()['data'];

        $this->userGroup = new UserGroup();
        $this->userGroup->setId(1);
        $this->userGroup->setName($data['attributes']['name']);
        $this->userGroup->setShortName($data['attributes']['shortName']);
        $this->userGroup->setUnifiedSocialCreditCode($data['attributes']['unifiedSocialCreditCode']);

        return $this->userGroup->edit();
    }

    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->edit();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getUserGroupEditRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/userGroups/1', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getUserGroupEditRequestData()['data'];
        $data['id'] = 1;

        $this->compareArrayAndObject($data, $this->userGroup);
    }
}
