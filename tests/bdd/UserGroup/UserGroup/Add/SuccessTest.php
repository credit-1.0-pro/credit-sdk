<?php


namespace Sdk\UserGroup\UserGroup\Add;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\UserGroupDataTrait;
use Sdk\UserGroup\UserGroup\UserGroupRestfulUtils;

/**
 * @Feature: 我是平台管理员,当我需要新增委办局时,在委办局管理中,新增对应的委办局数据
 *           根据我归集到的委办局数据新增,以便于我能更好的管理委办局信息
 * @Scenario: 正常新增委办局数据
 */
class SuccessTest extends TestCase
{
    use UserGroupDataTrait, UserGroupRestfulUtils, ErrorDataTrait;

    private $addUserGroup;

    private $mock;

    public function setUp()
    {
        $this->addUserGroup = new UserGroup();
    }

    public function tearDown()
    {
        unset($this->addUserGroup);
        unset($this->mock);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();
        $errorData = json_encode($data);

        $data = $this->getUserGroupDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $data = $this->getUserGroupDetailData()['data'];

        $this->addUserGroup = new UserGroup();
        $this->addUserGroup->setName($data['attributes']['name']);
        $this->addUserGroup->setShortName($data['attributes']['shortName']);
        $this->addUserGroup->setUnifiedSocialCreditCode($data['attributes']['unifiedSocialCreditCode']);
        $this->addUserGroup->setAdministrativeArea($data['attributes']['administrativeArea']);

        return $this->addUserGroup->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }


    private function request()
    {
        $data = $this->getUserGroupAddRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/userGroups', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getUserGroupDetailData()['data'];

        $this->compareArrayAndObject($data, $this->addUserGroup);
    }
}
