<?php


namespace Sdk\UserGroup\UserGroup\Add;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\UserGroupDataTrait;
use Sdk\UserGroup\UserGroup\UserGroupRestfulUtils;

/**
 * @Feature: 我是平台管理员,当我需要新增委办局时,在委办局管理中,新增对应的委办局数据
 *           根据我归集到的委办局数据新增,以便于我能更好的管理委办局信息
 * @Scenario: 异常流程-数据重复,新增失败
 */
class FailRepeatTest extends TestCase
{
    use UserGroupDataTrait, UserGroupRestfulUtils, ErrorDataTrait;

    private $userGroup;

    private $mock;

    public function setUp()
    {
        $this->userGroup = new UserGroup();
    }

    public function tearDown()
    {
        unset($this->userGroup);
        unset($this->mock);
    }

    /**
     * @Given: 我已经新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();
        $errorResourceNotExistData = json_encode($data);

        $data = $this->getErrorResourceIsUnique('userGroupName');
        $errorResourceIsUniqueData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorResourceNotExistData),
            new Response(409, ['Content-Type' => 'application/vnd.api+json'], $errorResourceIsUniqueData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回false
     */
    protected function add()
    {
        $data = $this->getUserGroupDetailData()['data'];

        $this->userGroup = new UserGroup();
        $this->userGroup->setName($data['attributes']['name']);
        $this->userGroup->setShortName($data['attributes']['shortName']);
        $this->userGroup->setUnifiedSocialCreditCode($data['attributes']['unifiedSocialCreditCode']);
        $this->userGroup->setAdministrativeArea($data['attributes']['administrativeArea']);

        return $this->userGroup->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();
        $data = $this->getUserGroupAddRequestData();

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/userGroups', $request->getUri()->getPath());
    }

    private function response()
    {
        $this->assertEmpty($this->userGroup->getId());
    }
}
