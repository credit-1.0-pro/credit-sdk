<?php
namespace Sdk\UserGroup\UserGroup;

trait UserGroupRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $userGroup
    ) {
        $this->assertEquals($expectedArray['id'], $userGroup->getId());
        $this->assertEquals($expectedArray['attributes']['name'], $userGroup->getName());
        $this->assertEquals($expectedArray['attributes']['shortName'], $userGroup->getShortName());
        $this->assertEquals(
            $expectedArray['attributes']['unifiedSocialCreditCode'],
            $userGroup->getUnifiedSocialCreditCode()
        );
        if (isset($expectedArray['attributes']['administrativeArea'])) {
            $this->assertEquals(
                $expectedArray['attributes']['administrativeArea'],
                $userGroup->getAdministrativeArea()
            );
        }
        if (isset($expectedArray['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['attributes']['createTime'], $userGroup->getCreateTime());
        }
        if (isset($expectedArray['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['attributes']['updateTime'], $userGroup->getUpdateTime());
        }
        if (isset($expectedArray['attributes']['status'])) {
            $this->assertEquals($expectedArray['attributes']['status'], $userGroup->getStatus());
        }
    }
}
