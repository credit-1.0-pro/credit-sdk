<?php


namespace Sdk\UserGroup\UserGroup;

use Sdk\UserGroup\UserGroup\Adapter\UserGroupRestfulAdapter;

trait UserGroupDataTrait
{
    protected function getUserGroupList(array $filter = []) : array
    {
        $adapter = new UserGroupRestfulAdapter();

        list($count, $userGroupList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $userGroupList;
    }

    protected function getUserGroupListData(): array
    {
        return [
            "meta" => [
                "count" => 4,
                "links" => [
                    "first" => null,
                    "last" => null,
                    "prev" => null,
                    "next" => null
                ]
            ],
            "links" => [
                "first" => null,
                "last" => null,
                "prev" => null,
                "next" => null
            ],
            "data" => [
                [
                    "type" => "userGroups",
                    "id" => "15",
                    "attributes" => [
                        "name" => "人力和资源社会保障局",
                        "shortName" => "人社局",
                        "unifiedSocialCreditCode" => "123456789012345678",
                        "administrativeArea" => 0,
                        "status" => 0,
                        "createTime" => 1558345495,
                        "updateTime" => 1598015256,
                        "statusTime" => 0
                    ],
                    "links" => [
                        "self" => "127.0.0.1:8080/userGroups/15"
                    ]
                ],
                [
                    "type" => "userGroups",
                    "id" => "1",
                    "attributes" => [
                        "name" => "发展和改革委员会",
                        "shortName" => "发改委",
                        "unifiedSocialCreditCode" => "123456789012345678",
                        "administrativeArea" => 0,
                        "status" => 0,
                        "createTime" => 1558345495,
                        "updateTime" => 1610539122,
                        "statusTime" => 0
                    ],
                    "links" => [
                        "self" => "127.0.0.1:8080/userGroups/1"
                    ]
                ]
            ]
        ];
    }

    protected function getUserGroupDetailData(
        int $id = 1
    ) : array {
        return [
            "meta" => [],
            "data" => [
                "type" => "userGroups",
                "id" => $id,
                "attributes" => [
                    "name" => "乌兰察布发展和改革委员会",
                    "shortName" => "市发改委",
                    "unifiedSocialCreditCode" => "123456789012345678",
                    "administrativeArea" => 0,
                    "status" => 0,
                    "createTime" => 1652949496,
                    "updateTime" => 1653114283,
                    "statusTime" => 0
                ],
                "links" => [
                    "self" => "127.0.0.1:8080/userGroups/1"
                ]
            ]
        ];
    }

    protected function getUserGroupAddRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"userGroups",
                "attributes"=>array(
                    "name"=>"乌兰察布发展和改革委员会",
                    "shortName"=>"市发改委",
                    "unifiedSocialCreditCode"=>"123456789012345678",
                    "administrativeArea"=>0
                ),
            )
        );
    }

    protected function getUserGroupEditRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"userGroups",
                "attributes"=>array(
                    "name"=>"发展和改革委员会",
                    "shortName"=>"发改委",
                    "unifiedSocialCreditCode"=>"123456789012345679",
                ),
            )
        );
    }
}
