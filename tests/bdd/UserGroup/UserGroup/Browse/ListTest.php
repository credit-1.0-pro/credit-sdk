<?php


namespace Sdk\UserGroup\UserGroup\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\UserGroupDataTrait;
use Sdk\UserGroup\UserGroup\UserGroupRestfulUtils;

/**
 * @Feature: 我是一名分公司员工（平管/超管）,当我需要查看我的委办局数据时,在委办局管理中,可以查看到我的所有委办局数据,
 *           通过列表和详情的形式查看到我所有的委办局信息,以便于我可以了解委办局的情况
 * @Scenario: 查看委办局数据列表
 */
class ListTest extends TestCase
{
    use UserGroupDataTrait, UserGroupRestfulUtils;

    private $userGroup;

    private $mock;

    public function setUp()
    {
        $this->userGroup = new UserGroup();
    }

    public function tearDown()
    {
        unset($this->userGroup);
        unset($this->mock);
    }

    /**
     * @Given: 存在委办局数据
     */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getUserGroupListData());

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看委办局列表时
     */
    public function fetchUserGroupList() : array
    {
        $filter = [];
        return $this->getUserGroupList($filter);
    }

    /**
     * @Then  我可以看见委办局数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUserGroupList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('userGroups', $request->getUri()->getPath());
    }

    private function response()
    {
        $userGroupList = $this->fetchUserGroupList();
        $userGroupListArray = $this->getUserGroupListData()['data'];

        foreach ($userGroupList as $userGroup) {
            foreach ($userGroupListArray as $userGroupArray) {
                if ($userGroupArray['id'] == $userGroup->getId()) {
                    $this->compareArrayAndObject($userGroupArray, $userGroup);
                }
            }
        }
    }
}
