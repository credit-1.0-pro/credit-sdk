<?php
namespace Sdk\UserGroup\UserGroup\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\UserGroup\UserGroup\Model\UserGroup;
use Sdk\UserGroup\UserGroup\UserGroupDataTrait;
use Sdk\UserGroup\UserGroup\UserGroupRestfulUtils;
use Sdk\UserGroup\UserGroup\Adapter\UserGroupRestfulAdapter;

/**
 * @Feature: 我是一名分公司员工（平管/超管）,当我需要查看我的委办局数据时,在委办局管理中,可以查看到我的所有委办局数据,
 *           通过列表和详情的形式查看到我所有的委办局信息,以便于我可以了解委办局的情况
 * @Scenario: 查看委办局数据详情
 */
class DetailTest extends TestCase
{
    use UserGroupDataTrait, UserGroupRestfulUtils;

    private $userGroup;

    private $mock;

    public function setUp()
    {
        $this->userGroup = new UserGroup();
    }

    public function tearDown()
    {
        unset($this->userGroup);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条委办局数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getUserGroupDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条委办局数据详情时
    */
    protected function fetchUserGroup($id)
    {

        $adapter = new UserGroupRestfulAdapter();

        $this->userGroup = $adapter->fetchOne($id);

        return $this->userGroup;
    }

    /**
     * @Then 我可以看见委办局详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchUserGroup($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'userGroups/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getUserGroupDetailData()['data'];

        $this->compareArrayAndObject($data, $this->userGroup);
    }
}
