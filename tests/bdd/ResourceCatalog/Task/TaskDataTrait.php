<?php


namespace Sdk\ResourceCatalog\Task;

use Sdk\ResourceCatalog\Task\Model\Task;
use Sdk\ResourceCatalog\Task\Adapter\Task\TaskRestfulAdapter;

trait TaskDataTrait
{
    protected function getTaskList(array $filter = []) : array
    {
        $adapter = new TaskRestfulAdapter();

        list($count, $taskList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $taskList;
    }

    protected function getTaskAttributesData() : array
    {
        return array(
            "administrativeArea" => 0,
            "total" => 5,
            "successNumber" => 4,
            "failureNumber" => 1,
            "template" => 4,
            "templateVersion" => 9,
            "rule" => 3,
            "ruleVersion" => 18,
            "errorNumber" => 0,
            "fileName" => "FAILURE_DJXX_4_1_76123897098765432123456789098734.xlsx",
            "status" => -3,
            "createTime" => 1664272842,
            "updateTime" => 1664272842,
            "statusTime" => 1664272842
        );
    }

    protected function getTaskRelationshipsData() : array
    {
        $relationships['crew'] = array("data"=>array("type"=>"crews","id"=>1));
        $relationships['userGroup'] = array("data"=>array("type"=>"userGroups","id"=>1));

        return $relationships;
    }

    protected function getTaskListData(): array
    {
        $attributes = $this->getTaskAttributesData();
        $relationships = $this->getTaskRelationshipsData();

        return [
            "meta" => [
                "count" => 4,
                "links" => [
                    "first" => null,
                    "last" => null,
                    "prev" => null,
                    "next" => null
                ]
            ],
            "links" => [
                "first" => null,
                "last" => null,
                "prev" => null,
                "next" => null
            ],
            "data" => [
                [
                    "type" => "tasks",
                    "id" => "1",
                    "attributes" =>$attributes,
                    "relationships" =>$relationships,
                    "links" => [
                        "self" => "127.0.0.1:8080/tasks/1"
                    ]
                ],
            ],
            "included" => [
                [
                    "type" => "crews",
                    "id" => "1",
                    "attributes" => [
                        "realName" => "白玉",
                    ]
                ]
            ]
        ];
    }

    protected function getTaskDetailData() : array
    {
        $attributes = $this->getTaskAttributesData();
        $relationships = $this->getTaskRelationshipsData();
        
        return [
            "meta" => [],
            "data" => [
                "type" => "tasks",
                "id" => 1,
                "attributes" => $attributes,
                "relationships" => $relationships,
                "links" => [
                    "self" => "127.0.0.1:8080/tasks/1"
                ]
            ],
            "included" => [
                [
                    "type" => "crews",
                    "id" => "1",
                    "attributes" => [
                        "realName" => "白玉",
                    ]
                ]
            ]
        ];
    }
}
