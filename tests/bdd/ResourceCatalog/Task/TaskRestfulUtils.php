<?php
namespace Sdk\ResourceCatalog\Task;

trait TaskRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $task
    ) {
        if (isset($expectedArray['id'])) {
            $this->assertEquals($expectedArray['id'], $task->getId());
        }
        
        $this->assertEquals($expectedArray['attributes']['administrativeArea'], $task->getAdministrativeArea());
        $this->assertEquals($expectedArray['attributes']['total'], $task->getTotal());
        $this->assertEquals($expectedArray['attributes']['successNumber'], $task->getSuccessNumber());
        $this->assertEquals($expectedArray['attributes']['failureNumber'], $task->getFailureNumber());
        $this->assertEquals($expectedArray['attributes']['template'], $task->getTemplate());
        $this->assertEquals($expectedArray['attributes']['templateVersion'], $task->getTemplateVersion());
        $this->assertEquals($expectedArray['attributes']['rule'], $task->getRule());
        $this->assertEquals($expectedArray['attributes']['ruleVersion'], $task->getRuleVersion());
        $this->assertEquals($expectedArray['attributes']['errorNumber'], $task->getErrorNumber());
        $this->assertEquals($expectedArray['attributes']['fileName'], $task->getFileName());
        if (isset($expectedArray['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['attributes']['createTime'], $task->getCreateTime());
        }
        if (isset($expectedArray['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['attributes']['statusTime'], $task->getStatusTime());
        }
        if (isset($expectedArray['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['attributes']['updateTime'], $task->getUpdateTime());
        }
        if (isset($expectedArray['attributes']['status'])) {
            $this->assertEquals($expectedArray['attributes']['status'], $task->getStatus());
        }

        $this->compareCrew($expectedArray, $task);
        $this->comparePublishUserGroup($expectedArray, $task);
    }

    private function compareCrew(array $expectedArray, $task)
    {
        if (isset($expectedArray['relationships']['crew'])) {
            $this->assertEquals($expectedArray['relationships']['crew']['data']['type'], 'crews');
            $this->assertEquals(
                $expectedArray['relationships']['crew']['data']['id'],
                $task->getCrew()->getId()
            );
        }
    }

    private function comparePublishUserGroup(array $expectedArray, $task)
    {
        if (isset($expectedArray['relationships']['publishUserGroup'])) {
            $this->assertEquals($expectedArray['relationships']['publishUserGroup']['data']['type'], 'userGroups');
            $this->assertEquals(
                $expectedArray['relationships']['publishUserGroup']['data']['id'],
                $task->getUserGroup()->getId()
            );
        }
    }
}
