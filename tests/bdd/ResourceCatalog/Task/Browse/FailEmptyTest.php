<?php


namespace Sdk\ResourceCatalog\Task\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\ResourceCatalog\Task\Model\Task;
use Sdk\ResourceCatalog\Task\TaskDataTrait;
use Sdk\ResourceCatalog\Task\TaskRestfulUtils;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 * 当我需要查看我的任务数据时, 在资源目录管理中的任务列表中, 可以查看到我的所有任务数据
 * 通过列表形式查看我所有的任务数据,以便于我可以更快的了解资源目录数据的导入情况
 * @Scenario: 查看任务列表-不存在任务数据
 */
class FailEmptyTest extends TestCase
{
    use TaskDataTrait, TaskRestfulUtils, ErrorDataTrait;

    private $task;

    private $mock;

    public function setUp()
    {
        $this->task = new Task();
    }

    public function tearDown()
    {
        unset($this->task);
        unset($this->mock);
    }

    /**
     * @Given: 不存在目录数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
                new Response(404, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看目录列表时
     */
    public function fetchTaskList() : array
    {
        $filter = [];
        return $this->task = $this->getTaskList($filter);
    }

    /**
     * @Then  我可以看见目录数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchTaskList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('resourceCatalogs/tasks', $request->getUri()->getPath());
    }

    private function response()
    {
        $this->assertEmpty($this->task);
    }
}
