<?php


namespace Sdk\ResourceCatalog\ErrorData\Edit;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\ErrorData\ErrorDataDataTrait;
use Sdk\ResourceCatalog\ErrorData\ErrorDataRestfulUtils;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,当我需要编辑失败数据时,
 * 在资源目录管理中的失败数据列表中,点击在线编辑,进入在线编辑界面，并根据我所采集的资源目录数据进行修改,以便于我维护数据列表
 * @Scenario: 在线编辑成功
 */
class SuccessTest extends TestCase
{
    use ErrorDataDataTrait, ErrorDataRestfulUtils;

    private $errorData;

    private $mock;

    public function setUp()
    {
        $this->errorData = new ErrorData();
    }

    public function tearDown()
    {
        unset($this->errorData);
        unset($this->mock);
    }

    /**
     * @Given: 我编辑数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorDataDetailData();
        $editJsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $editJsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用编辑函数,期待返回true
     */
    protected function edit()
    {
        $data = $this->getErrorDataOnlineEditRequestData()['data'];

        $this->errorData = new ErrorData();
        $this->errorData->setId(1);
        $this->errorData->setExpirationDate($data['attributes']['expirationDate']);

        $this->errorData->getCrew()->setId($data['relationships']['crew']['data'][0]['id']);
        $this->errorData->getItemsData()->setData(
            $data['relationships']['itemsData']['data'][0]['attributes']['data']
        );

        return $this->errorData->onlineEdit();
    }

    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->edit();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getErrorDataOnlineEditRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/resourceCatalogs/errorData/1/onlineEdit', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getErrorDataDetailData()['data'];
        $data['id'] = 1;

        $this->compareArrayAndObjectCommon($data, $this->errorData);
    }
}
