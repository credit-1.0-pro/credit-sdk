<?php


namespace Sdk\ResourceCatalog\ErrorData;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\ErrorData\Adapter\ErrorData\ErrorDataRestfulAdapter;

use Sdk\ResourceCatalog\Task\TaskDataTrait;
use Sdk\ResourceCatalog\Template\TemplateDataTrait;

trait ErrorDataDataTrait
{
    use TaskDataTrait, TemplateDataTrait;

    protected function getErrorDataList(array $filter = []) : array
    {
        $adapter = new ErrorDataRestfulAdapter();

        list($count, $errorDataList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $errorDataList;
    }

    protected function getErrorDataAttributesData() : array
    {
        return array(
            "name" => "天宇再生资源回收利用有限公司",
            "identify" => "91440705MA554JGT3A",
            "errorType" => 4,
            "errorReason" => array(
                "ZTMC" => ["4"],
                "FDDBR" => ["4"],
                "TYSHXYDM" => ["4"],
            ),
            "status" => 0,
            "createTime" => 1664274313,
            "updateTime" => 1664274313,
            "statusTime" => 0
        );
    }

    protected function getErrorDataRequestRelationshipsData() : array
    {
        return array(
            "crew" => array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            ),
            "itemsData" => array(
                "data" => array(
                    array(
                        "type" => "itemsData",
                        "attributes" => array(
                            "data" => array(
                                "ZTMC" => "陕西传媒有限公司",
                                "TYSHXYDM" => "921345679023456789",
                                "FDDBR" => "白玉",
                                "ZTLB" => "法人及非法人组织",
                                "GKFW" => "社会公开",
                                "GXPL" => "实时",
                                "XXFL" => "行政许可",
                                "XXLB" => "基础信息"
                            )
                        )
                    )
                )
            )
        );
    }

    protected function getErrorDataRelationshipsData() : array
    {
        $relationships['task'] = array("data"=>array("type"=>"tasks","id"=>1));
        $relationships['errorItemsData'] = array("data"=>array("type"=>"errorItemsData","id"=>1));
        $relationships['template'] = array("data"=>array("type"=>"templates","id"=>1));
        $relationships['templateVersion'] = array("data"=>array("type"=>"templateVersions","id"=>1));
 
        return $relationships;
    }

    protected function getErrorDataIncludedData()
    {
        return [
            $this->getTaskIncluded(),
            $this->getTemplateIncluded(),
            $this->getTemplateVersionIncluded(),
            $this->getErrorItemsDataIncluded()
        ];
    }

    protected function getTaskIncluded() : array
    {
        return $this->getTaskDetailData()['data'];
    }

    protected function getTemplateIncluded() : array
    {
        return $this->getTemplateDetailData()['data'];
    }

    protected function getErrorItemsDataIncluded() : array
    {
        return
        [
            "type" => "errorItemsData",
            "id" => 1,
            "attributes" => [
                "data" => [
                    "ZTMC" => "山西传媒有限公司",
                    "TYSHXYDM" => "921345679023456788",
                    "FDDBR" => "白玉",
                    "ZTLB" => "法人及非法人组织",
                    "GKFW" => "社会公开",
                    "GXPL" => "实时",
                    "XXFL" => "行政许可",
                    "XXLB" => "基础信息"
                ]
            ]
        ];
    }

    protected function getErrorDataListData(): array
    {
        $attributes = $this->getErrorDataAttributesData();
        $relationships = $this->getErrorDataRelationshipsData();

        return [
            "meta" => ["count" => 1],
            "data" => [
                [
                    "type" => "errorData",
                    "id" => "15",
                    "attributes" =>$attributes,
                    "relationships" =>$relationships,
                    "links" => [
                        "self" => "127.0.0.1:8080/errorData/15"
                    ]
                ],
            ],
            "included" => $this->getErrorDataIncludedData()
        ];
    }

    protected function getErrorDataDetailData() : array
    {
        $attributes = $this->getErrorDataAttributesData();
        $relationships = $this->getErrorDataRelationshipsData();
        
        return [
            "meta" => [],
            "data" => [
                "type" => "errorData",
                "id" => 1,
                "attributes" => $attributes,
                "relationships" => $relationships,
                "links" => [
                    "self" => "127.0.0.1:8080/errorData/1"
                ]
            ],
            "included" => $this->getErrorDataIncludedData()
        ];
    }

    protected function getErrorDataOnlineEditRequestData() : array
    {
        $relationships = $this->getErrorDataRequestRelationshipsData();

        return array(
            'data' => array(
                "type"=>"errorData",
                "attributes" => array(
                    "expirationDate" => 1618285915,    //有效期限
                ),
                "relationships"=>$relationships,
            )
        );
    }
}
