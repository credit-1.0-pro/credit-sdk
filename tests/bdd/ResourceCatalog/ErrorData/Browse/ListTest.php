<?php


namespace Sdk\ResourceCatalog\ErrorData\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\ErrorData\Model\ErrorData;
use Sdk\ResourceCatalog\ErrorData\ErrorDataDataTrait;
use Sdk\ResourceCatalog\ErrorData\ErrorDataRestfulUtils;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 * 当我需要查看我的失败数据时,在资源目录管理中的任务列表中,可以查看到我的失败数据
 * 通过列表形式查看我所有的失败数据,以便于我可以及时更新失败数据重新上报
 * @Scenario: 查看错误数据列表
 */
class ListTest extends TestCase
{
    use ErrorDataDataTrait, ErrorDataRestfulUtils;

    private $errorData;

    private $mock;

    public function setUp()
    {
        $this->errorData = new ErrorData();
    }

    public function tearDown()
    {
        unset($this->errorData);
        unset($this->mock);
    }

    /**
     * @Given: 存在目录数据
     */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getErrorDataListData());

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看目录列表时
     */
    public function fetchErrorDataList() : array
    {
        $filter = [];
        return $this->getErrorDataList($filter);
    }

    /**
     * @Then  我可以看见目录数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchErrorDataList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('resourceCatalogs/errorData', $request->getUri()->getPath());
    }

    private function response()
    {
        $errorDataList = $this->fetchErrorDataList();
        $errorDataListArray = $this->getErrorDataListData()['data'];

        foreach ($errorDataList as $errorData) {
            foreach ($errorDataListArray as $errorDataArray) {
                if ($errorDataArray['id'] == $errorData->getId()) {
                    $this->compareArrayAndObjectCommon($errorDataArray, $errorData);
                }
            }
        }
    }
}
