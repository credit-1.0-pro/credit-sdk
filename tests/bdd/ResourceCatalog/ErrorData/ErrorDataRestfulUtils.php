<?php
namespace Sdk\ResourceCatalog\ErrorData;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait ErrorDataRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $errorData
    ) {
        if (isset($expectedArray['id'])) {
            $this->assertEquals($expectedArray['id'], $errorData->getId());
        }

        if (isset($expectedArray['attributes']['name'])) {
            $this->assertEquals($expectedArray['attributes']['name'], $errorData->getName());
        }
        if (isset($expectedArray['attributes']['identify'])) {
            $this->assertEquals($expectedArray['attributes']['identify'], $errorData->getIdentify());
        }
        if (isset($expectedArray['attributes']['errorType'])) {
            $this->assertEquals($expectedArray['attributes']['errorType'], $errorData->getErrorType());
        }
        if (isset($expectedArray['attributes']['errorReason'])) {
            $this->assertEquals($expectedArray['attributes']['errorReason'], $errorData->getErrorReason());
        }
        if (isset($expectedArray['attributes']['errorStatus'])) {
            $this->assertEquals($expectedArray['attributes']['errorStatus'], $errorData->getErrorStatus());
        }
        if (isset($expectedArray['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['attributes']['createTime'], $errorData->getCreateTime());
        }
        if (isset($expectedArray['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['attributes']['statusTime'], $errorData->getStatusTime());
        }
        if (isset($expectedArray['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['attributes']['updateTime'], $errorData->getUpdateTime());
        }
        if (isset($expectedArray['attributes']['status'])) {
            $this->assertEquals($expectedArray['attributes']['status'], $errorData->getStatus());
        }
        if (isset($expectedArray['attributes']['expirationDate'])) {
            $this->assertEquals(
                $expectedArray['attributes']['expirationDate'],
                $errorData->getExpirationDate()
            );
        }

        $this->compareTask($expectedArray, $errorData);
        $this->compareTemplateVersion($expectedArray, $errorData);
        $this->compareTemplate($expectedArray, $errorData);
        $this->compareErrorItemsData($expectedArray, $errorData);
        $this->compareCrew($expectedArray, $errorData);
        $this->compareItemsData($expectedArray, $errorData);
    }

    private function compareCrew(array $expectedArray, $errorData)
    {
        if (isset($expectedArray['relationships']['crew'])) {
            $this->assertEquals($expectedArray['relationships']['crew']['data']['type'], 'crews');
            $this->assertEquals(
                $expectedArray['relationships']['crew']['data']['id'],
                $errorData->getCrew()->getId()
            );
        }
    }

    private function compareTask(array $expectedArray, $errorData)
    {
        if (isset($expectedArray['relationships']['task'])) {
            $this->assertEquals($expectedArray['relationships']['task']['data']['type'], 'tasks');
            $this->assertEquals(
                $expectedArray['relationships']['task']['data']['id'],
                $errorData->getTask()->getId()
            );
        }
    }

    private function compareTemplateVersion(array $expectedArray, $errorData)
    {
        if (isset($expectedArray['relationships']['templateVersion'])) {
            $this->assertEquals(
                $expectedArray['relationships']['templateVersion']['data']['type'],
                'templateVersions'
            );
            $this->assertEquals(
                $expectedArray['relationships']['templateVersion']['data']['id'],
                $errorData->getTemplateVersion()->getId()
            );
        }
    }

    private function compareTemplate(array $expectedArray, $errorData)
    {
        if (isset($expectedArray['relationships']['template'])) {
            $this->assertEquals($expectedArray['relationships']['template']['data']['type'], 'templates');
            $this->assertEquals(
                $expectedArray['relationships']['template']['data']['id'],
                $errorData->getTemplate()->getId()
            );
        }
    }

    private function compareErrorItemsData(array $expectedArray, $errorData)
    {
        if (isset($expectedArray['relationships']['errorItemsData'])) {
            $this->assertEquals($expectedArray['relationships']['errorItemsData']['data']['type'], 'errorItemsData');
            $this->assertEquals(
                $expectedArray['relationships']['errorItemsData']['data']['id'],
                $errorData->getItemsData()->getId()
            );
        }
    }

    private function compareItemsData(array $expectedArray, $errorData)
    {
        if (isset($expectedArray['relationships']['itemsData'])) {
            $this->assertEquals($expectedArray['relationships']['itemsData']['data']['type'], 'itemsData');
            $this->assertEquals(
                $expectedArray['relationships']['itemsData']['data']['attributes']['data'],
                $errorData->getItemsData()->getData()
            );
        }
    }
}
