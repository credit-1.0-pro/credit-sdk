<?php
namespace Sdk\ResourceCatalog\Rule\Rule\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\RuleDataTrait;
use Sdk\ResourceCatalog\Rule\RuleRestfulUtils;
use Sdk\ResourceCatalog\Rule\Adapter\Rule\RuleRestfulAdapter;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有规则发布权限、且当我需要查看通过审核的规则的列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的规则信息,以便于我维护规则模块
 * @Scenario: 查看规则数据详情
 */
class DetailTest extends TestCase
{
    use RuleDataTrait, RuleRestfulUtils;

    private $rule;

    private $mock;

    public function setUp()
    {
        $this->rule = new Rule();
    }

    public function tearDown()
    {
        unset($this->rule);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条规则数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getRuleDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条规则数据详情时
    */
    protected function fetchRule($id)
    {
        $adapter = new RuleRestfulAdapter();

        $this->rule = $adapter->fetchOne($id);

        return $this->rule;
    }

    /**
     * @Then 我可以看见规则详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchRule($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'resourceCatalogs/rules/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getRuleDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->rule);
    }
}
