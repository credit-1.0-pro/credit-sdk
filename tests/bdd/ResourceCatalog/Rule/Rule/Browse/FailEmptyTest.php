<?php


namespace Sdk\ResourceCatalog\Rule\Rule\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\RuleDataTrait;
use Sdk\ResourceCatalog\Rule\RuleRestfulUtils;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有规则发布权限、且当我需要查看通过审核的规则的列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的规则信息,以便于我维护规则模块
 * @Scenario: 查看规则列表-不存在规则数据
 */
class FailEmptyTest extends TestCase
{
    use RuleDataTrait, RuleRestfulUtils, ErrorDataTrait;

    private $rule;

    private $mock;

    public function setUp()
    {
        $this->rule = new Rule();
    }

    public function tearDown()
    {
        unset($this->rule);
        unset($this->mock);
    }

    /**
     * @Given: 不存在规则数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
                new Response(404, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看规则列表时
     */
    public function fetchRuleList() : array
    {
        $filter = [];
        return $this->rule = $this->getRuleList($filter);
    }

    /**
     * @Then  我可以看见规则数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchRuleList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('resourceCatalogs/rules', $request->getUri()->getPath());
    }

    private function response()
    {
        $this->assertEmpty($this->rule);
    }
}
