<?php


namespace Sdk\ResourceCatalog\Rule\Rule\Edit;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\RuleDataTrait;
use Sdk\ResourceCatalog\Rule\RuleRestfulUtils;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

/**
 * @Feature: 我是拥有规则发布权限的平台管理员/委办局管理员/操作用户,当我需要编辑规则信息时,在发布表中,编辑对应规则数据
 *           通过编辑规则界面，并根据我所采集的规则数据进行编辑,以便于我可以更好的维护规则管理列表
 * @Scenario: 正常编辑规则数据
 */
class SuccessTest extends TestCase
{
    use RuleDataTrait, RuleRestfulUtils;

    private $editRule;

    private $mock;

    public function setUp()
    {
        $this->editRule = new Rule();
    }

    public function tearDown()
    {
        unset($this->editRule);
        unset($this->mock);
    }

    /**
     * @Given: 我编辑数据
     */
    protected function prepareData()
    {
        $data = $this->getRuleDetailData();
        $editJsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $editJsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用编辑函数,期待返回true
     */
    protected function edit()
    {
        $data = $this->getRuleEditRequestData()['data'];

        $this->editRule = new Rule();
        $this->editRule->setId(1);
        $this->editRule->setVersionDescription('规则版本描述信息');
        $this->editRule->setRules($data['attributes']['rules']);
        $this->editRule->getCrew()->setId($data['relationships']['crew']['data'][0]['id']);

        return $this->editRule->edit();
    }

    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->edit();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getRuleEditRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/resourceCatalogs/rules/1', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getRuleDetailData('规则版本描述信息')['data'];
        $data['id'] = 1;

        $this->compareArrayAndObjectCommon($data, $this->editRule);
    }
}
