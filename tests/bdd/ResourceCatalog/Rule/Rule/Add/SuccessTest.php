<?php


namespace Sdk\ResourceCatalog\Rule\Rule\Add;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\RuleDataTrait;
use Sdk\ResourceCatalog\Rule\RuleRestfulUtils;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

/**
 * @Feature: 我是拥有规则发布权限的平台管理员/委办局管理员/操作用户,当我需要新增一个规则时,在发布表中,新增对应的规则数据
 *           通过新增规则界面，并根据我所采集的规则数据进行新增,以便于我维护规则列表
 * @Scenario: 正常新增数据数据
 */
class SuccessTest extends TestCase
{
    use RuleDataTrait, RuleRestfulUtils, ErrorDataTrait;

    private $addRule;

    private $mock;

    public function setUp()
    {
        $this->addRule = new Rule();
    }

    public function tearDown()
    {
        unset($this->addRule);
        unset($this->mock);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getRuleDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $data = $this->getRuleDetailData()['data'];

        $this->addRule = new Rule();

        $this->addRule->setVersionDescription('规则版本描述信息');
        $this->addRule->setRules($data['attributes']['rules']);
        $this->addRule->getCrew()->setId($data['relationships']['crew']['data']['id']);
        $this->addRule->getTemplate()->setId($data['relationships']['template']['data']['id']);

        return $this->addRule->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getRuleAddRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/resourceCatalogs/rules', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getRuleDetailData('规则版本描述信息')['data'];

        $this->compareArrayAndObjectCommon($data, $this->addRule);
    }
}
