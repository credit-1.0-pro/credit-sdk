<?php


namespace Sdk\ResourceCatalog\Rule\Rule\Update;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\RuleDataTrait;
use Sdk\ResourceCatalog\Rule\RuleRestfulUtils;
use Sdk\ResourceCatalog\Rule\Adapter\Rule\RuleRestfulAdapter;

/**
 * @Feature: 我是拥有规则发布权限的平台管理员/委办局管理员/操作用户,当我需要回退到上个版本时,在规则管理规则详情页查看版本列表,
 *           我可以进行版本比对,查看到两个版本的差异点,以便于我可以更好的管理规则
 * @Scenario: 版本回退
 */
class VersionRestoreTest extends TestCase
{
    use RuleDataTrait, RuleRestfulUtils;

    private $rule;

    private $mock;

    public function setUp()
    {
        $this->rule = new Rule();
    }

    public function tearDown()
    {
        unset($this->rule);
        unset($this->mock);
    }

    /**
     * @Given: 存在需要版本回退的数据
     */
    protected function prepareData()
    {
        $data = $this->getRuleDetailData();
        $versionRestoreJsonData = json_encode($data);

        $data = $this->getRuleDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $versionRestoreJsonData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要版本回退的规则
     */
    protected function fetchRule()
    {
        $adapter = new RuleRestfulAdapter();
        $this->rule = $adapter->fetchOne(1);

        return $this->rule;
    }

    /**
     * @When:当我调用版本回退函数,期待返回true
     */
    protected function versionRestore()
    {
        $this->rule->getRuleVersion()->setId(1);
        $this->rule->getCrew()->setId(1);

        return $this->rule->versionRestore();
    }

    /**
     * @Then  我可以查到版本回退的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchRule();
        $this->versionRestore();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($this->getRuleStatusRequestData()), $request->getBody()->getContents());
        $this->assertEquals('/resourceCatalogs/rules/1/versionRestore/1', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getRuleDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->rule);
    }
}
