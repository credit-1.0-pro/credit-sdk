<?php


namespace Sdk\ResourceCatalog\Rule\Rule\Update;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\RuleDataTrait;
use Sdk\ResourceCatalog\Rule\RuleRestfulUtils;
use Sdk\ResourceCatalog\Rule\Adapter\Rule\RuleRestfulAdapter;

/**
 * @Feature: 超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则删除权限,在政务网OA的资源目录规则管理中,可以对设置的规则进行删除
 *           可以查看使用规则的情况或删除已经设置好的规则,以便于我可以了解我设置的规则并进行管理
 * @Scenario: 删除规则数据
 */
class DeleteTest extends TestCase
{
    use RuleDataTrait, RuleRestfulUtils;

    private $rule;

    private $mock;

    public function setUp()
    {
        $this->rule = new Rule();
    }

    public function tearDown()
    {
        unset($this->rule);
        unset($this->mock);
    }

    /**
     * @Given: 存在需要删除的数据
     */
    protected function prepareData()
    {
        $data = $this->getRuleDetailData();
        $deleteJsonData = json_encode($data);

        $data = $this->getRuleDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $deleteJsonData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要删除的规则
     */
    protected function fetchRule()
    {
        $adapter = new RuleRestfulAdapter();
        $this->rule = $adapter->fetchOne(1);

        return $this->rule;
    }

    /**
     * @When:当我调用删除函数,期待返回true
     */
    protected function delete()
    {
        $this->rule->getCrew()->setId(1);

        return $this->rule->delete();
    }

    /**
     * @Then  我可以查到删除的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchRule();
        $this->delete();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('DELETE', $request->getMethod());
        $this->assertEquals(json_encode($this->getRuleStatusRequestData()), $request->getBody()->getContents());
        $this->assertEquals('/resourceCatalogs/rules/1/delete', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getRuleDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->rule);
    }
}
