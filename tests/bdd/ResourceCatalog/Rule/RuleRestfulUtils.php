<?php
namespace Sdk\ResourceCatalog\Rule;

trait RuleRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $rule
    ) {
        if (isset($expectedArray['id'])) {
            $this->assertEquals($expectedArray['id'], $rule->getId());
        }
        
        if (isset($expectedArray['attributes']['versionDescription'])) {
            $this->assertEquals(
                $expectedArray['attributes']['versionDescription'],
                $rule->getVersionDescription()
            );
        }
        $this->assertEquals($expectedArray['attributes']['rules'], $rule->getRules());

        if (isset($expectedArray['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['attributes']['createTime'], $rule->getCreateTime());
        }
        if (isset($expectedArray['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['attributes']['statusTime'], $rule->getStatusTime());
        }
        if (isset($expectedArray['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['attributes']['updateTime'], $rule->getUpdateTime());
        }
        if (isset($expectedArray['attributes']['status'])) {
            $this->assertEquals($expectedArray['attributes']['status'], $rule->getStatus());
        }

        $this->compareCrew($expectedArray, $rule);
        $this->comparePublishUserGroup($expectedArray, $rule);
        $this->compareRuleVersion($expectedArray, $rule);
        $this->compareTemplateVersion($expectedArray, $rule);
        $this->compareTemplate($expectedArray, $rule);
    }

    private function compareCrew(array $expectedArray, $rule)
    {
        if (isset($expectedArray['relationships']['crew'])) {
            $this->assertEquals($expectedArray['relationships']['crew']['data']['type'], 'crews');
            $this->assertEquals(
                $expectedArray['relationships']['crew']['data']['id'],
                $rule->getCrew()->getId()
            );
        }
    }

    private function comparePublishUserGroup(array $expectedArray, $rule)
    {
        if (isset($expectedArray['relationships']['publishUserGroup'])) {
            $this->assertEquals($expectedArray['relationships']['publishUserGroup']['data']['type'], 'userGroups');
            $this->assertEquals(
                $expectedArray['relationships']['publishUserGroup']['data']['id'],
                $rule->getUserGroup()->getId()
            );
        }
    }

    private function compareRuleVersion(array $expectedArray, $rule)
    {
        if (isset($expectedArray['relationships']['ruleVersion'])) {
            $this->assertEquals(
                $expectedArray['relationships']['ruleVersion']['data']['type'],
                'ruleVersions'
            );
            $this->assertEquals(
                $expectedArray['relationships']['ruleVersion']['data']['id'],
                $rule->getRuleVersion()->getId()
            );
        }
    }

    private function compareTemplateVersion(array $expectedArray, $rule)
    {
        if (isset($expectedArray['relationships']['templateVersion'])) {
            $this->assertEquals(
                $expectedArray['relationships']['templateVersion']['data']['type'],
                'templateVersions'
            );
            $this->assertEquals(
                $expectedArray['relationships']['templateVersion']['data']['id'],
                $rule->getTemplateVersion()->getId()
            );
        }
    }

    private function compareTemplate(array $expectedArray, $rule)
    {
        if (isset($expectedArray['relationships']['template'])) {
            $this->assertEquals(
                $expectedArray['relationships']['template']['data']['type'],
                'templates'
            );
            $this->assertEquals(
                $expectedArray['relationships']['template']['data']['id'],
                $rule->getTemplate()->getId()
            );
        }
    }

    private function compareArrayAndObjectCommonUnAuditRule(
        array $expectedArray,
        $unAuditedRule
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditedRule);
        
        if (isset($expectedArray['attributes']['applyStatus'])) {
            $this->assertEquals(
                $expectedArray['attributes']['applyStatus'],
                $unAuditedRule->getApplyStatus()
            );
        }
        if (isset($expectedArray['attributes']['operationType'])) {
            $this->assertEquals(
                $expectedArray['attributes']['operationType'],
                $unAuditedRule->getOperationType()
            );
        }
        if (isset($expectedArray['attributes']['relationId'])) {
            $this->assertEquals(
                $expectedArray['attributes']['relationId'],
                $unAuditedRule->getRelationId()
            );
        }

        $this->assertEquals(
            $expectedArray['attributes']['rejectReason'],
            $unAuditedRule->getRejectReason()
        );

        if (isset($expectedArray['relationships'])) {
            $relationships = $expectedArray['relationships'];
            if (isset($relationships['applyCrew']['data'])) {
                $this->assertEquals(
                    $relationships['applyCrew']['data']['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['applyCrew']['data']['id'],
                    $unAuditedRule->getApplyCrew()->getId()
                );
            }
        }
    }

    private function compareArrayAndObjectRuleVersion(
        array $expectedArray,
        $ruleVersion
    ) {
        if (isset($expectedArray['id'])) {
            $this->assertEquals($expectedArray['id'], $ruleVersion->getId());
        }
        
        $this->assertEquals($expectedArray['attributes']['number'], $ruleVersion->getNumber());
        $this->assertEquals($expectedArray['attributes']['description'], $ruleVersion->getDescription());
        $this->assertEquals($expectedArray['attributes']['ruleId'], $ruleVersion->getRuleId());
        $this->assertEquals($expectedArray['attributes']['info'], $ruleVersion->getInfo());
       
        if (isset($expectedArray['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['attributes']['createTime'], $ruleVersion->getCreateTime());
        }
        if (isset($expectedArray['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['attributes']['statusTime'], $ruleVersion->getStatusTime());
        }
        if (isset($expectedArray['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['attributes']['updateTime'], $ruleVersion->getUpdateTime());
        }
        if (isset($expectedArray['attributes']['status'])) {
            $this->assertEquals($expectedArray['attributes']['status'], $ruleVersion->getStatus());
        }

        $this->compareCrew($expectedArray, $ruleVersion);
        $this->compareTemplate($expectedArray, $ruleVersion);
        $this->compareTemplateVersion($expectedArray, $ruleVersion);
        $this->comparePublishUserGroup($expectedArray, $ruleVersion);
    }
}
