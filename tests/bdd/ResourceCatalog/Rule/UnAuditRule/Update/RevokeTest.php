<?php


namespace Sdk\ResourceCatalog\Rule\UnAuditRule\Update;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Rule\RuleDataTrait;
use Sdk\ResourceCatalog\Rule\RuleRestfulUtils;
use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule\UnAuditRuleRestfulAdapter;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则管理权限,在政务网OA的资源目录规则管理中,我可以管理我提交的资源目录的规则
 *           可以撤销审核状态为待审核的资源目录规则,以便于我可以了解我设置的规则并进行管理
 * @Scenario: 撤销
 */
class RevokeTest extends TestCase
{
    use RuleDataTrait, RuleRestfulUtils;

    private $revokeUnAuditRule;

    private $mock;

    public function setUp()
    {
        $this->revokeUnAuditRule = new UnAuditRule();
    }

    public function tearDown()
    {
        unset($this->revokeUnAuditRule);
        unset($this->mock);
    }

    /**
     * @Given: 存在需要待审核的数据
     */
    protected function prepareData()
    {
        $data = $this->getUnAuditRuleDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要待审核的规则
     */
    protected function fetchUnAuditRule()
    {
        $adapter = new UnAuditRuleRestfulAdapter();
        $this->revokeUnAuditRule = $adapter->fetchOne(1);

        return $this->revokeUnAuditRule;
    }

    /**
     * @When:当我调用撤销函数,期待返回true
     */
    protected function revoke()
    {
        $this->revokeUnAuditRule->getCrew()->setId(2);

        return $this->revokeUnAuditRule->revoke();
    }

    /**
     * @Then  我可以查到待审核的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUnAuditRule();
        $this->revoke();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(
            json_encode($this->getUnAuditRuleStatusRequestData()),
            $request->getBody()->getContents()
        );
        $this->assertEquals('/resourceCatalogs/unAuditedRules/1/revoke', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getUnAuditRuleDetailData()['data'];

        $this->compareArrayAndObjectCommonUnAuditRule($data, $this->revokeUnAuditRule);
    }
}
