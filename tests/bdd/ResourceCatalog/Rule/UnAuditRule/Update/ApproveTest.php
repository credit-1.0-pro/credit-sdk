<?php


namespace Sdk\ResourceCatalog\Rule\UnAuditRule\Update;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Rule\RuleDataTrait;
use Sdk\ResourceCatalog\Rule\RuleRestfulUtils;
use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule\UnAuditRuleRestfulAdapter;

/**
 * @Feature: 我是拥有规则审核权限的平台管理员/委办局管理员/操作用户,当我需要审核一个规则时,在审核表中,审核待审核的规则数据
 *           通过规则详情页面的审核通过与审核驳回操作,以便于我维护规则列表
 * @Scenario: 审核通过
 */
class ApproveTest extends TestCase
{
    use RuleDataTrait, RuleRestfulUtils;

    private $unAuditRule;

    private $mock;

    public function setUp()
    {
        $this->unAuditRule = new UnAuditRule();
    }

    public function tearDown()
    {
        unset($this->unAuditRule);
        unset($this->mock);
    }

    /**
     * @Given: 存在需要待审核的数据
     */
    protected function prepareData()
    {
        $data = $this->getUnAuditRuleDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要待审核的规则
     */
    protected function fetchUnAuditRule()
    {
        $adapter = new UnAuditRuleRestfulAdapter();
        $this->unAuditRule = $adapter->fetchOne(1);

        return $this->unAuditRule;
    }

    /**
     * @When:当我调用待审核函数,期待返回true
     */
    protected function approve()
    {
        $this->unAuditRule->getApplyCrew()->setId(2);

        return $this->unAuditRule->approve();
    }

    /**
     * @Then  我可以查到待审核的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUnAuditRule();
        $this->approve();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(
            json_encode($this->getUnAuditRuleApproveRequestData()),
            $request->getBody()->getContents()
        );
        $this->assertEquals('/resourceCatalogs/unAuditedRules/1/approve', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getUnAuditRuleDetailData()['data'];

        $this->compareArrayAndObjectCommonUnAuditRule($data, $this->unAuditRule);
    }
}
