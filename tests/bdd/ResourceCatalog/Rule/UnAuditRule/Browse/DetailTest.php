<?php
namespace Sdk\ResourceCatalog\Rule\UnAuditRule\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\ResourceCatalog\Rule\RuleDataTrait;
use Sdk\ResourceCatalog\Rule\RuleRestfulUtils;
use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;
use Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule\UnAuditRuleRestfulAdapter;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户,我拥有规则审核权限、且当我需要查看未审核或已驳回的规则列表时,
 *           在审核表中,通过列表与详情的形式查看到未审核或已驳回的规则信息,以便于我维护规则模块
 * @Scenario: 查看规则审核数据详情
 */
class DetailTest extends TestCase
{
    use RuleDataTrait, RuleRestfulUtils;

    private $unAuditRule;

    private $mock;

    public function setUp()
    {
        $this->unAuditRule = new UnAuditRule();
    }

    public function tearDown()
    {
        unset($this->unAuditRule);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条规则数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getUnAuditRuleDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条规则数据详情时
    */
    protected function fetchUnAuditRule($id)
    {
        $adapter = new UnAuditRuleRestfulAdapter();

        $this->unAuditRule = $adapter->fetchOne($id);

        return $this->unAuditRule;
    }

    /**
     * @Then 我可以看见规则详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchUnAuditRule($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'resourceCatalogs/unAuditedRules/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getUnAuditRuleDetailData()['data'];

        $this->compareArrayAndObjectCommonUnAuditRule($data, $this->unAuditRule);
    }
}
