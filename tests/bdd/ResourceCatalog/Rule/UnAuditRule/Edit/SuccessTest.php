<?php


namespace Sdk\ResourceCatalog\Rule\UnAuditRule\Edit;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Rule\RuleDataTrait;
use Sdk\ResourceCatalog\Rule\RuleRestfulUtils;
use Sdk\ResourceCatalog\Rule\Model\UnAuditRule;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

/**
 * @Feature: 我是拥有规则发布权限的平台管理员/委办局管理员/操作用户,当我需要编辑规则信息时,在审核表中,编辑已驳回规则数据
 *           通过编辑规则界面，并根据我所采集的规则数据进行编辑,以便于我可以更好的维护规则管理列表
 * @Scenario: 正常编辑规则审核数据
 */
class SuccessTest extends TestCase
{
    use RuleDataTrait, RuleRestfulUtils;

    private $unAuditedRule;

    private $mock;

    public function setUp()
    {
        $this->unAuditedRule = new UnAuditRule();
    }

    public function tearDown()
    {
        unset($this->unAuditedRule);
        unset($this->mock);
    }

    /**
     * @Given: 我编辑数据
     */
    protected function prepareData()
    {
        $data = $this->getUnAuditRuleDetailData();
        $editJsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $editJsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用编辑函数,期待返回true
     */
    protected function edit()
    {
        $data = $this->getUnAuditRuleEditRequestData()['data'];

        $this->unAuditedRule = new UnAuditRule();
        $this->unAuditedRule->setId(1);

        $this->unAuditedRule->setVersionDescription('规则版本描述信息');
        $this->unAuditedRule->setRules($data['attributes']['rules']);
        $this->unAuditedRule->getCrew()->setId($data['relationships']['crew']['data'][0]['id']);

        return $this->unAuditedRule->edit();
    }

    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->edit();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getUnAuditRuleEditRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/resourceCatalogs/unAuditedRules/1/resubmit', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getUnAuditRuleDetailData('规则版本描述信息')['data'];

        $this->compareArrayAndObjectCommonUnAuditRule($data, $this->unAuditedRule);
    }
}
