<?php


namespace Sdk\ResourceCatalog\Rule;

use Sdk\ResourceCatalog\Rule\Model\Rule;
use Sdk\ResourceCatalog\Rule\Adapter\Rule\RuleRestfulAdapter;
use Sdk\ResourceCatalog\Rule\Adapter\RuleVersion\RuleVersionRestfulAdapter;
use Sdk\ResourceCatalog\Rule\Adapter\UnAuditRule\UnAuditRuleRestfulAdapter;

trait RuleDataTrait
{
    protected function getRuleList(array $filter = []) : array
    {
        $adapter = new RuleRestfulAdapter();

        list($count, $ruleList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $ruleList;
    }

    protected function getUnAuditRuleList(array $filter = []) : array
    {
        $adapter = new UnAuditRuleRestfulAdapter();

        list($count, $unAuditRuleList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $unAuditRuleList;
    }

    protected function getRuleVersionList(array $filter = []) : array
    {
        $adapter = new RuleVersionRestfulAdapter();

        list($count, $ruleVersionList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $ruleVersionList;
    }

    protected function getRuleCommonAttributesData($versionDescription = '') : array
    {
        return array(
            "versionDescription" => $versionDescription,    //规则版本描述
            "rules" => array(
                'completionRule' =>  array(
                    'ZTMC' => array(
                        array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                        array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                    ),
                    'TYSHXYDM' => array(
                        array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                        array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                    ),
                ),
                'comparisonRule' =>  array(
                    'ZTMC' => array(
                        array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                        array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                    ),
                    'TYSHXYDM' => array(
                        array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                        array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                    ),
                ),
                'deDuplicationRule' => array("result"=>1, "items"=>array("ZTMC","TYSHXYDM"))
            )
        );
    }

    protected function getRuleAttributesData($versionDescription = '') : array
    {
        $attributes = $this->getRuleCommonAttributesData($versionDescription);
        $attributes['status'] = 0;
        $attributes['createTime'] = 1654673891;
        $attributes['updateTime'] = 1654673891;
        $attributes['statusTime'] = 1654673891;

        return $attributes;
    }

    protected function getUnAuditRuleAttributesData($versionDescription = '') : array
    {
        $attributes = $this->getRuleAttributesData($versionDescription);
        $attributes['applyStatus'] = 0;
        $attributes['rejectReason'] = "";
        $attributes['operationType'] = 1;
        $attributes['ruleId'] = 1;
        
        return $attributes;
    }

    protected function getRuleRequestRelationshipsData() : array
    {
        return array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            )
        );
    }

    protected function getRuleRelationshipsData() : array
    {
        $relationships['crew'] = array("data"=>array("type"=>"crews","id"=>1));
        $relationships['publishUserGroup'] = array("data"=>array("type"=>"userGroups","id"=>1));
        $relationships['ruleVersion'] = array("data"=>array("type"=>"ruleVersions","id"=>1));
        $relationships['templateVersion'] = array("data"=>array("type"=>"templateVersions","id"=>1));
        $relationships['template'] = array("data"=>array("type"=>"templates","id"=>1));

        return $relationships;
    }

    protected function getUnAuditRuleRelationshipsData() : array
    {
        $relationships = $this->getRuleRelationshipsData();
        $relationships['applyCrew'] = array("data"=>array("type"=>"crews","id"=>1));
        $relationships['publishCrew'] = array("data"=>array("type"=>"crews","id"=>1));
        $relationships['applyUserGroup'] = array("data"=>array("type"=>"userGroups","id"=>1));

        return $relationships;
    }

    protected function getRuleCommonIncludedData()
    {
        return [
            $this->getUserGroupIncluded(),
            $this->getRuleVersionIncluded(),
            $this->getTemplateVersionIncluded(),
            $this->getTemplateIncluded(),
            $this->getCrewIncluded()
        ];
    }

    protected function getUserGroupIncluded() : array
    {
        return
        [
            "type" => "userGroups",
            "id" => "1",
            "attributes" => [
                "name" => "市发展和改革委员会",
                "shortName" => "市发改委",
                "unifiedSocialCreditCode" => "123456789101234567",
                "administrativeArea" => 0,
                "status" => 0,
                "createTime" => 1652949496,
                "updateTime" => 1653114283,
                "statusTime" => 0
            ]
        ];
    }

    protected function getCrewIncluded() : array
    {
        return
        [
            "type" => "crews",
            "id" => "1",
            "attributes" => [
                "realName" => "戚岚",
                "cardId" => "412825198708023456",
                "userName" => "18800000012",
                "cellphone" => "18800000012",
                "category" => 4,
                "purview" => [
                    "1",
                    "2"
                ],
                "status" => 0,
                "createTime" => 1655198072,
                "updateTime" => 1655198195,
                "statusTime" => 0
            ],
            "relationships" => [
                "userGroup" => [
                    "data" => [
                        "type" => "userGroups",
                        "id" => "1"
                    ]
                ],
                "department" => [
                    "data" => [
                        "type" => "departments",
                        "id" => "1"
                    ]
                ]
            ]
        ];
    }

    protected function getTemplateItems() : array
    {
        return [
            [
                "name" => "姓名",
                "type" => "1",
                "length" => "200",
                "options" => [],
                "remarks" => "姓名",
                "identify" => "ZTMC",
                "isMasked" => "0",
                "maskRule" => [],
                "dimension" => "1",
                "isNecessary" => "1"
            ],
            [
                "name" => "身份证号",
                "type" => "1",
                "length" => "18",
                "options" => [],
                "remarks" => "身份证号",
                "identify" => "ZJHM",
                "isMasked" => "0",
                "maskRule" => [],
                "dimension" => "1",
                "isNecessary" => "1"
            ]
        ];
    }
    
    protected function templateVersionInfo() : array
    {
        return
        [
            "name" => "自然人登记信息",
            "items" => $this->getTemplateItems(),
            "status" => "0",
            "crew_id" => "1",
            "identify" => "ZRR_DJXX",
            "dimension" => "1",
            "create_time" => "1656492285",
            "description" => "自然人登记信息",
            "status_time" => "0",
            "template_id" => "0",
            "update_time" => "1656492285",
            "source_units" => [
                "1" => [
                    "user_group_id" => 1,
                    "name" => "市发展和改革委员会",
                    "short_name" => "市发改委",
                    "unified_social_credit_code" => "123456789012345678",
                    "administrative_area" => 0,
                    "status" => 0,
                    "create_time" => 1652949496,
                    "update_time" => 1652949525,
                    "status_time" => 0
                ]
            ],
            "info_category" => "1",
            "info_classify" => "1",
            "subject_category" => ["2"],
            "exchange_frequency" => "1",
            "template_version_id" => "0",
            "publish_usergroup_id" => "1"
        ];
    }
    
    protected function getTemplateVersionIncluded() : array
    {
        return
        [
            "type" => "templateVersions",
            "id" => "1",
            "attributes" => [
                "number" => "2206292118",
                "description" => "自然人人登记信息目录归集",
                "templateId" => 1,
                "info" => $this->templateVersionInfo(),
                "status" => 0,
                "createTime" => 1656492472,
                "updateTime" => 1656492472,
                "statusTime" => 1656492472
            ],
            "relationships" => [
                "publishUserGroup" => [
                    "data" => [
                        "type" => "userGroups",
                        "id" => "1"
                    ]
                ],
                "crew" => [
                    "data" => [
                        "type" => "crews",
                        "id" => "1"
                    ]
                ]
            ]
        ];
    }

    protected function getTemplateIncluded() : array
    {
        return
        [
            "type" => "templates",
            "id" => "1",
            "attributes" => [
                "name"=> "自然人登记信息",
                "identify"=> "ZRR_DJXX",
                "description" => "自然人登记信息目录归集",
                "subjectCategory" => array(2),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
                "items" => $this->getTemplateItems(),
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "exchangeFrequency" => 1,    //更新频率
                "infoClassify" => 1,    //信息分类
                "infoCategory" => 1,    //信息类别
                "status" => 0,
                "createTime" => 1656493761,
                "updateTime" => 1656493990,
                "statusTime" => 1656493990
            ],
            "relationships" => [
                "publishUserGroup" => [
                    "data" => [
                        "type" => "userGroups",
                        "id" => "1"
                    ]
                ],
                "crew" => [
                    "data" => [
                        "type" => "crews",
                        "id" => "1"
                    ]
                ],
                "templateVersion" => [
                    "data" => [
                        "type" => "templateVersions",
                        "id" => "1"
                    ]
                ],
                "sourceUnits" => [
                    "data" => [
                        [
                            "type" => "userGroups",
                            "id" => "1"
                        ],
                        [
                            "type" => "userGroups",
                            "id" => "2"
                        ]
                    ]
                ],
            ]
        ];
    }

    protected function ruleVersionInfo() : array
    {
        return
        [
            "rules" => [
                "completionRule" => array(
                    "ZTMC" => array(
                        array(
                            "id" => 2,
                            "versionId"=> 2,
                            "name"=> "法人登记信息",
                            "base" => array(2),
                            "item" =>"ZTMC",
                            "itemName" => "主体名称"
                        )
                    )
                ),
                "completionRule" => array(
                    "TYSHXYDM" => array(
                        array(
                            "id" => 2,
                            "versionId"=> 2,
                            "name"=> "法人登记信息",
                            "base" => array(1),
                            "item" =>"TYSHXYDM",
                            "itemName" => "统一社会信用代码"
                        )
                    )
                ),
                "deDuplicationRule" => array("result" => 1, "items" => array("ZTMC"))
            ],
            "status" => "0",
            "crew_id" => "1",
            "rule_id" => "0",
            "create_time" => "1659406202",
            "status_time" => "0",
            "update_time" => "1659406304",
            "template_id" => "1",
            "rule_version_id" => "0",
            "template_version_id" => "1",
            "publish_usergroup_id" => "1"
        ];
    }
    
    protected function getRuleVersionIncluded() : array
    {
        return
        [
            "type" => "ruleVersions",
            "id" => "1",
            "attributes" => [
                "number" => "2208122118",
                "description" => "自然人人登记信息规则设置",
                "ruleId" => 1,
                "info" => $this->ruleVersionInfo(),
                "status" => 0,
                "createTime" => 1656492472,
                "updateTime" => 1656492472,
                "statusTime" => 1656492472
            ],
            "relationships" => [
                "publishUserGroup" => [
                    "data" => [
                        "type" => "userGroups",
                        "id" => "1"
                    ]
                ],
                "crew" => [
                    "data" => [
                        "type" => "crews",
                        "id" => "1"
                    ]
                ],
                "templateVersion" => [
                    "data" => [
                        "type" => "templateVersions",
                        "id" => "1"
                    ]
                ],
                "template" => [
                    "data" => [
                        "type" => "templates",
                        "id" => "1"
                    ]
                ],
            ]
        ];
    }

    protected function getRuleListData($versionDescription = ''): array
    {
        $attributes = $this->getRuleAttributesData($versionDescription);
        $relationships = $this->getRuleRelationshipsData();

        return [
            "meta" => [
                "count" => 4,
                "links" => [
                    "first" => null,
                    "last" => null,
                    "prev" => null,
                    "next" => null
                ]
            ],
            "links" => [
                "first" => null,
                "last" => null,
                "prev" => null,
                "next" => null
            ],
            "data" => [
                [
                    "type" => "rules",
                    "id" => "15",
                    "attributes" =>$attributes,
                    "relationships" =>$relationships,
                    "links" => [
                        "self" => "127.0.0.1:8080/rules/15"
                    ]
                ],
            ],
            "included" => $this->getRuleCommonIncludedData()
        ];
    }

    protected function getRuleDetailData($versionDescription = '') : array
    {
        $attributes = $this->getRuleAttributesData($versionDescription);
        $relationships = $this->getRuleRelationshipsData();
        
        return [
            "meta" => [],
            "data" => [
                "type" => "rules",
                "id" => 1,
                "attributes" => $attributes,
                "relationships" => $relationships,
                "links" => [
                    "self" => "127.0.0.1:8080/rules/1"
                ]
            ],
            "included" => $this->getRuleCommonIncludedData()
        ];
    }

    protected function getUnAuditRuleListData(): array
    {
        $attributes = $this->getUnAuditRuleAttributesData();
        $relationships = $this->getUnAuditRuleRelationshipsData();

        return [
            "meta" => [
                "count" => 4,
                "links" => [
                    "first" => null,
                    "last" => null,
                    "prev" => null,
                    "next" => null
                ]
            ],
            "links" => [
                "first" => null,
                "last" => null,
                "prev" => null,
                "next" => null
            ],
            "data" => [
                [
                    "type" => "unAuditedRules",
                    "id" => "15",
                    "attributes" =>$attributes,
                    "relationships" =>$relationships,
                    "links" => [
                        "self" => "127.0.0.1:8080/unAuditedRules/15"
                    ]
                ],
            ],
            "included" => $this->getRuleCommonIncludedData()
        ];
    }

    protected function getUnAuditRuleDetailData($versionDescription = '') : array
    {
        $attributes = $this->getUnAuditRuleAttributesData($versionDescription);
        $relationships = $this->getUnAuditRuleRelationshipsData();
        
        return [
            "meta" => [],
            "data" => [
                "type" => "unAuditedRules",
                "id" => 1,
                "attributes" => $attributes,
                "relationships" => $relationships,
                "links" => [
                    "self" => "127.0.0.1:8080/unAuditedRules/1"
                ]
            ],
            "included" => $this->getRuleCommonIncludedData()
        ];
    }

    protected function getRuleAddRequestData() : array
    {
        $attributes = $this->getRuleCommonAttributesData('规则版本描述信息');
        $relationships = $this->getRuleRequestRelationshipsData();
        $relationships['template'] = array(
            "data"=>array(
                array("type"=>"templates","id"=>1)
            )
        );

        return array(
            'data' => array(
                "type"=>"rules",
                "attributes"=>$attributes,
                "relationships"=>$relationships,
            )
        );
    }

    protected function getRuleEditRequestData() : array
    {
        $attributes = $this->getRuleCommonAttributesData('规则版本描述信息');
        $relationships = $this->getRuleRequestRelationshipsData();

        return array(
            'data' => array(
                "type"=>"rules",
                "attributes"=>$attributes,
                "relationships"=>$relationships,
            )
        );
    }

    protected function getUnAuditRuleEditRequestData() : array
    {
        $attributes = $this->getRuleCommonAttributesData('规则版本描述信息');
        $relationships = $this->getRuleRequestRelationshipsData();

        return array(
            'data' => array(
                "type"=>"unAuditedRules",
                "attributes"=>$attributes,
                "relationships"=>$relationships,
            )
        );
    }

    protected function getUnAuditRuleApproveRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"unAuditedRules",
                "attributes"=>array(),
                "relationships"=>array(
                    "applyCrew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );
    }

    protected function getUnAuditRuleRejectRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"unAuditedRules",
                "attributes"=>array("rejectReason"=>"规则-审核驳回-数据不合理"),
                "relationships"=>array(
                    "applyCrew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );
    }

    protected function getRuleStatusRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"rules",
                "attributes"=>array(),
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );
    }
    
    protected function getUnAuditRuleStatusRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"unAuditedRules",
                "attributes"=>array(),
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );
    }
    
    protected function getRuleVersionDetailData(): array
    {
        return
        [
            "meta" => [],
            "data" => $this->getRuleVersionIncluded(),
            "included" => [
                $this->getUserGroupIncluded(),
                $this->getCrewIncluded(),
                $this->getTemplateIncluded(),
                $this->getTemplateVersionIncluded()
            ]
        ];
    }

    protected function getRuleVersionListData() : array
    {
        return [
            "meta" => [
                "count" => 3,
                "links" => [
                    "first" => 1,
                    "last" => 3,
                    "prev" => null,
                    "next" => 2
                ]
            ],
            "links" => [],
            "data" => [$this->getRuleVersionIncluded()],
            "included" => [
                $this->getUserGroupIncluded(),
                $this->getCrewIncluded(),
                $this->getTemplateIncluded(),
                $this->getTemplateVersionIncluded()
            ]
        ];
    }
}
