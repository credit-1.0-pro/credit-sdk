<?php


namespace Sdk\ResourceCatalog\Rule\RuleVersion\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Rule\RuleDataTrait;
use Sdk\ResourceCatalog\Rule\RuleRestfulUtils;
use Sdk\ResourceCatalog\Rule\Model\RuleVersion;

/**
 * @Feature: 我是拥有规则发布权限的平台管理员/委办局管理员/操作用户,当我需要查看我的规则版本记录时,在规则管理规则详情页
 *           可以查看到该规则下的所有版本记录,通过列表和详情的形式查看我所有的版本记录,以便于我可以更好的溯源历史记录
 * @Scenario: 查看规则版本记录数据
 */
class ListTest extends TestCase
{
    use RuleDataTrait, RuleRestfulUtils;

    private $ruleVersion;

    private $mock;

    public function setUp()
    {
        $this->ruleVersion = new RuleVersion();
    }

    public function tearDown()
    {
        unset($this->ruleVersion);
        unset($this->mock);
    }

    /**
     * @Given: 存在规则版本记录数据
     */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getRuleVersionListData());

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看规则版本记录列表时
     */
    public function fetchRuleVersionList() : array
    {
        $filter = [];
        return $this->getRuleVersionList($filter);
    }

    /**
     * @Then  我可以看见规则版本记录数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchRuleVersionList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('resourceCatalogs/ruleVersions', $request->getUri()->getPath());
    }

    private function response()
    {
        $ruleVersionList = $this->fetchRuleVersionList();
        $ruleVersionListArray = $this->getRuleVersionListData()['data'];
        foreach ($ruleVersionList as $ruleVersion) {
            foreach ($ruleVersionListArray as $ruleVersionArray) {
                if ($ruleVersionArray['id'] == $ruleVersion->getId()) {
                    $this->compareArrayAndObjectRuleVersion($ruleVersionArray, $ruleVersion);
                }
            }
        }
    }
}
