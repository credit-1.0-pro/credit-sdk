<?php
namespace Sdk\ResourceCatalog\Rule\RuleVersion\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\ResourceCatalog\Rule\RuleDataTrait;
use Sdk\ResourceCatalog\Rule\RuleRestfulUtils;
use Sdk\ResourceCatalog\Rule\Model\RuleVersion;
use Sdk\ResourceCatalog\Rule\Adapter\RuleVersion\RuleVersionRestfulAdapter;

/**
 * @Feature: 我是拥有规则发布权限的平台管理员/委办局管理员/操作用户,当我需要查看我的规则版本记录时,在规则管理规则详情页
 *           可以查看到该规则下的所有版本记录,通过列表和详情的形式查看我所有的版本记录,以便于我可以更好的溯源历史记录
 * @Scenario: 查看规则版本记录数据详情
 */
class DetailTest extends TestCase
{
    use RuleDataTrait, RuleRestfulUtils;

    private $ruleVersion;

    private $mock;

    public function setUp()
    {
        $this->ruleVersion = new RuleVersion();
    }

    public function tearDown()
    {
        unset($this->ruleVersion);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条规则版本记录数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getRuleVersionDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条规则版本记录数据详情时
    */
    protected function fetchRuleVersion($id)
    {
        $adapter = new RuleVersionRestfulAdapter();

        $this->ruleVersion = $adapter->fetchOne($id);

        return $this->ruleVersion;
    }

    /**
     * @Then 我可以看见规则版本记录详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchRuleVersion($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'resourceCatalogs/ruleVersions/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getRuleVersionDetailData()['data'];

        $this->compareArrayAndObjectRuleVersion($data, $this->ruleVersion);
    }
}
