<?php


namespace Sdk\ResourceCatalog\SearchData\Update;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\SearchDataDataTrait;
use Sdk\ResourceCatalog\SearchData\SearchDataRestfulUtils;
use Sdk\ResourceCatalog\SearchData\Adapter\SearchData\SearchDataRestfulAdapter;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,当我需要审核一个资源目录数据时,
 * 在资源目录管理中的资源目录审核数据下,查看待审核的数据,通过详情页面的审核通过与审核驳回操作,以便于我维护数据列表
 * @Scenario: 审核驳回
 */
class RejectTest extends TestCase
{
    use SearchDataDataTrait, SearchDataRestfulUtils;

    private $rejectSearchData;

    private $mock;

    public function setUp()
    {
        $this->rejectSearchData = new SearchData();
    }

    public function tearDown()
    {
        unset($this->rejectSearchData);
        unset($this->mock);
    }

    /**
     * @Given: 存在需要待审核的数据
     */
    protected function prepareData()
    {
        $data = $this->getSearchDataDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要待审核的目录
     */
    protected function fetchSearchData()
    {
        $adapter = new SearchDataRestfulAdapter();
        $this->rejectSearchData = $adapter->fetchOne(1);

        return $this->rejectSearchData;
    }

    /**
     * @When:当我调用待审核函数,期待返回true
     */
    protected function reject()
    {
        $this->rejectSearchData->setRejectReason("审核驳回原因");

        return $this->rejectSearchData->reject();
    }

    /**
     * @Then  我可以查到待审核的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchSearchData();
        $this->reject();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(
            json_encode($this->getSearchDataRejectRequestData()),
            $request->getBody()->getContents()
        );
        $this->assertEquals('/resourceCatalogs/searchData/1/reject', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getSearchDataDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->rejectSearchData);
    }
}
