<?php


namespace Sdk\ResourceCatalog\SearchData\Update;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\SearchDataDataTrait;
use Sdk\ResourceCatalog\SearchData\SearchDataRestfulUtils;
use Sdk\ResourceCatalog\SearchData\Adapter\SearchData\SearchDataRestfulAdapter;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,当我需要屏蔽资源目录数据时,
 * 在资源目录管理中的资源目录数据下,屏蔽对应的资源目录数据,根据我核实需要屏蔽的数据屏蔽,以便于我实现平台资源目录数据的呈现
 * @Scenario: 屏蔽资源目录数据
 */
class DeletedTest extends TestCase
{
    use SearchDataDataTrait, SearchDataRestfulUtils;

    private $deletedStub;

    private $mock;

    public function setUp()
    {
        $this->deletedStub = new SearchData();
    }

    public function tearDown()
    {
        unset($this->deletedStub);
        unset($this->mock);
    }

    /**
     * @Given: 存在需要待审核的数据
     */
    protected function prepareData()
    {
        $data = $this->getSearchDataDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要待审核的目录
     */
    protected function fetchSearchData()
    {
        $adapter = new SearchDataRestfulAdapter();
        $this->deletedStub = $adapter->fetchOne(1);

        return $this->deletedStub;
    }

    /**
     * @When:当我调用待审核函数,期待返回true
     */
    protected function deleted()
    {
        return $this->deletedStub->deleted();
    }

    /**
     * @Then  我可以查到待审核的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchSearchData();
        $this->deleted();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals('/resourceCatalogs/searchData/1/delete', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getSearchDataDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->deletedStub);
    }
}
