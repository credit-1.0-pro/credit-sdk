<?php
namespace Sdk\ResourceCatalog\SearchData\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\SearchDataDataTrait;
use Sdk\ResourceCatalog\SearchData\SearchDataRestfulUtils;
use Sdk\ResourceCatalog\SearchData\Adapter\SearchData\SearchDataRestfulAdapter;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 * 当我需要查看我的资源目录数据时,在资源目录管理中,可以查看到我所属委办局下的所有资源目录
 * 通过列表和详情的形式查看我所有的资源目录,以便于我可以精确的查看到不同资源目录下的数据
 * @Scenario: 查看资源目录数据详情
 */
class DetailTest extends TestCase
{
    use SearchDataDataTrait, SearchDataRestfulUtils;

    private $searchData;

    private $mock;

    public function setUp()
    {
        $this->searchData = new SearchData();
    }

    public function tearDown()
    {
        unset($this->searchData);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条目录数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getSearchDataDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条目录数据详情时
    */
    protected function fetchSearchData($id)
    {
        $adapter = new SearchDataRestfulAdapter();

        $this->searchData = $adapter->fetchOne($id);

        return $this->searchData;
    }

    /**
     * @Then 我可以看见目录详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchSearchData($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'resourceCatalogs/searchData/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getSearchDataDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->searchData);
    }
}
