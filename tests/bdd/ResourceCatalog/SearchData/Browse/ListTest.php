<?php


namespace Sdk\ResourceCatalog\SearchData\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\SearchDataDataTrait;
use Sdk\ResourceCatalog\SearchData\SearchDataRestfulUtils;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 * 当我需要查看我的资源目录数据时,在资源目录管理中,可以查看到我所属委办局下的所有资源目录
 * 通过列表和详情的形式查看我所有的资源目录,以便于我可以精确的查看到不同资源目录下的数据
 * @Scenario: 查看资源目录数据列表
 */
class ListTest extends TestCase
{
    use SearchDataDataTrait, SearchDataRestfulUtils;

    private $searchData;

    private $mock;

    public function setUp()
    {
        $this->searchData = new SearchData();
    }

    public function tearDown()
    {
        unset($this->searchData);
        unset($this->mock);
    }

    /**
     * @Given: 存在目录数据
     */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getSearchDataListData());

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看目录列表时
     */
    public function fetchSearchDataList() : array
    {
        $filter = [];
        return $this->getSearchDataList($filter);
    }

    /**
     * @Then  我可以看见目录数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchSearchDataList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('resourceCatalogs/searchData', $request->getUri()->getPath());
    }

    private function response()
    {
        $searchDataList = $this->fetchSearchDataList();
        $searchDataListArray = $this->getSearchDataListData()['data'];

        foreach ($searchDataList as $searchData) {
            foreach ($searchDataListArray as $searchDataArray) {
                if ($searchDataArray['id'] == $searchData->getId()) {
                    $this->compareArrayAndObjectCommon($searchDataArray, $searchData);
                }
            }
        }
    }
}
