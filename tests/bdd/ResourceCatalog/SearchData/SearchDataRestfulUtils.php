<?php
namespace Sdk\ResourceCatalog\SearchData;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait SearchDataRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $searchData
    ) {
        if (isset($expectedArray['id'])) {
            $this->assertEquals($expectedArray['id'], $searchData->getId());
        }

        if (isset($expectedArray['attributes']['name'])) {
            $this->assertEquals($expectedArray['attributes']['name'], $searchData->getName());
        }
        if (isset($expectedArray['attributes']['identify'])) {
            $this->assertEquals($expectedArray['attributes']['identify'], $searchData->getIdentify());
        }
        if (isset($expectedArray['attributes']['expirationDate'])) {
            $this->assertEquals($expectedArray['attributes']['expirationDate'], $searchData->getExpirationDate());
        }
        if (isset($expectedArray['attributes']['subjectCategory'])) {
            $this->assertEquals($expectedArray['attributes']['subjectCategory'], $searchData->getSubjectCategory());
        }
        if (isset($expectedArray['attributes']['dimension'])) {
            $this->assertEquals($expectedArray['attributes']['dimension'], $searchData->getDimension());
        }
        if (isset($expectedArray['attributes']['exchangeFrequency'])) {
            $this->assertEquals(
                $expectedArray['attributes']['exchangeFrequency'],
                $searchData->getExchangeFrequency()
            );
        }
        if (isset($expectedArray['attributes']['infoClassify'])) {
            $this->assertEquals($expectedArray['attributes']['infoClassify'], $searchData->getInfoClassify());
        }
        if (isset($expectedArray['attributes']['infoCategory'])) {
            $this->assertEquals($expectedArray['attributes']['infoCategory'], $searchData->getInfoCategory());
        }
        if (isset($expectedArray['attributes']['applyStatus'])) {
            $this->assertEquals($expectedArray['attributes']['applyStatus'], $searchData->getApplyStatus());
        }
        if (isset($expectedArray['attributes']['rejectReason'])) {
            $this->assertEquals($expectedArray['attributes']['rejectReason'], $searchData->getRejectReason());
        }
        if (isset($expectedArray['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['attributes']['createTime'], $searchData->getCreateTime());
        }
        if (isset($expectedArray['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['attributes']['statusTime'], $searchData->getStatusTime());
        }
        if (isset($expectedArray['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['attributes']['updateTime'], $searchData->getUpdateTime());
        }
        if (isset($expectedArray['attributes']['status'])) {
            $this->assertEquals($expectedArray['attributes']['status'], $searchData->getStatus());
        }

        $this->comparePublishUserGroup($expectedArray, $searchData);
        $this->compareTemplateVersion($expectedArray, $searchData);
        $this->compareCrew($expectedArray, $searchData);
        $this->compareItemsData($expectedArray, $searchData);
    }

    private function comparePublishUserGroup(array $expectedArray, $searchData)
    {
        if (isset($expectedArray['relationships']['publishUserGroup'])) {
            $this->assertEquals($expectedArray['relationships']['publishUserGroup']['data']['type'], 'userGroups');
            $this->assertEquals(
                $expectedArray['relationships']['publishUserGroup']['data']['id'],
                $searchData->getPublishUserGroup()->getId()
            );
        }
    }

    private function compareCrew(array $expectedArray, $searchData)
    {
        if (isset($expectedArray['relationships']['crew'])) {
            $this->assertEquals($expectedArray['relationships']['crew']['data']['type'], 'crews');
            $this->assertEquals(
                $expectedArray['relationships']['crew']['data']['id'],
                $searchData->getCrew()->getId()
            );
        }
    }

    private function compareTemplateVersion(array $expectedArray, $searchData)
    {
        if (isset($expectedArray['relationships']['templateVersion'])) {
            $this->assertEquals(
                $expectedArray['relationships']['templateVersion']['data']['type'],
                'templateVersions'
            );
            $this->assertEquals(
                $expectedArray['relationships']['templateVersion']['data']['id'],
                $searchData->getTemplateVersion()->getId()
            );
        }
    }

    private function compareItemsData(array $expectedArray, $searchData)
    {
        if (isset($expectedArray['relationships']['itemsData'])) {
            $itemsDataArray = $expectedArray['relationships']['itemsData']['data'];

            $this->assertEquals($itemsDataArray['type'], 'itemsData');

            if (isset($itemsDataArray['id'])) {
                $this->assertEquals($itemsDataArray['id'], $searchData->getItemsData()->getId());
            }

            if (isset($itemsDataArray['attributes']['data'])) {
                $this->assertEquals($itemsDataArray['attributes']['data'], $searchData->getItemsData()->getData());
            }
        }
    }
}
