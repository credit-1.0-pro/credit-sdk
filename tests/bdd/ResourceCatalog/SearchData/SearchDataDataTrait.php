<?php


namespace Sdk\ResourceCatalog\SearchData;

use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\Adapter\SearchData\SearchDataRestfulAdapter;

use Sdk\ResourceCatalog\Template\TemplateDataTrait;

trait SearchDataDataTrait
{
    use TemplateDataTrait;

    protected function getSearchDataList(array $filter = []) : array
    {
        $adapter = new SearchDataRestfulAdapter();

        list($count, $searchDataList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $searchDataList;
    }

    protected function getSearchDataAttributesData() : array
    {
        return array(
            "name" => "陕西传媒有限公司",
            "identify" => "921345679023456789",
            "expirationDate" => 1618285915,
            "subjectCategory" => 1,
            "dimension" => 1,
            "exchangeFrequency" => 1,
            "infoClassify" => 1,
            "infoCategory" => 1,
            "applyStatus" => 0,
            "rejectReason" => "",
            "extractStatus" => 0,
            "status" => 0,
            "createTime" => 1664263571,
            "updateTime" => 1664263571,
            "statusTime" => 0
        );
    }

    protected function getSearchDataRelationshipsData() : array
    {
        $relationships['publishUserGroup'] = array("data"=>array("type"=>"userGroups","id"=>1));
        $relationships['templateVersion'] = array("data"=>array("type"=>"templateVersions","id"=>1));
        $relationships['itemsData'] = array("data"=>array("type"=>"itemsData","id"=>1));
 
        return $relationships;
    }

    protected function getSearchDataIncludedData()
    {
        return [
            $this->getUserGroupFgwIncluded(),
            $this->getTemplateVersionIncluded(),
            $this->getItemsDataIncluded()
        ];
    }

    protected function getItemsDataIncluded() : array
    {
        return
        [
            "type" => "itemsData",
            "id" => 1,
            "attributes" => [
                "data" => [
                    "ZTMC" => "山西传媒有限公司",
                    "TYSHXYDM" => "921345679023456788",
                    "FDDBR" => "白玉",
                    "ZTLB" => "法人及非法人组织",
                    "GKFW" => "社会公开",
                    "GXPL" => "实时",
                    "XXFL" => "行政处罚",
                    "XXLB" => "基础信息"
                ]
            ]
        ];
    }

    protected function getSearchDataListData(): array
    {
        $attributes = $this->getSearchDataAttributesData();
        $relationships = $this->getSearchDataRelationshipsData();

        return [
            "meta" => ["count" => 1],
            "data" => [
                [
                    "type" => "searchData",
                    "id" => "15",
                    "attributes" =>$attributes,
                    "relationships" =>$relationships,
                    "links" => [
                        "self" => "127.0.0.1:8080/searchData/15"
                    ]
                ],
            ],
            "included" => $this->getSearchDataIncludedData()
        ];
    }

    protected function getSearchDataDetailData() : array
    {
        $attributes = $this->getSearchDataAttributesData();
        $relationships = $this->getSearchDataRelationshipsData();
        
        return [
            "meta" => [],
            "data" => [
                "type" => "searchData",
                "id" => 1,
                "attributes" => $attributes,
                "relationships" => $relationships,
                "links" => [
                    "self" => "127.0.0.1:8080/searchData/1"
                ]
            ],
            "included" => $this->getSearchDataIncludedData()
        ];
    }

    protected function getSearchDataRequestRelationshipsData() : array
    {
        return array(
            "crew" => array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            ),
            "template" => array(
                "data"=>array(
                    array("type"=>"templates","id"=>1)
                )
            ),
            "itemsData" => array(
                "data" => array(
                    array(
                        "type" => "itemsData",
                        "attributes" => array(
                            "data" => array(
                                "ZTMC" => "陕西传媒有限公司",
                                "TYSHXYDM" => "921345679023456789",
                                "FDDBR" => "白玉",
                                "ZTLB" => "法人及非法人组织",
                                "GKFW" => "社会公开",
                                "GXPL" => "实时",
                                "XXFL" => "行政处罚",
                                "XXLB" => "基础信息"
                            )
                        )
                    )
                )
            )
        );
    }

    protected function getSearchDataOnlineAddRequestData() : array
    {
        $relationships = $this->getSearchDataRequestRelationshipsData();

        return array(
            'data' => array(
                "type"=>"searchData",
                "attributes" => array(
                    "expirationDate" => 1618285915,    //有效期限
                ),
                "relationships"=>$relationships,
            )
        );
    }

    protected function getSearchDataRejectRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"searchData",
                "attributes"=>array("rejectReason"=>"审核驳回原因")
            )
        );
    }
}
