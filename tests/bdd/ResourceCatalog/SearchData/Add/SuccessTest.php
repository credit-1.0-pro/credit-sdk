<?php


namespace Sdk\ResourceCatalog\SearchData\SearchData\Add;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\ResourceCatalog\SearchData\Model\SearchData;
use Sdk\ResourceCatalog\SearchData\SearchDataDataTrait;
use Sdk\ResourceCatalog\SearchData\SearchDataRestfulUtils;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,当我需要新增一条资源目录数据时,
 * 在资源目录管理中的资源目录数据下,点击在线填报, 进入在线填报界面，并根据我所采集的资源目录数据进行新增, 以便于我维护数据列表
 * @Scenario: 在线填报成功
 */
class SuccessTest extends TestCase
{
    use SearchDataDataTrait, SearchDataRestfulUtils, ErrorDataTrait;

    private $addSearchData;

    private $mock;

    public function setUp()
    {
        $this->addSearchData = new SearchData();
    }

    public function tearDown()
    {
        unset($this->addSearchData);
        unset($this->mock);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getSearchDataDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $data = $this->getSearchDataOnlineAddRequestData()['data'];

        $this->addSearchData = new SearchData();

        $this->addSearchData->setExpirationDate($data['attributes']['expirationDate']);
        $this->addSearchData->getTemplate()->setId($data['relationships']['template']['data'][0]['id']);
        $this->addSearchData->getCrew()->setId($data['relationships']['crew']['data'][0]['id']);
        $this->addSearchData->getItemsData()->setData(
            $data['relationships']['itemsData']['data'][0]['attributes']['data']
        );

        return $this->addSearchData->onlineAdd();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getSearchDataOnlineAddRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/resourceCatalogs/searchData', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getSearchDataDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->addSearchData);
    }
}
