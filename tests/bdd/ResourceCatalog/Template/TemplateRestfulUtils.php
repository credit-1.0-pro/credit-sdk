<?php
namespace Sdk\ResourceCatalog\Template;

trait TemplateRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $template
    ) {
        if (isset($expectedArray['id'])) {
            $this->assertEquals($expectedArray['id'], $template->getId());
        }
        
        $this->assertEquals($expectedArray['attributes']['name'], $template->getName());
        $this->assertEquals($expectedArray['attributes']['identify'], $template->getIdentify());
        $this->assertEquals($expectedArray['attributes']['description'], $template->getDescription());

        if (isset($expectedArray['attributes']['versionDescription'])) {
            $this->assertEquals(
                $expectedArray['attributes']['versionDescription'],
                $template->getVersionDescription()
            );
        }
        $this->assertEquals($expectedArray['attributes']['subjectCategory'], $template->getSubjectCategory());
        $this->assertEquals($expectedArray['attributes']['dimension'], $template->getDimension());
        $this->assertEquals(
            $expectedArray['attributes']['exchangeFrequency'],
            $template->getExchangeFrequency()
        );
        $this->assertEquals($expectedArray['attributes']['infoClassify'], $template->getInfoClassify());
        $this->assertEquals($expectedArray['attributes']['infoCategory'], $template->getInfoCategory());
        $this->assertEquals($expectedArray['attributes']['items'], $template->getItems());

        if (isset($expectedArray['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['attributes']['createTime'], $template->getCreateTime());
        }
        if (isset($expectedArray['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['attributes']['statusTime'], $template->getStatusTime());
        }
        if (isset($expectedArray['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['attributes']['updateTime'], $template->getUpdateTime());
        }
        if (isset($expectedArray['attributes']['status'])) {
            $this->assertEquals($expectedArray['attributes']['status'], $template->getStatus());
        }

        $this->compareCrew($expectedArray, $template);
        $this->comparePublishUserGroup($expectedArray, $template);
        $this->compareTemplateVersion($expectedArray, $template);
        $this->compareSourceUnits($expectedArray, $template);
    }

    private function compareCrew(array $expectedArray, $template)
    {
        if (isset($expectedArray['relationships']['crew'])) {
            $this->assertEquals($expectedArray['relationships']['crew']['data']['type'], 'crews');
            $this->assertEquals(
                $expectedArray['relationships']['crew']['data']['id'],
                $template->getCrew()->getId()
            );
        }
    }

    private function comparePublishUserGroup(array $expectedArray, $template)
    {
        if (isset($expectedArray['relationships']['publishUserGroup'])) {
            $this->assertEquals($expectedArray['relationships']['publishUserGroup']['data']['type'], 'userGroups');
            $this->assertEquals(
                $expectedArray['relationships']['publishUserGroup']['data']['id'],
                $template->getUserGroup()->getId()
            );
        }
    }

    private function compareTemplateVersion(array $expectedArray, $template)
    {
        if (isset($expectedArray['relationships']['templateVersion'])) {
            $this->assertEquals(
                $expectedArray['relationships']['templateVersion']['data']['type'],
                'templateVersions'
            );
            $this->assertEquals(
                $expectedArray['relationships']['templateVersion']['data']['id'],
                $template->getTemplateVersion()->getId()
            );
        }
    }

    private function compareSourceUnits(array $expectedArray, $template)
    {
        if (isset($expectedArray['relationships'])) {
            $sourceUnitIds = array();
            $sourceUnits = $template->getSourceUnits();

            foreach ($sourceUnits as $sourceUnit) {
                $sourceUnitIds[] = $sourceUnit->getId();
            }
            $relationships = $expectedArray['relationships'];
            if (isset($relationships['sourceUnits']['data'])) {
                foreach ($relationships['sourceUnits']['data'] as $key => $value) {
                    $this->assertEquals($value['type'], 'userGroups');
                    $this->assertEquals($value['id'], $sourceUnitIds[$key]);
                }
            }
        }
    }

    private function compareArrayAndObjectCommonUnAuditTemplate(
        array $expectedArray,
        $unAuditedTemplate
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditedTemplate);
        
        if (isset($expectedArray['attributes']['applyStatus'])) {
            $this->assertEquals(
                $expectedArray['attributes']['applyStatus'],
                $unAuditedTemplate->getApplyStatus()
            );
        }
        if (isset($expectedArray['attributes']['operationType'])) {
            $this->assertEquals(
                $expectedArray['attributes']['operationType'],
                $unAuditedTemplate->getOperationType()
            );
        }
        if (isset($expectedArray['attributes']['applyInfoType'])) {
            $this->assertEquals(
                $expectedArray['attributes']['applyInfoType'],
                $unAuditedTemplate->getApplyInfoType()
            );
        }
        if (isset($expectedArray['attributes']['relationId'])) {
            $this->assertEquals(
                $expectedArray['attributes']['relationId'],
                $unAuditedTemplate->getRelationId()
            );
        }

        $this->assertEquals(
            $expectedArray['attributes']['rejectReason'],
            $unAuditedTemplate->getRejectReason()
        );

        if (isset($expectedArray['relationships'])) {
            $relationships = $expectedArray['relationships'];
            if (isset($relationships['applyCrew']['data'])) {
                $this->assertEquals(
                    $relationships['applyCrew']['data']['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['applyCrew']['data']['id'],
                    $unAuditedTemplate->getApplyCrew()->getId()
                );
            }
        }
    }

    private function compareArrayAndObjectTemplateVersion(
        array $expectedArray,
        $templateVersion
    ) {
        if (isset($expectedArray['id'])) {
            $this->assertEquals($expectedArray['id'], $templateVersion->getId());
        }
        
        $this->assertEquals($expectedArray['attributes']['number'], $templateVersion->getNumber());
        $this->assertEquals($expectedArray['attributes']['description'], $templateVersion->getDescription());
        $this->assertEquals($expectedArray['attributes']['templateId'], $templateVersion->getTemplateId());
        $this->assertEquals($expectedArray['attributes']['info'], $templateVersion->getInfo());
       
        if (isset($expectedArray['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['attributes']['createTime'], $templateVersion->getCreateTime());
        }
        if (isset($expectedArray['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['attributes']['statusTime'], $templateVersion->getStatusTime());
        }
        if (isset($expectedArray['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['attributes']['updateTime'], $templateVersion->getUpdateTime());
        }
        if (isset($expectedArray['attributes']['status'])) {
            $this->assertEquals($expectedArray['attributes']['status'], $templateVersion->getStatus());
        }

        $this->compareCrew($expectedArray, $templateVersion);
        $this->comparePublishUserGroup($expectedArray, $templateVersion);
    }
}
