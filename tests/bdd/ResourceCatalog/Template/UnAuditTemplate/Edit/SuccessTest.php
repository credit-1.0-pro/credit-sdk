<?php


namespace Sdk\ResourceCatalog\Template\UnAuditTemplate\Edit;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Template\TemplateDataTrait;
use Sdk\ResourceCatalog\Template\TemplateRestfulUtils;
use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

/**
 * @Feature: 我是拥有目录发布权限的平台管理员/委办局管理员/操作用户,当我需要编辑目录信息时,在审核表中,编辑已驳回目录数据
 *           通过编辑目录界面，并根据我所采集的目录数据进行编辑,以便于我可以更好的维护目录管理列表
 * @Scenario: 正常编辑目录审核数据
 */
class SuccessTest extends TestCase
{
    use TemplateDataTrait, TemplateRestfulUtils;

    private $unAuditedTemplate;

    private $mock;

    public function setUp()
    {
        $this->unAuditedTemplate = new UnAuditTemplate();
    }

    public function tearDown()
    {
        unset($this->unAuditedTemplate);
        unset($this->mock);
    }

    /**
     * @Given: 我编辑数据
     */
    protected function prepareData()
    {
        $data = $this->getUnAuditTemplateDetailData();
        $editJsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $editJsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用编辑函数,期待返回true
     */
    protected function edit()
    {
        $data = $this->getUnAuditTemplateEditRequestData()['data'];

        $this->unAuditedTemplate = new UnAuditTemplate();
        $this->unAuditedTemplate->setId(1);

        $this->unAuditedTemplate->setName($data['attributes']['name']);
        $this->unAuditedTemplate->setIdentify($data['attributes']['identify']);
        $this->unAuditedTemplate->setSubjectCategory($data['attributes']['subjectCategory']);
        $this->unAuditedTemplate->setDimension($data['attributes']['dimension']);
        $this->unAuditedTemplate->setExchangeFrequency($data['attributes']['exchangeFrequency']);
        $this->unAuditedTemplate->setInfoClassify($data['attributes']['infoClassify']);
        $this->unAuditedTemplate->setInfoCategory($data['attributes']['infoCategory']);
        $this->unAuditedTemplate->setDescription($data['attributes']['description']);
        $this->unAuditedTemplate->setVersionDescription('目录版本描述信息');
        $this->unAuditedTemplate->setItems($data['attributes']['items']);
        $this->unAuditedTemplate->getCrew()->setId($data['relationships']['crew']['data'][0]['id']);

        foreach ($data['relationships']['sourceUnits']['data'] as $sourceUnit) {
            $this->unAuditedTemplate->addSourceUnit(new UserGroup($sourceUnit['id']));
        }

        return $this->unAuditedTemplate->edit();
    }

    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->edit();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getUnAuditTemplateEditRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/resourceCatalogs/unAuditedTemplates/1/resubmit', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getUnAuditTemplateDetailData('目录版本描述信息')['data'];

        $this->compareArrayAndObjectCommonUnAuditTemplate($data, $this->unAuditedTemplate);
    }
}
