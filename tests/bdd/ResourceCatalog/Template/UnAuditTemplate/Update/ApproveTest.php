<?php


namespace Sdk\ResourceCatalog\Template\UnAuditTemplate\Update;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Template\TemplateDataTrait;
use Sdk\ResourceCatalog\Template\TemplateRestfulUtils;
use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;
use Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate\UnAuditTemplateRestfulAdapter;

/**
 * @Feature: 我是拥有目录审核权限的平台管理员/委办局管理员/操作用户,当我需要审核一个目录时,在审核表中,审核待审核的目录数据
 *           通过目录详情页面的审核通过与审核驳回操作,以便于我维护目录列表
 * @Scenario: 审核通过
 */
class ApproveTest extends TestCase
{
    use TemplateDataTrait, TemplateRestfulUtils;

    private $unAuditTemplate;

    private $mock;

    public function setUp()
    {
        $this->unAuditTemplate = new UnAuditTemplate();
    }

    public function tearDown()
    {
        unset($this->unAuditTemplate);
        unset($this->mock);
    }

    /**
     * @Given: 存在需要待审核的数据
     */
    protected function prepareData()
    {
        $data = $this->getUnAuditTemplateDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要待审核的目录
     */
    protected function fetchUnAuditTemplate()
    {
        $adapter = new UnAuditTemplateRestfulAdapter();
        $this->unAuditTemplate = $adapter->fetchOne(1);

        return $this->unAuditTemplate;
    }

    /**
     * @When:当我调用待审核函数,期待返回true
     */
    protected function approve()
    {
        $this->unAuditTemplate->getApplyCrew()->setId(2);

        return $this->unAuditTemplate->approve();
    }

    /**
     * @Then  我可以查到待审核的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUnAuditTemplate();
        $this->approve();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(
            json_encode($this->getUnAuditTemplateApproveRequestData()),
            $request->getBody()->getContents()
        );
        $this->assertEquals('/resourceCatalogs/unAuditedTemplates/1/approve', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getUnAuditTemplateDetailData()['data'];

        $this->compareArrayAndObjectCommonUnAuditTemplate($data, $this->unAuditTemplate);
    }
}
