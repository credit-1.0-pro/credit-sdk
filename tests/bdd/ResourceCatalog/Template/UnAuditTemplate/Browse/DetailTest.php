<?php
namespace Sdk\ResourceCatalog\Template\UnAuditTemplate\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\ResourceCatalog\Template\TemplateDataTrait;
use Sdk\ResourceCatalog\Template\TemplateRestfulUtils;
use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;
use Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate\UnAuditTemplateRestfulAdapter;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户,我拥有目录审核权限、且当我需要查看未审核或已驳回的目录列表时,
 *           在审核表中,通过列表与详情的形式查看到未审核或已驳回的目录信息,以便于我维护目录模块
 * @Scenario: 查看目录审核数据详情
 */
class DetailTest extends TestCase
{
    use TemplateDataTrait, TemplateRestfulUtils;

    private $unAuditTemplate;

    private $mock;

    public function setUp()
    {
        $this->unAuditTemplate = new UnAuditTemplate();
    }

    public function tearDown()
    {
        unset($this->unAuditTemplate);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条目录数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getUnAuditTemplateDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条目录数据详情时
    */
    protected function fetchUnAuditTemplate($id)
    {
        $adapter = new UnAuditTemplateRestfulAdapter();

        $this->unAuditTemplate = $adapter->fetchOne($id);

        return $this->unAuditTemplate;
    }

    /**
     * @Then 我可以看见目录详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchUnAuditTemplate($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'resourceCatalogs/unAuditedTemplates/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getUnAuditTemplateDetailData()['data'];

        $this->compareArrayAndObjectCommonUnAuditTemplate($data, $this->unAuditTemplate);
    }
}
