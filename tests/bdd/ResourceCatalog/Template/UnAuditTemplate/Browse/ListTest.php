<?php


namespace Sdk\ResourceCatalog\Template\UnAuditTemplate\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Template\TemplateDataTrait;
use Sdk\ResourceCatalog\Template\TemplateRestfulUtils;
use Sdk\ResourceCatalog\Template\Model\UnAuditTemplate;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有目录审核权限、且当我需要查看未审核或已驳回的目录列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的目录信息,以便于我维护目录模块
 * @Scenario: 查看目录列表
 */
class ListTest extends TestCase
{
    use TemplateDataTrait, TemplateRestfulUtils;

    private $unAuditTemplate;

    private $mock;

    public function setUp()
    {
        $this->unAuditTemplate = new UnAuditTemplate();
    }

    public function tearDown()
    {
        unset($this->unAuditTemplate);
        unset($this->mock);
    }

    /**
     * @Given: 存在目录数据
     */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getUnAuditTemplateListData());

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看目录列表时
     */
    public function fetchUnAuditTemplateList() : array
    {
        $filter = [];
        return $this->getUnAuditTemplateList($filter);
    }

    /**
     * @Then  我可以看见目录数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUnAuditTemplateList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('resourceCatalogs/unAuditedTemplates', $request->getUri()->getPath());
    }

    private function response()
    {
        $unAuditTemplateList = $this->fetchUnAuditTemplateList();
        $unAuditTemplateListArray = $this->getUnAuditTemplateListData()['data'];

        foreach ($unAuditTemplateList as $unAuditTemplate) {
            foreach ($unAuditTemplateListArray as $unAuditTemplateArray) {
                if ($unAuditTemplateArray['id'] == $unAuditTemplate->getId()) {
                    $this->compareArrayAndObjectCommonUnAuditTemplate($unAuditTemplateArray, $unAuditTemplate);
                }
            }
        }
    }
}
