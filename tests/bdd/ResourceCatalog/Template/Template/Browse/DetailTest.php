<?php
namespace Sdk\ResourceCatalog\Template\Template\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\TemplateDataTrait;
use Sdk\ResourceCatalog\Template\TemplateRestfulUtils;
use Sdk\ResourceCatalog\Template\Adapter\Template\TemplateRestfulAdapter;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有目录发布权限、且当我需要查看通过审核的目录的列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的目录信息,以便于我维护目录模块
 * @Scenario: 查看目录数据详情
 */
class DetailTest extends TestCase
{
    use TemplateDataTrait, TemplateRestfulUtils;

    private $template;

    private $mock;

    public function setUp()
    {
        $this->template = new Template();
    }

    public function tearDown()
    {
        unset($this->template);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条目录数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getTemplateDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条目录数据详情时
    */
    protected function fetchTemplate($id)
    {
        $adapter = new TemplateRestfulAdapter();

        $this->template = $adapter->fetchOne($id);

        return $this->template;
    }

    /**
     * @Then 我可以看见目录详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchTemplate($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'resourceCatalogs/templates/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getTemplateDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->template);
    }
}
