<?php


namespace Sdk\ResourceCatalog\Template\Template\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\TemplateDataTrait;
use Sdk\ResourceCatalog\Template\TemplateRestfulUtils;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有目录发布权限、且当我需要查看通过审核的目录的列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的目录信息,以便于我维护目录模块
 * @Scenario: 查看目录列表
 */
class ListTest extends TestCase
{
    use TemplateDataTrait, TemplateRestfulUtils;

    private $template;

    private $mock;

    public function setUp()
    {
        $this->template = new Template();
    }

    public function tearDown()
    {
        unset($this->template);
        unset($this->mock);
    }

    /**
     * @Given: 存在目录数据
     */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getTemplateListData());

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看目录列表时
     */
    public function fetchTemplateList() : array
    {
        $filter = [];
        return $this->getTemplateList($filter);
    }

    /**
     * @Then  我可以看见目录数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchTemplateList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('resourceCatalogs/templates', $request->getUri()->getPath());
    }

    private function response()
    {
        $templateList = $this->fetchTemplateList();
        $templateListArray = $this->getTemplateListData()['data'];

        foreach ($templateList as $template) {
            foreach ($templateListArray as $templateArray) {
                if ($templateArray['id'] == $template->getId()) {
                    $this->compareArrayAndObjectCommon($templateArray, $template);
                }
            }
        }
    }
}
