<?php


namespace Sdk\ResourceCatalog\Template\Template\Update;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\TemplateDataTrait;
use Sdk\ResourceCatalog\Template\TemplateRestfulUtils;
use Sdk\ResourceCatalog\Template\Adapter\Template\TemplateRestfulAdapter;

/**
 * @Feature: 我是拥有目录发布权限的平台管理员/委办局管理员/操作用户,当我需要回退到上个版本时,在目录管理目录详情页查看版本列表,
 *           我可以进行版本比对,查看到两个版本的差异点,以便于我可以更好的管理目录
 * @Scenario: 版本回退
 */
class VersionRestoreTest extends TestCase
{
    use TemplateDataTrait, TemplateRestfulUtils;

    private $template;

    private $mock;

    public function setUp()
    {
        $this->template = new Template();
    }

    public function tearDown()
    {
        unset($this->template);
        unset($this->mock);
    }

    /**
     * @Given: 存在需要版本回退的数据
     */
    protected function prepareData()
    {
        $data = $this->getTemplateDetailData();
        $versionRestoreJsonData = json_encode($data);

        $data = $this->getTemplateDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $versionRestoreJsonData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要版本回退的目录
     */
    protected function fetchTemplate()
    {
        $adapter = new TemplateRestfulAdapter();
        $this->template = $adapter->fetchOne(1);

        return $this->template;
    }

    /**
     * @When:当我调用版本回退函数,期待返回true
     */
    protected function versionRestore()
    {
        $this->template->getTemplateVersion()->setId(1);
        $this->template->getCrew()->setId(1);

        return $this->template->versionRestore();
    }

    /**
     * @Then  我可以查到版本回退的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchTemplate();
        $this->versionRestore();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($this->getTemplateStatusRequestData()), $request->getBody()->getContents());
        $this->assertEquals('/resourceCatalogs/templates/1/versionRestore/1', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getTemplateDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->template);
    }
}
