<?php


namespace Sdk\ResourceCatalog\Template\Template\Add;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\TemplateDataTrait;
use Sdk\ResourceCatalog\Template\TemplateRestfulUtils;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

/**
 * @Feature: 我是拥有目录发布权限的平台管理员/委办局管理员/操作用户,当我需要新增一个目录时,在发布表中,新增对应的目录数据
 *           通过新增目录界面，并根据我所采集的目录数据进行新增,以便于我维护目录列表
 * @Scenario: 正常新增数据数据
 */
class SuccessTest extends TestCase
{
    use TemplateDataTrait, TemplateRestfulUtils, ErrorDataTrait;

    private $addTemplate;

    private $mock;

    public function setUp()
    {
        $this->addTemplate = new Template();
    }

    public function tearDown()
    {
        unset($this->addTemplate);
        unset($this->mock);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getTemplateDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $data = $this->getTemplateDetailData()['data'];

        $this->addTemplate = new Template();

        $this->addTemplate->setName($data['attributes']['name']);
        $this->addTemplate->setIdentify($data['attributes']['identify']);
        $this->addTemplate->setSubjectCategory($data['attributes']['subjectCategory']);
        $this->addTemplate->setDimension($data['attributes']['dimension']);
        $this->addTemplate->setExchangeFrequency($data['attributes']['exchangeFrequency']);
        $this->addTemplate->setInfoClassify($data['attributes']['infoClassify']);
        $this->addTemplate->setInfoCategory($data['attributes']['infoCategory']);
        $this->addTemplate->setDescription($data['attributes']['description']);
        $this->addTemplate->setVersionDescription('目录版本描述信息');
        $this->addTemplate->setItems($data['attributes']['items']);
        $this->addTemplate->getCrew()->setId($data['relationships']['crew']['data']['id']);

        foreach ($data['relationships']['sourceUnits']['data'] as $sourceUnit) {
            $this->addTemplate->addSourceUnit(new UserGroup($sourceUnit['id']));
        }

        return $this->addTemplate->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getTemplateAddRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/resourceCatalogs/templates', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getTemplateDetailData('目录版本描述信息')['data'];

        $this->compareArrayAndObjectCommon($data, $this->addTemplate);
    }
}
