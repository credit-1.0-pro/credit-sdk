<?php


namespace Sdk\ResourceCatalog\Template\Template\Edit;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\TemplateDataTrait;
use Sdk\ResourceCatalog\Template\TemplateRestfulUtils;

use Sdk\UserGroup\UserGroup\Model\UserGroup;

/**
 * @Feature: 我是拥有目录发布权限的平台管理员/委办局管理员/操作用户,当我需要编辑目录信息时,在发布表中,编辑对应目录数据
 *           通过编辑目录界面，并根据我所采集的目录数据进行编辑,以便于我可以更好的维护目录管理列表
 * @Scenario: 正常编辑目录数据
 */
class SuccessTest extends TestCase
{
    use TemplateDataTrait, TemplateRestfulUtils;

    private $template;

    private $mock;

    public function setUp()
    {
        $this->template = new Template();
    }

    public function tearDown()
    {
        unset($this->template);
        unset($this->mock);
    }

    /**
     * @Given: 我编辑数据
     */
    protected function prepareData()
    {
        $data = $this->getTemplateDetailData();
        $editJsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $editJsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用编辑函数,期待返回true
     */
    protected function edit()
    {
        $data = $this->getTemplateEditRequestData()['data'];

        $this->template = new Template();
        $this->template->setId(1);
        $this->template->setName($data['attributes']['name']);
        $this->template->setIdentify($data['attributes']['identify']);
        $this->template->setSubjectCategory($data['attributes']['subjectCategory']);
        $this->template->setDimension($data['attributes']['dimension']);
        $this->template->setExchangeFrequency($data['attributes']['exchangeFrequency']);
        $this->template->setInfoClassify($data['attributes']['infoClassify']);
        $this->template->setInfoCategory($data['attributes']['infoCategory']);
        $this->template->setDescription($data['attributes']['description']);
        $this->template->setVersionDescription('目录版本描述信息');
        $this->template->setItems($data['attributes']['items']);
        $this->template->getCrew()->setId($data['relationships']['crew']['data'][0]['id']);

        foreach ($data['relationships']['sourceUnits']['data'] as $sourceUnit) {
            $this->template->addSourceUnit(new UserGroup($sourceUnit['id']));
        }

        return $this->template->edit();
    }

    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->edit();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getTemplateEditRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/resourceCatalogs/templates/1', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getTemplateDetailData('目录版本描述信息')['data'];
        $data['id'] = 1;

        $this->compareArrayAndObjectCommon($data, $this->template);
    }
}
