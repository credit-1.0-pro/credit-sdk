<?php
namespace Sdk\ResourceCatalog\Template\TemplateVersion\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\ResourceCatalog\Template\TemplateDataTrait;
use Sdk\ResourceCatalog\Template\TemplateRestfulUtils;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;
use Sdk\ResourceCatalog\Template\Adapter\TemplateVersion\TemplateVersionRestfulAdapter;

/**
 * @Feature: 我是拥有目录发布权限的平台管理员/委办局管理员/操作用户,当我需要查看我的目录版本记录时,在目录管理目录详情页
 *           可以查看到该目录下的所有版本记录,通过列表和详情的形式查看我所有的版本记录,以便于我可以更好的溯源历史记录
 * @Scenario: 查看目录版本记录数据详情
 */
class DetailTest extends TestCase
{
    use TemplateDataTrait, TemplateRestfulUtils;

    private $templateVersion;

    private $mock;

    public function setUp()
    {
        $this->templateVersion = new TemplateVersion();
    }

    public function tearDown()
    {
        unset($this->templateVersion);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条目录版本记录数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getTemplateVersionDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条目录版本记录数据详情时
    */
    protected function fetchTemplateVersion($id)
    {
        $adapter = new TemplateVersionRestfulAdapter();

        $this->templateVersion = $adapter->fetchOne($id);

        return $this->templateVersion;
    }

    /**
     * @Then 我可以看见目录版本记录详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchTemplateVersion($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'resourceCatalogs/templateVersions/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getTemplateVersionDetailData()['data'];

        $this->compareArrayAndObjectTemplateVersion($data, $this->templateVersion);
    }
}
