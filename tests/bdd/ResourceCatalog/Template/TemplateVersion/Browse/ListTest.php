<?php


namespace Sdk\ResourceCatalog\Template\TemplateVersion\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ResourceCatalog\Template\TemplateDataTrait;
use Sdk\ResourceCatalog\Template\TemplateRestfulUtils;
use Sdk\ResourceCatalog\Template\Model\TemplateVersion;

/**
 * @Feature: 我是拥有目录发布权限的平台管理员/委办局管理员/操作用户,当我需要查看我的目录版本记录时,在目录管理目录详情页
 *           可以查看到该目录下的所有版本记录,通过列表和详情的形式查看我所有的版本记录,以便于我可以更好的溯源历史记录
 * @Scenario: 查看目录版本记录数据
 */
class ListTest extends TestCase
{
    use TemplateDataTrait, TemplateRestfulUtils;

    private $templateVersion;

    private $mock;

    public function setUp()
    {
        $this->templateVersion = new TemplateVersion();
    }

    public function tearDown()
    {
        unset($this->templateVersion);
        unset($this->mock);
    }

    /**
     * @Given: 存在目录版本记录数据
     */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getTemplateVersionListData());

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看目录版本记录列表时
     */
    public function fetchTemplateVersionList() : array
    {
        $filter = [];
        return $this->getTemplateVersionList($filter);
    }

    /**
     * @Then  我可以看见目录版本记录数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchTemplateVersionList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('resourceCatalogs/templateVersions', $request->getUri()->getPath());
    }

    private function response()
    {
        $templateVersionList = $this->fetchTemplateVersionList();
        $templateVersionListArray = $this->getTemplateVersionListData()['data'];
        foreach ($templateVersionList as $templateVersion) {
            foreach ($templateVersionListArray as $templateVersionArray) {
                if ($templateVersionArray['id'] == $templateVersion->getId()) {
                    $this->compareArrayAndObjectTemplateVersion($templateVersionArray, $templateVersion);
                }
            }
        }
    }
}
