<?php


namespace Sdk\ResourceCatalog\Template;

use Sdk\ResourceCatalog\Template\Model\Template;
use Sdk\ResourceCatalog\Template\Adapter\Template\TemplateRestfulAdapter;
use Sdk\ResourceCatalog\Template\Adapter\TemplateVersion\TemplateVersionRestfulAdapter;
use Sdk\ResourceCatalog\Template\Adapter\UnAuditTemplate\UnAuditTemplateRestfulAdapter;

trait TemplateDataTrait
{
    protected function getTemplateList(array $filter = []) : array
    {
        $adapter = new TemplateRestfulAdapter();

        list($count, $templateList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $templateList;
    }

    protected function getUnAuditTemplateList(array $filter = []) : array
    {
        $adapter = new UnAuditTemplateRestfulAdapter();

        list($count, $unAuditTemplateList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $unAuditTemplateList;
    }

    protected function getTemplateVersionList(array $filter = []) : array
    {
        $adapter = new TemplateVersionRestfulAdapter();

        list($count, $templateVersionList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $templateVersionList;
    }

    protected function getTemplateCommonAttributesData($versionDescription = '') : array
    {
        return array(
            "name" => '登记信息',    //目录名称
            "identify" => 'DJXX',    //目录标识
            "description" => "目录描述信息",    //目录描述
            "versionDescription" => $versionDescription,    //目录描述
            "subjectCategory" => array(1, 3),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "exchangeFrequency" => 1,    //更新频率
            "infoClassify" => 1,    //信息分类
            "infoCategory" => 1,    //信息类别
            "items" => array(
                array(
                    "name" => '主体名称',    //信息项名称
                    "identify" => 'ZTMC',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '200',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(),    //脱敏规则
                    "remarks" => '信用主体名称',    //备注
                ),
                array(
                    "name" => '统一社会信用代码',    //信息项名称
                    "identify" => 'TYSHXYDM',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '50',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                    "remarks" => '信用主体代码',    //备注
                ),
                array(
                    "name" => '信息类别',    //信息项名称
                    "identify" => 'XXLB',    //数据标识
                    "type" => 5,    //数据类型
                    "length" => '50',    //数据长度
                    "options" => array(
                        "基础信息",
                        "守信信息",
                        "失信信息",
                        "其他信息"
                    ),    //可选范围
                    "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(1, 2),    //脱敏规则，即左边保留1位字符，右边保留2位字符
                    "remarks" => '信息性质类型，支持单选',    //备注
                ),
            ),
        );
    }

    protected function getTemplateAttributesData($versionDescription = '') : array
    {
        $attributes = $this->getTemplateCommonAttributesData($versionDescription);
        $attributes['status'] = 0;
        $attributes['createTime'] = 1654673891;
        $attributes['updateTime'] = 1654673891;
        $attributes['statusTime'] = 1654673891;

        return $attributes;
    }

    protected function getUnAuditTemplateAttributesData($versionDescription = '') : array
    {
        $attributes = $this->getTemplateAttributesData($versionDescription);
        $attributes['applyInfoType'] = 1;
        $attributes['applyStatus'] = 0;
        $attributes['rejectReason'] = "";
        $attributes['operationType'] = 1;
        $attributes['templateId'] = 1;
        
        return $attributes;
    }

    protected function getTemplateRequestRelationshipsData() : array
    {
        return array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            ),
            "sourceUnits"=>array(
                "data"=>array(
                    array("type"=>"userGroups","id"=>1),
                    array("type"=>"userGroups","id"=>2),
                    array("type"=>"userGroups","id"=>3)
                )
            )
        );
    }

    protected function getTemplateRelationshipsData() : array
    {
        $relationships['crew'] = array("data"=>array("type"=>"crews","id"=>1));
        $relationships['publishUserGroup'] = array("data"=>array("type"=>"userGroups","id"=>1));
        $relationships['templateVersion'] = array("data"=>array("type"=>"templateVersions","id"=>1));
        $relationships['sourceUnits'] = array(
            "data"=>array(
                array("type"=>"userGroups","id"=>1),
                array("type"=>"userGroups","id"=>2),
                array("type"=>"userGroups","id"=>3)
            )
        );

        return $relationships;
    }

    protected function getUnAuditTemplateRelationshipsData() : array
    {
        $relationships = $this->getTemplateRelationshipsData();
        $relationships['publishCrew'] = array("data"=>array("type"=>"crews","id"=>1));
        $relationships['applyCrew'] = array("data"=>array("type"=>"crews","id"=>1));
        $relationships['applyUserGroup'] = array("data"=>array("type"=>"userGroups","id"=>1));

        return $relationships;
    }

    protected function getTemplateCommonIncludedData()
    {
        return [
            $this->getUserGroupFgwIncluded(),
            $this->getUserGroupGxjIncluded(),
            $this->getUserGroupGajIncluded(),
            $this->getTemplateVersionIncluded(),
            $this->getCrewIncluded()
        ];
    }

    protected function getUserGroupFgwIncluded() : array
    {
        return
        [
            "type" => "userGroups",
            "id" => "1",
            "attributes" => [
                "name" => "乌兰察布发展和改革委员会",
                "shortName" => "市发改委",
                "unifiedSocialCreditCode" => "123456789012345678",
                "administrativeArea" => 0,
                "status" => 0,
                "createTime" => 1652949496,
                "updateTime" => 1653114283,
                "statusTime" => 0
            ]
        ];
    }

    protected function getUserGroupGxjIncluded() : array
    {
        return
        [
            "type" => "userGroups",
            "id" => "2",
            "attributes" => [
                "name" => "工业和信息化局",
                "shortName" => "工信局",
                "unifiedSocialCreditCode" => "",
                "administrativeArea" => 0,
                "status" => 0,
                "createTime" => 1652949525,
                "updateTime" => 1652949525,
                "statusTime" => 0
            ]
        ];
    }

    protected function getUserGroupGajIncluded() : array
    {
        return
        [
            "type" => "userGroups",
            "id" => "3",
            "attributes" => [
                "name" => "公安局",
                "shortName" => "公安局",
                "unifiedSocialCreditCode" => "",
                "administrativeArea" => 0,
                "status" => 0,
                "createTime" => 1652949539,
                "updateTime" => 1652949539,
                "statusTime" => 0
            ]
        ];
    }

    protected function getCrewIncluded() : array
    {
        return
        [
            "type" => "crews",
            "id" => "1",
            "attributes" => [
                "realName" => "白玉",
                "cardId" => "412825199008093456",
                "userName" => "18800000001",
                "cellphone" => "18800000001",
                "category" => 4,
                "purview" => [
                    "1",
                    "2",
                    "3",
                    "5"
                ],
                "status" => 0,
                "createTime" => 1655198072,
                "updateTime" => 1655198195,
                "statusTime" => 0
            ],
            "relationships" => [
                "userGroup" => [
                    "data" => [
                        "type" => "userGroups",
                        "id" => "1"
                    ]
                ],
                "department" => [
                    "data" => [
                        "type" => "departments",
                        "id" => "1"
                    ]
                ]
            ]
        ];
    }

    protected function templateVersionInfo() : array
    {
        return
        [
            "name" => "法人登记信息",
            "items" => [
                [
                    "name" => "主体名称",
                    "type" => "1",
                    "length" => "200",
                    "options" => [],
                    "remarks" => "在国家机关办理登记的主 体名称，以下同",
                    "identify" => "ZTMC",
                    "isMasked" => "0",
                    "maskRule" => [],
                    "dimension" => "1",
                    "isNecessary" => "1"
                ],
                [
                    "name" => "统一社会信用代码",
                    "type" => "1",
                    "length" => "18",
                    "options" => [],
                    "remarks" => "采用 GB 32100-2015",
                    "identify" => "TYSHXYDM",
                    "isMasked" => "0",
                    "maskRule" => [],
                    "dimension" => "1",
                    "isNecessary" => "1"
                ]
            ],
            "status" => "0",
            "crew_id" => "1",
            "identify" => "FR_DJXX",
            "dimension" => "1",
            "create_time" => "1656492285",
            "description" => "法人登记信息",
            "status_time" => "0",
            "template_id" => "0",
            "update_time" => "1656492285",
            "source_units" => [
                "1" => [
                    "user_group_id" => 1,
                    "name" => "乌兰察布发展和改革委员会",
                    "short_name" => "市发改委",
                    "unified_social_credit_code" => "123456789012345678",
                    "administrative_area" => 0,
                    "status" => 0,
                    "create_time" => 1652949496,
                    "update_time" => 1653114283,
                    "status_time" => 0
                ],
                "2" => [
                    "user_group_id" => 2,
                    "name" => "工业和信息化局",
                    "short_name" => "工信局",
                    "unified_social_credit_code" => "",
                    "administrative_area" => 0,
                    "status" => 0,
                    "create_time" => 1652949525,
                    "update_time" => 1652949525,
                    "status_time" => 0
                ]
            ],
            "info_category" => "1",
            "info_classify" => "1",
            "subject_category" => ["1", "3"],
            "exchange_frequency" => "1",
            "template_version_id" => "0",
            "publish_usergroup_id" => "1"
        ];
    }
    
    protected function getTemplateVersionIncluded() : array
    {
        return
        [
            "type" => "templateVersions",
            "id" => "1",
            "attributes" => [
                "number" => "2206292118",
                "description" => "法人登记信息目录归集",
                "templateId" => 1,
                "info" => $this->templateVersionInfo(),
                "status" => 0,
                "createTime" => 1656492285,
                "updateTime" => 1656492472,
                "statusTime" => 1656492472
            ],
            "relationships" => [
                "publishUserGroup" => [
                    "data" => [
                        "type" => "userGroups",
                        "id" => "1"
                    ]
                ],
                "crew" => [
                    "data" => [
                        "type" => "crews",
                        "id" => "1"
                    ]
                ]
            ]
        ];
    }

    protected function getTemplateListData($versionDescription = ''): array
    {
        $attributes = $this->getTemplateAttributesData($versionDescription);
        $relationships = $this->getTemplateRelationshipsData();

        return [
            "meta" => [
                "count" => 4,
                "links" => [
                    "first" => null,
                    "last" => null,
                    "prev" => null,
                    "next" => null
                ]
            ],
            "links" => [
                "first" => null,
                "last" => null,
                "prev" => null,
                "next" => null
            ],
            "data" => [
                [
                    "type" => "templates",
                    "id" => "15",
                    "attributes" =>$attributes,
                    "relationships" =>$relationships,
                    "links" => [
                        "self" => "127.0.0.1:8080/templates/15"
                    ]
                ],
            ],
            "included" => $this->getTemplateCommonIncludedData()
        ];
    }

    protected function getTemplateDetailData($versionDescription = '') : array
    {
        $attributes = $this->getTemplateAttributesData($versionDescription);
        $relationships = $this->getTemplateRelationshipsData();
        
        return [
            "meta" => [],
            "data" => [
                "type" => "templates",
                "id" => 1,
                "attributes" => $attributes,
                "relationships" => $relationships,
                "links" => [
                    "self" => "127.0.0.1:8080/templates/1"
                ]
            ],
            "included" => $this->getTemplateCommonIncludedData()
        ];
    }

    protected function getUnAuditTemplateListData(): array
    {
        $attributes = $this->getUnAuditTemplateAttributesData();
        $relationships = $this->getUnAuditTemplateRelationshipsData();

        return [
            "meta" => [
                "count" => 4,
                "links" => [
                    "first" => null,
                    "last" => null,
                    "prev" => null,
                    "next" => null
                ]
            ],
            "links" => [
                "first" => null,
                "last" => null,
                "prev" => null,
                "next" => null
            ],
            "data" => [
                [
                    "type" => "unAuditedTemplates",
                    "id" => "15",
                    "attributes" =>$attributes,
                    "relationships" =>$relationships,
                    "links" => [
                        "self" => "127.0.0.1:8080/unAuditedTemplates/15"
                    ]
                ],
            ],
            "included" => $this->getTemplateCommonIncludedData()
        ];
    }

    protected function getUnAuditTemplateDetailData($versionDescription = '') : array
    {
        $attributes = $this->getUnAuditTemplateAttributesData($versionDescription);
        $relationships = $this->getUnAuditTemplateRelationshipsData();
        
        return [
            "meta" => [],
            "data" => [
                "type" => "unAuditedTemplates",
                "id" => 1,
                "attributes" => $attributes,
                "relationships" => $relationships,
                "links" => [
                    "self" => "127.0.0.1:8080/unAuditedTemplates/1"
                ]
            ],
            "included" => $this->getTemplateCommonIncludedData()
        ];
    }

    protected function getTemplateAddRequestData() : array
    {
        $attributes = $this->getTemplateCommonAttributesData('目录版本描述信息');
        $relationships = $this->getTemplateRequestRelationshipsData();

        return array(
            'data' => array(
                "type"=>"templates",
                "attributes"=>$attributes,
                "relationships"=>$relationships,
            )
        );
    }

    protected function getTemplateEditRequestData() : array
    {
        $attributes = $this->getTemplateCommonAttributesData('目录版本描述信息');
        $relationships = $this->getTemplateRequestRelationshipsData();

        return array(
            'data' => array(
                "type"=>"templates",
                "attributes"=>$attributes,
                "relationships"=>$relationships,
            )
        );
    }

    protected function getUnAuditTemplateEditRequestData() : array
    {
        $attributes = $this->getTemplateCommonAttributesData('目录版本描述信息');
        $relationships = $this->getTemplateRequestRelationshipsData();

        return array(
            'data' => array(
                "type"=>"unAuditedTemplates",
                "attributes"=>$attributes,
                "relationships"=>$relationships,
            )
        );
    }

    protected function getUnAuditTemplateApproveRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"unAuditedTemplates",
                "attributes"=>array(),
                "relationships"=>array(
                    "applyCrew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );
    }

    protected function getUnAuditTemplateRejectRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"unAuditedTemplates",
                "attributes"=>array("rejectReason"=>"审核驳回原因"),
                "relationships"=>array(
                    "applyCrew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );
    }

    protected function getTemplateStatusRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"templates",
                "attributes"=>array(),
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );
    }
    
    protected function getTemplateVersionDetailData(): array
    {
        return
        [
            "meta" => [],
            "data" => $this->getTemplateVersionIncluded(),
            "included" => [$this->getUserGroupFgwIncluded(), $this->getCrewIncluded()]
        ];
    }

    protected function getTemplateVersionListData() : array
    {
        return [
            "meta" => [
                "count" => 3,
                "links" => [
                    "first" => 1,
                    "last" => 3,
                    "prev" => null,
                    "next" => 2
                ]
            ],
            "links" => [],
            "data" => [$this->getTemplateVersionIncluded()],
            "included" => [$this->getUserGroupFgwIncluded(), $this->getCrewIncluded()]
        ];
    }
}
