<?php


namespace Sdk\News;

use Sdk\News\News\Model\News;
use Sdk\News\News\Adapter\News\NewsRestfulAdapter;
use Sdk\News\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter;

trait NewsDataTrait
{
    protected function getNewsList(array $filter = []) : array
    {
        $adapter = new NewsRestfulAdapter();

        list($count, $newsList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $newsList;
    }

    protected function getUnAuditNewsList(array $filter = []) : array
    {
        $adapter = new UnAuditNewsRestfulAdapter();

        list($count, $unAuditNewsList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $unAuditNewsList;
    }

    protected function getNewsCommonAttributesData(
        $status = News::STATUS['ENABLED'],
        $stick = News::STICK['DISABLED']
    ) : array {
        return array(
            "title"=>"新闻标题测试",
            "source"=>"新闻来源",
            "attachments"=>array(
                array('name' => '新闻附件', 'identify' => 'identify.pdf')
            ),
            "status"=>$status,
            "dimension"=>News::DIMENSION['SOCIOLOGY'],
            "releaseTime"=>1654674300,
            "bannerImage"=>array('name' => '轮播图图片名称', 'identify' => '轮播图图片地址.jpg'),
            "content"=>"新闻内容",
            "stick"=>$stick,
            "cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
            "bannerStatus"=>News::BANNER_STATUS['ENABLED'],
            "homePageShowStatus"=>News::HOME_PAGE_SHOW_STATUS['DISABLED'],
        );
    }

    protected function getNewsAttributesData(
        $status = News::STATUS['ENABLED'],
        $stick = News::STICK['DISABLED']
    ) : array {
        $attributes = $this->getNewsCommonAttributesData($status, $stick);
        $attributes['description'] = "新闻描述";
        $attributes['createTime'] = 1654673891;
        $attributes['updateTime'] = 1654673891;
        $attributes['statusTime'] = 1654673891;

        return $attributes;
    }

    protected function getUnAuditNewsAttributesData(
        $status = News::STATUS['ENABLED'],
        $stick = News::STICK['DISABLED']
    ) : array {
        $attributes = $this->getNewsAttributesData($status, $stick);
        $attributes['applyInfoType'] = 1;
        $attributes['applyStatus'] = 0;
        $attributes['rejectReason'] = "";
        $attributes['operationType'] = 1;
        $attributes['newsId'] = 1;
        
        return $attributes;
    }

    protected function getNewsRequestRelationshipsData() : array
    {
        return array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            ),
            "newsType"=>array(
                "data"=>array(
                    array("type"=>"newsCategories","id"=>1)
                )
            )
        );
    }

    protected function getNewsRelationshipsData() : array
    {
        $relationships['crew'] = array("data"=>array("type"=>"crews","id"=>1));
        $relationships['publishUserGroup'] = array("data"=>array("type"=>"userGroups","id"=>1));
        $relationships['newsType'] = array("data"=>array("type"=>"newsCategories","id"=>1));

        return $relationships;
    }

    protected function getUnAuditNewsRelationshipsData() : array
    {
        $relationships = $this->getNewsRelationshipsData();
        $relationships['publishCrew'] = array("data"=>array("type"=>"crews","id"=>1));
        $relationships['applyCrew'] = array("data"=>array("type"=>"crews","id"=>1));
        $relationships['applyUserGroup'] = array("data"=>array("type"=>"userGroups","id"=>1));

        return $relationships;
    }

    protected function getNewsCommonIncludedData()
    {
        return [
            array(
                "type"=>"newsCategories",
                "id"=>"1",
                "attributes"=>array(
                    "name"=>"国家级工作动态",
                    "grade"=>3,
                    "parentCategory"=>1,
                    "category"=>4,
                    "status"=>0,
                    "createTime"=>1653892449,
                    "updateTime"=>1653892449,
                    "statusTime"=>0
                )
            ),
            array(
                "type"=>"userGroups",
                "id"=>"1",
                "attributes"=>array(
                    "name"=>"乌兰察布发展和改革委员会",
                    "shortName"=>"市发改委",
                    "unifiedSocialCreditCode"=>"123456789012345678",
                    "administrativeArea"=>0,
                    "status"=>0,
                    "createTime"=>1652949496,
                    "updateTime"=>1653114283,
                    "statusTime"=>0
                )
            ),
            array(
                "type"=>"crews",
                "id"=>"1",
                "attributes"=>array(
                    "realName"=>"超级管理员",
                    "cardId"=>"412825199009094538",
                    "userName"=>"18800000000",
                    "cellphone"=>"18800000000",
                    "category"=>1,
                    "purview"=>[],
                    "status"=>0,
                    "createTime"=>1516174523,
                    "updateTime"=>1516174523,
                    "statusTime"=>0
                ),
                "relationships"=>array(
                    "userGroup"=>array(
                        "data"=>array(
                            "type"=>"userGroups",
                            "id"=>"1"
                        )
                    ),
                    "department"=>array(
                        "data"=>array(
                            "type"=>"departments",
                            "id"=>"1"
                        )
                    )
                )
            )
        ];
    }

    protected function getNewsListData(): array
    {
        $attributes = $this->getNewsAttributesData();
        $relationships = $this->getNewsRelationshipsData();

        return [
            "meta" => [
                "count" => 4,
                "links" => [
                    "first" => null,
                    "last" => null,
                    "prev" => null,
                    "next" => null
                ]
            ],
            "links" => [
                "first" => null,
                "last" => null,
                "prev" => null,
                "next" => null
            ],
            "data" => [
                [
                    "type" => "news",
                    "id" => "15",
                    "attributes" =>$attributes,
                    "relationships" =>$relationships,
                    "links" => [
                        "self" => "127.0.0.1:8080/news/15"
                    ]
                ],
            ],
            "included" => $this->getNewsCommonIncludedData()
        ];
    }

    protected function getNewsDetailData(
        $status = News::STATUS['ENABLED'],
        $stick = News::STICK['DISABLED']
    ) : array {
        $attributes = $this->getNewsAttributesData($status, $stick);
        $relationships = $this->getNewsRelationshipsData();
        
        return [
            "meta" => [],
            "data" => [
                "type" => "news",
                "id" => 1,
                "attributes" => $attributes,
                "relationships" => $relationships,
                "links" => [
                    "self" => "127.0.0.1:8080/news/1"
                ]
            ],
            "included" => $this->getNewsCommonIncludedData()
        ];
    }

    protected function getUnAuditNewsListData(): array
    {
        $attributes = $this->getUnAuditNewsAttributesData();
        $relationships = $this->getUnAuditNewsRelationshipsData();

        return [
            "meta" => [
                "count" => 4,
                "links" => [
                    "first" => null,
                    "last" => null,
                    "prev" => null,
                    "next" => null
                ]
            ],
            "links" => [
                "first" => null,
                "last" => null,
                "prev" => null,
                "next" => null
            ],
            "data" => [
                [
                    "type" => "unAuditedNews",
                    "id" => "15",
                    "attributes" =>$attributes,
                    "relationships" =>$relationships,
                    "links" => [
                        "self" => "127.0.0.1:8080/unAuditedNews/15"
                    ]
                ],
            ],
            "included" => $this->getNewsCommonIncludedData()
        ];
    }

    protected function getUnAuditNewsDetailData() : array
    {
        $attributes = $this->getUnAuditNewsAttributesData();
        $relationships = $this->getUnAuditNewsRelationshipsData();
        
        return [
            "meta" => [],
            "data" => [
                "type" => "unAuditedNews",
                "id" => 1,
                "attributes" => $attributes,
                "relationships" => $relationships,
                "links" => [
                    "self" => "127.0.0.1:8080/unAuditedNews/1"
                ]
            ],
            "included" => $this->getNewsCommonIncludedData()
        ];
    }

    protected function getNewsAddRequestData() : array
    {
        $attributes = $this->getNewsCommonAttributesData();
        $relationships = $this->getNewsRequestRelationshipsData();

        return array(
            'data' => array(
                "type"=>"news",
                "attributes"=>$attributes,
                "relationships"=>$relationships,
            )
        );
    }

    protected function getNewsEditRequestData() : array
    {
        $attributes = $this->getNewsCommonAttributesData();
        $relationships = $this->getNewsRequestRelationshipsData();

        return array(
            'data' => array(
                "type"=>"news",
                "attributes"=>$attributes,
                "relationships"=>$relationships,
            )
        );
    }

    protected function getNewsStatusRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"news",
                "attributes"=>array(),
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );
    }

    protected function getUnAuditNewsEditRequestData() : array
    {
        $attributes = $this->getNewsCommonAttributesData();
        $relationships = $this->getNewsRequestRelationshipsData();

        return array(
            'data' => array(
                "type"=>"unAuditedNews",
                "attributes"=>$attributes,
                "relationships"=>$relationships,
            )
        );
    }

    protected function getUnAuditNewsApproveRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"unAuditedNews",
                "attributes"=>array(),
                "relationships"=>array(
                    "applyCrew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );
    }

    protected function getUnAuditNewsRejectRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"unAuditedNews",
                "attributes"=>array("rejectReason"=>"审核驳回原因"),
                "relationships"=>array(
                    "applyCrew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );
    }
}
