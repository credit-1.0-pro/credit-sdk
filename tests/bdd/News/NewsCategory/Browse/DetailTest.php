<?php
namespace Sdk\News\NewsCategory\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\NewsCategoryDataTrait;
use Sdk\News\NewsCategory\NewsCategoryRestfulUtils;
use Sdk\News\NewsCategory\Adapter\NewsCategory\NewsCategoryRestfulAdapter;

/**
 * @Feature: 我是平台管理员,当我需要查看所有新闻分类时,进入新闻分类管理列表,查看新闻分类信息,
 *           通过列表和详情的形式查看到我所有的新闻分类信息,以便于了解新闻分类的情况
 * @Scenario: 查看新闻分类数据详情
 */
class DetailTest extends TestCase
{
    use NewsCategoryDataTrait, NewsCategoryRestfulUtils;

    private $newsCategory;

    private $mock;

    public function setUp()
    {
        $this->newsCategory = new NewsCategory();
    }

    public function tearDown()
    {
        unset($this->newsCategory);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条新闻分类数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getNewsCategoryDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条新闻分类数据详情时
    */
    protected function fetchNewsCategory($id)
    {

        $adapter = new NewsCategoryRestfulAdapter();

        $this->newsCategory = $adapter->fetchOne($id);

        return $this->newsCategory;
    }

    /**
     * @Then 我可以看见新闻分类详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchNewsCategory($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'news/categories/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getNewsCategoryDetailData()['data'];

        $this->compareArrayAndObject($data, $this->newsCategory);
    }
}
