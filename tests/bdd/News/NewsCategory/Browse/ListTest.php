<?php


namespace Sdk\News\NewsCategory\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\NewsCategoryDataTrait;
use Sdk\News\NewsCategory\NewsCategoryRestfulUtils;

/**
 * @Feature: 我是平台管理员,当我需要查看所有新闻分类时,进入新闻分类管理列表,查看新闻分类信息,
 *           通过列表和详情的形式查看到我所有的新闻分类信息,以便于了解新闻分类的情况
 * @Scenario: 存在新闻分类数据
 */
class ListTest extends TestCase
{
    use NewsCategoryDataTrait, NewsCategoryRestfulUtils;

    private $newsCategory;

    private $mock;

    public function setUp()
    {
        $this->newsCategory = new NewsCategory();
    }

    public function tearDown()
    {
        unset($this->newsCategory);
        unset($this->mock);
    }

    /**
     * @Given: 存在新闻分类数据
     */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getNewsCategoryListData());

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看新闻分类列表时
     */
    public function fetchNewsCategoryList() : array
    {
        $filter = [];
        return $this->getNewsCategoryList($filter);
    }

    /**
     * @Then  我可以看见新闻分类数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchNewsCategoryList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('news/categories', $request->getUri()->getPath());
    }

    private function response()
    {
        $newsCategoryList = $this->fetchNewsCategoryList();
        $newsCategoryListArray = $this->getNewsCategoryListData()['data'];

        foreach ($newsCategoryList as $newsCategory) {
            foreach ($newsCategoryListArray as $newsCategoryArray) {
                if ($newsCategoryArray['id'] == $newsCategory->getId()) {
                    $this->compareArrayAndObject($newsCategoryArray, $newsCategory);
                }
            }
        }
    }
}
