<?php
namespace Sdk\News\NewsCategory;

trait NewsCategoryRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $newsCategory
    ) {
        $this->assertEquals($expectedArray['id'], $newsCategory->getId());
        $this->assertEquals($expectedArray['attributes']['name'], $newsCategory->getName());
        if (isset($expectedArray['attributes']['grade'])) {
            $this->assertEquals($expectedArray['attributes']['grade'], $newsCategory->getGrade());
        }
        if (isset($expectedArray['attributes']['parentCategory'])) {
            $this->assertEquals($expectedArray['attributes']['parentCategory'], $newsCategory->getParentCategory());
        }
        if (isset($expectedArray['attributes']['category'])) {
            $this->assertEquals($expectedArray['attributes']['category'], $newsCategory->getCategory());
        }

        if (isset($expectedArray['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['attributes']['createTime'], $newsCategory->getCreateTime());
        }
        if (isset($expectedArray['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['attributes']['updateTime'], $newsCategory->getUpdateTime());
        }
        if (isset($expectedArray['attributes']['status'])) {
            $this->assertEquals($expectedArray['attributes']['status'], $newsCategory->getStatus());
        }
    }
}
