<?php


namespace Sdk\News\NewsCategory\Add;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\NewsCategoryDataTrait;
use Sdk\News\NewsCategory\NewsCategoryRestfulUtils;

/**
 * @Feature: 我是平台管理员,当我需要新增新闻分类时,在新闻分类管理中,新增对应的新闻分类数据
 *           根据我归集到的新闻分类数据新增,以便于我能更好的管理新闻分类信息
 * @Scenario: 正常新增新闻分类数据
 */
class SuccessTest extends TestCase
{
    use NewsCategoryDataTrait, NewsCategoryRestfulUtils, ErrorDataTrait;

    private $addNewsCategory;

    private $mock;

    public function setUp()
    {
        $this->addNewsCategory = new NewsCategory();
    }

    public function tearDown()
    {
        unset($this->addNewsCategory);
        unset($this->mock);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();
        $errorData = json_encode($data);

        $data = $this->getNewsCategoryDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $data = $this->getNewsCategoryDetailData()['data'];

        $this->addNewsCategory = new NewsCategory();
        $this->addNewsCategory->setName($data['attributes']['name']);
        $this->addNewsCategory->setGrade($data['attributes']['grade']);
        $this->addNewsCategory->setParentCategory($data['attributes']['parentCategory']);
        $this->addNewsCategory->setCategory($data['attributes']['category']);

        return $this->addNewsCategory->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }


    private function request()
    {
        $data = $this->getNewsCategoryAddRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/news/categories', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getNewsCategoryDetailData()['data'];

        $this->compareArrayAndObject($data, $this->addNewsCategory);
    }
}
