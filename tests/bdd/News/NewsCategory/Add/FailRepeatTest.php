<?php


namespace Sdk\News\NewsCategory\Add;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\NewsCategoryDataTrait;
use Sdk\News\NewsCategory\NewsCategoryRestfulUtils;

/**
 * @Feature: 我是平台管理员,当我需要新增新闻分类时,在新闻分类管理中,新增对应的新闻分类数据
 *           根据我归集到的新闻分类数据新增,以便于我能更好的管理新闻分类信息
 * @Scenario: 异常流程-数据重复,新增失败
 */
class FailRepeatTest extends TestCase
{
    use NewsCategoryDataTrait, NewsCategoryRestfulUtils, ErrorDataTrait;

    private $newsCategory;

    private $mock;

    public function setUp()
    {
        $this->newsCategory = new NewsCategory();
    }

    public function tearDown()
    {
        unset($this->newsCategory);
        unset($this->mock);
    }

    /**
     * @Given: 我已经新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();
        $errorResourceNotExistData = json_encode($data);

        $data = $this->getErrorResourceIsUnique('newsCategoryName');
        $errorResourceIsUniqueData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorResourceNotExistData),
            new Response(409, ['Content-Type' => 'application/vnd.api+json'], $errorResourceIsUniqueData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回false
     */
    protected function add()
    {
        $data = $this->getNewsCategoryDetailData()['data'];

        $this->newsCategory = new NewsCategory();
        $this->newsCategory->setName($data['attributes']['name']);
        $this->newsCategory->setGrade($data['attributes']['grade']);
        $this->newsCategory->setParentCategory($data['attributes']['parentCategory']);
        $this->newsCategory->setCategory($data['attributes']['category']);

        return $this->newsCategory->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();
        $data = $this->getNewsCategoryAddRequestData();

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/news/categories', $request->getUri()->getPath());
    }

    private function response()
    {
        $this->assertEmpty($this->newsCategory->getId());
    }
}
