<?php


namespace Sdk\News\NewsCategory\Edit;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\News\NewsCategory\Model\NewsCategory;
use Sdk\News\NewsCategory\NewsCategoryDataTrait;
use Sdk\News\NewsCategory\NewsCategoryRestfulUtils;

/**
 * @Feature: 我是平台管理员,当我需要编辑新闻时,在新闻管理中,编辑对应的新闻数据
 *           根据我归集到的新闻数据编辑,以便于我能更好的管理新闻信息
 * @Scenario: 正常编辑新闻数据
 */
class SuccessTest extends TestCase
{
    use NewsCategoryDataTrait, NewsCategoryRestfulUtils, ErrorDataTrait;

    private $newsCategory;

    private $mock;

    public function setUp()
    {
        $this->newsCategory = new NewsCategory();
    }

    public function tearDown()
    {
        unset($this->newsCategory);
        unset($this->mock);
    }

    /**
     * @Given: 我编辑数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();
        $errorData = json_encode($data);

        $data = $this->getNewsCategoryDetailData();
        $requestDataAttributes = $this->getNewsCategoryEditRequestData()['data']['attributes'];

        $data['data']['attributes']['name'] = $requestDataAttributes['name'];

        $editJsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorData),
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $editJsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用编辑函数,期待返回true
     */
    protected function edit()
    {
        $data = $this->getNewsCategoryEditRequestData()['data'];

        $this->newsCategory = new NewsCategory();
        $this->newsCategory->setId(1);
        $this->newsCategory->setName($data['attributes']['name']);

        return $this->newsCategory->edit();
    }

    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->edit();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getNewsCategoryEditRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/news/categories/1', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getNewsCategoryEditRequestData()['data'];
        $data['id'] = 1;

        $this->compareArrayAndObject($data, $this->newsCategory);
    }
}
