<?php


namespace Sdk\News\NewsCategory;

use Sdk\News\NewsCategory\Adapter\NewsCategory\NewsCategoryRestfulAdapter;

trait NewsCategoryDataTrait
{
    protected function getNewsCategoryList(array $filter = []) : array
    {
        $adapter = new NewsCategoryRestfulAdapter();

        list($count, $newsCategoryList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $newsCategoryList;
    }

    protected function getNewsCategoryListData(): array
    {
        return [
            "meta" => [
                "count" => 4,
                "links" => [
                    "first" => null,
                    "last" => null,
                    "prev" => null,
                    "next" => null
                ]
            ],
            "links" => [
                "first" => null,
                "last" => null,
                "prev" => null,
                "next" => null
            ],
            "data" => [
                [
                    "type" => "newsCategories",
                    "id" => "15",
                    "attributes" => [
                        "name" => "信易+",
                        "grade" => 3,
                        "parentCategory" => 0,
                        "category" => 0,
                        "status" => 0,
                        "createTime" => 1653892480,
                        "updateTime" => 1653892480,
                        "statusTime" => 0
                    ],
                    "links" => [
                        "self" => "127.0.0.1:8080/newsCategories/15"
                    ]
                ],
                [
                    "type" => "newsCategories",
                    "id" => "1",
                    "attributes" => [
                        "name" => "信用动态",
                        "grade" => 1,
                        "parentCategory" => 0,
                        "category" => 0,
                        "status" => 0,
                        "createTime" => 1653891571,
                        "updateTime" => 1653891571,
                        "statusTime" => 0
                    ],
                    "links" => [
                        "self" => "127.0.0.1:8080/newsCategories/1"
                    ]
                ]
            ]
        ];
    }

    protected function getNewsCategoryDetailData(
        int $id = 1
    ) : array {
        return [
            "meta" => [],
            "data" => [
                "type" => "newsCategories",
                "id" => $id,
                "attributes" => [
                    "name" => "联合奖惩",
                    "grade" => 1,
                    "parentCategory" => 0,
                    "category" => 0,
                    "status" => 0,
                    "createTime" => 1653892359,
                    "updateTime" => 1653892359,
                    "statusTime" => 0
                ],
                "links" => [
                    "self" => "127.0.0.1:8080/newsCategories/1"
                ]
            ]
        ];
    }

    protected function getNewsCategoryAddRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"newsCategories",
                "attributes"=>array(
                    "name"=>"联合奖惩",
                    "grade"=>1,
                    "parentCategory"=>0,
                    "category"=>0
                ),
            )
        );
    }

    protected function getNewsCategoryEditRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"newsCategories",
                "attributes"=>array(
                    "name"=>"工作动态"
                ),
            )
        );
    }
}
