<?php


namespace Sdk\News\UnAuditNews\Edit;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\News\News\Model\UnAuditNews;
use Sdk\News\NewsDataTrait;
use Sdk\News\NewsRestfulUtils;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要编辑新闻信息时,在审核表中,编辑已驳回新闻数据
 *           通过编辑新闻界面，并根据我所采集的新闻数据进行编辑,以便于我可以更好的维护新闻管理列表
 * @Scenario: 正常编辑新闻审核数据
 */
class SuccessTest extends TestCase
{
    use NewsDataTrait, NewsRestfulUtils;

    private $unAuditedNews;

    private $mock;

    public function setUp()
    {
        $this->unAuditedNews = new UnAuditNews();
    }

    public function tearDown()
    {
        unset($this->unAuditedNews);
        unset($this->mock);
    }

    /**
     * @Given: 我编辑数据
     */
    protected function prepareData()
    {
        $data = $this->getUnAuditNewsDetailData();
        $editJsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $editJsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用编辑函数,期待返回true
     */
    protected function edit()
    {
        $data = $this->getUnAuditNewsEditRequestData()['data'];

        $this->unAuditedNews = new UnAuditNews();
        $this->unAuditedNews->setId(1);
        $this->unAuditedNews->setTitle($data['attributes']['title']);
        $this->unAuditedNews->setSource($data['attributes']['source']);
        $this->unAuditedNews->setCover($data['attributes']['cover']);
        $this->unAuditedNews->setAttachments($data['attributes']['attachments']);
        $this->unAuditedNews->setContent($data['attributes']['content']);
        $this->unAuditedNews->setStatus($data['attributes']['status']);
        $this->unAuditedNews->setStick($data['attributes']['stick']);
        $this->unAuditedNews->setReleaseTime($data['attributes']['releaseTime']);
        $this->unAuditedNews->setDimension($data['attributes']['dimension']);
        $this->unAuditedNews->setBannerStatus($data['attributes']['bannerStatus']);
        $this->unAuditedNews->setBannerImage($data['attributes']['bannerImage']);
        $this->unAuditedNews->setHomePageShowStatus($data['attributes']['homePageShowStatus']);
        $this->unAuditedNews->getNewsType()->setId($data['relationships']['newsType']['data'][0]['id']);
        $this->unAuditedNews->getCrew()->setId($data['relationships']['crew']['data'][0]['id']);


        return $this->unAuditedNews->edit();
    }

    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->edit();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getUnAuditNewsEditRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/news/unAuditedNews/1/resubmit', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getUnAuditNewsDetailData()['data'];
        $data['id'] = 1;

        $this->compareArrayAndObjectCommonUnAuditNews($data, $this->unAuditedNews);
    }
}
