<?php
namespace Sdk\News\UnAuditNews\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\News\News\Model\UnAuditNews;
use Sdk\News\NewsDataTrait;
use Sdk\News\NewsRestfulUtils;
use Sdk\News\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户,我拥有新闻发布权限、且当我需要查看未审核或已驳回的新闻列表时,
 *           在审核表中,通过列表与详情的形式查看到未审核或已驳回的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻审核数据详情
 */
class DetailTest extends TestCase
{
    use NewsDataTrait, NewsRestfulUtils;

    private $unAuditNews;

    private $mock;

    public function setUp()
    {
        $this->unAuditNews = new UnAuditNews();
    }

    public function tearDown()
    {
        unset($this->unAuditNews);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条新闻数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getUnAuditNewsDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条新闻数据详情时
    */
    protected function fetchUnAuditNews($id)
    {
        $adapter = new UnAuditNewsRestfulAdapter();

        $this->unAuditNews = $adapter->fetchOne($id);

        return $this->unAuditNews;
    }

    /**
     * @Then 我可以看见新闻详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchUnAuditNews($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'news/unAuditedNews/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getUnAuditNewsDetailData()['data'];

        $this->compareArrayAndObjectCommonUnAuditNews($data, $this->unAuditNews);
    }
}
