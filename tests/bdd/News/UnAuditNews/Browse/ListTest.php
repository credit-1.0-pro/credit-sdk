<?php


namespace Sdk\News\UnAuditNews\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\News\News\Model\UnAuditNews;
use Sdk\News\NewsDataTrait;
use Sdk\News\NewsRestfulUtils;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有新闻发布权限、且当我需要查看通过审核的新闻的列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻列表
 */
class ListTest extends TestCase
{
    use NewsDataTrait, NewsRestfulUtils;

    private $unAuditNews;

    private $mock;

    public function setUp()
    {
        $this->unAuditNews = new UnAuditNews();
    }

    public function tearDown()
    {
        unset($this->unAuditNews);
        unset($this->mock);
    }

    /**
     * @Given: 存在新闻数据
     */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getUnAuditNewsListData());

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看新闻列表时
     */
    public function fetchUnAuditNewsList() : array
    {
        $filter = [];
        return $this->getUnAuditNewsList($filter);
    }

    /**
     * @Then  我可以看见新闻数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUnAuditNewsList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('news/unAuditedNews', $request->getUri()->getPath());
    }

    private function response()
    {
        $unAuditNewsList = $this->fetchUnAuditNewsList();
        $unAuditNewsListArray = $this->getUnAuditNewsListData()['data'];

        foreach ($unAuditNewsList as $unAuditNews) {
            foreach ($unAuditNewsListArray as $unAuditNewsArray) {
                if ($unAuditNewsArray['id'] == $unAuditNews->getId()) {
                    $this->compareArrayAndObjectCommonUnAuditNews($unAuditNewsArray, $unAuditNews);
                }
            }
        }
    }
}
