<?php


namespace Sdk\News\UnAuditNews\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\News\News\Model\UnAuditNews;
use Sdk\News\NewsDataTrait;
use Sdk\News\NewsRestfulUtils;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户,我拥有新闻发布权限、且当我需要查看未审核或已驳回的新闻列表时,
 *           在审核表中,通过列表与详情的形式查看到未审核或已驳回的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻审核列表
 */
class FailEmptyTest extends TestCase
{
    use NewsDataTrait, NewsRestfulUtils, ErrorDataTrait;

    private $unAuditNews;

    private $mock;

    public function setUp()
    {
        $this->unAuditNews = new UnAuditNews();
    }

    public function tearDown()
    {
        unset($this->unAuditNews);
        unset($this->mock);
    }

    /**
     * @Given: 不存在新闻数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
                new Response(404, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看新闻列表时
     */
    public function fetchUnAuditNewsList() : array
    {
        $filter = [];
        return $this->unAuditNews = $this->getUnAuditNewsList($filter);
    }

    /**
     * @Then  我可以看见新闻数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUnAuditNewsList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('news/unAuditedNews', $request->getUri()->getPath());
    }

    private function response()
    {
        $this->assertEmpty($this->unAuditNews);
    }
}
