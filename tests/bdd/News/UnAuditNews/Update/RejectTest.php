<?php


namespace Sdk\News\UnAuditNews\Update;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\News\News\Model\UnAuditNews;
use Sdk\News\NewsDataTrait;
use Sdk\News\NewsRestfulUtils;
use Sdk\News\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter;

/**
 * @Feature: 我是拥有新闻审核权限的平台管理员/委办局管理员/操作用户,当我需要审核一个新闻时,在审核表中,审核待审核的新闻数据
 *           通过新闻详情页面的审核通过与审核驳回操作,以便于我维护新闻列表
 * @Scenario: 审核通过
 */
class RejectTest extends TestCase
{
    use NewsDataTrait, NewsRestfulUtils;

    private $rejectUnAuditNews;

    private $mock;

    public function setUp()
    {
        $this->rejectUnAuditNews = new UnAuditNews();
    }

    public function tearDown()
    {
        unset($this->rejectUnAuditNews);
        unset($this->mock);
    }

    /**
     * @Given: 存在需要待审核的数据
     */
    protected function prepareData()
    {
        $data = $this->getUnAuditNewsDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要待审核的新闻
     */
    protected function fetchUnAuditNews()
    {
        $adapter = new UnAuditNewsRestfulAdapter();
        $this->rejectUnAuditNews = $adapter->fetchOne(1);

        return $this->rejectUnAuditNews;
    }

    /**
     * @When:当我调用待审核函数,期待返回true
     */
    protected function reject()
    {
        $this->rejectUnAuditNews->setRejectReason("审核驳回原因");
        $this->rejectUnAuditNews->getApplyCrew()->setId(2);

        return $this->rejectUnAuditNews->reject();
    }

    /**
     * @Then  我可以查到待审核的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUnAuditNews();
        $this->reject();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(
            json_encode($this->getUnAuditNewsRejectRequestData()),
            $request->getBody()->getContents()
        );
        $this->assertEquals('/news/unAuditedNews/1/reject', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getUnAuditNewsDetailData()['data'];

        $this->compareArrayAndObjectCommonUnAuditNews($data, $this->rejectUnAuditNews);
    }
}
