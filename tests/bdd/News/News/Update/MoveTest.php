<?php


namespace Sdk\News\News\Update;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\News\News\Model\News;
use Sdk\News\NewsDataTrait;
use Sdk\News\NewsRestfulUtils;
use Sdk\News\News\Adapter\News\NewsRestfulAdapter;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要移动新闻时,在发布表中,移动对应新闻数据
 *           通过移动新闻界面，并根据我所采集的新闻数据进行移动,以便于我可以更好的维护新闻管理列表
 * @Scenario: 正常移动新闻数据
 */
class MoveTest extends TestCase
{
    use NewsDataTrait, NewsRestfulUtils;

    private $news;

    private $mock;

    public function setUp()
    {
        $this->news = new News();
    }

    public function tearDown()
    {
        unset($this->news);
        unset($this->mock);
    }

    /**
     * @Given: 存在需要移动的数据
     */
    protected function prepareData()
    {
        $data = $this->getNewsDetailData();
        $moveJsonData = json_encode($data);

        $data = $this->getNewsDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $moveJsonData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要移动的新闻
     */
    protected function fetchNews()
    {
        $adapter = new NewsRestfulAdapter();
        $this->news = $adapter->fetchOne(1);

        return $this->news;
    }

    /**
     * @When:当我调用移动函数,期待返回true
     */
    protected function move()
    {
        $this->news->getNewsType()->setId(2);
        $this->news->getCrew()->setId(2);

        return $this->news->move();
    }

    /**
     * @Then  我可以查到移动的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchNews();
        $this->move();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($this->getNewsStatusRequestData()), $request->getBody()->getContents());
        $this->assertEquals('/news/1/move/2', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getNewsDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->news);
    }
}
