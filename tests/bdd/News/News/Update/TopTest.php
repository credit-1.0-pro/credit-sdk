<?php


namespace Sdk\News\News\Update;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\News\News\Model\News;
use Sdk\News\NewsDataTrait;
use Sdk\News\NewsRestfulUtils;
use Sdk\News\News\Adapter\News\NewsRestfulAdapter;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要修改某个特定新闻置顶时,在发布表的操作栏中,将新闻修改为置顶状态
 *           通过发布表的操作栏中的置顶操作,以便于我可以更好的维护新闻发布管理列表
 * @Scenario: 置顶新闻
 */
class TopTest extends TestCase
{
    use NewsDataTrait, NewsRestfulUtils;

    private $news;

    private $mock;

    public function setUp()
    {
        $this->news = new News();
    }

    public function tearDown()
    {
        unset($this->news);
        unset($this->mock);
    }

    /**
     * @Given: 存在需要置顶的数据
     */
    protected function prepareData()
    {
        $data = $this->getNewsDetailData();
        $topJsonData = json_encode($data);

        $data = $this->getNewsDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $topJsonData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要置顶的新闻
     */
    protected function fetchNews()
    {
        $adapter = new NewsRestfulAdapter();
        $this->news = $adapter->fetchOne(1);

        return $this->news;
    }

    /**
     * @When:当我调用置顶函数,期待返回true
     */
    protected function top()
    {
        $this->news->getCrew()->setId(2);

        return $this->news->top();
    }

    /**
     * @Then  我可以查到置顶的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchNews();
        $this->top();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($this->getNewsStatusRequestData()), $request->getBody()->getContents());
        $this->assertEquals('/news/1/top', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getNewsDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->news);
    }
}
