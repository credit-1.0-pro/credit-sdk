<?php


namespace Sdk\News\News\Edit;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\News\News\Model\News;
use Sdk\News\NewsDataTrait;
use Sdk\News\NewsRestfulUtils;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要编辑新闻信息时,在发布表中,编辑对应新闻数据
 *           通过编辑新闻界面，并根据我所采集的新闻数据进行编辑,以便于我可以更好的维护新闻管理列表
 * @Scenario: 正常编辑新闻数据
 */
class SuccessTest extends TestCase
{
    use NewsDataTrait, NewsRestfulUtils;

    private $news;

    private $mock;

    public function setUp()
    {
        $this->news = new News();
    }

    public function tearDown()
    {
        unset($this->news);
        unset($this->mock);
    }

    /**
     * @Given: 我编辑数据
     */
    protected function prepareData()
    {
        $data = $this->getNewsDetailData();
        $editJsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $editJsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用编辑函数,期待返回true
     */
    protected function edit()
    {
        $data = $this->getNewsEditRequestData()['data'];

        $this->news = new News();
        $this->news->setId(1);
        $this->news->setTitle($data['attributes']['title']);
        $this->news->setSource($data['attributes']['source']);
        $this->news->setCover($data['attributes']['cover']);
        $this->news->setAttachments($data['attributes']['attachments']);
        $this->news->setContent($data['attributes']['content']);
        $this->news->setStatus($data['attributes']['status']);
        $this->news->setStick($data['attributes']['stick']);
        $this->news->setReleaseTime($data['attributes']['releaseTime']);
        $this->news->setDimension($data['attributes']['dimension']);
        $this->news->setBannerStatus($data['attributes']['bannerStatus']);
        $this->news->setBannerImage($data['attributes']['bannerImage']);
        $this->news->setHomePageShowStatus($data['attributes']['homePageShowStatus']);
        $this->news->getNewsType()->setId($data['relationships']['newsType']['data'][0]['id']);
        $this->news->getCrew()->setId($data['relationships']['crew']['data'][0]['id']);


        return $this->news->edit();
    }

    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->edit();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getNewsEditRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/news/1', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getNewsDetailData()['data'];
        $data['id'] = 1;

        $this->compareArrayAndObjectCommon($data, $this->news);
    }
}
