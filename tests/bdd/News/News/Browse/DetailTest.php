<?php
namespace Sdk\News\News\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\News\News\Model\News;
use Sdk\News\NewsDataTrait;
use Sdk\News\NewsRestfulUtils;
use Sdk\News\News\Adapter\News\NewsRestfulAdapter;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有新闻发布权限、且当我需要查看通过审核的新闻的列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻数据详情
 */
class DetailTest extends TestCase
{
    use NewsDataTrait, NewsRestfulUtils;

    private $news;

    private $mock;

    public function setUp()
    {
        $this->news = new News();
    }

    public function tearDown()
    {
        unset($this->news);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条新闻数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getNewsDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条新闻数据详情时
    */
    protected function fetchNews($id)
    {
        $adapter = new NewsRestfulAdapter();

        $this->news = $adapter->fetchOne($id);

        return $this->news;
    }

    /**
     * @Then 我可以看见新闻详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchNews($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'news/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getNewsDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->news);
    }
}
