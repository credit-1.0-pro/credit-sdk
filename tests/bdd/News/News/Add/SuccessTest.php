<?php


namespace Sdk\News\News\Add;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\News\News\Model\News;
use Sdk\News\NewsDataTrait;
use Sdk\News\NewsRestfulUtils;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要新增一个新闻时,在发布表中,新增对应的新闻数据
 *           通过新增新闻界面，并根据我所采集的新闻数据进行新增,以便于我维护新闻列表
 * @Scenario: 正常新增数据数据
 */
class SuccessTest extends TestCase
{
    use NewsDataTrait, NewsRestfulUtils;

    private $addNews;

    private $mock;

    public function setUp()
    {
        $this->addNews = new News();
    }

    public function tearDown()
    {
        unset($this->addNews);
        unset($this->mock);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getNewsDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $data = $this->getNewsDetailData()['data'];

        $this->addNews = new News();

        $this->addNews->setTitle($data['attributes']['title']);
        $this->addNews->setSource($data['attributes']['source']);
        $this->addNews->setCover($data['attributes']['cover']);
        $this->addNews->setAttachments($data['attributes']['attachments']);
        $this->addNews->setContent($data['attributes']['content']);
        $this->addNews->setStatus($data['attributes']['status']);
        $this->addNews->setStick($data['attributes']['stick']);
        $this->addNews->setReleaseTime($data['attributes']['releaseTime']);
        $this->addNews->setDimension($data['attributes']['dimension']);
        $this->addNews->setBannerStatus($data['attributes']['bannerStatus']);
        $this->addNews->setBannerImage($data['attributes']['bannerImage']);
        $this->addNews->setHomePageShowStatus($data['attributes']['homePageShowStatus']);
        $this->addNews->getNewsType()->setId($data['relationships']['newsType']['data']['id']);
        $this->addNews->getCrew()->setId($data['relationships']['crew']['data']['id']);

        return $this->addNews->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getNewsAddRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/news', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getNewsDetailData()['data'];

        $this->compareArrayAndObjectCommon($data, $this->addNews);
    }
}
