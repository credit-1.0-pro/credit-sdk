<?php
namespace Sdk\News;

trait NewsRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $news
    ) {
        $this->assertEquals($expectedArray['id'], $news->getId());
        
        $this->assertEquals($expectedArray['attributes']['title'], $news->getTitle());
        $this->assertEquals($expectedArray['attributes']['dimension'], $news->getDimension());
        if (isset($expectedArray['attributes']['description'])) {
            $this->assertEquals($expectedArray['attributes']['description'], $news->getDescription());
        }
        $this->assertEquals($expectedArray['attributes']['releaseTime'], $news->getReleaseTime());
        $this->assertEquals($expectedArray['attributes']['bannerImage'], $news->getBannerImage());
        $this->assertEquals($expectedArray['attributes']['content'], $news->getContent());
        $this->assertEquals($expectedArray['attributes']['stick'], $news->getStick());
        $this->assertEquals($expectedArray['attributes']['attachments'], $news->getAttachments());
        $this->assertEquals($expectedArray['attributes']['cover'], $news->getCover());
        $this->assertEquals($expectedArray['attributes']['source'], $news->getSource());
        $this->assertEquals($expectedArray['attributes']['bannerStatus'], $news->getBannerStatus());
        $this->assertEquals(
            $expectedArray['attributes']['homePageShowStatus'],
            $news->getHomePageShowStatus()
        );

        if (isset($expectedArray['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['attributes']['createTime'], $news->getCreateTime());
        }
        if (isset($expectedArray['attributes']['statusTime'])) {
            $this->assertEquals($expectedArray['attributes']['statusTime'], $news->getStatusTime());
        }
        if (isset($expectedArray['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['attributes']['updateTime'], $news->getUpdateTime());
        }
        $this->assertEquals($expectedArray['attributes']['status'], $news->getStatus());

        $this->compareCrew($expectedArray, $news);
        $this->compareNewsType($expectedArray, $news);
        $this->comparePublishUserGroup($expectedArray, $news);
    }
    
    private function compareCrew(array $expectedArray, $news)
    {
        if (isset($expectedArray['relationships']['crew'])) {
            $this->assertEquals($expectedArray['relationships']['crew']['data']['type'], 'crews');
            $this->assertEquals(
                $expectedArray['relationships']['crew']['data']['id'],
                $news->getCrew()->getId()
            );
        }
    }

    private function comparePublishUserGroup(array $expectedArray, $news)
    {
        if (isset($expectedArray['relationships']['publishUserGroup'])) {
            $this->assertEquals($expectedArray['relationships']['publishUserGroup']['data']['type'], 'userGroups');
            $this->assertEquals(
                $expectedArray['relationships']['publishUserGroup']['data']['id'],
                $news->getUserGroup()->getId()
            );
        }
    }

    private function compareNewsType(array $expectedArray, $news)
    {
        if (isset($expectedArray['relationships']['newsType'])) {
            $this->assertEquals($expectedArray['relationships']['newsType']['data']['type'], 'newsCategories');
            $this->assertEquals(
                $expectedArray['relationships']['newsType']['data']['id'],
                $news->getNewsType()->getId()
            );
        }
    }

    private function compareArrayAndObjectCommonUnAuditNews(
        array $expectedArray,
        $unAuditNews
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditNews);
        
        if (isset($expectedArray['attributes']['applyStatus'])) {
            $this->assertEquals($expectedArray['attributes']['applyStatus'], $unAuditNews->getApplyStatus());
        }
        if (isset($expectedArray['attributes']['operationType'])) {
            $this->assertEquals($expectedArray['attributes']['operationType'], $unAuditNews->getOperationType());
        }
        if (isset($expectedArray['attributes']['applyInfoType'])) {
            $this->assertEquals($expectedArray['attributes']['applyInfoType'], $unAuditNews->getApplyInfoType());
        }
        if (isset($expectedArray['attributes']['relationId'])) {
            $this->assertEquals($expectedArray['attributes']['relationId'], $unAuditNews->getRelationId());
        }

        $this->assertEquals($expectedArray['attributes']['rejectReason'], $unAuditNews->getRejectReason());

        if (isset($expectedArray['relationships']['applyCrew'])) {
            $this->assertEquals($expectedArray['relationships']['applyCrew']['data']['type'], 'crews');
            $this->assertEquals(
                $expectedArray['relationships']['applyCrew']['data']['id'],
                $unAuditNews->getApplyCrew()->getId()
            );
        }

        if (isset($expectedArray['relationships']['publishCrew'])) {
            $this->assertEquals($expectedArray['relationships']['publishCrew']['data']['type'], 'crews');
            $this->assertEquals(
                $expectedArray['relationships']['publishCrew']['data']['id'],
                $unAuditNews->getPublishCrew()->getId()
            );
        }

        if (isset($expectedArray['relationships']['applyUserGroup'])) {
            $this->assertEquals($expectedArray['relationships']['applyUserGroup']['data']['type'], 'userGroups');
            $this->assertEquals(
                $expectedArray['relationships']['applyUserGroup']['data']['id'],
                $unAuditNews->getApplyUserGroup()->getId()
            );
        }
    }
}
