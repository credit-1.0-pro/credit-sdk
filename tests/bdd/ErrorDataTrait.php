<?php


namespace Sdk;

trait ErrorDataTrait
{
    protected function getErrorResourceNotExistData(): array
    {
        return [
            "errors" => [
                [
                    "id" => "10",
                    "links" => [
                        "about" => ""
                    ],
                    "status" => "404",
                    "code" => "RESOURCE_NOT_EXIST",
                    "title" => "Resource not exist",
                    "detail" => "Server can not find resource",
                    "source" => [],
                    "meta" => []
                ]
            ]
        ];
    }

    protected function getErrorResourceIsUnique(string $pointer = ''): array
    {
        return [
            "errors" => [
                [
                    "id" => "103",
                    "links" => [
                        "about" => ""
                    ],
                    "status" => "409",
                    "code" => "RESOURCE_ALREADY_EXIST",
                    "title" => "资源已存在",
                    "detail" => "表述传输了一个已存在的资源",
                    "source" => ["pointer"=>$pointer],
                    "meta" => []
                ]
            ]
        ];
    }

    protected function getErrorOldPasswordInCorrectData(): array
    {
        return [
            "errors" => [
                [
                    "id" => "502",
                    "links" => [
                        "about" => ""
                    ],
                    "status" => "403",
                    "code" => "OLD_PASSWORD_INCORRECT",
                    "title" => "当前密码不正确,请重新输入",
                    "detail" => "当前密码不正确,请重新输入",
                    "source" => ['pointer'=>'oldPassword'],
                    "meta" => []
                ]
            ]
        ];
    }
}
