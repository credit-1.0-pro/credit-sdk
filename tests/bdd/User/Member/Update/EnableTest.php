<?php


namespace Sdk\User\Member\Update;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\User\Member\Model\Member;
use Sdk\User\Member\MemberDataTrait;
use Sdk\User\Member\MemberRestfulUtils;
use Sdk\User\Member\Adapter\Member\MemberRestfulAdapter;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,当我需要使OA管理门户网注册的用户账号生效时,在门户网用户管理列表,可以启用某个用户的账号
 *           通过门户网用户管理列表中的启用操作,以便于我管理门户网用户列表
 * @Scenario: 启用用户数据
 */
class EnableTest extends TestCase
{
    use MemberDataTrait, MemberRestfulUtils, ErrorDataTrait;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new Member();
    }

    public function tearDown()
    {
        unset($this->member);
        unset($this->mock);
    }

    /**
     * @Given: 存在需要启用的数据
     */
    protected function prepareData()
    {
        $data = $this->getMemberDetailData(Member::STATUS['DISABLED']);
        $disableJsonData = json_encode($data);

        $data = $this->getMemberDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $disableJsonData),
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要启用的用户
     */
    protected function fetchMember()
    {
        $adapter = new MemberRestfulAdapter();
        $this->member = $adapter->fetchOne(1);

        return $this->member;
    }

    /**
     * @When:当我调用启用函数,期待返回true
     */
    protected function enable()
    {
        return $this->member->enable();
    }

    /**
     * @Then  我可以查到启用的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchMember();
        $this->enable();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals('[]', $request->getBody()->getContents());
        $this->assertEquals('/users/members/1/enable', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getMemberDetailData()['data'];

        $this->compareArrayAndObject($data, $this->member);
    }
}
