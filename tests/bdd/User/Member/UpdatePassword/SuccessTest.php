<?php


namespace Sdk\User\Member\UpdatePassword;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\User\Member\Model\Member;
use Sdk\User\Member\MemberDataTrait;
use Sdk\User\Member\MemberRestfulUtils;

/**
 * @Feature: 我是信用网站门户网站的用户,当我需要修改密码时,在用户中心页面,可以进行修改密码操作
 *           通过输入账号的旧密码、并输入修改后的密码及确认密码,以便于用户可以在个人中心就修改密码不用每次都对账号的密码进行重置
 * @Scenario: 修改密码成功
 */
class SuccessTest extends TestCase
{
    use MemberDataTrait, MemberRestfulUtils, ErrorDataTrait;

    private $updatePasswordMember;

    private $mock;

    public function setUp()
    {
        $this->updatePasswordMember = new Member();
    }

    public function tearDown()
    {
        unset($this->updatePasswordMember);
        unset($this->mock);
    }

    /**
     * @Given: 我并未重置过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getMemberDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function updatePassword()
    {
        $this->updatePasswordMember = new Member();

        $this->updatePasswordMember->setId(1);
        $this->updatePasswordMember->setPassword('Admin123$$');
        $this->updatePasswordMember->setOldPassword('Admin123$');

        return $this->updatePasswordMember->updatePassword();
    }

    /**
     * @Then  我可以查到重置的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->updatePassword();

        $this->request();
        $this->response();
    }


    private function request()
    {
        $data = $this->getMemberUpdatePasswordRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/users/members/1/updatePassword', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getMemberDetailData()['data'];

        $this->compareArrayAndObject($data, $this->updatePasswordMember);
    }
}
