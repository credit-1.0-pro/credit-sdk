<?php


namespace Sdk\User\Member\UpdatePassword;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\User\Member\Model\Member;
use Sdk\User\Member\MemberDataTrait;
use Sdk\User\Member\MemberRestfulUtils;

/**
 * @Feature: 我是信用网站门户网站的用户,当我需要修改密码时,在用户中心页面,可以进行修改密码操作
 *           通过输入账号的旧密码、并输入修改后的密码及确认密码,以便于用户可以在个人中心就修改密码不用每次都对账号的密码进行重置
 * @Scenario: 异常流程-旧密码不正确,修改密码失败
 */
class FailRepeatTest extends TestCase
{
    use MemberDataTrait, MemberRestfulUtils, ErrorDataTrait;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new Member();
    }

    public function tearDown()
    {
        unset($this->member);
        unset($this->mock);
    }

    /**
     * @Given: 不存在用户数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorOldPasswordInCorrectData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(502, ['Content-Type' => 'application/vnd.api+json'], $jsonData)

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用修改密码函数,期待返回false
     */
    protected function updatePassword()
    {
        $this->member = new Member();
        $this->member->setId(1);
        $this->member->setPassword('Admin123$$');
        $this->member->setOldPassword('Admin123$');

        return $this->member->updatePassword();
    }

    /**
     * @Then  用户密码修改失败
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->updatePassword();

        $this->request();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();
        $data = $this->getMemberUpdatePasswordRequestData();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/users/members/1/updatePassword', $request->getUri()->getPath());
    }
}
