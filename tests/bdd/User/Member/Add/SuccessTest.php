<?php


namespace Sdk\User\Member\Add;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\User\Member\Model\Member;
use Sdk\User\Member\MemberDataTrait;
use Sdk\User\Member\MemberRestfulUtils;

/**
 * @Feature: 我是信用网站门户网站的用户,当我需要为自己创建一个账号时,在门户网用户注册页面,注册一个门户网用户账号
 *           通过输入网页展示的信息,以便于我可以在信用网站上查询到更多的相关信息
 * @Scenario: 正常新增用户数据
 */
class SuccessTest extends TestCase
{
    use MemberDataTrait, MemberRestfulUtils, ErrorDataTrait;

    private $addMember;

    private $mock;

    public function setUp()
    {
        $this->addMember = new Member();
    }

    public function tearDown()
    {
        unset($this->addMember);
        unset($this->mock);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();
        $errorData = json_encode($data);

        $data = $this->getMemberDetailData(1);
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorData),
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorData),
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorData),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $data = $this->getMemberDetailData()['data'];

        $this->addMember = new Member();

        $this->addMember->setSecurityAnswer('黑色');
        $this->addMember->setPassword('Admin123$');
        $this->addMember->setEmail($data['attributes']['email']);
        $this->addMember->setCardId($data['attributes']['cardId']);
        $this->addMember->setUserName($data['attributes']['userName']);
        $this->addMember->setRealName($data['attributes']['realName']);
        $this->addMember->setCellphone($data['attributes']['cellphone']);
        $this->addMember->setContactAddress($data['attributes']['contactAddress']);
        $this->addMember->setSecurityQuestion($data['attributes']['securityQuestion']);

        return $this->addMember->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }


    private function request()
    {
        $data = $this->getMemberAddRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/users/members', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getMemberDetailData()['data'];

        $this->compareArrayAndObject($data, $this->addMember);
    }
}
