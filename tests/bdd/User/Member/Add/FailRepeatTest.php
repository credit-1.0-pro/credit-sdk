<?php


namespace Sdk\User\Member\Add;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\User\Member\Model\Member;
use Sdk\User\Member\MemberDataTrait;
use Sdk\User\Member\MemberRestfulUtils;

/**
 * @Feature: 我是信用网站门户网站的用户,当我需要为自己创建一个账号时,在门户网用户注册页面,注册一个门户网用户账号
 *           通过输入网页展示的信息,以便于我可以在信用网站上查询到更多的相关信息
 * @Scenario: 异常流程-数据重复,新增失败
 */
class FailRepeatTest extends TestCase
{
    use MemberDataTrait, MemberRestfulUtils, ErrorDataTrait;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new Member();
    }

    public function tearDown()
    {
        unset($this->member);
        unset($this->mock);
    }

    /**
     * @Given: 我已经新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();
        $errorResourceNotExistData = json_encode($data);

        $data = $this->getErrorResourceIsUnique('userName');
        $errorResourceIsUniqueData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorResourceNotExistData),
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorResourceNotExistData),
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorResourceNotExistData),
            new Response(409, ['Content-Type' => 'application/vnd.api+json'], $errorResourceIsUniqueData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回false
     */
    protected function add()
    {
        $data = $this->getMemberDetailData()['data'];

        $this->member = new Member();
        $this->member->setUserName($data['attributes']['userName']);
        $this->member->setRealName($data['attributes']['realName']);
        $this->member->setCardId($data['attributes']['cardId']);
        $this->member->setCellphone($data['attributes']['cellphone']);
        $this->member->setEmail($data['attributes']['email']);
        $this->member->setContactAddress($data['attributes']['contactAddress']);
        $this->member->setSecurityQuestion($data['attributes']['securityQuestion']);
        $this->member->setSecurityAnswer('黑色');
        $this->member->setPassword('Admin123$');

        return $this->member->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();
        $data = $this->getMemberAddRequestData();

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/users/members', $request->getUri()->getPath());
    }

    private function response()
    {
        $this->assertEmpty($this->member->getId());
    }
}
