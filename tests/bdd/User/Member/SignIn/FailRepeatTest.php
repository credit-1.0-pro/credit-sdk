<?php


namespace Sdk\User\Member\SignIn;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\User\Member\Model\Member;
use Sdk\User\Member\MemberDataTrait;
use Sdk\User\Member\MemberRestfulUtils;

 /**
 * @Feature: 我是信用网站门户网站的用户,当我需要了解信用网站门户中需要登录才可以查看的内容时,在门户网用户登录页面,登录自己在门户网的账号
 *           通过输入用户名、密码并拖动滑条 ,以便于信用网站区分不同类型的用户
 * @Scenario: 登录-异常流程,登录失败
 */
class FailRepeatTest extends TestCase
{
    use MemberDataTrait, MemberRestfulUtils, ErrorDataTrait;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new Member();
    }

    public function tearDown()
    {
        unset($this->member);
        unset($this->mock);
    }

    /**
     * @Given: 不存在登录的数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();
        $errorJsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorJsonData)
        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用登录函数,期待返回false
     */
    protected function signIn()
    {
        $data = $this->getMemberDetailData()['data'];

        $this->member = new Member();
        $this->member->setUserName($data['attributes']['userName']);
        $this->member->setPassword('Admin123$');

        return $this->member->signIn();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->signIn();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();
        $data = $this->getMemberSignInRequestData();

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/users/members/signIn', $request->getUri()->getPath());
    }

    private function response()
    {
        $this->assertEmpty($this->member->getId());
    }
}
