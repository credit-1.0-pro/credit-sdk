<?php


namespace Sdk\User\Member\ResetPassword;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\User\Member\Model\Member;
use Sdk\User\Member\MemberDataTrait;
use Sdk\User\Member\MemberRestfulUtils;

/**
 * @Feature: 我是信用网站门户网站的用户,当我忘记了我的密码时,在门户网用户忘记密码页面,找回自己的密码
 *           根据注册时所选择的密保问题与填写的密保答案,以便于我在忘记密码时，重置自己的密码
 * @Scenario: 重置密码成功
 */
class SuccessTest extends TestCase
{
    use MemberDataTrait, MemberRestfulUtils, ErrorDataTrait;

    private $resetPasswordMember;

    private $mock;

    public function setUp()
    {
        $this->resetPasswordMember = new Member();
    }

    public function tearDown()
    {
        unset($this->resetPasswordMember);
        unset($this->mock);
    }

    /**
     * @Given: 我并未重置过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getMemberDetailData();
        $requestDataAttributes = $this->getMemberResetPasswordRequestData()['data']['attributes'];
        $data['data']['attributes']['password'] = $requestDataAttributes['password'];
        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function resetPassword()
    {
        $data = $this->getMemberDetailData()['data'];

        $this->resetPasswordMember = new Member();

        $this->resetPasswordMember->setUserName($data['attributes']['userName']);
        $this->resetPasswordMember->setSecurityAnswer('黑色');
        $this->resetPasswordMember->setPassword('Admin123$$');

        return $this->resetPasswordMember->resetPassword();
    }

    /**
     * @Then  我可以查到重置的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->resetPassword();

        $this->request();
        $this->response();
    }


    private function request()
    {
        $data = $this->getMemberResetPasswordRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/users/members/resetPassword', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getMemberDetailData()['data'];

        $this->compareArrayAndObject($data, $this->resetPasswordMember);
    }
}
