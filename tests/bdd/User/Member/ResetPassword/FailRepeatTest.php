<?php


namespace Sdk\User\Member\ResetPassword;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\User\Member\Model\Member;
use Sdk\User\Member\MemberDataTrait;
use Sdk\User\Member\MemberRestfulUtils;

/**
 * @Feature: 我是信用网站门户网站的用户,当我忘记了我的密码时,在门户网用户忘记密码页面,找回自己的密码
 *           根据注册时所选择的密保问题与填写的密保答案,以便于我在忘记密码时，重置自己的密码
 * @Scenario: 异常流程-用户名不存在,重置密码失败
 */
class FailRepeatTest extends TestCase
{
    use MemberDataTrait, MemberRestfulUtils, ErrorDataTrait;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new Member();
    }

    public function tearDown()
    {
        unset($this->member);
        unset($this->mock);
    }

    /**
     * @Given: 不存在用户数据
     */
    protected function prepareData()
    {
        $data = $this->getErrorResourceNotExistData();
        $errorResourceNotExistData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], $errorResourceNotExistData)

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用重置密码函数,期待返回false
     */
    protected function resetPassword()
    {
        $data = $this->getMemberDetailData()['data'];

        $this->member = new Member();
        $this->member->setUserName($data['attributes']['userName']);
        $this->member->setSecurityAnswer('黑色');
        $this->member->setPassword('Admin123$$');

        return $this->member->resetPassword();
    }

    /**
     * @Then  用户密码重置失败
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->resetPassword();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();
        $data = $this->getMemberResetPasswordRequestData();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/users/members/resetPassword', $request->getUri()->getPath());
    }

    private function response()
    {
        $this->assertEmpty($this->member->getId());
    }
}
