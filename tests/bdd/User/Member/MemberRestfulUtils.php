<?php
namespace Sdk\User\Member;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
trait MemberRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $member
    ) {
        $this->assertEquals($expectedArray['id'], $member->getId());

        if (isset($expectedArray['attributes']['gender'])) {
            $this->assertEquals($expectedArray['attributes']['gender'], $member->getGender());
        }
        if (isset($expectedArray['attributes']['userName'])) {
            $this->assertEquals($expectedArray['attributes']['userName'], $member->getUserName());
        }
        if (isset($expectedArray['attributes']['realName'])) {
            $this->assertEquals($expectedArray['attributes']['realName'], $member->getRealName());
        }
        if (isset($expectedArray['attributes']['cellphone'])) {
            $this->assertEquals($expectedArray['attributes']['cellphone'], $member->getCellphone());
        }
        if (isset($expectedArray['attributes']['email'])) {
            $this->assertEquals($expectedArray['attributes']['email'], $member->getEmail());
        }
        if (isset($expectedArray['attributes']['cardId'])) {
            $this->assertEquals($expectedArray['attributes']['cardId'], $member->getCardId());
        }
        if (isset($expectedArray['attributes']['contactAddress'])) {
            $this->assertEquals($expectedArray['attributes']['contactAddress'], $member->getContactAddress());
        }
        if (isset($expectedArray['attributes']['securityQuestion'])) {
            $this->assertEquals($expectedArray['attributes']['securityQuestion'], $member->getSecurityQuestion());
        }
        if (isset($expectedArray['attributes']['securityAnswer'])) {
            $this->assertEquals($expectedArray['attributes']['securityAnswer'], $member->getSecurityAnswer());
        }
        if (isset($expectedArray['attributes']['password'])) {
            $this->assertEquals($expectedArray['attributes']['password'], $member->getPassword());
        }
        if (isset($expectedArray['attributes']['oldPassword'])) {
            $this->assertEquals($expectedArray['attributes']['oldPassword'], $member->getOldPassword());
        }
    }
}
