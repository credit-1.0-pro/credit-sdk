<?php


namespace Sdk\User\Member;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Adapter\Member\MemberRestfulAdapter;

trait MemberDataTrait
{
    protected function getMemberList(array $filter = []) : array
    {
        $adapter = new MemberRestfulAdapter();

        list($count, $memberList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $memberList;
    }

    protected function getMemberListData(): array
    {
        return [
            "meta" => [
                "count" => 4,
                "links" => [
                    "first" => null,
                    "last" => null,
                    "prev" => null,
                    "next" => null
                ]
            ],
            "links" => [
                "first" => null,
                "last" => null,
                "prev" => null,
                "next" => null
            ],
            "data" => [
                [
                    "type" => "members",
                    "id" => "15",
                    "attributes" => [
                        "userName" => "贾晓莉",
                        "realName" => "贾晓莉",
                        "cellphone" => "13720406329",
                        "email" => "997934308@qq.com",
                        "cardId" => "412825199009095763",
                        "contactAddress" => "雁塔区长延堡街道",
                        "securityQuestion" => 5,
                        "gender" => 1,
                        "status" => 0,
                        "createTime" => 1653380918,
                        "updateTime" => 1653381255,
                        "statusTime" => 1653381255
                    ],
                    "links" => [
                        "self" => "127.0.0.1:8080/members/15"
                    ]
                ],
                [
                    "type" => "members",
                    "id" => "1",
                    "attributes" => [
                        "userName" => "吴天一",
                        "realName" => "吴天一",
                        "cellphone" => "13720406328",
                        "email" => "997934208@qq.com",
                        "cardId" => "412825199409095764",
                        "contactAddress" => "雁塔区长延堡街道",
                        "securityQuestion" => 5,
                        "gender" => 2,
                        "status" => -2,
                        "createTime" => 1653380973,
                        "updateTime" => 1653381265,
                        "statusTime" => 1653381265
                    ],
                    "links" => [
                        "self" => "127.0.0.1:8080/members/1"
                    ]
                ]
            ]
        ];
    }

    protected function getMemberDetailData(
        int $status = Member::STATUS['ENABLED']
    ) : array {
        return [
            "meta" => [],
            "data" => [
                "type" => "members",
                "id" => 1,
                "attributes" => [
                    "userName" => "吴天一",
                    "realName" => "吴天一",
                    "cellphone" => "13720406328",
                    "email" => "997934208@qq.com",
                    "cardId" => "412825199409095764",
                    "contactAddress" => "雁塔区长延堡街道",
                    "securityQuestion" => 5,
                    "gender" => 2,
                    "status" => $status,
                    "createTime" => 1653380973,
                    "updateTime" => 1653381265,
                    "statusTime" => 1653381265
                ],
                "links" => [
                    "self" => "127.0.0.1:8080/members/1"
                ]
            ]
        ];
    }

    protected function getMemberAddRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"members",
                "attributes"=>array(
                    "userName"=>"吴天一",
                    "realName"=>"吴天一",
                    "cellphone"=>"13720406328",
                    "email"=>"997934208@qq.com",
                    "cardId"=>"412825199409095764",
                    "contactAddress"=>"雁塔区长延堡街道",
                    "securityQuestion"=>5,
                    "securityAnswer"=>"黑色",
                    "password"=>"Admin123$"
                ),
            )
        );
    }

    protected function getMemberEditRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"members",
                "attributes"=>array(
                    "gender"=>1
                ),
            )
        );
    }

    protected function getMemberSignInRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"members",
                "attributes"=>array(
                    "userName"=>"吴天一",
                    "password"=>"Admin123$"
                ),
            )
        );
    }

    protected function getMemberResetPasswordRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"members",
                "attributes"=>array(
                    "userName"=>"吴天一",
                    "securityAnswer"=>"黑色",
                    "password"=>"Admin123$$",
                ),
            )
        );
    }

    protected function getMemberUpdatePasswordRequestData() : array
    {
        return array(
            'data' => array(
                "type"=>"members",
                "attributes"=>array(
                    "password"=>"Admin123$$",
                    "oldPassword"=>"Admin123$",
                ),
            )
        );
    }
}
