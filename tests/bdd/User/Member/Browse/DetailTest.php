<?php
namespace Sdk\User\Member\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Marmot\Framework\Classes\Request;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\MemberDataTrait;
use Sdk\User\Member\MemberRestfulUtils;
use Sdk\User\Member\Adapter\Member\MemberRestfulAdapter;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户 ,当我需要通过OA管理门户网注册的用户们时,我可以查看门户网注册的用户的具体信息,
 *           通过在门户网用户管理列表中进行查看、启用禁用操作, 以便于我可以通过查看与禁用的操作快速处理应急问题
 * @Scenario: 查看用户数据详情
 */
class DetailTest extends TestCase
{
    use MemberDataTrait, MemberRestfulUtils;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new Member();
    }

    public function tearDown()
    {
        unset($this->member);
        unset($this->mock);
    }

    /**
    * @Given: 存在一条用户数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getMemberDetailData());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When: 当我查看该条用户数据详情时
    */
    protected function fetchMember($id)
    {

        $adapter = new MemberRestfulAdapter();

        $this->member = $adapter->fetchOne($id);

        return $this->member;
    }

    /**
     * @Then 我可以看见用户详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchMember($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'users/members/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getMemberDetailData()['data'];

        $this->compareArrayAndObject($data, $this->member);
    }
}
