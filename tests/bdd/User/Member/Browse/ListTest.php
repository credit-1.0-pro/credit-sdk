<?php


namespace Sdk\User\Member\Browse;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\MemberDataTrait;
use Sdk\User\Member\MemberRestfulUtils;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户 ,当我需要通过OA管理门户网注册的用户们时,我可以查看门户网注册的用户的具体信息,
 *           通过在门户网用户管理列表中进行查看、启用禁用操作, 以便于我可以通过查看与禁用的操作快速处理应急问题
 * @Scenario: 查看用户数据列表
 */
class ListTest extends TestCase
{
    use MemberDataTrait, MemberRestfulUtils;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new Member();
    }

    public function tearDown()
    {
        unset($this->member);
        unset($this->mock);
    }

    /**
     * @Given: 存在用户数据
     */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getMemberListData());

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看用户列表时
     */
    public function fetchMemberList() : array
    {
        $filter = [];
        return $this->getMemberList($filter);
    }

    /**
     * @Then  我可以看见用户数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchMemberList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('users/members', $request->getUri()->getPath());
    }

    private function response()
    {
        $memberList = $this->fetchMemberList();
        $memberListArray = $this->getMemberListData()['data'];

        foreach ($memberList as $member) {
            foreach ($memberListArray as $memberArray) {
                if ($memberArray['id'] == $member->getId()) {
                    $this->compareArrayAndObject($memberArray, $member);
                }
            }
        }
    }
}
