<?php


namespace Sdk\User\Member\Edit;

use Marmot\Core;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

use Sdk\ErrorDataTrait;
use Sdk\User\Member\Model\Member;
use Sdk\User\Member\MemberDataTrait;
use Sdk\User\Member\MemberRestfulUtils;

/**
 * @Feature: 我是信用网站门户网站的用户,当我需要对我的个人信息进行修改时,在用户中心页面,可以修改个人信息
 *           通过点击修改个人信息按钮，并进入修改个人信息页面,以便于我及时修改我的个人信息
 * @Scenario: 正常编辑用户数据
 */
class SuccessTest extends TestCase
{
    use MemberDataTrait, MemberRestfulUtils, ErrorDataTrait;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new Member();
    }

    public function tearDown()
    {
        unset($this->member);
        unset($this->mock);
    }

    /**
     * @Given: 我编辑数据
     */
    protected function prepareData()
    {
        $data = $this->getMemberDetailData();
        $requestDataAttributes = $this->getMemberEditRequestData()['data']['attributes'];

        $data['data']['attributes']['gender'] = $requestDataAttributes['gender'];

        $editJsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/vnd.api+json'], $editJsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用编辑函数,期待返回true
     */
    protected function edit()
    {
        $data = $this->getMemberEditRequestData()['data'];

        $this->member = new Member();
        $this->member->setId(1);
        $this->member->setGender($data['attributes']['gender']);

        return $this->member->edit();
    }

    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->edit();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $data = $this->getMemberEditRequestData();
        $request = $this->mock->getLastRequest();

        $this->assertEquals('PATCH', $request->getMethod());
        $this->assertEquals(json_encode($data), $request->getBody()->getContents());
        $this->assertEquals('/users/members/1', $request->getUri()->getPath());
    }

    private function response()
    {
        $data = $this->getMemberEditRequestData()['data'];
        $data['id'] = 1;

        $this->compareArrayAndObject($data, $this->member);
    }
}
