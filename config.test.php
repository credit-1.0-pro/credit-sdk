<?php
ini_set("display_errors", "on");

return [
    //database
    'database.host'     => 'mysql',
    'database.port'     => 3306,
    'database.dbname'   => 'credit_pro_test',
    'database.user'     => 'root',
    'database.password' => '123456',
    'database.tablepre' => 'pcore_',
    //mongo
    'mongo.host' => 'mongodb://mongo:27017',
    'mongo.uriOptions' => [
    ],
    'mongo.driverOptions' => [
    ],
    //cache
    'cache.route.disable' => true,
    //fluentd
    'fluentd.address' => '',
    'fluentd.port' => '24224',
    'fluentd.tag' => '',
    //memcached
    'memcached.serevice'=>[['memcached-1',11211],['memcached-2',11211]],
    //services
    'services.backend.url' => 'http://marmot-backend-sample-code/',
    //cookie
    'cookie.domain'     => '',
    'cookie.path'       => '/',
    'cookie.duration'   => 2592000,
    'cookie.name'       => 'portal_marmot',
    'cookie.encrypt.key' => 'marmot',
    //services
    // 'services.api.url' => 'http://47.96.150.169:8000/',
    // 'services.demo.url' => 'http://47.96.150.169:8080/',
    'baseSdk.url' => 'http://credit-pro-backend-nginx',
    'sdk.url' => 'http://credit-pro-backend-nginx',
    
    //附件路径
    'attachment.upload.path' => '/var/www/html/attachment/upload/',
    //附件下载路径
    'attachment.download.path' => '/attachment/download/',
    //附件本地存储路径
    'attachment.storage.path' => '/var/www/html/public/attachment/download/',

    //资源目录数据上传路径
    'attachment.resourceCatalog.upload.path' => '/var/www/html/attachment/resourceCatalog/upload/',
    //资源目录数据下载路径
    'attachment.resourceCatalog.download.path' => '/var/www/html/attachment/resourceCatalog/download/',
    
    //jwt
    'jwt.key' => '1gHuiop975cdashyex9Ud23ldsvm2Xq',
    'jwt.iss' => 'oa.base.qixinyun.com',
    'jwt.aud' => 'oa.base.qixinyun.com',
    'jwt.nbf' => 0,
    'jwt.exp' => 2592000,
];
